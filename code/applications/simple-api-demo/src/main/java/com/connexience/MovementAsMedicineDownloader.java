package com.connexience;

import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscUser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


public class MovementAsMedicineDownloader {
    String hostname = "mec.escapp.net";
    int port = 80;
    String username = "simon.woodman@ncl.ac.uk"; //set me
    String password = "password"; //set me
    String downloadLocation = "/Users/nsjw7/Desktop/Sarah";
    StorageClient storageClient;
    WorkflowClient wfClient;
    String topLevelId = "460809";

    MovementAsMedicineDownloader() {
        storageClient = new StorageClient(hostname, port, false, username, password);
        wfClient = new WorkflowClient(hostname, port, false, username, password);
    }

    //Download all subdirectories
    public void downloadDirectories() {

        try {

            // Check that the current user can be accessed
            EscUser currentUser = storageClient.currentUser();
            System.out.println(currentUser.getName());

            //Download each directory
            EscFolder[] output_ggirDirs = storageClient.listChildFolders(topLevelId);
            for (EscFolder f : output_ggirDirs) {
                downloadDirectory(f);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void downloadDirectory(EscFolder folder) throws Exception {

        //Get the results folder
        EscFolder resultsFolder = storageClient.getOrCreateNamedChildFolder(folder.getId(), "results");

        //Get each document
        EscDocument[] results = storageClient.folderDocuments(resultsFolder.getId());
        for (EscDocument result : results) {
            try {
                //Download to temp file
                File downloadedFile = new File(downloadLocation + File.separator + "temp.csv");
                storageClient.download(result, downloadedFile);

                //Read each file and rename according to original file/participant id
                //Uses filename as there are two files per participant
                BufferedReader reader = new BufferedReader(new FileReader(downloadedFile));
                reader.readLine(); //header line
                String firstline = reader.readLine();
                String[] parts = firstline.split(",");
                if (parts.length >= 2) {

                    //participant id could be in either column and is quoted.
                    String participantId = parts[1].replaceAll("\"", "");

                    //filename should end with .RData. If it doesn't ues the other column
                    if (!participantId.endsWith(".RData")) {
                        participantId = firstline.split(",")[0].replaceAll("\"", "");
                    }

                    if (participantId == null || participantId.equals("")) {
                        System.err.println("Cannot find participantId in " + firstline);
                    } else {

                        //Rename file
                        participantId = participantId.substring(0, participantId.length() - 6);
                        downloadedFile.renameTo(new File(downloadLocation + File.separator +
                                participantId +
                                "_" + result.getName()));
                    }
                } else {
                    System.err.println("Invalid line: " + firstline);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        MovementAsMedicineDownloader app = new MovementAsMedicineDownloader();
        app.downloadDirectories();
    }

}
