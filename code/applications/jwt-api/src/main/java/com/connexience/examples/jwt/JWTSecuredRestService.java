/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.examples.jwt;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.JWTRecord;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.AuthHelper;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

/**
 * Simple service that uses JWT to authenticate
 * @author hugo
 */
@Path("/rest/jwt")
public class JWTSecuredRestService {
    @Context HttpHeaders headers;
    
    private Ticket getTicket() throws ConnexienceException {
        return AuthHelper.getTicket(headers);
    }
    
    @Path("/login")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_PLAIN)
    public String login(@FormParam("username")String username, @FormParam("password")String password) throws Exception {
        JWTRecord record = EJBLocator.lookupTicketBean().createToken(username, password, "Demo Project");
        return record.getJwt();
    }
    
    @Path("/user")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getUser() throws Exception {
        Ticket t = getTicket();
        User u = EJBLocator.lookupUserDirectoryBean().getUser(t, t.getUserId());
        return u.getName();
    }
}