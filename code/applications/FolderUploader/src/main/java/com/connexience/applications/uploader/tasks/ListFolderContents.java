/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.applications.uploader.*;
import java.util.*;
/**
 * This task lists the contents of a folder
 * @author hugo
 */
public class ListFolderContents extends UploadTask {
    /** ID of the folder to list */
    private String id;

    /** Folder contents */
    private ArrayList<EscFolder> folders = new ArrayList<EscFolder>();
    
    /** Document contents */
    private ArrayList<EscDocument> documents = new ArrayList<EscDocument>();
    
    public ListFolderContents(UploadObject source, String id) {
        super(source);
        this.id = id;
        taskName = "List Folder Contents";
        taskDescription = id;
    }

    @Override
    public void execute() throws Exception {
        EscFolder[] folderList = api.listChildFolders(id);
        for(EscFolder obj : folderList){
            folders.add(obj);
        }
        
        EscDocument[] docList = api.folderDocuments(id);
        for(EscDocument obj : docList){
            documents.add(obj);
        }
    }
    
    public int getFolderCount(){
        return folders.size();
    }
    
    public EscFolder getFolder(int index){
        return folders.get(index);
    }
    
    public int getDocumentCount(){
        return documents.size();
    }
    
    public EscDocument getDocument(int index){
        return documents.get(index);
    }
    
    public boolean containsNamedChild(String name){
        for(EscFolder f : folders){
            if(f.getName().equals(name)){
                return true;
            }
        }
        
        for(EscDocument d : documents){
            if(d.getName().equals(name)){
                return true;
            }
        }
        return false;
    }
    
    public Object getChildByName(String name){
        for(EscFolder f : folders){
            if(f.getName().equals(name)){
                return f;
            }
        }
        
        for(EscDocument d : documents){
            if(d.getName().equals(name)){
                return d;
            }
        }        
        return null;
    }
}