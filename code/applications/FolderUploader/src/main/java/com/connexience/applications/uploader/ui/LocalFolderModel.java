/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.ui;

import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.objects.UploadFolder;
import java.util.ArrayList;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * This class provides a model for displaying the contents of the local folder
 * @author hugo
 */
public class LocalFolderModel implements TreeModel {
    UploadFolder root;
    ArrayList<TreeModelListener> listeners = new ArrayList<TreeModelListener>();
    
    public LocalFolderModel(UploadFolder root) {
        this.root = root;
    }
    
    @Override
    public Object getRoot() {
        return root;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if(parent instanceof UploadFolder){
            return ((UploadFolder)parent).getChild(index);
        } else {
            return null;
        }
        
    }

    @Override
    public int getChildCount(Object parent) {
        if(parent instanceof UploadFolder){
            return ((UploadFolder)parent).getChildCount();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isLeaf(Object node) {
        if(node instanceof UploadFolder){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {
        
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        if(parent instanceof UploadFolder && child instanceof UploadObject){
            return ((UploadFolder)parent).getIndexOfChild((UploadObject)child);
        } else {
            return -1;
        }
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        listeners.remove(l);
    }
    
}
