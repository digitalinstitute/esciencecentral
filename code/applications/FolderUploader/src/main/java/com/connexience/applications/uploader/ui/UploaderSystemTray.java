/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.ui;

import com.connexience.applications.uploader.RootUploader;
import com.connexience.applications.uploader.SimpleUploader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 * This class provides the system tray icon that manages the uploader
 * client. There is one of these for all of the uploaders and it is
 * the starting point for adding and configuring uploaders.
 * @author hugo
 */
public class UploaderSystemTray implements TableModelListener {
    /** Global uploader */
    private ImageIcon idleIcon;
    private ImageIcon busyIcon;
    private ImageIcon errorIcon;
    
    private SimpleUploader uploader;
    private SystemTray tray;
    private TrayIcon icon;
    private Menu uploaders;
    private SettingsWindow settingsWindow;
    private MessagesWindow messagesWindow;
    
    public UploaderSystemTray(SimpleUploader uploader) {
        String os = System.getProperty("os.name").toLowerCase();
        if(os.indexOf("mac")>=0){
            idleIcon = new ImageIcon(getClass().getResource("/images/mac/idle.png"));
            busyIcon = new ImageIcon(getClass().getResource("/images/mac/busy.png"));     
            errorIcon = new ImageIcon(getClass().getResource("/images/mac/error.png"));     
        } else {
            idleIcon = new ImageIcon(getClass().getResource("/images/pc/idle.png"));
            busyIcon = new ImageIcon(getClass().getResource("/images/pc/busy.png"));
            errorIcon = new ImageIcon(getClass().getResource("/images/pc/error.png"));     
        }
        this.uploader = uploader;
        settingsWindow = new SettingsWindow(uploader);
        settingsWindow.setVisible(false);
        
        messagesWindow = new MessagesWindow(uploader);
        messagesWindow.setVisible(false);
        uploader.getErrorManager().addTableModelListener(this);
        setup();
    }

    @Override
    public void tableChanged(TableModelEvent tme) {
        updateIcon();
    }
    
    /** Setup the system tray */
    public void setup(){
        tray = SystemTray.getSystemTray();
        PopupMenu menu = new PopupMenu();
        
        MenuItem settings = new MenuItem("Settings...");
        settings.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                showSettingsWindow();
            }
        });
        
        MenuItem errors = new MenuItem("Error Messages...");
        errors.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                showMessagesWindow();
            }
        });
        
        MenuItem quit = new MenuItem("Quit Uploader");
        quit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                uploader.quit();
            }
        });
        
        MenuItem addUpload = new MenuItem("New Uploader...");
        addUpload.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                RootUploader up = new RootUploader();
                up.setParent(uploader);
                up.configureFromParent();
                RootUploaderFrame panel = new RootUploaderFrame(up, uploader);
                panel.setVisible(true);
            }
        });
        
        
        uploaders = new Menu("Uploaders");
        
        
        menu.add(settings);
        menu.add(errors);
        menu.add(addUpload);
        menu.add(uploaders);
        menu.add(quit);
        
        
        icon = new TrayIcon(idleIcon.getImage(), "Uploader", menu);
        
        try {
            tray.add(icon);
        } catch (Exception e){
            e.printStackTrace();
        }
        createUploadersMenu();
        
    }
    
    public void timeDifferenceChanged(){
        settingsWindow.displayTimeDifference();
    }
    
    /** Show the settings window */
    public void showSettingsWindow(){
        settingsWindow.displayData();
  
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if(settingsWindow != null) {    
                    settingsWindow.setVisible(true);    
                    System.out.println(settingsWindow.requestFocusInWindow());
                    settingsWindow.toFront();
                    settingsWindow.repaint();
                }
            }
        });
           
    }
    
    /** Show the messages window */
    public void showMessagesWindow(){
        messagesWindow.setVisible(true);
    }
    
    /** Update the icon to reflec activity of the client */
    public void updateIcon(){
        if(uploader.getErrorManager().containsErrors()){
            icon.setImage(errorIcon.getImage());
        } else {
            if(uploader.communicatingTaskCount()>0){
                icon.setImage(busyIcon.getImage());
            } else {
                icon.setImage(idleIcon.getImage());
            }
        }
    }
    
    /** Recreate the uploaders menu */
    public void createUploadersMenu(){
        if(uploaders!=null){
            uploaders.removeAll();            
            RootUploader up;
            for(int i=0;i<uploader.getUploaderCount();i++){
                up = uploader.getUploader(i);
                MenuItem m = createMenu(up);
                uploaders.add(m);
            }
        }
    }
    
    /** Create a menu for an uploader */
    private MenuItem createMenu(final RootUploader up){
        MenuItem m = new MenuItem(up.getTargetFolder().getName());

        m.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RootUploaderFrame dialog = new RootUploaderFrame(up, uploader);
                dialog.setVisible(true);
                dialog.requestFocus();
                dialog.toFront();
            }
        });
        
        return m;
    }
}