/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;
import java.util.Date;

/**
 * This class represents an uploader error message. It is stored in the uploader
 * and the presence of unviewed error messages will change the icon in the toolbar
 * to one that indicates a problem.
 * @author hugo
 */
public class UploaderError {
    /** Object that generated the error */
    private RootUploader errorSource;
    
    /** Time of message */
    private Date messageTime = new Date();
    
    /** Message text */
    private String message;

    /** Has this message been viewed */
    private boolean viewed = false;
    
    public UploaderError(RootUploader errorSource, String message) {
        this.errorSource = errorSource;
        this.message = message;
    }

    public RootUploader getErrorSource() {
        return errorSource;
    }

    public String getMessage() {
        return message;
    }

    public Date getMessageTime() {
        return messageTime;
    }

    public boolean isViewed() {
        return viewed;
    }

    public void setViewed(boolean viewed) {
        this.viewed = viewed;
    }
}