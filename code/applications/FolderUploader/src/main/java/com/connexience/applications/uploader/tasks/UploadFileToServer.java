/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;
import com.connexience.applications.uploader.objects.UploadFile;

import com.connexience.server.util.DigestBuilder;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

import java.io.*;

/**
 * This class uploads a file to the server
 * @author hugo
 */
public class UploadFileToServer extends UploadTask{
    private File localFile;
    private String containerId;
    private int settlingInterval = 1000;
    
    public UploadFileToServer(UploadObject source, File localFile, String containerId) {
        super(source);
        this.localFile = localFile;
        this.containerId = containerId;
        taskName = "Upload";
        taskDescription = localFile.getName();
        settlingInterval = PreferenceManager.getEditablePropertyGroup("Settings").intValue("SettlingTime", 1000);
    }
    
    @Override
    public void execute() throws Exception {
        FileInputStream stream = null;
        EscDocument doc = null;
        EscDocumentVersion version = null;
        try {
            if(localFile.exists()){
                waitForSizeToStabilise();
                doc = api.createDocumentInFolder(containerId, localFile.getName());
                
                String versionId = null;
                if(root.isProjectUpload()){
                    doc.setProjectId(root.getProjectId());
                    doc = api.updateDocument(doc);
                    version = api.upload(doc, localFile);
                } else {
                    version = api.upload(doc, localFile);
                }
                
                if(version!=null){
                    String localMd5 = DigestBuilder.calculateMD5(localFile);
                    if(localMd5.equals(version.getMd5())){
                        root.logMessage("File uploaded: " + localFile.getName() + "->" + doc.getId());
                    } else {
                        root.logError("MD5 does not match for: " + localFile.getName());
                        throw new Exception("MD5 mismatch");
                    }

                } else {
                    root.logError("No version returned");
                }
                
            }
        } catch (Exception e){
            if(doc!=null){
                root.logMessage("Compensating for failed upload:");
                
                if(version!=null){
                    try {
                        // Got as far as uploading a version - need to remove this version
                        root.logMessage("Removing failed version: " + version.getId());
                        api.deleteDocumentVersion(doc.getId(), version.getId());
                    } catch (Exception ex){
                        root.logError("Error removing failed upload: " + ex.getMessage());
                    }
                }
                
                try {
                    EscDocumentVersion[] versions = api.listDocumentVersions(doc.getId());
                    if(versions.length==0){
                        // Remove the document as there is no data
                        root.logMessage("No versions in document - deleting document");
                        api.deleteDocument(doc.getId());
                    }
                } catch (Exception ex){
                    root.logError("Error removing empty document: " + ex.getMessage());
                }                
                
                // Remove the record from the parent so that a re-scan can happen 
                if(getSource() instanceof UploadFile){
                    ((UploadFile)getSource()).getParentFolder().removeChildDocument(localFile);
                }
            }
            throw e;
        }
    }
    
    /** Wait for a file size to stabilise */
    private void waitForSizeToStabilise(){
        long size = localFile.length();
        long newSize;
        boolean settled = false;
        while(!settled){
            try {
                Thread.sleep(settlingInterval);
            } catch (Exception e){}
            newSize = localFile.length();
            if(newSize==size){
                settled = true;
            } else {
                size = newSize;
            }
        }
    }
}
