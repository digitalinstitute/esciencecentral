/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;
import com.connexience.api.*;
/**
 * This class defines a basic task that the uploader can perform
 * @author hugo
 */
public abstract class UploadTask implements Runnable {
    /** Status enumn */
    public enum UploadStatus {
        TASK_OK,
        TASK_WAITING,
        TASK_RUNNING,
        TASK_FAILED
    }
    
    /** Status of the task */
    protected UploadStatus status = UploadStatus.TASK_WAITING;
    
    /** API to do the call */
    protected StorageClient api;

    /** Callback for when the task has finished */
    protected UploadTaskCallback callback;
    
    /** Root uploader */
    protected RootUploader root;
    
    /** Source of the task */
    protected UploadObject source = null;
    
    /** Name of this task */
    protected String taskName;
    
    /** Longer description */
    protected String taskDescription;
    
    /** Minimum ammount of time that this task is required to run. This is set
     * to reduce the load on the server by making sure that not too many tasks
     * execute quickly */
    protected int minimumTaskTime = 500;
    
    public UploadTask(UploadObject source) {
    }

    public void setApi(StorageClient api) {
        this.api = api;
    }

    public void setCallback(UploadTaskCallback callback){
        this.callback = callback;
    }

    public RootUploader getRoot(){
        return root;
    }
    
    public void setRoot(RootUploader root) {
        this.root = root;
    }

    @Override
    public void run() {
        try {
            long startTime = System.currentTimeMillis();
            status = UploadStatus.TASK_RUNNING;
            root.taskStarted(this);
            execute();
            while(System.currentTimeMillis() < (startTime + minimumTaskTime)){
                try{
                    long delta = (startTime + minimumTaskTime) - System.currentTimeMillis();
                    if(delta>0){
                        Thread.sleep(delta + 100);
                    }
                } catch (Exception e){
                    
                }
            }
            status = UploadStatus.TASK_OK;
            notifyOk();
            
        } catch(Exception e){
            status = UploadStatus.TASK_FAILED;
            notifyFailed(e.getMessage());
        }
        root.taskFinished(this);
    }
    
    
    private void notifyOk(){
        if(callback!=null){
            callback.taskSucceeded(this);
        }
    }
    
    private void notifyFailed(String message){
        if(callback!=null){
            callback.taskFailed(this, message);
        }
    }

    public UploadObject getSource() {
        return source;
    }

    public String getTaskName() {
        return taskName;
    }

    public UploadStatus getStatus() {
        return status;
    }

    public String getTaskDescription() {
        return taskDescription;
    }
    
    
    
    
    public abstract void execute() throws Exception;
    
    
}
