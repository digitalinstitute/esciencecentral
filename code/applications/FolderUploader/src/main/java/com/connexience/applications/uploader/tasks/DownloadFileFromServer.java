/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;

import java.io.File;
import java.io.FileOutputStream;


/**
 * Download a file from the server to the local directory
 * @author hugo
 */
public class DownloadFileFromServer extends UploadTask {
    EscDocument serverDocument;
    File localFile;
    
    public DownloadFileFromServer(UploadObject source, EscDocument serverDocument, File localFile) {
        super(source);
        this.serverDocument = serverDocument;
        this.localFile = localFile;
        taskName = "Download file";
        taskDescription = "Checking " + localFile.getName();        
    }

    @Override
    public void execute() throws Exception {
        try {
            if(localFile.exists()){
                // Find out the timestamp of the latest version
                EscDocumentVersion v = api.getLatestDocumentVersion(serverDocument.getId());
                if(getRoot().adjustToServerTime(localFile.lastModified())<v.getTimestamp()){
                    // Server version is newer
                    FileOutputStream stream = new FileOutputStream(localFile);
                    api.download(serverDocument, stream);
                    stream.flush();
                    stream.close();
                    root.logMessage("Downloaded newer version of: " + serverDocument.getName());                      
                }
                
            } else {
                // Download the latest version
                FileOutputStream stream = new FileOutputStream(localFile);
                api.download(serverDocument, stream);
                stream.flush();
                stream.close();
                root.logMessage("Downloaded: " + serverDocument.getName());                
            }
        } catch (Exception e){
            root.logError("Error downloading file: " + e.getMessage());
        }
    }
}