/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;
import java.io.*;
import com.connexience.api.StorageClient;
import com.connexience.api.model.*;


/**
 * This class is the base class for an object that can be uploaded to the server.
 * It can represent either a file or a folder.
 * @author hugo
 */
public abstract class UploadObject {
    protected String id;
    protected String name;
    protected String containerId;
    protected File localFile;
    protected RootUploader root;
    
    protected UploadObject(File localFile, RootUploader root){
        this.localFile = localFile;
        this.root = root;
        this.name = localFile.getName();
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public File getLocalFile() {
        return localFile;
    }

    public void setLocalFile(File localFile) {
        this.localFile = localFile;
    }
    
    public RootUploader getRootUploader(){
        return root;
    }
    
    public void process(){
        
    }
    
    public void clear(){
        localFile = null;
        id = null;
        name = null;
        root = null;
    }
    
    public void purgeTasksFromQueue(){
        root.purgeTasksForObject(this);
    }
    
    public void scanForChanges(){
        
    }
}