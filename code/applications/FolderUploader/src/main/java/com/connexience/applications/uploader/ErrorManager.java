/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader;

import java.text.DateFormat;
import java.util.ArrayList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * This class stores and manages uploader error messages
 * @author hugo
 */
public class ErrorManager implements TableModel {
    /** List of errors */
    private ArrayList<UploaderError> errors = new ArrayList<UploaderError>();
        
    /** Date format for message time */
    private DateFormat format = DateFormat.getDateTimeInstance();
    
    /** Listeners */
    private ArrayList<TableModelListener> listeners = new ArrayList<TableModelListener>();
    
    public void add(UploaderError error){
        errors.add(error);
        notifyChange();
    }
    
    public void clear(){
        errors.clear();
        notifyChange();
    }
    
    public boolean containsErrors(){
        if(errors.size()>0){
            return true;
        } else {
            return false;
        }
    }
    
    private void notifyChange(){
        for(TableModelListener l : listeners){
            l.tableChanged(new TableModelEvent(this));
        }
    }

    @Override
    public int getRowCount() {
        return errors.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int i) {
        switch(i){
            case 0:
                return "Time";
            case 1:
                return "Message";
            default:
                return "NA";
        }
    }

    @Override
    public Class<?> getColumnClass(int i) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        switch(i1){
            case 0:
                return format.format(errors.get(i).getMessageTime());
            case 1:
                return errors.get(i).getMessage();
            default:
                return "NA";
        }
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        listeners.add(tl);
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        listeners.remove(tl);
    }
    
}