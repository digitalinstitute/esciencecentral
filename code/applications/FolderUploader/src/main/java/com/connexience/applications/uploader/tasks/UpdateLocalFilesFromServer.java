/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;
import com.connexience.applications.uploader.objects.UploadFolder;

/**
 * This class provides a task that downloads files from the remote server to the 
 * local disk if they don't already exist.
 * @author hugo
 */
public class UpdateLocalFilesFromServer extends UploadTask {
    private UploadFolder baseFolder;
    
    public UpdateLocalFilesFromServer(UploadObject source, UploadFolder baseFolder) {
        super(source);
        this.baseFolder = baseFolder;
        taskName = "Sync with Server";
        taskDescription = "Syncing remote files in: " + baseFolder.getName();
    }

    @Override
    public void execute() throws Exception {
        baseFolder.process();
    }
    
    
}