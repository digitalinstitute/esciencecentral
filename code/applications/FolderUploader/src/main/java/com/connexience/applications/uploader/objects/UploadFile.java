/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.objects;
import com.connexience.api.model.EscDocument;
import com.connexience.applications.uploader.RootUploader;
import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;
import com.connexience.applications.uploader.UploadTaskCallback;
import com.connexience.applications.uploader.tasks.DownloadFileFromServer;
import com.connexience.applications.uploader.tasks.UploadFileToServer;
import java.io.*;

/**
 * Represents a mapping between a local and remote file
 * @author hugo
 */
public class UploadFile extends UploadObject {
    private boolean uploadProcessed = false;
    private boolean uploadError = true;
    private UploadFolder parentFolder;
    public UploadFile(File localFile, RootUploader root, UploadFolder parentFolder) {
        super(localFile, root);
        this.parentFolder = parentFolder;
    }

    public UploadFolder getParentFolder() {
        return parentFolder;
    }

    @Override
    public void process() {
        UploadTaskCallback cb = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
                uploadError= false;
            }

            @Override
            public void taskFailed(UploadTask task, String message) {
                uploadError = true;
                root.logError("Error uploading file: " + message);
            }
        };
        
        uploadProcessed = true;
        if(!root.getParent().isBannedName(localFile.getName())){
            UploadFileToServer task = new UploadFileToServer(this, localFile, containerId);
            root.queueTask(task, cb);
        } else {
            uploadError = false;
        }
    }

    /** Download the file from the server */
    public void downloadFromServer(EscDocument serverDoc){
        id = serverDoc.getId();
        containerId = serverDoc.getContainerId();
        
        UploadTaskCallback cb = new UploadTaskCallback() {

            @Override
            public void taskSucceeded(UploadTask task) {
                uploadError = false;
                uploadProcessed = true;
            }

            @Override
            public void taskFailed(UploadTask task, String message) {
                uploadError = true;
                uploadProcessed = true;
                root.logError("Error downloading file: " + message);
            }
        };
        
        if(!localFile.exists() || root.isOverwriteWithNewer()){
            DownloadFileFromServer task = new DownloadFileFromServer(this, serverDoc, localFile);
            root.queueTask(task, null);
        }
    }
    
    public boolean isUploadProcessed() {
        return uploadProcessed;
    }

    public boolean isUploadError() {
        return uploadError;
    }

    public void setUploadProcessed(boolean uploadProcessed) {
        this.uploadProcessed = uploadProcessed;
    }
    
    @Override
    public String toString() {
        if(localFile!=null){
            return localFile.getName();
        } else {
            return "NO NAME";
        }
    }    
}