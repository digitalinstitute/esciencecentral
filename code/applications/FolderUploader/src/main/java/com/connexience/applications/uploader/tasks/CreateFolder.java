/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.api.model.EscFolder;
import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;


/**
 * Create a remote folder
 * @author hugo
 */
public class CreateFolder extends UploadTask {
    private String containerId;
    private String name;
    private EscFolder createdFolder;
    
    public CreateFolder(UploadObject source, String containerId, String name) {
        super(source);
        this.containerId = containerId;
        this.name = name;
        taskName = "Create Folder";
        taskDescription = name;
    }
    
    @Override
    public void execute() throws Exception {
        createdFolder = api.createChildFolder(containerId, name);
        root.logMessage("Folder created: " + name);
    }

    public EscFolder getCreatedFolder() {
        return createdFolder;
    }
}
