/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.applications.uploader.tasks;

import com.connexience.applications.uploader.UploadObject;
import com.connexience.applications.uploader.UploadTask;
import com.connexience.applications.uploader.objects.UploadFolder;

/**
 * This class scans for changes in a root folder
 * @author hugo
 */
public class ScanChanges extends UploadTask {
    UploadFolder baseFolder;

    public ScanChanges(UploadObject source, UploadFolder baseFolder) {
        super(source);
        this.baseFolder = baseFolder;
        taskName = "Scan For Changes";
        taskDescription = "Regular task";
    }
   
    @Override
    public void execute() throws Exception {
        baseFolder.scanForChanges();
    }
}
