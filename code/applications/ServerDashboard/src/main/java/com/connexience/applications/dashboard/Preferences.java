/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.applications.dashboard;

import java.util.Enumeration;
import javax.swing.DefaultListModel;

/**
 *
 * @author hugo
 */
public class Preferences extends javax.swing.JDialog {
    Dashboard dashboard;
    DefaultListModel<String> listModel = new DefaultListModel<String>();
    
    /**
     * Creates new form Preferences
     */
    public Preferences(java.awt.Frame parent, Dashboard dashboard) {
        super(parent, true);
        this.dashboard = dashboard;
        setSize(500, 500);
        setLocationRelativeTo(null);
        initComponents();
        
        mailUserField.setText(dashboard.getMailUser().trim());
        mailPasswordField.setText(dashboard.getMailPassword());
        enableMessagesCheckbox.setSelected(dashboard.isEnableMessages());
        
        for(String s : dashboard.getEmails()){
            listModel.addElement(s);
        }
        addressList.setModel(listModel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        okButton = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        enableMessagesCheckbox = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        mailPasswordField = new javax.swing.JTextField();
        mailUserField = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        addressList = new javax.swing.JList();
        jPanel4 = new javax.swing.JPanel();
        deleteAddressButton = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        newAddressField = new javax.swing.JTextField();
        addAddressButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                okButtonActionPerformed(evt);
            }
        });
        jPanel1.add(okButton);

        jButton2.setText("Cancel");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        enableMessagesCheckbox.setText("Send message E-Mails");
        enableMessagesCheckbox.setToolTipText("");

        jLabel1.setText("Mail user:");

        jLabel2.setText("Mail password:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(enableMessagesCheckbox, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mailPasswordField)
                            .addComponent(mailUserField))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(enableMessagesCheckbox)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(mailUserField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(mailPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.NORTH);

        jPanel3.setLayout(new java.awt.BorderLayout());

        addressList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        addressList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(addressList);

        jPanel3.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        deleteAddressButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/remove.png"))); // NOI18N
        deleteAddressButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteAddressButtonActionPerformed(evt);
            }
        });
        jPanel4.add(deleteAddressButton);

        jPanel3.add(jPanel4, java.awt.BorderLayout.EAST);

        jPanel5.setLayout(new java.awt.BorderLayout());
        jPanel5.add(newAddressField, java.awt.BorderLayout.CENTER);

        addAddressButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/add.png"))); // NOI18N
        addAddressButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAddressButtonActionPerformed(evt);
            }
        });
        jPanel5.add(addAddressButton, java.awt.BorderLayout.EAST);

        jPanel3.add(jPanel5, java.awt.BorderLayout.SOUTH);

        getContentPane().add(jPanel3, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void okButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_okButtonActionPerformed
        dashboard.setMailUser(mailUserField.getText().trim());
        dashboard.setMailPassword(mailPasswordField.getText());
        dashboard.setEnableMessages(enableMessagesCheckbox.isSelected());
        dashboard.saveSettings();
        dashboard.getEmails().clear();
        Enumeration<String> addresses = listModel.elements();
        while(addresses.hasMoreElements()){
            dashboard.getEmails().add(addresses.nextElement());
        }
        setVisible(false);
    }//GEN-LAST:event_okButtonActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void deleteAddressButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteAddressButtonActionPerformed
        if(addressList.getSelectedIndex()!=-1){
            listModel.remove(addressList.getSelectedIndex());
        }
    }//GEN-LAST:event_deleteAddressButtonActionPerformed

    private void addAddressButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAddressButtonActionPerformed
        if(!newAddressField.getText().trim().isEmpty()){
            listModel.addElement(newAddressField.getText().trim());
        }
    }//GEN-LAST:event_addAddressButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAddressButton;
    private javax.swing.JList addressList;
    private javax.swing.JButton deleteAddressButton;
    private javax.swing.JCheckBox enableMessagesCheckbox;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField mailPasswordField;
    private javax.swing.JTextField mailUserField;
    private javax.swing.JTextField newAddressField;
    private javax.swing.JButton okButton;
    // End of variables declaration//GEN-END:variables
}
