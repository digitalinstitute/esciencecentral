/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.applications.dashboard;

import java.awt.Color;

/**
 *
 * @author hugo
 */
public class DashboardColors {
    public static final Color okBackgroundColor = new Color(0,102,0);
    public static final Color okTextColor = Color.WHITE;
    
    public static final Color errorTextColor = Color.WHITE;   
    public static final Color errorBackgroundColor = new Color(102,0,0);
    
    public static final Color warningTextColor = Color.BLACK;
    public static final Color warningBackgroundColor = new Color(255,204,0);
    
    public static final Color uptimeWarningColor = new Color(255,204,0);
    public static final Color uptimeOkColor = new Color(0,102,0);
    public static final Color uptimeErrorColor = new Color(102,0,0);
}