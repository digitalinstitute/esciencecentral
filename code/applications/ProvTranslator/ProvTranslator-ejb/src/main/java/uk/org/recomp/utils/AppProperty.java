package uk.org.recomp.utils;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.*;

/**
 * Inspired by Chris Ritche http://stackoverflow.com/a/28996863/752580
 */
@Documented
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
//@Target({ElementType.FIELD, ElementType.LOCAL_VARIABLE, ElementType.METHOD})
public @interface AppProperty {

    @Nonbinding
    String name();

    @Nonbinding
    String defaultValue() default "";
}
