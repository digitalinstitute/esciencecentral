package uk.org.recomp.utils;

import com.connexience.api.StorageClient;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import org.jboss.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Jacek on 09/08/2016.
 */
public class Resources
{
    private static final Logger _Logger = Logger.getLogger(Resources.class);


    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass());
    }


    @Produces
    public StorageRemote produceStorageAPIRef() {
        try {
            return EJBLocator.lookupStorageBean();
            //return new StorageClient();
        } catch (Exception x) {
            _Logger.warn("Cannot create a StorageClient reference", x);
            return null;
        }
    }
}
