package uk.org.recomp.utils;

import org.jboss.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import java.lang.annotation.Annotation;

/**
 * Created by Jacek on 12/08/2016.
 * Inspired by Chris Ritchie http://stackoverflow.com/a/28996863/752580
 */
public class AppPropertyProducer
{
    @Inject
    PropertiesLoader _props;

    @Produces
    @AppProperty(name = "")
    public String getPropertyAsString(InjectionPoint injectionPoint) {
        AppProperty a = injectionPoint.getAnnotated().getAnnotation(AppProperty.class);
        return _props.getProperty(a.name(), a.defaultValue());
    }
}
