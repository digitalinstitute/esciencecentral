package uk.org.recomp.utils;

import org.jboss.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Jacek on 12/08/2016.
 * Inspired by Chris Ritchie http://stackoverflow.com/a/28996863/752580
 */
@Singleton
public class PropertiesLoader
{
    private static final String _PROPERTIES_FILE_NAME = "prov-translator.properties";

    public static final String PROP_NAME_HDB_URL = "uk.org.recomp.hdb.url";
    public static final String PROP_DEFAULT_HDB_URL = "http://localhost:5000/hdb";

    @Inject
    private Logger _logger;

    private java.util.Properties properties;

    @PostConstruct
    private void _init() {
        properties = new java.util.Properties();

        // Try to read the properties file
        Path propFile = Paths.get(System.getProperty("user.home"), ".inkspot", _PROPERTIES_FILE_NAME);
        _logger.info("Trying to load properties file: " + propFile);
        try (Reader reader = Files.newBufferedReader(propFile, StandardCharsets.UTF_8)) {
            properties.load(reader);
            _logger.info("Loaded properties from file: " + propFile);
        } catch (IOException x) {
            _logger.warn("Cannot load properties file", x);
        }
    }


    public String getProperty(String key, String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }
}
