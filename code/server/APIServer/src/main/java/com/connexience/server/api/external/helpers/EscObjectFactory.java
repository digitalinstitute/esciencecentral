/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.helpers;

import com.connexience.api.model.*;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.MetadataQueryItem;
import com.connexience.server.model.metadata.queries.BooleanMetadataQueryItem;
import com.connexience.server.model.metadata.queries.TextMetadataQueryItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatDevice;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatPerson;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.DynamicWorkflowLibrary;
import com.connexience.server.model.workflow.DynamicWorkflowService;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameter;
import com.connexience.server.model.workflow.WorkflowParameterList;
import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;
import org.bson.Document;
/**
 * This class creates EscXX objects from the standard database model objects
 * @author hugo
 */
public class EscObjectFactory {
    private static void populateEscObject(EscObject obj, ServerObject serverObj){
        obj.setContainerId(serverObj.getContainerId());
        obj.setCreatorId(serverObj.getCreatorId());
        obj.setDescription(serverObj.getDescription());
        obj.setId(serverObj.getId());
        obj.setName(serverObj.getName());
        obj.setProjectId(serverObj.getProjectId());
        obj.setCreationTime(serverObj.getTimeInMillis());
        obj.setInternalClassName(serverObj.getClass().getName());
    }
    
    public static EscDocument createEscDocument(DocumentRecord doc){
        EscDocument escDoc;
        if(doc instanceof WorkflowDocument){
             escDoc = new EscWorkflow();
        } else {
            escDoc = new EscDocument();
        }
        
        populateEscObject(escDoc, doc);
        escDoc.setCurrentVersionSize(doc.getCurrentVersionSize());
        escDoc.setCurrentVersionNumber(doc.getCurrentVersionNumber());
        escDoc.setCurrentVersionHash(doc.getCurrentVersionHash());
        escDoc.setDownloadPath("/data/" + doc.getId() + "/latest");
        escDoc.setUploadPath("/data/" + doc.getId());
        return escDoc;
    }
    
    public static EscDocumentVersion createEscDocumentVersion(DocumentVersion v){
        EscDocumentVersion escVersion = new EscDocumentVersion();
        escVersion.setId(v.getId());
        escVersion.setDocumentRecordId(v.getDocumentRecordId());
        escVersion.setComments(v.getComments());
        escVersion.setUserId(v.getUserId());
        escVersion.setVersionNumber(v.getVersionNumber());
        escVersion.setSize(v.getSize());
        escVersion.setTimestamp(v.getTimestamp());
        escVersion.setDownloadPath("/data/" + v.getDocumentRecordId()+ "/" + v.getId());
        escVersion.setMd5(v.getMd5());
        return escVersion;
    }
    
    public static EscFolder createEscFolder(Folder f){
        EscFolder escFolder = new EscFolder();
        populateEscObject(escFolder, f);
        return escFolder;
    }
    
    public static EscProject createEscProject(Project p){
        EscProject escProject = new EscProject();
        escProject.setId(Long.toString(p.getId()));
        escProject.setExternalId(p.getExternalId());
        escProject.setName(p.getName());
        escProject.setDescription(p.getDescription());
        escProject.setDataFolderId(p.getDataFolderId());
        escProject.setWorkflowFolderId(p.getWorkflowFolderId());
        escProject.setCreatorId(p.getOwnerId());
        
        if(p instanceof FlatStudy){
            escProject.setProjectType(EscProject.ProjectType.FLAT);
        } else if(p instanceof Study){
            escProject.setProjectType(EscProject.ProjectType.HEIRARCHICAL);
        } else {
            escProject.setProjectType(EscProject.ProjectType.BASIC);
        }
        
        return escProject;
    }
    
    public static EscUser createEscUser(User u){
        EscUser escUser = new EscUser();
        escUser.setId(u.getId());
        escUser.setName(u.getName());
        escUser.setFirstName(u.getFirstName());
        escUser.setSurname(u.getSurname());
        escUser.setWorkflowFolderId(u.getWorkflowFolderId());
        escUser.setHomeFolderId(u.getHomeFolderId());
        return escUser;
    }
    
    public static EscWorkflow createEscWorkflow(WorkflowDocument wf){
        EscWorkflow escWorkflow = new EscWorkflow();
        populateEscObject(escWorkflow, wf);
        escWorkflow.setCurrentVersionNumber(wf.getCurrentVersionNumber());
        escWorkflow.setCurrentVersionSize(wf.getCurrentVersionSize());
        return escWorkflow;
    }
    
    public static EscWorkflowInvocation createEscWorkflowInvocation(WorkflowInvocationFolder f, String workflowName){
        EscWorkflowInvocation escWorkflowInvocation = new EscWorkflowInvocation();
        populateEscObject(escWorkflowInvocation, f);
        escWorkflowInvocation.setWorkflowName(workflowName);
        escWorkflowInvocation.setWorkflowId(f.getWorkflowId());
        escWorkflowInvocation.setWorkflowVersionId(f.getVersionId());
        escWorkflowInvocation.setPercentComplete(f.getPercentComplete());
        escWorkflowInvocation.setEngineId(f.getEngineId());

        if (f.getQueuedTime() != null) {
            escWorkflowInvocation.setQueuedTimestamp(f.getQueuedTime().getTime());
        }

        if (f.getDequeuedTime() != null) {
            escWorkflowInvocation.setDequeuedTimestamp(f.getDequeuedTime().getTime());
        }

        if(f.getExecutionStartTime()!=null){
            escWorkflowInvocation.setStartTimestamp(f.getExecutionStartTime().getTime());
        }
        if(f.getExecutionEndTime()!=null){
            escWorkflowInvocation.setEndTimestamp(f.getExecutionEndTime().getTime());
        }

        switch(f.getInvocationStatus()){
            case WorkflowInvocationFolder.INVOCATION_FINISHED_OK:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_FINISHED_OK);
                break;
                
            case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_FINISHED_WITH_ERRORS);
                break;
                
            case WorkflowInvocationFolder.INVOCATION_RUNNING:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_RUNNING);
                break;
                
            case WorkflowInvocationFolder.INVOCATION_WAITING:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_WAITING);
                break;
                
            case WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_WAITING_FOR_DEBUGGER);
                break;
                
            default:
                escWorkflowInvocation.setStatus(EscWorkflowInvocation.INVOCATION_STATE_UNKNOWN);
        }

        escWorkflowInvocation.setStatusMessage(f.getMessage());

        return escWorkflowInvocation;   
    }
    
    public static WorkflowParameter createEscWorkflowParameter(EscWorkflowParameter escWorkflowParameter){
        WorkflowParameter p = new WorkflowParameter();
        p.setBlockName(escWorkflowParameter.getBlockName());
        p.setName(escWorkflowParameter.getName());
        p.setValue(escWorkflowParameter.getValue());
        return p;
    }
    
    public static WorkflowParameterList createEscWorkflowParameterList(EscWorkflowParameterList escWorkflowParameterList){
        WorkflowParameterList p = new WorkflowParameterList();
        EscWorkflowParameter[] values = escWorkflowParameterList.getValues();
        for(int i=0;i<values.length;i++){
            p.add(values[i].getBlockName(), values[i].getName(), values[i].getValue());
        }
        return p;
    }
    
    public static MetadataQueryItem createMetadataQueryItem(EscMetadataItem escMetadataItem) throws Exception {
        MetadataQueryItem q = null;
        switch(escMetadataItem.getMetadataType()){
            case BOOLEAN:
                q = new BooleanMetadataQueryItem();
                q.setCaseSensitiveCategoryName(false);
                q.setCaseSensitiveName(false);
                q.setCategory(escMetadataItem.getCategory());
                q.setName(escMetadataItem.getName());                
                ((BooleanMetadataQueryItem)q).setTargetValue(Boolean.valueOf(escMetadataItem.getStringValue()));
                return q;

            case TEXT:
                q = new TextMetadataQueryItem();
                q.setCaseSensitiveCategoryName(false);
                q.setCaseSensitiveName(false);
                q.setCategory(escMetadataItem.getCategory());
                q.setName(escMetadataItem.getName());                
                ((TextMetadataQueryItem)q).setSearchText(escMetadataItem.getStringValue());
                return q;
               
            default:
                throw new Exception("Unsupported metadata type");
        }


    }
    
    public static EscMetadataItem createMetadataItem(MetadataItem md){
        EscMetadataItem escMetadataItem = new EscMetadataItem();
        escMetadataItem.setObjectId(md.getObjectId());
        escMetadataItem.setName(md.getName());
        escMetadataItem.setCategory(md.getCategory());
        escMetadataItem.setStringValue(md.getStringValue());
        escMetadataItem.setId(md.getId());
        
        if(md instanceof TextMetadata){
            escMetadataItem.setMetadataType(EscMetadataItem.METADATA_TYPE.TEXT);
            
        } else if(md instanceof BooleanMetadata){
            escMetadataItem.setMetadataType(EscMetadataItem.METADATA_TYPE.BOOLEAN);
            
        } else if(md instanceof NumericalMetadata){
            escMetadataItem.setMetadataType(EscMetadataItem.METADATA_TYPE.NUMERICAL);
            
        } else if(md instanceof DateMetadata){
            escMetadataItem.setMetadataType(EscMetadataItem.METADATA_TYPE.DATE);
            
        } else {
            escMetadataItem.setMetadataType(EscMetadataItem.METADATA_TYPE.TEXT);
            
        }
        
        return escMetadataItem;
    }
    
    public static EscWorkflowService createEscWorkflowService(DynamicWorkflowService service){
        EscWorkflowService escWorkflowService = new EscWorkflowService();
        populateEscObject(escWorkflowService, service);
        escWorkflowService.setCategory(service.getCategory());
        escWorkflowService.setCurrentVersionNumber(service.getCurrentVersionNumber());
        escWorkflowService.setCurrentVersionSize(service.getCurrentVersionSize());
        escWorkflowService.setDownloadPath("/data/" + service.getId() + "/latest");
        escWorkflowService.setUploadPath("/data/" + service.getId());
        return escWorkflowService;
    }
    
    public static EscPermission createEscPermission(Permission p){
        EscPermission escPermission = new EscPermission();
        escPermission.setPermissionType(p.getType());
        escPermission.setPrincipalId(p.getPrincipalId());
        return escPermission;
    }
    
    public static EscWorkflowLibrary createEscWorkflowLibrary(DynamicWorkflowLibrary library){
        EscWorkflowLibrary escLibrary = new EscWorkflowLibrary();
        populateEscObject(escLibrary, library);
        escLibrary.setCurrentVersionNumber(library.getCurrentVersionNumber());
        escLibrary.setCurrentVersionSize(library.getCurrentVersionSize());
        escLibrary.setDownloadPath("/data" + library.getId() + "/latest");
        escLibrary.setUploadPath("/data/" + library.getId());
        escLibrary.setLibraryName(library.getLibraryName());
        return escLibrary;
    }
    
    public static EscGateway createEscGateway(FlatGateway gateway, String studyCode) {
        EscGateway escGateway = new EscGateway();
        escGateway.setExternalId(gateway.getExternalId());
        escGateway.setFolderId(gateway.getFolderId());
        escGateway.setId(gateway.getId());
        escGateway.setName(gateway.getName());
        escGateway.setStudyId(gateway.getStudyId());
        escGateway.getAdditionalProperties().clear();
        for(String key : gateway.getAdditionalProperties().keySet()){
            escGateway.getAdditionalProperties().put(key, gateway.getAdditionalProperties().get(key));
        }
        escGateway.setStudyCode(studyCode);
        return escGateway;
    }    
    
    public static EscPerson createEscPerson(FlatPerson person){
        EscPerson escPerson = new EscPerson();
        escPerson.setExternalId(person.getExternalId());
        escPerson.setId(person.getId());
        escPerson.setFolderId(person.getFolderId());
        escPerson.setName(person.getName());
        escPerson.setStudyId(person.getStudyId());
        escPerson.getAdditionalProperties().clear();
        for(String key : person.getAdditionalProperties().keySet()){
            escPerson.getAdditionalProperties().put(key, person.getAdditionalProperties().get(key));
        }        
        return escPerson;
    }
    
    public static EscDevice createEscDevice(FlatDevice device){
        EscDevice escDevice = new EscDevice();
        escDevice.setExternalId(device.getExternalId());
        escDevice.setFolderId(device.getFolderId());
        escDevice.setId(device.getId());
        escDevice.setName(device.getName());
        escDevice.setStudyId(device.getStudyId());
        escDevice.getAdditionalProperties().clear();
        for(String key : device.getAdditionalProperties().keySet()){
            escDevice.getAdditionalProperties().put(key, device.getAdditionalProperties().get(key));
        }           
        return escDevice;
    }
}
