/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.helpers;

import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscGatewayRegistrationRequest;
import com.connexience.api.model.EscGatewayRegistrationResponse;
import com.connexience.api.model.EscProject;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.StorageUtils;
import java.util.List;
import com.connexience.api.model.CatalogInterface;
import com.connexience.api.model.EscDevice;
import com.connexience.api.model.EscEvent;
import com.connexience.api.model.EscGateway;
import com.connexience.api.model.EscPerson;
import com.connexience.server.model.project.flatstudy.FlatDevice;
import com.connexience.server.model.project.flatstudy.FlatPerson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.ejb.EJBLocalObject;
import javax.ws.rs.PathParam;
import org.apache.log4j.Logger;

/**
 * Code to support the FlatStudyAPI
 * @author hugo
 */
public class FlatStudyHelper {
    private static final Logger logger = Logger.getLogger(FlatStudyHelper.class);
    
    private Ticket t;
    
    public FlatStudyHelper() {
    }

    public FlatStudyHelper(Ticket t) {
        this.t = t;
    }
                
    public EscProject[] listFlatStudies() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public EscGatewayRegistrationResponse registerGateway(EscGatewayRegistrationRequest request) throws Exception {
        Ticket studyTicket = EJBLocator.lookupTicketBean().createStudyTicket(request.getStudyCode());
        List<Project> projects = EJBLocator.lookupProjectsBean().getProjectsByExternalId(studyTicket, request.getStudyCode());
        
        if(projects.size()==1 && projects.get(0) instanceof FlatStudy){
            FlatStudy study = (FlatStudy)projects.get(0);
            EJBLocator.lookupFlatStudyBean().checkFlatStudyCodeFolders(studyTicket, study.getId());
            
            String registrationCode;
            if(study.getAdditionalProperties().containsKey("RegistrationCode")){
                registrationCode = study.getAdditionalProperty("RegistrationCode").trim();
            } else {
                registrationCode = EJBLocator.lookupPreferencesBean().stringValue("IOT", "DefaultDeviceRegistrationCode", "0000");
            }

            // Is there already a gateway present
            FlatGateway gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalId(studyTicket, study.getId(), request.getGatewayId());
            if(gateway==null){
                
                // Check with registration code
                if(request.verify(registrationCode)){
                    gateway = EJBLocator.lookupFlatStudyBean().createFlatGateway(studyTicket, study.getId(), request.getGatewayId());
                    String devicePassword = new RandomGUID().toString();
                    gateway.setDevicePassword(devicePassword);
                    gateway.setGatewayType(request.getGatewayType());
                    gateway = EJBLocator.lookupFlatStudyBean().saveFlatGateway(studyTicket, gateway);
                    return new EscGatewayRegistrationResponse(EscObjectFactory.createEscGateway(gateway, study.getExternalId()), gateway.getDevicePassword());
                } else {
                    return new EscGatewayRegistrationResponse("Request verification failed for new gateway");
                }

            } else {
                // Check with device password
                if(request.verify(gateway.getDevicePassword())){
                    if(request.getTimestamp()>=gateway.getLastRegistrationTimestamp()){
                        gateway.setLastRegistrationTimestamp(request.getTimestamp());
                        if(request.getGatewayType()!=null && ! request.getGatewayType().isEmpty()){
                            gateway.setGatewayType(request.getGatewayType());
                        }
                        gateway = EJBLocator.lookupFlatStudyBean().saveFlatGateway(studyTicket, gateway);
                        EJBLocator.lookupFlatStudyBean().checkFlatGatewayFolders(studyTicket, gateway.getId());
                        return new EscGatewayRegistrationResponse(EscObjectFactory.createEscGateway(gateway, study.getExternalId()));
                    } else {
                        return new EscGatewayRegistrationResponse("Supplied registration time before last registration");
                    }
                    
                } else {
                    return new EscGatewayRegistrationResponse("Request verification failed for existing gateway");
                }
                
            }
            
        } else {
            return new EscGatewayRegistrationResponse(request.getStudyCode() + " is not a FlatStudy");
        }
    }
    
    public EscFolder getCodeFolder(String gatewayId, String studyCode, CatalogInterface.FolderType folderType) throws ConnexienceException {
        Folder f = null;
        FlatGateway gateway;
        FlatStudy study;
        Folder studyFolder;
        
        switch(folderType){
            case STUDY_DATAFLOWS:
                // Dataflows folder
                if(t.getDefaultProjectId()!=null && !t.getDefaultProjectId().isEmpty()){
                    study = EJBLocator.lookupFlatStudyBean().getFlatStudy(t, Integer.valueOf(t.getDefaultProjectId()));
                    studyFolder = EJBLocator.lookupStorageBean().getFolder(t, study.getDataFolderId());
                    f = StorageUtils.getOrCreateFolderPath(t, studyFolder, FlatStudy.DATAFLOWS_FOLDER_NAME);
                } else {
                    f = null;
                }
                break;
                
            case DEFAULT_CODE:
                // Code that every gateway gets
                if(t.getDefaultProjectId()!=null && !t.getDefaultProjectId().isEmpty()){
                    study = EJBLocator.lookupFlatStudyBean().getFlatStudy(t, Integer.valueOf(t.getDefaultProjectId()));
                    studyFolder = EJBLocator.lookupStorageBean().getFolder(t, study.getDataFolderId());
                    f = StorageUtils.getOrCreateFolderPath(t, studyFolder, FlatStudy.CODE_FOLDER_NAME + "/" + FlatStudy.DEFAULT_CODE_FOLDER_NAME);
                } else {
                    f = null;
                }
                break;
                
            case DEVICE_DOWNLOADS:
                // gateway's downloads folder
                if(gatewayId!=null && !gatewayId.isEmpty()){
                    gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalIdAndStudyCode(t, studyCode, gatewayId);
                    f = EJBLocator.lookupStorageBean().getFolder(t, gateway.getDownloadsFolderId());
                } else {
                    f = null;
                }
                break;
                
            case DEVICE_CODE:
                // Gateway's specific code folder
                if(gatewayId!=null && !gatewayId.isEmpty()){
                    gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalIdAndStudyCode(t, studyCode, gatewayId);
                    f = EJBLocator.lookupStorageBean().getFolder(t, gateway.getCodeFolderId());
                } else {
                    f = null;
                }
                break;
                
            case DEVICE_TYPE_CODE:
                // Code for all gateways of this type
                if(gatewayId!=null && !gatewayId.isEmpty()){
                    gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalIdAndStudyCode(t, studyCode, gatewayId);
                    study = EJBLocator.lookupFlatStudyBean().getFlatStudy(t, gateway.getStudyId());
                    studyFolder = EJBLocator.lookupStorageBean().getFolder(t, study.getDataFolderId());
                    f = StorageUtils.getOrCreateFolderPath(t, studyFolder, FlatStudy.CODE_FOLDER_NAME + "/" + gateway.getGatewayType());
                } else {
                    f = null;
                }
                break;
                
            case DEVICE_UPLOADS:
                // Uploads folder for this gateway
                if(gatewayId!=null && ! gatewayId.isEmpty()){
                    gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalIdAndStudyCode(t, studyCode, gatewayId);
                    f = EJBLocator.lookupStorageBean().getFolder(t, gateway.getUploadsFolderId());
                } else {
                    f = null;
                }
                break;
                
            case DEVICE_FOLDER:
                // Uploads folder for this gateway
                if(gatewayId!=null && ! gatewayId.isEmpty()){
                    gateway = EJBLocator.lookupFlatStudyBean().getFlatGatewayByExternalIdAndStudyCode(t, studyCode, gatewayId);
                    f = EJBLocator.lookupStorageBean().getFolder(t, gateway.getFolderId());
                } else {
                    f = null;
                }
                break;
                
            default:
                f = null;
        }
        if(f!=null){
            return EscObjectFactory.createEscFolder(f);
        } else {
            throw new ConnexienceException("Cannot find folder");
        }
        
    }

    public void pushEvents(String gatewayId, String studyCode, EscEvent[] events) throws Exception {
        if(t.getDefaultProjectId()!=null){
            ArrayList<EscEvent> eventList = new ArrayList<>();
            eventList.addAll(Arrays.asList(events));
            EJBLocator.lookupEventStoreBean().pushEvents(t, gatewayId, studyCode, eventList);
        }
    }
       
    
    
    public EscProject getProjectByStudyCode(@PathParam("studyCode")String studyCode) throws Exception {
        List<Project> projects = EJBLocator.lookupProjectsBean().getProjectsByExternalId(t, studyCode);
        if(projects.size()==1){
            return EscObjectFactory.createEscProject(projects.get(0));
        } else {
            throw new Exception("There must be exactly one project with matching study code:" + studyCode);
        }
    }
    
    public int getNumberOfPeopleInStudy(int studyId) throws Exception {
        return (int)EJBLocator.lookupFlatStudyBean().getObjectCount(t, studyId, FlatPerson.class);
    }
    
    public int getNumberOfDevicesInStudy(int studyId) throws Exception {
        return (int)EJBLocator.lookupFlatStudyBean().getObjectCount(t, studyId, FlatDevice.class);
    }
    
    public int getNumberOfGatewaysInStudy(int studyId) throws Exception {
        return (int)EJBLocator.lookupFlatStudyBean().getObjectCount(t, studyId, FlatGateway.class);
    }
    
    public EscPerson[] getPeople(int studyId, int startPos, int count) throws Exception {
        List<FlatPerson> results = EJBLocator.lookupFlatStudyBean().getObjects(t, studyId, FlatPerson.class, startPos, count);
        ArrayList<EscPerson> people = new ArrayList<>();
        for(FlatPerson p : results){
            people.add(EscObjectFactory.createEscPerson(p));
        }
        return people.toArray(new EscPerson[people.size()]);
    }
    
    public EscPerson movePerson(int personId, int newStudyId) throws Exception {
        EJBLocator.lookupFlatStudyBean().moveFlatPerson(t, personId, newStudyId);
        FlatPerson p = EJBLocator.lookupFlatStudyBean().getFlatPerson(t, personId);
        return EscObjectFactory.createEscPerson(p);
    }
    
    public EscDevice[] getDevices(int studyId, int startPos, int count) throws Exception {
        List<FlatDevice> results = EJBLocator.lookupFlatStudyBean().getObjects(t, studyId, FlatDevice.class, startPos, count);
        ArrayList<EscDevice> devices = new ArrayList<>();
        for(FlatDevice d : results){
            devices.add(EscObjectFactory.createEscDevice(d));
        }
        return devices.toArray(new EscDevice[devices.size()]);        
    }
    
    public EscGateway[] getGateways(int studyId, int startPos, int count) throws Exception {
        List<FlatGateway> results = EJBLocator.lookupFlatStudyBean().getObjects(t, studyId, FlatGateway.class, startPos, count);
        ArrayList<EscGateway> gateways = new ArrayList<>();
        FlatStudy s = EJBLocator.lookupFlatStudyBean().getFlatStudy(t, studyId);
        
        for(FlatGateway g : results){
            gateways.add(EscObjectFactory.createEscGateway(g, s.getExternalId()));
        }
        return gateways.toArray(new EscGateway[gateways.size()]);        
    }    
    
    public void removePerson(int personId) throws Exception {
        EJBLocator.lookupFlatStudyBean().removeFlatPerson(t, personId);
    }
    
    public EscPerson getPerson(int personId) throws Exception {
        FlatPerson p = EJBLocator.lookupFlatStudyBean().getFlatPerson(t, personId);
        return EscObjectFactory.createEscPerson(p);
    }
    
    public EscPerson[] searchPeople(String projectId, String searchText) throws Exception {
        List people = EJBLocator.lookupFlatStudyBean().getObjects(t, Integer.parseInt(projectId), FlatPerson.class, searchText, 0, Integer.MAX_VALUE);
        EscPerson[] results = new EscPerson[people.size()];
        
        for(int i=0;i<people.size();i++){
            results[i] = EscObjectFactory.createEscPerson((FlatPerson)people.get(i));
        }
        return results;
    }
    
    public EscDevice getDevice(int deviceId) throws Exception {
       FlatDevice d = EJBLocator.lookupFlatStudyBean().getFlatDevice(t, deviceId);
       return EscObjectFactory.createEscDevice(d);
    }
    
    public EscGateway getGateway(int gatewayId) throws Exception {
        FlatGateway g = EJBLocator.lookupFlatStudyBean().getFlatGateway(t, gatewayId);
        FlatStudy s = EJBLocator.lookupFlatStudyBean().getFlatStudy(t, g.getStudyId());
        return EscObjectFactory.createEscGateway(g, s.getExternalId());
    }
    
    public String[] listEventTypes(String studyCode, String gatewayId) throws Exception {
        List<String> results = EJBLocator.lookupEventStoreBean().listEventTypes(t, gatewayId, studyCode);
        return results.toArray(new String[results.size()]);
    }
    
    public String[] listEventTypes(String studyCode) throws Exception {
        List<String> results = EJBLocator.lookupEventStoreBean().listEventTypes(t, studyCode);
        return results.toArray(new String[results.size()]);
    }
    
    public EscEvent[] queryEvents(String studyCode, String gatewayId, Date startDate, Date endDate, String eventType, int maxResults) throws Exception {   
        List<EscEvent> results = EJBLocator.lookupEventStoreBean().query(t, gatewayId, studyCode, eventType, startDate, endDate, maxResults);
        return results.toArray(new EscEvent[results.size()]);
    }
}