/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.jaxrs.v1;

import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscGatewayRegistrationRequest;
import com.connexience.api.model.EscGatewayRegistrationResponse;
import com.connexience.server.ConnexienceException;
import com.connexience.server.util.AuthHelper;
import com.connexience.server.api.external.helpers.FlatStudyHelper;
import com.connexience.server.model.security.Ticket;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import com.connexience.api.model.CatalogInterface;
import com.connexience.api.model.EscDevice;
import com.connexience.api.model.EscEvent;
import com.connexience.api.model.EscGateway;
import com.connexience.api.model.EscPerson;
import com.connexience.api.model.EscProject;
import java.util.Date;
import javax.ws.rs.DELETE;
import org.jboss.resteasy.annotations.Body;

/**
 * This class provides the back end RestEasy service for the flat study API
 * @author hugo
 */
@Path("/public/rest/v1/catalog")
@Stateless
public class RestCatalogService implements CatalogInterface {
    @Context HttpHeaders headers;    
    
    /** Create a ticket for the current security context */
    private Ticket getTicket() throws ConnexienceException {
        return AuthHelper.getTicket(headers);
    }
        
    @POST
    @Path("/registergateway")
    @Consumes("application/json")
    @Produces("application/json")
    @Override
    public EscGatewayRegistrationResponse registerGateway(EscGatewayRegistrationRequest request) throws Exception {
        return new FlatStudyHelper().registerGateway(request);
    }

    @GET
    @Path("/studies/{studyCode}/folders/{gatewayId}/{folderType}")
    @Produces("application/json")
    @Override
    public EscFolder getFolder(@PathParam("gatewayId")String gatewayId, @PathParam("studyCode") String studyCode, @PathParam("folderType")FolderType folderType) throws Exception {
        return new FlatStudyHelper(getTicket()).getCodeFolder(gatewayId, studyCode, folderType);
    }

    @POST
    @Path("/studies/{studyCode}/batchevents/{gatewayId}")
    @Consumes("application/json")
    @Produces("text/plain")
    @Override
    public String pushEvents(@PathParam("gatewayId")String gatewayId, @PathParam("studyCode")String studyCode, EscEvent[] events) throws Exception {
        try {
            new FlatStudyHelper(getTicket()).pushEvents(gatewayId, studyCode, events);
            return "OK";
        } catch (Exception e){
            return "ERROR: " + e.getMessage();
        }
    }

    @GET
    @Path("/studiesbyexternalcode/{studyCode}")
    @Produces("application/json")
    @Override
    public EscProject getProjectByStudyCode(@PathParam("studyCode")String studyCode) throws Exception {
        return new FlatStudyHelper(getTicket()).getProjectByStudyCode(studyCode);
    }

    @GET
    @Path("/studiesbyid/{id}/people/count")
    @Produces("text/plain")
    @Override
    public int getNumberOfPeopleInStudy(@PathParam("id")int projectId) throws Exception {
        return new FlatStudyHelper(getTicket()).getNumberOfPeopleInStudy(projectId);
    }

    @GET
    @Path("/studiesbyid/{id}/people/list/{start}/{count}")
    @Produces("application/json")
    @Override
    public EscPerson[] getPeople(@PathParam("id")int projectId, @PathParam("start")int startIndex, @PathParam("count")int count) throws Exception {
        return new FlatStudyHelper(getTicket()).getPeople(projectId, startIndex, count);
    }

    @GET
    @Path("/peoplebyid/{id}")
    @Produces("application/json")
    @Override
    public EscPerson getPerson(@PathParam("id")int personId) throws Exception {
        return new FlatStudyHelper(getTicket()).getPerson(personId);
    }

    @POST
    @Path("/studiesbyid/{projectId}/people/search")
    @Produces("application/json")
    @Consumes("text/plain")
    @Override
    public EscPerson[] searchPeople(@PathParam("projectId")String projectId, String queryText) throws Exception {
        return new FlatStudyHelper(getTicket()).searchPeople(projectId, queryText);
    }

    @GET
    @Path("/peoplebyid/{personId}/moveto/{newStudyId}")
    @Produces("application/json")
    @Override
    public EscPerson movePerson(@PathParam("personId")int personId, @PathParam("newStudyId")String newStudyCode) throws Exception {
        return new FlatStudyHelper(getTicket()).movePerson(personId, Integer.parseInt(newStudyCode));
    }
    
    @GET
    @Path("/studiesbyid/{id}/devices/count")
    @Produces("text/plain")
    @Override
    public int getNumberOfDevicesInStudy(@PathParam("id")int projectId) throws Exception {
        return new FlatStudyHelper(getTicket()).getNumberOfDevicesInStudy(projectId);
    }

    @GET
    @Path("/devicesbyid/{id}")
    @Produces("application/json")
    @Override
    public EscDevice getDevice(@PathParam("id")int deviceId) throws Exception {
        return new FlatStudyHelper(getTicket()).getDevice(deviceId);
    }

    @Path("/studiesbyid/{id}/devices/list/{start}/{count}")
    @Produces("application/json")
    @Override
    public EscDevice[] getDevices(@PathParam("id")int projectId, @PathParam("start")int startIndex, @PathParam("count")int count) throws Exception {
        return new FlatStudyHelper(getTicket()).getDevices(projectId, startIndex, count);
    }

    @GET
    @Path("/studiesbyid/{id}/gateways/count")
    @Produces("text/plain")
    @Override    
    public int getNumberOfGatewaysInStudy(@PathParam("id")int projectId) throws Exception {
        return new FlatStudyHelper(getTicket()).getNumberOfGatewaysInStudy(projectId);
    }

    @GET
    @Path("/gatewaysbyid/{id}")
    @Produces("application/json")
    @Override
    public EscGateway getGateway(@PathParam("id")int gatewayId) throws Exception {
        return new FlatStudyHelper(getTicket()).getGateway(gatewayId);
    }
    
    @DELETE
    @Path("/peoplebyid/{id}")
    @Override
    public void removePerson(@PathParam("id")int personId) throws Exception {
        new FlatStudyHelper(getTicket()).removePerson(personId);
    }

    @GET
    @Path("/studiesbyid/{id}/gateways/list/{start}/{count}")
    @Produces("application/json")
    @Override
    public EscGateway[] getGateways(@PathParam("id")int projectId, @PathParam("start")int startIndex, @PathParam("count")int count) throws Exception {
        return new FlatStudyHelper(getTicket()).getGateways(projectId, startIndex, count);
    }

    @GET
    @Path("/studiesbyid/{id}/gateways/{gatewayId}/eventtypes")
    @Produces("application/json")
    @Override
    public String[] listEventTypes(@PathParam("id")String studyCode, @PathParam("gatewayId")String gatewayId) throws Exception {
        return new FlatStudyHelper(getTicket()).listEventTypes(studyCode, gatewayId);
    }    

    @GET
    @Path("/studiesbyid/{id}/eventtypes")
    @Produces("application/json")
    @Override
    public String[] listEventTypes(@PathParam("id")String studyCode) throws Exception {
        return new FlatStudyHelper(getTicket()).listEventTypes(studyCode);
    }

    @GET
    @Path("/studiesbyid/{id}/gateways/{gatewayId}/events/{eventType}/{startDate}/{endDate}/{maxResults}")
    @Produces("application/json")
    @Override
    public EscEvent[] queryEvents(@PathParam("id")String studyCode, @PathParam("gatewayId")String gatewayId, 
                                  @PathParam("startDate")long startDate, @PathParam("endDate")long endDate, 
                                  @PathParam("eventType")String eventType, @PathParam("maxResults")int maxResults) throws Exception {
        return new FlatStudyHelper(getTicket()).queryEvents(studyCode, gatewayId, new Date(startDate), new Date(endDate), eventType, maxResults);
    }
}