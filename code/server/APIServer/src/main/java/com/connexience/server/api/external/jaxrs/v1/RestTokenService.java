/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.api.external.jaxrs.v1;

import com.connexience.api.model.EscJWT;
import com.connexience.api.model.TokenInterface;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.JWTRecord;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST service to create, validate and release tokens.
 * @author hugo
 */
@Stateless
@Path("/public/rest/v1/tokens")
public class RestTokenService implements TokenInterface {
    @Path("/test")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String test(){
        return "Hello";
    }
    
    @Path("/issue")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Override
    public EscJWT issueToken(@FormParam("username")String username, @FormParam("password")String password, @FormParam("label")String label) throws Exception {
        JWTRecord record = EJBLocator.lookupTicketBean().createToken(username, password, label);
        EscJWT jwt = new EscJWT();
        jwt.setExpiryTimestamp(record.getExpiry());
        jwt.setId(record.getId());
        jwt.setToken(record.getJwt());
        return jwt;
    }

    @Path("/release/{id}")
    @DELETE
    @Override
    public void releaseToken(@PathParam("id")String id) throws Exception {
        EJBLocator.lookupTicketBean().removeToken(id);
    }

    @Path("/validate")
    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    @Override
    public String validateToken(String jwtData) throws Exception {
        if(EJBLocator.lookupTicketBean().validateToken(jwtData)){
            return TOKEN_VALID;
        } else {
            return TOKEN_INVALID;
        }
    }
}
