package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import com.connexience.api.model.json.JSONObject;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class provides a postgreSQL event store that can operate outside an
 * AppServer to store events.
 * @author hugo
 */
public class PostgreSQLEventStore extends SQLEventStore {

    public PostgreSQLEventStore() {
        super("org.postgresql.Driver", "postgresql");
    }

    @Override
    public boolean validate() {
        return true;
    }
    
    @Override
    public void pushEvent(String deviceId, EscEvent event) throws Exception {
        try (Connection c = getConnection()){
            try (PreparedStatement s = c.prepareStatement("INSERT INTO " + table + " (timestamp,deviceid,eventtype,jsondata) VALUES(?,?,?,?")){
                s.setTimestamp(1, new java.sql.Timestamp(event.getTimestamp()));
                s.setString(2, deviceId);
                s.setString(3, event.getEventType());
                s.setString(4, event.toJsonObject().toString());
                s.executeUpdate();
            }
        }
    }

    @Override
    public void pushEvents(String deviceId, List<EscEvent> events) throws Exception {
        try (Connection c = getConnection()){
            try (PreparedStatement s = c.prepareStatement("INSERT INTO " + table + " (timestamp,deviceid,eventtype,jsondata) VALUES(?,?,?,?)")){
                for(EscEvent event : events){
                    s.setTimestamp(1, new java.sql.Timestamp(event.getTimestamp()));
                    s.setString(2, deviceId);
                    s.setString(3, event.getEventType());
                    s.setString(4, event.toJsonObject().toString());
                    s.executeUpdate();
                }
            }
        } catch (Exception e){
            throw e;
        }
    }

    @Override
    public EscEventCursor getCursor(String deviceId, Date startDate, Date endDate) throws Exception {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        
        try {
            c = getConnection();
            s = c.prepareStatement("SELECT jsonData FROM " + table + " WHERE deviceId=? AND timestamp>=? AND timestamp<=?");
            s.setString(1, deviceId);
            s.setTimestamp(2, new Timestamp(startDate.getTime()));
            s.setTimestamp(3, new Timestamp(endDate.getTime()));
            r = s.executeQuery();
            SQLEventCursor cursor = new SQLEventCursor(r);
            return cursor;
            
        } catch (Exception ex){
            throw ex;
        } finally {
            try {r.close();} catch (Exception e){}
            try {s.close();} catch (Exception e){}
            try {c.close();} catch (Exception e){}            
        }
    }

    @Override
    public EscEventCursor getCursor(String deviceId, String eventType, Date startDate, Date endDate) throws Exception {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        
        try {
            c = getConnection();
            s = c.prepareStatement("SELECT jsonData FROM " + table + " WHERE deviceId=? AND eventType=? AND timestamp>=? AND timestamp<=?");
            s.setString(1, deviceId);
            s.setString(2, eventType);
            s.setTimestamp(3, new Timestamp(startDate.getTime()));
            s.setTimestamp(4, new Timestamp(endDate.getTime()));
            
            r = s.executeQuery();
            return new SQLEventCursor(r);
        } catch (Exception ex){
            throw ex;
        } finally {
            try {r.close();} catch (Exception e){}
            try {s.close();} catch (Exception e){}
            try {c.close();} catch (Exception e){}
        }
    }

    @Override
    public EscEvent getMostRecent(String deviceId) throws Exception {
        try(Connection c = getConnection()){
            try(PreparedStatement s = c.prepareStatement("SELECT jsonData FROM " + table + " WHERE deviceId=? ORDER BY timestamp DESC")){
                s.setMaxRows(1);
                s.setString(1, deviceId);
                try (ResultSet r = s.executeQuery()){
                    if(r.next()){
                        JSONObject json = new JSONObject(r.getString(1));
                        return new EscEvent(json);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e){
            throw new Exception("Error getting most recent data: " + e.getMessage(), e);
        }
    }

    @Override
    public EscEvent getMostRecent(String deviceId, String eventType) throws Exception {
        try(Connection c = getConnection()){
            try(PreparedStatement s = c.prepareStatement("SELECT jsonData FROM " + table + " WHERE deviceId=? AND eventType=? ORDER BY timestamp DESC")){
                s.setMaxRows(1);
                s.setString(1, deviceId);
                s.setString(2, eventType);
                try (ResultSet r = s.executeQuery()){
                    if(r.next()){
                        JSONObject json = new JSONObject(r.getString(1));
                        return new EscEvent(json);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e){
            throw new Exception("Error getting most recent data: " + e.getMessage(), e);
        }
    }

    @Override
    public void dropEvents(String deviceId) throws Exception {
        try(Connection c = getConnection()){
            try(PreparedStatement s = c.prepareStatement("DELETE FROM " + table + " WHERE deviceId=?")){
                s.setString(1, deviceId);
                s.executeUpdate();
            }
        } catch (Exception e){
            throw new Exception("Error getting most recent data: " + e.getMessage(), e);
        }
    }

    @Override
    public void dropEvents(String deviceId, String eventType) throws Exception {
        try(Connection c = getConnection()){
            try(PreparedStatement s = c.prepareStatement("DELETE FROM " + table + " WHERE deviceId=? AND eventType=?")){
                s.setString(1, deviceId);
                s.setString(2, eventType);
                s.executeUpdate();
            }
        } catch (Exception e){
            throw new Exception("Error getting most recent data: " + e.getMessage(), e);
        }
    }
    
    @Override
    public List<String> listEventTypes(String deviceId) throws Exception {
        try(Connection c = getConnection()){
            try (PreparedStatement s = c.prepareStatement("SELECT DISTINCT eventType FROM " + table + " WHERE deviceId=?")){
                s.setString(1, deviceId);
                ArrayList<String> results = new ArrayList<>();
                try (ResultSet r = s.executeQuery()){
                    while(r.next()){
                        results.add(r.getString(1));
                    }
                }
                return results;
            }
        } catch (Exception e){
            throw new Exception("Error listing event types: " + e.getMessage(), e);
        }
    }

    @Override
    public List<String> listEventTypes() throws Exception {
        try(Connection c = getConnection()){
            try (PreparedStatement s = c.prepareStatement("SELECT DISTINCT eventType FROM " + table)){
                ArrayList<String> results = new ArrayList<>();
                try (ResultSet r = s.executeQuery()){
                    while(r.next()){
                        results.add(r.getString(1));
                    }
                }
                return results;
            }
        } catch (Exception e){
            throw new Exception("Error listing event types: " + e.getMessage(), e);
        }
    }
    
    
    @Override
    public void close() {
        super.close();
    }

    @Override
    public void init() {
        super.init(); 
        try {
            createEventsTable(table);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void createEventsTable(String tableName) throws Exception {
        try(Connection c = getConnection()){
            DatabaseMetaData metaData = c.getMetaData();
            boolean found = false;
            try(ResultSet r = metaData.getTables(null, null, null, null)){
                while(r.next()){
                    if(r.getString("TABLE_NAME").equalsIgnoreCase(tableName)){
                        found = true;
                    }
                }
            }
            
            if(!found){
                try(PreparedStatement s = c.prepareStatement("CREATE TABLE " + tableName + "(id BIGSERIAL PRIMARY KEY, timestamp TIMESTAMP, deviceId VARCHAR(255), eventType VARCHAR(255), jsonData TEXT)")){
                    s.executeUpdate();
                }
            }

        } catch (Exception e){
            throw new Exception("Error creating events table: " + e.getMessage(), e);
        }
    }
}