package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import com.connexience.api.model.json.JSONObject;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This base class can store events in an SQL database
 * @author hugo
 */
public abstract class SQLEventStore extends AbstractEventStore {
    protected ComboPooledDataSource dataSource;
    protected String driverClass;
    protected String jdbcUrlDriver;
    
    public SQLEventStore(String driverClass, String jdbcUrlDriver) {
        this.driverClass = driverClass;
        this.jdbcUrlDriver = jdbcUrlDriver;
    }

    @Override
    public void init() {
        super.init(); 
        dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass(driverClass);
        } catch (Exception e){
            e.printStackTrace();
        }
        dataSource.setJdbcUrl("jdbc:" + jdbcUrlDriver + "://" + hostName + ":" + port + "/" + database);
        if(username!=null && !username.isEmpty()){
            dataSource.setUser(username);
            dataSource.setPassword(password);
        }
        dataSource.setMinPoolSize(5);
        dataSource.setMaxPoolSize(10);
        dataSource.setAcquireIncrement(2);
        
        // Now create the table if it doesn't exist
        setupTable();
    }

    private void setupTable(){
        
    }
    
    @Override
    public void close() {
        if(dataSource!=null){
            dataSource.close();
        }
    }
    
    protected Connection getConnection() throws Exception {
        if(dataSource!=null){
            return dataSource.getConnection();
        } else {
            throw new Exception("Datasource not initialized");
        }
    }
    
    public class SQLEventCursor implements EscEventCursor {
        volatile boolean finished = false;
        volatile boolean closed = false;
        List<EscEvent> events;
        Iterator<EscEvent> iterator;

        public SQLEventCursor(ResultSet results) {
            populateEvents(results);
        }
        
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        private void populateEvents(ResultSet r){
            events = new ArrayList<>();
            try {
                while(r.next()){
                    JSONObject json = new JSONObject(r.getString(1));
                    events.add(new EscEvent(json));
                }
                iterator = events.iterator();
            } catch (Exception e){

            }            
        }
        
        @Override
        public EscEvent next() {
            return iterator.next();
        }
        
        @Override
        public void close(){

        }
    }    
}