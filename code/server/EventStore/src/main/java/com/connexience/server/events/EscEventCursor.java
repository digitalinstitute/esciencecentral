package com.connexience.server.events;

import com.connexience.api.model.EscEvent;

/**
 * This interface defines a simple cursor that can step through a collection of
 * EscEvents.
 * @author hugo
 */
public interface EscEventCursor {
    public boolean hasNext();
    public EscEvent next();
    public void close();
}