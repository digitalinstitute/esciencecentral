
package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Store that just drops data. This is created by default and allows data to still
 * be sent via ActiveMQ somewhere else.
 * @author hugo
 */
public class NullStore extends AbstractEventStore {

    @Override
    public boolean validate() {
        return true;
    }

    @Override
    public void pushEvent(String deviceId, EscEvent event) throws Exception {
    }

    @Override
    public void pushEvents(String deviceId, List<EscEvent> events) throws Exception {
    }

    @Override
    public EscEventCursor getCursor(String deviceId, Date startDate, Date endDate) throws Exception {
        return new NullStoreEventCursor();
    }

    @Override
    public EscEventCursor getCursor(String deviceId, String eventType, Date startDate, Date endDate) throws Exception {
        return new NullStoreEventCursor();
    }

    @Override
    public EscEvent getMostRecent(String deviceId) throws Exception {
        return null;
    }

    @Override
    public EscEvent getMostRecent(String deviceId, String eventType) throws Exception {
        return null;
    }

    @Override
    public List<String> listEventTypes(String deviceId) throws Exception {
        return new ArrayList<>();
    }

    @Override
    public List<String> listEventTypes() throws Exception {
        return new ArrayList<>();
    }
    
    

    @Override
    public void dropEvents(String deviceId) throws Exception {
    }

    @Override
    public void dropEvents(String deviceId, String eventType) throws Exception {
    }
    
    public class NullStoreEventCursor implements EscEventCursor {

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public EscEvent next() {
            return null;
        }

        @Override
        public void close() {
        }
        
    }
}
