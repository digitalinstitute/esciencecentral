/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.bson.Document;

/**
 * This class represents an event store for a flat study
 * @author hugo
 */
public class MongoEventStore extends AbstractEventStore {
    
    /** Mongo client for this study's database */
    private MongoClient client;
    
    public MongoEventStore() {
    }

    private MongoClient getOrCreateClient() throws Exception {
        if(client!=null){
            return client;
        } else {
            synchronized(this){
                if(username!=null && !username.isEmpty()){
                    List<MongoCredential> credentials = new ArrayList<>();
                    credentials.add(MongoCredential.createScramSha1Credential(username, database, password.toCharArray()));
                    ServerAddress addr = new ServerAddress(hostName, port);
                    client = new MongoClient(addr, credentials);                 
                } else {
                    client = new MongoClient(hostName, port);
                }
                
            }
            return client;
        }
    }
    
    public MongoDatabase getMongoDatabase() throws Exception {
        return getOrCreateClient().getDatabase(database);
    }
         
    @Override
    public void pushEvent(String deviceId, EscEvent event) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        pushEvent(deviceId, createEventDocument(event), collection);
    }
    
    public void pushEvent(String collectionName, String deviceId, EscEvent event) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(collectionName);
        pushEvent(deviceId, createEventDocument(event), collection);
    }

    @Override
    public void pushEvents(String deviceId, List<EscEvent> events) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        pushEvents(deviceId, events, collection);
    }

    public void pushEvents(String deviceId, List<EscEvent> events, MongoCollection collection) throws Exception {
        List<Document> eventDocuments = new ArrayList<>();
        Document eventDoc;
        for(EscEvent e : events){
            eventDoc = createEventDocument(e);
            eventDoc.append("_deviceId", deviceId);
            eventDocuments.add(eventDoc);
        }
        collection.insertMany(eventDocuments);
    }

    @Override
    public List<String> listEventTypes(String deviceId) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Iterator<String> types = collection.distinct("_eventtype", String.class).iterator();
        ArrayList<String> results = new ArrayList<>();
        while(types.hasNext()){
            results.add(types.next());
        }
        return results;
    }

    @Override
    public List<String> listEventTypes() throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Iterator<String> types = collection.distinct("_eventtype", String.class).iterator();
        ArrayList<String> results = new ArrayList<>();
        while(types.hasNext()){
            results.add(types.next());
        }
        return results;
    }
    
    
    
    private void pushEvent(String deviceId, Document eventDoc, MongoCollection collection) throws Exception {
        eventDoc.append("_deviceId", deviceId);
        collection.insertOne(eventDoc);
    }
    
    public void close(){
        super.close();
        if(client!=null){
            client.close();
        }
    }

    @Override
    public EscEventCursor getCursor(String deviceId, Date startDate, Date endDate) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        query.put("_timestamp", new Document("$gt", startDate.getTime()));
        Document dateDocument = new Document();
        dateDocument.put("$gte", startDate.getTime());
        dateDocument.put("$lte", endDate.getTime());
        query.put("_timestamp", dateDocument);  
        return new MongoEventCursor(collection.find(query));
    }

    @Override
    public EscEventCursor getCursor(String deviceId, String eventType, Date startDate, Date endDate) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        query.put("_eventtype", new Document("$eq", eventType));
        Document dateDocument = new Document();
        dateDocument.put("$gte", startDate.getTime());
        dateDocument.put("$lte", endDate.getTime());
        query.put("_timestamp", dateDocument);        
        return new MongoEventCursor(collection.find(query));
    }

    
    @Override
    public EscEvent getMostRecent(String deviceId) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        Document sort = new Document("_timestamp", -1);
        FindIterable<Document> results = collection.find(query).sort(sort).limit(1);
        Iterator<Document> docs = results.iterator();
        if(docs.hasNext()){
            return createEvent(docs.next());
        } else {
            return null;
        }
    }

    @Override
    public EscEvent getMostRecent(String deviceId, String eventType) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        query.put("_eventtype", new Document("$eq", eventType));
        Document sort = new Document("_timestamp", -1);
        FindIterable<Document> results = collection.find(query).sort(sort).limit(1);
        Iterator<Document> docs = results.iterator();
        if(docs.hasNext()){
            return createEvent(docs.next());
        } else {
            return null;
        }
    }

    @Override
    public void dropEvents(String deviceId) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        collection.deleteMany(query);
    }

    @Override
    public void dropEvents(String deviceId, String eventType) throws Exception {
        MongoDatabase db = getMongoDatabase();
        MongoCollection collection = db.getCollection(table);
        Document query = new Document();
        query.put("_deviceId", new Document("$eq", deviceId));
        query.put("_eventtype", new Document("$eq", eventType));
        collection.deleteMany(query);
    }
    
    public Document createEventDocument(EscEvent event){
        Document doc = new Document();
        doc.append("_timestamp", event.getTimestamp());
        doc.append("_primaryKey", event.getPrimaryKey());
        doc.append("_eventtype", event.getEventType());
        
        HashMap<String, Object> data = event.getData();
        for(String key : data.keySet()){
            doc.append(key, data.get(key));
        }
        return doc;
    }    
    
    public EscEvent createEvent(Document doc){
        EscEvent event = new EscEvent();

        Set<String> keys = doc.keySet();
        event.put("Time", doc.get("_formattedtime"));
        event.put("Timestamp", doc.get("_timestamp"));
        event.setTimestamp(doc.getLong("_timestamp"));
        
        if(doc.containsKey("_primaryKey")){
            event.setPrimaryKey(doc.getString("_primaryKey"));
        }
        
        if(doc.containsKey("_eventtype")){
            event.setEventType(doc.getString("_eventType"));
        }
        
        for(String key : keys){
            if(!key.startsWith("_")){
                // Standard value, always add
                event.put(key, doc.get(key));
            }
        }
        return event;        
    }

    @Override
    public boolean validate() {
        return true;
    }
    
    public class MongoEventCursor implements EscEventCursor {
        private FindIterable<Document> results;
        private Iterator<Document> iterator;
        
        public MongoEventCursor(FindIterable<Document> results) {
            this.results = results;
            iterator = results.iterator();
        }
        
        
        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public EscEvent next() {
            if(iterator.hasNext()){
                return createEvent(iterator.next());
            } else {
                return null;
            }
        }    

        @Override
        public void close() {
            
        }
    }
}