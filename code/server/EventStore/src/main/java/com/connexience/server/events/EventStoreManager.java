/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class manages connections to various database servers that are declared
 * by FlatStudies in e-SC
 * @author hugo
 */
public class EventStoreManager {
    private ConcurrentHashMap<String, AbstractEventStore> dataStores = new ConcurrentHashMap<>();
    
    public synchronized boolean addFlatStudyStore(String eventStoreType, String studyCode, String host, int port, String username, String password, String databaseName, String tableName, String exportJMSTopicName) {
        if(!dataStores.containsKey(studyCode)){
            AbstractEventStore store = null;
            switch(eventStoreType){
                case "mongo":
                    store = new MongoEventStore();
                    break;
                    
                case "postgres":
                    store = new PostgreSQLEventStore();
                    break;
                    
                case "null":
                case "default":
                default:
                    store = new NullStore();
                    break;
            }

            if(store!=null){
                store.setDatabase(databaseName);
                store.setHostName(host);
                store.setPassword(password);
                store.setPort(port);
                store.setTable(tableName);
                store.setUsername(username);       
                store.setExportJMSTopicName(exportJMSTopicName);
                try {
                    store.init();
                } catch (Exception e){
                    e.printStackTrace();
                }
                if(store.validate()){
                    dataStores.put(studyCode, store);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    
    public synchronized void resetAll(){
        for(AbstractEventStore store : dataStores.values()){
            store.close();
        }
        dataStores.clear();
    }
    
    public synchronized void reset(String studyCode){
        if(dataStores.containsKey(studyCode)){
            dataStores.get(studyCode).close();
            dataStores.remove(studyCode);
        }
    }
    
    public boolean containsFlatStudyStore(String studyCode){
        return dataStores.containsKey(studyCode);
    }
    
    public AbstractEventStore getFlatStudyStore(String studyCode){
        return dataStores.get(studyCode);
    }
    
    /** Add an event to the collection */
    public void pushEvent(String studyCode, String deviceId, EscEvent event) throws Exception {
        if(dataStores.containsKey(studyCode)){
            dataStores.get(studyCode).pushEvent(deviceId, event);
        } else {
            throw new Exception("No storage available for study: " + studyCode);
        }
    }
    
    public void pushEvents(String studyCode, String deviceId, List<EscEvent> events) throws Exception {
        if(dataStores.containsKey(studyCode)){
            dataStores.get(studyCode).pushEvents(deviceId, events);
        } else {
            throw new Exception("No storage available for study: " + studyCode);
        }
    }
    
    /** Close evertything */
    public void close(){
        for(AbstractEventStore store : dataStores.values()){
            store.close();
        }
        dataStores.clear();
    }
    
    /** Drop events for a device */
    public void dropEvents(String studyCode, String deviceId) throws Exception {
        if(dataStores.containsKey(studyCode)){
            dataStores.get(studyCode).dropEvents(deviceId);
        } else {
            throw new Exception("No storage available for study: " + studyCode);
        }
    }
    
    /** Drop specific events for a device */
    public void dropEvents(String studyCode, String deviceId, String eventType) throws Exception {
        if(dataStores.containsKey(studyCode)){
            dataStores.get(studyCode).dropEvents(deviceId, eventType);
        } else {
            throw new Exception("No storage available for study: " + studyCode);
        }
    }
}