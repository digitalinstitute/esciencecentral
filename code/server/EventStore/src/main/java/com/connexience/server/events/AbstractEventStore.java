/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.events;

import com.connexience.api.model.EscEvent;
import java.util.Date;
import java.util.List;
import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 * This is the base class for an event store
 * @author hugo
 */
public abstract class AbstractEventStore implements ExceptionListener {
    protected String hostName;
    protected String database;
    protected String table;
    protected int port;
    protected String username;
    protected String password;
    protected String exportJMSTopicName;
    protected Connection jmsConnection;
    protected MessageProducer jmsProducer;
    protected Session jmsSession;


    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public MessageProducer getJmsProducer() {
        return jmsProducer;
    }

    public Session getJmsSession() {
        return jmsSession;
    }

    public void setJmsConnection(Connection jmsConnection, MessageProducer jmsProducer, Session jmsSession){
        // Reset the old producer
        if(this.jmsProducer!=null){
            try {
                this.jmsProducer.close();
            } catch (Exception e){
                e.printStackTrace();
            }
            this.jmsProducer = null;
        }
        
        // Reset the old session
        if(this.jmsSession!=null){
            try {
                this.jmsSession.close();
            } catch (Exception e){
                e.printStackTrace();
            }
            this.jmsSession = null;
        }
        
        // Reset the old connection
        if(this.jmsConnection!=null){
            try {
                this.jmsConnection.setExceptionListener(null);
                this.jmsConnection.close();
            } catch (Exception e){
                e.printStackTrace();
            }
            this.jmsConnection = null;
        }
        
        if(jmsConnection!=null){
            try {
                jmsConnection.setExceptionListener(this);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        this.jmsConnection = jmsConnection;
        this.jmsProducer = jmsProducer;
        this.jmsSession = jmsSession;
    }

    @Override
    public void onException(JMSException jmse) {
        // Disconnect if there is an exception
        setJmsConnection(null, null, null);
    }
    
    
    public void setExportJMSTopicName(String exportJMSTopicName) {
        this.exportJMSTopicName = exportJMSTopicName;
    }

    public String getExportJMSTopicName() {
        return exportJMSTopicName;
    }
    
    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    /** Do any event-source specific setup */
    public void init(){
        
    }
    
    public void close(){
        setJmsConnection(null, null, null);
    }
    
    public abstract boolean validate();
    public abstract void pushEvent(String deviceId, EscEvent event) throws Exception;
    public abstract void pushEvents(String deviceId, List<EscEvent> events) throws Exception;
    public abstract EscEventCursor getCursor(String deviceId, Date startDate, Date endDate) throws Exception;
    public abstract EscEventCursor getCursor(String deviceId, String eventType, Date startDate, Date endDate) throws Exception;
    public abstract EscEvent getMostRecent(String deviceId) throws Exception;
    public abstract EscEvent getMostRecent(String deviceId, String eventType) throws Exception;
    public abstract List<String> listEventTypes(String deviceId) throws Exception;
    public abstract List<String> listEventTypes() throws Exception;
    
    public abstract void dropEvents(String deviceId) throws Exception;
    public abstract void dropEvents(String deviceId, String eventType) throws Exception;
}