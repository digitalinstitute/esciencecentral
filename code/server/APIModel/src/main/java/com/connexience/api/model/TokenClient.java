/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.net.GenericClient;
import java.util.HashMap;

/**
 * Client to manage JWTs
 * @author hugo
 */
public class TokenClient extends GenericClient implements TokenInterface {

    public TokenClient(String hostname, int port, boolean secure) {
        super(hostname, port, secure, "/api/public/rest/v1/tokens");
    }

    @Override
    public EscJWT issueToken(String username, String password, String label) throws Exception {
        HashMap<String, Object> form = new HashMap<>();
        form.put("username", username);
        form.put("password", password);
        form.put("label", label);
        JSONObject json = postFormRetrieveJson("/issue", form);
        return new EscJWT(json);
    }

    @Override
    public void releaseToken(String id) throws Exception {
        deleteResource("/release/" + id);
    }

    @Override
    public String validateToken(String jwtData) throws Exception {
        return postTextRetrieveText("/validate", jwtData);
    }
}