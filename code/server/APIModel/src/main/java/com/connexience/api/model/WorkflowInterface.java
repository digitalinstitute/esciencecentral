/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import java.util.HashMap;

/**
 * This interface describes the functionality of the externally accessible
 * workflow management API.
 * @author hugo
 */
public interface WorkflowInterface {
    /**
     * List workflows
     */
    EscWorkflow[] listWorkflows() throws Exception;

    /**
     * List workflows shared with the current user.
     */
    EscWorkflow[] listSharedWorkflows() throws Exception;

    /**
     * List all of the shared services the user has access to
     */
    EscWorkflowService[] listSharedServices() throws Exception;
    
    /**
     * List workflows related to the project with id projectId.
     */
    EscWorkflow[] listProjectWorkflows(String projectId) throws Exception;

    /**
     * List all workflows the current user has access to including user's workflows, shared workflows and all workflows
     * of projects in which the user is a participant.
     */
    EscWorkflow[] listAllWorkflows() throws Exception;

    /**
     * List all of the workflow invocations related to a document
     */
    EscWorkflowInvocation[] listInvocationsRelatedToDocument(String documentId) throws Exception;

    /**
     * List all user workflows that can be called as webservices
     */
    EscWorkflow[] listCallableWorkflows() throws Exception;

    /**
     * List all workflows (including shared) that can be called as webservices
     */
    EscWorkflow[] listAllCallableWorkflows() throws Exception;

    /**
     * List of parameters required by a callable workflow
     */
    HashMap<String, String> listCallableWorkflowParameters(String workflowId) throws Exception;

    /**
     * List of parameters required by a callable workflow in the specific version
     */
    HashMap<String, String> listCallableWorkflowParameters(String workflowId, String versionId) throws Exception;

    /**
     * List of parameters required by a callable workflow and get more details about the parameters.
     */
    HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId) throws Exception;

    /**
     * List of parameters required by a callable workflow in the specific version and get more details about the parameters.
     */
    HashMap<String, EscWorkflowParameterDesc> listCallableWorkflowParametersEx(String workflowId, String versionId) throws Exception;

    /**
     * Get a workflow by ID
     */
    EscWorkflow getWorkflow(String workflowId) throws Exception;
    
    /**
     * Save a workflow document description
     */
    EscWorkflow saveWorkflow(EscWorkflow workflow) throws Exception;
    
    /**
     * Delete a workflow
     */
    void deleteWorkflow(String workflowId) throws Exception;
    
    /**
     * Execute a workflow
     */
    EscWorkflowInvocation executeWorkflow(String workflowId) throws Exception;
    
    /**
     * Execute a version of a workflow
     */
    EscWorkflowInvocation executeWorkflow(String workflowId, String versionId) throws Exception;
    
    /** 
     * Execute a workflow on a document
     */
    EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String documentId) throws Exception;
    
    /** 
     * Execute a version of a workflow on a document
     */
    EscWorkflowInvocation executeWorkflowOnDocument(String workflowId, String versionId, String documentId) throws Exception;
    
    /**
     * Execute a workflow with a list of parameters
     */
    EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, EscWorkflowParameterList parameters) throws Exception;
    
    /** Execute a version of a workflow with a list of parameters */
    EscWorkflowInvocation executeWorkflowWithParameters(String workflowId, String versionId, EscWorkflowParameterList parameters) throws Exception;
    
    /**
     * Execute a callable workflow with a HashMap of paraeters
     */
    EscWorkflowInvocation executeCallableWorkflow(String workflowId, String parametersJson) throws Exception;

    /**
     * Execute a callable workflow with a HashMap of paraeters
     */
    EscWorkflowInvocation executeCallableWorkflow(String workflowId, String versionId, String parametersJson) throws Exception;

    /**
     * List the invocations of a workflow
     */
    EscWorkflowInvocation[] listInvocationsOfWorkflow(String workflowId) throws Exception;
    
    /**
     * Get an invocation by ID
     */        
    EscWorkflowInvocation getInvocation(String invocationId) throws Exception;
    
    /**
     * Terminate an invocation
     */
    EscWorkflowInvocation terminateInvocation(String invocationId) throws Exception;
    
    /** 
     * Get a workflow service
     */
    EscWorkflowService getService(String serviceId) throws Exception;
    
    /**
     * Save a workflow service
     */
    EscWorkflowService saveService(EscWorkflowService service) throws Exception;
    
    /** 
     * Get a workflow library
     */
    EscWorkflowLibrary getLibraryByName(String name) throws Exception;
    
    /**
     * Save a workflow library
     */
    EscWorkflowLibrary saveLibrary(EscWorkflowLibrary library) throws Exception;
    
    /** Enable external data processing for a workflow */
    public void enableWorkflowExternalDataSupport(String workflowId, String blockName) throws Exception;
    
    /** Disable external data processing for a workflow */
    public void disableWorkflowExternalDataSupport(String workflowId) throws Exception;    
    
}