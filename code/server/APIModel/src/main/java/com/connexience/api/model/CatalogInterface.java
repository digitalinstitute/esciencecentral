/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import java.util.Date;

/**
 * This interface defines methods that allow API clients to access FlatStudy
 * IoT type projects.
 * @author hugo
 */
public interface CatalogInterface {
    /** Server can store folders that contain gateway data. These can be one of
     * three types referring to code for a specific device, code for a class of
     * device or code that all devices should get*/
    public enum FolderType {
        DEVICE_FOLDER, DEVICE_CODE, DEVICE_TYPE_CODE, DEFAULT_CODE, DEVICE_UPLOADS, DEVICE_DOWNLOADS, STUDY_DATAFLOWS
    }

    /** Register a gateway in a study */
    public EscGatewayRegistrationResponse registerGateway(EscGatewayRegistrationRequest request) throws Exception;
    
    /** Get a code folder for a device */
    public EscFolder getFolder(String gatewayId, String studyCode, FolderType folderType) throws Exception;

    /** Upload a set of events */
    public String pushEvents(String deviceId, String studyCode, EscEvent[] events) throws Exception;
    
    /** Get a project by external ID */
    public EscProject getProjectByStudyCode(String studyCode) throws Exception;
    
    /** Get the number of people in a study */
    public int getNumberOfPeopleInStudy(int projectId) throws Exception;
    
    /** Remove a person from a study */
    public void removePerson(int personId) throws Exception;
    
    /** Get a person from a study */
    public EscPerson getPerson(int personId) throws Exception;
    
    /** Move a person into a different study */
    public EscPerson movePerson(int personId, String newStudyCode) throws Exception;
    
    /** Get a set of people from a study */
    public EscPerson[] getPeople(int projectId, int startIndex, int count) throws Exception;

    /** Search for a person based on their external ID */
    public EscPerson[] searchPeople(String projectId, String queryText) throws Exception;
    
    /** Get the number of devices in a study */
    public int getNumberOfDevicesInStudy(int projectId) throws Exception;
    
    /** Get a device from a study */
    public EscDevice getDevice(int deviceId) throws Exception;
    
    /** Get a set of devices from a study */
    public EscDevice[] getDevices(int projectId, int startIndex, int count) throws Exception;
    
    /** Get the number of gateways in a study */
    public int getNumberOfGatewaysInStudy(int projectId) throws Exception;
    
    /** Get a gateway from a study */
    public EscGateway getGateway(int gatewayId) throws Exception;
    
    /** Get a set of gateways from a study */
    public EscGateway[] getGateways(int projectId, int startIndex, int count) throws Exception;
    
    /** List the event types available for a gateway */
    public String[] listEventTypes(String studyCode, String gatewayId) throws Exception;
    
    /** List all event types available for a study */
    public String[] listEventTypes(String studyCode) throws Exception;
    
    /** Query a events for a gateway between two timestamps */
    public EscEvent[] queryEvents(String studyCode, String gatewayId, long startDate, long endDate, String eventType, int maxResults) throws Exception;
    
}