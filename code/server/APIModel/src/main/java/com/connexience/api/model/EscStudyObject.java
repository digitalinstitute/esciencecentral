/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Base object for a study related item
 * @author hugo
 */
public abstract class EscStudyObject implements Serializable, JsonSerializable {
    protected int id;
    protected int studyId;
    protected String folderId;
    protected String externalId;
    protected String name;
    protected HashMap<String, String> additionalProperties = new HashMap<>();

    public EscStudyObject() {
    }

    public EscStudyObject(JSONObject json) {
        parseJsonObject(json);
    }

    public HashMap<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(HashMap<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudyId() {
        return studyId;
    }

    public void setStudyId(int studyId) {
        this.studyId = studyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    @Override
    public JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("studyId", studyId);
        json.put("folderId", folderId);
        json.put("externalId", externalId);
        json.put("name", name);
        JSONObject additionalPropertiesJson = new JSONObject();

        Iterator<String> keys = additionalProperties.keySet().iterator();
        String key;
        while(keys.hasNext()){
            key = keys.next();
            additionalPropertiesJson.put(key, additionalProperties.get(key));
        }
        json.put("additionalProperties", additionalPropertiesJson);
        return json;        
    }

    @Override
    public void parseJsonObject(JSONObject json) {
        id = json.getInt("id", -1);
        studyId = json.getInt("studyId", -1);
        folderId = json.getString("folderId", "");
        externalId = json.getString("externalId", "");
        name = json.getString("name", "");
        String key;        
        additionalProperties.clear();
        if(json.has("additionalProperties")){
            JSONObject additionalPropertiesJson = json.getJSONObject("additionalProperties");
            Iterator keys = additionalPropertiesJson.keys();
            while(keys.hasNext()){
                key = (String)keys.next();
                additionalProperties.put(key, additionalPropertiesJson.getString(key));
            }
        }
    }

    @Override
    public String toString() {
        if(externalId!=null && !externalId.isEmpty()){
            return externalId;
        } else {
            return super.toString();
        }
    }
    
    
}