/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;

/**
 * This class provides a simple wrapper for a workflow library.
 * @author hugo
 */
public class EscWorkflowLibrary extends EscDocument {
    private String libraryName = "";
    
    public EscWorkflowLibrary() {
    }

    public EscWorkflowLibrary(JSONObject json) {
        parseJsonObject(json);
    }

    @Override
    public void parseJsonObject(JSONObject json) {
        super.parseJsonObject(json);
        libraryName = json.getString("libraryName", "");
    }

    public String getLibraryName() {
        return libraryName;
    }

    public void setLibraryName(String libraryName) {
        this.libraryName = libraryName;
    }
    
    @Override
    public JSONObject toJsonObject() {
        return new JSONObject(this);
    }
    
    @Override
    public String getObjectType() {
        return getClass().getSimpleName();
    }           
}