/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.Base64;
import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;
import java.security.MessageDigest;

/**
 * This class represents a request to register a gateway with the IoT platform
 * @author hugo
 */
public class EscGatewayRegistrationRequest implements Serializable, JsonSerializable {
    private String studyCode;
    private String gatewayId;
    private String gatewayType;
    private long timestamp;
    private String signature;

    public EscGatewayRegistrationRequest(String studyCode, String gatewayId, String gatewayType) {
        this.studyCode = studyCode;
        this.gatewayId = gatewayId;
        this.gatewayType = gatewayType;
        this.timestamp = System.currentTimeMillis();
    }

    public String getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getStudyCode() {
        return studyCode;
    }

    public void setStudyCode(String studyCode) {
        this.studyCode = studyCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getGatewayType() {
        return gatewayType;
    }

    public void setGatewayType(String gatewayType) {
        this.gatewayType = gatewayType;
    }

    public EscGatewayRegistrationRequest() {
    }

    public EscGatewayRegistrationRequest(JSONObject json) {
        parseJsonObject(json);
    }
    
    public void sign(String code){
        // Hash the data with the key
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(studyCode.getBytes());
            md.update(gatewayId.getBytes());
            md.update(gatewayType.getBytes());
            md.update(Long.toString(timestamp).getBytes());
            md.update(code.getBytes());
            signature = Base64.encodeBytes(md.digest());
            
        } catch (Exception e){
            signature = "";
        }        
    }
    
    public boolean verify(String code){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(studyCode.getBytes());
            md.update(gatewayId.getBytes());
            md.update(gatewayType.getBytes());
            md.update(Long.toString(timestamp).getBytes());
            md.update(code.getBytes());
            String calculatedSignature = Base64.encodeBytes(md.digest());
            return(calculatedSignature.equals(signature));
        } catch (Exception e){
            return false;
        }
    }

    @Override
    public final JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        json.put("studyCode", studyCode);
        json.put("gatewayId", gatewayId);
        json.put("timestamp", timestamp);
        json.put("gatewayType", gatewayType);
        json.put("signature", signature);
        return json;
    }

    @Override
    public final void parseJsonObject(JSONObject json) {
        studyCode = json.getString("studyCode", "");
        gatewayId = json.getString("gatewayId", "");
        timestamp = json.getLong("timestamp", 0L);
        gatewayType = json.getString("gatewayType", "");
        signature = json.getString("signature");
    }
}