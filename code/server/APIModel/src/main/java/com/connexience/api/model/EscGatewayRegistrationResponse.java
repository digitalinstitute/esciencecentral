/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;

/**
 * This class contains the servers response to a registration request
 * @author hugo
 */
public class EscGatewayRegistrationResponse implements Serializable, JsonSerializable {
    private EscGateway gateway;
    private String gatewayPassword;
    private boolean gatewayPasswordPresent = false;
    private boolean registrationAccepted = false;
    private String rejectionReason ="";

    public EscGatewayRegistrationResponse() {
    }

    public EscGatewayRegistrationResponse(EscGateway gateway, String gatewayPassword) {
        this.gateway = gateway;
        this.gatewayPassword = gatewayPassword;
        this.gatewayPasswordPresent = true;
        registrationAccepted = true;
    }
    
    public EscGatewayRegistrationResponse(EscGateway gateway){
        this.gateway = gateway;
        this.gatewayPasswordPresent = false;
        this.gatewayPassword = "";
        registrationAccepted = true;
    }
    
    public EscGatewayRegistrationResponse(String rejectionReason) {
        this.rejectionReason = rejectionReason;
        registrationAccepted = false;
    }    
    
    public EscGatewayRegistrationResponse(JSONObject json){
        parseJsonObject(json);
    }

    public void setRegistrationAccepted(boolean registrationAccepted) {
        this.registrationAccepted = registrationAccepted;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public boolean isRegistrationAccepted() {
        return registrationAccepted;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public boolean isGatewayPasswordPresent() {
        return gatewayPasswordPresent;
    }

    public EscGateway getGateway() {
        return gateway;
    }

    public String getGatewayPassword() {
        return gatewayPassword;
    }

    public void setGateway(EscGateway gateway) {
        this.gateway = gateway;
    }

    public void setGatewayPassword(String gatewayPassword) {
        this.gatewayPassword = gatewayPassword;
    }

    public void setGatewayPasswordPresent(boolean gatewayPasswordPresent) {
        this.gatewayPasswordPresent = gatewayPasswordPresent;
    }
    
    @Override
    public final JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        if(gateway!=null){
            json.put("gateway", gateway.toJsonObject());
        } else {
            json.put("gateway", JSONObject.NULL);
        }
        json.put("gatewayPasswordPresent", gatewayPasswordPresent);
        json.put("gatewayPassword", gatewayPassword);
        json.put("rejectionReason", rejectionReason);
        json.put("registrationAccepted", registrationAccepted);
        return json;
    }

    @Override
    public final void parseJsonObject(JSONObject json) {
        if(!json.isNull("gateway")){
            gateway = new EscGateway(json.getJSONObject("gateway"));            
        } else {
            gateway = null;
        }
        gatewayPassword = json.getString("gatewayPassword", "");
        gatewayPasswordPresent = json.getBoolean("gatewayPasswordPresent", false);
        rejectionReason = json.getString("rejectionReason");
        registrationAccepted = json.getBoolean("registrationAccepted");
    }
}