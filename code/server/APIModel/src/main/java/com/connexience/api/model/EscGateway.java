/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class defines a gateway in a FlatStudy
 * @author hugo
 */
public class EscGateway extends EscStudyObject {
    private String studyCode;
    
    public EscGateway(){
        super();
    }
    
    public EscGateway(JSONObject json){
        parseJsonObject(json);
    }
    
    @Override
    public final JSONObject toJsonObject() {
        JSONObject json = super.toJsonObject();
        json.put("studyCode", studyCode);
        return json;
    }

    @Override
    public final void parseJsonObject(JSONObject json) {
        super.parseJsonObject(json);
        studyCode = json.getString("studyCode", "");
    }

    public String getStudyCode() {
        return studyCode;
    }

    public void setStudyCode(String studyCode) {
        this.studyCode = studyCode;
    }
}