/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

/**
 * This interface defines a token service that can generate JWTs for users
 * to access external APIs
 * @author hugo
 */
public interface TokenInterface {
    public static final String TOKEN_VALID = "VALID";
    public static final String TOKEN_INVALID = "INVALID";
    
    public EscJWT issueToken(String username, String password, String label) throws Exception;
    public void releaseToken(String id) throws Exception;
    public String validateToken(String jwtData) throws Exception;
}