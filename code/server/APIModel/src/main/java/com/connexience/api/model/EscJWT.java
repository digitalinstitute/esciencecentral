/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;

/**
 * This class provides a simple wrapper round a JWT with details of expiry
 * time.
 * @author hugo
 */
public class EscJWT implements Serializable, JsonSerializable {
    private long expiryTimestamp;
    private String id;
    private String token;

    public EscJWT() {
    }

    public long getExpiryTimestamp() {
        return expiryTimestamp;
    }

    public void setExpiryTimestamp(long expiryTimestamp) {
        this.expiryTimestamp = expiryTimestamp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    public EscJWT(JSONObject json){
        parseJsonObject(json);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void parseJsonObject(JSONObject json) {
        expiryTimestamp = json.getLong("expiryTimestamp", 0L);
        token = json.getString("token", null);
        id = json.getString("id", null);
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        json.put("expiryTimestamp", expiryTimestamp);
        json.put("token", token);     
        json.put("id", id);
        return json;
    }
}