/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This class represents an event that can get sent to the server and persisted
 * in a MongoDB.
 * @author hugo
 */
public class EscEvent implements Serializable, JsonSerializable, Comparable {
    protected long timestamp;
    protected HashMap<String, Object> data = new HashMap<>();
    protected HashMap<String, Object> metadata = new HashMap<>();
    protected String eventType = "generic";
    protected String primaryKey;
    
    public EscEvent(String eventType){
        this.eventType = eventType;
        timestamp = new Date().getTime();
    }
    
    public EscEvent() {
        timestamp = new Date().getTime();
    }
    
    public EscEvent(long timestamp){
        this.timestamp = timestamp;
    }
    
    public EscEvent(String primaryKey, Object value){
        timestamp = new Date().getTime();
        this.primaryKey = primaryKey;
        data.put(primaryKey, value);
    }
    
    public EscEvent(EscEvent objectToCopy){
        timestamp = objectToCopy.getTimestamp();
        eventType = objectToCopy.getEventType();
        Iterator<String> keys = objectToCopy.keys();
        String key;
        while(keys.hasNext()){
            key = keys.next();
            data.put(key, objectToCopy.objectValue(key));
        }
        
        keys = objectToCopy.getMetadata().keySet().iterator();
        while(keys.hasNext()){
            key = keys.next();
            metadata.put(key, objectToCopy.getMetadata().get(key));
        }
        primaryKey = objectToCopy.getPrimaryKey();
    }
    
    public EscEvent(JSONObject json){
        parseJsonObject(json);;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public void setData(HashMap<String, Object> data) {
        this.data = data;
    }

    public HashMap<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(HashMap<String, Object> metadata) {
        this.metadata = metadata;
    }

    public void setPrimaryKey(String primaryKey){
        this.primaryKey = primaryKey;
    }
    
    public boolean containsValue(String key){
        return data.containsKey(key);
    }
    
    public void setPrimaryKeyAndValue(String primaryKey, Object value){
        data.put(primaryKey, value);
        this.primaryKey = primaryKey;
    }
    
    public String getPrimaryKey(){
        return primaryKey;
    }
    
    public Object getPrimaryValue(){
        return data.get(primaryKey);
    }
    
    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
        
    public int size(){
        return data.size();
    }
    
    public void put(String key, Object value){
        if(data.size()==0){
            primaryKey = key;
        }
        data.put(key, value);
    }
    
    public Iterator<String> keys(){
        return data.keySet().iterator();
    }
    
    public Object objectValue(String key){
        return data.get(key);
    }
    
    public boolean containsKey(String key){
        return data.containsKey(key);
    }
    
    public String stringValue(String key){
        return data.get(key).toString().replace("\"","");
    }
    
    public int intValue(String key) throws Exception {
        if(data.get(key) instanceof Number){
            return ((Number)data.get(key)).intValue();
        } else {
            throw new Exception("Value: " + key + " is not a number");
        }
    }
    
    public long longValue(String key) throws Exception {
        if(data.get(key) instanceof Number){
            return ((Number)data.get(key)).longValue();
        } else {
            throw new Exception("Value: " + key + " is not a number");
        }
    }
        
    public double doubleValue(String key) throws Exception {
        if(data.get(key) instanceof Number){
            return ((Number)data.get(key)).doubleValue();
        } else {
            try {
                return Double.parseDouble(data.get(key).toString().replace("\"", ""));
            } catch (Exception e){
                throw e;
            }
            
        }
    }

    public String getCSVRow(List<String> columnNames){
        StringBuilder builder = new StringBuilder();
        String name;
        
        for(int i=0;i<columnNames.size();i++){
            name = columnNames.get(i);
            if(i>0){
                builder.append(",");
            }
            
            if(containsValue(name)){
                builder.append(stringValue(name)); 
            }
        }
        return builder.toString();
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Iterator<String>keys = keys();
        boolean first = true;
        String key;
        while(keys.hasNext()){
            key = keys.next();
            if(!first){
                builder.append(",");
            }
            builder.append(key);
            builder.append("=");
            builder.append(data.get(key).toString());
        }
        
        return builder.toString();
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        json.put("primaryKey", primaryKey);
        json.put("timestamp", timestamp);
        json.put("eventType", eventType);
        JSONObject dataJson = new JSONObject();
        for(String key : data.keySet()){
            dataJson.put(key, data.get(key));
        }
        
        JSONObject metadataJson = new JSONObject();
        for(String key : metadata.keySet()){
            metadataJson.put(key, metadata.get(key));
        }
        json.put("data", dataJson);
        json.put("metadata", metadataJson);
        return json;
    }

    @Override
    public void parseJsonObject(JSONObject json) {
        primaryKey = json.getString("primaryKey", null);
        timestamp = json.getLong("timestamp", 0L);
        eventType = json.getString("eventType", "generic");
        JSONObject dataJson = json.getJSONObject("data");
        Iterator keys = dataJson.keys();
        String key;
        data.clear();
        while(keys.hasNext()){
            key = keys.next().toString();
            data.put(key, dataJson.get(key));
        }
        
        metadata.clear();
        if(json.has("metadata")){
            JSONObject metadataJson = json.getJSONObject("metadata");
            keys = metadataJson.keys();
            while(keys.hasNext()){
                key = keys.next().toString();
                metadata.put(key, metadataJson.get(key));
            }
        }
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof EscEvent){
            EscEvent event = (EscEvent)o;
            if(event.getTimestamp()<timestamp){
                return -1;
            } else if(event.getTimestamp()>timestamp){
                return 1;
            } else {
                return 0;
            }
        } else {
            throw new ClassCastException();
        }
    }
    
    public String toJsonString(){
        return toJsonObject().toString(1);
    }
    
    public void parseJsonString(String jsonSource) throws Exception {
        JSONObject json = new JSONObject(jsonSource);
        parseJsonObject(json);
    }
}
