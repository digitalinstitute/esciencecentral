/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api;

import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscGateway;
import com.connexience.api.model.EscGatewayRegistrationRequest;
import com.connexience.api.model.EscGatewayRegistrationResponse;
import com.connexience.api.model.net.GenericClient;
import java.io.File;
import com.connexience.api.model.CatalogInterface;
import com.connexience.api.model.EscDevice;
import com.connexience.api.model.EscEvent;
import com.connexience.api.model.EscPerson;
import com.connexience.api.model.EscProject;
import com.connexience.api.model.json.JSONArray;
import com.connexience.api.model.json.JSONObject;
import java.util.Date;

/**
 * This class provides an client that can interact with the flat study api
 * @author hugo
 */
public class CatalogClient extends GenericClient implements CatalogInterface {
    public CatalogClient(String hostname, int port, boolean secure, String studyCode, String deviceId, String devicePassword){
        super(hostname, port, secure, "/api/public/rest/v1/catalog", studyCode, deviceId, devicePassword);
    }
    
    public CatalogClient(String hostname, int port, boolean secure, EscGateway gateway, String devicePassword){
        super(hostname, port, secure, "/api/public/rest/v1/catalog", gateway.getStudyCode(), gateway.getExternalId(), devicePassword);
    }

    public CatalogClient(String hostname, int port, boolean secure, String username, String password){
        super(hostname, port, secure, "/api/public/rest/v1/catalog", username, password);
    }
    
    public CatalogClient(String hostname, int port, boolean secure){
        super(hostname, port, secure, "/api/public/rest/v1/catalog");
    }
    
    public CatalogClient(String hostname, int port, boolean secure, String jwt){
        super(hostname, port, secure, "/api/public/rest/v1/catalog", jwt);
    }
    
    public CatalogClient(File apiProperties) throws Exception {
        super("/api/public/rest/v1/catalog", apiProperties);
    }

    public CatalogClient(GenericClient existingClient) throws Exception {
        existingClient.configureClient(this);
        this.setUrlBase("/api/public/rest/v1/catalog");
    }
    

    @Override
    public EscGatewayRegistrationResponse registerGateway(EscGatewayRegistrationRequest request) throws Exception {
        EscGatewayRegistrationResponse response = new EscGatewayRegistrationResponse(postJsonRetrieveJson("/registergateway", request.toJsonObject()));
        return response;
    }

    public EscGatewayRegistrationResponse registerNewGateway(String studyCode, String gatewayId, String gatewayType, String studyRegistrationCode) throws Exception {
        EscGatewayRegistrationRequest request = new EscGatewayRegistrationRequest(studyCode, gatewayId, gatewayType);
        request.sign(studyRegistrationCode);
        EscGatewayRegistrationResponse response = registerGateway(request);
        if(response.isRegistrationAccepted()){
            setAuthMethod(AuthMethod.GATEWAY);
            setDeviceId(response.getGateway().getExternalId());
            setDevicePassword(response.getGatewayPassword());
            setStudyCode(response.getGateway().getStudyCode());
        }
        return response;
    }
    
    public EscGatewayRegistrationResponse authenticateGateway(String studyCode, String gatewayId, String gatewayPassword) throws Exception {
        EscGatewayRegistrationRequest request = new EscGatewayRegistrationRequest(studyCode, gatewayId, "");
        request.sign(gatewayPassword);
        EscGatewayRegistrationResponse response = registerGateway(request);
        if(response.isRegistrationAccepted()){
            setAuthMethod(AuthMethod.GATEWAY);
            setDeviceId(gatewayId);
            setDevicePassword(gatewayPassword);
            setStudyCode(studyCode);
        }
        return response;
    }
    
    @Override
    public EscFolder getFolder(String gatewayId, String studyCode, FolderType folderType) throws Exception {
        return new EscFolder(retrieveJson("/studies/" + studyCode + "/folders/" + gatewayId + "/" + folderType.toString()));
    }
    
    public EscFolder getFolder(FolderType folderType) throws Exception {
        if(getAuthMethod()==AuthMethod.GATEWAY){
            return new EscFolder(retrieveJson("/studies/" + getStudyCode() + "/folders/" + getDeviceId() + "/" + folderType.toString()));
        } else {
            throw new Exception("getFolder only works for GatewayDevices");
        }
    }
    
    public StorageClient createDeviceStorageClient(){
        return new StorageClient(this);
    }

    public String pushEvent(String gatewayId, String studyCode, EscEvent event) throws Exception {
        return postJsonRetrieveText("/studies/" + studyCode + "/events/" + gatewayId, event.toJsonObject());
    } 
    
    public String pushEvent(EscEvent event) throws Exception {
        return pushEvents(new EscEvent[]{event});
    }

    @Override
    public String pushEvents(String deviceId, String studyCode, EscEvent[] events) throws Exception {
        JSONArray eventsArray = new JSONArray();
        for(EscEvent e : events){
            eventsArray.put(e.toJsonObject());
        }
        return postJsonArrayRetrieveText("/studies/" + studyCode + "/batchevents/" + deviceId, eventsArray);
    }

    
    public String pushEvents(EscEvent[] events) throws Exception {
        return pushEvents(getDeviceId(), getStudyCode(), events);
    }

    @Override
    public EscProject getProjectByStudyCode(String studyCode) throws Exception {
        return new EscProject(retrieveJson("/studiesbyexternalcode/" + studyCode));
    }

    @Override
    public int getNumberOfPeopleInStudy(int projectId) throws Exception {
        return Integer.parseInt(retrieveString("/studiesbyid/" + projectId + "/people/count"));
    }

    @Override
    public void removePerson(int personId) throws Exception {
        deleteResource("/peoplebyid/" + personId);
    }
    
    @Override
    public EscPerson getPerson(int personId) throws Exception {
        JSONObject json = retrieveJson("/peoplebyid/" + personId);
        return new EscPerson(json);
    }

    @Override
    public EscPerson[] searchPeople(String projectId, String queryText) throws Exception {
        JSONArray results = postTextRetrieveJsonArray("/studiesbyid/" + projectId + "/people/search", queryText);
        EscPerson[] people = new EscPerson[results.length()];
        for(int i=0;i<results.length();i++){
            people[i] = new EscPerson(results.getJSONObject(i));
        }
        return people;
    }

    @Override
    public EscPerson movePerson(int personId, String newStudyId) throws Exception {
        JSONObject json = retrieveJson("/peoplebyid/" + personId + "/moveto/" + newStudyId);
        return new EscPerson(json);
    }
    
    @Override
    public EscPerson[] getPeople(int projectId, int startIndex, int count) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + projectId + "/people/list/" + startIndex + "/" + count);
        EscPerson[] people = new EscPerson[json.length()];
        for(int i=0;i<json.length();i++){
            people[i] = new EscPerson(json.getJSONObject(i));
        }
        return people;
    }

    @Override
    public int getNumberOfDevicesInStudy(int projectId) throws Exception {
        return Integer.parseInt(retrieveString("/studiesbyid/" + projectId + "/devices/count"));
    }

    @Override
    public EscDevice getDevice(int deviceId) throws Exception {
        JSONObject json = retrieveJson("/devicesbyid/" + deviceId);
        return new EscDevice(json);
    }

    @Override
    public EscDevice[] getDevices(int projectId, int startIndex, int count) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + projectId + "/devices/list/" + startIndex + "/" + count);
        EscDevice[] devices = new EscDevice[json.length()];
        for(int i=0;i<json.length();i++){
            devices[i] = new EscDevice(json.getJSONObject(i));
        }
        return devices;        
    }

    @Override
    public int getNumberOfGatewaysInStudy(int projectId) throws Exception {
        return Integer.parseInt(retrieveString("/studiesbyid/" + projectId + "/gateways/count"));
    }

    @Override
    public EscGateway getGateway(int gatewayId) throws Exception {
        JSONObject json = retrieveJson("/gatewaysbyid/" + gatewayId);
        return new EscGateway(json);
    }

    @Override
    public EscGateway[] getGateways(int projectId, int startIndex, int count) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + projectId + "/gateways/list/" + startIndex + "/" + count);
        EscGateway[] gateways = new EscGateway[json.length()];
        for(int i=0;i<json.length();i++){
            gateways[i] = new EscGateway(json.getJSONObject(i));
        }
        return gateways;  
    }

    @Override
    public String[] listEventTypes(String studyCode, String gatewayId) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + studyCode + "/gateways/" + gatewayId + "/eventtypes");
        String[] results = new String[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = json.getString(i);
        }
        return results;
    }

    @Override
    public String[] listEventTypes(String studyCode) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + studyCode + "/eventtypes");
        String[] results = new String[json.length()];
        for(int i=0;i<json.length();i++){
            results[i] = json.getString(i);
        }
        return results;
    }
    
    @Override
    public EscEvent[] queryEvents(String studyCode, String gatewayId, long startDate, long endDate, String eventType, int maxResults) throws Exception {
        JSONArray json = retrieveJsonArray("/studiesbyid/" + studyCode + "/gateways/" + gatewayId + "/events/" + eventType + "/" + startDate + "/" + endDate + "/" + maxResults);
        EscEvent[] events = new EscEvent[json.length()];
        for(int i=0;i<json.length();i++){
            events[i] = new EscEvent(json.getJSONObject(i));
        }
        return events;
    }
}