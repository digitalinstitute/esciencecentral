/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model;

import com.connexience.api.model.json.JSONObject;
import com.connexience.api.model.json.JsonSerializable;

/**
 * This clas represents a project / group within ESC
 * @author hugo
 */
public class EscProject implements JsonSerializable {
    public enum ProjectType {
        HEIRARCHICAL, FLAT, BASIC
    }
    
private String id;
private String name;
private String description;
private String workflowFolderId;
private String dataFolderId;
private String creatorId;
private String externalId;
private ProjectType projectType = ProjectType.HEIRARCHICAL;
    
    public EscProject() {
    }
    
    public EscProject(JSONObject json){
        parseJsonObject(json);
    }
    
    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkflowFolderId() {
        return workflowFolderId;
    }

    public ProjectType getProjectType() {
        return projectType;
    }

    public void setProjectType(ProjectType projectType) {
        this.projectType = projectType;
    }
    
    public void setWorkflowFolderId(String workflowFolderId) {
        this.workflowFolderId = workflowFolderId;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }
   
    @Override
    public void parseJsonObject(JSONObject json) {
        creatorId = json.getString("creatorId", null);
        id = json.getString("id", null);
        name = json.getString("name", null);
        description = json.getString("description", null);
        dataFolderId = json.getString("dataFolderId", null);
        workflowFolderId = json.getString("workflowFolderId", null);
        projectType = ProjectType.valueOf(json.getString("projectType", "HEIRARCHICAL"));
        externalId = json.getString("externalId", null);
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject json = new JSONObject();
        json.put("creatorId", creatorId);
        json.put("id", id);
        json.put("name", name);
        json.put("description", description);
        json.put("dataFolderId", dataFolderId);
        json.put("workflowFolderId", workflowFolderId);
        json.put("projectType", projectType.toString());
        json.put("externalId", externalId);
        return json;
    }
}