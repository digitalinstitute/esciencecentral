/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model.events;

import com.connexience.api.model.EscEvent;

/**
 * This is a standard event that represents an environmental reading
 * @author hugo
 */
public class EnvironmentalEvent extends EscEvent {

    public EnvironmentalEvent() {
        super();
        setEventType("Environmental");
    }
    
    public void setTemperature(double degreesC){
        put("Temperature", degreesC);
    }
    
    public void setPressure(double mmHg){
        put("Pressure", mmHg);
    }
    
    public void setHumidity(double percentageRH){
        put("Humidity", percentageRH);
    }
}