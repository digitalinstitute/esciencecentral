/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.api.model.events;

import com.connexience.api.model.EscEvent;

/**
 * Event wrapper for a location event
 * @author hugo
 */
public class LocationEvent extends EscEvent {
    public enum Provider {gps, network};

    public LocationEvent() {
        super();
        setEventType("Location");
        put("Altitude", -1);
        put("Speed", -1);
        
    }

    public void setLatitude(double latitude) {
        put("Latitude", latitude);
    }
    
    public void setLongitude(double longitude){
        put("Longitude", longitude);
    }
    
    public void setAltitude(double altitudeInM){
        put("Altitude", altitudeInM);
    }
    
    public void setAccuracy(double accuracyInM){
        put("Accuracy", accuracyInM);
    }
    
    public void setSpeed(double speedInKmH){
        put("Speed", speedInKmH);
    }
    
    public void setProvider(Provider p){
        put("Provider", p.toString());
    }
}