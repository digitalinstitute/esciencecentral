/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.study.parser;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class description here.
 *
 * @author ndjm8
 */
public class ParsedGroup implements Serializable {
    public Integer escID = null;

    public String name = null;

    public String category = null;

    public String phaseName = null;

    public String password = null;

    public ParsedGroup parent = null;

    public Map<String, ParsedGroup> children = new HashMap<>();

    public Map<String, ParsedSubject> subjects = new HashMap<>();

    public Map<String, ParsedLoggerDeployment> deployments = new HashMap<>();

    public ParsedGroup checkGroup(final String name, final String category, final String phaseName, final String password) {
        if (!children.containsKey(name)) {
            final ParsedGroup group = new ParsedGroup();

            group.parent = this;
            group.name = name;
            group.category = category;
            group.phaseName = phaseName;
            group.password = password;

            children.put(name, group);
        }

        return children.get(name);
    }

    public ParsedSubject checkSubject(final String externalID) {
        if (!subjects.containsKey(externalID)) {
            final ParsedSubject subject = new ParsedSubject();

            subject.externalID = externalID;

            subjects.put(externalID, subject);
        }

        return subjects.get(externalID);
    }

    public ParsedLoggerDeployment checkDeployment(final String loggerID, final String configName, final Date startDate, final Date endDate) {
        if (!deployments.containsKey(loggerID)) {
            final ParsedLoggerDeployment logger = new ParsedLoggerDeployment();
            logger.loggerID = loggerID;
            logger.config = configName;
            logger.startDate = startDate;
            logger.endDate = endDate;

            deployments.put(loggerID, logger);
        }

        return deployments.get(loggerID);
    }
}
