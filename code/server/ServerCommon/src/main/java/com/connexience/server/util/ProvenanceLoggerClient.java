/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import com.connexience.server.ejb.provenance.IProvenanceLogger;
import com.connexience.server.model.provenance.events.GraphOperation;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.jboss.logging.Logger;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;

/**
 * Client to send a JMS Message to the provenance server to log an operation without going via the server
 * User: nsjw7
 * Date: Mar 15, 2011
 * Time: 4:02:04 PM
 */
public class ProvenanceLoggerClient implements IProvenanceLogger, Closeable {
    static Logger logger = Logger.getLogger(ProvenanceLoggerClient.class.getName());

    private static boolean enabled = false;
    private Destination destination = null;
    private Session session = null;
    private Connection connection = null;

    public ProvenanceLoggerClient() {
        enabled = PreferenceManager.getSystemPropertyGroup("Provenance").booleanValue("Enabled", true);
    }

    /**
     * Create the default properties for the provenance logger client
     */
    public static void createDefaultProperties() {
        PreferenceManager.getSystemPropertyGroup("Provenance").add("Enabled", true, "Should JMS Messages be sent to the provenance server");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSServerHost", "localhost", "Host of the JMS Server");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSServerPort", 8080, "Port of the JMS Server");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSUser", "connexience", "Username of the JMS Destination");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSPassword", "1234", "Password for the JMS Destination");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSHttpAttach", "true", "Should HTTP be used for JMS attachment");
    }

    public boolean checkConnection() throws Exception {
        if(session != null && connection != null && destination != null) {
            return true;
        } else {
            // Try and connect
            String jMSServerURLHost = PreferenceManager.getSystemPropertyGroup("Provenance").stringValue("JMSServerHost", "localhost");
            int jMSServerURLPort = PreferenceManager.getSystemPropertyGroup("Provenance").intValue("JMSServerPort", 8080);
            String jMSUser = PreferenceManager.getSystemPropertyGroup("Provenance").stringValue("JMSUser", "connexience");
            String jMSPassword = PreferenceManager.getSystemPropertyGroup("Provenance").stringValue("JMSPassword", "1234");
            boolean httpAttach = PreferenceManager.getSystemPropertyGroup("Provenance").booleanValue("JMSHttpAttach", true);

            if (httpAttach) {
                logger.debug("Attaching HTTP JMS to: " + jMSServerURLHost + ":" + jMSServerURLPort);
                final Properties env = new Properties();
                env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
                env.put(Context.PROVIDER_URL, "http-remoting://" + jMSServerURLHost + ":" + jMSServerURLPort);
                env.put(Context.SECURITY_PRINCIPAL, jMSUser);
                env.put(Context.SECURITY_CREDENTIALS, jMSPassword);
                Context context = new InitialContext(env);

                // Lookup the connection factory and the queue
                ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup("jms/RemoteConnectionFactory");
                destination = (Destination) context.lookup("ProvenanceEvents");

                //TO SET THE CONSUMER WINDOW SIZE IN WILDFLY EDIT STANDALONE.XML
                connection = connectionFactory.createConnection(jMSUser, jMSPassword);
                session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
                return true;
            } else {
                logger.debug("Attaching NETTY JMS to: " + jMSServerURLHost + ":" + jMSServerURLPort + " on queue: ProvenanceEvents");
                Map connectionParameters = new HashMap();
                connectionParameters.put(TransportConstants.HOST_PROP_NAME, jMSServerURLHost);
                connectionParameters.put(TransportConstants.PORT_PROP_NAME, jMSServerURLPort);
                TransportConfiguration config = new TransportConfiguration(NettyConnectorFactory.class.getName(), connectionParameters);
                ConnectionFactory cf = HornetQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.CF, config);
                connection = cf.createConnection("connexience", "1234");
                session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
                destination = session.createTopic("ProvenanceEvents");
                return true;
            }            
        }
    }
    
    /**
     * Log a graph operation
     *
     * @param operation operation to be logged
     */
    public void log(GraphOperation operation) {
        try {
            if (ProvenanceLoggerClient.enabled) {
                sendMessage(operation); //ProvenanceQueue = queue name
            }
        } catch (Exception e) {
            logger.fatal("Cannot send message to proveneance service");
        }
    }

    /**
     * Send the message
     *
     * @param message   graph operation
     * @throws Exception something went wrong
     */
    private void sendMessage(GraphOperation message) throws Exception {

        try {
            if(checkConnection()){
                MessageProducer producer = session.createProducer(destination);
                BytesMessage bm = session.createBytesMessage();
                byte[] data = SerializationUtils.serialize(message);
                bm.writeBytes(data);
                producer.send(bm);
                producer.close();
            } else {
                logger.error("Cannot get JMS connection");
            }
        } catch (Exception e) {
            logger.info("Cannot send provenance message: " + e.getMessage() + " - disabling provenance until next restart");
            logger.debug(e);
            ProvenanceLoggerClient.enabled = false;
        }
    }


    @Override
    public void close() throws IOException
    {
        if (session != null) {
            try {
                session.close();
                session = null;
            } catch (JMSException x) {
                throw new IOException("Problems closing the JMS session", x);
            }
        }

        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (JMSException x) {
                throw new IOException("Problems closing the JMS connection", x);
            }
        }
    }
}
