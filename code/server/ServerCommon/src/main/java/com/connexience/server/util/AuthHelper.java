/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import com.connexience.api.model.HexEncoder;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.Ticket;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;

/**
 * This class reads the authorization header from a request and returns the
 * correct type of ticket.
 * @author hugo
 */
public class AuthHelper {
    private static Ticket getTicket(String authMethod, String basicAuthHeader, String projectContext) throws ConnexienceException {
        if(authMethod.equalsIgnoreCase("USERNAME_PASSWORD")){
            Ticket t;
            if(basicAuthHeader.toLowerCase().startsWith("basic")){
                t = doBasicAuth(basicAuthHeader); 
            } else {
                t = doJwtAuth(basicAuthHeader);
            }
            
            if(projectContext!=null){
                List<Project> projects = EJBLocator.lookupProjectsBean().getProjectsByExternalId(t, projectContext);
                if(projects.size()>0){
                    t.setDefaultProjectId(Integer.toString(projects.get(0).getId()));
                }
            }
            return t;
            
        } else if(authMethod.equalsIgnoreCase("GATEWAY")){
            return doGatewayAuth(basicAuthHeader);
            
        } else if(authMethod.equalsIgnoreCase("JWT")){
            Ticket t = doJwtAuth(basicAuthHeader);
            if(projectContext!=null){
                List<Project> projects = EJBLocator.lookupProjectsBean().getProjectsByExternalId(t, projectContext);
                if(projects.size()>0){
                    t.setDefaultProjectId(Integer.toString(projects.get(0).getId()));
                }
            }
            return t;
            
        } else {
            return null;
        }
    }
    
    public static Ticket getTicket(HttpServletRequest request) throws ConnexienceException {
        String authData = request.getHeader("Authorization");
        String authMethod;
        String projectContext = request.getHeader("ProjectContext");

        if(request.getHeader("AuthMethod")!=null){
            authMethod = request.getHeader("AuthMethod").trim();
        } else {
            authMethod = "USERNAME_PASSWORD";
        }
        Ticket t = getTicket(authMethod, authData, projectContext);
        

        return t;
    }
    
    public static Ticket getTicket(HttpHeaders headers) throws ConnexienceException {
        List<String> authMethodHeaders = headers.getRequestHeader("AuthMethod");
        List<String> authData = headers.getRequestHeader("Authorization");
        List<String> contextData = headers.getRequestHeader("ProjectContext");
        String projectContext;
        if(contextData.size()>0){
            projectContext = contextData.get(0);
        } else {
            projectContext = null;
        }
        
        Ticket t;
        if(authMethodHeaders.size()>0){
            String authMethod = authMethodHeaders.get(0).trim();
            if(authData.size()>0){
                String basicAuthHeader = authData.get(0).trim();
                t = getTicket(authMethod, basicAuthHeader, projectContext);
            } else {
                throw new ConnexienceException("No authentication data present in request");
            }
        } else {
            // Default to a BASIC AUTH ticket because this is probably an old client
            if(authData.size()>0){
                String basicAuthHeader = authData.get(0).trim();
                t = getTicket("USERNAME_PASSWORD", basicAuthHeader, projectContext);
                            
            } else {
                throw new ConnexienceException("No authentication data present in request");
            }            
        }
        
        return t;
    }
    
    private static Ticket doGatewayAuth(String authData) throws ConnexienceException {
        String auth = authData.substring(6).trim();
        String decoded = new String(HexEncoder.decode(auth.toCharArray()));

        StringTokenizer tokenizer = new StringTokenizer(decoded, ":");
        if(tokenizer.countTokens()==3){
            String studyCode = tokenizer.nextToken();
            String deviceId = tokenizer.nextToken();
            String password = tokenizer.nextToken();
            return EJBLocator.lookupTicketBean().createGatewayTicket(deviceId, studyCode, password);
        } else {
            throw new ConnexienceException("Malformed gateway authentication data");
        }
    }
    
    private static Ticket doJwtAuth(String authData) throws ConnexienceException {
        String jwt = authData.substring(6).trim();
        return EJBLocator.lookupTicketBean().acquireTicketFromToken(jwt);
    }
    
    private static Ticket doBasicAuth(String authData) throws ConnexienceException {                
        String auth = authData.substring(5).trim();
        String decoded = new String(Base64.decode(auth));
        int pos = decoded.indexOf(":");
        String userId = decoded.substring(0, pos);
        String password = decoded.substring(pos + 1);
        return EJBLocator.lookupTicketBean().acquireTicket(userId, password);
    }
    
    public static void securityCheck(Ticket ticket, String resourceId) throws ConnexienceException {
        if(ticket.getUserType()==Ticket.UserType.DEVICE){
            ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, resourceId, ServerObject.class);
            if(obj!=null){
                securityCheck(ticket, obj);
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        }
    }
    
    public static void excludeDeviceTicket(Ticket ticket) throws ConnexienceException {
        if(ticket.getUserType()==Ticket.UserType.DEVICE){
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    public static void securityCheck(Ticket ticket, ServerObject obj) throws ConnexienceException {
        if(ticket.getUserType()==Ticket.UserType.DEVICE){
            if(obj instanceof Folder){
                if(!canGatewayAccessFolder(ticket, (Folder)obj)){
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else if(obj instanceof DocumentRecord){
                if(!canGatewayAccessFile(ticket, (DocumentRecord)obj)){
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                } 
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        }
    }
    
    public static boolean canGatewayAccessFolder(Ticket gatewayTicket, String folderId) throws ConnexienceException {
        Folder folder = EJBLocator.lookupStorageBean().getFolder(gatewayTicket, folderId);
        return canGatewayAccessFolder(gatewayTicket, folder);
    }
    
    public static boolean canGatewayAccessFolder(Ticket gatewayTicket, Folder folder) throws ConnexienceException {
        String dataFolderId = EJBLocator.lookupFlatStudyBean().getFlatGatewayFolderId(gatewayTicket, gatewayTicket.getDeviceId());
        if(folder.getContainerId().equals(dataFolderId) || folder.getId().equals(dataFolderId)){
            return true;
        } else {
            if(gatewayTicket.getDefaultProjectId()!=null && !gatewayTicket.getDefaultProjectId().isEmpty()){
                FlatStudy s = EJBLocator.lookupFlatStudyBean().getFlatStudy(gatewayTicket, Integer.valueOf(gatewayTicket.getDefaultProjectId()));
                if(s!=null){
                    if(folder.getContainerId().equals(s.getCodeFolderId())){
                        return true;
                    } else if(folder.getId().equals(s.getCodeFolderId())){
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            return false;
        }
    }
    
    public static boolean canGatewayAccessFile(Ticket gateTicket, DocumentRecord doc) throws ConnexienceException {
        Folder parent = EJBLocator.lookupStorageBean().getFolder(gateTicket, doc.getContainerId());
        return canGatewayAccessFolder(gateTicket, parent);
    }
}