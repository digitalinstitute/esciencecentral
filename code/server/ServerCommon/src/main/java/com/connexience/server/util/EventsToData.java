/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import com.connexience.api.model.EscEvent;
import com.connexience.api.model.events.EnvironmentalEvent;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.CSVDataExporter;

/**
 * This class converts a collection of EscEvents to a Data set
 * @author hugo
 */
public class EventsToData {
    List<EscEvent> events;
    private DateFormat dataFormat = DateFormat.getDateTimeInstance();
    
    public EventsToData(List<EscEvent> events) {
        this.events = events;
    }

    public void setDataFormat(DateFormat dataFormat) {
        this.dataFormat = dataFormat;
    }
    
    public Data toData() throws Exception {
        Data results = new Data();
        Data row;
        boolean firstRow = true;
        Column existingColumn;
        Column newColumn;
        
        for(EscEvent e : events){
            row = createRowForEvent(e, results.getSmallestRows());
            if(firstRow){
                results = row;
                firstRow = false;
            } else {
                // Check existing rows
                for(int i=1;i<results.getColumnCount();i++){
                    existingColumn = results.column(i);
                    if(row.containsColumn(existingColumn.getName())){
                        // Can add data
                        try {
                            existingColumn.appendObjectValue(row.column(existingColumn.getName()).getObjectValue(0));
                        } catch (Exception ex){
                            existingColumn.appendObjectValue(new MissingValue());
                        }
                    } else {
                        // Nothing in the new row - need a missing value
                        existingColumn.appendObjectValue(new MissingValue());
                    }
                }
                
                // Check for missing rows
                for(int i=1;i<row.getColumnCount();i++){
                    if(!results.containsColumn(row.column(i).getName())){
                        // Need to add a new one
                        newColumn = row.column(i).getEmptyCopy();
                        newColumn.padToSize(results.getSmallestRows());
                        newColumn.appendObjectValue(row.column(i).getObjectValue(0));
                        results.addColumn(newColumn);
                    }
                }
                
                // Stick in the timestamp
                results.column(0).appendObjectValue(row.column(0).getObjectValue(0));
            }
        }
        return results;
    }
    
    public Data createRowForEvent(EscEvent e, int existingSize) throws Exception {
        Data d = new Data();
        Iterator<String> keys = e.keys();
        String key;
        Object value;
        // Timestamp column
        DateColumn timestampColumn = new DateColumn("Timestamp");
        timestampColumn.appendDateValue(new Date(e.getTimestamp()));
        d.addColumn(timestampColumn);
        
        // Add rest
        while(keys.hasNext()){
            key = keys.next();
            value = e.objectValue(key);
            if(!"timestamp".equalsIgnoreCase(key) && !"time".equalsIgnoreCase(key)){
                if(value instanceof Number){
                    // Add as double
                    DoubleColumn c = new DoubleColumn(key);
                    c.appendDoubleValue(((Number)value).doubleValue());
                    d.addColumn(c);

                } else if(value instanceof Date){
                    // Add as Date
                    DateColumn c = new DateColumn(key);
                    c.appendDateValue((Date)value);
                    d.addColumn(c);

                } else {
                    // Add as string
                    StringColumn c = new StringColumn(key);
                    c.appendStringValue(value.toString());
                    d.addColumn(c);
                }
            }
        }
        return d;
    }
}