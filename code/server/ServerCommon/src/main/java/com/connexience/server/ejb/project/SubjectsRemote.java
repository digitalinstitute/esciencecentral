package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.project.study.Subject;
import com.connexience.server.model.project.study.SubjectGroup;

import javax.ejb.Remote;
import java.util.Collection;
import java.util.HashMap;

@Remote
@SuppressWarnings("unused")
public interface SubjectsRemote {
    /**
     * Core method for retrieving a single SubjectGroup
     */
    SubjectGroup getSubjectGroup(Ticket ticket, Integer subjectGroupId) throws ConnexienceException;

    /**
     * Core method for saving/updating a single SubjectGroup
     */
    SubjectGroup saveSubjectGroup(Ticket ticket, SubjectGroup subjectGroup) throws ConnexienceException;

    /**
     * Core method for removing a SubjectGroup (and all of its children and Subjects)
     */
    void deleteSubjectGroup(Ticket ticket, Integer subjectGroupId) throws ConnexienceException;

    Collection<SubjectGroup> getPhaseSubjectGroups(Ticket ticket, Integer phaseId) throws ConnexienceException;

    Collection<SubjectGroup> getChildSubjectGrous(Ticket ticket, Integer phaseId, Integer parentGroupId) throws ConnexienceException;

    /**
     * Core method for retrieving a single Subject
     */
    Subject getSubject(Ticket ticket, Integer subjectId) throws ConnexienceException;

    /**
     * Get a subject from a Study using its external Id
     */
    Subject getSubjectInStudyByExternalId(Ticket ticket, Integer studyId, String externalId) throws ConnexienceException;

    /**
     * Core method for saving/updating a single Subject
     */
    Subject saveSubject(Ticket ticket, Subject subject);

    /**
     * Core method for removing a Subject
     */
    void deleteSubject(Ticket ticket, Integer subjectId) throws ConnexienceException;

    /**
     * Convenience method for retrieving all SubjectGroup objects associated with a Study
     */
    Collection<SubjectGroup> getSubjectGroups(Ticket ticket, Integer studyId) throws ConnexienceException;

    /**
     * Creates a new SubjectGroup in the specified Study
     */
    SubjectGroup createSubjectGroup(Ticket ticket, Integer studyId, String groupName, Integer phaseId) throws ConnexienceException;

    SubjectGroup createSubjectGroup(final Ticket ticket, Integer studyId, final String groupName, final String categoryName, final String phaseName, HashMap<String,String>additionalProperties) throws ConnexienceException;

    SubjectGroup createSubjectGroup(Ticket ticket, Integer studyId, String groupName, String categoryName, String phaseName) throws ConnexienceException;

    SubjectGroup createChildGroup(final Ticket ticket, Integer studyId, Integer parentSubjectGroupId, final String groupName, final String categoryName, final String phaseName, final HashMap<String, String> additionalProperties) throws ConnexienceException;

    /**
     * Created a new SubjectGroup under the specified parent SubjectGroup
     */
    SubjectGroup createChildGroup(Ticket ticket, Integer studyId, Integer parentSubjectGroupId, String groupName, final Integer phaseId) throws ConnexienceException;

    /**
     * Adds an specified Subject to the specified SubjectGroup
     */
    SubjectGroup addGroupSubject(Ticket ticket, Integer parentSubjectGroupId, Integer subjectId) throws ConnexienceException;

    /**
     * Removes the specified Subject from the specified SubjectGroup
     */
    SubjectGroup removeGroupSubject(Ticket ticket, Integer parentSubjectGroupId, Integer subjectId) throws ConnexienceException;

    /**
     * Convenience method to find all Subjects associated with a specific Study (instead of traversing SubjectGroups)
     */
    Collection<Subject> getSubjects(Ticket ticket, Integer studyId) throws ConnexienceException;

    Collection<Subject> getSubjectsInPhase(Ticket ticket, Integer studyId, Integer phaseId) throws ConnexienceException;

    Collection<Subject> getSubjectsInGroup(final Ticket ticket, final Integer subjectGroupId) throws ConnexienceException;

    SubjectGroup createChildGroup(Ticket ticket, Integer studyId, Integer parentSubjectGroupId, String groupName, final String categoryName, String phaseName) throws ConnexienceException;

    Subject createSubject(Ticket ticket, Integer parentSubjectGroupId, String subjectReadableId) throws ConnexienceException;

    Subject createSubject(Ticket ticket, Integer parentSubjectGroupId, String subjectReadableId, HashMap<String, String> additionalProperties) throws ConnexienceException;
    
    void deleteSubjectGroup(final Ticket ticket, final Integer subjectGroupId, final Boolean deleteFiles) throws ConnexienceException;

    Collection<Subject> getSubjectFromProperty(final Ticket ticket, final String propertyName, final String propertyValue) throws ConnexienceException;

    boolean isPropertyUnique(final Ticket ticket, final String propertyName, final String propertyValue) throws ConnexienceException;
    
    Subject addPropertiesToSubject(final Ticket ticket, final Integer subjectId, final HashMap<String, String> properties) throws ConnexienceException;
    
    SubjectGroup addPropertiesToSubjectGroup(final Ticket ticket, final Integer groupId, final HashMap<String, String> properties) throws ConnexienceException;
}
