package com.connexience.server.util;

import java.io.Serializable;

/**
 *
 * Represents a report from a failed login
 *
 * User: nsjw7
 * Date: 31/03/15
 * Time: 11:27
 */
public class LoginReport implements Serializable {

    private String username = "";
    private boolean allowPasswordReset = false;
    private String reason = "";
    private LoginNextAction nextAction;

    public static final String WRONG_PASSWORD = "Incorrect username and password combination";


    public LoginReport() {
    }

    public LoginReport(String username, String reason) {
        this.username = username;
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isAllowPasswordReset() {
        return allowPasswordReset;
    }

    public void setAllowPasswordReset(boolean allowPasswordReset) {
        this.allowPasswordReset = allowPasswordReset;
    }

    public LoginNextAction getNextAction() {
        return nextAction;
    }

    public void setNextAction(LoginNextAction nextAction) {
        this.nextAction = nextAction;
    }
}
