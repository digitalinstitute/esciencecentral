package com.connexience.server.util;

/**
 * A class that helps to convert between long and data size in the SI, IEC and JEDEC formats.
 *
 * Created by Jacek on 14/02/2017.
 */
public class DataSize
{
    public static class Prefix {
        public static final int SI = 1 << 0;
        public static final int JEDEC  = 1 << 1;
        public static final int IEC    = 1 << 2;
    }

    public static long parseDataSize(String dataSize)
    {
        return parseDataSize(dataSize, Prefix.SI | Prefix.IEC);
    }


    public static long parseDataSize(String dataSize, int fmtFlag)
    {
        if ((fmtFlag & Prefix.JEDEC) != 0 && (fmtFlag & Prefix.SI) != 0) {
            throw new IllegalArgumentException("JEDEC and Metric data unit size formats are in conflict. Please use one of these.");
        }

        NumberFormatException ex = null;

        if ((fmtFlag & Prefix.IEC) != 0) {
            try {
                return parseDataSize_IEC(dataSize);
            } catch (NumberFormatException x) {
                // Ignore for this moment but remember for later
                ex = x;
            }
        }

        if ((fmtFlag & Prefix.SI) != 0) {
            try {
                return parseDataSize_SI(dataSize);
            } catch (NumberFormatException x) {
                // Ignore for this moment but remember for later
                ex = x;
            }
        }

        if ((fmtFlag & Prefix.JEDEC) != 0) {
            try {
                return parseDataSize_JEDEC(dataSize);
            } catch (NumberFormatException x) {
                // Ignore for this moment but remember for later
                ex = x;
            }
        }

        if (ex != null) {
            throw ex;
        } else {
            throw new IllegalArgumentException("Cannot parse value " + dataSize + " with data size format " + fmtFlag);
        }
    }


    /**
     * Parses the amount of data which can optionally be expressed with suffixes in accordance to the IEC standard,
     * like KiB/MiB, etc.
     * Recognizes sizes upto ExbiByte = 1EiB = 1024^6 = 2^60 as 16EiB-1 is the maximum long value.
     *
     * If no suffix is provided, byte size is assumed.
     *
     * For example, strings "1024", "1024B" and "1KiB" will all result in value 1024.
     *
     * @param dataSize
     * @return
     */
    private static long parseDataSize_IEC(String dataSize)
    {
        long factor = 1;

        if (dataSize.endsWith("KiB")) {
            factor = 0x0400L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("MiB")) {
            factor = 0x0010_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("GiB")) {
            factor = 0x4000_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("TiB")) {
            factor = 0x0010_0000_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("PiB")) {
            factor = 0x0004_0000_0000_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("EiB")) {
            factor = 0x1000_0000_0000_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("B")) {
            dataSize = dataSize.substring(0, dataSize.length() - 1);
        }

        return factor * Long.parseLong(dataSize);
    }

    private static Long parseDataSize_SI(String dataSize)
    {
        long factor = 1;

        if (dataSize.endsWith("kB")) {
            factor = 1000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("MB")) {
            factor = 1000_000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("GB")) {
            factor = 1000_000_000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("TB")) {
            factor = 1000_000_000_000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("PB")) {
            factor = 1000_000_000_000_000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("EB")) {
            factor = 1000_000_000_000_000_000L;
            dataSize = dataSize.substring(0, dataSize.length() - 2);
        } else if (dataSize.endsWith("B")) {
            dataSize = dataSize.substring(0, dataSize.length() - 1);
        }

        return factor * Long.parseLong(dataSize);
    }

    private static Long parseDataSize_JEDEC(String dataSize)
    {
        // Note that officially JEDEC units end on GB.

        long factor = 1;

        if (dataSize.endsWith("KB")) {
            factor = 0x0400L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("MB")) {
            factor = 0x0010_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
        } else if (dataSize.endsWith("GB")) {
            factor = 0x4000_0000L;
            dataSize = dataSize.substring(0, dataSize.length() - 3);
            /*}
            else if (dataSize.endsWith("TB")) {
                factor = 0x0010_0000_0000L;
                dataSize = dataSize.substring(0, dataSize.length() - 3);
            } else if (dataSize.endsWith("PB")) {
                factor = 0x0004_0000_0000_0000L;
                dataSize = dataSize.substring(0, dataSize.length() - 3);
            } else if (dataSize.endsWith("EB")) {
                factor = 0x1000_0000_0000_0000L;
                dataSize = dataSize.substring(0, dataSize.length() - 3);
            */
        } else if (dataSize.endsWith("B")) {
            dataSize = dataSize.substring(0, dataSize.length() - 1);
        }

        return factor * Long.parseLong(dataSize);
    }


    /**
     * Turn the long <code>dataSize</code> value into a string with the largest possible unit that leads to the value
     * greater than or equal to 1. <code>standard</code> controls the base units used in conversion: SI -- 1000, IEC and
     * JEDEC -- 1024; <code>precision</code> tell how many precision points will be shown in the output string.
     * Note that for byte units no decimal point is generated irrespective of what precision is.
     *
     * @param dataSize the value to be converted to a data size string.
     * @param standard one of the <code>DataSize.Prefix</code> constants: IEC, JEDEC or SI.
     * @param precision the number of digits after decimal point.
     * @return the string representing the dataSize value according to the given standard and precision.
     */
    public static String toString(long dataSize, int standard, int precision)
    {
        switch (standard) {
            case Prefix.IEC:
                return unitsToString(dataSize, 1024, IEC_Units, precision);
            case Prefix.JEDEC:
                return unitsToString(dataSize, 1024, JEDEC_Units, precision);
            case Prefix.SI:
                return unitsToString(dataSize, 1000, SI_Units, precision);
        }

        throw new IllegalArgumentException("Unsupported data size format provided.");
    }

    /**
     * Turn the long <code>dataSize</code> value into a string with the largest possible unit that leads to the value
     * greater than or equal to 1. The format argument needs to include references to a double and string values, e.g.
     * <pre>"%f %s"</pre> may lead to <pre>"1.035729 MB"</pre>.
     *
     * @param dataSize the value to be converted to a data size string.
     * @param standard one of the DataSize.Prefix constants: IEC, JEDEC or SI.
     * @param format the format string used to produce the result string.
     * @return the string representing the dataSize value according to the given standard and format.
     */
    public static String toString(long dataSize, int standard, String format)
    {
        switch (standard) {
            case Prefix.IEC:
                return unitsToString(dataSize, 1024, IEC_Units, format);
            case Prefix.JEDEC:
                return unitsToString(dataSize, 1024, JEDEC_Units, format);
            case Prefix.SI:
                return unitsToString(dataSize, 1000, SI_Units, format);
        }

        throw new IllegalArgumentException("Unsupported data size format provided.");
    }

    private static final String[] IEC_Units = { "B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB" };
    private static final String[] SI_Units = { "B", "kB", "MB", "GB", "TB", "PB", "EB" };
    private static final String[] JEDEC_Units = { "B", "KB", "MB", "GB" };

    private static String unitsToString(long dataSize, int base, String[] units, int precision)
    {
        if (precision < 0) {
            throw new IllegalArgumentException("precision must be non-negative.");
        }

        long divisor = base;
        int i = 0;

        while (dataSize / divisor > 0 && i < units.length - 1) {
            divisor *= base;
            i++;
        }

        if (precision == 0 || i == 0) {
            return dataSize / (divisor / base) + " " + units[i];
        } else {
            return String.format("%." + precision + "f %s", (double)dataSize / (divisor / base), units[i]);
        }
    }

    private static String unitsToString(long dataSize, int base, String[] units, String format)
    {
        long divisor = base;
        int i = 0;

        while (dataSize / divisor > 0 && i < units.length - 1) {
            divisor *= base;
            i++;
        }

        return String.format(format, (double)dataSize / (divisor / base), units[i]);
    }
}
