package com.connexience.server.rest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents a Json Web Token (JWT) used to specify claims in Restful requests.
 * <p/>
 * The fields supported by this implementation are a subset of the latest draft of the JWT specification (draft 19),
 * found at http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-19
 * <p/>
 * TODO: Switch over to JsonObjectBuilder internally, when using Java EE 7
 *
 * @author Derek Mortimer
 */
public class JsonWebToken {
	public static final String JWT_ISSUER = "iss";

	public static final String JWT_SUBJECT = "sub";

	public static final String JWT_AUDIENCE = "aud";

	public static final String JWT_EXPIRATION = "exp";

	public static final String JWT_NOT_BEFORE = "nbf";

	public static final String JWT_ISSUED_AT = "iat";

	public static final String JWT_TOKEN_ID = "jti";

	public static final String JWT_HEADER_TYPE = "typ";

	public static final String JWT_HEADER_ALGORITHM = "alg";

	private SignatureAlgorithm algorithm;

	private JSONObject payload = new JSONObject();

	private JSONObject header = new JSONObject();

	public JsonWebToken(final SignatureAlgorithm algorithm) {
		this.algorithm = algorithm;

		// Setup header
		_jsonPut(header, JWT_HEADER_TYPE, "JWT");
		_jsonPut(header, JWT_HEADER_ALGORITHM, algorithm.name());

		// Default to issued at now
		_jsonPut(payload, JWT_ISSUED_AT, System.currentTimeMillis() / 1000);
	}

	protected JsonWebToken(final JSONObject header, final JSONObject payload) {
		this.header = header;
		this.payload = payload;
		setAlgorithm(SignatureAlgorithm.valueOf(header.optString(JWT_HEADER_ALGORITHM, "HS256")));
	}

	/**
	 * Get the algorithm that this token should be signed using
	 *
	 * @return An enum value indicating which Signature algorithm to use for signing.
	 */
	public SignatureAlgorithm getAlgorithm() {
		return algorithm;
	}

	/**
	 * Set the algorithm this token should be signed using
	 *
	 * @param algorithm An enum value indicating which Signature algorithm to use for signing.
	 */
	public void setAlgorithm(final SignatureAlgorithm algorithm) {
		this.algorithm = algorithm;
		_jsonPut(header, JWT_HEADER_ALGORITHM, algorithm.name());
	}

	/**
	 * Get the entity who issued this JWT
	 *
	 * @return A string containing the issuer.
	 */
	public String getIssuer() {
		return payload.optString(JWT_ISSUER);
	}

	/**
	 * Set the entity who issued this JWT
	 *
	 * @param issuer A string containing the issuer.
	 */
	public void setIssuer(final String issuer) {
		_jsonPut(payload, JWT_ISSUER, issuer);
	}

	/**
	 * Get the subject this JWT relates to
	 *
	 * @return A string containing the subject.
	 */
	public String getSubject() {
		return payload.optString(JWT_SUBJECT);
	}

	/**
	 * Set the subject this JWT relates to
	 *
	 * @param subject A string containing the subject.
	 */
	public void setSubject(final String subject) {
		_jsonPut(payload, JWT_SUBJECT, subject);
	}

	/**
	 * Get the intended audience for this JWT
	 *
	 * @return A string containing the audience.
	 */
	public String getAudience() {
		return payload.optString(JWT_AUDIENCE);
	}

	/**
	 * Set the intended audience for this JWT
	 *
	 * @param audience A string containing the audience.
	 */
	public void setAudience(final String audience) {
		_jsonPut(payload, JWT_AUDIENCE, audience);
	}

	/**
	 * Get the unique identifier for this JWT
	 *
	 * @return A string containing the identifier.
	 */
	public String getJwtId() {
		return payload.optString(JWT_TOKEN_ID);
	}

	/**
	 * Set the unique identifier for this JWT
	 *
	 * @param jwtId A string containing the identifier.
	 */
	public void setJwtId(final String jwtId) {
		_jsonPut(payload, JWT_TOKEN_ID, jwtId);
	}

	/**
	 * Get the expiration time for this JWT
	 *
	 * @return A long representing the expiration time, in seconds, since epoch.
	 */
	public Long getExpiration() {
		return payload.optLong(JWT_EXPIRATION);
	}

	/**
	 * Set the expiration time for this JWT
	 *
	 * @param expiration A long representing the expiration time, in seconds, since epoch.
	 */
	public void setExpiration(final Long expiration) {
		_jsonPut(payload, JWT_EXPIRATION, expiration);
	}

	/**
	 * Get the time this JWT was issued
	 *
	 * @return A long representing the issue time, in seconds, since epoch.
	 */
	public Long getIssuedAt() {
		return payload.optLong(JWT_ISSUED_AT);
	}

	/**
	 * Set the time this JWT was issued
	 *
	 * @param issuedAt A long representing the issue time, in seconds, since epoch.
	 */
	public void setIssuedAt(final Long issuedAt) {
		_jsonPut(payload, JWT_ISSUED_AT, issuedAt);
	}

	/**
	 * Get the time before which this JWT should <b>not</b> be accepted
	 *
	 * @return A long representing the time before which the token should <b>not</b> be accepted, in seconds, since epoch.
	 */
	public Long getNotBefore() {
		return payload.optLong(JWT_NOT_BEFORE);
	}

	/**
	 * Set the time before which this JWT should <b>not</b> be accepted
	 *
	 * @param notBefore A long representing the time before which the token should <b>not</b> be accepted, in seconds, since epoch.
	 */
	public void setNotBefore(final Long notBefore) {
		_jsonPut(payload, JWT_NOT_BEFORE, notBefore);
	}

	/**
	 * Get the type of token, this will always be "JWT"
	 *
	 * @return A string representing the type of token this is, always "JWT".
	 */
	public String getType() {
		return header.optString(JWT_HEADER_TYPE);
	}

	/**
	 * Get the underlying JSONObject representing the contents of the payload. This can be used to put your own private claims into the JWT.
	 *
	 * @return The underlying JSONObject representing the contents of this JWT's payload (i.e., the public and private claims)
	 */
	public JSONObject getPayload() {
		return payload;
	}

	/**
	 * Get the underlying JSONObject representing the contents of the header.
	 *
	 * @return The underlying JSONObject representing the contents of this JWT's header
	 */
	public JSONObject getHeader() {
		return header;
	}

	/**
	 * Convenience method to return the JSON representing the header of this JWT token in a string.
	 *
	 * @return A string containing the JSON representing the header of this JWT token.
	 */
	public String getHeaderJson() {
		return header.toString();
	}

	/**
	 * Convenience method to return the JSON representing the payload of this JWT token in a string.
	 *
	 * @return A string containing the JSON representing the payload of this JWT token.
	 */
	public String getPayloadJson() {
		return payload.toString();
	}

	/**
	 * Convenience method to set the value of a custom claim within the JWT.
	 *
	 * @param key   The property name (or key) of the claim
	 * @param value The value of the claim (expected to be a string, boolean or numeric value)
	 */
	public void setCustomClaim(final String key, final Object value) {
		_jsonPut(payload, key, value);
	}

	/**
	 * Convenience method to get the value of a custom claim within the JWT.
	 *
	 * @param key The property name (or key) of the claim
	 * @return The value of the claim (expected to be a string, boolean or numeric value)
	 */
	public Object getCustomClaim(final String key) {
		return payload.opt(key);
	}

	// Set a name value pair and deal with the JSONException
	private void _jsonPut(final JSONObject object, final String key, final Object value) {
		try {
			object.put(key, value);
		} catch (JSONException e) {
			throw new IllegalArgumentException("Invalid JSON in key or value: '" + e.getMessage() + "'", e);
		}
	}
}
