package com.connexience.server.rest;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;

public class SignedJsonWebToken {
	private final byte[] header;

	private final byte[] payload;

	private final byte[] signature;

	public SignedJsonWebToken(final byte[] header, final byte[] payload, final byte[] signature) {
		this.header = header;
		this.payload = payload;
		this.signature = signature;
	}

	public SignedJsonWebToken(final String signedJwt) {
		final String[] parts = signedJwt.split("\\.");

		if (parts.length != 3) {
			throw new IllegalStateException("Expected 3 parts in String encoded signed JWT token");
		}

		this.header = parts[0].getBytes();
		this.payload = parts[1].getBytes();
		this.signature = parts[2].getBytes();
	}

	public byte[] getHeader() {
		return header;
	}

	public byte[] getPayload() {
		return payload;
	}

	public byte[] getSignature() {
		return signature;
	}

	public String getEncodedHeader() {
		return new String(header);
	}

	public String getEncodedPayload() {
		return new String(payload);
	}

	public String getEncodedSignature() {
		return new String(signature);
	}

	public JSONObject getDecodedHeader() throws JSONException {
		return new JSONObject(new String(Base64.decodeBase64(getEncodedHeader())));
	}

	public JSONObject getDecodedPayload() throws JSONException {
		return new JSONObject(new String(Base64.decodeBase64(getEncodedPayload())));
	}

	public String toString() {
		return getEncodedHeader() + "." + getEncodedPayload() + "." + getEncodedSignature();
	}
}
