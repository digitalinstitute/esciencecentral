/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.flatstudy;

/**
 * Various constants for flat studies.
 * @author hugo
 */
public interface FlatStudyConstants {
    public static final String PERSON_MOVED_MESSAGE = "PERSONMOVED";
    public static final String PERSON_REMOVED_MESSAGE = "PERSONREMOVED";
    public static final String NEW_STUDY_ID_PROPERTY = "NEWSTUDYID";
    public static final String STUDY_ID_PROPERTY = "STUDYID";
    public static final String STUDY_CODE_PROPERTY = "STUDYCODE";
    public static final String OBJECT_ID_PROPERTY = "OBJECTID";
    public static final String OBJECT_EXTERNAL_ID_PROPERTY = "OBJECTEXTERNALID";
}