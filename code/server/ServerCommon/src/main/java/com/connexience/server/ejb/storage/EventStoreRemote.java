package com.connexience.server.ejb.storage;

import com.connexience.api.model.EscEvent;
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.security.Ticket;
import java.util.Date;
import java.util.List;
import javax.ejb.Remote;

/**
 * Provide access to stored event data
 * @author hugo
 */
@Remote
public interface EventStoreRemote {
    public abstract void pushEvent(Ticket ticket, String deviceId, String studyCode, EscEvent event) throws ConnexienceException;
    public abstract void pushEvents(Ticket ticket, String deviceId, String studyCode, List<EscEvent> events) throws ConnexienceException;
    public abstract List<EscEvent> query(Ticket ticket, String deviceId, String studyCode, Date startDate, Date endDate, int maxEvents) throws ConnexienceException;
    public abstract List<EscEvent> query(Ticket ticket, String deviceId, String studyCode, String eventType, Date startDate, Date endDate, int maxEvents) throws ConnexienceException;
    public abstract EscEvent getMostRecent(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException;
    public abstract EscEvent getMostRecent(Ticket ticket, String deviceId, String studyCode, String eventType) throws ConnexienceException;    
    public abstract List<String> listEventTypes(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException;
    public abstract List<String> listEventTypes(Ticket ticket, String studyCode) throws ConnexienceException;
    public abstract void dropEvents(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException;
    public abstract void dropEvents(Ticket ticket, String deviceId, String studyCode, String eventType) throws ConnexienceException;
    public abstract void resetAllEventStores(Ticket ticket) throws ConnexienceException;
    public abstract void resetStudyEventStore(Ticket ticket, String studyCode) throws ConnexienceException;
    public abstract boolean eventStorePresent(Ticket ticket, String studyCode) throws ConnexienceException;
    public abstract void createEventStore(Ticket ticket, String studyCode) throws ConnexienceException;
}