package com.connexience.server.model.project.study;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * User: nsjw7
 * Date: 31/05/2016
 * Time: 11:16
 */

@Entity
@Table(name = "hubtypes")
@NamedQueries({
        @NamedQuery(name = "HubType.countHubTypes", query = "SELECT count(h) FROM HubType h"),
        @NamedQuery(name = "HubType.findByName", query = "SELECT h FROM HubType h WHERE h.name = :name")
})
public class HubType implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;

    private String name;

    private String dataFolderId;

    @OneToMany(mappedBy = "hubType", cascade = CascadeType.REMOVE)
    private List<Hub> instances = new ArrayList<Hub>();

    public HubType() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public List<Hub> getInstances() {
        return instances;
    }

    public void setInstances(List<Hub> instances) {
        this.instances = instances;
    }

    public void addInstance(Hub instance){
        this.instances.add(instance);
        instance.setHubType(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HubType)) return false;

        HubType hubType = (HubType) o;

        return id != null ? id.equals(hubType.id) : hubType.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HubType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
