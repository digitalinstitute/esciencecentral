package com.connexience.server.model.project.study;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * User: nsjw7
 * Date: 31/05/2016
 * Time: 11:16
 */

@Entity
@Table(name = "hubdeployments")
@NamedQueries({
        @NamedQuery(name = "HubDeployment.countDeployments", query = "SELECT count(hd) FROM HubDeployment hd")
})
public class HubDeployment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;

    @ManyToOne
    private SubjectGroup subjectGroup;

    @ManyToOne
    private Hub hub;

    private Date startDate;

    private Date endDate;

    private boolean active = true;

    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, String> additionalProperties = new HashMap<>();


    public HubDeployment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SubjectGroup getSubjectGroup() {
        return subjectGroup;
    }

    public void setSubjectGroup(final SubjectGroup subjectGroup) {
        this.subjectGroup = subjectGroup;
        subjectGroup.getHubDeployments().add(this);
    }

    public Hub getHub() {
        return hub;
    }

    public void setHub(Hub hub) {
        this.hub = hub;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return active;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HubDeployment)) return false;

        HubDeployment that = (HubDeployment) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "HubDeployment{" +
                "id=" + id +
                ", subjectGroup=" + subjectGroup +
                ", hub.serialNumber=" + hub.getSerialNumber() +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
