/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class contains utilities for signing and verifying objects and data files
 * @author nhgh
 */
public abstract class SignatureUtils {

    public static String getHashedPassword(String plainPassword) {
        try {
            //compute the SHA1 hash of the password
            MessageDigest msgDigest = MessageDigest.getInstance("SHA-1");
            msgDigest.update(plainPassword.getBytes());
            byte rawByte[] = msgDigest.digest();
            return HexUtils.getHexString(rawByte);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static String getFileHash(File file, String salt) throws Exception {
        MessageDigest msgDigest = MessageDigest.getInstance("SHA-1");
        FileInputStream inStream = new FileInputStream(file);
        byte[] buffer = new byte[32768];
        int len;
        while ((len = inStream.read(buffer)) != -1) {
            msgDigest.update(buffer, 0, len);
        }
        msgDigest.update(salt.getBytes());
        return HexUtils.getHexString(msgDigest.digest());
    }

    public static boolean validateFileHash(File file, String hash, String salt) throws Exception {
        String calculatedHash = getFileHash(file, salt);
        return calculatedHash.equals(hash);
    }


    public static boolean isValidSHA1(String s) {
        return s.matches("[a-fA-F0-9]{40}");
    }

    /**
     * This method checks to see if the name of a property passed in appears to contain a password.
     * If the name contains "password" and "hash" then the value returned will be a SHA1 hash of the value.
     *
     * Values that are "valid" SHA1 hashes are not affected.  A SHA1 hash is 40 hex chars.
     * @param name name of the property
     * @param value value of the property
     * @return the value parameter if the name doesn't contain hash and password.  Otherwise a SHA1 hash of the value.
     */
    public static String hashPropertyIfPasswordField(String name, String value) {
        //check the key
        if (name.toLowerCase().contains("password") && name.toLowerCase().contains("hash")) {

            //don't re-sign valid SHA1 hashes
            if (!SignatureUtils.isValidSHA1(value)) {
                //update the property
                return getHashedPassword(value);
            }
        }
        return value;
    }
}