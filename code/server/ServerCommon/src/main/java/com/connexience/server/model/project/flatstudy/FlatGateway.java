/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.flatstudy;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * Representation of a gateway device in a flat study
 * @author hugo
 */
@Entity
@Table(name = "flatgateways")
@NamedQueries({
    @NamedQuery(name = "FlatGateway.countForStudy", query = "SELECT COUNT(*) FROM FlatGateway g WHERE g.studyId=:studyId"),
    @NamedQuery(name = "FlatGateway.countMatchingForStudy", query = "SELECT COUNT(*) FROM FlatGateway g WHERE g.studyId=:studyId AND (g.name LIKE :search OR g.externalId LIKE :search OR g.gatewayType LIKE :search)"),
    @NamedQuery(name = "FlatGateway.searchForStudy", query = "SELECT g FROM FlatGateway g WHERE g.studyId=:studyId AND (g.name LIKE :search OR g.externalId LIKE :search OR g.gatewayType LIKE :search)"),    
    @NamedQuery(name = "FlatGateway.listForStudy", query = "SELECT g FROM FlatGateway g WHERE g.studyId=:studyId ORDER BY g.creationDate DESC"),
    @NamedQuery(name = "FlatGateway.deleteForStudy", query = "DELETE FlatGateway g WHERE g.studyId=:studyId"),
    @NamedQuery(name = "FlatGateway.deleteById", query = "DELETE FlatGateway g WHERE g.id=:id"),
    @NamedQuery(name = "FlatGateway.findByExternalId", query = "SELECT g FROM FlatGateway g WHERE g.externalId=:externalId AND g.studyId=:studyId"),
    @NamedQuery(name = "FlatGateway.getFolderId", query = "SELECT g.folderId FROM FlatGateway g WHERE g.id=:id"),
    @NamedQuery(name = "FlatGateway.getCodeFolderId", query = "SELECT g.codeFolderId FROM FlatGateway g WHERE g.id=:id"),
    @NamedQuery(name = "FlatGateway.getUploadsFolderId", query = "SELECT g.uploadsFolderId FROM FlatGateway g WHERE g.id=:id"),
    @NamedQuery(name = "FlatGateway.getDownloadsFolderId", query = "SELECT g.downloadsFolderId FROM FlatGateway g WHERE g.id=:id")
})
public class FlatGateway implements Serializable, FlatStudyObject {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;   
    
    @Basic @Column(name="studyId") private Integer studyId;
    @Temporal(TemporalType.TIMESTAMP) @Column(name="creationDate") private Date creationDate = new Date();
    @Basic @Column(name="folderId") private String folderId;        
    @Basic @Column(name="externalId") private String externalId;
    @Basic @Column(name="name") private String name;
    @Basic @Column(name="devicePassword") private String devicePassword;
    @Basic @Column(name="gatewayType") private String gatewayType = "default";
    @Basic @Column(name="lastRegistrationTimestamp") private long lastRegistrationTimestamp = 0L;
    @Basic @Column(name="codeFolderId") private String codeFolderId;
    @Basic @Column(name="uploadsFolderId") private String uploadsFolderId;
    @Basic @Column(name="downloadsFolderId") private String downloadsFolderId;
    
    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> additionalProperties = new HashMap<>();
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodeFolderId() {
        return codeFolderId;
    }

    public void setCodeFolderId(String codeFolderId) {
        this.codeFolderId = codeFolderId;
    }

    public String getDownloadsFolderId() {
        return downloadsFolderId;
    }

    public void setDownloadsFolderId(String downloadsFolderId) {
        this.downloadsFolderId = downloadsFolderId;
    }

    public String getUploadsFolderId() {
        return uploadsFolderId;
    }

    public void setUploadsFolderId(String uploadsFolderId) {
        this.uploadsFolderId = uploadsFolderId;
    }

    public long getLastRegistrationTimestamp() {
        return lastRegistrationTimestamp;
    }

    public void setLastRegistrationTimestamp(long lastRegistrationTimestamp) {
        this.lastRegistrationTimestamp = lastRegistrationTimestamp;
    }

    public Integer getStudyId() {
        return studyId;
    }

    public void setStudyId(Integer studyId) {
        this.studyId = studyId;
    }    
    
    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }    

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public HashMap<String, String> getAdditionalDisplayFields() {
        return new HashMap<>();
    }
    
    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }    

    public String getDevicePassword() {
        return devicePassword;
    }

    public void setDevicePassword(String devicePassword) {
        this.devicePassword = devicePassword;
    }

    public String getGatewayType() {
        return gatewayType;
    }

    public void setGatewayType(String gatewayType) {
        this.gatewayType = gatewayType;
    }
}