package com.connexience.server;

/**
 * Created by Jacek on 14/02/2017.
 */
public class ConnexienceClientException extends ConnexienceException
{
    public ConnexienceClientException(String msg)
    {
        super(msg);
    }


    public ConnexienceClientException(Throwable t)
    {
        super(t);
    }


    public ConnexienceClientException(String message, Throwable t)
    {
        super(message, t);
    }


    public ConnexienceClientException(String messageFormat, Object... args)
    {
        super(messageFormat, args);
    }


    public ConnexienceClientException(String messageFormat, Throwable t, Object... args)
    {
        super(messageFormat, t, args);
    }
}
