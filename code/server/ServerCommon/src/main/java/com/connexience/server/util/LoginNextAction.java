package com.connexience.server.util;

/**
 * User: nsjw7
 * Date: 02/04/15
 * Time: 12:53
 */
public enum LoginNextAction {
    NOTHING,
    RESET_PASSWORD
}
