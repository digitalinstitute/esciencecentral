package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Ticket;

import javax.ejb.Remote;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Remote
@SuppressWarnings("unused")
public interface HubRemote {

    Hub saveHub(Ticket ticket, Hub hub);

    Hub getHub(Ticket ticket, int hubId) throws ConnexienceException;

    void deleteHub(Ticket ticket, Hub hub) throws ConnexienceException;

    HubType createHubType(Ticket ticket, String name) throws ConnexienceException;

    HubType getHubType(Ticket ticket, int id);

    long countHubTypes(Ticket ticket);

    List<HubType> getHubTypes(Ticket ticket, final int start, final int maxResults);

    HubType updateHubType(Ticket ticket, HubType hubType) throws ConnexienceException;

    void deleteHubtype(Ticket ticket, HubType hubType) throws ConnexienceException;

    Hub createHub(Ticket ticket, HubType hubType, String serialNumber) throws ConnexienceException;

    long countHubs(Ticket ticket);

    List<Hub> getHubs(Ticket ticket, final int start, final int maxResults);

    Hub updateHub(Ticket ticket, Hub hub) throws ConnexienceException;

    void deleteHubDeployment(Ticket ticket, HubDeployment hubDeployment) throws ConnexienceException;

    HubDeployment updateHubDeployment(Ticket ticket, HubDeployment hubDeployment);

    List<HubDeployment> getHubDeployments(Ticket ticket, final int start, final int maxResults);

    long countHubDeployments(Ticket ticket);

    HubDeployment getDeployment(Ticket ticket, int deploymentId) throws ConnexienceException;

    /*************************/

    HubDeployment deployHub(Ticket ticket, Hub hub, SubjectGroup sg, Date startDate, Date endDate) throws ConnexienceException;

    void undeployHub(Ticket ticket, Hub hub, SubjectGroup sg);

    List<HubDeployment> getInactiveHubDeployments(Ticket ticket, SubjectGroup sg, final int start, final int maxResults);

    List<HubDeployment> getActiveHubDeployments(Ticket ticket, SubjectGroup sg, final int start, final int maxResults);

    HubType getHubType(Ticket ticket, String name);
}
