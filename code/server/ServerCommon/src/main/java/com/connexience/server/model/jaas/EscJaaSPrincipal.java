/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.jaas;

import org.jboss.security.SimplePrincipal;


/**
 * JAAS principal that extends the basic JBoss principal with details as to whether
 * the user is an e-SC user or an Upload user
 * @author hugo
 */
public class EscJaaSPrincipal extends SimplePrincipal {
    public enum UserType {
        ESC_USER, UPLOAD_USER, UNKNOWN, NO_USER
    }
    private UserType userType;
    
    public EscJaaSPrincipal(String name, UserType userType) {
        super(name);
        this.userType = userType;
    }

    public UserType getUserType() {
        return userType;
    }
}