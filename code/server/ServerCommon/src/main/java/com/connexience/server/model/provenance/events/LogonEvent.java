/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.provenance.events;

import java.util.Date;

/**
 * This event is triggered when a user logs on.
 *
 * Note that it doesn't store the UserId as this is not always available.  Specifically it's not available in the Logon Realm used by the Basic Auth API.
 *
 * @author simon
 */
public class LogonEvent extends GraphOperation {

    private static final long serialVersionUID = 1L;

    public static final String WEBSITE_LOGON = "WEBSITE_LOGON";

    public static final String API_LOGON = "API_LOGON";

    public static final String REMEMBERED_LOGON = "REMEMBERED_LOGON";

    public static final String IMPERSONATE_LOGON = "IMPERSONATE_LOGON";

    public LogonEvent() {
    }

    /**
     * Is the logon via the API or the website?
     */
    private String logonType = "";

    /**
     * IP address the logon came from
     */
    private String ipAddress = "";

    /**
     * Username of the user logging on
     */
    private String username = "";


    public LogonEvent(String logonType, String username, String ipAddress) {
        this.logonType = logonType;
        this.username = username;
        this.ipAddress = ipAddress;
        this.setTimestamp(new Date());
    }

    public String getLogonType() {
        return logonType;
    }

    public void setLogonType(String logonType) {
        this.logonType = logonType;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}