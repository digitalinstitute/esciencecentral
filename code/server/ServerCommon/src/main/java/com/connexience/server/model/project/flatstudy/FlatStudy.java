/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.flatstudy;

import com.connexience.server.model.project.Project;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * This class provides a basic study that does not contain a subject heirarchy
 * @author hugo
 */
@Entity
@DiscriminatorValue("FLATSTUDY")
public class FlatStudy extends Project {
    public static final String CODE_FOLDER_NAME = "code";
    public static final String DEFAULT_CODE_FOLDER_NAME = "default";
    public static final String DATAFLOWS_FOLDER_NAME = "dataflows";
    
    @Basic @Column(name = "codeFolderId")private String codeFolderId;

    public String getCodeFolderId() {
        return codeFolderId;
    }

    public void setCodeFolderId(String codeFolderId) {
        this.codeFolderId = codeFolderId;
    }    
}