package com.connexience.server.model.project.flatstudy;

import java.util.Date;
import java.util.HashMap;

/**
 * Common interface implemented by flat study objects
 * @author hugo
 */
public interface FlatStudyObject {
    public Date getCreationDate();
    public String getExternalId();
    public HashMap<String, String> getAdditionalDisplayFields();
    public Integer getId();
    public String getFolderId();
}