/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.workflow;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class describes a parameter that can be set on an externally callable
 * workflow. It contains descriptions of its name, type, default value, any
 * help text and list of options if any are set.
 * @author hugo
 */
public class CallableWorkflowParameterDescription implements Serializable {
    private String name;
    private String description;
    private String defaultValue;
    private Class parameterClass;
    private String[] options = new String[0];
    private boolean optionListPresent = false;
    private boolean complexParameter = false;

    public CallableWorkflowParameterDescription() {
    }

    public Class getParameterClass() {
        return parameterClass;
    }

    public void setParameterClass(Class parameterClass) {
        this.parameterClass = parameterClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String[] getOptions() {
        return options;
    }

    public boolean isComplexParameter() {
        return complexParameter;
    }

    public boolean isOptionListPresent() {
        return optionListPresent;
    }

    public void setComplexParameter(boolean complexParameter) {
        this.complexParameter = complexParameter;
    }

    public void setOptionListPresent(boolean optionListPresent) {
        this.optionListPresent = optionListPresent;
    }

    public void setOptions(String[] options) {
        this.options = options;
    }
}