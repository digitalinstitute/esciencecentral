/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.study;

import com.connexience.server.util.SignatureUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: nsjw7 Date: 03/06/2013 Time: 09:35
 */
@Entity
@Table(name = "loggers")
public class Logger implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;

    @Column(unique = true)
    private String serialNumber;

    private String location;

    @ManyToOne(fetch = FetchType.LAZY)
    private LoggerType loggerType;

    // remove any deployment if this logger is deleted
    @OneToMany(mappedBy = "logger", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<LoggerDeployment> loggerDeployments = new ArrayList<>();

    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, String> additionalProperties = new HashMap<>();

    protected Logger() {
    }

    public Logger(final String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public LoggerType getLoggerType() {
        return loggerType;
    }

    public void setLoggerType(final LoggerType loggerType) {
        // unassociate, if required
        if (this.loggerType != null && this.loggerType.getLoggers().contains(this)) {
            this.loggerType.getLoggers().remove(this);
        }

        this.loggerType = loggerType;

        // add this sensor to the new loggerType
        if ((loggerType != null) && !loggerType.getLoggers().contains(this)) {
            loggerType.getLoggers().add(this);
        }
    }

    public List<LoggerDeployment> getLoggerDeployments() {
        return loggerDeployments;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Logger)) {
            return false;
        }

        final Logger logger = (Logger) o;

        if (id != null ? !id.equals(logger.id) : logger.id != null) {
            return false;
        }
        if (location != null ? !location.equals(logger.location) : logger.location != null) {
            return false;
        }
        if (!serialNumber.equals(logger.serialNumber)) {
            return false;
        }

        return true;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        //hash passwords if they contain both "password" and "hash"
        for (String propName : additionalProperties.keySet()) {

            //Hash any fields that look like passwords.
            String value = SignatureUtils.hashPropertyIfPasswordField(propName, additionalProperties.get(propName));
            additionalProperties.put(propName, value);
        }
        this.additionalProperties = additionalProperties;
    }

    public String getAdditionalProperty(String key) {
        return additionalProperties.get(key);
    }

    public String putAdditionalProperty(String key, String value) {
        //Hash any fields that look like passwords.
        value = SignatureUtils.hashPropertyIfPasswordField(key, value);
        return additionalProperties.put(key, value);
    }

    public String removeAdditionalProperty(String key) {
        return additionalProperties.remove(key);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + serialNumber.hashCode();
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("serialNumber", serialNumber)
                .append("location", location)
                .append("loggerType", loggerType)
                        //			.append("loggerDeployment", loggerDeployment)
                .toString();
    }
}
