/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.project.flatstudy.FlatDevice;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatPerson;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.Ticket;
import java.util.HashMap;
import java.util.List;
import javax.ejb.Remote;

/**
 * This interface defines the behaviour of the flat study management bean
 * @author hugo
 */
@Remote
@SuppressWarnings("unused")
public interface FlatStudyRemote {
    /** Get a flat study by ID */
    public FlatStudy getFlatStudy(Ticket ticket, Integer studyId) throws ConnexienceException;
    
    /** Create a new flat study */
    public FlatStudy saveFlatStudy(Ticket ticket, FlatStudy study) throws ConnexienceException;
    
    /** Save a flat device */
    public FlatDevice saveFlatDevice(Ticket ticket, FlatDevice device) throws ConnexienceException;
    
    /** Check that a flat study has the correct folders */
    public void checkFlatStudyCodeFolders(Ticket ticket, Integer studyId) throws ConnexienceException;
    
    /** Check that a flat study has the correct properties assigned to it */
    public void checkFlatStudyProperties(Ticket ticket, Integer studyId) throws ConnexienceException;
    
    /** Create a flat device */
    public FlatDevice createFlatDevice(Ticket ticket, Integer studyId, String externalDeviceId) throws ConnexienceException;
    
    /** Remove a flat device */
    public void removeFlatDevice(Ticket ticket, Integer deviceId) throws ConnexienceException;
    
    /** Move a flat device to a different study */
    public void moveFlatDevice(Ticket ticket, Integer deviceId, Integer targetStudyId) throws ConnexienceException;
    
    /** Remove a flat study */
    public void removeFlatStudy(Ticket ticket, Integer studyId, boolean removeStorage) throws ConnexienceException;
    
    /** Get a flat device by ID */
    public FlatDevice getFlatDevice(Ticket ticket, Integer deviceId) throws ConnexienceException;
    
    /** Get a flat device by external ID */
    public FlatDevice getFlatDeviceByExternalId(Ticket ticket, Integer studyId, String externalDeviceId) throws ConnexienceException;
    
    /** Save a flat person object */
    public FlatPerson saveFlatPerson(Ticket ticket, FlatPerson person) throws ConnexienceException;
    
    /** Create a new flat person object as part of a flat study */
    public FlatPerson createFlatPerson(Ticket ticket, Integer studyId, String externalPersonId) throws ConnexienceException;
    
    /** Remove a flat person */
    public void removeFlatPerson(Ticket ticket, Integer personId) throws ConnexienceException;
    
    /** Move a flat person and all of their data to a different study */
    public void moveFlatPerson(Ticket ticket, Integer personId, Integer targetStudyId) throws ConnexienceException;
    
    /** Get a flat person */
    public FlatPerson getFlatPerson(Ticket ticket, Integer personId) throws ConnexienceException;
    
    /** Get a flat person by external ID */
    public FlatPerson getFlatPersonByExternalId(Ticket ticket, Integer studyId, String externalPersonId) throws ConnexienceException;
    
    /** Get a flat person by just the external ID */
    public FlatPerson getFlatPersonByExternalIdOnly(Ticket ticket, String externalPersonId) throws ConnexienceException;
    
    /** Find a flat person using a document record ID. This looks up the container folder and then maps it to a flat person */
    public FlatPerson getFlatPersonUsingDocumentId(Ticket ticket, String documentId) throws ConnexienceException;
    
    /** Create a flat gateway */
    public FlatGateway createFlatGateway(Ticket ticket, Integer studyId, String externalGatewayId) throws ConnexienceException;
    
    /** Save a flat gateway */
    public FlatGateway saveFlatGateway(Ticket ticket, FlatGateway gateway) throws ConnexienceException;
    
    /** Remove a flat gateway */
    public void removeFlatGateway(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Move a flat gateway to a different stydy */
    public void moveFlatGateway(Ticket ticket, Integer gatewayId, Integer targetStudyId) throws ConnexienceException;
    
    /** Get a flat gateway */
    public FlatGateway getFlatGateway(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Get a flat gateway by external ID */
    public FlatGateway getFlatGatewayByExternalId(Ticket ticket, Integer studyId, String externalGatewayId) throws ConnexienceException;
    
    /** Get a flat gateway by external ID in a study */
    public FlatGateway getFlatGatewayByExternalIdAndStudyCode(Ticket ticket, String studyCode, String externalGatewayId) throws ConnexienceException;
    
    /** Get the ID of a flat gateways data folder */
    public String getFlatGatewayFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Get the uploads folder ID of a flat gateway */
    public String getFlatGatewayUploadsFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Get the downloads folder ID of a flat gateway */
    public String getFlatGatewayDownloadsFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Get the code folder ID of a flat gateway */
    public String getFlatGatewayCodeFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Check that the correct folders exist for a flat gateway */
    public void checkFlatGatewayFolders(Ticket ticket, Integer gatewayId) throws ConnexienceException;
    
    /** Get the number of a specific type of object in a study */
    public long getObjectCount(Ticket ticket, Integer studyId, Class objectClass) throws ConnexienceException;
    
    /** Get a set of flat study objects */
    public List getObjects(Ticket ticket, Integer studyId, Class objectClass, int startPosition, int pageSize) throws ConnexienceException;
    
    /** Get the number of objects that match a search parameter */
    public long getObjectCount(Ticket ticket, Integer studyId, Class objectClass, String searchText) throws ConnexienceException;
    
    /** Get a set of flat study objects that match a search parameter */
    public List getObjects(Ticket ticket, Integer studyId, Class objectClass, String searchText, int startPosition, int pageSize) throws ConnexienceException;
    
    /** Send an event describing some flat study action */
    public void sendFlatStudyEventMessage(Ticket ticket, String message, HashMap<String, Object> extraParameters) throws ConnexienceException;
}
