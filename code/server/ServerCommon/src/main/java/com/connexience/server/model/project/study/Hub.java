package com.connexience.server.model.project.study;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: nsjw7
 * Date: 31/05/2016
 * Time: 11:16
 */

@Entity
@Table(name = "hubs")
@NamedQueries({
        @NamedQuery(name = "Hub.findBySerialNumber", query = "SELECT h FROM Hub h WHERE h.serialNumber= :serialNumber"),
        @NamedQuery(name = "Hub.countHubs", query = "SELECT count(h) FROM Hub h")
})
public class Hub implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;

    private String serialNumber;

    private String dataFolderId;

    @ManyToOne(fetch = FetchType.LAZY)
    private HubType hubType;

    @OneToMany(mappedBy = "hub", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<HubDeployment> deployments = new ArrayList<>();

    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, String> additionalProperties = new HashMap<>();

    public Hub() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public List<HubDeployment> getDeployments() {
        return deployments;
    }

    public void setDeployments(List<HubDeployment> deployments) {
        this.deployments = deployments;
    }

    public HubType getHubType() {
        return hubType;
    }

    public void setHubType(HubType hubType) {
        this.hubType = hubType;
    }

    public void addDeployment(HubDeployment deployment){
        this.deployments.add(deployment);
        deployment.setHub(this);
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hub)) return false;

        Hub hub = (Hub) o;

        return id != null ? id.equals(hub.id) : hub.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Hub{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }
}
