/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Found
 */
package com.connexience.server.util;

import com.connexience.server.ConnexienceException;

/**
 * This class can check password acceptability using the security preferences.
 * @author hugo
 */
public class BasicPasswordChecker implements IPasswordChecker  {

    public enum AcceptabilityStatus {
        ACCEPTABLE,
        FAILED_RULES,
        IN_HISTORY
    }

    protected String proposedPassword;
    protected String userId;
    protected String rejectionMessage = "";

    public BasicPasswordChecker() {
    }

    public BasicPasswordChecker(String userId, String proposedPassword) {
        this.proposedPassword = proposedPassword;
        this.userId = userId;
    }

    public String getProposedPassword() {
        return proposedPassword;
    }

    @Override
    public void setProposedPassword(String proposedPassword) {
        this.proposedPassword = proposedPassword;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRejectionMessage() {
        return rejectionMessage;
    }

    public boolean isAcceptable() throws ConnexienceException{
        System.out.println("BasicPasswordChecker.isAcceptable");
        if(userId == null || proposedPassword == null)
        {
            throw new ConnexienceException("No username or password in password strength checker");
        }

        return true;
    }

    public boolean isHistoryCheckSupported() {
        return false;
    }
}