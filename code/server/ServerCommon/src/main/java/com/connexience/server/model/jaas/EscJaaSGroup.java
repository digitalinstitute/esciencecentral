/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.jaas;

import org.jboss.security.SimpleGroup;

/**
 * Wrapper around a JBoss JAAS group for use by the combined login module
 * @author hugo
 */
public class EscJaaSGroup extends SimpleGroup {
    public enum GroupType {
        USERS, UPLOAD_USERS
    }
    private GroupType groupType;
    
    public EscJaaSGroup(String name, GroupType groupType) {
        super(name);
        this.groupType = groupType;
    }

    public GroupType getGroupType() {
        return groupType;
    }
}