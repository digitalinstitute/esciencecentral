/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.types;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * Base class for study objects
 * @author hugo
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class WorkflowStudyObject implements XmlStorable, Serializable {
    private int id = 0;
    private int studyId = 0;
    private int parentId = 0;
    
    private HashMap<String, String> additionalProperties = new HashMap<>();
    
    public WorkflowStudyObject() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudyId() {
        return studyId;
    }

    public void setStudyId(int studyId) {
        this.studyId = studyId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public HashMap<String, String> getAdditionalProperties() {
        return additionalProperties;
    }
    
    public void copyInAddtionalProperties(Map<String,String> properties){
        Iterator<String> i = properties.keySet().iterator();
        String name;
        while(i.hasNext()){
            name = i.next();
            additionalProperties.put(name, properties.get(name));
        }
    }
    
    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("WorkflowStudyObject");
        store.add("ID", id);
        store.add("StudyID", studyId);
        store.add("ParentID", parentId);
        
        // Add the properties
        store.add("PropertyCount", additionalProperties.size());
        Iterator<String> i = additionalProperties.keySet().iterator();
        int count = 0;
        String name, value;
        while(i.hasNext()){
            name = i.next();
            value = additionalProperties.get(name);
            store.add("Property" + count + "Name", name);
            store.add("Property" + count + "Value", value);
            count++;
        }
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        id = store.intValue("ID", 0);
        studyId = store.intValue("StudyID", 0);
        parentId = store.intValue("ParentID", 0);
        int count  = store.intValue("PropertyCount", 0);
        String name, value;
        additionalProperties.clear();
        for(int i=0;i<count;i++){
            name = store.stringValue("Property" + i + "Name", null);
            value = store.stringValue("Property" + i + "Value", null);
            if(name!=null && value!=null){
                additionalProperties.put(name, value);
            }
        }
    }
}