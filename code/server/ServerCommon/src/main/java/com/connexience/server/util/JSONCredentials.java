/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import com.connexience.server.model.security.StoredCredentials;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

public class JSONCredentials extends JSONObject {

    public JSONCredentials() {
        try {put("_type", "Credentials");}catch(Exception e){}
        try {put("name", "");}catch(Exception e){}
        try {put("id", "");}catch(Exception e){}
        try {put("value", "");}catch(Exception e){}
    }

    public JSONCredentials(JSONTokener x) throws JSONException {
        super(x);
    }

    public JSONCredentials(Map map) {
        super(map);
    }

    public JSONCredentials(Object bean) {
        super(bean);
    }

    public JSONCredentials(String source) throws JSONException {
        super(source);
    }

    public JSONCredentials(JSONObject jo, String[] names) throws JSONException {
        super(jo, names);
    }

    public JSONCredentials(Object object, String[] names) {
        super(object, names);
    }

    public JSONCredentials(StoredCredentials obj) throws JSONException {
        put("_type", "Credentials");
        put("className", obj.getClass().getName());
        put("id", obj.getId());
        put("name", obj.getName());
        put("value", obj.getName());
    }

    public int getCredentialsId() throws JSONException {
        return getInt("id");
    }

    public String getCredentialsName() throws JSONException {
        return getString("name");
    }

    public JSONCredentials(String className, int id, String name) throws JSONException {
        put("_type", "Credentials");
        put("className", className);
        put("id", id);
        put("name", name);
        put("value", name);        
    }


    public boolean isCredentials(JSONObject obj){
        try {
            if(obj.has("_type") && obj.getString("_type").equals("Credentials")){
                return true;
            } else {
                return false;
            }
        } catch (Exception e){
            return false;
        }
    }
}