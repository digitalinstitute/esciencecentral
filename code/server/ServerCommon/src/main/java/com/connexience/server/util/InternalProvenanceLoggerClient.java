/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

import com.connexience.server.ejb.provenance.IProvenanceLogger;
import com.connexience.server.model.provenance.events.GraphOperation;
import org.jboss.logging.Logger;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;

/**
 * Client to send a JMS Message to the provenance server to log an operation without going via the server
 * User: nsjw7
 * Date: Mar 15, 2011
 * Time: 4:02:04 PM
 */
public class InternalProvenanceLoggerClient implements IProvenanceLogger {
    static Logger logger = Logger.getLogger(InternalProvenanceLoggerClient.class.getName());

    private static boolean enabled = false;

    public InternalProvenanceLoggerClient() {
        enabled = PreferenceManager.getSystemPropertyGroup("Provenance").booleanValue("Enabled", true);
    }

    /**
     * Create the default properties for the provenance logger client
     */
    public static void createDefaultProperties() {
        PreferenceManager.getSystemPropertyGroup("Provenance").add("Enabled", true, "Should JMS Messages be sent to the provenance server");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSUser", "connexience", "Username of the JMS Destination");
        PreferenceManager.getSystemPropertyGroup("Provenance").add("JMSPassword", "1234", "Password for the JMS Destination");
    }

    /**
     * Log a graph operation
     *
     * @param operation operation to be logged
     */
    public void log(GraphOperation operation) {
        try {
            if (InternalProvenanceLoggerClient.enabled) {
                sendMessage("ProvenanceEvents", operation); //ProvenanceQueue = queue name
            }
        } catch (Exception e) {
            logger.fatal("Cannot send message to provenance service");
        }
    }

    /**
     * Send the message
     *
     * @param destName queuename
     * @param message   graph operation
     * @throws Exception something went wrong
     */
    private void sendMessage(String destName, GraphOperation message) throws Exception {

        Connection connection = null;
        try {

            String jMSUser = PreferenceManager.getSystemPropertyGroup("Provenance").stringValue("JMSUser", "connexience");
            String jMSPassword = PreferenceManager.getSystemPropertyGroup("Provenance").stringValue("JMSPassword", "1234");

            ConnectionFactory connectionFactory;
            Destination destination;

            //We are internal to the server so look up the internal Connection Factory and Queue
            Context context = new InitialContext();
            connectionFactory = (ConnectionFactory) context.lookup("ConnectionFactory");
            destination = (Destination) context.lookup("java:jboss/exported/" + destName);

            connection = connectionFactory.createConnection(jMSUser, jMSPassword);
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.PERSISTENT);

            BytesMessage bm = session.createBytesMessage();
            byte[] data = SerializationUtils.serialize(message);
            bm.writeBytes(data);
            producer.send(bm);


        } catch (Exception e) {
            logger.info("Cannot send provenance message: " + e.getMessage() + " - disabling provenance until next restart");
            InternalProvenanceLoggerClient.enabled = false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

    }


}
