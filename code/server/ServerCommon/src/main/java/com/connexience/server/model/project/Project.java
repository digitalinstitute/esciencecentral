/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.io.Serializable;
import java.util.*;

@Entity
@Inheritance
@DiscriminatorColumn(name = "projecttype")
@DiscriminatorValue("PROJECT")
@Table(name = "projects")
@NamedQueries({
        @NamedQuery(name = "Project.findByExternalId", query = "SELECT p FROM Project p WHERE p.externalId=:externalId"),
    
        // All projects whose visibility is public
        @NamedQuery(name = "Project.public", query = "SELECT p FROM Project p WHERE p.privateProject = false"),
        @NamedQuery(name = "Project.public.count", query = "SELECT COUNT(*) FROM Project p WHERE p.privateProject = false"),

        // All projects whose associated groups are in the specified list
        @NamedQuery(name = "Project.member", query = "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships"),
        @NamedQuery(name = "Project.member.count", query = "SELECT COUNT(*) FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships"),

        // All projects whose associated groups are in the specified list or are public
        @NamedQuery(name = "Project.visible", query = "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships OR p.privateProject = false"),
        @NamedQuery(name = "Project.visible.count", query = "SELECT COUNT(*) FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships OR p.privateProject = false"),

        // Count projects with a name or id
        @NamedQuery(name = "Project.name.count", query = "SELECT COUNT(*) FROM Project p WHERE p.name = :name"),
        @NamedQuery(name = "Project.externalid.count", query = "SELECT COUNT(*) FROM Project p WHERE p.externalId = :externalid"),

        //Ordered public queries
        @NamedQuery(name = "Project.public.name.asc", query =  "SELECT p FROM Project p WHERE p.privateProject = false ORDER BY p.name ASC"),
        @NamedQuery(name = "Project.public.name.desc", query = "SELECT p FROM Project p WHERE p.privateProject = false ORDER BY p.name DESC"),
        @NamedQuery(name = "Project.public.date.asc", query =  "SELECT p FROM Project p WHERE p.privateProject = false ORDER BY p.startDate ASC"),
        @NamedQuery(name = "Project.public.date.desc", query = "SELECT p FROM Project p WHERE p.privateProject = false ORDER BY p.startDate DESC"),

        //Ordered member queries
        @NamedQuery(name = "Project.member.name.asc", query =  "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships ORDER BY p.name ASC"),
        @NamedQuery(name = "Project.member.name.desc", query = "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships ORDER BY p.name DESC"),
        @NamedQuery(name = "Project.member.date.asc", query =  "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships ORDER BY p.startDate ASC"),
        @NamedQuery(name = "Project.member.date.desc", query = "SELECT p FROM Project p WHERE p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships ORDER BY p.startDate DESC"),

        //Active studies are in the right date rangp
        @NamedQuery(name = "Project.member.active.name.asc", query =  "SELECT p FROM Project p WHERE (p.adminGroupId IN (:groupMemberships) OR p.membersGroupId IN (:groupMemberships)) AND p.startDate <= CURRENT_DATE AND p.endDate > CURRENT_DATE ORDER BY p.name ASC"),
        @NamedQuery(name = "Project.member.active.name.desc", query = "SELECT p FROM Project p WHERE (p.adminGroupId IN (:groupMemberships) OR p.membersGroupId IN (:groupMemberships)) AND p.startDate <= CURRENT_DATE AND p.endDate > CURRENT_DATE ORDER BY p.name DESC"),
        @NamedQuery(name = "Project.member.active.date.asc", query =  "SELECT p FROM Project p WHERE (p.adminGroupId IN (:groupMemberships) OR p.membersGroupId IN (:groupMemberships)) AND p.startDate <= CURRENT_DATE AND p.endDate > CURRENT_DATE ORDER BY p.startDate ASC"),
        @NamedQuery(name = "Project.member.active.date.desc", query = "SELECT p FROM Project p WHERE (p.adminGroupId IN (:groupMemberships) OR p.membersGroupId IN (:groupMemberships)) AND p.startDate <= CURRENT_DATE AND p.endDate > CURRENT_DATE ORDER BY p.startDate DESC"),
        @NamedQuery(name = "Project.member.count.active", query =     "SELECT COUNT(*) FROM Study s WHERE (s.adminGroupId IN (:groupMemberships) OR s.membersGroupId IN (:groupMemberships)) AND s.startDate <= CURRENT_DATE AND s.endDate > CURRENT_DATE"),
        
        //Shortcuts to get project attributes
        @NamedQuery(name = "Project.getStorageFolderId", query = "SELECT p.dataFolderId from Project p WHERE p.externalId=:externalId"),
        @NamedQuery(name = "Project.getOwnerId", query = "SELECT p.ownerId from Project p WHERE p.externalId=:externalId"),
        @NamedQuery(name = "Project.getOwnerIdByStudyId", query = "SELECT p.ownerId FROM Project p WHERE p.id=:id"),
        @NamedQuery(name = "Project.getIdFromExternalId", query = "SELECT p.id FROM Project p WHERE p.externalId=:externalId")

})
public class Project implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;

    private String externalId;

    private String name;

    private String description;

    private String ownerId;

    private String adminGroupId;

    private String membersGroupId;

    private String dataFolderId;

    private String workflowFolderId;

    private Long remoteScannerId;

    private boolean privateProject = false;

    private Date startDate;

    private Date endDate;

    private boolean visibleOnExternalSite = false;

    @ManyToMany
    private Collection<Uploader> uploaders = new ArrayList<>();

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> additionalProperties = new HashMap<>();

    protected Project() {
    }

    public Project(final String name, final String ownerId) {
        this.name = name;
        this.ownerId = ownerId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String readableId) {
        this.externalId = readableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getAdminGroupId() {
        return adminGroupId;
    }

    public void setAdminGroupId(String adminGroupId) {
        this.adminGroupId = adminGroupId;
    }

    public String getMembersGroupId() {
        return membersGroupId;
    }

    public void setMembersGroupId(String membersGroupId) {
        this.membersGroupId = membersGroupId;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public String getWorkflowFolderId() {
        return workflowFolderId;
    }

    public void setWorkflowFolderId(final String escWorkflowFolderId) {
        this.workflowFolderId = escWorkflowFolderId;
    }

    public boolean isPrivateProject() {
        return privateProject;
    }

    public void setPrivateProject(boolean privateStudy) {
        this.privateProject = privateStudy;
    }

    public Long getRemoteScannerId() {
        return remoteScannerId;
    }

    public void setRemoteScannerId(final Long remoteScannerId) {
        this.remoteScannerId = remoteScannerId;
    }

    public Collection<Uploader> getUploaders() {
        return uploaders;
    }

    public void addUploader(final Uploader uploader) {
        if (!uploaders.contains(uploader)) {
            uploaders.add(uploader);
        }

        if (!uploader.getProjects().contains(this)) {
            uploader.getProjects().add(this);
        }
    }

    public void removeUploader(final Uploader uploader) {
        uploaders.remove(uploader);
        uploader.getProjects().remove(this);
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getAdditionalProperty(String key) {
        return additionalProperties.get(key);
    }

    public String setAdditionalProperty(String key, String value) {
        return additionalProperties.put(key, value);
    }

    public String removeAdditionalProperty(String key) {
        return additionalProperties.remove(key);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isVisibleOnExternalSite() {
        return visibleOnExternalSite;
    }

    public void setVisibleOnExternalSite(boolean visibleOnExternalSite) {
        this.visibleOnExternalSite = visibleOnExternalSite;
    }


}
