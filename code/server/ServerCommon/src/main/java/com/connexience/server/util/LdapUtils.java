package com.connexience.server.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang3.StringUtils;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

/**
 * Utility class for generating LDAP authentication components
 */
public class LdapUtils {
    private LdapUtils() {
    }

    /**
     * Given a username, password, domain and host, attempt to authenticate the credentials via an LDAP server
     *
     * @param username The username
     * @param password The password
     * @param domain   The domain (in the form users.acme.com)
     * @param host     The host (and port) of the LDAP server, e.g., localhost:389
     * @return True if the credentials could be validated, false otherwise
     */
    public static boolean validateCredentials(final String username, final String password, final String domain, final String host) {
        // Set up the environment for creating the initial context
        final Hashtable<String, String> env = new Hashtable<>();

        // Factory and Host
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, host);

        // Authentication specifics
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, LdapUtils.getPrinciple(username, domain));
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            // Create the initial context
            DirContext ctx = new InitialDirContext(env);
            ctx.close();

            return true;
        } catch (NamingException e) {
            System.err.printf("Failed LDAP Login (Host: '%s', Username: '%s', Domain: '%s', Cause: '%s')\n", host, username, domain, e.getMessage());
            return false;
        }
    }

    /**
     * Extracts the username from an LDAP login which possibly includes a domain.
     *
     * @param login The LDAP login, possibly including a DOMAIN element (e.g., DOMAIN\bob).
     * @return The extracted username (e.g., bob).
     */
    public static String extractUsername(final String login) {
        if (StringUtils.isBlank(login)) {
            throw new IllegalArgumentException("Login cannot be blank.");
        }

        final String[] parts = login.split("\\\\");

        if (parts.length > 2) {
            throw new IllegalArgumentException("Logins should contain only zero or one backslash");
        } else if (parts.length == 2) {
            return parts[1];
        } else {
            return parts[0];
        }
    }

    /**
     * Extracts the domain from an LDAP login which possibly includes a domain.
     *
     * @param login         The LDAP login, possibly including a DOMAIN element (e.g., DOMAIN\bob).
     * @param defaultDomain The domain to return in case one is not provided in the login parameter.
     * @return The extracted domain (e.g., DOMAIN) or null.
     */
    public static String extractDomain(final String login, final String defaultDomain) {
        if (StringUtils.isBlank(login)) {
            throw new IllegalArgumentException("Login cannot be blank.");
        }

        final String[] parts = login.split("\\\\");

        if (parts.length > 2) {
            throw new IllegalArgumentException("Logins should contain only zero or one backslash");
        } else if (parts.length == 2) {
            return parts[0];
        } else {
            return defaultDomain;
        }
    }

    /**
     * Generate an external logon ID given a username and LDAP domain
     *
     * @param username The username
     * @param domain   The domain
     * @return An identifier for use as external logon name
     */
    public static String generateExternalLogonId(final String username, final String domain) {
        return domain.toUpperCase() + "\\" + username.toLowerCase();
    }

    /**
     * Turn a username and domain into an LDAP principle claim
     *
     * @param username The username to be the claim name, e.g., admin
     * @param domain   The domain to be the claim domain components, e.g., users.acme.com
     * @return Generated LDAP principle claim, e.g., cn=admin,dc=users,dc=acme,dc=com
     */
    private static String getPrinciple(final String username, final String domain) {
        return String.format("cn=%s,%s", username, getDomainComponents(domain)).toLowerCase();
    }

    /**
     * Generate the dc (domain components) part of the LDAP auth request
     * e.g., users.acme.com => dc=users,dc=acme,dc=com
     *
     * @param domain Domain name to be converted into domain components, e.g. users.acme.com
     * @return Converted domain components e.g., dc=users,dc=acme,dc=com
     */
    private static String getDomainComponents(final String domain) {
        final List<String> parts = Arrays.asList(domain.split("\\."));

        CollectionUtils.transform(parts, new Transformer() {
            @Override
            public Object transform(Object input) {
                return "dc=" + input.toString();
            }
        });

        return StringUtils.join(parts, ",").toLowerCase();
    }
}
