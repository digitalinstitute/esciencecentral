/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.model.project.flatstudy;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * Representation of a physical device in a flat study
 * @author hugo
 */
@Entity
@Table(name = "flatdevices")
@NamedQueries({
    @NamedQuery(name = "FlatDevice.countForStudy", query = "SELECT COUNT(*) FROM FlatDevice d WHERE d.studyId=:studyId"),
    @NamedQuery(name = "FlatDevice.countMatchingForStudy", query = "SELECT COUNT(*) FROM FlatDevice d WHERE d.studyId=:studyId AND (d.name LIKE :search OR d.externalId LIKE :search)"),
    @NamedQuery(name = "FlatDevice.searchForStudy", query = "SELECT d FROM FlatDevice d WHERE d.studyId=:studyId AND (d.name LIKE :search OR d.externalId LIKE :search)"),
    @NamedQuery(name = "FlatDevice.listForStudy", query = "SELECT d FROM FlatDevice d WHERE d.studyId=:studyId ORDER BY d.creationDate DESC"),
    @NamedQuery(name = "FlatDevice.deleteForStudy", query = "DELETE FlatDevice d WHERE d.studyId=:studyId"),
    @NamedQuery(name = "FlatDevice.deleteById", query = "DELETE FlatDevice d WHERE d.id=:id"),
    @NamedQuery(name = "FlatDevice.findByExternalId", query = "SELECT d FROM FlatDevice d WHERE d.externalId=:externalId and d.studyId=:studyId"),
})
public class FlatDevice implements Serializable, FlatStudyObject {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "esc_int_generator")
    @GenericGenerator(name = "esc_int_generator", strategy = "com.connexience.server.ejb.IntegerSequenceGenerator")
    private Integer id;    

    @Basic @Column(name="studyId") private Integer studyId;
    @Temporal(TemporalType.TIMESTAMP) @Column(name="creationDate") private Date creationDate = new Date();
    @Basic @Column(name="folderId") private String folderId;
    @Basic @Column(name="externalId") private String externalId;
    @Basic @Column(name="name") private String name;
    
    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, String> additionalProperties = new HashMap<>();
    
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudyId() {
        return studyId;
    }

    public void setStudyId(Integer studyId) {
        this.studyId = studyId;
    }

    @Override
    public HashMap<String, String> getAdditionalDisplayFields() {
        HashMap<String, String> fields = new HashMap<>();
        return fields;
    }

    @Override
    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    @Override
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}