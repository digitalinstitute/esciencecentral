package com.connexience.server.ejb.scheduler;

import javax.ejb.Remote;

/**
 * User: nsjw7
 * Date: 08/04/15
 * Time: 12:55
 */
@Remote
public interface SchedulerRemote {

    void addRunnable(String taskName);
    void removeAllNamedTasks(String taskName);
}
