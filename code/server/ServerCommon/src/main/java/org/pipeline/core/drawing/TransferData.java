package org.pipeline.core.drawing;

import java.io.Serializable;

/**
 * This interface must be implemented by objects that need to be transferred
 * between Blocks in a Drawing. 
 * @author  hugo
 */
public interface TransferData extends Serializable {
    /** Get a copy of this data */
    public TransferData getCopy() throws DrawingException;
    
    /** Get the data payload */
    public Object getPayload();
}
