package org.pipeline.core.data;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class represents a range of numerical data
 * @author nhgh
 */
public class NumericalRange implements XmlStorable {
    /** Upper bound of the range */
    private double upperBound = 1.0;

    /** Lower bound of the range */
    private double lowerBound = -1.0;

    /** Set the upper bound */
    public void setUpperBound(double upperBound){
        this.upperBound = upperBound;
    }

    /** Get the upper bound */
    public double getUpperBound(){
        return upperBound;
    }

    /** Set the lower bound */
    public void setLowerBound(double lowerBound){
        this.lowerBound = lowerBound;
    }

    /** Get the lower bound */
    public double getLowerBound(){
        return lowerBound;
    }

    /** Does a value lie within this range */
    public boolean isWithinRange(double value){
        if(value>=lowerBound && value<=upperBound){
            return true;
        } else {
            return false;
        }
    }

    /** Count the number of hits for this range given a column of numerical data */
    public int countHits(NumericalColumn column) throws DataException {
        int count = 0;
        int size = column.getRows();
        for(int i=0;i<size;i++){
            if(!column.isMissing(i)){
                if(isWithinRange(column.getDoubleValue(i))){
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public String toString() {
        return lowerBound + " <= x <= " + upperBound;
    }
        
    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("NumericalRange");
        store.add("UpperBound", upperBound);
        store.add("LowerBound", lowerBound);
        return store;
    }

    /** Load from storage */
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        upperBound = store.doubleValue("UpperBound", 1.0);
        lowerBound = store.doubleValue("LowerBound", -1.0);
    }
}
