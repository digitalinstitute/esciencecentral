package org.pipeline.core.data.columns;

/**
 * Mixed data type column which returns values as Objects
 * @author  hugo
 */
public class ObjectColumn {
    
    /** Creates a new instance of ObjectColumn */
    public ObjectColumn() {
    }
    
}
