package org.pipeline.core.drawing;

/**
 * This is the only exception that should get thrown when there is
 * an internal execution error in a block.
 * @author hugo
 */
public class BlockExecutionException extends java.lang.Exception {
    
    /** Creates a new instance of <code>DrawingException</code> without detail message.*/
    public BlockExecutionException() {
    }
    
    /** Constructs an instance of <code>DrawingException</code> with the specified detail message.
     * @param msg the detail message.*/
    public BlockExecutionException(String msg) {
        super(msg);
    }
}
