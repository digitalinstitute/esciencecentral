package org.pipeline.core.data.manipulation.functions;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.data.maths.StdOutlierRemover;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class selects a column and removes outliers from it using the
 * n x Standard deviation criteria.
 * @author hugo
 */
public class StdOutlierRemovingColumnPicker extends ColumnPicker {
    /** Outler removing calculator */
    private StdOutlierRemover filter = new StdOutlierRemover();
    
    /** Creates a new instance of StdOutlierRemovingColumnPicker */
    public StdOutlierRemovingColumnPicker() {
        super();
        setLimitColumnTypes(true);
        addSupportedColumnClass(DoubleColumn.class);
        addSupportedColumnClass(IntegerColumn.class);        
        setCopyData(false);
    }
    
    /** Pick and scale the column */
    public Column pickColumn(Data data) throws IndexOutOfBoundsException, DataException {
        Column c = super.pickColumn(data);
        if(c instanceof NumericalColumn){
            NumericalColumn raw = (NumericalColumn)c;
            NumericalColumn result = filter.screenColumn(raw);
            return (Column)result;
        } else {
            throw new DataException("Filters can only operate on numerical columns");
        }
    }    
    
    /** Set the standard deviation tolerance */
    public double getStdTolerance(){
        return filter.getStd();
    }
    
    /** Get the standard deviation tolerance */
    public void setStdTolerance(double std){
        filter.setStd(std);
    }
    
    /** Get the missing value index column */
    public IntegerColumn getOutlierIndexColumn(){
        return filter.getOutlierIndex();
    }
    
    /** Get the original raw data column */
    public NumericalColumn getRawDataColumn(){
        return filter.getOriginalColumn();
    }
    
    /** Get the screened column */
    public NumericalColumn getScreenedColumn(){
        return filter.getScreenedColumn();
    }
    
    /** Recreate from storage */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        super.recreateObject(xmlDataStore);
        filter.setStd(xmlDataStore.doubleValue("StdTolerance", 3));
    }

    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("StdTolerance", filter.getStd());
        return store;
    }        
}
