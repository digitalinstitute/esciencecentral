package org.pipeline.core.data.manipulation.text;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.data.text.TextColumnSplitter;

/**
 * This class splits a text column into a set of columns based on a delimiter
 * @author nhgh
 */
public class TextColumnSplitterPicker extends ColumnPicker {
    /** Delimiting text string used to split column */
    private TextColumnSplitter splitter = new TextColumnSplitter();
    
    /**
     * Creates a new instance of TextColumnSplitterPicker
     */
    public TextColumnSplitterPicker() {
        super();
        setCopyData(false);
        setLimitColumnTypes(false);
        setMultiColumnPicker(true);
    }
    
    /** Get the delimiter text */
    public String getDelimiter(){
        return splitter.getDelimiter();
    }
    
    /** Set the delimiter text */
    public void setDelimiterText(String delimiter){
        splitter.setDelimiter(delimiter);
    }

    /** Extract columns */
    public Column[] pickColumns(Data data) throws IndexOutOfBoundsException, DataException {
        Column column = super.pickColumn(data);
        return splitter.splitColumn(column);
    }
}