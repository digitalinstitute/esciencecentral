package org.pipeline.core.util.block;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.gui.data.table.DataTableModel;

import javax.swing.*;
import java.awt.*;

/**
 * This class provides a generic tabular preview of a column picker block.
 * @author hugo
 */
public class GenericTablePreviewPanel extends JPanel implements ColumnPickerBlockPreviewPanel {
    /** Title caption */
    private JLabel title;
    
    /** Scroller for the table */
    private JScrollPane scroller;
    
    /** Data table */
    private JTable dataTable;
    
    /** Constructor */
    public GenericTablePreviewPanel(){
        setLayout(new BorderLayout());
        title = new JLabel("Results");
        add(title, BorderLayout.NORTH);
        
        scroller = new JScrollPane();
        add(scroller, BorderLayout.CENTER);
    }
    /** Clear the display */
    public void clearDisplay() {
        dataTable = new JTable();
        scroller.setViewportView(dataTable);
        scroller.updateUI();
    }

    /** Display the data */
    public void displayResults(ColumnPicker picker, Column originalColumn, Column[] pickerResults) {
        Data displayData = new Data();
        try {
            Column originalCopy = originalColumn.getCopy();
            originalCopy.setName("Original Data");
            displayData.addColumn(originalCopy);
            
            displayData.addColumns(pickerResults);
            dataTable = new JTable(new DataTableModel(displayData));
            scroller.setViewportView(dataTable);
            scroller.updateUI();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}