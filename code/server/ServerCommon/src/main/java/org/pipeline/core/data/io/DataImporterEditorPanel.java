package org.pipeline.core.data.io;

/**
 * This interface defines an editor panel for a data importer.
 * @author hugo
 */
public interface DataImporterEditorPanel {
    /** Set the importer */
    public void setImporter(DataImporter importer);
    
    /** Update the editor with the current settings */
    public void updateImporter();
}
