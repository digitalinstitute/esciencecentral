package org.pipeline.core.util.block;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.manipulation.ColumnPicker;

/**
 * This interface defines the behaviour of a column picker preview
 * panel that can display the results of a pick operation.
 * @author hugo
 */
public interface ColumnPickerBlockPreviewPanel {
    /** Display the new results */
    public void displayResults(ColumnPicker picker, Column originalColumn, Column[] pickerResults);
    
    /** Clears the display if nothing has been selected */
    public void clearDisplay();
}