package org.pipeline.core.data.time;

import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.StringColumn;

import java.text.SimpleDateFormat;

/**
 * This column takes a text column and converts it to a date
 * type column.
 * @author hugo
 */
public class StringToDateColumnConverter {
    /** Original String column */
    private StringColumn original;
    
    /** Newly created Date column */
    private DateColumn dateColumn;
    
    /** Parse format */
    private SimpleDateFormat format = new SimpleDateFormat();

    /** Text pattern */
    private String pattern = null;
    
    /** Creates a new instance of StringToDateColumnConverter */
    public StringToDateColumnConverter(StringColumn original) {
        this.original = original;
        
    }
    
    /** Set the format. The format complies with the standard
     * java date formatting rules */
    public void setDateFormat(String pattern) throws IllegalArgumentException {
        try {
            format.applyPattern(pattern);
            this.pattern = pattern;
        } catch (NullPointerException e){
        }
    }
    
    /** Convert to a DateColumn */
    public DateColumn convertToDateColumn() throws DataException {
        dateColumn = new DateColumn(original.getRows());
        dateColumn.setName(original.getName());
        
        
        
        return dateColumn;
    }
    
    /** Get the date column */
    public DateColumn getDateColumn(){
        if(dateColumn==null){
            try {
                dateColumn = convertToDateColumn();
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return dateColumn;
    }
}
