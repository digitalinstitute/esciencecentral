package org.pipeline.core.data.manipulation;

import java.io.Serializable;

/**
 * This calss contains a name and index reference to a column of data
 * @author hugo
 */
public class ColumnReference implements Serializable {
    private static final long serialVersionUID = 1L;
    private int index;
    private String name;

    public ColumnReference() {
    }

    public ColumnReference(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setName(String name) {
        this.name = name;
    }
}