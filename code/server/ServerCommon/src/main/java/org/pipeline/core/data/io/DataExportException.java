package org.pipeline.core.data.io;

/**
 * This exception is thrown if there are errors when exporting data
 * @author hugo
 */
public class DataExportException extends Exception {
    
    /** Creates a new instance of DataExportException */
    public DataExportException() {
        super();
    }
    
    /** Creates a new instance of DataExportException */
    public DataExportException(String message) {
        super(message);
    }

    DataExportException(String message, Throwable cause){
        super(message, cause);
    }
        
}
