package org.pipeline.gui.data.manipulation;

import org.pipeline.core.data.manipulation.ColumnPicker;

/**
 * Classes that implement this interface listen for events from a column
 * picker collection listener. It is designed to be used to extend the
 * column picker editor to be able to edit more sophisticated column
 * pickers that process data.
 * @author hugo
 */
public interface ColumnPickerCollectionEditorListener {
    /** Picker selection changed */
    public void pickerSelected(ColumnPicker picker);
    
    /** Update a picker */
    public void updatePicker(ColumnPicker picker);
}
