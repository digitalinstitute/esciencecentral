package org.pipeline.core.xmlstorage;

/**
 * Classes that implement this interface can be persisted automatically
 * using introspection and the xmlstorage package. All objects contained
 * within this object must also be XmlStorable or XmlAutoStorable.
 *
 * This interface has no methods, but classes that implement it have
 * to adhere to the following rules:
 *
 * 1) There must be a blank constructor
 * 2) All the instance variables must be supported by the xmlstorage
 *    package, either natively, or using user added xmldatatypes,
 *    or implement either the XmlStorage or XmlAutoStorable
 *    interfaces.
 *
 * @author  hugo
 */
public interface XmlAutoStorable {
}
