package org.pipeline.gui.data.manipulation;

import org.pipeline.core.data.manipulation.ColumnPicker;

/**
 * Classes that implement this interface listen to config panel changes.
 * @author hugo
 */
public interface ColumnPickerConfigListener {
    /** Picker has been updated */
    public void pickerUpdated(ColumnPicker picker);
}
