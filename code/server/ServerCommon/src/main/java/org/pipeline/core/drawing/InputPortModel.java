package org.pipeline.core.drawing;

/**
 * This interface defines functionality specific to an input port.
 * @author  hugo
 */
public interface InputPortModel extends PortModel {
    public boolean isOptional();
    public void setOptional(boolean optional);
}