package org.pipeline.core.xmlstorage.security;

/**
 * Objects that implement this interface listen for changes in the status of
 * the signature data object
 * @author hugo
 */
public interface XmlDataStoreSignatureListener {
    /** State of the signature has changed */
    public void signatureStateChanged(XmlDataStoreSignatureHelper sig);
}
