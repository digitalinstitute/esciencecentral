package org.pipeline.core.drawing;

import org.pipeline.core.drawing.layout.DrawingLayout;

import java.awt.*;
/**
 * This interface defines behaviour of a class that renders a block onto some
 * form of drawing surface.
 * @author  hugo
 */
public interface BlockModelRenderer {
    /** Set the block */
    public void setBlock(BlockModel block);
    
    /** Get the block */
    public BlockModel getBlock();
    
    /** Re-render the block onto an Object. This will be cast to the appropriate
     * type by the renderer. This method also passes a DrawingLayout that is used\
     * to lookup the position to render the block at */
    public void render(Graphics2D display, DrawingLayout layout, BlockExecutionReport status) throws DrawingException;
    
    /** Set the drawing view */
    public void setDrawingView(Object drawingView);
}
