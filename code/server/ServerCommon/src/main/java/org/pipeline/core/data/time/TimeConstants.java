package org.pipeline.core.data.time;

/**
 * This interface provides a number of time related constants that are
 * used by the time processing functions
 * @author hugo
 */
public interface TimeConstants {
    /** Units are milliseconds */
    public static final int MILLISECONDS = 0;
    
    /** Units are seconds */
    public static final int SECONDS = 1;
    
    /** Units are minutes */
    public static final int MINUTES = 2;
    
    /** Units are hours */
    public static final int HOURS = 3;
    
    /** Units are days */
    public static final int DAYS = 4;
    
    /** Units are weeks */
    public static final int WEEKS = 5;
    
    /** Number of units supported */
    public static final int UNIT_COUNT = 6;
    
    /** Multipliers */
    public static final long[] MULTIPLERS = {1,         // Milliseconds
                                             1000,      // Seconds
                                             60000,     // Minutes
                                             3600000,   // Hours
                                             86400000,  // Days
                                             604800000};// Weeks

    /** String keys for labels */
    public static final String[] KEYS = {"MILLISECONDS_LABEL", "SECONDS_LABEL", "MINUTES_LABEL", "HOURS_LABEL", "DAYS_LABEL", "WEEKS_LABEL"};

    /** Units for labels */
    public static final String[] UNITS = {"Milliseconds", "Seconds", "Minutes" ,"Hours", "Days", "Weeks"};
}
