package org.pipeline.core.data.maths;

import org.pipeline.core.data.DataException;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;

/**
 * This class calculates the RMS error between two numerical columns.
 * @author hugo
 */
public class ErrorCalculator {
    /** First data column */
    private NumericalColumn column1;
    
    /** Second data column */
    private NumericalColumn column2;
    
    /** Squared error column */
    private DoubleColumn rmsColumn;
    
    /** RMS Error value */
    private double rmsValue;
    
    /** Construct with two NumericalColumns */
    public ErrorCalculator(NumericalColumn column1, NumericalColumn column2) {
        this.column1 = column1;
        this.column2 = column2;
    }

    /** Calculate the errors */
    public DoubleColumn calculate() throws DataException, IndexOutOfBoundsException {
        int rows = Math.min(column1.getRows(), column2.getRows());
        rmsColumn = new DoubleColumn("SqErr_" + column1.getName());

        // Calculate squared errors
        for(int j=0;j<rows;j++){
            if(!column1.isMissing(j) && !column2.isMissing(j)){
                rmsColumn.appendDoubleValue(Math.sqrt(Math.pow(column1.getDoubleValue(j) - column2.getDoubleValue(j), 2)));
            } else {
                rmsColumn.appendObjectValue(MissingValue.get());
            }
        }

        // Calculate the rms error
        double mean = new MeanValueCalculator(rmsColumn).doubleValue();
        rmsValue = Math.sqrt(mean);
        return rmsColumn;
    }
    
    /** Get the RMS value */
    public double getRmsValue(){
        return rmsValue;
    }
}
