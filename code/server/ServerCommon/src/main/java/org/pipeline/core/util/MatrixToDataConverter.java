package org.pipeline.core.util;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.matrix.Matrix;

/**
 * This classs converts a matrix to a set of data. Use sparingly, it copies
 * data and isn't that efficient.
 * @author hugo
 */
public class MatrixToDataConverter {
    
    /** Matrix to convert */
    private Matrix matrix;
    
    /** Creates a new instance of MatrixToDataConterter */
    public MatrixToDataConverter(Matrix matrix) {
        this.matrix = matrix;
    }
    
    /** Convert to a set of data */
    public Data toData() {
        try {
            Data data = new Data();
            int rows = matrix.getRowDimension();
            int cols = matrix.getColumnDimension();
            
            DoubleColumn c;
            
            for(int i=0;i<cols;i++){
                c = new DoubleColumn(rows);
                c.setName("c" + i);
                for(int j=0;j<rows;j++){
                    c.appendDoubleValue(matrix.get(j, i));
                }
                data.addColumn(c);
            }
            
            return data;
            
        } catch (Exception e){
            return new Data();
        }
    }
}
