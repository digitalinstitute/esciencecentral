package org.pipeline.core.drawing.spanning;

// <editor-fold defaultstate="collapsed" desc=" Imports ">
import org.pipeline.core.drawing.BlockModel;
import org.pipeline.core.drawing.DrawingModel;
// </editor-fold>

/**
 * Classes that implement this interface listen to drawing execution
 * events coming from the DrawingExecutionProcessor.
 * @author hugo
 */
public interface DrawingExecutionListener {
    /** Drawing Execution has started */
    public void drawingExecutionStarted(DrawingModel drawing);
    
    /** Drawing Execution has finished */
    public void drawingExecutionFinished(DrawingModel drawing);
    
    /** A block is about to be executed */
    public void blockExecutionStarted(BlockModel block);
    
    /** Block has finished executing */
    public void blockExecutionFinished(BlockModel block);
}
