package org.pipeline.core.data.io;

import org.pipeline.core.data.Column;

/**
 * This class stores a set of preferred data types for columns. It is used
 * when importing data so that the user can specify the data types of
 * columns if they cannot automatically be determined. N.B. This is currently
 * a placeholder, which doesn't do anything.
 * @author hugo
 */
public class ColumnTypePreferences {
    
    /** Creates a new instance of ColumnTypePreferences */
    public ColumnTypePreferences() {
    }
    
    /** Get the number of preferred column types */
    public int getPreferenceCount(){
        return 0;
    }
    
    /** Create the relevant type of column */
    public Column createColumn(int column){
        return null;
    }
}
