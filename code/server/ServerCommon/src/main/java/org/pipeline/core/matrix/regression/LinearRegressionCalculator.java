package org.pipeline.core.matrix.regression;

import org.pipeline.core.matrix.Matrix;

/**
 * This class calculates a regression model given a set of X and Y
 * data using a pseudo inverse.
 * @author hugo
 */
public class LinearRegressionCalculator {
    /** X-Values */
    Matrix x;
    
    /** Y-Values */
    Matrix y;
    
    /** Creates a new instance of LinearRegressionCalculator */
    public LinearRegressionCalculator(Matrix x, Matrix y) {
        this.x = x;
        this.y = y;
    }
    
    /** Calculate the regression model. Parameters are calculated using:
     * B = ((X'X)^-1)X'Y */
    public Matrix calculate(){
        Matrix xTx = x.transpose().times(x);
        Matrix temp = xTx.inverse().times(x.transpose());
        Matrix B = temp.times(y);
        return B;
    }
}
