package org.pipeline.core.drawing.customio;

import org.pipeline.core.drawing.BlockModel;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.model.DefaultOutputPortModel;

/**
 * This class provides customised output ports.
 * @author hugo
 */
public class CustomOutputDefinition extends CustomPortDefinition {
    
    /** Creates a new instance of CustomOutputDefinition */
    public CustomOutputDefinition() {
        super();
        setLocation(PortModel.RIGHT_OF_BLOCK);
    }
    
    /** Create a port based on this definition */
    public PortModel createPort(BlockModel block) {
        DefaultOutputPortModel port = new DefaultOutputPortModel(this.getName(), getLocation(), getOffset(), block);
        if(getDataType()!=null){
            port.addDataType(getDataType());
            port.setDataTypeRestricted(true);
        } else {
            port.setDataTypeRestricted(false);
        }
        port.setStreamable(isStreamable());
        return port;
    }    
    
    /** Override toString method to provide useful text on the IO editor */
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("Output");
        buffer.append(": ");
        buffer.append(getName());
        buffer.append("  ");
        buffer.append("at");
        buffer.append(" ");
        buffer.append(getOffset());
        buffer.append("%  ");
        buffer.append("along");
        buffer.append(" ");
        buffer.append(getLocationText());
        return buffer.toString();
       
    }    
}
