package org.pipeline.core.data.manipulation;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.ColumnFactory;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class is used to create a column of data. It forms the basis for
 * objects that create data without necessarily picking columns.
 * @author hugo
 */
public abstract class ColumnCreator implements XmlStorable {
    /** Name for the new column */
    private String name = "";
    
    /** Data type for new column */
    private String columnTypeId = "double-column";
    
    /** Creates a new instance of ColumnCreator */
    public ColumnCreator(String columnTypeId) {
        this.columnTypeId = columnTypeId;
    }

    /** Create the empty column */
    protected Column createEmptyColumn() throws DataException {
        Column col = ColumnFactory.createColumn(columnTypeId);
        col.setName(name);
        return col;
    }
    
    /** Recreate this object from a store */
    public void recreateObject(XmlDataStore store) throws XmlStorageException {
        name = store.stringValue("Name", "");
        columnTypeId = store.stringValue(columnTypeId, "");
    }

    /** Save this object to a store */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore("ColumnCreator");
        store.add("Name", name);
        store.add("ColumnTypeID", columnTypeId);
        return store;
    }
    
    /** Create the column of data */
    public Column createColumn() throws DataException {
        return createEmptyColumn();
    }
    
    /** Create the column based on an existing set of data */
    public Column createColumn(Data data) throws DataException {
        return createEmptyColumn();
    }
}
