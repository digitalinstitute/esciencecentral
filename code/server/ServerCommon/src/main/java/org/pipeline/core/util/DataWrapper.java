package org.pipeline.core.util;

import org.pipeline.core.data.Data;
import org.pipeline.core.drawing.DataType;
import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.TransferData;

/**
 * This class wraps up a Data object in a TransferData interface.
 * @author hugo
 */
public class DataWrapper implements TransferData {
    /** Data being passed around */
    private Data data = null;
    
    /** Static data type */
    public static final DataType DATA_WRAPPER_TYPE = new DataWrapperDataType();
    
    /** Creates a new instance of DataWrapper from a Data object */
    public DataWrapper(Data data) {
        this.data = data;            
    }

    /** Get the data payload */
    public Object getPayload() {
        return data;
    }

    /** Get a copy of this data */
    public TransferData getCopy() throws DrawingException {
        try {
            DataWrapper wrapper = new DataWrapper(data.getCopy());
            return wrapper;
            
        } catch (Exception e){
            // TODO: Internationalise
            throw new DrawingException("Cannot create data copy");
        }
    }
}
