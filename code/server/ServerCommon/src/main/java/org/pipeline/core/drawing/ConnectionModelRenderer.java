package org.pipeline.core.drawing;

import org.pipeline.core.drawing.layout.DrawingLayout;

import java.awt.*;

/**
 * This interface defines the behaviour of a renderer for a connection.
 * @author  hugo
 */
public interface ConnectionModelRenderer {
        /** Set the connection */
    public void setConnection(ConnectionModel connection);
    
    /** Get the block */
    public ConnectionModel getConnection();
    
    /** Re-render the connection onto an Object. This will be cast to the appropriate
     * type by the renderer. This method also gets a DrawingLayout object so
     * that the renderer can find out where to draw the connection */
    public void render(Graphics2D display, DrawingLayout layout, boolean selected) throws DrawingException;
}
