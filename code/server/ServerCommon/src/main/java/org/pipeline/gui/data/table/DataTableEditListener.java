package org.pipeline.gui.data.table;

import org.pipeline.core.data.Data;
/**
 * Classes that implement this interface can listen for edit events
 * on a Data set being edited using a DataTableModel.
 * @author hugo
 */
public interface DataTableEditListener {
    /** Data has chagned */
    public void dataChanged(Data data);
}
