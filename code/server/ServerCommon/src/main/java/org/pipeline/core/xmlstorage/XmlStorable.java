package org.pipeline.core.xmlstorage;

/**
 * Classes that implement this interface can be stored to an XmlDataStore object. Note
 * the user is responsible for implementing the code within the storeObject and
 * retrieveObject methods
 * @author  hugo
 */
public interface XmlStorable {
    /** Save this object to an XmlDataStore */
    public XmlDataStore storeObject() throws XmlStorageException;
    
    /** Recreate this object from an XmlDataStore */
    public void recreateObject(XmlDataStore store) throws XmlStorageException;
}
