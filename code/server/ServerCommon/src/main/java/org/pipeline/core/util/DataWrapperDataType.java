package org.pipeline.core.util;

import org.pipeline.core.drawing.DataType;

/**
 * This class describes the data type for a standard set of Data that
 * gets passed between blocks
 * @author hugo
 */
public class DataWrapperDataType extends DataType {
    
    /** Creates a new instance of DataWrapperDataType */
    public DataWrapperDataType() {
        super("Data", "Standard set of data", DataWrapper.class);
    }
}
