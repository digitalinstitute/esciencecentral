package org.pipeline.core.util.block;

import org.pipeline.core.data.manipulation.ColumnPicker;

import javax.swing.*;
import java.util.Vector;

/**
 * This panel provides the basic functions of a config panel for a column 
 * picker used by a ColumnPickerBlock. It handles listeners etc and can
 * be extended to provide the desired editing functionaliy.
 * @author hugo
 */
public abstract class DefaultColumnPickerBlockConfigPanel extends JPanel implements ColumnPickerBlockConfigPanel {
    /** Listeners */
    private Vector<ColumnPickerConfigPanelListener> listeners = new Vector<>();

    /** Currently selected picker */
    private ColumnPicker _picker = null;
    
    /** Add a listener to this panel */
    public void addColumnPickerConfigPanelListener(ColumnPickerConfigPanelListener listener) {
        listeners.add(listener);
    }

    /** Remove a config panel listener from this panel */
    public void removeColumnPickerConfigPanelListener(ColumnPickerConfigPanelListener listener) {
        listeners.remove(listener);
    }
    
    /** Notify a change in the configuration of the selected picker */
    protected void notifyPickerUpdated(){
        if(_picker!=null){
            for(int i=0;i<listeners.size();i++){
                listeners.get(i).configPanelPickerUpdated(_picker);
            }
        }
    }

    /** Get the currently selected picker */
    public ColumnPicker getPicker(){
        return _picker;
    }
    
    /** Set the currently selected picker */
    public void setPicker(ColumnPicker picker) {
        this._picker = picker;
        pickerChanged();
    }

    /** Update the selected picker with the new settings */
    public void updatePicker(ColumnPicker picker) {
        if(picker!=null){
            if(picker.equals(this._picker)){
                updatePicker();
            }
        }
    }
    
    /** Save changes to the current picker */
    public abstract void updatePicker();
    
    /** Picker has changed or been set */
    public abstract void pickerChanged();
}
