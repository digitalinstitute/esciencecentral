package org.pipeline.core.xmlstorage;

import java.util.Enumeration;
import java.util.Vector;

/**
 * This class steps through an Enumeration to return a numbered item
 * @author  hugo
 */
public class EnumerationStepper {
    private Enumeration enm = null;
    
    /** Creates a new instance of EnumerationStepper */
    public EnumerationStepper(Enumeration en) {
        enm = en;
    }
    
    /** Step to an element */
    public Object stepTo(int iIndex){
        int iCount = 0;
        while(iCount<=iIndex){
            if(iCount==iIndex){
                return enm.nextElement();
            } else {
                iCount++;
                enm.nextElement();
            }
        }
        return null;
    }
    
    /** Return the index of an object */
    public int indexOf(Object oObject){
        int iCount=0;
        while(enm.hasMoreElements()){
            if(enm.nextElement().equals(oObject)){
                return iCount;
            } else {
                enm.nextElement();
                iCount++;
            }
        }
        return -1;
    }
    
    /** Put the data into a new Vector */
    public Vector toVector(){
    	Vector vector = new Vector();
    	while(enm.hasMoreElements()){
    		vector.add(enm.nextElement());
    	}
    	return vector;
    }
}
