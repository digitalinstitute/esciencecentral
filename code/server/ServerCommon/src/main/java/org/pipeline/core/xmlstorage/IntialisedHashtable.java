package org.pipeline.core.xmlstorage;

import java.util.Hashtable;

/**
 * This object extends a Hashtable to allow it to be created
 * with a pre-initialised key-value set
 * @author  hugo
 */
public class IntialisedHashtable extends Hashtable {
    /**
     * Class version UID.
     * 
     * Please increment this value whenever your changes may cause 
     * incompatibility with the previous version of this class. If unsure, ask 
     * one of the core development team or read:
     *   http://docs.oracle.com/javase/6/docs/api/java/io/Serializable.html
     * and
     *   http://docs.oracle.com/javase/6/docs/platform/serialization/spec/version.html#6678
     */
    private static final long serialVersionUID = 1L;

    /** Creates a new instance of IntialisedHashtable */
    public IntialisedHashtable(Object[] keys, Object[] values){
        super();
        if(keys.length == values.length){
            for(int i=0;i<keys.length;i++){
                put(keys[i], values[i]);
            }
        }
    }
}
