package org.pipeline.core.data.source;

/**
 * Classes that implement this interface can listen for events coming
 * from data source objects.
 * @author hugo
 */
public interface DataSourceListener {
    /** All data has been updated */
    public void dataChanged(DataSource source);
    
    /** A single part of the data hase been updated */
    public void dataChanged(DataSource source, String name);
}
