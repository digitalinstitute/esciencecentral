package org.pipeline.core.drawing;

/**
 * This class acts as a redirector for the data held within a PortModel. It is
 * used to enable drawings to save intermediate data to disk instead of holding
 * it in memory during long running remote invocations.
 * @author hugo
 */
public interface PortDataSource {
    /** Get some data for a specific port */
    public TransferData getData(PortModel port) throws DrawingException;
    
    /** Set the data for a specific port */
    public void setData(PortModel port, TransferData data);
    
    /** Remove the data from this store */
    public void clearData(PortModel port);
    
    /** Does this source contain data for a port */
    public boolean containsData(PortModel port) throws DrawingException;
}