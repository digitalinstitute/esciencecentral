package org.pipeline.core.data;

import java.io.Serializable;

/**
 * This class represents a missing value in a data column
 * @author hugo
 */
public class MissingValue implements Serializable
{
    static final long serialVersionUID = -153603249070293003L;
    
    /** Missing value message */
    public final static String MISSING_VALUE_TEXT = "MISSING";
    
    /** Text that is used to represent missing values in internal code */
    public final static String MISSING_VALUE_REPRESENTATION = "_XX_MISSING_XX_";

    /** Message for missing value */
    public static final String MISSING_VALUE_MESSAGE="[MISSING]";


    /**
     * Creates a new instance of MissingValue.
     * 
     * <p>Please DO NOT USE this constructor.
     * The preferred way to get an instance of <code>MissingValue</code> is 
     * using static {@link #get()}.
     * </p>
     * 
     * TODO: Make this constructor private at the next deep update of the system.
     * Once the constructor is made private, search through the code for text: 
     * <code> instanceof MissingValue</code> and remove it.
     * For more details contact Jacek.
     */
    public MissingValue()
    { }


    /** The preferred way to get an instance of MissingValue. */
    public static MissingValue get()
    {
        return _missingValue;
    }


    /** Override toString method */
    public String toString()
    {
        return MISSING_VALUE_TEXT;
    }


    private static final MissingValue _missingValue = new MissingValue();
}
