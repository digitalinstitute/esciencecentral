package org.pipeline.core.data.source;

import org.pipeline.core.data.Data;

/**
 * This interface extends the simple data source object to provide a multiple
 * output data source, which could be a drawing.
 * @author nhgh
 */
public interface MultiDataSource extends DataSource {
    /** Get the names of the outputs */
    public String[] getOutputNames();
    
    /** Get a named data output */
    public Data getData(String name);
    
    /** Is a named set of data valid */
    public boolean dataValid(String name);
}
