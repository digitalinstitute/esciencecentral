package org.pipeline.core.xmlstorage.security;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamWriter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.PrivateKey;
import java.security.Signature;

/**
 * This class signs an XmlDataStore by converting it to a String and signing the
 * data. 
 * @author hugo
 */
public class XmlDataStoreSigner {
    /** Data storage object to be signed */
    private XmlDataStore store;
    
    /** Creates a new instance of XmlDataStoreSigner */
    public XmlDataStoreSigner(XmlDataStore store) {
        this.store = store;
    }
    
    /** Return a byte array containing the signature data for a button. Needs
     * to have a PrivateKey supplied in order to sign the object */
    public byte[] sign(PrivateKey key) throws XmlStorageException {
        try {
            // Save object to a byte array
            ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
            PrintStream p = new PrintStream(byteStream);
            XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(store);
            writer.write(p);
            p.flush();                    
            byte[] objectData = byteStream.toByteArray();            
            
            // Sign the data using a private key
            Signature sig = Signature.getInstance("SHA1withDSA");
            sig.initSign(key);
            sig.update(objectData);            
            byte[] signatureData = sig.sign();
            
            // Set the data in the store
            store.setSignatureData(signatureData);
            return signatureData;
            
        } catch (Exception e){
            throw new XmlStorageException(e.getLocalizedMessage());            
        }
    }
}
