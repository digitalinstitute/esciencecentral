package org.pipeline.core.drawing;

/**
 * This Exception is thrown by classes in the Drawing package.
 * @author  hugo
 */
public class DrawingException extends java.lang.Exception {
    
    /**
     * Creates a new instance of <code>DrawingException</code> without detail message.
     */
    public DrawingException() {
    }
    
    
    /**
     * Constructs an instance of <code>DrawingException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DrawingException(String msg) {
        super(msg);
    }

    public DrawingException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
    
}
