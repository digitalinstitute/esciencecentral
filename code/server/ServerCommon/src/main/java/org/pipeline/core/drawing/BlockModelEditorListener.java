package org.pipeline.core.drawing;

/**
 * Classes that implement this interface listen to events coming
 * from a block editor object.
 * @author hugo
 */
public interface BlockModelEditorListener {
    /** Editor wants to close */
    public void editorCloseRequest(BlockModelEditor editor);
}
