package org.pipeline.core.xmlstorage;

import java.util.Vector;

/**
 * This class allows the creation of a Vector with an existing
 * set of objects in it.
 * @author  hugo
 */
public class InitialisedVector extends Vector {
    /**
     * Class version UID.
     * 
     * Please increment this value whenever your changes may cause 
     * incompatibility with the previous version of this class. If unsure, ask 
     * one of the core development team or read:
     *   http://docs.oracle.com/javase/6/docs/api/java/io/Serializable.html
     * and
     *   http://docs.oracle.com/javase/6/docs/platform/serialization/spec/version.html#6678
     */
    private static final long serialVersionUID = 1L;

    /** Creates a new instance of InitialisedVector */
    public InitialisedVector(Object[] values) {
        super();
        for(int i=0;i<values.length;i++){
            add(values[i]);
        }
    }   
}
