package org.pipeline.core.matrix.xmlstorage;

/**
 * This Exception is thrown during matrix read operations.
 * @author hugo
 */
public class MatrixReadException extends java.lang.Exception {
    
    /**
     * Creates a new instance of <code>MatrixReadException</code> without detail message.
     */
    public MatrixReadException() {
    }
    
    
    /**
     * Constructs an instance of <code>MatrixReadException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public MatrixReadException(String msg) {
        super(msg);
    }
}
