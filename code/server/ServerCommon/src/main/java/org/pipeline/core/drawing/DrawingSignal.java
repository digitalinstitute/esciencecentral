package org.pipeline.core.drawing;

import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * This class represents a signal that can be passed through a drawing. It
 * is used to signify the start of streaming or some event specific to a 
 * collection of blocks.
 * @author hugo
 */
public class DrawingSignal {
    /** Signal type */
    private String signalType;
    
    /** Signal description */
    private String signalDescription;
    
    /** Signal properties */
    private XmlDataStore properties;
    
    /** Creates a new instance of DrawingSignal */
    public DrawingSignal(String signalType, String signalDescription) {
        this.signalType = signalType;
        this.signalDescription = signalDescription;
        properties = new XmlDataStore();
    }
    
    /** Get the signal type */
    public String getSignalType(){
        return signalType;
    }
    
    /** Get the signal description */
    public String getSignalDescription(){
        return signalDescription;
    }
    
    /** Get the signal properties */
    public XmlDataStore getProperties(){
        return properties;
    }
}
