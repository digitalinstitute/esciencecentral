package org.pipeline.core.data.maths;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.NumericalColumn;

/**
 * Calculates the standard deviation of a column
 * @author hugo
 */
public class StdCalculator {
    /** Column to calculate */
    private Column column;
    
    /** Creates a new instance of StdCalculator */
    public StdCalculator(NumericalColumn column) {
        this.column = (Column)column;
    }
    
    /** Calculate the standard deviation */
    public double doubleValue(){
        double ssq = 0;
        NumericalColumn numberCol = (NumericalColumn)column;
        double mean = new MeanValueCalculator(numberCol).doubleValue();
        int count = column.getNonMissingRows();
        int size = column.getRows();
        
        if(!Double.isNaN(mean)){
            if(count > 1){
                for(int i=0;i<size;i++){
                    if(!column.isMissing(i)){
                        try {
                            ssq = ssq + Math.pow(numberCol.getDoubleValue(i) - mean, 2);
                        } catch (Exception e){
                            // Thrown by missing
                        }
                    }
                }
                return Math.sqrt(ssq / (count - 1));
                
            } else {
                // Only one row
                return 0;
            }
            
        } else {
            return Double.NaN;
        }
    }
}
