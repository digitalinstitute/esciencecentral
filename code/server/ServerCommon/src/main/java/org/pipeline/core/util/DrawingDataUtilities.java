package org.pipeline.core.util;

import org.pipeline.core.data.Data;
import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.InputPortModel;
import org.pipeline.core.drawing.OutputPortModel;
import org.pipeline.core.drawing.TransferData;
        
/**
 * This class provides utility methods for passing data in and out of blocks
 * wrapped up as TransferData objects
 * @author hugo
 */
public abstract class DrawingDataUtilities {
    /** Get a set of Data from an input port. A DrawingException is thrown if
     * the data is not of the correct type */
     public static Data getInputData(InputPortModel input) throws DrawingException {
         TransferData transfer = input.getData();
         if(transfer!=null){
             Object payload = transfer.getPayload();
             if(payload instanceof Data){
                 return (Data)payload;
             } else {
                 throw new DrawingException("No data transferred on input port");
             }

         } else {
             throw new DrawingException("No data available");
         }
     }
     
     /** Pass a set of data to an output port. This method wraps the data up in a DataWrapper
      * object so that it can be passed around the drawing. The data is always made read-only
      * when it is set as an output. This is so that a single copy can be passed to downstream
      * blocks without danger of changes being made which will corrupt the data for other blocks. */
     public static void setOutputData(OutputPortModel output, Data data) {
         data.setReadOnly(true);
         DataWrapper wrapper = new DataWrapper(data);
         output.setData(wrapper);
     }
}
