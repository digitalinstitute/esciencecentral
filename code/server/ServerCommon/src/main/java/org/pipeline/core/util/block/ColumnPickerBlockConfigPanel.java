package org.pipeline.core.util.block;

import org.pipeline.core.data.manipulation.ColumnPicker;
/**
 * This interface defines the functionality of a configuration panel
 * that is used to set specific properties of a column picker.
 * @author hugo
 */
public interface ColumnPickerBlockConfigPanel {
    /** Set the current column picker */
    public void setPicker(ColumnPicker picker);
    
    /** Update the current picker */
    public void updatePicker(ColumnPicker picker);
    
    /** Add a listener */
    public void addColumnPickerConfigPanelListener(ColumnPickerConfigPanelListener listener);
    
    /** Remove a listener */
    public void removeColumnPickerConfigPanelListener(ColumnPickerConfigPanelListener listener);
}