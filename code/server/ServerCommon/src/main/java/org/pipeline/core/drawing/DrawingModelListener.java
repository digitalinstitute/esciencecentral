package org.pipeline.core.drawing;

/**
 * Classes that implement this interface DrawingModelListenernts from a DrawingModel.
 * @author  hugo
 */
public interface DrawingModelListener {
    /** A block has been selected */
    public void objectSelected(DrawingModelEvent e);
    
    /** A block has been unselected */
    public void objectUnSelected(DrawingModelEvent e);
    
    /** Request editing for a block */
    public void editRequest(DrawingModelEvent e);
    
    /** Request execution from a block */
    public void executeRequest(DrawingModelEvent e) throws DrawingException;
    
    /** Request execution from a block in an exising Thread */
    public void inThreadExecuteRequest(DrawingModelEvent e) throws DrawingException;
    
    /** A block has been added */
    public void blockAdded(DrawingModelEvent e);
    
    /** A block has been removed */
    public void blockRemoved(DrawingModelEvent e);  
    
    /** A connection has been added */
    public void connectionAdded(DrawingModelEvent e);
    
    /** A connection has been removed */
    public void connectionRemoved(DrawingModelEvent e);
    
    /** Drawing wants a re-draw */
    public void redrawRequested(DrawingModelEvent e);
    
    /** Drawing wants to send a signal */
    public void signalRequested(DrawingModelEvent e);
}
