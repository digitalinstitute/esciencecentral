package org.pipeline.core.xmlstorage.xmldatatypes;

import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class serializes AutoPersistable objects
 * @author  hugo
 */
public class XmlAutoStorableDataObject extends XmlDataObject {
    /** Class name to recreate */
    private String className = "";
    
    /** Data store containing class data */
    //private XmlDataStore myClassData = null;
    private XmlDataStore classData = null;
    
    /** Creates a new instance of XmlAutoStorableDataObject */
    public XmlAutoStorableDataObject() {
    }
    
    /** Creates a new instance of XmlAutoStorableDataObject with a name */
    public XmlAutoStorableDataObject(String strName) {
        super(strName);
    }

    /** Creates a new instance of XmlAutoStorableDataObject with a name, class data and a class name */
    public XmlAutoStorableDataObject(String name, XmlDataStore classData, String className) {
        super(name);
        this.className = className;
        this.classData = (XmlDataStore)classData.getCopy();
    }

    /** Add contents to an XmlElement */
    public void appendToXmlElement(Document xmlDocument, Element xmlElement, boolean includeDescription) throws XmlStorageException {
    }
    
    /** Recreate from an XmlElement */
    public void buildFromXmlElement(Element xmlElement) throws XmlStorageException {
    }
    
    /** Create an exact copy of this object */
    public XmlDataObject getCopy() {
        XmlAutoStorableDataObject copy = new XmlAutoStorableDataObject();
        copy.setDescription(getDescription());
        copy.setCategory(getCategory());
        copy.setOptions(getOptions());
        return copy;
    }
    
    /** Return the type label as used in the XML document */
    public String getTypeLabel() {
        return "XmlAutoStorable";
    }
    
    /** Return the value as an object reference. This method instantiates the object using the stored class data and the class name */
    public Object getValue() {
        return null;
    }
    
    /** Set the value as an object reference. This method tries to persist the object using the autopersist framework to an XmlDataStore */
    public void setValue(Object objectValue) throws XmlStorageException {
    }

    @Override
    public Object getDefaultValue() throws XmlStorageException {
        throw new XmlStorageException("Default value not supported for: " + getClass().getSimpleName());
    }

    @Override
    public boolean containsNonDefaultValue() {
        return true;
    }
    
    
}
