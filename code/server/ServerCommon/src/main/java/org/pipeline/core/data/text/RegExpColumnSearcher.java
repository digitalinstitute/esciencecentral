package org.pipeline.core.data.text;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.DataException;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class searches a text column using a regular expression. It returns
 * a list of matching and non-matching row numbers
 * @author nhgh
 */
public class RegExpColumnSearcher {
    /** Column to search */
    private Column column;
    
    /** Matching rows */
    private Vector matchingRows = new Vector();
    
    /** Non-matching rows */
    private Vector excludedRows = new Vector();
    
    /** Text pattern */
    private String patternText;
    
    /** Creates a new instance of RegExpColumnSearcher */
    public RegExpColumnSearcher(Column column) {
        this.column = column;
    }
    
    /** Do the search and return the list of matching rows */
    public Vector search(String patternText) throws DataException {
        if(column!=null){
            this.patternText = patternText;
            matchingRows.clear();
            excludedRows.clear();
            Pattern pattern = null;

            // Create the pattern
            try {
                pattern = Pattern.compile(patternText);
            } catch (Exception e){
                throw new DataException("Cannot compile pattern: " + e.getLocalizedMessage());
            }

            int size = column.getRows();
            Matcher match;
            
            for(int i=0;i<size;i++){
                try {
                    if(!column.isMissing(i)){
                        match = pattern.matcher(column.getStringValue(i));
                        if(match.matches()){
                            matchingRows.addElement(new Integer(i)); 
                        } else {
                            excludedRows.addElement(new Integer(i));
                        }
                    } else {
                        // Missing values go to excluded rows
                        excludedRows.addElement(new Integer(i));
                    }
                    
                    
                } catch (Exception e){
                    // Errors get added to the excluded rows
                    excludedRows.addElement(new Integer(i));
                }
            }
            
            return matchingRows;
            
        } else {
            throw new DataException("No data present");
        }
    }
    
    /** Get the matching row indices */
    public Vector getMatchingRows(){
        return matchingRows;
    }
    
    /** Get the non-matching row indices */
    public Vector getExcludedRows(){
        return excludedRows;
    }
}
