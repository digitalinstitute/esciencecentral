package org.pipeline.core.xmlstorage;

/**
 * This is the general purpose Exception thrown by the xmlstorage package
 * @author  hugo
 */
public class XmlStorageException extends Exception
{
    /**
     * Class version UID.
     * 
     * Please increment this value whenever your changes may cause 
     * incompatibility with the previous version of this class. If unsure, ask 
     * one of the core development team or read:
     *   http://docs.oracle.com/javase/6/docs/api/java/io/Serializable.html
     * and
     *   http://docs.oracle.com/javase/6/docs/platform/serialization/spec/version.html#6678
     */
    private static final long serialVersionUID = 1L;


    /**
     * Creates a new instance of <code>XmlStorageException</code> without detail message.
     */
    public XmlStorageException() {
    }


    /**
     * Constructs an instance of <code>XmlStorageException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public XmlStorageException(String msg) {
        super(msg);
    }


    public XmlStorageException(Throwable cause) {
        super(cause);
    }


    public XmlStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
