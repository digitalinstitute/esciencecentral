package org.pipeline.core.util.block;

import org.pipeline.core.data.manipulation.ColumnPicker;

/**
 * This interface defines a listener for a column picker
 * configuration panel.
 * @author hugo
 */
public interface ColumnPickerConfigPanelListener {
    /** A picker has been edited */
    public void configPanelPickerUpdated(ColumnPicker picker);
}