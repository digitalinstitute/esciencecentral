package org.pipeline.core.data.maths;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;

import java.util.ArrayList;
import java.util.List;

/**
 * This class calculates a histogram for a column of numerical data. It returns
 * a Data object with the first three columns the min,centre.max of the range and 
 * this fourth column the number of hits in each range
 * @author hugo
 */
public class HistogramCalculator {
    /** Column to calculate the histogram on */
    private NumericalColumn column;

    /** Number of bins */
    private int binCount = 10;
    
    /** List of ranges */
    private List<NumericalRange> ranges = new ArrayList<>();
    
    public HistogramCalculator(NumericalColumn column) {
        this.column = column;
    }

    public void setBinCount(int binCount) {
        this.binCount = binCount;
    }

    public int getBinCount() {
        return binCount;
    }
    
    /** Produce a histogram */
    public Data getHistogramData() throws DataException {
        buildBins();
        int count;
        double binStart;
        double binEnd;
        double binCentre;
        Data histogram = new Data();
        histogram.addColumn(new DoubleColumn("LowerBound"));
        histogram.addColumn(new DoubleColumn("MidPoint"));
        histogram.addColumn(new DoubleColumn("UpperBound"));
        histogram.addColumn(new DoubleColumn("HitCount"));
        
        int pos = 0;
        for(NumericalRange r : ranges){
            count = r.countHits(column);
        
            binStart = r.getLowerBound();
            binEnd = r.getUpperBound();
            binCentre = binStart + ((binEnd - binStart) / 2.0);
        
            ((DoubleColumn)histogram.column(0)).appendDoubleValue(binStart);
            ((DoubleColumn)histogram.column(1)).appendDoubleValue(binCentre);
            ((DoubleColumn)histogram.column(2)).appendDoubleValue(binEnd);
            ((DoubleColumn)histogram.column(3)).appendDoubleValue(count);
        }
        
        return histogram;
    }
    
    /** Build the bins */
    private void buildBins() throws DataException {
        if(((Column)column).getNonMissingRows() > 0){
            MinValueCalculator minCalc = new MinValueCalculator(column);
            MaxValueCalculator maxCalc = new MaxValueCalculator(column);

            double minValue = minCalc.doubleValue();
            double maxValue = maxCalc.doubleValue();
            
            double span = maxValue - minValue;
            double binWidth = span / (double)binCount;
            double startPos = minValue;
            ranges.clear();;
            NumericalRange r;
            for(int i=0;i<binCount;i++){
                r = new NumericalRange();
                r.setLowerBound(startPos);
                r.setUpperBound(startPos + binWidth);
                startPos+=binWidth;
                ranges.add(r);
            }
        } else {
            throw new DataException("Column contains no non-missing rows");
        }
    }
}
