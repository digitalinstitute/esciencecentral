package org.pipeline.core.data.maths;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;

/**
 * This class unscales a unit variance column.
 * @author hugo
 */
public class UnitVarianceColumnUnScaler {
    /** Column to be unscaled */
    private Column scaledColumn;
    
    /** Mean of unscaled data */
    private double mean;
    
    /** Standard deviation of unscaled data */
    private double std;
    
    /** Creates a new instance of UnitVarianceColumnUnScaler */
    public UnitVarianceColumnUnScaler(NumericalColumn column, double mean, double std) {
        this.scaledColumn = (Column)column;
        this.mean = mean;
        this.std = std;
    }
    
    /** Get the unscaled column */
    public DoubleColumn unscaledColumn() throws DataException {
        DoubleColumn unscaledColumn = new DoubleColumn();
        unscaledColumn.setName(scaledColumn.getName());
        NumericalColumn numberColumn = (NumericalColumn)scaledColumn;
        double value;
        int size = scaledColumn.getRows();
        for(int i=0;i<size;i++){
            if(!scaledColumn.isMissing(i)){
                value = numberColumn.getDoubleValue(i);
                unscaledColumn.appendDoubleValue((value * std) + mean);
            } else {
                unscaledColumn.appendObjectValue(MissingValue.get());
            }
        }
        return unscaledColumn;
    }
}
