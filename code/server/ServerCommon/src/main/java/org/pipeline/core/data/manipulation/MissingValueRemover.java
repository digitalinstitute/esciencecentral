package org.pipeline.core.data.manipulation;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;

/**
 * This class removes rows that contain any missing values from a Data set
 * @author hugo
 */
public class MissingValueRemover {
    Data input;

    private boolean filterNaN = true;
    private boolean filterInf = true;
   
    public MissingValueRemover(Data input) {
        this.input = input;
    }
    
    public Data removeMissingRows() throws DataException {
        Data output = input.getEmptyCopy();        
        boolean missingFound;
        int rows = input.getSmallestRows();
        int cols = input.getColumns();
        Column col;
        double value;
        
        for(int i=0;i<rows;i++){
            missingFound = false;
            for(int j=0;j<cols;j++){
                col = input.column(j);
                if(col instanceof NumericalColumn){
                    if(!col.isMissing(i)){
                        // Other checks for numerical data
                        if(filterNaN || filterInf){
                            value = ((NumericalColumn)col).getDoubleValue(i);
                            if(filterNaN && Double.isNaN(value)){
                                missingFound = true;
                            }

                            if(filterInf && Double.isInfinite(value)){
                                missingFound = true;
                            }
                        }
                    } else {
                        missingFound = true;
                    }
                    
                } else {
                    // Just check for missing values
                    if(col.isMissing(i)){
                        missingFound = true;
                    }
                }
            }
            
            // Add row if no missings found
            if(missingFound==false){
                for(int j=0;j<cols;j++){
                    output.column(j).appendObjectValue(input.column(j).copyObjectValue(i));
                }         
            }               
        }
        return output;
    }
     
}
