package org.pipeline.core.xmlstorage.autopersist;

import org.pipeline.core.xmlstorage.XmlAutoStorable;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * Automatically persists XmlStorable objects using reflection
 * @author  hugo
 */
public class XmlAutoPersister {
    /** Object being persisted */
    private XmlAutoStorable object = null;
    
    /** Creates a new instance of XmlAutoPersister */
    public XmlAutoPersister(XmlAutoStorable object) {
        this.object = object;
    }
    
    /** Persist object */
    public XmlDataStore persist() throws XmlStorageException {
        XmlDataStore store = new XmlDataStore();
        try {
            new XmlStorablePersister(store, this.object).persist();
        } catch (Exception e){
            throw new XmlStorageException(e.getMessage());
        }
        return store;
    }
}
