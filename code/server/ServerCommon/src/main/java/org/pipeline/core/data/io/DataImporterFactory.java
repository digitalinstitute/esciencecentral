package org.pipeline.core.data.io;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * This class defines a factory that can be used to create data import
 * objects. It is provided so that multiple data import types can be
 * added. Installers can register importers with this factory.
 * @author hugo
 */
public abstract class DataImporterFactory {
    /** Registered importers */
    private static Hashtable importers;
    
    /** Register an importer */
    public static void registerImporter(DataImporterInfo info){
        importers.put(info.getId(), info);
    }
    
    /** Unregister an importer */
    public static void unregisterImporter(String id){
        importers.remove(id);
    }
    
    /** Create an importer */
    public static DataImporter createImporter(String id) throws DataImportException {
        if(importers.containsKey(id)){
            try {
                DataImporterInfo info = (DataImporterInfo)importers.get(id);
                return (DataImporter)info.getImporterClass().newInstance();
            } catch (Exception e){
                throw new DataImportException("Cannot create specified importer: " + e.getMessage());
            }
        } else {
            throw new DataImportException("Specified data importer does not exist");
        }
    }
    
    /** Does a specified importer have an editor */
    public static boolean importerHasEditor(String id) {
        if(importers.containsKey(id)){
            DataImporterInfo info = (DataImporterInfo)importers.get(id);
            if(info.getEditorClass()!=null){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
        
    }
    
    /** Does a specified importer have an editor */
    public static boolean importerHasEditor(DataImporter importer) {
        DataImporterInfo info = findInfoForImporter(importer);
        if(info!=null){
            if(info.getEditorClass()!=null){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /** Create an editor for an importer */
    public static DataImporterEditorPanel createEditor(DataImporter importer) throws DataImportException {
        DataImporterInfo info = findInfoForImporter(importer);
        if(info!=null){
            try {
                DataImporterEditorPanel editor = (DataImporterEditorPanel)info.getEditorClass().newInstance();
                editor.setImporter(importer);
                return editor;
                
            } catch (Exception e){
                throw new DataImportException("Cannot create specified importer");
            }
            
        } else {
            throw new DataImportException("Cannot find information for specified importer");
        }
    }
    
    /** Find the importer info object corresponding to a DataImporter instance */
    public static DataImporterInfo findInfoForImporter(DataImporter importer) {
        Enumeration e = importers.elements();
        DataImporterInfo info;
        while(e.hasMoreElements()){
            info = (DataImporterInfo)e.nextElement();
            if(info.getImporterClass().equals(importer.getClass())){
                return info;
            }
        }
        return null;
    }
    
    /** List all of the importers */
    public Enumeration getImporters(){
        return importers.elements();
    }
}
