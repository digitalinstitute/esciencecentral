package org.pipeline.core.drawing.model;

import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.PortDataSource;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.TransferData;

/**
 * This data source keeps a reference to data in memory.
 * @author hugo
 */
public class DefaultMemoryPortDataSource implements PortDataSource {
    /** Data object */
    private TransferData data = null;
    
    public TransferData getData(PortModel port) throws DrawingException {
        return data;
    }

    public void clearData(PortModel port) {
        data = null;
    }

    public void setData(PortModel port, TransferData data) {
        this.data = data;
    }

    public boolean containsData(PortModel port) throws DrawingException {
        if(data!=null){
            return true;
        } else {
            return false;
        }
    }
    
    
}