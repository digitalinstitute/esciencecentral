package org.pipeline.core.drawing;

/**
 * This interface defines the functionality of a connection that goes between
 * two PortModels.
 * @author  hugo
 */
public interface ConnectionModel extends DrawingObject {
    /** Source port */
    public OutputPortModel getSourcePort();
    
    /** Destination port */
    public InputPortModel getDestinationPort();
}
