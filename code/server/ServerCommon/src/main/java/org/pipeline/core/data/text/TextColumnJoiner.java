package org.pipeline.core.data.text;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.columns.StringColumn;

/**
 * This class joins two columns of data together to produce a new text column
 * @author nhgh
 */
public class TextColumnJoiner {
    /** Left hand column */
    private Column leftColumn;

    /** Right hand column */
    private Column rightColumn;

    public TextColumnJoiner(Column leftColumn, Column rightColumn) {
        this.leftColumn = leftColumn;
        this.rightColumn = rightColumn;
    }

    public StringColumn joinColumns(boolean includeDelimiter, String delimiterText, String newName) throws DataException, IndexOutOfBoundsException {
        StringColumn results = new StringColumn(newName);
        int rows;
        if(leftColumn.getRows()<rightColumn.getRows()){
            rows = leftColumn.getRows();
        } else {
            rows = rightColumn.getRows();
        }
        String value;
        for(int i=0;i<rows;i++){

            if(leftColumn.isMissing(i)==false && rightColumn.isMissing(i)==false){
                if(includeDelimiter){
                    value = leftColumn.getStringValue(i) + delimiterText + rightColumn.getStringValue(i);
                } else {
                    value = leftColumn.getStringValue(i) + rightColumn.getStringValue(i);
                }
                results.appendStringValue(value);
            } else {
                results.appendObjectValue(MissingValue.get());
            }
        }
        return results;
    }
}
