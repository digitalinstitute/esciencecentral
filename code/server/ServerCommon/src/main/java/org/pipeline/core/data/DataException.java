package org.pipeline.core.data;

/**
 * This exception is thrown by classes in the data package
 * @author  hugo
 */
public class DataException extends java.lang.Exception {
    
    /**
     * Creates a new instance of <code>DataException</code> without detail message.
     */
    public DataException() {
    }
    
    
    /**
     * Constructs an instance of <code>DataException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DataException(String msg) {
        super(msg);
    }
    
    public DataException(String msg, Throwable cause){
        super(msg, cause);
    }
}
