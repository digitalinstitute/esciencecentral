package org.pipeline.core.data.io;

/**
 * This interface defines a data importer that can be used to load
 * data into the program. It supports getting and setting properties
 * as XmlDataStore objects.
 * @author hugo
 */
public interface DataImporter {
    
}
