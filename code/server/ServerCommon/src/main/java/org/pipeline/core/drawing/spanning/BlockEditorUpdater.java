package org.pipeline.core.drawing.spanning;

// <editor-fold defaultstate="collapsed" desc=" Imports ">
import org.pipeline.core.drawing.BlockModel;
// </editor-fold>

/**
 * This thread is started to update a drawing editor on a separate thread. It
 * can be started using the Swing.invokeLater style method call.
 * @author hugo
 */
public class BlockEditorUpdater implements Runnable {
    /** Block that will have its editor window updated */
    private BlockModel block = null;
    
    /** Creates a new instance of BlockEditorUpdater */
    public BlockEditorUpdater(BlockModel block) {
        this.block = block;
    }
    
    /** Run this thread */
    public void run(){
        if(block!=null && block.getEditor()!=null){
            block.getEditor().blockExecuted();
        }
    }
}
