package org.pipeline.core.data.manipulation.text;

/**
 * This class provides a column picker that selects text values based
 * upon a specified Java regular expression.
 * @author nhgh
 */
public class RegExpTextColumnPicker {
    
    /** Creates a new instance of RegExpTextColumnPicker */
    public RegExpTextColumnPicker() {
    }
    
}
