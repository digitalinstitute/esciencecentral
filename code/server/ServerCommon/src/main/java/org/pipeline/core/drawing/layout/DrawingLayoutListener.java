package org.pipeline.core.drawing.layout;

/**
 * Classes that implement this interface can listen for changes
 * to the drawing layout
 * @author hugo
 */
public interface DrawingLayoutListener {
    /** The position of one or more objects has changed */
    public void layoutChanged();
}
