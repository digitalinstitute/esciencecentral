package org.pipeline.core.xmlstorage.io;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.OutputStream;

/**
 * This class just outputs an XmlDataStore to a Stream
 * @author  hugo
 */
public class XmlDataStoreStreamWriter extends XmlDataStoreReadWriter {
    
    /** Creates a new instance of XmlDataStoreStreamWriter */
    public XmlDataStoreStreamWriter(XmlDataStore dataStore) throws XmlStorageException {
        super(dataStore);
    }
    
    /** Write to an output stream */
    public void write(OutputStream stream) throws XmlStorageException {
        try{
            // Write XML to a string
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();

            DOMSource source = new DOMSource(getDocument());
            StreamResult result = new StreamResult(stream);
            t.transform(source, result);
            stream.flush();
        } catch (Exception e) {
            throw new XmlStorageException("Error writing XML to PrintStream: " + e.getMessage());
        }        
    }

    public void prettyPrint(OutputStream stream) throws XmlStorageException
    {
        try {
            DOMImplementationLS lsImpl = 
                    (DOMImplementationLS)DOMImplementationRegistry
                        .newInstance()
                        .getDOMImplementation("LS");
            LSSerializer writer = lsImpl.createLSSerializer();
            writer.getDomConfig().setParameter("format-pretty-print", true);
            LSOutput output = lsImpl.createLSOutput();
            output.setByteStream(stream);
            writer.write(getDocument(), output);
            stream.flush();
        } catch (Exception e) {
            throw new XmlStorageException("Error writing XML to PrintStream: ", e);
        }
    }
}
