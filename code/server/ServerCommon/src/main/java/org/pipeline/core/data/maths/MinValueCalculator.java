package org.pipeline.core.data.maths;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.NumericalColumn;

/**
 * Calculates the minimum value of a numerical column
 * @author hugo
 */
public class MinValueCalculator {
    /** Column to calculate */
    private Column column;
    
    /** Index of the minimum value */
    private int minValueIndex = 0;
    
    /** Creates a new instance of MinValueCalculator */
    public MinValueCalculator(NumericalColumn column) {
        this.column = (Column)column;
    }
    
    /** Calculate the minimum value */
    public double doubleValue(){
        int size = column.getRows();
        int count = 0;
        double min = Double.MAX_VALUE;
        double v;
        NumericalColumn numberCol = (NumericalColumn)column;
        
        for(int i=0;i<size;i++){
            if(!column.isMissing(i)){
                try {
                    v = numberCol.getDoubleValue(i);

                    if(v<min){
                        min = v;
                        minValueIndex = i;
                    }
                    count++;
                } catch (Exception e){
                    // Thrown by missing
                }
            }
        }
        
        if(count>0){
            return min;        
        } else {
            return Double.NaN;
        }
    }
    
    /** Get the index of the minimum value */
    public int getIndexOfMinimumValue(){
        return minValueIndex;
    }
    
    /** Return the minimum value as an integer */
    public int intValue(){
        return (int)doubleValue();
    }
    
    /** Return the minimum value as a long */
    public long longValue(){
        return (long)doubleValue();
    }    
}
