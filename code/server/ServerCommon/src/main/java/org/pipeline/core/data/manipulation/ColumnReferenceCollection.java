package org.pipeline.core.data.manipulation;

import org.pipeline.core.data.DataException;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class contains a set of column references that can be used to construct
 * a ColumnPickerCollection. This collection can be set to pick based on column
 * name or column index.
 * @author hugo
 */
public class ColumnReferenceCollection implements Serializable {
    private static final long serialVersionUID = 1L;
    private ArrayList<ColumnReference> references = new ArrayList<>();

    public ColumnReferenceCollection() {
    }
    
    public void addReference(ColumnReference reference){
        references.add(reference);
    }
    
    public ColumnPickerCollection createIndexBasedPickers() throws DataException {
        ColumnPickerCollection pickers = new ColumnPickerCollection();
        for(ColumnReference r : references){
            pickers.addColumnPicker(new ColumnPicker("#" + r.getIndex()));
        }
        return pickers;
    }
    
    public ColumnPickerCollection createNameBasedPickers() throws DataException {
        ColumnPickerCollection pickers = new ColumnPickerCollection();
        for(ColumnReference r : references){
            pickers.addColumnPicker(new ColumnPicker(r.getName()));
        }
        return pickers;
    }
}