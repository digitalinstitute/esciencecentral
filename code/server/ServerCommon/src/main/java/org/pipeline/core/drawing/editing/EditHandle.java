package org.pipeline.core.drawing.editing;

/**
 * This class provides an editing handle that can be used to move and re-size
 * drawing objects.
 * @author  hugo
 */
public class EditHandle {
    /** Top left handle */
    public static final int TOP_LEFT_HANDLE = 0;
    
    /** Top right handle */
    public static final int TOP_RIGHT_HANDLE = 1;
    
    /** Bottom left handle */
    public static final int BOTTOM_LEFT_HANDLE = 2;
    
    /** Bottom right handle */
    public static final int BOTTOM_RIGHT_HANDLE = 3;
    
    /** Free standing handle */
    public static final int FREE_STANDING_HANDLE = 4;
    
    /** Handle type */
    private int handleType = TOP_LEFT_HANDLE;
    
    /** Creates a new instance of EditHandle */
    public EditHandle() {
    }
}
