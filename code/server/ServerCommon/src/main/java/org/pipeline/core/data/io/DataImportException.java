package org.pipeline.core.data.io;

/**
 * This Exception is thrown by data import operations
 * @author hugo
 */
public class DataImportException extends Exception {
    
    /** Creates a new instance of DataImportException */
    public DataImportException() {
        super();
    }
    
    /** Creates a new instance of DataImportException */
    public DataImportException(String msg) {
        super(msg);
    }

    DataImportException(String msg, Throwable cause){
        super(msg, cause);
    }
}
