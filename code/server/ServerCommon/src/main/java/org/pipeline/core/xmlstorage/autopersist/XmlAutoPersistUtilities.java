package org.pipeline.core.xmlstorage.autopersist;

import org.pipeline.core.xmlstorage.XmlAutoStorable;

/**
 * Contains miscellaneous utility methods that are used by the XmlAutoPersist code.
 * @author  hugo
 */
public abstract class XmlAutoPersistUtilities {
    /** Is an object XmlAutoStorable */
    public static boolean isAutoStorable(Object object) {
        try {
            return (object instanceof XmlAutoStorable);
        } catch (Exception e) {
            return false;
        }
    }
}
