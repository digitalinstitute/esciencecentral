package org.pipeline.gui.xmlstorage;

import org.pipeline.core.xmlstorage.XmlDataObject;

/**
 * Classes that implement this interface can act as XmlDataObject editors
 * @author hugo
 */
public interface XmlDataObjectEditor {
    /** Set the object being edited */
    public void setObject(XmlDataObject object);
    
    /** Update the object */
    public void updateValue();
 
    /** Reset the edited value back to its original value */
    public void resetValue();
    
    /** Set the width of the caption */
    public void setCaptionWidth(int captionWidth);
    
    /** Set the parent editor window */
    public void setParent(XmlDataStoreEditor parent);
    
    /** The helper data for this object has changed */
    public void helperDataChanged();
}
