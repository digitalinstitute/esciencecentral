package org.pipeline.core.data.manipulation.functions;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class provides a column picker that applies a moving average
 * filter to the selected data column.
 * @author hugo
 */
public class MovingAverageFilterColumnPicker extends ColumnPicker {
    /** Moving average window length */
    private int windowLength = 10;
    
    /** Creates a new instance of MovingAverageFilterColumnPicker */
    public MovingAverageFilterColumnPicker() {
        super();
        setLimitColumnTypes(true);
        addSupportedColumnClass(DoubleColumn.class);
        addSupportedColumnClass(IntegerColumn.class);        
        setCopyData(false);        
    }

    /** Get the moving average window length */
    public int getWindowLength() {
        return windowLength;
    }

    /** Set the moving average window length */
    public void setWindowLength(int windowLength) {
        if(windowLength>0){
            this.windowLength = windowLength;
        }
    }
    
    /** Pick and scale the column */
    public Column pickColumn(Data data) throws IndexOutOfBoundsException, DataException {
        Column c = super.pickColumn(data);
        if(c instanceof NumericalColumn){
            NumericalColumn raw = (NumericalColumn)c;
            int rows = c.getRows();
            DoubleColumn filtered = new DoubleColumn();
            filtered.setName(c.getName() + "_Filtered");
            
            // Create a data window
            double[] window = new double[windowLength];
            
            
            
            return filtered;
            
        } else {
            throw new DataException("Filters can only operate on numerical columns");
        }
    }
    
    /** Shuffle the points of a vector along by one and add a new data point in position 0 */
    private void shiftWindow(double[] window, double dataPoint){
        int length = window.length;
        for(int i=length-1;i>0;i--){
            window[i]=window[i-1];
        }
        window[0]=dataPoint;
    }
    
    
    /** Recreate from storage */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        super.recreateObject(xmlDataStore);
        windowLength = xmlDataStore.intValue("WindowLength", 10);
    }

    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("WindowLength", windowLength);
        return store;
    }        
}
