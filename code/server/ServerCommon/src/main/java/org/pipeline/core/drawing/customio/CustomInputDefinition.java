package org.pipeline.core.drawing.customio;

import org.pipeline.core.drawing.BlockModel;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.model.DefaultInputPortModel;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class provides a custom input definition.
 * @author hugo
 */
public class CustomInputDefinition extends CustomPortDefinition {
    /** Is this an optional input */
    private boolean optional = false;
    
    /** Creates a new instance of CustomInputDefinition */
    public CustomInputDefinition() {
        super();
        setLocation(PortModel.LEFT_OF_BLOCK);
    }
    
    /** Create a port based on this definition */
    public PortModel createPort(BlockModel block) {
        DefaultInputPortModel port = new DefaultInputPortModel(this.getName(), getLocation(), getOffset(), block);
        if(getDataType()!=null){
            port.addDataType(getDataType());
            port.setDataTypeRestricted(true);
        } else {
            port.setDataTypeRestricted(false);
        }
        port.setOptional(optional);
        port.setStreamable(isStreamable());
        return port;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }
    
    /** Override toString method to provide useful text on the IO editor */
    public String toString(){
        StringBuffer buffer = new StringBuffer();
        buffer.append("input");
        buffer.append(": ");
        buffer.append(getName());
        buffer.append("  ");
        buffer.append("at");
        buffer.append(" ");
        buffer.append(getOffset());
        buffer.append("%  ");
        buffer.append("along");
        buffer.append(" ");
        buffer.append(getLocationText());
        return buffer.toString();
       
    }

    @Override
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("Optional", optional);
        return store;
    }

    @Override
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        super.recreateObject(xmlDataStore);
        optional = xmlDataStore.booleanValue("Optional", false);
    }
}