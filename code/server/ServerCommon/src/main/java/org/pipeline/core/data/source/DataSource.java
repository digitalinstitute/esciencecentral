package org.pipeline.core.data.source;

import org.pipeline.core.data.Data;

/**
 * This interface defines an object that can act as a source of data
 * set objects. It is used mainly for workspace objects that can
 * supply data sets such as database connections.
 * @author hugo
 */
public interface DataSource {
    /** Add a data source listener */
    public void addDataSourceListener(DataSourceListener listener);
    
    /** Remove a data source listener */
    public void removeDataSourceListener(DataSourceListener listener);
    
    /** Is the current data valid */
    public boolean dataValid();
    
    /** Get the current data */
    public Data getData();
}
