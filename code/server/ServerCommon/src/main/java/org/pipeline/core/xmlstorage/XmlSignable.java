package org.pipeline.core.xmlstorage;

import org.pipeline.core.xmlstorage.security.XmlDataStoreSignatureHelper;

/**
 * Classes that implement this interface can contain signatures and have their contents
 * signed and verified.
 * @author hugo
 */
public interface XmlSignable {
    /** Get the signature data */
    public XmlDataStoreSignatureHelper getSignatureData();
}
