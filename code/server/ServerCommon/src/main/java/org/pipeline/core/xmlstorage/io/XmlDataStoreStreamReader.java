package org.pipeline.core.xmlstorage.io;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;

/**
 * This class reads XmlDataStores from an input stream.
 * @author  hugo
 */
public class XmlDataStoreStreamReader extends XmlDataStoreReadWriter {
    /** Input stream to read */
    private InputStream inStream = null;
    
    /** Creates a new instance of XmlDataStoreStreamReader */
    public XmlDataStoreStreamReader(InputStream inStream) {
        super();
        this.inStream = inStream;
    }
    
    /** Read the XmlDataStore from the stream */
    public XmlDataStore read() throws XmlStorageException {
        if(inStream!=null){
            DocumentBuilder db = null;
            DocumentBuilderFactory dbf = null;
            Document doc = null;
            
            try {
                dbf = DocumentBuilderFactory.newInstance();
                dbf.setIgnoringElementContentWhitespace(true);
                dbf.setCoalescing(true);
                db = dbf.newDocumentBuilder();
                doc = db.parse(inStream);
            } catch (Exception e){
                throw new XmlStorageException("Error parsing XmlData");
            }
            
            // Parse the document
            parseXmlDocument(doc); 
            return getDataStore();
            
        } else {
            throw new XmlStorageException("No input stream available");
        }
    }
}
