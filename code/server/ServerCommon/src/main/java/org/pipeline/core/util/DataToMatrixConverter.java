package org.pipeline.core.util;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.NumericalColumn;
import org.pipeline.core.data.manipulation.NumericalColumnExtractor;
import org.pipeline.core.matrix.Matrix;

import java.util.Vector;
/**
 * This class converts Data objects to Matrix objects by extracting the largest
 * possible matrix of data missing out all of the non numerical columns. Missing
 * values in the Data object will be converted to NaNs in the matrix. Optionally, 
 * rows that have any missing values in them can be ignored, giving a shorter matrix.
 * @author hugo
 */
public class DataToMatrixConverter {
    /** Data being converted */
    private Data data = null;
    
    /** Creates a new instance of DataToMatrixConverter */
    public DataToMatrixConverter(Data data) {
        this.data = data;
    }
    
    /** Convert to matrix */
    public Matrix toMatrix() throws DataException, IndexOutOfBoundsException {
        NumericalColumnExtractor extractor = new NumericalColumnExtractor(data);
        
        Vector columns = extractor.extractColumns();
        int rows = extractor.getShortestNumericalColumnLength();
        int cols = columns.size();
        Matrix m = new Matrix(rows, cols);
        NumericalColumn numerical;
        Column c;
        
        for(int i=0;i<cols;i++){
            numerical = (NumericalColumn)columns.elementAt(i);
            c = (Column)columns.elementAt(i);
            
            for(int j=0;j<rows;j++){
                try {
                    if(!c.isMissing(j)){
                        m.set(j, i, numerical.getDoubleValue(j));
                    } else {
                        m.set(j, i, Double.NaN);
                    }
                } catch (Exception e){
                    m.set(j, i, Double.NaN);
                }   
            }
        }
        
        return m;
    }
    
    /** Convert to a double[][] array */
    public double[][]toDoubleArray() throws DataException, IndexOutOfBoundsException {
        return toMatrix().getArray();
    }
}
