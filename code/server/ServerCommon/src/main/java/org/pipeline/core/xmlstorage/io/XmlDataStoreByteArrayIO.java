package org.pipeline.core.xmlstorage.io;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * This class provides byte array storage / retrieval for XmlDataStores
 * @author hugo
 */
public class XmlDataStoreByteArrayIO extends XmlDataStoreReadWriter {
    /** Object data */
    private byte[] data;
    
    /** Creates a new instance of XmlDataStoreByteArrayIO */
    public XmlDataStoreByteArrayIO(XmlDataStore dataStore) throws XmlStorageException {
        super(dataStore);
    }
    
    /** Creates from a byte array */
    public XmlDataStoreByteArrayIO(byte[] data) throws XmlStorageException {
        this.data = data;
    }
    
    /** Save to byte array */
    public byte[] toByteArray() throws XmlStorageException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        PrintStream p = new PrintStream(byteStream);
        XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(getDataStore());
        writer.setDescriptionIncluded(isDescriptionIncluded());
        writer.write(p);
        p.flush();                    
        return byteStream.toByteArray();              
    }
    
    /** Access as XmlDataStore */
    public XmlDataStore toXmlDataStore() throws XmlStorageException {
        ByteArrayInputStream byteStream = new ByteArrayInputStream(data);
        XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(byteStream);
        return reader.read();
    }
}
