package org.pipeline.core.data.io;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Turn a Data object into an Excel spreadsheet
 * Created by nsjw7 on 08/09/2017.
 */
public class ExcelDataExporter {


    private HashMap<String, Data> datas = new HashMap<>();
    private boolean alphabetOrderSheets = false;

    public boolean isAlphabetOrderSheets() {
        return alphabetOrderSheets;
    }

    public void setAlphabetOrderSheets(boolean alphabetOrderSheets) {
        this.alphabetOrderSheets = alphabetOrderSheets;
    }

    public ExcelDataExporter(Data data) {

        datas.put("Sheet1", data);
    }

    public ExcelDataExporter(HashMap<String, Data> datas) {
        this.datas = datas;
    }

    public void writeToStream(OutputStream os) {
        try {
            Workbook wb = new HSSFWorkbook();

            for (String sheetName : datas.keySet()) {

                Data data = datas.get(sheetName);
                Sheet sheet = wb.createSheet(sheetName);

                CreationHelper createHelper = wb.getCreationHelper();

                //Create the labels in the first row
                Row labels = sheet.createRow((short) 0);

                for (int i = 0; i < data.getColumnCount(); i++) {
                    Column col = data.column(i);
                    Cell cell = labels.createCell(i);
                    cell.setCellValue(createHelper.createRichTextString(col.getName()));
                }

                //Now do the data itself
                for (int i = 0; i < data.getColumnCount(); i++) {
                    Column col = data.column(i);

                    for (int j = 0; j < col.getRows(); j++) {

                        Row row = sheet.getRow(j + 1);

                        if (row == null) {
                            row = sheet.createRow(j + 1);
                        }

                        Cell cell = row.createCell(i);

                        if (!col.isMissing(j)) {
                            try {
                                if (col instanceof IntegerColumn) {
                                    cell.setCellValue(((IntegerColumn) col).getIntValue(j));
                                } else if (col instanceof DoubleColumn) {
                                    cell.setCellValue(((DoubleColumn) col).getDoubleValue(j));
                                } else if (col instanceof StringColumn) {
                                    cell.setCellValue(createHelper.createRichTextString(col.getStringValue(j)));
                                } else if (col instanceof DateColumn) {
                                    cell.setCellValue(((DateColumn) col).getDateValue(j));
                                } else {
                                    throw new Exception("Unknonwn column type");
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            if (alphabetOrderSheets) {
                List<String> sheetNames = new ArrayList<>();
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    sheetNames.add(wb.getSheetName(i));
                }
                Collections.sort(sheetNames);

                for (int i = 0; i < sheetNames.size(); i++) {
                    wb.setSheetOrder(sheetNames.get(i), i);
                }
            }

            wb.setActiveSheet(0);
            wb.write(os);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
