package org.pipeline.gui.data.table;

import org.pipeline.core.data.Data;

import javax.swing.*;
import java.awt.*;


/**
 * This class provides a table that can show a set of data
 * @author hugo
 */
public class DataTable extends JPanel {
    /** Data being displayed */
    private Data data = null;
        
    /** Table scroll pane */
    private JScrollPane tableScroller;
    
    /** Minimum column width */
    private int minimumWidth = 100;
    
    /** Current JTable */
    private JTable table = null;
    
    /** Create with no data */
    public DataTable(){
        setLayout(new BorderLayout());
        tableScroller = new JScrollPane();
        tableScroller.setViewportView(new JTable());
        add(tableScroller, BorderLayout.CENTER);
    }
    
    /** Create with data */
    public DataTable(Data data){
        setLayout(new BorderLayout());
        tableScroller = new JScrollPane();
        add(tableScroller, BorderLayout.CENTER);
        setData(data);
    }
    
    /** Set the data to display */
    public void setData(Data data){
        this.data = data;
        if(data!=null){
            DataTableModel model = new DataTableModel(data);
            table = new JTable(model);
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            table.setModel(new DataTableModel(data));

            // Set preferred column widths
            int size = table.getColumnModel().getColumnCount();
            for(int i=0;i<size;i++){
                table.getColumnModel().getColumn(i).setMinWidth(minimumWidth);
            }         
            tableScroller.setViewportView(table);   
        } else {
            table = null;
            tableScroller.setViewportView(new JTable());
        }
    }
    
    /** Get the selected row. This method returns -1 if no row is selected */
    public int getSelectedRow(){
        if(table!=null){
            return table.getSelectedRow();
        } else {
            return -1;
        }
    }
}
