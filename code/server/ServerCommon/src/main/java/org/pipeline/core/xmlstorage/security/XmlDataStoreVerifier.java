package org.pipeline.core.xmlstorage.security;

import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamWriter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.X509Certificate;

/**
 * This class verifies the signature data in an XmlDataStore object.
 * @author hugo
 */
public class XmlDataStoreVerifier {
    /** Store to be verified */
    private XmlDataStore store;
    
    /** Creates a new instance of XmlDataStoreVerifier */
    public XmlDataStoreVerifier(XmlDataStore store) {
        this.store = store;
    }
    
    /** Check the signature */
    public boolean validate(X509Certificate certificate) throws XmlStorageException {
        try {
            if(store.isSigned()){
                // Get the byte data from the XmlDataStore
                ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
                PrintStream p = new PrintStream(byteStream);
                XmlDataStoreStreamWriter writer = new XmlDataStoreStreamWriter(store);
                writer.write(p);
                p.flush();                    
                byte[] objectData = byteStream.toByteArray();                    

                // Set up the signature and verify object data
                byte[] signatureData = store.getSignatureData();
                PublicKey key = certificate.getPublicKey();
                Signature signature = Signature.getInstance("SHA1withDSA");
                signature.initVerify(key);
                signature.update(objectData);
                return signature.verify(signatureData);
                                
            } else {            
                return false;
            }
            
        } catch (XmlStorageException de){
            throw de;
            
        } catch (Exception e){
            throw new XmlStorageException(e.getLocalizedMessage());
        }
        
    }
}
