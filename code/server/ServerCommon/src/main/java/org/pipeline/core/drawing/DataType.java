package org.pipeline.core.drawing;

/**
 * This class describes a data type that can be passed between blocks.
 * @author  hugo
 */
public class DataType {
    /** Name */
    private String name = "";
    
    /** Description */
    private String description = "";
    
    /** Class of the actual data that will be transferred */
    private Class dataClass = null;
    
    /** Creates a new instance of DataType */
    public DataType(String name, String description, Class dataClass) {
        this.name = name;
        this.description = description;
        this.dataClass = dataClass;
    }
    
    /** Get the name of this data type */
    public String getName(){
        return name;
    }
    
    /** Get the description of this data type */
    public String getDescrption(){
        return description;
    }
    
    /** Get the class of this data type */
    public Class getDataClass(){
        return dataClass;
    }
}
