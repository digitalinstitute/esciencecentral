package org.pipeline.gui.data.editor;

import org.pipeline.core.data.Data;

/**
 * Classes that implement this interface listen to events from the data editor
 * @author hugo
 */
public interface DataEditorListener {
    /** A column has been added or removed */
    public void columnsChanged(Data data);
    
    /** A row has been added or removed */
    public void rowsChanged(Data data);
    
    /** A value in the data has changed */
    public void dataChanged(Data data);
}
