package org.pipeline.core.data.manipulation;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
/**
 * This class extracts rows of data from a data set
 * @author nhgh
 */
public class RowExtractor {
    /** Data to extract rows from */
    private Data data;

    public RowExtractor(Data data) {
        this.data = data;
    }

    /** Extract data */
    public Data extract(int[] rowIndices) throws DataException, IndexOutOfBoundsException {
        Data results = data.getEmptyCopy();
        for(int i=0;i<rowIndices.length;i++){
            results.appendRows(extract(rowIndices[i]), true);
        }
        return results;
    }
    
    /** Extract data */
    public Data extract(Integer[] rowIndices) throws DataException, IndexOutOfBoundsException {
        Data results = data.getEmptyCopy();
        for(int i=0;i<rowIndices.length;i++){
            results.appendRows(extract(rowIndices[i]), true);
        }
        return results;
    }    

    /** Extract a single row */
    public Data extract(int rowIndex) throws DataException, IndexOutOfBoundsException {
        if(rowIndex>=0 && rowIndex<data.getSmallestRows()){
            Data results = data.getEmptyCopy();
            for(int i=0;i<data.getColumns();i++){
                results.column(i).appendObjectValue(data.column(i).copyObjectValue(rowIndex));
            }

            if (data.hasIndexColumn()) {
                results.getIndexColumn().appendObjectValue(data.getIndexColumn().getObjectValue(rowIndex));
            }

            return results;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }
}
