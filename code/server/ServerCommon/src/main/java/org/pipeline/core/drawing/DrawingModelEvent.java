package org.pipeline.core.drawing;

/**
 * This class contains information regarding an Event that has been created
 * by a DrawingModel.
 * @author  hugo
 */
public class DrawingModelEvent {
    /** Drawing causing the Event */
    private DrawingModel drawing = null;
    
    /** Block triggering the Event if there was one */
    private DrawingObject obj = null;
    
    /** Signal if there is one */
    private DrawingSignal signal = null;
    
    /** Creates a new instance of DrawingModelEvent */
    public DrawingModelEvent(DrawingModel drawing, DrawingObject obj) {
        this.obj = obj;
        this.drawing = drawing;
    }
    
    /** Creates a new instance of DrawingModelEvent */
    public DrawingModelEvent(DrawingModel drawing, DrawingObject obj, DrawingSignal signal) {
        this.obj = obj;
        this.drawing = drawing;
        this.signal = signal;
    }
    
    /** Get the drawing */
    public DrawingObject getObject(){
        return obj;
    }
    
    /** Get the block */
    public DrawingModel getDrawing(){
        return drawing;
    }
    
    /** Get the drawing signal */
    public DrawingSignal getSignal(){
        return signal;
    }
}
