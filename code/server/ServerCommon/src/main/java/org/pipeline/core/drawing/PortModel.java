package org.pipeline.core.drawing;

import java.util.Enumeration;

/**
 * This interface defines the functionality of a Port that is used
 * as an I/O point on a block.
 * @author  hugo
 */
public interface PortModel {   
    // Static typedefs
    
    /** Port located on the left of the block */
    public static final int LEFT_OF_BLOCK = 0;
    
    /** Port located on the top of the block */
    public static final int TOP_OF_BLOCK = 1;
    
    /** Port located on the right of the block */
    public static final int RIGHT_OF_BLOCK = 2;
    
    /** Port located on the bottom of the block */
    public static final int BOTTOM_OF_BLOCK = 3;
    
    /** Execution is not in progress on this port */
    public static final int PORT_IDLE = 0;
    
    /** Execution is in progress on this port */
    public static final int PORT_RUNNING = 1;
    
    /** Input port */
    public static final int INPUT_PORT = 0;
    
    /** Output port */
    public static final int OUTPUT_PORT = 1;
    
    /** Does the port support streaming */
    public boolean streamingPort();
    
    /** Get the status of the port */
    public int getStatus();
    
    /** Location of port on the block */
    public int getPortLocation();
    
    /** Distance along the edge of the block */
    public int getPortOffset();
    
    /** Maximum number of supported connections */
    public int getMaximumConnection();
    
    /** Add a connection */
    public void addConnection(ConnectionModel connection) throws DrawingException;
    
    /** Remove a connection */
    public void removeConnection(ConnectionModel connection);
    
    /** Remove all connections */
    public void removeAllConnections();
    
    /** Get the block containing this port */
    public BlockModel getParentBlock();
    
    /** Get the supported data types */
    public Enumeration supportedDataTypes();
    
    /** Is a data type supported */
    public boolean dataTypeSupported(DataType type);
    
    /** Can this port connect to the specified port */
    public boolean canConnectToPort(PortModel port);
    
    /** Get the name of this port */
    public String getName();
    
    /** Get the port type. Input port or output port */
    public int getType();
    
    /** Get all the connections */
    public Enumeration connections();
    
    /** Get the data held in this port */
    public TransferData getData() throws DrawingException;
    
    /** Can this port accept another connection */
    public boolean canAcceptAdditionalConnection();
    
    /** Get the port data source. The data source is the backend data storage. Typically this is
     * in memory, but may actually be anywhere */
     public PortDataSource getDataSource();
     
    /** Set the port data source. The data source is the backend data storage. Typically this is
     * in memory, but may actually be anywhere */
     public void setDataSource(PortDataSource dataSource);
}
