package org.pipeline.core.drawing.model;

import org.pipeline.core.drawing.ConnectionModel;
import org.pipeline.core.drawing.InputPortModel;
import org.pipeline.core.drawing.OutputPortModel;

import java.io.Serializable;

/**
 * This class provide the default implementation of a ConnectionModel.
 * @author  hugo
 */
public class DefaultConnectionModel implements Serializable, ConnectionModel {
    /** Source port */
    private OutputPortModel sourcePort;
    
    /** Destination port */
    private InputPortModel destinationPort;
    
    /** Creates a new instance of DefaultConnectionModel */
    public DefaultConnectionModel(OutputPortModel sourcePort, InputPortModel destinationPort) {
        this.sourcePort = sourcePort;
        this.destinationPort = destinationPort;
    }

    /** Override toString method */
    public String toString(){
        if(sourcePort.getParentBlock()!=null && destinationPort.getParentBlock()!=null){
            String inLabel = sourcePort.getParentBlock().toString();
            String outLabel = destinationPort.getParentBlock().toString();
            return inLabel + " --> " + outLabel;
        } else {
            return "Link";
        }
    }
    
    // =========================================================================
    // ConnectionModel implementation
    // =========================================================================
    
    /** Source port */
    public OutputPortModel getSourcePort() {
        return sourcePort;
    }

    /** Destination port */
    public InputPortModel getDestinationPort() {
        return destinationPort;
    }
}
