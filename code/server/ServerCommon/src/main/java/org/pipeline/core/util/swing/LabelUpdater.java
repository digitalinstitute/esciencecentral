package org.pipeline.core.util.swing;
import javax.swing.*;

/**
 * This class implements a Runnable interface and updates the text of a JLabel
 * @author hugo
 */
public class LabelUpdater implements Runnable {
    /** Label to update */
    private JLabel label;
    
    /** Text to set */
    private String text;
    
    /** Creates a new instance of LabelUpdater */
    public LabelUpdater(JLabel label, String text) {
        this.label = label;
        this.text = text;
    }
    
    /** Do the update */
    public void run(){
        label.setText(text);
    }
}
