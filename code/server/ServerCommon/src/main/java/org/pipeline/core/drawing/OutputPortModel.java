package org.pipeline.core.drawing;

/**
 * This interface describes functionality that is specific to an output port.
 * @author  hugo
 */
public interface OutputPortModel extends PortModel {
    /** Clear all the data in the output port */
    public void clearData();
    
    /** Set the data in the output port. In cases where a block streams
     * data, this will be send straight to the connected blocks. */
    public void setData(TransferData data);
}
