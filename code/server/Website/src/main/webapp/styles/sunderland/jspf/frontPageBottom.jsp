<%
    String themeFolder = request.getParameter("themeFolder");
    String siteName = request.getParameter("siteName");
%>

<style type="text/css">

    #logoBox {
        margin-right: 20px;
    }

    #logoBox img {
        padding-bottom: 10px;
        width: 143px;
        margin: 0;
    }

        /*#logoBox :first-child {*/
        /*padding-right: 0;*/
        /*}*/

        /*#logoBox :last-child {*/
        /*padding-left: 0;*/
        /*}*/


</style>

<div class="grid_5 content rightBox" id="logoBox">
    <img src="../../styles/sunderland/images/nsc.jpg" style="width:190px" alt="NSC"/>
    <img src="../../styles/sunderland/images/poweredby.jpg" style="width:190px" alt="Powered By Inkspot"/>
</div>

<div class="grid_7 content leftBox">
    <p>Developing innovative products and services requires collaboration between companies, customers and partners.
        This is especially important in the scientific domain, where increasing
        technical complexity makes collaboration challenging.
    </p>

    <p>

        <%=siteName%> has developed a collaborative platform to meet these needs in a cost-effective way. Our
        cloud-based services combine powerful data management and analysis, workflow process
        definition and automation, and the ability to share and collaborate in a secure, reliable and scalable
        environment.
    </p>


</div>