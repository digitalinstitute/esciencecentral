<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

<%@page import="com.connexience.server.model.workflow.*" %>
<%@page import="com.connexience.server.model.image.*" %>
<%@page import="com.connexience.server.ejb.util.*" %>
<%@page import="java.util.*" %>
<%@ page import="com.connexience.server.model.folder.Folder" %>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<link rel="stylesheet" type="text/css" href="../../scripts/datatables/css/datatable.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/wfeditor/wfeditor.css">
<link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen" charset="utf-8"/>
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
<script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="../../scripts/canvas/canvas.text.js"></script>
<script language="javascript" type="text/javascript" src="../../scripts/flot/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="../../scripts/flot/jquery.flot.pie.js"></script>

<script type="text/javascript" src="../../scripts/filebrowser/filetree.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
<script type="text/javascript" src="../../scripts/canvas/optimer-bold-normal.js"></script>
<script type="text/javascript" src="../../scripts/canvas/optimer-normal-normal.js"></script>
<script type="text/javascript" src="../../scripts/jscrollpane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="../../scripts/jscrollpane/jquery.em.js"></script>
<script type="text/javascript" src="../../scripts/jscrollpane/jScrollPane.js"></script>
<script type="text/javascript" src="../../scripts/jgrowl/jquery.jgrowl_minimized.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/PerformanceDataFetcher.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingBlock.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingConnection.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingConstants.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingContextMenu.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingEditor.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingExecutionManager.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingFetcher.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingInvocation.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingObject.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingPoint.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingPort.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingProperty.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingPropertyList.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingSaver.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingUtils.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/PaletteFetcher.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingInvocationsDialog.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingBlockDebugger.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingBlockOutputViewer.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingTerminationReportDialog.js"></script>
<script type="text/javascript" src="../../scripts/wfeditor/DrawingLockManager.js"></script>
<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>  
<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>

<script type="text/javascript" src="../../scripts/wfeditor/WorkflowEditor.js"></script>
<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/wfeditor/css/WorkflowEditor.css">



<script type="text/javascript">
  var acl = null;
  var dialog =  null;
  var msgDialog =  null;
  var inputDialog = null;
  var invocations =  null;
  var workflowIds =  null;
  var manager =  null;
  var chooser =  null;
  var timeoutValue;
  $(document).ready(function () {
      acl = new AccessControl();
      dialog = new ConfirmDialog();
      msgDialog = new MessageDialog();
      invocations = new DrawingInvocationsDialog();
      workflowIds = new Array();
      manager = new DrawingExecutionManager();
      chooser = new FileChooser();
      inputDialog = new InputDialog();
      inputDialog.title = "Please enter a new name";
      
    clearTabs();
    setTab('workflowsTab', 'current');
    dialog.init("confirmdialog");
    invocations.init("drawingCanvas_drawing_invocations");
    chooser.init("chooser");
    msgDialog.init("messagedialog");
    inputDialog.init("inputdialog");
    timeoutValue = setTimeout(refreshInvocations, 5000);
    manager.drawingExecuted = function (id) {
      refreshInvocations();
    }
  });

  function deleteWorkflow(id) {
    var cb = function () {
      window.location.href = "../../pages/workflow/list.jsp";
    }
    jsonCall({id:id}, "../../servlets/workflow?method=deleteWorkflow", cb);
  }

  function showCopyWorkflowDialog(id, name){      
      inputDialog.okCallback = function(value){
          copyWorkflow(id, value);
      };
      inputDialog.show(name);
  }
  
  function copyWorkflow(id, newName) {
    var cb = function (o) {
      if (!o.error) {
        if (o.id) {
          window.location.href = "../../pages/workflow/wfeditor.jsp?id=" + o.id;
        } else {
          $.jGrowl("No workflow ID returned");
        }
      }
    }
    jsonCall({id:id, newName: newName}, "../../servlets/workflow?method=copyWorkflow", cb);
  }

  function shareWorkflow(id) {
    $('#securitydialog').remove();

    //add a new ACL
    $('body').append('<div id="securitydialog" class="acl"></div>');
    acl.init("securitydialog", _userId, id, _publicUserId);
    acl.open();
  }

  function removeWorkflow(id, name) {
    dialog.yesCallback = function () {
      deleteWorkflow(id);
    }
    dialog.noCallback = function () {
      dialog.yesCallback = null;
    }
    dialog.show("Are you sure you want to delete the workflow " + name);
  }

  function showRuns(id) {
    invocations.showDialog(id);
  }

  function refreshInvocations() {
    var cb = function (o) {
      if (!o.error) {
        var i;
        var id;
        var count;
        var div;
        for (i = 0; i < o.results.length; i++) {
          id = o.results[i].workflowId;
          count = o.results[i].count;
          div = document.getElementById("invocations_" + id);
          if (div) {
            div.innerHTML = count;

            if (div.invocationCount != undefined && div.invocationCount != count) {
              // Flash the div
              if (div.invocationCount < count) {
                // Increased
                flashClass("invocations_" + id, "invocationsIncreased", 2);
              } else {
                // Decreased
                flashClass("invocations_" + id, "invocationsDecreased", 2);
              }
              //highlightFor("invocations_" + id, "red", 2);
            }
            div.invocationCount = count;
          }
        }
        timeoutValue = setTimeout(refreshInvocations, 5000);
      }

    };
    manager.getInvocationCounts(workflowIds, cb);
  }

  function createTrigger(id) {
    chooser.okCallback = function (c) {
      var folderId = c.selectedFolderId;
      if (folderId) {
        manager.createTrigger(id, folderId, function () {
          msgDialog.show("Workflow Trigger created for folder: " + c.selectedFolderName);
        });
      }
    };
    chooser.showHomeFolder(false);
  }

  function executeWorkflow(id, pickFile) {
    if (pickFile) {
      chooser.okCallback = function (c) {
        var documentId = c.selectedFileId;
        if (documentId) {
          clearTimeout(timeoutValue);
          manager.executeWithDocumentId(id, documentId);
        }
      }
      chooser.showHomeFolder(false);

    } else {
      clearTimeout(timeoutValue);
      manager.execute(id);
    }
  }
  
  function killAll(id, name){
    dialog.yesCallback = function(){
        manager.terminateAllInvocations(id);
    };
    
    dialog.noCallback = null;
    dialog.show("Are you sure you want to kill all invocations of: " + name);
    
  }

  function highlightFor(id, color, seconds) {
    var element = document.getElementById(id)
    var origcolor = element.style.backgroundColor
    element.style.backgroundColor = color;
    var t = setTimeout(function () {
      element.style.backgroundColor = origcolor;
    }, (seconds * 1000));
  }

  function flashClass(id, className, seconds) {
    $("#" + id).addClass(className).animate({"opacity":1}, (1000 * seconds), function () {
      $("#" + id).removeClass(className);
    })
  }


   

</script>
  <%
    String listTypeParam = request.getParameter("type");
    String listType = listTypeParam;
    if (listTypeParam == null) {
      listType = "user";
    }

    //if there is a default oldProject set then show the oldProject workflows
    if (listTypeParam == null && ticket.getDefaultProjectId() != null) {
      listType = "project";
    }

    List workflows;
    if (listType.equals("user")) {
      workflows = EJBLocator.lookupObjectDirectoryBean().getOwnedObjectsOrderByTimeDesc(ticket, ticket.getUserId(), WorkflowDocument.class, START, PAGE_SIZE);
      NUM_OBJECTS = EJBLocator.lookupObjectDirectoryBean().getNumberOfOwnedObjects(ticket, ticket.getUserId(), WorkflowDocument.class);
    } else if (listType.equals("project")) {
      workflows = EJBLocator.lookupObjectDirectoryBean().getProjectObjectsOrderByTimeDesc(ticket, ticket.getUserId(), WorkflowDocument.class, START, PAGE_SIZE);
      NUM_OBJECTS = EJBLocator.lookupObjectDirectoryBean().getNumberOfProjectObjects(ticket, ticket.getUserId(), WorkflowDocument.class);
    } else {
      workflows = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), WorkflowDocument.class, START, PAGE_SIZE);
      long myWorkflows = EJBLocator.lookupObjectDirectoryBean().getNumberOfOwnedObjects(ticket, ticket.getUserId(), WorkflowDocument.class);
      long sharedWorkflows = EJBLocator.lookupObjectDirectoryBean().getNumberOfObjectsUserHasAccessTo(ticket, ticket.getUserId(), WorkflowDocument.class);
      NUM_OBJECTS = sharedWorkflows - myWorkflows;
    }

  %>
  <!-- Caption Line -->

  <%
    if (listType.equals("user")) {
  %>
  <h2>My Workflows</h2>
  <%
  } else if (listType.equals("project")) {
  %>
  <h2>Project Workflows</h2>
  <%
  } else {
  %>
  <h2>Workflows Shared with Me</h2>
  <%}%>

  <script type="text/javascript">
    $(document).ready(function(){
      $("#<%=listType%>List").addClass("cat_selected");
    })
  </script>
  <div class="row-fluid">
  <%-- Main Content Left Hand Side--%>
  <div class="span9">
    <div class="dialog" id="confirmdialog"></div>
    <div class="dialog" id="drawingCanvas_drawing_invocations"></div>
    <div class="dialog" id="chooser"></div>
    <div class="dialog" id="messagedialog"></div>
    <div class="dialog" id="inputdialog"></div>
    <ul id="workflow-list" class="unstyled">
      <%
        WorkflowDocument workflow;
        int invocationCount;
        for (int i = 0; i < workflows.size(); i++) {
          workflow = (WorkflowDocument) workflows.get(i);
          invocationCount = WorkflowEJBLocator.lookupWorkflowManagementBean().getNumberOfInvocations(ticket, workflow.getId());
      %>

      <li class="clearfix">
        <a class="wf-thumbnail" href="../../pages/workflow/wfeditor.jsp?id=<%=workflow.getId()%>">
          <img src="../../servlets/image?soid=<%=workflow.getId()%>&type=<%=ImageData.WORKFLOW_PREVIEW%>" alt="profile picture" class="img-polaroid" />
        </a>
          <h5><%if(workflow.isExternalService()){%><img src="../../styles/common/images/brick_go.png" style="padding-right:10px"/><%} else {%><img src="../../styles/common/images/brick.png"  style="padding-right:10px"/><%}%><%=workflow.getName()%></h5>
          <div class="wf-description">
        <p>
            <%=workflow.getDescription()%>
        </p>
          </div>
          <h6>
              <span id="invocations_<%=workflow.getId()%>"><%=invocationCount%></span> Running Invocations
          </h6>
          <%
              if (!workflow.getCreatorId().equals(ticket.getUserId())) {
          %>
          <span>
              Created by <%=EJBLocator.lookupObjectInfoBean().getObjectName(ticket, workflow.getCreatorId())%>
          </span>
          <%}%>
        <ul class="inline">
          <li><a href="wfeditor.jsp?id=<%=workflow.getId()%>">Edit</a></li>
          <li><a onclick="showCopyWorkflowDialog('<%=workflow.getId()%>', '<%=workflow.getName()%>');">Copy & Edit</a><li>
          <%
            if (workflow.getCreatorId().equals(ticket.getUserId())) {//My workflows
          %>
            <li><a onclick="removeWorkflow('<%=workflow.getId()%>', '<%=workflow.getName()%>');">Delete</a></li>
            <li><a onclick="shareWorkflow('<%=workflow.getId()%>');">Share</a></li>
          <%
          } else { // Shared workflows
          %>
          <%--<a style="cursor: pointer;" onclick="copyWorkflow('<%=workflow.getId()%>')">Copy & Open</a>--%>
          <%}%>
          <li><a onclick="executeWorkflow('<%=workflow.getId()%>', <%=workflow.isExternalDataSupported()%>);">Run</a></li>
            <li><a onclick="showRuns('<%=workflow.getId()%>');">Invocations</a></li>
            <li><a onclick="killAll('<%=workflow.getId()%>', '<%=workflow.getName()%>');">Kill All</a></li>
            <%
                if (workflow.isExternalDataSupported()) {
            %>
               <li>
                <a onclick="createTrigger('<%=workflow.getId()%>');">Create Trigger</a>
               </li>
            <%}%>
        </ul>
        
      </li>

      <script type="text/javascript">
          $(document).ready(function(){
              workflowIds.push('<%=workflow.getId()%>');
        document.getElementById("invocations_<%=workflow.getId()%>").invocationCount = <%=invocationCount%>;
          })
      </script>
      <%}%>

      <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>
    </ul>
  </div>


  <!-- Right Hand Side -->
    <div class="span3">
        <ul id="workflowMenu" class="unstyled sidebar-menu">
            <li>
                <h4><a href="../../pages/workflow/wfeditor.jsp?id=new"><i class="icomoon-plus"></i>Create Workflow</a></h4>
            </li>
            <li class="divider"></li>
            <li>
                <h4><a id="userList" href="../../pages/workflow/list.jsp?type=user"><i class="icomoon-user"></i>My Workflows</a></h4>
            </li>
            <%
                if (ticket.getDefaultProjectId() != null) {%>
            <li>
                <h4><a id="projectList" href="../../pages/workflow/list.jsp?type=project"><i class="icomoon-briefcase"></i>Project Workflows</a></h4>
            </li>
            <%
                }
            %>
            <li>
                <h4><a id="sharedList" href="../../pages/workflow/list.jsp?type=shared"><i class="icomoon-share"></i>Shared Workflows</a></h4>
            </li>
            <li class="divider"></li>
            <li>
                <h4><a href="../../pages/workflow/runs.jsp"><i class="icomoon-play3"></i>Running Workflows</a></h4>
            </li>
            <li>
                <h4><a href="../../pages/workflow/autorun.jsp"><i class="icomoon-alarm2"></i>Triggered Workflows</a></h4>
            </li>
        </ul>
    </div>
</div>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
