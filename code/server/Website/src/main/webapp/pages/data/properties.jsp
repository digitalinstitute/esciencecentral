<%@ page import="com.connexience.server.model.ServerObject" %>
<%@ page import="com.connexience.server.model.document.DocumentRecord" %>
<%--
    Document   : data browser
    Created on : Sep 24, 2010, 11:15:26 AM
    Author     : hugo
--%>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>
<link rel="stylesheet" type="text/css" href="../../scripts/jquery-selectbox/jquery.selectBox.css"/>

<%--QuickView--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">

<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>

<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>
<script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>

<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>



<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.esc.css">



<%
    String docId = request.getParameter("id");
    if (docId == null || docId.equals("")) {
        String url = request.getRequestURI();
        String[] parts = url.split("/");
        String info = parts[parts.length - 1];
        if (info.equals("info")) {
            docId = parts[parts.length - 2];
        } else {
            if (!url.endsWith("info")) {
                response.sendRedirect(request.getRequestURI() + "/info");
                return;
            }
        }
    }
    ServerObject so = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, docId, ServerObject.class);
%>
<script type="text/javascript" charset="utf-8">
    var viewer = new ViewerPanel();
    var editorChooser = new ViewerChooser();
    $(document).ready(function () {
//        setTab('dataTab', 'current');
        editorChooser.init("editorchooser");
        viewer.init("quickviewdiv");

    });
    <%
    if(so instanceof DocumentRecord){
    %>
        $(document).ready(function () {
            viewer.showVersions('<%=docId%>');
        });
    <%
    }
    else{
    %>
        $(document).ready(function () {
            viewer.showMetadata('<%=docId%>');
        });
    <%
    }
    %>

    function showVersions() {
        viewer.showVersions('<%=docId%>');
    }

    function showFile() {
        viewer.showFile('<%=docId%>');
    }

    function showProv() {
        viewer.showProvenance('<%=docId%>');
    }

    function showMetadata() {
        viewer.showMetadata('<%=docId%>')
    }

    function showTags() {
        viewer.showTags('<%=docId%>')
    }


</script>

<!-- Caption Line -->
<h2><%=so.getName()%></h2>


<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
        <div id="quickviewdiv"></div>
        <div id="editorchooser"></div>
    </div>


    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <%
                if (so instanceof DocumentRecord) {
            %>
            <li><h5><a onclick="showVersions()">Versions</a></h5></li>
            <li><h5><a onclick="showFile()">Viewer</a></h5></li>
            <li><h5><a onclick="showProv()">Provenance</a></h5></li>

            <%
                }
            %>
            <li><h5><a onclick="showMetadata()">Metadata</a></h5></li>
            <li><h5><a onclick="showTags()">Tags</a></h5></li>
        </ul>
    </div>

</div>

