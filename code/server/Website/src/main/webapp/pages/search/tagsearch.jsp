
<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@ page import="com.connexience.server.web.util.SearchResult" %>
<%@ page import="com.connexience.server.web.util.SearchUtil" %>
<%@ page import="java.util.List" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
  <script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
  <script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
  <script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>

  <script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
  <script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>

  <script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>

  <script type="text/javascript">
    var qv;

     $(document).ready(function ()
     {
       clearTabs();
       $("#submitSearch").button();

       qv = new QuickView();
       qv.init("quickview")
     });


  </script>


  <%
    String t = request.getParameter("t");

    //if the query is null, redirect them to the index
    if (t == null)
    {
      if (!response.isCommitted())
      {
        response.sendRedirect("../../pages/search/search.jsp");
        return;
      }
    }

    //get the search results
    List resultObjects = EJBLocator.lookupSearchBean().tagSearch(ticket, t, START, PAGE_SIZE);
    NUM_OBJECTS = EJBLocator.lookupSearchBean().countTagSearch(ticket, t);

    List<SearchResult> results = SearchUtil.formatSearchResults(ticket, resultObjects);

    long numObjOnThisPage = 10;
    if (NUM_OBJECTS < PAGE_SIZE)
    {
      numObjOnThisPage = NUM_OBJECTS;
    }

    if (NUM_OBJECTS == 0)
    {
      START = -1;
    }
  %>

  <!-- Caption Line -->
  <h2>Results</h2>
    <h4><%=START + 1%> to <%=numObjOnThisPage%> of
    about <%=NUM_OBJECTS%> for items tagged with <span style="color:#3399FF"><%=t%></span></h4>

  <div class="row-fluid">
      <div class="span9">

    <div id="quickview"></div>

    <form action="../../pages/search/tagsearch.jsp" class="searchForm">
      <input type="text" id="searchBox" size="50" name="t" value="<%=t%>" align="middle"
             style="vertical-align:text-bottom;"/>
      <button type="submit" id="submitSearch">Search</button>
      <input type="hidden" name="advSearch" value="false" id="advSearch">
    </form>

    <%
      if (publicUser && !t.equals(""))
      {
    %>

    <div style="padding:15 0 0 200px;color:gray;">
      <h4>
        More results may be available if you Login
      </h4>
    </div>

    <%
      }
    %>
  </div>

    <ul>

      <%

        for (SearchResult result : results)

        {

      %>

      <li>
        <img src="../../styles/common/images/<%=result.getImage()%>" alt="<%=result.getImageAlt()%>"
             class="listItemImg"/>

        <div class="listTopText">
          <%
            if (result.getLink() == null || result.getLink().equals(""))
            {
          %>
          <a onclick="<%=result.getJavascript()%>"><%=result.getName()%>
          </a>
          <%
          }
          else
          {
          %>
          <a href="<%=result.getLink()%>"><%=result.getName()%>
          </a>
          <%
            }
          %>
        </div>
        <div class="listMainText">
          <p>
            <%=result.getDetails()%>
          </p>
        </div>
        <div class="listBottomText">
        </div>
      </li>



      <%
        }
      %>

      <%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>
    </ul>
      <div class="span3"></div>
      </div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
