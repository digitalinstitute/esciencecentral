<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>

<%--<%@ page import="com.connexience.server.ConnexienceException" %>--%>
<%--<%@ page import="com.connexience.server.ejb.search.MetaDataSearchResult" %>--%>
<%--<%@ page import="com.connexience.server.ejb.search.MetaDataSearchResults" %>--%>
<%--<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>--%>
<%--<%@ page import="com.connexience.server.util.XPathExpression" %>--%>
<%--<%@ page import="java.util.Arrays" %>--%>
<%--<%@ page import="java.util.Vector" %>--%>
<%--<%@ page import="com.connexience.server.web.util.SearchUtil" %>--%>
<%--<%@ page import="javax.xml.xpath.XPathExpression" %>--%>

<%--<html>--%>
<%--<head>--%>
  <%--<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>--%>
  <%--<%@ include file="../../WEB-INF/jspf/page.jspf" %>--%>
  <%--<%@ include file="../../WEB-INF/jspf/page/publicredirect.jspf" %>--%>


  <%--<%--%>
    <%--String[] x = request.getParameterValues("x");--%>

    <%--String queryText = "";--%>
    <%--String badQueryText = "";--%>
    <%--Vector<XPathExpression> xpathExpressions = null;--%>

    <%--//vector to hold the separate xpath expressions--%>
    <%--Vector xpaths = new Vector();--%>
    <%--if (x != null)--%>
    <%--{//if x contains anything, add it to the xpaths.  Length should always be >= 1 as set above--%>
      <%--if (x.length > 0)--%>
      <%--{--%>
        <%--xpaths.addAll(Arrays.asList(x));--%>
      <%--}--%>

      <%--//parse the xpath expressions--%>
      <%--xpathExpressions = new Vector<XPathExpression>();--%>
      <%--for (Object obj : xpaths)--%>
      <%--{--%>
        <%--try--%>
        <%--{--%>
          <%--String xpath = (String) obj;--%>
          <%--xpathExpressions.add(SearchUtil.parseXpath(xpath));--%>
          <%--queryText += " " + xpath;--%>
        <%--}--%>
        <%--catch (ConnexienceException ce)--%>
        <%--{--%>
          <%--badQueryText += " " + obj.toString();--%>
          <%--System.err.println("Unable to parse XPATH: " + obj.toString());--%>
        <%--}--%>
      <%--}--%>
    <%--}--%>
    <%--queryText = queryText.trim();--%>
  <%--%>--%>


<%--</head>--%>


<%--<body>--%>
<%--<%--%>
  <%--try--%>
  <%--{--%>
    <%--long numObjOnThisPage = 10;--%>

    <%--//if there are no xpath expressions we can parse then go to the error page (shown if exception caught)--%>
    <%--if (x != null && xpathExpressions.size() == 0)--%>
    <%--{--%>
      <%--throw new ConnexienceException("Unable to parse xpath");--%>
    <%--}--%>

    <%--MetaDataSearchResults results = new MetaDataSearchResults();--%>
    <%--//get the search results--%>
    <%--if (x != null && x.length >= 0)--%>
    <%--{--%>
      <%--results = EJBLocator.lookupMetaDataBean().searchVisibleMetaData(ticket, xpathExpressions, START, PAGE_SIZE);--%>

      <%--NUM_OBJECTS = results.getMaxQueryResults();--%>

      <%--if (NUM_OBJECTS < PAGE_SIZE)--%>
      <%--{--%>
        <%--numObjOnThisPage = NUM_OBJECTS;--%>
      <%--}--%>

      <%--if (NUM_OBJECTS == 0)--%>
      <%--{--%>
        <%--START = -1;--%>
      <%--}--%>
    <%--}--%>
<%--%>--%>

<%--<div id="wrapper" class="container_12 clearfix">--%>
  <%--<%@include file="../../WEB-INF/jspf/page/top.jspf" %>--%>


  <%--<%--%>
    <%--if (x == null || x.length == 0)--%>
    <%--{--%>
  <%--%>--%>
  <%--<h2 class="grid_12 caption">XML MetaData Search using XPATH</h2>--%>
  <%--<%--%>
  <%--}--%>
  <%--else--%>
  <%--{--%>

  <%--%>--%>
  <%--<h2 class="grid_12 caption">Results <%=START + 1%> to <%=numObjOnThisPage%> of--%>
    <%--about <%=NUM_OBJECTS%> for <span style="color:#3399FF"><%=queryText%></span> </h2>--%>
      <%--<%}%>--%>


    <%--<div class="hr grid_12 clearfix">&nbsp;</div>--%>


    <%--&lt;%&ndash; An example List of items&ndash;%&gt;--%>
    <%--<div class="grid_12 content">--%>


      <%--<form action="../../pages/search/xpathsearch.jsp" class="searchForm">--%>
        <%--<input type="text" id="searchBox" size="50" name="x" value="" align="middle"--%>
               <%--style="vertical-align:text-bottom;"/>--%>
        <%--<button type="submit">Search</button>--%>
      <%--</form>--%>


      <%--<p>--%>

      <%--<h2>Instructions:</h2>--%>

      <%--At present you can search the XML MetaData that has been attached to documents using XPATH.--%>
      <%--You can use XPATH to match the contents of text within the XML document.--%>
      <%--XPATH searching is not case sensitive. Only the = operator can be used (we're working hard to get other--%>
      <%--operators working).--%>
      <%--<br/>--%>
      <%--Examples: <br/>--%>
      <%--//name/text()='john&nbsp;smith' matches &lt;name&gt;john&nbsp;smith&lt;/name&gt;--%>
      <%--<br/>--%>
      <%--//patient/@age="23" matches &lt;patient age="23"&gt;john&nbsp;smith&lt;/patient&gt;--%>

      <%--</p>--%>

    <%--</div>--%>

      <%--<%--%>

              <%--for (MetaDataSearchResult result : results.getResults())--%>
              <%--{--%>

            <%--%>--%>
    <%--<div class="listElement">--%>
      <%--<img src="../../styles/common/images/search/doc.png" alt="doc"--%>
           <%--class="listItemImg"/>--%>

      <%--<div class="listTopText">--%>
        <%--<a href="../secure/dataproperties.jsp?id=<%=result.getDocumentId()%>"><%=result.getDocumentName()%>--%>
        <%--</a> Linked From--%>
        <%--<a target="_blank"--%>
           <%--href="../secure/dataproperties.jsp?id=<%=result.getMetaDataDocId()%>"><%=result.getMetaDataDocName()%>--%>
        <%--</a>--%>

      <%--</div>--%>
      <%--<div class="listMainText">--%>
        <%--<p>--%>
          <%--&lt;%&ndash;;--%>
        <%--</p>--%>
      <%--</div>--%>
      <%--<div class="listBottomText">--%>
      <%--</div>--%>
    <%--</div>--%>

    <%--<div class="hr">&nbsp;</div>--%>


      <%--<%--%>
              <%--}--%>
            <%--%>--%>

    <%--<%@include file="../../WEB-INF/jspf/page/paginate.jspf" %>--%>

<%--</div>--%>


<%--<%--%>
<%--}--%>
<%--catch (ConnexienceException ce)--%>
<%--{--%>
<%--%>--%>

<%--<div id="wrapper" class="container_12 clearfix">--%>
  <%--<%@include file="../../WEB-INF/jspf/page/top.jspf" %>--%>

  <%--<!-- Caption Line -->--%>
  <%--<h2 class="grid_12 caption"> Search Error</h2>--%>

  <%--<div style="padding-left:150px; padding-bottom:5px">--%>
    <%--<form action="../../pages/search/xpathsearch.jsp">--%>
      <%--<input type="text" id="searchBox2" size="50" name="x" value="<%=badQueryText%>" align="middle"--%>
             <%--style="vertical-align:text-bottom;"/>--%>
          <%--<span id="searchbutton" class="yui-button">--%>
            <%--<span class="first-child">--%>
              <%--<button type="submit">Search</button>--%>
            <%--</span>--%>
          <%--</span>--%>
    <%--</form>--%>
  <%--</div>--%>


  <%--<div class="cell">--%>
    <%--<h2>We were unable to parse your search</h2>--%>

    <%--<div>--%>
      <%--At present you can search the XML MetaData that has been attached to documents using XPATH.--%>
      <%--You can use XPATH to match the contents of text within the XML document.--%>
      <%--XPATH searching is not case sensitive. Only the = operator can be used (we're working hard to get other--%>
      <%--operators working).--%>
      <%--<br/>--%>
      <%--Examples: <br/>--%>
      <%--//name/text()='john&nbsp;smith' matches &lt;name&gt;john&nbsp;smith&lt;/name&gt;--%>
      <%--<br/>--%>
      <%--//patient/@age="23" matches &lt;patient age="23"&gt;john&nbsp;smith&lt;/patient&gt;--%>

    <%--</div>--%>
  <%--</div>--%>

<%--</div>--%>
<%--<b class="bb"><b></b></b>--%>
<%--</div>--%>
<%--</div>--%>
<%--</div>--%>


<%--<%--%>

  <%--}--%>
<%--%>--%>


<%--</body>--%>

<%--</html>--%>
