<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

      <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
            <link rel="stylesheet" href="../../scripts/properties/PropertyEditorNoDatatables.css"/>
            <!-- Caption Line -->
            <h2>General Settings</h2>
            <h4>External Tokens</h4>

            <div class="row-fluid">
                <%-- Main Content Left Hand Side--%>
                <div class="span9">
                    <div id="tokens"></div>
                </div>

                <!-- Right Hand Side -->
                <div class="span3">
                    <%@include file="menu.jspf" %>
                </div>

            </div>


            <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

    <script type="text/javascript">
        $(document).ready(function () {
            fetchTokens();
        });
        
        function fetchTokens(){
            var cb = function(o){
                if(!o.error){
                    displayTokens(o.tokens);
                }
            };
            jsonCall({}, "../../servlets/admin?method=listTokens", cb);
        }
        
        function displayTokens(tokens){
        
            var row;
            var token;
            var html='<table id="' + this.divName + '_table" style="width: 100%;">';
            html+='<thead><tr><th>Label</th><th>Expiry</th><th>Edit</th></tr></thead><tbody>';
            for(var i=0;i<tokens.length;i++){
               token = tokens[i];
               row = '<tr>';
               row+='<td>' + token.label + '</td>';
               row+='<td>' + token.expiry + '</td>';
               row+='<td><img src="../../styles/common/images/cross.png" style="cursor:pointer" onclick="revokeToken(\'' + token.id + '\')"/></td>';
               row+= '</tr>';
               html+=row;
            }
            html+='</tbody></table>';
            document.getElementById("tokens").innerHTML = html;
        }
        
        function revokeToken(id){
            var cb = function(o){
                if(!o.error){
                    $.jGrowl("Token Revoked");
                    fetchTokens();
                }
            };
            jsonCall({id: id}, "../../servlets/admin?method=revokeToken", cb);
        }
    </script>
