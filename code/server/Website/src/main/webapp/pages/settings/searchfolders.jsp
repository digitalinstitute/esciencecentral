
<%@ page import="java.util.List" %>
<%@ page import="com.connexience.server.model.folder.*" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

   <%
        List folders = EJBLocator.lookupObjectDirectoryBean().getOwnedObjects(ticket, ticket.getUserId(), SearchFolder.class, START, PAGE_SIZE);
        NUM_OBJECTS = EJBLocator.lookupObjectDirectoryBean().getNumberOfOwnedObjects(ticket, ticket.getUserId(), SearchFolder.class);  
  %>

    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
    <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
    <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>  
    <script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
    <script type="text/javascript" src="../../scripts/metadata/MetadataManager.js"></script>
  <script type="text/javascript">

    var dialog = new InputDialog();
    var confirm = new ConfirmDialog();
    var manager = new MetadataManager();
    
    $(document).ready(function() {
        dialog.title = "Please enter a folder name";
        dialog.init("creatediv");
        
        var searchCreateCallback = function(){
            location.reload();
        }
        
        dialog.okCallback = function(value){
            manager.createSearchFolder(value, searchCreateCallback);
        }
        
        confirm.init("confirm");
        confirm.yesCallback = function(){
            manager.removeSearchFolder(this.folderId, function(){location.reload();});
        }
        manager.init("editfolder");
    });         
        
    function showFolderCreateDialog(){
        dialog.show("New Search Folder");
    }
    
    function editFolder(id){
        manager.editSearchFolder(id);
    }
    
    function deleteFolder(id){
        confirm.folderId = id;
        confirm.show("Are you sure you want to remove the selected search folder?");
    }
    
  </script>
  <!-- Caption Line -->
  <h2>Search Folders</h2>
<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
    <div id="editfolder"></div>
    <div id="confirm" class="dialog"></div>
    <div id="creatediv" class="dialog"></div>
    <ul class="unstyled" id="generic-list">

        <%
        SearchFolder folder;
        for(int i=0;i<folders.size();i++){
            folder = (SearchFolder)folders.get(i);
        %>
        
            <li class="clearfix">
                <a class="generic-picture" onclick="editFolder('<%=folder.getId()%>');">
                    <img src="../../styles/common/images/search/folder-large.png" alt="Folder icon"/>

                </a>                    
                <h6><%=folder.getName()%></h6>                


              <div class="generic-description">
                  <p><%=folder.getDescription()%></p>
              </div>
              <ul class="inline">
                  <li><a style="cursor: pointer;" onclick="editFolder('<%=folder.getId()%>');">Edit</a></li>
                  <li><a style="cursor: pointer;" onclick="deleteFolder('<%=folder.getId()%>');">Delete</a></li>
              </ul>
            </li>                
         <%}%>
        <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>
    </ul>

  </div>

        <!-- Right Hand Side -->
        <div class="span3">
            <%@include file="menu.jspf" %>
        </div>

</div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
