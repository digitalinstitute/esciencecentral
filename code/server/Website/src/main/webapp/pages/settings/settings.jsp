<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

      <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

            <!-- Caption Line -->
            <h2>General Settings</h2>
            <h4>Password Reset</h4>

            <div class="row-fluid">
            <%-- Main Content Left Hand Side--%>
            <div class="span9">
                <form id="password-reset" autocomplete="off">
                    <input type="hidden" id="changepw" name="changepw" value="true"/>
                    <label for="oldpw">Old Password</label>
                    <input type="password" id="oldpw" name="oldpw"/>
                    <label for="newpw1">New Password</label>
                    <input type="password" name="newpw1" id="newpw1"/>
                    <label for="newpw2">New Password</label>
                    <input type="password" name="newpw2" id="newpw2"/>
                    <p>
                        <input id="submitbutton" type="button" name="submit" class="btn" value="Change Password" onclick="changePassword()"/>
                    </p>
                </form>
            </div>
            
            <!-- Right Hand Side -->
            <div class="span3">
                <%@include file="menu.jspf" %>
            </div>

            </div>


            <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

    <script type="text/javascript">
        function changePassword(){
            var currentPassword = $("#oldpw").val();
            var newPassword1 = $("#newpw1").val();
            var newPassword2 = $("#newpw2").val();
            var failedUserId = _userId;
            if(newPassword1===newPassword2){
                var cb = function(o){
                    if(o.error===false){
                        flashMessage("Password changed", "success", $("#header-divider"));
                    }
                };

                var errorCb = function(message, o){
                    flashMessage(message, "error", $("#header-divider"));
                };

                jsonCall({currentPassword: currentPassword, newPassword: newPassword1, userId:failedUserId}, "../../unsecured/servlets/profile?method=resetPassword", cb, errorCb);
            } else {
                flashMessage("Passwords do not match", "error", $("#header-divider"));
            }            
        }
    </script>

