<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.model.image.ImageData" %>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>


<%
    try{

        GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

        String groupId = request.getParameter("id");
        if (groupId == null || groupId.equals(""))
        {
            response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
            return;
        }
        Group group = groupEJB.getGroup(ticket, groupId);
        if (group == null)
        {
            response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
            return;
        }

        String action = request.getParameter("action");
        if ("join".equals(action))
        {
            groupEJB.addUserToGroup(ticket, ticket.getUserId(), groupId);
        }
        if ("leave".equals(action))
        {
            groupEJB.removeUserFromGroup(ticket, ticket.getUserId(), groupId);
        }

%>

<!-- Caption Line -->
<h2><%=group.getName()%></h2>

<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <img src="../../styles/common/images/group_large.png" style="float: left;" alt="group picture"/>
        <div style="display: inline-block;margin-top:20px;margin-left:20px;">
            <p><h5><%=group.getDescription()%></h5></p>
            <hr/>
            <%
            HashMap stats = EJBLocator.lookupGroupDirectoryBean().getGroupStatistics(ticket, groupId);
            Iterator names = stats.keySet().iterator();
            String name;
            String value;
            while (names.hasNext())
            {
              name = names.next().toString();
              value = stats.get(name).toString();
            %>
            <p>
                <%=name%>: <%=value%>
            </p>

          <%}%>
        </div>
        
        



    </div>
    
    <div class="span3">
        <%@include file="menu.jspf" %>
    </div>
</div>
</div>

<!-- Right Hand Side -->


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    }
    catch(ConnexienceException e)
    {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>

