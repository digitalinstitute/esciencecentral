<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%
    try
    {
      GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

      String id = request.getParameter("id");
      String name = "";
      String desc = "";
      boolean adminApprove;
      boolean nonMembersList;
      String adminApproveText = "checked=checked";
      String nonMembersListText = "checked=checked";
      String buttonText = "Create Group";
      String h2Text = "Create a New Group";
      String action = request.getParameter("action");
      String actionText = "create";
      String idText = "";
      if (id != null)
      {
        idText = id;
      }

      if (id != null && !id.equals(""))
      {
        //editing
        Group g = groupEJB.getGroup(ticket, id);
        if (g != null)
        {
          name = g.getName();
          desc = g.getDescription();
          adminApprove = g.isAdminApproveJoin();
          nonMembersList = g.isNonMembersList();

          if (!adminApprove)
          {
            adminApproveText = "";
          }
          if (!nonMembersList)
          {
            nonMembersListText = "";
          }
          buttonText = "Save Group";
          h2Text = "Edit " + g.getName();
          actionText = "edit";
        }
      }

      if (action != null && action.equals("create"))
      {
        //creating
        name = request.getParameter("name");
        if (name != null && !name.equals(""))
        {
          desc = request.getParameter("description");
          adminApprove = request.getParameter("adminApprove") != null;
          nonMembersList = request.getParameter("nonMembersList") != null;

          Group g = new Group();
          g.setName(name);
          g.setDescription(desc);
          g.setAdminApproveJoin(adminApprove);
          g.setNonMembersList(nonMembersList);
          g = groupEJB.saveGroup(ticket, g);

          if (g.getId() != null)
          {
            response.sendRedirect("../../pages/groups/group.jsp?id=" + g.getId());
          }
        }
      }
      else if (action != null && action.equals("edit"))
      {
        Group g = groupEJB.getGroup(ticket, id);
        if (g != null)
        {
          name = request.getParameter("name");
          desc = request.getParameter("description");
          adminApprove = request.getParameter("adminApprove") != null;
          nonMembersList = request.getParameter("nonMembersList") != null;

          g.setName(name);
          g.setDescription(desc);
          g.setAdminApproveJoin(adminApprove);
          g.setNonMembersList(nonMembersList);
          g = groupEJB.saveGroup(ticket, g);

          if (g.getId() != null)
          {
            response.sendRedirect("../../pages/groups/group.jsp?id=" + g.getId());
          }
        }
      }
  %>

  <script type="text/javascript">
    $(document).ready(function ()
    {
      clearTabs();
      $('#submitButton').button();
    });
  </script>

  <!-- Caption Line -->
  <h2><%=h2Text%></h2>

  <%-- Main Content Left Hand Side--%>
<div class="row-fluid">
  <div class="span9">

    <form id="createGroupForm" method="post" action="create.jsp" class="form-inline">
        <% if (id == null){ %>
        <input name="project" type="hidden" id="project" value="true">
        <%}%>
        <input name="action" value="<%=actionText%>" type="hidden">
        <input name="id" value="<%=idText%>" type="hidden">
        <div class="control-group">
            <label>Name</label>
            <div class="controls">
                <input name="name" type="text" id="name" value="<%=name%>">
            </div>
        </div>
        <div class="control-group">
            <label>Description</label>
            <div class="controls">
                <textarea name="description" id="description">
                    <%=desc%>
                </textarea>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label checkbox">
                <input name="adminApprove" class="checkbox" type="checkbox" id="adminApprove"> Should the project owner have to approve memberships?
            </label>
        </div>
        <div class="control-group">
            <label class="control-label checkbox">
                <input name="nonMembersList" class="checkbox" type="checkbox" id="nonMembersList"> Can non-members of the project see the project members?
            </label>
        </div>
        <div class="control-group">
            <input name="submit" value="<%=buttonText%>" class="button" type="submit" id="submitButton">
        </div>
    </form>
  </div>
  <div class="span3">
      <ul class="unstyled sidebar-menu">
          <li>
              <h4><a href="../../pages/groups/grouplist.jsp"><i class="icomoon-users"></i>My Groups</a></h4>
          </li>
          <li>
              <h4><a href="../../pages/groups/grouplist.jsp?filter=all"><i class="icomoon-users2"></i>All Groups</a></h4>
          </li>
          <li>
              <h4><a href="../../pages/groups/create.jsp"><i class="icomoon-plus"></i>Create Group</a></h4>
          </li>
      </ul>
  </div>
</div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

<%
  }
  catch (Exception e)
  {
    e.printStackTrace();
    if (!response.isCommitted())
    {
      response.sendRedirect("../../pages/error.jsp");
    }
  }

%>
