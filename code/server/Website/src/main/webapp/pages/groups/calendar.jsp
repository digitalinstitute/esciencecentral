<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%
    GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

    String groupId = request.getParameter("id");
    if (groupId == null || groupId.equals(""))
    {
      response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
      return;
    }
    Group group = groupEJB.getGroup(ticket, groupId);
    if (group == null)
    {
      response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
      return;
    }
  %>

  <link rel='stylesheet' href='scripts/timeline/webapp/docs/styles.css' type='text/css'/>

  <link rel='stylesheet' type='text/css' href='scripts/fullcalendar/fullcalendar.css'/>
  <script type='text/javascript' src='scripts/fullcalendar/fullcalendar.min.js'></script>
  <style type='text/css'>


    #loading {
      position: absolute;
      top: 5px;
      right: 5px;
    }

    #calendar {
      width: 600px;
      margin: 0 auto;
    }

  </style>

  <script type="text/javascript">
    $(document).ready(function()
    {
      clearTabs();


      $('#calendar').fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        theme: true,
        events: "servlets/calendar/<%=groupId%>/",

        eventDrop: function(event, delta)
        {
          alert(event.title + ' was moved ' + delta + ' days\n' +
                '(should probably update your database)');
        },

        loading: function(bool)
        {
          if (bool) $('#loading').show();
          else $('#loading').hide();
        },
        
        eventClick: function(calEvent, jsEvent, view){
            $("#eventDialog").dialog("open");
            evD.loadEvent(calEvent.id);
        }

      });
    });

  </script>
  <!-- Caption Line -->
  <h2 class="grid_12 caption">Events Calendar for: <span><%=group.getName()%></span>
  </h2>


  <div class="hr grid_12 clearfix">&nbsp;</div>

  <%-- Main Content Left Hand Side--%>
  <div class="grid_9 content">
    <div id='loading' style='display:none'>loading...</div>
    <div id='calendar'></div>
  </div>


  <!-- Right Hand Side -->
  <%@include file="menu.jspf" %>

  <div class="grid_12 clearfix">&nbsp;</div>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>