<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%
    try
    {
      GroupDirectoryRemote groupEJB = EJBLocator.lookupGroupDirectoryBean();

      String groupId = request.getParameter("id");
      if (groupId == null || groupId.equals(""))
      {
        response.sendRedirect("../../pages/groups/grouplist.jsp?error=noGroupId");
        return;
      }
      Group group = groupEJB.getGroup(ticket, groupId);
      if (group == null)
      {
        response.sendRedirect("../../pages/groups/grouplist.jsp?error=groupNotFound");
        return;
      }

       //setup timeline
    String timelineTopInterval = "Timeline.DateTime.WEEK";
    String timelineBottomInterval = "Timeline.DateTime.MONTH";
  %>

  <link rel='stylesheet' href='scripts/timeline/webapp/docs/styles.css' type='text/css'/>



  <%--Needed for timeline--%>
  <script src="../../scripts/timeline/ajax/api/simile-ajax-api.js" type="text/javascript"></script>
  <script src="../../scripts/timeline/webapp/api/timeline-api.js" type="text/javascript"></script>
  <script type="text/javascript" src="../../scripts/timeline/timeline.js"></script>


  <script type="text/javascript">
    $(document).ready(function()
    {
      clearTabs();
    });

    Timeline.OriginalEventPainter.prototype._oldShowBubble = Timeline.OriginalEventPainter.prototype._showBubble;
    Timeline.OriginalEventPainter.prototype._showBubble = function(x, y, evt)
    {
      if(evt.getProperty("eventType") === "GroupEvent")
      {

      }
      this._oldShowBubble(x,y,evt);
    };
    
    $(window).load(function()
    {
      loadTimeline("servlets/newsapi?method=getGroupNews&id=<%=request.getParameter("id")%>&themeFolder=<%=themeFolder%>", <%=timelineTopInterval%>, <%=timelineBottomInterval%>, new Date());
    });

  </script>

  <!-- Caption Line -->
  <h2 class="grid_12 caption">What's been happening in <span><%=group.getName()%></span>
  </h2>

  <div class="hr grid_12 clearfix">&nbsp;</div>

  <%-- Main Content Left Hand Side--%>
  <div class="grid_9 content">

    <div id="timeline" class="timeline-default" style="height: 300px"></div>

    <div id="eventFiles"></div>
  </div>


  <!-- Right Hand Side -->
  <%@include file="menu.jspf" %>

  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>


<%
  }
  catch (Exception e)
  {
    e.printStackTrace();
    if (!response.isCommitted())
    {
      response.sendRedirect("../../pages/error.jsp");
    }
  }

%>

