<%--
  Demo Page
  Author: Simon
--%>
<%@page import="com.connexience.server.model.security.Permission" %>
<%@page import="com.connexience.server.model.workflow.WorkflowDocument" %>

  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

<div id="ide" class="content ui-corner-all"></div>
<div id="positionPush"></div>

  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/css/blockbuilder.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/servicexmlui.css">
  <link type="text/css" href="../../scripts/layout/1.2.0/layout-default.css" rel="stylesheet"/>
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-selectbox/jquery.selectBox.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/CodeMirror-2.13/lib/codemirror.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/CodeMirror-2.13/theme/default.css">
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

    <!--<script type="text/javascript" src="../../scripts/layout/1.2.0/jquery.layout.min-1.2.0.js"></script>-->
    <script type="text/javascript" src="../../scripts/layout/1.2.0/jquery.layout-latest.min.js"></script>
    <!--<link type="text/css" href="../../scripts/layout/1.2.0/layout-default.css" rel="stylesheet"/>-->
    <script type="text/javascript" src="../../scripts/util/xmlutil.js"></script>
    <script type="text/javascript" src="../../scripts/util/StringReplacer.js"></script>
    <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
    <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
    <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
    <script type="text/javascript" src="../../scripts/filebrowser/filetree.js"></script>
    <link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen" charset="utf-8"/>
    <script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
    <script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/lib/codemirror.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/javascript/javascript.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/xml/xml.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/r/r.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/htmlmixed/htmlmixed.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/css/css.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/SimpleEditorPanel.js"></script>
    <script type="text/javascript" src="../../scripts/util/APIClient.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockType.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/ServiceXML.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockIO.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockProperty.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/Block.js"></script>
    <script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/NewBlockUI.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/GettingStartedPanel.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/VersionList.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockFileManager.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockWizard.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/BlockBuilder.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/blocks/javascript/JavascriptBlockHelper.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/blocks/octave/OctaveBlockHelper.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/blocks/r/RBlockHelper.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/blocks/gnuplot/GnuPlotBlockHelper.js"></script>
    <script type="text/javascript" src="../../scripts/blockbuilder/blocks/binarylibrary/BinaryLibraryHelper.js"></script>
    <script type="text/javascript">
        <%if(request.getParameter("id")!=null){%>
            var blockId = '<%=request.getParameter("id")%>';
        <%} else {%>
            var blockId = null;
        <%}%>

    </script>

<script type="text/javascript">
    $(document).ready(function () {

            $("#_tophr").css("display", "none");
            
            globalBuilder.init("ide");
            globalBuilder.resize();
            var oldResize = window.onresize;
            window.onresize = function()
            {
                globalBuilder.resize();
            };           
            
            /*
            fileTree = new FileTree();
            fileTree.init("filetree");
            fileTree.showHomeFolder();
            */
            
            //resizeFrame();
            if(blockId!=null){
                globalBuilder.openBlockUsingId(blockId);
            } else {
                globalBuilder.showGettingStarted();
            }
    });

</script>

<%@ include file="../../WEB-INF/jspf/page/footer.jspf" %>