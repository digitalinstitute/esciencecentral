<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>

<%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>        
        <script type="text/javascript" src="../../scripts/admin/WorkflowLockAdmin.js"></script>

            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>Workflow Lock Management</h4>


<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
                <div id="locksdiv">
                    
                </div>
                
              
                
    </div>


        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>

        <script type="text/javascript">
            
            var lockAdmin = new WorkflowLockAdmin();
            lockAdmin.init("locksdiv");
            
            $(document).ready(function(){
                lockAdmin.fetchLocks();
            });
            
            function refreshList(){
                lockAdmin.fetchLocks();
            }
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
