<%@page import="com.connexience.server.model.security.Permission"%>

<%@ page import="com.connexience.server.model.security.Ticket" %>
<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.model.folder.Folder" %>


  <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

  <script type="text/javascript" src="../../scripts/admin/TemplateFolderAdmin.js"></script>
  <script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
  <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
  <link rel="stylesheet" type="text/css" href="../../scripts/datatables/css/datatable.css"/>

  <%

  %>

  <!-- Caption Line -->
  <h2>System Administration</h2>

    <h4>Permission Validation</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

        <%
            Ticket rootTicket = new Ticket();
            rootTicket.setUserId(ticket.getUserId());
            rootTicket.setOrganisationId(ticket.getOrganisationId());
            rootTicket.setLastAccessTime(new Date());
            rootTicket.setSuperTicket(true);        
            
            Group users = EJBLocator.lookupGroupDirectoryBean().getGroup(rootTicket, organisation.getDefaultGroupId());
            Group admins = EJBLocator.lookupGroupDirectoryBean().getGroup(rootTicket, organisation.getAdminGroupId());
            Group publicGroup = EJBLocator.lookupGroupDirectoryBean().getGroup(rootTicket, organisation.getPublicGroupId());
            User publicUserObject = EJBLocator.lookupUserDirectoryBean().getUser(rootTicket, organisation.getDefaultUserId());
            Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(rootTicket, organisation.getDataFolderId());
            Folder groupsFolder = EJBLocator.lookupStorageBean().getFolder(rootTicket, organisation.getGroupFolderId());
            Folder docTypesFolder = EJBLocator.lookupStorageBean().getFolder(rootTicket, organisation.getDocumentTypesFolderId());
            
        %>
        <h5>Permission check results</h5>
        <p>Data folder checks</p>
        <ul>

        <%
            // Check ADD permission for users            
            if(!EJBLocator.lookupAccessControlBean().permissionExists(users.getId(), dataFolder, Permission.ADD_PERMISSION)){
                
                EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), dataFolder.getId(), Permission.ADD_PERMISSION);
            %>
            <li>Added Users ADD permission to data folder</li>
            <%} else {%>
            <li>ADD permission exists for users on data folder</li>
            
            <%}%>
        <%
            
        // Check READ permission for users
        if(!EJBLocator.lookupAccessControlBean().permissionExists(users.getId(), dataFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), dataFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Users READ permission to data folder</li>

        <%} else {%>
        <li>READ permission exists for users on data folder</li>
        <%}%>
        
        <%
            // Check ADD permission for admins            
            if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), dataFolder, Permission.ADD_PERMISSION)){
                
                EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), dataFolder.getId(), Permission.ADD_PERMISSION);
            %>
            <li>Added Admins ADD permission to data folder</li>
            <%} else {%>
            <li>ADD permission exists for Admins on data folder</li>
            
            <%}%>
        <%
            
        // Check READ permission for admins
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), dataFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), dataFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Admins READ permission to data folder</li>

        <%} else {%>
        <li>READ permission exists for Admins on data folder</li>
        <%}%>        
        
        <%
        // Check WRITE permission for admins
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), dataFolder, Permission.WRITE_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
        %>
        <li>Added Admins WRITE permission to data folder</li>

        <%} else {%>
        <li>WRITE permission exists for Admins on data folder</li>
        <%}%>  
        </ul>
        
        <p>Groups folder checks</p>
        <ul>
        
        <%
        // Check ADD permission for group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(users.getId(), groupsFolder, Permission.ADD_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), groupsFolder.getId(), Permission.ADD_PERMISSION);
        %>
        <li>Added Users ADD permission to groups folder</li>

        <%} else {%>
        <li>ADD permission exists for users on groups folder</li>
        <%}%>    
        
        <%
        // Check READ permission for group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(users.getId(), groupsFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Users READ permission to groups folder</li>

        <%} else {%>
        <li>READ permission exists for users on groups folder</li>
        <%}%>            
        
        <%
        // Check ADD permission for group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), groupsFolder, Permission.ADD_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), groupsFolder.getId(), Permission.ADD_PERMISSION);
        %>
        <li>Added Admins ADD permission to groups folder</li>

        <%} else {%>
        <li>ADD permission exists for Admins on groups folder</li>
        <%}%>    
        
        <%
        // Check READ permission for group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), groupsFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Admins READ permission to groups folder</li>

        <%} else {%>
        <li>READ permission exists for Admins on groups folder</li>
        <%}%>     
        
        <%
        
        // Check WRITE permission for group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), groupsFolder, Permission.WRITE_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), groupsFolder.getId(), Permission.WRITE_PERMISSION);
        %>
        <li>Added Admins WRITE permission to groups folder</li>

        <%} else {%>
        <li>WRITE permission exists for Admins on groups folder</li>
        <%}%>           
            
        </ul>
        
        <p>Document types folder checks</p>
        <ul>
        <%
        // Check READ permission for doc types for users
        if(!EJBLocator.lookupAccessControlBean().permissionExists(users.getId(), docTypesFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, users.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Users READ permission to document types folder</li>

        <%} else {%>
        <li>READ permission exists for users on document types folder</li>
        <%}%>       
        
        <%
        // Check READ permission for doc types for public group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(publicGroup.getId(), docTypesFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicGroup.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Public Users Group READ permission to document types folder</li>

        <%} else {%>
        <li>READ permission exists for Public Users Group on document types folder</li>
        <%}%>       
        
        <%
        // Check READ permission for doc types for public group
        if(!EJBLocator.lookupAccessControlBean().permissionExists(publicUserObject.getId(), docTypesFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicUserObject.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Public User READ permission to document types folder</li>

        <%} else {%>
        <li>READ permission exists for Public User on document types folder</li>
        <%}%>               
        
        <%
            // Check ADD permission for admins            
            if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), docTypesFolder, Permission.ADD_PERMISSION)){
                
                EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.ADD_PERMISSION);
            %>
            <li>Added Admins ADD permission to document types folder</li>
            <%} else {%>
            <li>ADD permission exists for Admins on document types folder</li>
            
            <%}%>
        <%
            
            
            
        // Check READ permission for admins
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), docTypesFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Admins READ permission to document types folder</li>

        <%} else {%>
        <li>READ permission exists for Admins on document types folder</li>
        <%}%>        
        
        <%
        // Check WRITE permission for admins
        if(!EJBLocator.lookupAccessControlBean().permissionExists(admins.getId(), docTypesFolder, Permission.WRITE_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, admins.getId(), docTypesFolder.getId(), Permission.WRITE_PERMISSION);
        %>
        <li>Added Admins WRITE permission to document types folder</li>

        <%} else {%>
        <li>WRITE permission exists for Admins on document types folder</li>
        <%}%>  
        </ul>        
        
        <p>Public user permission checks</p>
        <ul>
            
        <%
        // Check read on groups folder for public user
        if(!EJBLocator.lookupAccessControlBean().permissionExists(publicUserObject.getId(), publicUserObject, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicUserObject.getId(), publicUserObject.getId(), Permission.READ_PERMISSION);
        %>
        <li>Made Public User self readable</li>
        <%} else {%>
        <li>Public User is already self readable</li>
        <%}%>  
        
        <%
        // Check read on groups folder for public user
        if(!EJBLocator.lookupAccessControlBean().permissionExists(publicUserObject.getId(), groupsFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicUserObject.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Public User READ permission to groups folder</li>
        <%} else {%>
        <li>READ permission exists for Public User on groups folder</li>
        <%}%>  
        
        <%
        // Check read on groups folder for public user
        if(!EJBLocator.lookupAccessControlBean().permissionExists(publicGroup.getId(), groupsFolder, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(rootTicket, publicGroup.getId(), groupsFolder.getId(), Permission.READ_PERMISSION);
        %>
        <li>Added Public Group READ permission to groups folder</li>
        <%} else {%>
        <li>READ permission exists for Public Group on groups folder</li>
        <%}%>  
        
        </ul>         

  </div>

  <!-- Right Hand Side -->

        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>


  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
