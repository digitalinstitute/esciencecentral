<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="com.connexience.server.model.scanner.*" %>

        <%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>     
        <script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
        <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
        <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
        <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
        <script type="text/javascript" src="../../scripts/admin/RemoteFilesystemScannerAdmin.js"></script>
        <%
            List scanners = EJBLocator.lookupScannerBean().listScanners(ticket);
            NameCache nameCache = new NameCache(100);
        %>
            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>External Filesystem Scanners</h4>

            <div class="row-fluid">
                <%-- Main Content Left Hand Side--%>
                <div class="span9">
                    <table id="scannerstable" class="display">
                        <thead>
                            <tr>
                                <th>Type</th>
                                <th>Owner</th>
                                <th>Local Folder</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                        <%
                        RemoteFilesystemScanner scanner;
                        for(Object o : scanners){
                            scanner = (RemoteFilesystemScanner)o;%>
                            <tr>
                                <td><%=scanner.getTypeName()%></td>
                                <%if(scanner.isStudyScanner()){
                                    try {%>
                                    <td><img src="../../styles/common/images/group.png"/> <%=EJBLocator.lookupStudyBean().getStudy(ticket, (int)scanner.getStudyId()).getName()%></td>
                                <%} catch (Exception e){%>
                                        <td>Unknown</td>
                                <%}}else {%>
                                <td><img src="../../styles/common/images/user.png"/> <%=nameCache.getObjectName(ticket, scanner.getUserId())%></td>
                                <%}%>
                                <td><a onclick="changeFolder('<%=scanner.getId()%>');"><%=nameCache.getObjectName(ticket, scanner.getTargetFolderId())%></a></td>
                                <td>
                                    <div class="btn" onclick="showFiles('<%=scanner.getId()%>');">Files</div>
                                    <div class="btn" onclick="editScanner('<%=scanner.getId()%>');">Edit</div>
                                    <%if(!scanner.isStudyScanner()){%>
                                    <div class="btn" onclick="deleteScanner('<%=scanner.getId()%>');">Delete</div>
                                    <%}%>
                                    <div class="btn" onclick="resetScanner('<%=scanner.getId()%>');">Reset</div>
                                    <div class="btn" onclick="forceRescan('<%=scanner.getId()%>');">Upload Now</div>
                                    <%if(scanner.isEnabled()){%>
                                        <div class="btn" onclick="disableScanner('<%=scanner.getId()%>');">Disable</div>
                                    <%} else {%>
                                        <div class="btn" onclick="enableScanner('<%=scanner.getId()%>');">Enable</div>
                                    <%}%>
                                </td>
                            </tr>
                            
                            
                            
                        <%}%>
                            
                            
                            
                        </tbody>
                        
                        
                    </table>
                    <div class="dialog" id="confirmdialog"></div>
                    <div class="dialog" id="filechooser"></div>
                    <div class="dialog" id="files"></div>
                    <div class="dialog" id="choosedialog"></div>
                    <div class="dialog" id="editdialog"></div>
                </div>

                
                <%-- Side menu --%>
                <div class="span3">
                    <%@include file="menu.jspf"%>
                </div>

            </div>

        <script type="text/javascript">
            var dialog = new ConfirmDialog();
            var chooser = new FileChooser();
            var manager = new RemoteFilesystemScannerAdmin();
            
            $(document).ready(function(){
                dialog.init("confirmdialog");    
                manager.initChooserDialog("choosedialog");
                manager.initEditDialog("editdialog");
                manager.initFilesDialog("files");
                chooser.folderMode = true;
                chooser.init("filechooser");
                $("#scannerstable").dataTable({"bJQueryUI": true});
            });
            
            function createScanner(){
                var cb = function(){
                    location.reload();
                };
                manager.createScannerUsingChooser(cb);
            }
            
            function changeFolder(id){
                chooser.okCallback = function(){
                    if(chooser.selectedFolderId){
                        manager.setScannerFolder(id, chooser.selectedFolderId, function(){location.reload();});
                    }
                };
                chooser.showHomeFolder(false);
            }
            
            function editScanner(id){
                var cb = function(){
                    $.jGrowl("Scanner settings changed");
                };
                manager.editScanner(id, cb);
            }
            
            function deleteScanner(id){
                var cb = function(){
                    location.reload();
                };
                
                var yesCallback = function(){
                    manager.deleteScanner(id, cb);
                };

                dialog.yesCallback = yesCallback;
                dialog.show("Are you sure you want to delete the selected scanner?");
            }
            
            function forceRescan(id){
                var cb = function(){
                    $.jGrowl("Scanner rescan started");
                };
                manager.executeScanner(id, cb);
            }
            
            function resetScanner(id){
                var cb = function(){
                    $.jGrowl("Scanner reset");
                };
                manager.resetScanner(id, cb);
            }            
            
            function enableScanner(id){
                var cb = function(){
                    location.reload();
                };
                manager.enableScanner(id, cb);
            }
            
            function disableScanner(id){
                var cb = function(){
                    location.reload();
                };                
                manager.disableScanner(id, cb);
            }
            
            function showFiles(id){
                manager.showScannerFiles(id);
            }
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>