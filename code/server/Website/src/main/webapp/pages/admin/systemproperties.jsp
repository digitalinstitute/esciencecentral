<%--
  System Properties Menu
  Author: Hugo
--%>


<%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
<link rel="stylesheet" href="../../scripts/properties/PropertyEditorNoDatatables.css"/>
<script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
<script type="text/javascript" src="../../scripts/admin/PropertiesAdmin.js"></script>

            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>System Properties</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9" id="propertiesdiv" >


    </div>


    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>

        <script type="text/javascript">
            
            var propertiesAdmin = new PropertiesAdmin();
            
            
            $(document).ready(function(){
                propertiesAdmin.init("propertiesdiv");
                propertiesAdmin.populatePropertyGroupList();
                
                window.onbeforeunload = function(){
                    propertiesAdmin.syncSavePropertyGroup();
                    return null;
                };                
                
            });
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>