<%@ page import="java.util.List"%>
<%@ page import="com.connexience.server.model.storage.migration.*"%>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

<%
    List migrations = EJBLocator.lookupDataStoreMigrationBean().listMigrations(ticket);

%>
  <script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
  <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
  <link rel="stylesheet" type="text/css" href="../../scripts/datatables/css/datatable.css"/>
  <link rel="stylesheet" type="text/css" href="../../scripts/properties/PropertyEditorNoDatatables.css"/>
  <script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>
  <script type="text/javascript" src="../../scripts/admin/DataStoreAdmin.js"></script>
  
  <style type="text/css">
    td{
      padding:10px;
    }
    thead{
      font-weight: bold;
    }
  </style>

  <script type="text/javascript">
    var storeAdmin = new DataStoreAdmin();
    var propertyEditor = new PropertyEditor();
    var migrationPropertyEditor = new PropertyEditor();
    var storeTypeSelect = new MultiValueSelectDialog();
    var confirmDialog = new ConfirmDialog();
    var deleteConfirm = new ConfirmDialog();
    var accessConfirm = new ConfirmDialog();
    
    var storeTypes = new Array();
    storeTypes[0] = "file";
    storeTypes[1] = "s3";
    storeTypes[2] = "azure";
    
    $(document).ready(function(){
        propertyEditor.withoutDataTables = true;
        propertyEditor.init("storeproperties");
        migrationPropertyEditor.init("migrationproperties");
        propertyEditor.okCallback = function(properties){
            saveSystemDataStore(properties);
        };
        
        migrationPropertyEditor.okCallback = function(properties){
            _setProperties(this.migrationId, properties);
        };
        
        storeTypeSelect.init("storetype");
        storeTypeSelect.okCallback = function(value){
            _resetStoreType(this.migrationId, value);
        };
        
        confirmDialog.init("confirmdialog");
        confirmDialog.yesCallback = function(){
            _finishMigration(this.migrationId);
        };
        
        deleteConfirm.init("deleteconfirm");
        deleteConfirm.yesCallback = function(){
            _deleteMigration(this.migrationId);
        };
        
        accessConfirm.init("keyconfirm");
        accessConfirm.yesCallback = function(){
            _resetAccessKey();
        };
        
        updateMigrationStates();
    }); 
    
    function updateMigrationStates(){
        var cb = function(migrations){
            var div;
            for(var i=0;i<migrations.length;i++){
                div = document.getElementById("maintext_" + migrations[i].id);
                if(div){
                    div.innerHTML = "<p>Fetching...</p>";
                }
                fetchMigrationState(migrations[i].id);
            }
        };
        storeAdmin.listMigrations(cb);
    }
    
    function fetchMigrationState(id){
        var cb = function(state){
            var div = document.getElementById("maintext_" + state.migrationId);
            if(div){
                div.innerHTML = "<p>Done: " + state.complete + "/" + state.total + "</p>";
            }
        };
        storeAdmin.getMigrationState(id, cb);
    }
    
    function _setProperties(id, properties){
        var cb = function(){
            $.jGrowl("Updated migration data store properties");
        };
        storeAdmin.setMigrationDataStoreProperties(id, properties, cb);
    }
    
    function _resetStoreType(id, value){
        var cb = function(){
            $.jGrowl("Store reset to: " + value);
        };
        storeAdmin.resetMigrationDataStore(id, value, cb);
    }
    
    function saveSystemDataStore(properties){
        var cb = function(){
            $.jGrowl("System data store modified");
        };
        storeAdmin.setSystemDatastoreProperties(properties, cb);
    }
    
    function editSystemDataStore(){
        var callback = function(properties){
            propertyEditor.editProperties(properties);
        };
        storeAdmin.getSystemDatastore(callback);
    }
    
    function createMigration(){
        var callback = function(){
            window.location = "storageadmin.jsp";
        };
        storeAdmin.createMigration("azure", callback);
    }
    
    function startMigration(id){
        var callback = function(){
            $.jGrowl("Started migration");
            window.location = "storageadmin.jsp";
        };
        storeAdmin.startMigration(id, callback);
    }
    
    function stopMigration(id){
        var callback = function(){
            $.jGrowl("Stopped migration");
            window.location = "storageadmin.jsp";
        };
        storeAdmin.stopMigration(id, callback);        
    }
    
    function editMigrationStore(id){
        var cb = function(migration){
            migrationPropertyEditor.migrationId = id;
            migrationPropertyEditor.editProperties(migration.properties);
        };
        storeAdmin.getMigration(id, cb);
    }
    
    function deleteMigration(id){
        deleteConfirm.migrationId = id;
        deleteConfirm.show("Are you sure you want to delete this migration?");
    }
    
    function _deleteMigration(id){
        var cb = function(){
            $.jGrowl("Migration removed");
            window.location = "storageadmin.jsp";
        };
        storeAdmin.deleteMigration(id, cb);        
    }
    
    function retryFailed(id){
        var cb = function(){
            $.jGrowl("Reset failed documents");
        };
        storeAdmin.retryFailedDocumentsForMigration(id, cb);
    }
    
    function resetDataStore(id){
        storeTypeSelect.migrationId = id;
        storeTypeSelect.show(storeTypes, "file");
    }
    
    function resetAccessKey(){
        accessConfirm.show("Are you sure you want to reset the store access to to the local MAC address?");
    }
    
    function _resetAccessKey(){
        var cb = function(){
            window.location = "storageadmin.jsp";
        };
        storeAdmin.resetAccessKey(cb);
    }
    
    function finishMigration(id){
        confirmDialog.migrationId = id;
        confirmDialog.show("Are you sure you want to switch the organisation data store?");
    }
    
    function _finishMigration(id){
        var cb = function(){
            window.location = "storageadmin.jsp";
        };
        storeAdmin.finishMigration(id, cb);
    }
    
  </script>

  <!-- Caption Line -->
  <h2>System Administration</h2>
    <h4>Data Storage</h4>
<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

    <div class="list">

        <%if(migrations.size()==0){%>
            <p>No data migrations in progress</p>
        <%} else {%>
            <%
            DataStoreMigration migration;
            for(int i=0;i<migrations.size();i++){
                migration = (DataStoreMigration)migrations.get(i);
            %>

                <div class="listElement">
                    <%if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){%>
                        <img src="../../styles/common/images/search/workflow.png" alt="Dashboard icon" class="listItemImg"/>
                    <%} else {%>
                        <img src="../../styles/common/images/search/folder.png" alt="Dashboard icon" class="listItemImg"/>
                    <%}%>


                  <div class="listTopText">Migration</div>
                  <div class="listMainText" id="maintext_<%=migration.getId()%>">
                      <p>A migration description</p>
                  </div>
                  <div class="listBottomText">
                      <%if(migration.getStatus()==DataStoreMigration.MIGRATION_RUNNING){%>
                        <a style="cursor: pointer;" onclick="stopMigration('<%=migration.getId()%>')">Pause</a>
                        <a style="cursor: pointer;" onclick="retryFailed('<%=migration.getId()%>')">Retry Failed</a>
                        <a style="cursor: pointer;" onclick="finishMigration('<%=migration.getId()%>')">Switch to this data store</a>
                      <%} else {%>
                        <a style="cursor: pointer;" onclick="startMigration('<%=migration.getId()%>')">Start</a>
                        <a style="cursor: pointer;" onclick="retryFailed('<%=migration.getId()%>')">Retry Failed</a>
                        <a style="cursor: pointer;" onclick="resetDataStore('<%=migration.getId()%>')">Reset Store</a>
                        <a style="cursor: pointer;" onclick="editMigrationStore('<%=migration.getId()%>')">Edit Store</a>
                        <a style="cursor: pointer;" onclick="deleteMigration('<%=migration.getId()%>')">Delete</a>                  
                      <%}%>

                  </div>
                </div>                
             <%}%>
          <%}%>
        <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>
    </div>      
      <div id="storeproperties"></div>
      <div id="migrationproperties"></div> 
      <div id="storetype" class="dialog"></div>
      <div id="confirmdialog" class="dialog"></div>
      <div id="deleteconfirm" class="dialog"></div>
      <div id="keyconfirm" class="dialog"></div>
  </div>

        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>


  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
