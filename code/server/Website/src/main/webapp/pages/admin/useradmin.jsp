<%--
  User Admin Menu
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>

        <%@ include file="../../WEB-INF/jspf/page/header.jspf"%>
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
        <script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
        <link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>    
        <script type="text/javascript" src="../../scripts/admin/UserAdmin.js"></script>

            <!-- Caption Line -->
            <h2>System Administration</h2>
            <h4>User Management</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9" id="admin-users">
        <form action="#" onsubmit="return false;">
            <div class="input-append">
                <input id="searchtext" name="searchtext" type="text" placeholder="Search">
                <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                    <button class="btn" onclick="showUsers()">
                        <i class="icon-search"></i>Search
                    </button>
                </div>
            </div>
        </form>
        <div>
            <h3>Search Results</h3>
            <div id="userlist"></div>
            <div id="passworddialog" class="dialog"></div>
            <div id="quotadialog" class="dialog"></div>
            <div id="groupdialog" class="dialog"></div>
            <div id="userdialog" class="dialog"></div>
            <div id="deleteconfirmdialog" class="dialog"></div>
            <div id="lockreasondialog" class="dialog"></div>
        </div>
    </div>


    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>

        <script type="text/javascript">
            
            var userAdmin = new UserAdmin();

            function createUser(){
                userAdmin.showCreateUserDialog();
            }
            
            function showUser(id){
                var callback = function(o){
                    userAdmin.userList = new Array();
                    userAdmin.userList.push(o.user);
                    userAdmin.displayUserList("userlist", "");
                };
                userAdmin.getUserById(id, callback);
            }
            
            function keyPress(event){
                var chCode = ('charCode' in event) ? event.charCode : event.keyCode;
                if(chCode===13){
                    showUsers();
                }
            }
            
            function showUsers(){
                var searchText = document.getElementById("searchtext").value;
                userAdmin.searchUsers(searchText, function(){userAdmin.displayUserList("userlist", "");});    
            }
            
            $(document).ready(function(){
                userAdmin.initPasswordDialog("passworddialog");
                userAdmin.initQuotaDialog("quotadialog");
                userAdmin.initGroupDialog("groupdialog");
                userAdmin.initCreateUserDialog("userdialog");
                userAdmin.initDeleteUserConfirmDialog("deleteconfirmdialog");
                userAdmin.initLockReasonDialog("lockreasondialog")
                <%if(request.getParameter("userid")!=null && !request.getParameter("userid").isEmpty()){%>
                    showUser('<%=request.getParameter("userid")%>');
                <%}%>
            });
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>