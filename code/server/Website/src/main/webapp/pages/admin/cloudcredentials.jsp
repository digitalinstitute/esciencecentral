


  <%@page import="com.connexience.server.model.security.StoredCredentials"%>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<%@page import="java.util.List"%>
  <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css"/>        
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>     
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>

  <script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
  <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
  <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
  <script type="text/javascript" src="../../scripts/admin/CloudCredentialsAdmin.js"></script>
  <script type="text/javascript" src="../../scripts/properties/PropertyEditor.js"></script>

  

  <%
      List credentials = EJBLocator.lookupCredentialsDirectoryBean().listCredentials(ticket);
  %>

  <!-- Caption Line -->
  <h2>System Administration</h2>

    <h4>Cloud Credentials</h4>

<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

        <table id="credentialstable" class="display" style="width:100%;">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            
            <tbody>
                <%
                StoredCredentials c;
                for(int i=0;i<credentials.size();i++){
                    c = (StoredCredentials)credentials.get(i);%>
                    <tr>
                        <td><%=c.getCredentialType()%></td>
                        <td><%=c.getName()%></td>
                        <td>
                            <div class="btn" onclick="editCredentials('<%=c.getId()%>');">Edit</div>
                            <div class="btn" onclick="deleteCredentials('<%=c.getId()%>', '<%=c.getName()%>');">Delete</div>
                        </td>
                    </tr>
                <%}%>
                
            </tbody>
            
        </table>
                
        <div class="dialog" id="credentialstypes"></div>
        <div class="dialog" id="deleteconfirm"></div>
        <div class="dialog" id="properitesdialog"></div>
    </div>

  <!-- Right Hand Side -->

        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>

</div>

            <script type="text/javascript">
                var manager = new CloudCredentialsAdmin();
                var typesDialog = new MultiValueSelectDialog();
                var deleteConfirmDialog = new ConfirmDialog();
                var propertyEditor = new PropertyEditor();
                var acl = new AccessControl();
                
                $(document).ready(function(){
                    $("#credentialstable").dataTable({"bJQueryUI": true});
                    typesDialog.init("credentialstypes");
                    deleteConfirmDialog.init("deleteconfirm");
                    propertyEditor.init("properitesdialog");
                    propertyEditor.okCallback = function(properties){
                        if(this.credentials){
                            var credentials = this.credentials;
                            credentials.properties = properties;
                            manager.saveCredentials(credentials, function(){
                                location.reload();
                            });
                            this.credentials = undefined;
                        }
                    };
                    
                    typesDialog.okCallback = function(value, index){
                        var cb = function(){
                            location.reload();
                        };
                        manager.createCredentials(value, cb);
                    };
                }); 
                
                function addCredentials(){
                    var cb = function(types){
                        if(types.length>0){
                            typesDialog.show(types, types[0]);
                        }
                    };
                    manager.listCredentialTypes(cb);
                }
                
                function editCredentials(id){
                    var cb = function(credentials){
                        propertyEditor.credentials = credentials;
                        propertyEditor.editProperties(credentials.properties);
                    };
                    manager.getCredentials(id, cb);
                }
                
                function deleteCredentials(id, name){
                    var cb = function(){
                        manager.deleteCredentials(id, function(){
                            location.reload();
                        });
                    };
                    
                    deleteConfirmDialog.yesCallback = cb;
                    deleteConfirmDialog.show("Are you sure you want to delete the credentials: " + name);
                }
                
                function shareCredentials(id){
                    
                    $('#securitydialog').remove();

                    //add a new ACL
                    $('body').append('<div id="securitydialog" class="acl"></div>');
                    acl.init("securitydialog", _userId, id, _publicUserId, _enablePublic, _usersGroupId);
                    acl.open();        
                }               
            </script>
  <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
