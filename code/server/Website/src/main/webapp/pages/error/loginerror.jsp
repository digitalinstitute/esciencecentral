<%@ page import="com.connexience.server.util.LoginReportHolder" %>
<%@ page import="com.connexience.server.util.LoginReport" %><%
    LoginReport report = LoginReportHolder.getLoginReport();
    if(report != null)
    {
        session.setAttribute("LOGINREPORT", report);
    }
    LoginReportHolder.clearLoginReport();

    response.sendRedirect("../../pages/front/welcome.jsp");
%>