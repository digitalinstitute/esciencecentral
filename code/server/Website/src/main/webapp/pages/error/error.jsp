<%@page isErrorPage="true" %>
<%@page import="java.io.PrintWriter" %>
<%@page import="java.io.StringWriter" %>

<!DOCTYPE HTML>
<html>
<head>

    <script type="text/javascript">
        _globalDialogHeight = 200;
    </script>

    <!--Open Sans Font Link-->
    <link href='<%=request.getContextPath()%>/styles/common/fonts/GoogleOpenSans.css' rel='stylesheet' type='text/css'>
    <!--Icomoon-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/common/icomoon.css"/>
    <!--Bootstrap-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/common/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/common/bootstrap-responsive.css"/>
    <!--Theme-->
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/common/styles.css"/>
    <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/styles/esc/styles.css"/>
    <link rel="icon" href="<%=request.getContextPath()%>/styles/common/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="<%=request.getContextPath()%>/styles/common/images/favicon.ico" type="image/x-icon"/>
    <meta http-equiv="content-type" content="text/html; UTF-8"/>
    <meta name="keywords" content="collaboration, e-Science, bioinformatics, data analysis"/>
    <meta name="robots" content="all"/>
</head>
<body>
<div id="toolbar">
    <div class="container">
        <ul id="toolbar-menu" class="inline">

        </ul>

    </div>
</div>
<div id="wrapper" class="container clearfix">
    <div class="row-fluid">
        <div class="span6">
            <a href="<%=request.getContextPath()%>/pages/front/index.jsp">
                <h1 id="logo">
                    <img src="<%=request.getContextPath()%>/styles/esc/images/logo_head.png"/>
                </h1>
                <h4 id="tagline">Store, Analyse and Share your Data
                </h4>
            </a>
        </div>
    </div>
    <hr id="header-divider"/>


    <%
        String errorType = request.getParameter("type");
        if (errorType == null) {
    %>
    <h3>We're sorry but something has gone wrong.</h3>
    <h4>This error has been logged and we're investigating.</h4>
    <%
    } else if (errorType.equalsIgnoreCase("quota")) {
    %>
    <h3>Error Quota Error</h3>
    <%
        }
        if (pageContext != null) {

            try{
            if (pageContext.getErrorData() != null) {
    %>
    <hr>
    <dl class="dl-horizontal">
        <dt>Request URI</dt>
        <dd><%=pageContext.getErrorData().getRequestURI()%>
        </dd>
        <dt>Servlet Name</dt>
        <dd><%=pageContext.getErrorData().getServletName()%>
        </dd>
        <dt>Status Code</dt>
        <dd><%=pageContext.getErrorData().getStatusCode()%>
        </dd>
    </dl>
    <%

        if (pageContext.getErrorData().getThrowable() != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            pageContext.getErrorData().getThrowable().printStackTrace(printWriter);
    %>
    <pre><%= stringWriter.toString() %></pre>
    <%
                }
            }
        }
            catch (Throwable ignored)
            {

            }
        }
    %>
    <%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    <jsp:useBean id="date" class="java.util.Date"/>
    <div id="push"></div>
</div>
<!-- Footer -->
<div id="footer">
    <div class="container">
        <p class="copyright">&copy; Copyright e-Science Central <fmt:formatDate value="${date}" pattern="yyyy"/></p>

        <div id="partners" class="clearfix">
            <div class="partner-logo">
                <img src="<%=request.getContextPath()%>/styles/esc/images/science-city.png" alt="Newcastle Science City"/>
            </div>
            <div class="partner-logo">
                <img src="<%=request.getContextPath()%>/styles/esc/images/jisc.png" alt="JISC"/>
            </div>
            <div class="partner-logo">
                <img src="<%=request.getContextPath()%>/styles/esc/images/epsrc.png" alt="EPSRC"/>
            </div>
            <div class="partner-logo">
                <img src="<%=request.getContextPath()%>/styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
            </div>
            <div class="partner-logo">
                <img src="<%=request.getContextPath()%>/styles/esc/images/side.png" alt="SiDE"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>