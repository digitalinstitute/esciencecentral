<div id="logger-sensors-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="logger-sensors-modal-title" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="logger-sensors-modal-title"></h3>
    </div>
    <div id="sensor-modal-errors">
        <div class="alert" id="noFileTypes" style="display: none;">
            <h4>Warning!</h4>
            <span>You must create at least one file type to be able to add a sensor to this logger type.</span>
        </div>
    </div>
    <div id="list-logger-sensors" class="optional-content" data-section-title="Logger Sensors" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <table>

                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary load-add-sensor-modal">Add New</button>
                <button data-dismiss="modal" class="btn">Close</button>
            </div>
        </form>
    </div>
    <div id="no-sensors-no-filetypes" class="optional-content" data-section-title="No sensors available" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <div class="alert">
                    <h4>Warning!</h4>
                    <p>No sensors or file types available.</p>
                    <p>You must create at least one file type to be able to add a sensor to this logger type.</p>
                </div>

                <div class="control-group">
                    <label class="control-label">File Type Name</label>
                    <div class="controls">
                        <input type="text" id="file-type-name" name="name" placeholder="Name" value="" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">File Type Description</label>
                    <div class="controls">
                        <textarea id="file-type-description" name="description" placeholder="Description"></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">File Type Conversion Workflow</label>
                    <div class="controls">
                        <select name="workflow" id="file-type-workflow" multiple="multiple">
                        </select>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button class="btn btn-primary add-new-filetype">Add New File Type</button>
                <button data-dismiss="modal" class="btn">Close</button>
            </div>
        </form>
    </div>
    <div id="view-logger-sensor" class="optional-content" data-section-title="Logger Sensor" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <p>list of all itrems</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default backto-list-logger-sensors">&laquo; Back to list</button>
                <button class="btn btn-danger delete-sensor">Delete</button>
                <button id="edit-sensor-button" class="btn btn-primary">Edit</button>
            </div>
        </form>
    </div>
    <div id="edit-logger-sensor" class="optional-content" data-section-title="Edit Logger Sensor" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <div class="control-group">
                    <label class="control-label">Name</label>
                    <div class="controls">
                        <input type="text" id="sensor-name" name="name" placeholder="Name" value="" >
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">File Type</label>
                    <div class="controls">
                        <select name="sensor-filetype" id="sensor-filetype">

                        </select>
                    </div>
                </div>
            </div>
            <div id="submit-sensor-message"></div>
            <div class="modal-footer" data-action="action-edit">
                <button class="btn btn-default backto-list-logger-sensors">&laquo; Back to list</button>
                <button class="btn btn-danger delete-sensor">Delete</button>
                <button class="btn btn-primary submit-sensor">Save</button>
            </div>
            <div class="modal-footer" data-action="action-add">
                <button class="btn btn-default backto-list-logger-sensors">&laquo; Back to list</button>
                <button class="btn btn-primary submit-sensor">Add sensor</button>
            </div>
        </form>
    </div>
    <div id="delete-logger-sensor" class="optional-content" data-section-title="Delete Logger Sensor" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <p>Are you sure you wish to delete this Logger Sensor?</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger confirm-sensor-delete">Delete</button>
                <button class="btn cancel-sensor-delete">Cancel</button>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    var loggerSensorsModal = {};

    $(document).ready(function () {

        var loggerSensorForm = $( "#edit-logger-sensor form" );

        loggerSensorForm.on('keydown', function(e){
            if(e.keyCode === 13 && e.target.tagName === 'INPUT'){
                e.preventDefault();
                $('.submit-sensor').eq(0).trigger('click');
            }
        });

        loggerSensorForm.validate({
            rules: {
                name: {required: true, maxlength: 255}
            }
        });

        loggerSensorsModal.$title = $('#logger-sensors-modal-title');
        loggerSensorsModal.$modalElement = $('#logger-sensors-modal');

        loggerSensorsModal.load = function(contentId, currentId, title, action) {

            var contentSection = $('#' + contentId);

            $('#sensor-modal-errors div').css('display', 'none');

            if (contentSection.length && contentSection.length > 0) {
                loggerSensorsModal.$modalElement.find('.validation-message').html('').removeClass('validation-message alert');
                loggerSensorsModal.$modalElement.find('.control-group.error').removeClass('error').find('small').remove();
                loggerSensorsModal.$modalElement.find('.optional-content').css('display', 'none');

                if (title) {
                    loggerSensorsModal.$title.html(title);
                } else {
                    loggerSensorsModal.$title.html(contentSection.attr('data-section-title'));
                }

                $('[data-action^="action-"]').css('display', 'none');
                $('[data-action="action-'+action+'"]').css('display', 'block');


                contentSection.css('display', 'block');

                loggerSensorsModal.$modalElement.modal("show");
            }
        };

        // Add click events to page
        $('.load-list-sensors-modal').on('click', function(e){
            e.preventDefault();
            loggerSensorsModal.currentLoggerTypeId = $(this).attr('data-logger-type-id');
            loadSensorsList();
        });

        $('.load-add-sensor-modal').on('click', function(e){
            e.preventDefault();

            if ($(this).attr('data-logger-type-id')) {
                loggerSensorsModal.currentLoggerTypeId = $(this).attr('data-logger-type-id');
            }

            loggerSensorsModal.currentSensor = null;
            loggerSensorsModal.currentSensorId = null;
            updateSensorEdit();

            var filetypes = $.get('/website-api/rest/logger/filetype/all');

            filetypes.done(function(filetypes){

                $('#sensor-filetype').html('');

                if (filetypes && filetypes.length > 0) {

                    $.each(filetypes, function(i, filetype) {
                        $('#sensor-filetype').append($('<option>').val(JSON.stringify(filetype)).html(filetype.name));
                    });


                }

                loggerSensorsModal.load('edit-logger-sensor', loggerSensorsModal.currentLoggerTypeId, 'Add Sensor', 'add');
            });

            filetypes.fail(function(){

            });
        });

        // Add click events to action buttons

        $('#list-logger-sensors').on('click', 'table tr', function(){
            loggerSensorsModal.currentSensorId = $(this).attr('data-sensor-id');
            loggerSensorsModal.currentSensor = $(this).data('sensor');
            updateSensorView();
            loggerSensorsModal.load('view-logger-sensor', loggerSensorsModal.currentLoggerTypeId);
        });

        $('.backto-list-logger-sensors').on('click', function(e){
            e.preventDefault();
            loggerSensorsModal.load('list-logger-sensors', loggerSensorsModal.currentLoggerTypeId);
        });

        $('#edit-sensor-button').on('click', function(e){
            e.preventDefault();

            var filetypes = $.get('/website-api/rest/logger/filetype/all');

            filetypes.done(function(filetypes){

                $('#sensor-filetype').html('');

                if (filetypes && filetypes.length > 0) {

                    $.each(filetypes, function(i, filetype) {
                        $('#sensor-filetype').append($('<option>').val(JSON.stringify(filetype)).html(filetype.name));
                    });


                }

                updateSensorEdit();
                loggerSensorsModal.load('edit-logger-sensor', loggerSensorsModal.currentLoggerTypeId, null, 'edit');
            });


        });

        $('#edit-logger-sensor').on('click', '.add-property', function(e) {
            e.preventDefault();
            appendAdditionalField($('#edit-logger-sensor .additional-properties'));
        });

        $('#edit-logger-sensor').on('click', '.submit-sensor', function(e){
            e.preventDefault();

            if (loggerSensorForm.valid()) {
                var edit = loggerSensorsModal.currentSensorId || false;
                var sensor = {};

                if (edit){
                    sensor.id = loggerSensorsModal.currentSensorId;
                }

                sensor.name = $('#edit-logger-sensor input[name="name"]').val();
                sensor.fileType = JSON.parse($('#edit-logger-sensor select[name="sensor-filetype"]').val());

                $.ajax({
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/logger/type/'+loggerSensorsModal.currentLoggerTypeId+'/sensor/save',
                    data: JSON.stringify(sensor),
                    success: function (sensor) {
                        loggerSensorsModal.currentSensor = sensor;
                        updateSensorView();
                        $('table tr[data-sensor-id="'+loggerSensorsModal.currentSensorId+'"]').data('sensor', sensor);

                        if (edit){
                            loggerSensorsModal.load('view-logger-sensor');
                            loadSensorsList(false);
                        } else {
                            loadSensorsList();
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                });
            } else {
                // Not valid!
                if(loggerSensorsModal.$modalElement.find('.validation-message').length == 0)
                {
                    var message = "Correct these errors to create/edit this logger sensor.";
                    var type = "error";
                    var target = $("#submit-sensor-message");
                    var cssClass = "validation-message";

                    flashMessage(message, type, target, cssClass)
                }
            }
        });

        $('#logger-sensors-modal').on('click', '.delete-sensor', function(e) {
            e.preventDefault();
            loggerSensorsModal.load('delete-logger-sensor');
        });

        $('#logger-sensors-modal').on('click', '.confirm-sensor-delete', function(e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                url: '/website-api/rest/logger/type/'+loggerSensorsModal.currentLoggerTypeId+'/sensor/'+loggerSensorsModal.currentSensorId+'/remove',
                //data: JSON.stringify(sensor),
                success: function (sensor) {
                    loadSensorsList();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });



        });

        $('#logger-sensors-modal').on('click', '.cancel-sensor-delete', function(e) {
            e.preventDefault();
            loggerSensorsModal.load('view-logger-sensor');
        });

        $('#logger-sensors-modal').on('click', '.add-new-filetype', function(e) {
            e.preventDefault();

            var filetype = {
                name : $('#file-type-name').val(),
                description : $('#file-type-description').val(),
                conversionWorkflows : $('#file-type-workflow').val() || []
            };

            $.ajax({
                type: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                url: '/website-api/rest/logger/filetype/save',
                data: JSON.stringify(filetype),
                success: function (filetype) {

                    loadSensorsList();
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });



        });

        // Data loading for modal content and helper functions
        function loadSensorsList (show) {

            if (show !== false) {
                show = true;
            }

            var sensors = $.get('/website-api/rest/logger/type/'+loggerSensorsModal.currentLoggerTypeId+'/sensor/all');

            sensors.done(function(data){
                if (data && data.length) {
                    var table = $('<table>').addClass('table').append('<thead><tr><th>Name</th><th>File Type</th></tr></thead>');

                    $.each(data, function(i, sensor){
                        var row = $('<tr>').addClass('hover').attr('data-sensor-id', sensor.id).data('sensor', sensor),
                                name = $('<td>').html(sensor.name),
                                desc = $('<td>').html(sensor.fileType.name);

                        row.append(name);
                        row.append(desc);

                        table.append(row);
                    });

                    $('#list-logger-sensors .modal-body').html(table);
                } else {
                    $('#list-logger-sensors .modal-body').html('<p>No sensors available for this logger type.</p>');



                    var filetypes = $.get('/website-api/rest/logger/filetype/all');

                    filetypes.done(function(data){
                        if (data && data.length < 1) {

                            var wf = $.get('/website-api/rest/workflows');

                            wf.done(function(workflows){
                                if (workflows && workflows.length){
                                    var wfSelect = $('#file-type-workflow');

                                    wfSelect.html('');

                                    $.each(workflows, function(i, workflow){
                                        wfSelect.append($('<option>').val(workflow.id).html(workflow.name));
                                    });
                                }

                                loggerSensorsModal.load('no-sensors-no-filetypes');
                            });

                            wf.fail(function(){

                            });


                        }
                    });

                    filetypes.fail(function(){

                    });
                }

                if (show) {
                    loggerSensorsModal.load('list-logger-sensors', loggerSensorsModal.currentLoggerTypeId);
                }
            });

            sensors.fail(function(error){
                console.log(error);
            });
        }

        function updateSensorView() {
            var sensor = loggerSensorsModal.currentSensor,
                    viewDL = $('<dl>').addClass('dl-horizontal');

            // Add name, description and type to list
            viewDL.append($('<dt>').html('Name'));
            viewDL.append($('<dd>').html(sensor.name));

            viewDL.append($('<dt>').html('File Type'));
            viewDL.append($('<dd>').html(sensor.fileType.name));

            $('#view-logger-sensor .modal-body').html(viewDL);


        }

        function updateSensorEdit() {
            var sensor = loggerSensorsModal.currentSensor || {name:'', fileType: {id:''}};

            // Fill in the default fields
            $('#edit-logger-sensor input[name="name"]').val(sensor.name);
            $('#edit-logger-sensor select[name="sensor-filetype"]').val(JSON.stringify(sensor.fileType));

        }

        function createAdditionalField(labelText, name, value) {
            name = name || '';
            value = value || '';

            var input = $('<input>').attr('type', 'text');
            var keyInput = input.clone().val(name).attr('placeholder', 'Property Name'),
                    valueInput = input.clone().val(value).attr('placeholder', 'Property Value');

            var label = $('<label>').addClass('control-label').html(labelText),
                    controls = $('<div>').addClass('controls controls-row').append(keyInput).append(valueInput),
                    controlGroup = $('<div>').addClass('control-group');



            return controlGroup.append(label).append(controls);
        }

        function appendAdditionalField($container, name, value) {
            var currentInputCount = $container.children().length;
            $container.append(createAdditionalField('Additional Field ' + (currentInputCount + 1), name, value));
        }
    });

</script>