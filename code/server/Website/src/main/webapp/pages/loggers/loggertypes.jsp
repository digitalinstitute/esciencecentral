<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    String searchText = (request.getParameter("searchtext") != null) ? request.getParameter("searchtext") : "";

%>


<h2>Logger Types</h2>
<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <form action="loggertypes.jsp">
            <div class="input-append">
                <input id="searchtext" name="searchtext" type="text" placeholder="Search" value="<%=searchText%>">
                <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                    <button class="btn">
                        <i class="icon-search"></i>Search
                    </button>
                </div>
            </div>
        </form>

        <%

            try {
                List<LoggerType> loggerTypes = (List<LoggerType>)EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, 0, Integer.MAX_VALUE);

                List<LoggerType> searchResults = new ArrayList<LoggerType>();
                if (!searchText.trim().isEmpty()) {

                    String searchtext = request.getParameter("searchtext");
                    System.out.println(searchtext);

                    for(LoggerType type : loggerTypes) {

                        if(type.getName().contains(searchtext) || type.getManufacturer().contains(searchtext)) {
                            searchResults.add(type);
                        }
                    }
                }

                System.out.println(loggerTypes.size());
        %>
        <ul id="device-list" class="unstyled clearfix">

            <%
                LoggerType loggerType;

                Iterator i = (searchResults.size() > 0) ? searchResults.iterator() : loggerTypes.iterator();

                while (i.hasNext()) {
                    loggerType = (LoggerType) i.next();

            %>

            <li>
                <a href="loggertype.jsp?id=<%=loggerType.getId()%>">
                    <h4><%=loggerType.getName()%></h4>
                    <h6><%=loggerType.getManufacturer()%></h6>
                </a>
            </li>

            <%

                }

            %>
            <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>
        </ul>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $('#create-type').on('hidden', function () {

            document.location = "../../pages/loggers/loggertypes.jsp";

        })

    });

</script>
<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
<%@include file="createType.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>