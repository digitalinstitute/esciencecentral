<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<h2>Logger Types</h2>
<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <form action="list.jsp">
            <div class="input-append">
                <input id="searchtext" name="searchtext" type="text" placeholder="Search">
                <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                    <button class="btn">
                        <i class="icon-search"></i>Search
                    </button>
                </div>
            </div>
        </form>

        <%

            try {
                List loggerTypes;
                if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
                    loggerTypes = (List<LoggerType>)EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, 0, 100);
                }
                else
                {
                    loggerTypes = (List<LoggerType>)EJBLocator.lookupLoggersBean().getLoggerTypes(ticket, 0, 100);
                }
        %>
        <ul id="device-list" class="unstyled clearfix">

            <%
                LoggerType loggerType;
                Iterator i = loggerTypes.iterator();
                while (i.hasNext()) {
                    loggerType = (LoggerType) i.next();

            %>

            <li>
                <a href="loggertype.jsp?id=1" class="img-polaroid">
                    <span><%=loggerType.getName()%></span>
                    <span><%=loggerType.getManufacturer()%></span>
                </a>
            </li>

            <%

                }

            %>

        </ul>
    </div>
    <div class="span3">
        <%@include file="menu.jspf" %>
        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="#create-type" data-toggle="modal">Add Logger Type</a></h4>
            </li>
        </ul>
    </div>
</div>

<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>

<%@include file="createType.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>