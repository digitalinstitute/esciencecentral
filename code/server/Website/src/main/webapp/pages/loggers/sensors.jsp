<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<div>
    <h2>Sensors</h2>
    <div class="row-fluid">
        <div class="span9">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" id="sensors">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>File Type</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <div class="span3">
            <ul class="unstyled sidebar-menu">
                <li>
                    <h6><a href="loggertypes.jsp">Logger Types</a></h6>
                </li>
                <li>
                    <h6><a href="configurations.jsp">Configurations</a></h6>
                </li>
                <li>
                    <h6><a href="filetypes.jsp">File Types</a></h6>
                </li>
                <li>
                    <h6>Sensors</h6>
                </li>
                <li>
                    <h6><a href="uploadusers.jsp">Upload Users</a></h6>
                </li>
            </ul>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/tools/dataTables.tableTools.js"></script>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link type="text/css" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css" />
<link type="text/css" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css" />
<script type="text/javascript">

    var deletions = [];

    $(document).ready(function(){

        var sensorsTable;
        var selectedSensors = [];

        $('#sensors').on('click', 'tr', function () {

            var rowData = sensorsTable.fnGetData(sensorsTable.fnGetPosition(this));
            var index = jQuery.inArray(rowData.id, selectedSensors);

            if ( index === -1 )
            {
                selectedSensors.push(rowData);
            }
            else
            {
                selectedSensors.splice(index, 1);
            }

            if(selectedSensors.length == 1)
            {
                $('#editSensor').prop('disabled', false);
                $('#deleteSensor').prop('disabled', false);
            }
            else if(selectedSensors.length > 1)
            {
                $('#editSensor').prop('disabled', true);
                $('#deleteSensor').prop('disabled', false);
            }
            else
            {
                $('#editSensor').prop('disabled', true);
                $('#deleteSensor').prop('disabled', true);
            }

            $(this).toggleClass('active');
        });

        $(document).on("click", "#createSensor", function (event) {

            $("#sensorName").val('');
            $("#sensorFileType").val('');

            $("#sensor-modal-label").text("Create Sensor");
            $("#edit-sensor").css('display', 'block');
            $("#delete-sensor").css('display', 'none');
            $("#submit-sensor").text("Create Sensor");

            $("#sensor-modal").modal("show");
        });

        $(document).on("click", "#editSensor", function (event)
        {


            $("#sensorName").val(selectedSensors[0].name);
            $("#sensorFileType").val(selectedSensors[0].fileType.name);

            $("#sensor-modal-label").text("Edit Sensor");
            $("#edit-sensor").css('display', 'block');
            $("#delete-sensor").css('display', 'none');
            $("#submit-sensor").text("Edit Sensor");

            $("#sensor-modal").modal("show");
        });

        $(document).on("click", "#deleteSensor", function (event)
        {
            deletions = selectedSensors;

            var message;

            if(deletions.length == 1)
            {
                message = "<p>Are you sure you wish to delete this sensor?</p>";
                $("#sensor-modal-label").text("Delete Sensor");
                $("#delete-sensors").text("Delete Sensor");
            }
            else if(deletions.length > 1)
            {
                message = "<p>Are you sure you wish to delete these " + deletions.length + " sensors?</p>";
                $("#sensor-modal-label").text("Delete Sensors");
                $("#delete-sensors").text("Delete Sensors");
            }

            $("#delete-upload-user .modal-body").html(message);

            $("#edit-sensor").css('display', 'none');
            $("#delete-sensor").css('display', 'block');

            $("#sensor-modal").modal("show");
        });

        sensorsTable = $('#sensors').dataTable( {
            "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "oLanguage": {
                "sSearch": ""
            },
            "bRetrieve": true,
            "sAjaxSource": "/website-api/rest/study/uploaders",
            "sAjaxDataProp": "",
            "fnServerParams": function ( aoData ) {
                aoData.push( { "start": 0, "end": -1 } );
            },
            "aoColumns": [
                {
                    "mData": "name"
                },
                {
                    "mDataProp": "fileType.name"
                }
            ]
        } );

        $('.dataTables_filter input').attr('placeholder', 'Search...');

        var editButtons =   '<button id="createSensor" class="createSensor btn">Create</button>' +
                '<button id="editSensor" class="editSensor btn" disabled="true">Edit</button>' +
                '<button id="deleteSensor" class="deleteSensor btn" disabled="true">Delete</button>';

        $("#sensors_wrapper .btn-group").html(editButtons);

    });

</script>
<%@include file="sensor.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>