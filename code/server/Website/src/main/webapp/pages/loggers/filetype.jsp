<%@ page import="com.connexience.server.model.project.FileType" %>
<%@ page import="com.connexience.server.model.workflow.WorkflowDocument" %>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ page import="com.connexience.server.model.project.ConversionWorkflow" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    String id = request.getParameter("id");

    FileType fileType = null;

    if(id!=null)
    {
        fileType = EJBLocator.lookupLoggersBean().getFileType(ticket, Integer.parseInt(id));
    }

    menu_loggerFileType = fileType;

    String action = request.getParameter("action");

    if(action!=null && action.equals("edit"))
    {
        editing = true;
    }

    if(id==null)
    {  %>
        <h2>Create New File Type</h2>
  <%  }

    else {%>
<h2><%= fileType.getName()%></h2> <% } %>
<div class="btn-group action-menu">
    <% if(!editing) { %>
    <a href="../../pages/loggers/filetype.jsp?id=<%=menu_loggerFileType.getId()%>&action=edit" class="btn">Edit</a>
        <%--<%if(fileType.getSensors().size() == 0){ %>
        <a href="#delete-file-type-dialog" data-toggle="modal" class="btn">Delete</a>
        <%} else {%>
        <a href="#delete-file-type-dialog" data-toggle="modal" class="btn disabled">Delete</a>
        <% } %>--%>
    <% } else if(fileType != null) { %>
    <a href="#add-workflow" data-toggle="modal" class="btn">Add Conversion Workflow</a>
    <a href="#" id="save-file-type" class="btn">Save</a>
    <a href="../../pages/loggers/filetype.jsp?id=<%=id%>" class="btn">Cancel</a>
        <%--<%if(fileType.getSensors().size() == 0){ %>
        <a href="#delete-file-type-dialog" data-toggle="modal" class="btn">Delete</a>
        <%} else {%>
        <a href="#delete-file-type-dialog" data-toggle="modal" class="btn disabled">Delete</a>
        <% } %>--%>
    <% } else { %>
    <a href="#add-workflow" data-toggle="modal" class="btn">Add Conversion Workflow</a>
    <a href="#" id="save-file-type" class="btn">Save</a>
    <a href="../../pages/loggers/filetypes.jsp" class="btn disabled">Cancel</a>
    <a href="#delete-file-type-dialog" data-toggle="modal" class="btn disabled">Delete</a>
    <% } %>
</div>
<div class="row-fluid">
    <div class="span9">
       <%-- <%if(fileType.getSensors().size() != 0){ %>
        <div class="alert">
            <strong>Warning!</strong> This file type is currently in use and cannot be deleted.
        </div>
        <%}%>--%>
        <%if(editing && fileType != null){%>
        <div id="submit-type-message"></div>
        <form id="create-type-form">
        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                <input type="text" id="name" name="name" placeholder="Name" value="<%=fileType.getName()%>">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Description</label>
            <div class="controls">
                <textarea type="text" id="description" name="description" placeholder="Description"><%=fileType.getDescription()%></textarea>
            </div>
        </div>
        <h4>Conversion Workflows</h4>
        <ul id="conversion-workflows" class="unstyled">
            <%
                for (ConversionWorkflow conversion : fileType.getConversionWorkflows()) {
                    WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, conversion.getWorkflowDocumentId());
            %>

            <li class="conversion-workflow" id="<%=workflow.getId()%>">
                <a target="_blank" class="wf-thumbnail" href="../../pages/workflow/wfeditor.jsp?id=<%=workflow.getId()%>">
                    <img src="../../servlets/image?soid=<%=workflow.getId()%>&type=<%=ImageData.WORKFLOW_PREVIEW%>" alt="profile picture" class="img-polaroid" />
                </a>
                <h5><%=workflow.getName()%><a class="remove-workflow"><span class="icomoon-close"></span></a></h5>
                <div class="wf-description">
                    <small>
                        <%=workflow.getDescription()%>
                    </small>
                    <h6>
                        Created by <span><%=EJBLocator.lookupObjectInfoBean().getObjectName(ticket, workflow.getCreatorId())%></span>
                    </h6>
                </div>
            </li>
            <%}%>
        </ul>
        </form>
        <%}else if(editing && fileType == null){%>
        <div id="submit-type-message"></div>
        <form id="create-type-form">
        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                <input type="text" id="name" name="name" placeholder="Name">
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Description</label>
            <div class="controls">
                <textarea type="text" id="description" name="description" placeholder="Description"></textarea>
            </div>
        </div>
        <h4>Conversion Workflows</h4>
        <ul id="conversion-workflows" class="unstyled">
            <small>Use the menu above to add conversion workflows.</small>
        </ul>
        <%}else if(!editing && fileType != null){%>
            <p><%=fileType.getDescription()%></p>
            <hr>
            <h4><%= fileType.getConversionWorkflows().size()%> Conversion Workflows</h4>
            <ul id="conversion-workflows" class="unstyled">
                <%
                    for (ConversionWorkflow conversion : fileType.getConversionWorkflows()) {
                        WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, conversion.getWorkflowDocumentId());
                %>

                <li class="conversion-workflow" id="<%=workflow.getId()%>">
                    <a target="_blank" class="wf-thumbnail" href="../../pages/workflow/wfeditor.jsp?id=<%=workflow.getId()%>">
                        <img src="../../servlets/image?soid=<%=workflow.getId()%>&type=<%=ImageData.WORKFLOW_PREVIEW%>" alt="profile picture" class="img-polaroid" />
                    </a>
                    <h5><%=workflow.getName()%></h5>
                    <div class="wf-description">
                        <small>
                            <%=workflow.getDescription()%>
                        </small>
                        <h6>
                            Created by <span><%=EJBLocator.lookupObjectInfoBean().getObjectName(ticket, workflow.getCreatorId())%></span>
                        </h6>
                    </div>
                </li>
                <%}%>
            </ul>
        <%}%>
        </form>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<div id="delete-file-type-dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="delete-file-type-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="delete-file-type-label">Delete File Type</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you wish to delete this file type?</p>
    </div>
    <div class="modal-footer">
        <button type="submit" id="delete-file-type" class="btn">Confirm</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        $('#conversionWorkflow').selectpicker({
            showSubtext: true
        });

        var form = $( "#create-type-form" );
        form.validate({
            rules: {
                name: {required: true, maxlength: 255},
                description: {required: true, maxlength: 255}
            }
        });

        $(document).on('click', '.remove-workflow', function(){

            $(this).closest('.conversion-workflow').remove();



        });

        $(document).on("click", "#save-file-type", function (event) {

            event.preventDefault();

            if(form.valid())
            {
                var fileType = new Object();
                <% if(fileType != null)
                { %>
                    fileType.id = '<%=fileType.getId() %>'
              <%  }  %>
                fileType.name = $("#name").val();
                fileType.description = $("#description").val();

                var workflows = new Array();

                $.each($('#conversion-workflows li'), function(key, value){

                    workflows.push($(this).attr('id'));

                    console.log(workflows);

                });

                fileType.conversionWorkflows = workflows;

                console.log(fileType);

                var encodedFileType = JSON.stringify(fileType);

                $.ajax({
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/logger/filetype/save',
                    data: encodedFileType,
                    success: function (fileType) {
                        console.log(fileType);
                        document.location = "../../pages/loggers/filetype.jsp?id=" + fileType.id;

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);
                    }
                });
            }
            else
            {
                if($('#validation-message').length == 0)
                {
                    if($('.validation-message').length == 0)
                    {
                        var message = "Correct these errors to create file logger type.";
                        var type = "error";
                        var target = $("#submit-type-message");
                        var cssClass = "validation-message";

                        flashMessage(message, type, target, cssClass)
                    }
                }
            }
        });

        <% if(fileType != null) { %>

        $(document).on('click', '#delete-file-type', function(){

            $.ajax({
                type: 'POST',
                url: '/website-api/rest/logger/filetype/<%=fileType.getId()%>/remove',
                success: function (fileType) {
                    console.log(fileType);

                    document.location = "../../pages/loggers/filetypes.jsp";

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });

        });

        <% } %>
    });

</script>
<%@include file="add-workflow.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>