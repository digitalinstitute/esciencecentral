<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="com.connexience.server.model.document.DocumentRecord" %>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<%


    try {

        Long loggerTypes = EJBLocator.lookupLoggersBean().getLoggerTypeCount(ticket);
        Long loggerConfigurations = EJBLocator.lookupLoggersBean().getLoggerConfigurationCount(ticket);
        Long loggers = EJBLocator.lookupLoggersBean().getLoggerCount(ticket);
        Long fileTypes = EJBLocator.lookupLoggersBean().getFileTypeCount(ticket);
        Long deployments = EJBLocator.lookupLoggersBean().getLoggerDeploymentCount(ticket);
        String loggerTypeId = "0";

        if (request.getParameter("loggerTypeId") != null){
            loggerTypeId = request.getParameter("loggerTypeId");
        }

        String loggerId = "";
        List<String> results = null;
        String fileName = "";
        DocumentRecord doc = new DocumentRecord();
        String errorMessage = "";

        String contentType = request.getContentType();

        if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0))
        {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List items = upload.parseRequest(request);

            Iterator iter = items.iterator();

            ArrayList<String> fileNames = new ArrayList<String>();
            ArrayList<Long> sizes = new ArrayList<Long>();


            while (iter.hasNext())
            {
                FileItem item = (FileItem) iter.next();

                if(item.getFieldName().equals("loggerType")){
                    loggerId = item.getString();
                }

                if (!item.isFormField())
                {
                    fileName = item.getName();

                    fileNames.add(fileName);
                    sizes.add(item.getSize());

                    String dataFolderId = user.getHomeFolderId();

                    doc.setName(fileName);
                    doc.setContainerId(dataFolderId);
                    doc.setCreatorId(ticket.getUserId());
                    doc.setProjectId(ticket.getDefaultProjectId());
                    doc = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, doc);

                    StorageUtils.upload(ticket, item.getInputStream(), item.getSize(), doc, "Saved by user: " + user.getDisplayName());
                }
            }

            results = EJBLocator.lookupStudyParserBean().parseLoggerDefinitions(ticket, doc.getId(), Integer.parseInt(loggerId));

        }

%>


<h2>Logger Summary</h2>
<div class="row-fluid">
    <%-- An example List of items--%>
    <div class="span9">
        <form id="loggers-upload" method="POST" action="upload-loggers.jsp" enctype="multipart/form-data" class="clearfix">
            <ol>
                <li class="stage-type current-stage">
                    <div class="control-group">
                        <label class="control-label">Logger Type</label>
                        <div class="controls">
                            <select type="text" name="loggerType" id="loggerType">
                                <option value="">Select a logger type..</option>
                            </select>
                            <span class="help-block">Choose the type of loggers you are creating</span>
                        </div>
                    </div>
                </li>
                <li class="stage-file">
                    <div class="control-group">
                        <label class="control-label">Loggers File</label>
                        <div class="controls">
                            <div class="fileupload fileupload-new clearfix" data-provides="fileupload">
                                <span class="btn btn-file">
                                    <span class="fileupload-new">Select file</span>
                                    <span class="fileupload-exists">Change</span>
                                    <input name="loggersFile" id="loggersFile" type="file" />
                                </span>
                                <div id="file-preview" class="stage-preview">
                                    <h6 class="fileupload-preview"></h6>
                                </div>
                            </div>
                            <span class="help-block">Choose the excel file containing the loggers you wish to use for this study. Logger IDs should be in a column called 'Logger ID'.</span>
                        </div>
                    </div>
                </li>
                <li class="stage-upload">
                    <div class="control-group">
                        <div class=controls>
                            <button id="submit-loggers" class="btn" disabled="disabled">
                                <i class="icomoon-cloud-upload"></i>&nbsp;Upload
                            </button>
                        </div>
                    </div>
                </li>
            </ol>
        </form>
        <hr>
        <div class="row-fluid" id="upload-results">
            <div class="span12">
                <% if(errorMessage != "") { %>
                <p><%=errorMessage%></p>
                <% } %>
                <% if(results != null) { %>
                <h5><%=results.size()%> Warnings</h5>
                <ul id="parsingWarnings" class="unstyled">
                    <% for(String problem : results) { %>
                    <li class="alert">
                        <i class="icomoon-warning"></i>
                        <strong>Warning!</strong>&nbsp;<%= problem %>
                    </li>
                    <% } %>
                </ul>
                <% } %>
            </div>
        </div>
    </div>
    <div class="span3">
        <%@include file="../loggers/menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        $(document).on('change', '#loggerType', function(){

            if($(this).val() != "")
            {
                $('#loggersFile').removeAttr('disabled');
                $('.stage-type').removeClass('current-stage');
                $('.stage-file').addClass('current-stage');
            }
            else
            {
                $('#loggersFile').attr('disabled', 'disabled');
                $('.stage-type').addClass('current-stage');
                $('.stage-file').removeClass('current-stage');
            }

        });

        $(document).on('change', '#loggersFile', function(){

            if($(this).val() != "")
            {
                $('#file-preview h2').css('display', 'block');
                $('#submit-loggers').removeAttr('disabled');
                $('.stage-file').removeClass('current-stage');
                $('.stage-upload').addClass('current-stage');
            }
            else
            {
                $('#file-preview h2').css('display', 'none');
                $('#submit-loggers').attr('disabled', 'disabled');
                $('.stage-file').addClass('current-stage');
                $('.stage-upload').removeClass('current-stage');
            }
        });

        $('#upload-loggers').on('hidden', function(){

            $('#loggers-upload')[0].reset();

            $('.current-stage').removeClass('current-stage');
            $('.stage-1').addClass('current-stage');
            $('#loggersFile').removeAttr('disabled');
            $('#submit-loggers').attr('disabled', 'disabled');

        });

        $.ajax({
            type: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            url: '/website-api/rest/logger/type/all?start=0&end=<%= Integer.MAX_VALUE%>',
            success: function (loggers) {
                $.each(loggers, function(key, logger){

                    $('#loggerType').append('<option value="' + logger.id + '">' + logger.name + '</option>');

                });

                $('#loggerType').val(<%=loggerTypeId%>).trigger('change');
            },
            error: function(xhr, ajaxOptions, thrownError) {

            }
        });

    });

</script>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>