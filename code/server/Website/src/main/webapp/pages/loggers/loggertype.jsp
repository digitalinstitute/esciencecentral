<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%

    try {

        LoggerType loggerType = null;

        if(request.getParameter("id") == null)
        {
            response.sendRedirect("overview.jsp");
        }

        else if (request.getParameter("id") != null && !request.getParameter("id").trim().isEmpty()) {
            loggerType = EJBLocator.lookupLoggersBean().getLoggerType(ticket, Integer.parseInt(request.getParameter("id")));
            menu_loggerType = loggerType;
        }

        if(loggerType == null) {
            response.sendRedirect("overview.jsp");
        }

        if(EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId()))
        {
            String action = request.getParameter("action");

            if(action!=null && action.equals("edit"))
            {
                editing = true;
            }
        }

%>

<h2><%=loggerType.getManufacturer()%>: <%=loggerType.getName()%></h2>
<% if(loggerType.isPhysicalDevice()) { %>
<p><strong>Physical Device</strong></p>
<% } else { %>
<p><strong>Non-Physical Device</strong></p>
<% } %>

<div id="avail-summary" class="row-fluid fleet-availability-control-wrapper">
    <div class="span6"><div><i class="icomoon-calendar"></i> Available loggers shown from <strong>today</strong> for <strong>1 month</strong> <a id="editAvailability" class="btn btn-mini" href="#"><i class="icomoon-pencil"></i> Edit</a> <button id="showAllLoggers" class="btn btn-mini"><i class="icomoon-stack"></i> Show All</button></div></div>
</div>

<div style="display:none;" class="row-fluid fleet-availability-control-wrapper">
    <div class="span12">
        <form class="form-inline">
            <div class="form-group">
                <label for="startDateAvailability"><i class="icomoon-calendar"></i> Availability from:</label>
                <input type="text" class="form-control span2" id="startDateAvailability" placeholder="">

                <label for="dateUnitCount">for</label>
                <select name="" class="span1" id="dateUnitCount">
                    <option selected value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                </select>

                <label for="dateUnit"></label>
                <select name="" class="span2" id="dateUnit">
                    <option value="days">Day</option>
                    <option value="weeks">Week</option>
                    <option selected value="months">Month</option>
                </select>

                <label for="cancelAvailabilityEdit"></label>
                    <button id="cancelAvailabilityEdit" class="btn btn-mini"><i class="icomoon-close"></i>&nbsp;Close Edit</button>
            </div>
        </form>
    </div>
</div>
<div class="row-fluid fleet-availability-control-wrapper" style="display:none">
    <div class="span12">
        <form action="" class="form-inline">
            <div class="form-group">
                <label for="showFilterByDate"></label>
                <button id="showFilterByDate" class="btn btn-default"><i class="icomoon-calendar"></i> Filter By Availability</button>
            </div>
        </form>
    </div>
</div>

<div id="device" class="row-fluid">
    <div class="span9">


        <%--<div class="btn-group action-menu">
            <% if(editing && EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){ %>
            <a id="save-type" class="btn btn-small">Save</a>
            <a href="../../pages/loggers/loggertype.jsp?id=<%=menu_loggerType.getId()%>" class="btn btn-small">Cancel</a>
            <a href="#delete-type-dialog" data-toggle="modal" class="btn btn-small">Delete</a>
            <a href="#" class="btn btn-small"disabled="disabled">Upload Logger List</a>
            <% }else if(!editing && EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), ticket.getOrganisationId())){ %>
            <a href="../../pages/loggers/loggertype.jsp?id=<%=menu_loggerType.getId()%>&action=edit" class="btn btn-small">Edit</a>
            <a href="#delete-type-dialog" data-toggle="modal" class="btn btn-small">Delete</a>
            <a href="#upload-loggers" data-toggle="modal" class="btn btn-small">Upload Logger List</a>
            <% }else{ %>
            <a href="#" class="btn btn-small" disabled="disabled">Edit</a>
            <a href="#" class="btn btn-small" disabled="disabled">Delete</a>
            <a href="#upload-loggers" data-toggle="modal" class="btn btn-small">Upload Logger List</a>
            <% } %>
        </div>--%>

        <%if(editing){%>
        <div class="row-fluid">
            <div class="span12">
                <form id="logger-type-form">
                    <div class="control-group">
                        <label class="control-label">Model</label>
                        <div class="controls">
                            <input type="text" id="model" name="model" placeholder="Model" value="<%=loggerType.getName()%>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Manufacturer</label>
                        <div class="controls">
                            <input type="text" id="manufacturer" name="manufacturer" placeholder="Manufacturer" value="<%=loggerType.getManufacturer()%>">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <label class="checkbox">
                                <input id="physicalDevice" name="physicalDevice" type="checkbox" checked="<%=loggerType.isPhysicalDevice()%>"> Physical Device
                            </label>
                        </div>
                    </div>
                    <div id="logger-sensors">

                    </div>
                    <button id="add-sensor" class="btn" disabled="disabled">Add Sensor</button>
                    <div id="noFileTypes" class="alert" style="display:none;">
                        <h4>Warning!</h4>
                        <span>You must create at least one file type to be able to add a sensor to this logger type.</span>
                    </div>
                </form>
            </div>
        </div>
        <%}else{%>
        <%--<div class="row-fluid">
            <div class="span12">
                &lt;%&ndash;<img src="geneactiv.png" />&ndash;%&gt;
                <div id="type-attributes" class="clearfix">
                    <dl class="dl-horizontal">
                        <dt>Model</dt>
                        <dd><%=loggerType.getName()%></dd>
                        <dt>Manufacturer</dt>
                        <dd><%=loggerType.getManufacturer()%></dd>
                        <dt>Type</dt>
                        <% if(loggerType.isPhysicalDevice()) { %>
                        <dd>Physical Device</dd>
                        <% } else { %>
                        <dd>Non-Physical Device</dd>
                        <% } %>
                    </dl>
                </div>
                <div id="sensor-attributes" class="clearfix">
                    <h4>Sensors</h4>
                </div>
            </div>
        </div>--%>
        <%}%>

        <% if (!editing) { %>
        <div class="row-fluid">
            <div class="span12">
                <%--<h4 class="clearfix">Loggers</h4>--%>
                <table id="loggers" cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Serial Number</th>
                        <th>Location</th>
                        <%--<th width="40px"></th>--%>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>

        <hr/>

<%--        <div class="row-fluid">
            <div class="span12">
                <div id="sensor-attributes" class="clearfix">
                    <h4>Sensors</h4>
                </div>
            </div>
        </div>--%>


        <% } %>
    </div>
    <div class="span3">
        <%@include file="menu.jspf" %>
    </div>
</div>
<div id="delete-type-dialog" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="delete-type-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="delete-type-label">Delete Logger Type</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you wish to delete this logger type?</p>
    </div>
    <div class="modal-footer">
        <button type="submit" id="delete-type" class="btn">Confirm</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
</div>


<script src="../../scripts/deployment-date-filter/deployment-date-filter.js"></script>

<script>

    //deploymentDateFilter.eventCallbacks.changeDate = updateStatusBars;

    //deploymentDateFilter.eventCallbacks.changeDateUnitOrCount = updateStatusBars;

    deploymentDateFilter.eventCallbacks.ajaxSuccess = function(data) {
        if (data && data.length) {

            $('#editLogger').prop('disabled', true);
            $('#deleteLogger').prop('disabled', true);
            $('#viewActivity').prop('disabled', true);
            selectedLoggers = [];
            loggersTable.fnClearTable();
            loggersTable.fnAddData(data);

            // Populate the activity select

            $('#activity-select').html('');

            $.each(data, function(i, item){
                $('#activity-select').append($('<option>').html(item.serialNumber).val(item.id));
            });
        }
    };

    var loggerTypeIdsString = '[<%=loggerType.getId()%>]';

    if (loggerTypeIdsString && loggerTypeIdsString !== '') {

            deploymentDateFilter.loggerTypeIds = JSON.parse(loggerTypeIdsString);


    } else {

    }



</script>

<script type="text/javascript">

var selectedLoggers = [];
var fileTypes = {};
var updateTableButtons = function(){};
var loggersTable;

$(document).ready(function(){

    var form = $( "#logger-type-form" );

    form.validate({
        rules: {
            manufacturer: {required: true, maxlength: 255},
            model: {required: true, maxlength: 255}
        }
    });

    var editButtons =   '<button id="createLogger" class="btn">Create</button>' +
            '<button id="editLogger" class="btn" disabled="true">Edit</button>' +
            '<button id="deleteLogger" class="btn" disabled="true">Delete</button>' +
            '<button id="viewActivity" class="btn" disabled="true">View Deployments</button>';

    loggersTable = $('#loggers').dataTable( {
        "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "oLanguage": {
            "sSearch": ""
        },
        "bRetrieve": true,
        //"sAjaxSource": "/website-api/rest/logger/type/<%=request.getParameter("id")%>/loggers/all",
        "sAjaxDataProp": "",
        "fnInitComplete" : function() {
            deploymentDateFilter.init('loggers');
        },
        "aoColumns": [
            {
                "mData": "id",
                "bSearchable": false,
                "bVisible": false
            },
            {
                "mData": "serialNumber",
                "bSortable": true
            },
            {
                "mData": "location",
                "bSortable": true

            }/*,
            {
                "mDataProp": null,
                "sDefaultContent": '<span></span>',
                "fnRender": function(oObj) {

                    return '<span class="center icomoon-share" data-id="' + oObj.aData.id + '"></span>';
                }
            }*/
        ]
    });

    $("#loggers_wrapper .btn-group").html(editButtons);
    $('.dataTables_filter input').attr('placeholder', 'Search...');

    updateTableButtons = function(activeRow) {

        var el;

        if (activeRow.nodeName && activeRow.nodeName === 'TR'){
            el = activeRow;
        } else {
            el = this;
        }

        var rowData = loggersTable.fnGetData(loggersTable.fnGetPosition(el));


        if (! $(loggersTable.fnGetNodes(loggersTable.fnGetPosition(el))).hasClass('active')){
            $('#currentLoggerIndex').val(loggersTable.fnGetPosition(el));
            currentRow = loggersTable.fnGetPosition(el);
        }


        var index = jQuery.inArray(rowData, selectedLoggers);

        if ( index === -1 )
        {
            selectedLoggers.push(rowData);
        }
        else
        {
            selectedLoggers.splice(index, 1);
        }

        if(selectedLoggers.length == 1)
        {
            $('#editLogger').prop('disabled', false);
            $('#deleteLogger').prop('disabled', false);
            $('#viewActivity').prop('disabled', false);
        }
        else if(selectedLoggers.length > 1)
        {
            $('#editLogger').prop('disabled', true);
            $('#deleteLogger').prop('disabled', false);
            $('#viewActivity').prop('disabled', true);
        }
        else
        {
            $('#editLogger').prop('disabled', true);
            $('#deleteLogger').prop('disabled', true);
            $('#viewActivity').prop('disabled', true);
        }

        $(el).toggleClass('active');
    };

    $('table#loggers').on('click', 'tr', updateTableButtons );

    $(document).on("click", "#createLogger", function (event) {

        $('#serialNumber').val('');
        $('#location').val('');
        $('#loggerId').val('');
        $('#edit-logger .additional-properties').html('');
        $('#loggerTypeId').val(<%=loggerType.getId()%>);

        $('#edit-logger').css('display','block');
        $('#delete-logger').css('display','none');
        $('#logger-label').text('Create Logger');
        $('#submit-logger').text('Create Logger');

        $("#logger").modal("show");
    });

    $(document).on("click", "#editLogger", function (event)
    {
        $('#serialNumber').val(selectedLoggers[0].serialNumber);
        $('#location').val(selectedLoggers[0].location);
        $('#loggerId').val(selectedLoggers[0].id);
        $('#loggerTypeId').val(<%=loggerType.getId()%>);

        $('#edit-logger').css('display','block');
        $('#delete-logger').css('display','none');
        $('#logger-label').text('Edit Logger');
        $('#submit-logger').text('Edit Logger');

        $('#edit-logger .additional-properties').html('');

        if (selectedLoggers[0].additionalProperties) {
            $.each(selectedLoggers[0].additionalProperties, function(key, value) {
                appendAdditionalField($('#edit-logger .additional-properties'), key, value);
            });
        } else {
            $('#edit-logger .additional-properties').html('');
        }

        $("#logger").modal("show");
    });

    $(document).on("click", "#deleteLogger", function (event)
    {
        $('#edit-logger').css('display','none');
        $('#delete-logger').css('display','block');

        if(selectedLoggers.length == 1)
        {
            $('#delete-logger .modal-body').html('<p>Are you sure you wish to delete this logger?</p>');
            $('#logger-label').text('Delete Logger');
            $('#delete-loggers').text('Delete Logger');
        }
        else if(selectedLoggers.length > 1)
        {
            $('#delete-logger .modal-body').html('<p>Are you sure you wish to delete the ' + selectedLoggers.length + ' selected loggers?</p>');
            $('#logger-label').text('Delete Loggers');
            $('#delete-loggers').text('Delete Loggers');
        }

        $("#logger").modal("show");
    });

    $(document).on('click', '#viewActivity', function() {
        $('#currentLoggerId').val(selectedLoggers[0].id);
        $('#currentLoggerSerial').val(selectedLoggers[0].serialNumber);
        $('#loggerDeploymentSerial').html(selectedLoggers[0].serialNumber);
        $('#selectedRowNumber').val(selectedLoggers[0].serialNumber);
        $('#show-logger-activity a:first').tab('show');
        $('#logger-activity').modal('show');
    });

    $(document).on("click", "#save-type", function (event) {

        event.preventDefault();

        if(form.valid())
        {
            var loggerType = new Object();
            loggerType.id = <%=loggerType.getId()%>
            loggerType.manufacturer = $("#manufacturer").val();
            loggerType.name = $("#model").val();
            loggerType.physicalDevice = $('#physicalDevice').is(":checked");

            var sensors = [];

            $.each($('#logger-sensors .controls'), function(key, input){

                var id = $(this).data('id');

                var sensor = {};
                var sensorName = $(this).children('input').eq(0).val();
                var sensorFileType = $(this).children('select').eq(0).val();

                if(id != null)
                {
                    sensor.id = id;
                }

                sensor.name = sensorName;
                sensor.fileType = fileTypes['type-' + sensorFileType];

                sensors.push(sensor);

            });

            loggerType.sensors = sensors;

            var encodedLoggerType = JSON.stringify(loggerType);

            $.ajax({
                type: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                url: '/website-api/rest/logger/type/save/',
                data: encodedLoggerType,
                success: function (loggerType) {

                    document.location = "../../pages/loggers/loggertype.jsp?id=<%=loggerType.getId()%>";

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                }
            });
        }
        else
        {
            if($('#validation-message').length == 0)
            {
                if($('.validation-message').length == 0)
                {
                    var message = "Correct these errors to create this logger type.";
                    var type = "error";
                    var target = $("#submit-type-message");
                    var cssClass = "validation-message";

                    flashMessage(message, type, target, cssClass)
                }
            }
        }
    });

    $(document).on('click', '#delete-type', function(){

        $.ajax({
            type: 'POST',
            url: '/website-api/rest/logger/type/<%=loggerType.getId()%>/remove',
            success: function (loggerType) {

                document.location = "../../pages/loggers/loggertypes.jsp";

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }
        });

    });

});
</script>
<%@include file="logger.jspf" %>
<%@include file="logger-activity.jspf" %>
<%@include file="sensor.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<%@include file="modals/logger-type.jspf"%>
<%@include file="modals/logger-configurations.jspf"%>
<%@include file="modals/logger-sensors.jspf"%>
<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>