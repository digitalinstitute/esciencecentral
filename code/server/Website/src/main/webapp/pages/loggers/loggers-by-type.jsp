<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%@ page import="com.connexience.server.model.project.study.LoggerType" %>
<%
  String searchText = (request.getParameter("searchtext") != null) ? request.getParameter("searchtext") : "";
  LoggerType loggerType = null;
%>


<h2>Loggers</h2>
<div class="row-fluid">
  <div class="span9">



    <table id="loggers" cellpadding="0" cellspacing="0" border="0" class="table table-bordered">
      <thead>
      <tr>
        <th>ID</th>
        <th>Serial Number</th>
        <th>Location</th>
        <th>Type</th>
        <%--<th width="40px"></th>--%>
      </tr>
      </thead>
      <tbody>

      </tbody>
      <tfoot>

      </tfoot>
    </table>
  </div>
  <div class="span3">
    <%@include file="menu.jspf"%>
  </div>
</div>
<script type="text/javascript">

  $(document).ready(function(){

    $('#create-type').on('hidden', function () {

      document.location = "../../pages/loggers/loggertypes.jsp";

    })

  });

  /*$.ajax({
    type: 'GET',
    url: '/website-api/rest/logger/all',
    data: "start=0&size=<%=Integer.MAX_VALUE%>",
    success: function (data) {
      console.log(data)
    }
  });*/

  var editButtons =   '<button id="createLogger" class="btn">Create</button>' +
          '<button id="editLogger" class="btn" disabled="true">Edit</button>' +
          '<button id="deleteLogger" class="btn" disabled="true">Delete</button>';

  var selectedLoggers = [];

  var loggersTable = $('#loggers').dataTable( {
    "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    "oLanguage": {
      "sSearch": ""
    },
    "bRetrieve": true,
    "sAjaxSource": "/website-api/rest/logger/all",
    "sAjaxDataProp": "",
    "aoColumns": [
      {
        "mData": "id",
        "bSearchable": false,
        "bVisible": false
      },
      {
        "mData": "serialNumber",
        "mRender": function(data){
          return data;
        }
      },
      {
        "mData": "location"
      },
      {
        "mData": "loggerType.name"
      }/*,
       {
       "mDataProp": null,
       "sDefaultContent": '<span></span>',
       "fnRender": function(oObj) {

       return '<span class="center icomoon-share" data-id="' + oObj.aData.id + '"></span>';
       }
       }*/
    ]
  });

  $("#loggers_wrapper .btn-group").html(editButtons);
  $('.dataTables_filter input').attr('placeholder', 'Search...');

  $('table#loggers').on('click', 'tr', function () {

    var rowData = loggersTable.fnGetData(loggersTable.fnGetPosition(this));
    var index = jQuery.inArray(rowData, selectedLoggers);

    if ( index === -1 )
    {
      selectedLoggers.push(rowData);
    }
    else
    {
      selectedLoggers.splice(index, 1);
    }

    if(selectedLoggers.length == 1)
    {
      $('#editLogger').prop('disabled', false);
      $('#deleteLogger').prop('disabled', false);
    }
    else if(selectedLoggers.length > 1)
    {
      $('#editLogger').prop('disabled', true);
      $('#deleteLogger').prop('disabled', false);
    }
    else
    {
      $('#editLogger').prop('disabled', true);
      $('#deleteLogger').prop('disabled', true);
    }

    $(this).toggleClass('active');
  } );

  function populateLoggerTypes(currentType) {

    var $loggerTypeSelect = $('#loggerType');

    $loggerTypeSelect.html('').attr('disabled', true);


    $.ajax({
      type: 'GET',
      url: '/website-api/rest/logger/type/all',
      data: "start=0&size=<%=Integer.MAX_VALUE%>",
      success: function (types) {

        $.each(types, function(key, type) {
          $loggerTypeSelect.append($("<option></option>")
                          .attr("value",type.id)
                          .text(type.name));
        });

        if (currentType) {
          $loggerTypeSelect.val(currentType);
        }

        $loggerTypeSelect.attr('disabled', false);
      }
    });
  }

  $(document).on("click", "#createLogger", function (event) {

    $('#serialNumber').val('');
    $('#location').val('');
    $('#loggerId').val('');

    $('#edit-logger').css('display','block');
    $('#delete-logger').css('display','none');
    $('#logger-label').text('Create Logger');
    $('#submit-logger').text('Create Logger');

    $("#logger").modal("show");
    populateLoggerTypes();
  });

  $(document).on("click", "#editLogger", function (event)
  {
    $('#serialNumber').val(selectedLoggers[0].serialNumber);
    $('#location').val(selectedLoggers[0].location);
    $('#loggerId').val(selectedLoggers[0].id);

    $('#edit-logger').css('display','block');
    $('#delete-logger').css('display','none');
    $('#logger-label').text('Edit Logger');
    $('#submit-logger').text('Edit Logger');

    $("#logger").modal("show");
    populateLoggerTypes(selectedLoggers[0].loggerType.id);
  });

  $(document).on("click", "#deleteLogger", function (event)
  {
    $('#edit-logger').css('display','none');
    $('#delete-logger').css('display','block');

    if(selectedLoggers.length == 1)
    {
      $('#delete-logger .modal-body').html('<p>Are you sure you wish to delete this logger?</p>');
      $('#logger-label').text('Delete Logger');
      $('#delete-loggers').text('Delete Logger');
    }
    else if(selectedLoggers.length > 1)
    {
      $('#delete-logger .modal-body').html('<p>Are you sure you wish to delete the ' + selectedLoggers.length + ' selected loggers?</p>');
      $('#logger-label').text('Delete Loggers');
      $('#delete-loggers').text('Delete Loggers');
    }

    $("#logger").modal("show");
  });



</script>

<%@include file="createType.jspf" %>
<%@include file="logger.jspf" %>
<%@include file="sensor.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>