<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@ page import="com.connexience.server.model.security.Ticket" %>
<%@ page import="com.connexience.server.util.SessionUtils" %>
<%--
    Document   : logoff
    Created on : 09-Jul-2008, 17:06:57
    Author     : hugo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<%

    //delete the remember me cookie
    Cookie rememberMe = SessionUtils.getCookie(request, "rememberMe");
    if (rememberMe != null) {
        //delete the rememberMe cookie
        rememberMe.setMaxAge(-1);
        rememberMe.setPath(request.getContextPath());
        response.addCookie(rememberMe);

        //delete the db value for the rememberMe cookie
        Ticket ticket = (Ticket) session.getAttribute("TICKET");
        if (ticket != null) {
            EJBLocator.lookupTicketBean().deleteRememberMe(ticket, rememberMe.getValue());
        }
    }

    if (session != null) {
        session.invalidate();
    }

    response.sendRedirect(request.getContextPath() + "/pages/front/index.jsp");
%>
<html>
<head>
</head>
<body>
</body>
</html>
