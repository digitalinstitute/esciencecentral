<%@ page import="com.connexience.server.web.tags.*" %>
<%@ page import="com.connexience.server.model.social.*"%>
<%--
  Index Page
  Author: Simon
--%>
<style>
    .icon-span{
        margin-right: 10px;
    }
</style>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%

    if(publicUser)
    {
      response.sendRedirect("../../pages/front/welcome.jsp");
    }
    %>

    <h4><span class="icomoon-storage icon-span"></span><span id="DataUsed">0 B</span><span> stored</span></h4>
    <h4><span class="icomoon-checkmark icon-span"></span><span id="Success">0</span><span> Workflows executed</span></h4>
    <h4><span class="icomoon-play3 icon-span"></span><span id="Running">0</span><span> Workflows running </span></h4>
    <h4><span class="icomoon-clock icon-span"></span><span id="Waiting">0</span><span> Workflows waiting </span></h4>
    <h4><span class="icomoon-notification icon-span"></span><span id="Failed">0</span><span> Workflows failed </span></h4>
    
    <hr>
<div class="row-fluid" id="_introDiv">
    
</div>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#_introDiv").load("../../styles/<%=themeFolder%>/intro.jspf");
        
        asyncJsonGet("../../servlets/stats?method=summary", function(returnData){
            var results = returnData.data;
            var keys = Object.keys(results);
            for(var i=0;i<keys.length;i++){
                var obj = document.getElementById(keys[i]);
                if(obj){
                    obj.innerHTML = results[keys[i]];
                }
            }
        });

        $('body').addClass('homepage')
    });
</script>