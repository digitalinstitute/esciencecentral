<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.social.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="java.text.*"%>

<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    String id = request.getParameter("id");
    User pageUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, id);

    boolean writable = false;
    if(pageUser.getId().equals(ticket.getUserId())){
        writable = true;
    }
%>
<!-- Caption Line -->
<h2><%=pageUser.getDisplayName()%></h2>
<h4>Shared Files</h4>
<div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">
        <div id="elfinder"></div>
        <div id="quickviewdiv"></div>
        <div id="editorchooser"></div>
    </div>
    <!-- Right Hand Side -->
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="profile.jsp?id=<%=id%>"><i class="icomoon-user"></i>Profile</a></h4>
            </li>
            <li>
                <h4><a href="../../data/<%=id%>/info"><i class="icomoon-notebook"></i>Metadata</a></h4>
            </li>
            <li>
                <h4><a href="profilefiles.jsp?id=<%=id%>"><i class="icomoon-copy"></i>Shared Files</a></h4>
            </li>
            <%if(writable){%>
            <li>
                <h4><a href="profile.jsp?id=<%=id%>&action=edit"><i class="icomoon-pencil"></i>Edit Profile</a></h4>
            </li>
            <li>
                <h4><a href="settings.jsp?id=<%=id%>"><i class="icomoon-cogs"></i>Settings</a></h4>
            </li>
            <%}%>
        </ul>
    </div>
</div>

<script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>
<link rel="stylesheet" type="text/css" href="../../scripts/jquery-selectbox/jquery.selectBox.css"/>

<%--QuickView--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">

<script type="text/javascript" src="../../scripts/util/list.js"></script>
<script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/json/json2.js"></script>
<script type="text/javascript" src="../../scripts/util/JsonUtil.js"></script>
<script type="text/javascript" src="../../scripts/jgrowl/jquery.jgrowl_minimized.js"></script>
<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="../../scripts/jquery-csv/jquery.csv-0.71.min.js"></script>
<script type="text/javascript" src="../../scripts/util/CSVTable.js"></script>


<!--%@ include file="../../pages/data/visualiser.jspf"%-->

<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>

<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

<script type="text/javascript" src="../../scripts/viewer/QuickView.js"></script>

<!-- elFinder CSS (REQUIRED) -->
<%--<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder2/css/elfinder.full.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder2/css/theme.css">--%>
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/elfinder-esc/css/elfinder.esc.css">

<%--ACL--%>
<link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen"
      charset="utf-8"/>

<!-- elFinder JS (REQUIRED) -->
<%--<script type="text/javascript" src="../../scripts/elfinder2/js/elfinder.full.js"></script>--%>
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.min.js"></script>
<script type="text/javascript" src="../../scripts/elfinder-esc/js/jQuery.elFinder.esc.js"></script>

<%--ACL--%>
<script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>

<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/accesscontrol/accesscontrol.css">
<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">

<![endif]-->

<script type="text/javascript" charset="utf-8">
$(document).ready(function()
{
  //create the file browser and plugin components
  <%if(id!=null){%>
      $('#elfinder').elfinder({
        url : '../../servlets/elfinder?vol0=<%=id%>&shared=true',
        places : "",
          uiOptions : {
              contextmenu : {
                  navbar : ['open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
                  cwd    : ['reload', 'back', '|', 'escupload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'escinfo'],
                  files  : ['getfile', '|','open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info', 'escprovenance']
              },
              toolbar : [
                  ['back', 'forward'],
                  ['netmount'],
                  ['mkdir', 'mkfile', 'escupload'],
                  ['open',  'getfile'],
                  ['escinfo', 'escprovenance'], //SMW add 'escprovenance'
                  ['escquickview', 'acl', 'escarchive', 'escrestore'],
                  ['copy', 'cut', 'paste'],
                  ['rm'],
                  ['rename', 'edit'],
                  ['extract', 'archive'],
                  ['view', 'sort']
              ]}
      });

  <%} else {%>
      $('#elfinder').elfinder({
        url : '../../servlets/elfinder',
        places : "",
          uiOptions : {
              contextmenu : {
                  navbar : ['open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'info'],
                  cwd    : ['reload', 'back', '|', 'escupload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'escinfo'],
                  files  : ['getfile', '|','open', '|', 'copy', 'cut', 'paste', 'duplicate', '|', 'rm', '|', 'edit', 'rename', '|', 'archive', 'extract', '|', 'info', 'escprovenance']
              },
              toolbar : [
                  ['back', 'forward'],
                  ['netmount'],
                  ['mkdir', 'mkfile', 'escupload'],
                  ['open',  'getfile'],
                  ['escinfo', 'escprovenance'], //SMW add 'escprovenance'
                  ['escquickview', 'acl', 'escarchive', 'escrestore'],
                  ['copy', 'cut', 'paste'],
                  ['rm'],
                  ['rename', 'edit'],
                  ['extract', 'archive'],
                  ['view', 'sort']
              ]}
      });

  <%}%>

  var editorChooser = new ViewerChooser();
  editorChooser.init("editorchooser");
  var quickView = new QuickView();
  quickView.init("quickviewdiv");

  jQuery.event.add(window, "load", resizeFinder);
  jQuery.event.add(window, "resize", resizeFinder);

});

//function to resize the file browser when the window size changes
function resizeFinder() {
    var h = $(window).height();
    if (h < 600) {
        h = 600;
    }

    var elfinderHeight = h - 300;
    var workzoneHeight = elfinderHeight - 84;
    var navbarHeight = elfinderHeight - 60;

    $(".elfinder").css('height', elfinderHeight);
    $(".elfinder-workzone").css('height', workzoneHeight);
    $(".elfinder-cwd-wrapper").css('height', workzoneHeight);
    $(".elfinder-navbar").css('height', navbarHeight);

}

</script>


<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
