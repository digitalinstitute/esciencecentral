<%--
  Lists all of the workflows that a user owns or can view
  Author: Hugo
--%>

<%@ page import="java.util.*" %>
<%@ page import="com.connexience.server.*" %>
<%@ page import="com.connexience.server.ejb.util.*" %>
<%@ page import="com.connexience.server.util.*" %>
<%@ page import="com.connexience.server.model.security.*" %>
<%@ page import="com.connexience.server.model.social.profile.*"%>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ page import="java.text.*"%>
        <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

        <%
            String id = request.getParameter("id");
            if(id==null){
                id = ticket.getUserId();
            }
            UserProfile pageProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, id);
            User pageUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, id);
            if(pageProfile==null){
                response.sendRedirect("../../pages/front/index.jsp");
            }
            
            boolean writable = false;
            editing = false;
            if(pageUser.getId().equals(ticket.getUserId())){
                writable = true;
                String action = request.getParameter("action");
                if(action!=null && action.equals("edit")){
                    editing = true;
                }
            }
            String type = ImageData.LARGE_PROFILE;
            
            if(pageProfile.getWebsite()==null){
                pageProfile.setWebsite("");
            }
            
            if(pageProfile.getEmailAddress()==null){
                pageProfile.setEmailAddress("");
            }

        %>
        <%if(editing){%>
            <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/image/jquery.Jcrop.css">
            <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">

            <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">

            <script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
            <script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
            <script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>
            <script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
            <script type="text/javascript" src="../../scripts/image/jquery.Jcrop.min.js"></script>
            <script type="text/javascript" src="../../scripts/image/ImageCropperDialog.js"></script>
            <script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

        <%}%>
        <script type="text/javascript">
            <%if(editing){%>
                
                var imageDialog = new ImageCropperDialog();
                
                function showImageDialog(){
                    //imageDialog.show();

                    var uploadURL = '${pageContext.request.contextPath}/rest/users/<%=id%>/profilePicture/';

                        buildFileUploader(uploadURL);

                        $('#image-upload').modal('show');
                }         

                function imageOkCallback(){
                    var img = document.getElementById("profilepicture");
                    img.setAttribute("src", "../../servlets/image?soid=<%=pageProfile.getId()%>&type=<%=type%>&ts=" + new Date().getTime());
                }      
                
                function cancelEdit(event){

                    event.preventDefault();

                    document.location = "../../pages/profile/profile.jsp?id=<%=pageUser.getId()%>";
                }
                
                function saveChanges(){
                    var callback = function(){
                        document.location = "../../pages/profile/profile.jsp?id=<%=pageUser.getId()%>";
                    }
                    
                    var profileText = tinyMCE.get('editorarea').getContent();
                    var callData = {
                        profileId: "<%=pageProfile.getId()%>",
                        userId: "<%=pageUser.getId()%>",
                        profileText: profileText,
                        firstname: document.getElementById("firstname").value,
                        surname: document.getElementById("lastname").value,
                        email: document.getElementById("profileemail").value,
                        website: document.getElementById("profileweb").value
                    }
                    
                    jsonCall(callData, "../../servlets/profile?method=saveProfile", callback);
                }
              
            <%}%>
            $(document).ready(function()
            {
                clearTabs();
                <%if(editing){%>
                     imageDialog.init("imagedialog"); 
                     imageDialog.okCallback = imageOkCallback;
                     
                    tinyMCE.init({
                            // General options
                            mode : "none",
                            theme : "advanced",
                            plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
                            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,insertdate,inserttime,|,forecolor,backcolor",

                            theme_advanced_toolbar_location : "top",
                            theme_advanced_toolbar_align : "left",
                            theme_advanced_statusbar_location : "bottom"
                    });                    

                    // Add to text area
                    tinyMCE.execCommand("mceAddControl", true, "editorarea");                     
                <%}%>

            });
            

        </script>
            <!-- Caption Line -->


            <div class="row-fluid">
            <%-- Main Content Left Hand Side--%>
            <div class="span9">
                <%if(editing){%>
                    <div id="imagedialog"></div>
                <%}%>
                <div class="profileHeader clearfix">
                    <div class="profileHeaderImg"><img class="img-polaroid img-circle" src="../../servlets/image?soid=<%=pageProfile.getId()%>&type=<%=type%>" id="profilepicture" width="200" height="200"/></div>
                    <%if(editing){%>
                    <label id="profile-firstname">First Name</label>
                    <input type="text" placeholder="First Name" id="firstname" value="<%=pageUser.getFirstName()%>">
                    <label id="profile-lastname">Last Name</label>
                    <input type="text" placeholder="Last Name" id="lastname" value="<%=pageUser.getSurname()%>">
                    <label id="profile-email">E-Mail</label>
                    <input type="text" placeholder="E-Mail" id="profileemail" value="<%=pageProfile.getEmailAddress()%>">
                    <label id="profile-website">Website</label>
                    <input type="text" placeholder="Website" id="profileweb" value="<%=pageProfile.getWebsite()%>">
                    <label id="profile-bio">Bio</label>
                    <TextArea id="editorarea" style="height:500px;width:100%;">
                        <%=pageProfile.getText()%>
                    </TextArea>
                   
                    <%} else {%>

                    <h2><%=pageUser.getDisplayName()%></h2>
                    <address id="profile-email">
                        <strong>Email</strong><br>
                        <a href="mailto:<%=pageProfile.getEmailAddress()%>"><%=pageProfile.getEmailAddress()%></a>
                    </address>
                    <address id="profile-website">
                        <strong>Website</strong><br>
                        <a href="<%=pageProfile.getWebsite()%>"><%=pageProfile.getWebsite()%></a>
                    </address>
                    <address id="profile-bio">
                        <strong>Bio</strong><br>
                        <p><%=pageProfile.getText()%></p>
                    </address>

                    <%}%>

                </div>

            </div>
            


            <!-- Right Hand Side -->
            <div class="span3">
                <ul class="unstyled sidebar-menu">
                    <li>
                        <h4><a href="profile.jsp?id=<%=id%>"><i class="icomoon-user"></i>Profile</a></h4>
                    </li>
                    <li>
                        <h4><a href="../../data/<%=id%>/info"><i class="icomoon-notebook"></i>Metadata</a></h4>
                    </li>
                <%if(!editing){%>
                    <li>
                        <h4><a href="profilefiles.jsp?id=<%=id%>"><i class="icomoon-copy"></i>Shared Files</a></h4>
                    </li>
                <%}%>
                <%if(writable){%>
                    <%if(editing){%>
                        <li>
                            <h4><a href="#" onclick="showImageDialog()"><i class="icomoon-image"></i>Change Picture</a></h4>
                        </li>
                        <li>
                            <h4><a href="#" onclick="cancelEdit(event)"><i class="icomoon-close"></i>Cancel Edit</a></h4>
                        </li>
                        <li>
                            <h4><a href="#" onclick="saveChanges()"><i class="icomoon-checkmark"></i>Save Changes</a></h4>
                        </li>
                    <%} else {%>
                        <li>
                            <h4><a href="profile.jsp?id=<%=id%>&action=edit"><i class="icomoon-pencil"></i>Edit Profile</a></h4>
                        </li>
                <%}%>
                    <li>
                        <h4><a href="settings.jsp?id=<%=id%>"><i class="icomoon-cogs"></i>Settings</a></h4>
                    </li>
                <%}%>
                </ul>
            </div>

            </div>

<%@include file="image-upload.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>



