<%--
  Demo Page
  Author: Simon
--%>

    <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../../scripts/messages/friendPanel.css">
    <script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
    <script type="text/javascript" src="../../scripts/messages/friendPanel.js"></script>
    <script type="text/javascript" src="../../scripts/messages/messages.js"></script>
    <script type="text/javascript" src="../../scripts/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>

    <script type="text/javascript">
        $(document).ready(function()
        {
            var messages = new Messages();
            messages.init('messageList', '<%=user.getId()%>');
            messages.initEditor("editor");

            $.ajax({
                url: rewriteAjaxUrl('../../people/<%=user.getId()%>/friends'),
                dataType: 'json',
                xhrFields: {
                    withCredentials: true
                },
                success: function(data) {
                    if (data !== null) {

                        var recipientTags = $('#recipients').tags({
                            bootstrapVersion: "2",
                            suggestions:_.map(data, 'label'),
                            popoverData:_.map(data, 'id'),
                            popovers: false,
                            popoverTrigger: '',
                            caseInsensitive: true,
                            tagClass: 'btn',
                            promptText: 'Enter user\'s name...'
                        });

                        $("#sendMessageButton").click(function()
                        {
                            var recipients = recipientTags.getTagsWithContent();

                            for (var i = 0; i < recipients.length; i++)
                            {
                                messages.addRecipient(recipients[i].content);
                            }

                            messages.sendMessage("editor", $("#messageSubject").val(), "");
                        });

                    }
                }
            });
        });

    </script>

    <!-- Caption Line -->
    <h2>Send Message</h2>
    <div class="row-fluid">
    <%-- Main Content Left Hand Side--%>
    <div class="span9">

        <div id="messageList" class="list">
        </div>
        <div class="dialog">
            <form method="post" action="" id="createMessageForm">

            <label for="recipients">Recipients</label>
            <div id="recipients" class="tag-list"></div>

            <label for="messageSubject">Subject</label>
            <input type="text" name="messageSubject" id="messageSubject" class="input-block-level" value="" placeholder="Subject"/>

            <label for="editor">Message</label>
            <textarea id="editor" name="editor"></textarea>
            <br /><br />
            <button id="sendMessageButton" class="btn btn-primary">Send Message</button>&nbsp;&nbsp;
            <a class="btn btn-default" href="../../pages/messages/inbox.jsp">Cancel</a>
            </form>
        </div>

    </div>

    <!-- Right Hand Side -->
    <div class="span3">
        <ul class="unstyled sidebar-menu">
            <li>
                <h4><a href="inbox.jsp"><i class="icomoon-list"></i>Inbox</a></h4>
            </li>
        </ul>
    </div>

    </div>

    <%@include file="../../WEB-INF/jspf/page/footer.jspf" %>