<%@page import="com.connexience.server.ejb.util.EJBLocator"%>
<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ page import="java.util.*"%>
  <%@include file="../../WEB-INF/jspf/page/header.jspf"%>
  <%
    try{
        Collection friends = EJBLocator.lookupLinkBean().getLinkedSourceObjects(ticket, user, User.class, START, PAGE_SIZE);
        NUM_OBJECTS = EJBLocator.lookupLinkBean().getNumberOfLinkedSourceObjects(ticket, user, User.class);
  %>

  <!-- Caption Line -->
  <h2>My Connections</h2>

  <%-- An example List of items--%>
<div class="row-fluid">
  <div class="span9">
      <%if(friends.size()>0){%>
    <div class="list">

      <%
        User u;
        Iterator i = friends.iterator();
        while(i.hasNext())
        {
          u = (User)i.next();
          //if there is no photo set, use a default one
          UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, u.getId());
//          String photoURL = "../servlets/image?soid=" + up.getId() + "&type=" + ImageData.SMALL_PROFILE;

          String noHTMLString = up.getText();
          if (noHTMLString == null || noHTMLString.equals(""))
          {
            noHTMLString = u.getDisplayName() + " has no profile information yet.";
          }
          else
          {
            noHTMLString = noHTMLString.replaceAll("<.*?>", "");
            if (noHTMLString.length() > 240)
            {
              noHTMLString = noHTMLString.substring(0, 150) + "... <a href=\"../../pages/profile/profile.jsp?id=" + u.getId() + "\">[Read More]</a>";
            }
          }
      %>

      <div class="listElement">
          <img src="../../servlets/image?soid=<%=u.getId()%>&type=<%="small profile"%>" alt="profile picture" class="listItemImg"/>

          <%--<div class="listTopText"><a href="../../pages/profile/profile.jsp?id=<%=u.getId()%>"></a>--%>
        </div>
        <div class="listMainText">
          <h2><%=u.getDisplayName()%></h2>
          <p>
            <%=noHTMLString%>
          </p>
        </div>
      </div>

      <div class="hr">&nbsp;</div>

      <%
        }
      %>
      <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>

    </div>
      <%} else {%>
      <p>You have no connections yet</p>
      <%}%>
  </div>


  <!-- Right Hand Side -->
        <div class="span3">
            <ul class="unstyled sidebar-menu">
                <li>
                    <h4><a href="people.jsp"><i class="icomoon-users"></i>My Connections</a></h4>
                </li>
                <li>
                    <h4><a href="peoplesearch.jsp"><i class="icomoon-search"></i>Search People</a></h4>
                </li>
                <li>
                    <h4><a href="peoplesearch.jsp?browse=true"><i class="icomoon-users2"></i>Browse All People</a></h4>
                </li>
            </ul>
        </div>

</div>

  <%
    }
    catch(ConnexienceException e)
    {
      e.printStackTrace();
      response.sendRedirect("../../pages/error.jsp");
      return;
    }
  %>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
