<%@page import="com.connexience.server.ejb.util.EJBLocator"%>
<%@ page import="com.connexience.server.model.security.Group" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.ejb.directory.GroupDirectoryRemote" %>
<%@ page import="com.connexience.server.model.image.ImageData" %>
<%@ page import="java.util.*"%>

        <%@ include file="../../WEB-INF/jspf/page/header.jspf" %>

            <%


                try {
                    List users;
                    if (request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()) {
                        users = EJBLocator.lookupUserDirectoryBean().searchForUsers(ticket, request.getParameter("searchtext").trim(), START, PAGE_SIZE);
                    }else if(request.getParameter("browse") != null)
                    {
                      users = EJBLocator.lookupUserDirectoryBean().searchForUsers(ticket, "%", START, PAGE_SIZE);
                    }
                    else {
                        users = new ArrayList();
                    }
                    
                    // List of existing connections
                    Collection friends = EJBLocator.lookupLinkBean().getLinkedSourceObjects(ticket, user, User.class);
            %>

            <!-- Caption Line -->
            <h2>People Search</h2>

<div class="row-fluid">
            <%-- An example List of items--%>
            <div class="span9">
                <div  class="dialog">
                    <form action="peoplesearch.jsp">
                        <div class="input-append">
                            <input id="searchtext" name="searchtext" type="text" placeholder="Search">
                            <div name="searchbutton" id="searchbutton" class="btn-group" type="submit">
                                <button class="btn">
                                    <i class="icon-search"></i>Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>    
                <div>
                    <%if(request.getParameter("searchtext") != null && !request.getParameter("searchtext").trim().isEmpty()){%>
                        <h5>Search results:</h5>
                    <%}%>
                    <ul id="people-list" class="unstyled">

                        <%
                            User u;
                            Iterator i = users.iterator();
                            while (i.hasNext()) {
                                u = (User) i.next();
                                //if there is no photo set, use a default one
                                UserProfile up = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, u.getId());
                  //          String photoURL = "../servlets/image?soid=" + up.getId() + "&type=" + ImageData.SMALL_PROFILE;

                                String noHTMLString = up.getText();
                                if (noHTMLString == null || noHTMLString.equals("")) {
                                    noHTMLString = u.getDisplayName() + " has no profile information yet.";
                                } else {
                                    noHTMLString = noHTMLString.replaceAll("<.*?>", "");
                                    if (noHTMLString.length() > 240) {
                                        noHTMLString = noHTMLString.substring(0, 200) + "... <a href=\"../../pages/profile/profile.jsp?id=" + u.getId() + "\">[Read More]</a>";
                                    }
                                }
                        %>

                        <li class="clearfix">
                            <a class="profile-picture" href="../../pages/profile/profile.jsp?id=<%=u.getId()%>">
                                <img src="../../servlets/image?soid=<%=u.getId()%>&type=<%="small profile"%>" alt="profile picture" class="img-polaroid img-circle"/>
                            </a>
                            <div class="user-bio">
                                <h4><%=u.getDisplayName()%> </h4>
                                <p>
                                    <%=noHTMLString%>
                                </p>
                            </div>
                            <ul class="inline">
                                <li>
                            <%if(!friends.contains(u)){%>
                                <h6><a style="cursor: pointer;" onclick="makeConnection('<%=u.getId()%>')">Connect</a></h6>
                            <%}%>
                                </li>
                                <li>
                                    <h6><a href="../../pages/profile/profile.jsp?id=<%=u.getId()%>">View Profile</a></h6>
                                </li>
                            </ul>
                        </li>

                        <%
                            }
                        %>

                      <%@include file="../../WEB-INF/jspf/page/paginate.jspf"%>
                    </ul>
                </div>
            </div>

            <!-- Right Hand Side -->
            <div class="span3">
                <ul class="unstyled sidebar-menu">
                    <li>
                        <h4><a href="people.jsp"><i class="icomoon-users"></i>My Connections</a></h4>
                    </li>
                    <li>
                        <h4><a href="peoplesearch.jsp"><i class="icomoon-search"></i>Search People</a></h4>
                    </li>
                    <li>
                        <h4><a href="peoplesearch.jsp?browse=true"><i class="icomoon-users2"></i>Browse All People</a></h4>
                    </li>
                </ul>
            </div>

</div>

<%
                } catch (ConnexienceException e) {
                    e.printStackTrace();
                    response.sendRedirect("../../pages/error.jsp");
                    return;
                }
            %>
        </div>

        <script type="text/javascript">
            function makeConnection(userId){
                var url = "../../servlets/profile?method=requestConnection";
                var callData = {userId: userId};
                var cb = function(){
                    $.jGrowl("Posted connection request");
                }
                jsonCall(callData, url, cb);
            }
        </script>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

