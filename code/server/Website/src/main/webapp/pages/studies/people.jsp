
<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatPerson"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatStudy"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatDevice"%>
<%@page import="java.util.List"%>
<%--
  Index Page
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }

    
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    String id = idStr;
    
    FlatStudy study = null;
    if (id != null) {
        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(id));
        if (p instanceof FlatStudy) {
            study = (FlatStudy) p;
        } else {
            response.sendRedirect("studies.jsp");
        }
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <h2><%=study.getName()%>: People</h2>
        <h4>Managing: <%=EJBLocator.lookupFlatStudyBean().getObjectCount(ticket, study.getId(), FlatPerson.class)%> people</h4>
        <p><a onclick="registerList(false)">Register new people</a></p>
        <p><a onclick="registerList(true)">Register new people and hash IDs</a></p>
        <table id="devices" class="table table-bordered" width="100%">
            <thead>
                <tr>
                    <th style="width:30%;">Created</th>
                    <th>ID</th>
                    <th style="width: 15%;">Action</th>
                </tr>
            </thead>
        </table>      
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<div id="registerDiv" class="dialog"></div>
<div id="multiRegisterDiv" class="dialog"></div>
<div id="confirmDiv" class="dialog"></div>
<div id="studyListDiv" class="dialog"></div>
<div id="propertiesListDiv" class="dialog"></div>
<script type="text/javascript">
    var registrationDialog = new InputDialog();
    var confirmDialog = new ConfirmDialog();
    var studiesDialog = new MultiValueSelectDialog();
    var registerListDialog = new TextEditorDialog();
    var propertiesDialog = new AttributesDialog();
    
    var table;
    
    var studyId = <%=study.getId()%>;
    $(document).ready(function(){
        registerListDialog.title = "Please enter a list of User IDs";
        registerListDialog.width = 500;
        registerListDialog.height = 500;
        registerListDialog.init("multiRegisterDiv");
        registerListDialog.okCallback = function(text){
            var arrayOfLines = text.match(/[^\r\n]+/g); 
            var callData = {
                hashIds: this.hashIds,
                userIds: arrayOfLines,
                studyId: studyId               
            };

            jsonCall(callData, "../../servlets/studyuserregistration?method=registerUserList", function(o){
                table.fnDraw(true);
            });
        };

        registrationDialog.title = "Please enter a User ID";        
        registrationDialog.width = 500;
        registrationDialog.init("registerDiv");    
        registrationDialog.okCallback = function(userId){
            var callData = {
                userId: userId,
                studyId: studyId
            };
            jsonCall(callData, "../../servlets/studyuserregistration?method=registerUser", function(o){
                table.fnDraw(true);
            });
        };
        
        confirmDialog.init("confirmDiv");
        studiesDialog.title = "Select a project";
        studiesDialog.init("studyListDiv");
        
        table = $('#devices').dataTable( {
            "bjQueryUI": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../../servlets/flatstudypager?cn=<%=FlatPerson.class.getName()%>&sid=<%=study.getId()%>&showPropertiesButton=true&showDataButton=true&showDeleteButton=true&showMoveButton=true"
        });

        propertiesDialog.title = "Additional Properties";
        propertiesDialog.width = 500;
        propertiesDialog.height = 500;
        propertiesDialog.init("propertiesListDiv");
    });

    function showProperties(object){
        console.log(object);
        propertiesDialog.show(object);
    }

    function showFolder(id){
        document.location = "data.jsp?id=<%=study.getId()%>&folderid=" + id;
    }
    
    function moveObject(id, externalId){
        // Get the studies
        studiesDialog.okCallback = function(value, index){
            var targetStudy = studiesDialog.studyList[index];
            jsonCall({
                    userId: id,
                    targetStudyId: targetStudy.id
                }, "../../servlets/studyuserregistration?method=moveUser", function(o){
                table.fnDraw(true);
            });
        };
        
        jsonCall({}, "../../servlets/studyuserregistration?method=listFlatStudies", function(o){
            studiesDialog.studyList = o.studies;
            studiesDialog.show(o.studyNames);
        });
    }
    
    function deleteDevice(id, externalId){
        confirmDialog.yesCallback = function(){
            var callData = {
                userId: id
            };
            jsonCall(callData, "../../servlets/studyuserregistration?method=deleteUser", function(o){
               table.fnDraw(true);
            });
        };
        confirmDialog.show("Are you sure you want to delete: " + externalId);
    }
    
    function registerList(hashIds){
        registerListDialog.hashIds = hashIds;
        registerListDialog.show("");
    }
    
    function registerUser(){
        registrationDialog.show("");
    }
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>