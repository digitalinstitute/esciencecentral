
<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatPerson"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatStudy"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatDevice"%>
<%@page import="java.util.List"%>
<%--
  Index Page
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    String id = idStr;
    
    FlatStudy study = null;
    if (id != null) {
        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(id));
        if (p instanceof FlatStudy) {
            study = (FlatStudy) p;
        } else {
            response.sendRedirect("studies.jsp");
        }
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <h2><%=study.getName()%>: Devices</h2>
        <h4>Managing: <%=EJBLocator.lookupFlatStudyBean().getObjectCount(ticket, study.getId(), FlatDevice.class)%> devices</h4>


        <table id="devices" class="table table-bordered" width="100%">
            <thead>
                <tr>
                    <th>Created</th>
                    <th>ID</th>
                </tr>
            </thead>
        </table>      
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        
        $('#devices').dataTable( {
            "bjQueryUI": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../../servlets/flatstudypager?cn=<%=FlatDevice.class.getName()%>&sid=<%=study.getId()%>"

            
        });
    });

</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
