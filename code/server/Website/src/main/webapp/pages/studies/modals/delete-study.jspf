<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<div id="remove-study-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="remove-study-modal-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="remove-study-modal-label">Are you sure?</h3>
    </div>
    <form class="form-inline">
        <div class="modal-body">
            <label for="reAuthPassword"> Re-enter your password to delete this
                study</label>
            <input style="display: block; margin-bottom: 10px" id="reAuthPassword"
                   type="password">

            <label class="checkbox">
                <input type="checkbox" id="deleteStudyData" name="deleteStudyData"> Remove all study files and folders
            </label>
        </div>
        <div class="modal-footer">
            <button type="submit" id="remove-study" class="btn btn-danger">Confirm</button>
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        $('#remove-study-modal').on("click", "#remove-study", function (event) {

            event.preventDefault();

            var removeData = $('#deleteStudyData').is(':checked');
            var reAuthPassword = $('#reAuthPassword').val();

            $.ajax({
                type: 'POST',
                url: '/website-api/rest/study/<%=menu_study.getId()%>/remove',
                data: 'removeData=' + removeData + '&reAuthPassword=' + reAuthPassword,
                success: function (data) {

                    $("#subject-group-modal").modal("hide");

                    window.location = "/beta/pages/studies/studies.jsp?searchtext=&inactive=false";
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(thrownError);
                    if (xhr.status === 403) {
                        $("#remove-study-modal .modal-body").prepend("<div id='reAuthPasswordError' style='color: red'>Incorrect Password</div>");
                    }
                }
            });
        });
    });

</script>