
<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatPerson"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatStudy"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatDevice"%>

<%--
  Index Page
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
  <%

    if(publicUser)
    {
      response.sendRedirect("../../pages/front/welcome.jsp");
    }
    
    FlatStudy s = new FlatStudy();
    s.setName("Test Flat Study");
    s.setExternalId("FS001");
    s.setStartDate(new Date());
    s.setEndDate(new Date());
    s = EJBLocator.lookupFlatStudyBean().saveFlatStudy(ticket, s);
    
    FlatDevice d = EJBLocator.lookupFlatStudyBean().createFlatDevice(ticket, s.getId(), "FDEV1");
    d.getAdditionalProperties().put("email", "user@company.com");
    FlatDevice updated = EJBLocator.lookupFlatStudyBean().saveFlatDevice(ticket, d);
    
    FlatPerson p = EJBLocator.lookupFlatStudyBean().createFlatPerson(ticket, s.getId(), "FPERS1");
    FlatPerson updatedPerson = EJBLocator.lookupFlatStudyBean().saveFlatPerson(ticket, p);
    
    FlatGateway  g = EJBLocator.lookupFlatStudyBean().createFlatGateway(ticket, s.getId(), "FGATE1");
    FlatGateway updatedGateway = EJBLocator.lookupFlatStudyBean().saveFlatGateway(ticket, g);
%>

<h1><%=s.getName() + ": " + s.getId()%></h1>
<h1>Device ID: <%=updated.getId()%></h1>
<h1>Person ID: <%=updatedPerson.getId()%></h1>
<h1>Gateway ID: <%=updatedGateway.getId()%></h1>
<%
    //EJBLocator.lookupFlatStudyBean().removeFlatDevice(ticket, updated.getId());
%>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>
