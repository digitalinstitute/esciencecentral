<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<div id="phase-modal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="create-phase-label" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="phase-modal-label">Create Phase</h3>
    </div>
    <div id="edit-phase" style="display: none;">
        <form id="create-phase-form" class="form-inline">
            <div class="modal-body">
                <div class="modal-content">
                    <input type="hidden" id="phaseId" value="">
                    <input type="hidden" id="phaseStudy" value="<%=study.getId()%>">
                    <div class="control-group">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input type="text" name="phaseName" id="phaseName" placeholder="Phase Name">
                        </div>
                    </div>
                </div>
                <div class="modal-message"></div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="submit-phase" class="btn">Create Phase</button>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </form>
    </div>
    <div id="delete-phase" style="display:none;">
        <form class="form-inline">
            <div class="modal-body">
                <p>Are you sure you wish to delete this phase?</p>
                <label class="checkbox">
                    <input type="checkbox" id="deleteData" name="deleteData"> Remove all files and folders
                </label>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="deletePhaseId" value="">
                <button type="submit" id="submit-delete-phase" class="btn btn-danger">OK</button>
                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
            </div>
        </form>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {

        var form = $("#create-phase-form");
        form.validate({
            rules: {
                phaseName: {required: true, maxlength: 255}
            }
        });

        $('#phase-modal').on('hidden', function () {
            $('.modal-message').css('display', 'none');
            $('.modal-content').css('display', 'block');
            $( "#create-phase-form")[0].reset();
        });

        $('#phase-modal').on("click", "#submit-phase", function (event) {

            event.preventDefault();

            if(form.valid())
            {
                var phase = new Object();
                phase.name = $("#phaseName").val();
                phase.studyId = <%=study.getId()%>;
                phase.subjectGroups = null;

                console.log($("#phaseId").val());

                if($("#phaseId").val() !== ""){
                    phase.id = $("#phaseId").val();
                }

                var encodedPhase = JSON.stringify(phase);

                $.ajax({
                    type: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/study/<%=study.getId()%>/phases/',
                    data: encodedPhase,
                    success: function (study) {
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);

                        var errorMessage = '<h2 class="text-error text-center">Error!</h2>' +
                                '<p class="text-center">' + thrownError + '</p>';

                        $('.modal-message').html(errorMessage);
                        $('.modal-content').css('display', 'none');
                        $('.modal-message').css('display', 'block');
                    }
                });
            }
            else
            {
                if($('#validation-message').length == 0)
                {
                    if($('.validation-message').length == 0)
                    {
                        var message = "Correct these errors to create this study.";
                        var type = "error";
                        var target = $("#study");
                        var cssClass = "validation-message";

                        flashMessage(message, type, target, cssClass)
                    }
                }
            }
        });

        $('#phase-modal').on("click", "#submit-delete-phase", function (event) {

            event.preventDefault();

            var deleteData = $('#delete-phase #deleteData').is(':checked');

            if(form.valid())
            {
                $.ajax({
                    type: 'DELETE',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: '/website-api/rest/study/<%=study.getId()%>/phases/' + $("#deletePhaseId").val(),
                    data: "deleteData=" + deleteData,
                    success: function (study) {
                        location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(thrownError);

                        var errorMessage = '<h2 class="text-error text-center">Error!</h2>' +
                                '<p class="text-center">' + thrownError + '</p>';

                        $('.modal-message').html(errorMessage);
                        $('.modal-content').css('display', 'none');
                        $('.modal-message').css('display', 'block');
                    }
                });
            }
            else
            {
                if($('#validation-message').length == 0)
                {
                    if($('.validation-message').length == 0)
                    {
                        var message = "Correct these errors to create this study.";
                        var type = "error";
                        var target = $("#study");
                        var cssClass = "validation-message";

                        flashMessage(message, type, target, cssClass)
                    }
                }
            }
        });
    });

</script>