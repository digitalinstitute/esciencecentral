<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.web.APIUtils"%>
<%--
  Index Page
  Author: Hugo
--%>

<style>
    .queryToolbar {
    padding-top: 10px;
    padding-left: 10px;        
    }
</style>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<script type="text/javascript" src="../../scripts/moment/moment.min.js"></script>
<link rel="stylesheet" href="gateway.css"/>

<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    
    String id = idStr;
    
    FlatStudy study = null;
    if (id != null) {
        study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, Integer.valueOf(id));
    } else {
        response.sendRedirect("studies.jsp");
    }
    
    boolean storePresent = EJBLocator.lookupEventStoreBean().eventStorePresent(ticket, study.getExternalId());

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid">
            <h2>Event storage for: <%=study.getName()%></h2>        
        </div>

        <div class="row-fluid">
            <%if(storePresent){%>
            <h4>Data store is present: <button onclick="removeStorage();">Remove</button></h4>
            <%} else {%>
            <h4>Data store not initialised yet: <button onclick="createStorage();">Create</button></h4>
            <%}%>
        </div>

    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    
    $(document).ready(function(){
        $('button').button();
    });
    
    function removeStorage() {
        var callData = {studyCode: '<%=study.getExternalId()%>'};
        jsonCall(callData, "../../servlets/admin?method=resetEventStore", function(){
            location.reload();
        });
    }
    
    function createStorage(){
        var callData = {studyCode: '<%=study.getExternalId()%>'};
        jsonCall(callData, "../../servlets/admin?method=createEventStore", function(){
            location.reload();
        });        
    }
    
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>