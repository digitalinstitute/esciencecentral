<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.connexience.server.model.document.DocumentRecord" %>
<%@ include file="../../WEB-INF/jspf/page/header.jspf" %>
<%

String externalId = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(request.getParameter("studyid"))).getExternalId();

%>
<script type="text/javascript">
    var apisuffix = "";
    var studyId = <%=request.getParameter("studyid")%>;
    var externalId = "<%=externalId%>";
</script>
<link rel="stylesheet" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css"/>


<%
    Project project = null;


    try {
        String idStr;
        idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("studyid"));
        if (idStr == null || idStr.isEmpty()) {
            response.sendRedirect("studies.jsp");
        }

        Integer studyId = Integer.parseInt(idStr);

        idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
        if (idStr == null || idStr.isEmpty()) {
            response.sendRedirect("studies.jsp");
        }

        String id = idStr;
        project = EJBLocator.lookupProjectsBean().getProject(ticket, studyId);

        menu_study = project;

        Folder dashboardFolder = EJBLocator.lookupStorageBean().getFolder(ticket, id);
        Folder topLevelFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, project.getDataFolderId(), "Dashboards");
        if (dashboardFolder == null && topLevelFolder!=null) {
%>
<div class="row-fluid">
    <div class="span9">
        <p>This study has no dashboard defined.</p>
    </div>
    <div class="span3">
        <%@include file="menu.jspf" %>
    </div>
</div>
<%
} else {

    //load common files
    List<Folder> parentFolders = EJBLocator.lookupStorageBean().getChildFolders(ticket, topLevelFolder.getId());
    for (Folder folder : parentFolders) {
        if (folder.getName().equalsIgnoreCase("common")) {
            List<DocumentRecord> dashboardDocs = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, folder.getId());
            Collections.sort(dashboardDocs, new Comparator<DocumentRecord>() {
                public int compare(DocumentRecord x, DocumentRecord y) {
                    return x.getName().toLowerCase().compareTo(y.getName().toLowerCase());
                }
            });
            for (DocumentRecord dr : dashboardDocs) {
                if (dr.getName().endsWith(".js")) {
%>
<script src="../../servlets/download/<%=dr.getName()%>?documentid=<%=dr.getId()%>&forcemime=application/javascript"></script>
<%
} else if (dr.getName().endsWith(".css")) {
%>
<link rel="stylesheet" type="text/css"
      href="../../servlets/download/<%=dr.getName()%>?documentid=<%=dr.getId()%>&forcemime=text/css"/>
<%
                } else {
                    System.err.println("Ignoring common dashboard file: " + dr.getName() + " in study: " + project.getName());
                }
            }

        }
    }

    //load custom dashboard files
    List<DocumentRecord> dashboardDocs = EJBLocator.lookupStorageBean().getFolderDocumentRecords(ticket, dashboardFolder.getId());
    Collections.sort(dashboardDocs, new Comparator<DocumentRecord>() {
        public int compare(DocumentRecord x, DocumentRecord y) {
            return x.getName().toLowerCase().compareTo(y.getName().toLowerCase());
        }
    });
    for (DocumentRecord dr : dashboardDocs) {
        if (dr.getName().endsWith(".js")) {
%>

<script src="../../servlets/download/<%=dr.getName()%>?documentid=<%=dr.getId()%>&forcemime=application/javascript"></script>
<%
} else if (dr.getName().endsWith(".css")) {
%>
<link rel="stylesheet" type="text/css"
      href="../../servlets/download/<%=dr.getName()%>?documentid=<%=dr.getId()%>&forcemime=text/css"/>
<%
        } else {
            System.err.println("Ignoring dashboard file: " + dr.getName() + " in study: " + project.getName());
        }
    }
%>
<div class="row-fluid">
    <div class="span9">
        <h1 class="page-header"><%=dashboardFolder.getName()%>
        </h1>
        <div id="study-dashboard">
            <div id="study-dashboard-name"></div>
            <div id="study-dashboard-control"></div>
            <div id="study-dashboard-data"></div>
        </div>
    </div>
    <div class="span3">
        <%@include file="menu.jspf" %>
    </div>
</div>

<%
    }
%>

<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>

<%
    } catch (ConnexienceException e) {
        e.printStackTrace();
        response.sendRedirect("../../pages/error.jsp");
        return;
    }
%>
