<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.web.APIUtils"%>
<%--
  Index Page
  Author: Hugo
--%>

<style>
    .queryToolbar {
    padding-top: 10px;
    padding-left: 10px;        
    }
</style>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<script type="text/javascript" src="../../scripts/moment/moment.min.js"></script>
<link rel="stylesheet" href="gateway.css"/>

<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    
    String id = idStr;
    
    FlatStudy study = null;
    FlatGateway gateway = null;
    if (id != null) {
        gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, Integer.valueOf(id));
        study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, gateway.getStudyId());
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid">
            <h2>Gateway: <%=gateway.getExternalId()%></h2>        
        </div>

         <%@include file="include/daterange.jspf"%>
         <div class="row-fluid queryToolbar">
             <div class="span2">Data type:</div>
            <div class="span9">
                <select id="eventtypeselector"></select>
            </div>             
         </div>
         
         <div class="row-fluid queryToolbar">
             <div class="span2">Download</div>
             <div class="span5"><button onclick="doDownload();">Download</button><button onclick="purge();">Delete all data</button></div>
         </div>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">

    var eventTypeArray = new Array();
            
    Number.prototype.pad = function(size) {
          var s = String(this);
          while (s.length < (size || 2)) {s = "0" + s;}
          return s;
    }
    
    $(document).ready(function(){
        $('button').button();
        $('#eventtypeselector').selectBox();
           
        asyncJsonGet('../../servlets/eventdownload?datatype=eventtypes&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>', function(o){
            eventTypeArray = o;
            $("#eventtypeselector").selectBox('options', o);
        });
    });
    
    function formatDate(dt){
        return dt.getDate().pad(2) + "-" + (dt.getMonth() + 1).pad(2) + "-" + dt.getUTCFullYear();
    }
    
    function purge(){
        var eventTypeIndex = $("#eventtypeselector").selectBox('value');
        var eventType = eventTypeArray[eventTypeIndex];
        var callData = {
            deviceId: '<%=gateway.getExternalId()%>',
            studyCode: '<%=study.getExternalId()%>',
            eventType: eventType
        };
        jsonCall(callData, "../../servlets/admin?method=dropEvents", function(){
            $.jGrowl("Events cleared");
        }, function(o){
            $.jGrowl("Error clearing events");
        });
    }        
    
    function doDownload(){
        var startDateField = document.getElementById("startDate");
        var sd = moment(startDateField.value, "DD-MM-YYYY").toDate();
        
        var endDateField = document.getElementById("endDate");
        var ed = moment(endDateField.value, "DD-MM-YYYY").toDate();
        
        var eventTypeIndex = $("#eventtypeselector").selectBox('value');
        var eventType = eventTypeArray[eventTypeIndex];
        
        
        var url = '../../servlets/eventdownload/' + eventType + '.csv?eventtype=' + eventType + '&datatype=csv&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>&start=' + sd.getTime() + '&end=' + ed.getTime();
        
        document.location = url;
    }
    
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>