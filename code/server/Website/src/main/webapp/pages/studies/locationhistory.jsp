<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.web.APIUtils"%>
<%--
  Index Page
  Author: Hugo
--%>

<style>
    .queryToolbar {
    padding-top: 10px;
    padding-left: 10px;        
    }
</style>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<script type="text/javascript" src="../../scripts/openlayers/OpenLayers.js"></script>
<script type="text/javascript" src="../../scripts/d3/d3.v3.min.js"></script>
<script type="text/javascript" src="../../scripts/d3/nv.d3.js"></script>
<script type="text/javascript" src="../../scripts/moment/moment.min.js"></script>
<link rel="stylesheet" href="../../scripts/d3/nv.d3.css"/>
<link rel="stylesheet" href="gateway.css"/>

<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    
    String id = idStr;
    
    FlatStudy study = null;
    FlatGateway gateway = null;
    if (id != null) {
        gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, Integer.valueOf(id));
        study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, gateway.getStudyId());
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid">
            <h2>Gateway: <%=gateway.getExternalId()%></h2>        
        </div>
        <input type="button" onclick="showHistory();" value="Refresh"/>
        <%@include file="include/daterange.jspf"%>
        
        <hr>
        <div class="row-fluid">
            <div id="basicMap" style="width:100%; height:500px;"></div>            
        </div>

    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<script type="text/javascript">
    var map = new OpenLayers.Map("basicMap");
    var fromProjection = new OpenLayers.Projection("EPSG:4326");
    var toProjection   = new OpenLayers.Projection("EPSG:900913");
    var markers;
    var vectors;
    var lgpx = null;
    
    Number.prototype.pad = function(size) {
          var s = String(this);
          while (s.length < (size || 2)) {s = "0" + s;}
          return s;
    }
    
    $(document).ready(function(){
        var mapnik = new OpenLayers.Layer.OSM();
        map.addLayer(mapnik);
        
        markers = new OpenLayers.Layer.Markers("Samples");
        map.addLayer(markers);

        vectors = new OpenLayers.Layer.Vector("Track");
        
        map.addLayer(vectors);
        
/*
        lgpx = new OpenLayers.Layer.Vector("Lakeside cycle ride", {
                       strategies: [new OpenLayers.Strategy.Fixed()],
                       protocol: new OpenLayers.Protocol.HTTP({
                               url: "../../servlets/eventdownload/location.gpx?eventtype=Location&datatype=gpx&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>",
                               format: new OpenLayers.Format.GPX()
                       }),
                       style: {strokeColor: "green", strokeWidth: 5, strokeOpacity: 0.5},
                       projection: new OpenLayers.Projection("EPSG:4326")
               });        
        map.addLayer(lgpx);
*/
   
        /*
        asyncJsonGet('../../servlets/eventdownload?eventtype=Environmental&datatype=mostrecent&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>', function(o){
            var data = o.data;
            document.getElementById("Temperature").innerHTML = data.Temperature.toFixed(1) + "C";
            document.getElementById("Humidity").innerHTML = data.Humidity.toFixed(1) + "%";
            document.getElementById("Pressure").innerHTML = data.Pressure.toFixed(1) + "kPa";       
            document.getElementById("readingDate").innerHTML = new Date(o.timestamp).toUTCString();
        });
        
        */
        asyncJsonGet('../../servlets/eventdownload?eventtype=Location&datatype=mostrecent&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>', function(o){
            var data = o.data;
            var position = new OpenLayers.LonLat(data.Longitude,data.Latitude).transform( fromProjection, toProjection);
            var zoom = 15;             
            var m = new OpenLayers.Marker(position);
            map.setCenter(position, zoom );  
            markers.addMarker(m);
            setDefaultDateRange(new Date(o.timestamp));
            showHistory();
        });
       
        
    });
    
    function formatDate(dt){
        return dt.getDate().pad(2) + "-" + (dt.getMonth() + 1).pad(2) + "-" + dt.getUTCFullYear();
    }
    
    function showHistory(){
        var startDateField = document.getElementById("startDate");
        var sd = moment(startDateField.value, "DD-MM-YYYY").toDate();
        
        var endDateField = document.getElementById("endDate");
        var ed = moment(endDateField.value, "DD-MM-YYYY").toDate();
        
        asyncJsonGet('../../servlets/eventdownload?eventtype=Location&datatype=trend&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>&start=' + sd.getTime() + '&end=' + ed.getTime(), function(o){
            var row;
            var m;
            var position;
            var point;
            var points = new Array();
            
            markers.clearMarkers();
            vectors.removeAllFeatures();
            
            for(var i=0;i<o.length;i++){
                row = JSON.parse(o[i]);
                position = new OpenLayers.LonLat(row.data.Longitude, row.data.Latitude).transform(fromProjection, toProjection);
                m = new OpenLayers.Marker(position);
                markers.addMarker(m);
                point = new OpenLayers.Geometry.Point(row.data.Longitude, row.data.Latitude).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject());
                points.push(point);
            }

            if(points.length>0){
                var style = {
                    strokeColor: '#0000ff',
                    strokeOpacity: 0.5,
                    strokeWidth: 5
                };          

                var line = new OpenLayers.Geometry.LineString(points);
                var lineFeature = new OpenLayers.Feature.Vector(line, null, style);
                vectors.addFeatures([lineFeature]);
            }
            vectors.display(true);
            console.log("Fetched");
        });
    }

 
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>