
<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatPerson"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatStudy"%>
<%@page import="com.connexience.server.model.project.flatstudy.FlatDevice"%>
<%@page import="java.util.List"%>
<%--
  Index Page
  Author: Hugo
--%>

<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    
    String id = idStr;
    
    FlatStudy study = null;
    if (id != null) {
        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(id));
        if (p instanceof FlatStudy) {
            study = (FlatStudy) p;
        } else {
            response.sendRedirect("studies.jsp");
        }
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <h2><%=study.getName()%>: Gateways</h2>
        <h4>Managing: <%=EJBLocator.lookupFlatStudyBean().getObjectCount(ticket, study.getId(), FlatGateway.class)%> gateways</h4>
        <p>
            <a href="../../servlets/deviceregistration/registration.json?studyId=<%=study.getId()%>&deviceType=generic&method=generate">Register new device</a></p>
        </p>

        <table id="devices" class="table table-bordered" width="100%">
            <thead>
                <tr>
                    <th style="width:20%;">Created</th>
                    <th>ID</th>
                    <th style="width:15%;">Action</th>
                </tr>
            </thead>
        </table>      
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
    </div>
</div>
<div id="confirmDiv" class="dialog"></div>
<script type="text/javascript">
    var table;
    var confirmDialog = new ConfirmDialog();
    confirmDialog.init("confirmDiv");
    
    $(document).ready(function(){
        
        table = $('#devices').dataTable( {
            "bjQueryUI": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../../servlets/flatstudypager?cn=<%=FlatGateway.class.getName()%>&sid=<%=study.getId()%>&showDataButton=true&showManageButton=true&showDeleteButton=true&showDownloadButton=true&showMapButton=true&showEventDownloadButton=true"
        });
        
    });
    
    function showFolder(id){
        document.location = "data.jsp?id=<%=study.getId()%>&folderid=" + id;
    }    

    function mapDevice(id){
        document.location="locationhistory.jsp?id=" + id;
    }
    
    function manageDevice(id){
        document.location = "gateway.jsp?id=" + id;
    }    
    
    function downloadEvents(id){
        document.location = "gatewaydownload.jsp?id=" + id;
    }
    
    function deleteDevice(id){
        confirmDialog.yesCallback = function(){
            jsonCall({gatewayId: id}, "../../servlets/admin?method=deleteFlatGateway", function(o){
                table.fnDraw(true);
            });
        };
        confirmDialog.show("Are you sure you want to delete this gateway?");
    }
    
    function downloadRegistration(id){
        document.location = "../../servlets/deviceregistration/registration.json?studyId=<%=study.getId()%>&deviceId=" + id + "&method=download";
    }
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>