<%@page import="com.connexience.server.model.project.flatstudy.FlatGateway"%>
<%@page import="com.connexience.server.web.APIUtils"%>
<%--
  Index Page
  Author: Hugo
--%>

<style>
    .queryToolbar {
    padding-top: 10px;
    padding-left: 10px;        
    }
</style>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>
<script type="text/javascript" src="../../scripts/openlayers/OpenLayers.js"></script>
<script type="text/javascript" src="../../scripts/d3/d3.v3.min.js"></script>
<script type="text/javascript" src="../../scripts/d3/nv.d3.js"></script>
<script type="text/javascript" src="../../scripts/moment/moment.min.js"></script>
<link rel="stylesheet" href="../../scripts/d3/nv.d3.css"/>
<link rel="stylesheet" href="gateway.css"/>

<%
    if (publicUser) {
        response.sendRedirect("../../pages/front/welcome.jsp");
    }
    String idStr = ParameterSanitizer.sanitizeNumerical(request.getParameter("id"));
    if(idStr==null || idStr.isEmpty()){
        response.sendRedirect("studies.jsp");
    }
    
    String id = idStr;
    
    FlatStudy study = null;
    FlatGateway gateway = null;
    if (id != null) {
        gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, Integer.valueOf(id));
        study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, gateway.getStudyId());
    } else {
        response.sendRedirect("studies.jsp");
    }

    menu_study = study;
%>

<div class="row-fluid">
    <div class="span9">
        <div class="row-fluid">
            <h2>Gateway: <%=gateway.getExternalId()%></h2>        
        </div>
        
        <%@include file="include/daterange.jspf"%>

        <hr>
        <div class="row-fluid">
            Reading date: <span id="readingDate">--</span>
        </div>        
        <div class="row-fluid">
            <div class="span4">        
                <div class="panel blue" onclick="showTrend('Temperature');">        
                    <div class="panel-heading">        
                        <div class="row">        
                            <div class="col-xs-3">        
                                <i class="fa fa-thermometer fa-5x fa-white"></i>        
                            </div>        
                            <div class="col-xs-9 text-right panel-text">        
                                <div class="huge" id="Temperature">---</div>        
                                <div>Ambient Temperature</div>    
                            </div>    
                        </div>    
                    </div>    
                </div>    
            </div>       

            <div class="span4">        
                <div class="panel blue" onclick="showTrend('Humidity');">        
                    <div class="panel-heading">        
                        <div class="row">        
                            <div class="col-xs-3">        
                                <i class="fa fa-flask fa-5x fa-white"></i>        
                            </div>        
                            <div class="col-xs-9 text-right panel-text">        
                                <div class="huge" id="Humidity">---</div>        
                                <div>Relative Humidity</div>    
                            </div>    
                        </div>    
                    </div>    
                </div>    
            </div>              

            <div class="span4">        
                <div class="panel blue" onclick="showTrend('Pressure');">        
                    <div class="panel-heading">        
                        <div class="row">        
                            <div class="col-xs-3">        
                                <i class="fa fa-cloud fa-5x fa-white"></i>        
                            </div>        
                            <div class="col-xs-9 text-right panel-text">        
                                <div class="huge" id="Pressure">---</div>        
                                <div>Pressure</div>    
                            </div>    
                        </div>    
                    </div>    
                </div>    
            </div>             
        </div>
          
        <div class="row-fluid">
            <div class="span12">
                <h4>Plot: <span id="plottype"></span></h4>
                <div class="dash_chart" id="chart1"><svg></svg></div>
            </div>
            
        </div>
    </div>
    <div class="span3">
        <%@include file="menu.jspf"%>
        <h4>Last Location</h4>
        <div id="basicMap" style="width:275px; height:300px;"></div>
    </div>
</div>
<script type="text/javascript">
    var map = new OpenLayers.Map("basicMap");
    var fromProjection = new OpenLayers.Projection("EPSG:4326");
    var toProjection   = new OpenLayers.Projection("EPSG:900913");
    var markers;
    
    Number.prototype.pad = function(size) {
          var s = String(this);
          while (s.length < (size || 2)) {s = "0" + s;}
          return s;
    }
    
    $(document).ready(function(){
        var mapnik = new OpenLayers.Layer.OSM();
        map.addLayer(mapnik);
        
        markers = new OpenLayers.Layer.Markers( "Markers" );
        map.addLayer(markers);
        
        /*
        $('#devices').dataTable( {
            "bjQueryUI": true,
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "../../servlets/flatstudypager?cn=<%=FlatGateway.class.getName()%>&sid=<%=study.getId()%>&showDataButton=true&showManageButton=true"
        });
        */
           
        asyncJsonGet('../../servlets/eventdownload?eventtype=Environmental&datatype=mostrecent&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>', function(o){
            var data = o.data;
            if(data.Temperature){
                document.getElementById("Temperature").innerHTML = data.Temperature.toFixed(1) + "C";
            } else {
                document.getElementById("Temperature").innerHTML = "N/A";
            }
            
            if(data.Humidity){
                document.getElementById("Humidity").innerHTML = data.Humidity.toFixed(1) + "%";
            } else {
                document.getElementById("Humidity").innerHTML = "N/A";
            }
            
            if(data.Pressure){
                document.getElementById("Pressure").innerHTML = data.Pressure.toFixed(1) + "kPa";   
            } else {
                document.getElementById("Pressure").innerHTML = "N/A";
            }
                
            document.getElementById("readingDate").innerHTML = new Date(o.timestamp).toUTCString();
            setDefaultDateRange(new Date(o.timestamp));
        });
        
        asyncJsonGet('../../servlets/eventdownload?eventtype=Location&datatype=mostrecent&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>', function(o){
            var data = o.data;
            var position = new OpenLayers.LonLat(data.Longitude,data.Latitude).transform( fromProjection, toProjection);
            var zoom = 15;             
            var m = new OpenLayers.Marker(position);
            map.setCenter(position, zoom );  
            markers.addMarker(m);            
        });
        
       
        
    });
    
    function formatDate(dt){
        return dt.getDate().pad(2) + "-" + (dt.getMonth() + 1).pad(2) + "-" + dt.getUTCFullYear();
    }
    
    function showFolder(id){
        document.location = "data.jsp?id=<%=study.getId()%>&folderid=" + id;
    }    

    function manageDevice(id){
        document.location = "data.jsp?id=<%=study.getId()%>&folderid=" + id;
    }    
    
    function showTrend(name){
        var startDateField = document.getElementById("startDate");
        var sd = moment(startDateField.value, "DD-MM-YYYY").toDate();
        
        var endDateField = document.getElementById("endDate");
        var ed = moment(endDateField.value, "DD-MM-YYYY").toDate();
        
        asyncJsonGet('../../servlets/eventdownload?eventtype=Environmental&datatype=trend&id=<%=gateway.getId()%>&studyId=<%=study.getId()%>&start=' + sd.getTime() + '&end=' + ed.getTime(), function(o){
            var values = new Array();
            var row;
            for(var i=0;i<o.length;i++){
                row = JSON.parse(o[i]);
                values.push({"x": row["timestamp"], "y":row.data[name]});
            }
            var plotData = {
                "key": name,
                "color": "#ffaa00",
                "values": values
            };
            
            data = [plotData];
            
            d3.select('#chart1 svg')
                .datum(data)
                .call(chart);
            document.getElementById('plottype').innerHTML = name;
        });
        
    }

    // Wrapping in nv.addGraph allows for '0 timeout render', stores rendered charts in nv.graphs, and may do more in the future... it's NOT required
    var chart;
    var data;
    
    nv.addGraph(function() {
        chart = nv.models.lineChart()
            .options({
                duration: 300,
                useInteractiveGuideline: true
            })
        ;
        // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
        chart.xAxis
            .axisLabel("Time (s)")
            .tickFormat(function(d) {
                return d3.time.format.utc('%Y-%m-%dT%H:%M:%S.%LZ')(new Date(d))
            })
            .staggerLabels(true)
        ;
        chart.yAxis
            .axisLabel('Value')
            .tickFormat(function(d) {
                if (d == null) {
                    return 'N/A';
                }
                return d3.format(',.2f')(d);
            });
        
        chart.xScale(d3.time.scale());
        nv.utils.windowResize(chart.update);
        return chart;
    });
</script>
            
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>