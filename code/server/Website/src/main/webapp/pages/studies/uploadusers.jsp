<!--
* e-Science Central
* Copyright (C) 2008-2016 Inkspot Science Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation at:
* http://www.gnu.org/licenses/gpl-2.0.html
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<%@ page import="com.connexience.server.model.project.study.Study" %>
<%@include file="../../WEB-INF/jspf/page/header.jspf" %>

<div id="participants">
    <div class="row-fluid">
        <div class="span9">
            <h2>In-Field Users</h2>
            <table cellpadding="0" cellspacing="0" border="0" class="upload-users table table-bordered" id="upload-users">
                <thead>
                    <tr>
                        <th>Username</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
        <div class="span3">
            <%@include file="menu.jspf"%>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/tools/dataTables.tableTools.js"></script>
<script type="text/javascript" src="../../scripts/datatables-1.9.4/js/dataTables.bootstrap.js"></script>
<link type="text/css" href="../../scripts/datatables-1.9.4/css/jquery.dataTables.css" />
<link type="text/css" href="../../scripts/datatables-1.9.4/css/dataTables.bootstrap.css" />
<script type="text/javascript">

    var selectedUploaders = [];

    $(document).ready(function(){

        var uploadersTable;
        var editRow;

        $('#upload-users').on('click', 'tr', function () {

            var rowData = uploadersTable.fnGetData(uploadersTable.fnGetPosition(this));
            var index = jQuery.inArray(rowData.id, selectedUploaders);

            editRow = rowData;

            if ( index === -1 )
            {
                selectedUploaders.push(rowData.id);
            }
            else
            {
                selectedUploaders.splice(index, 1);
            }

            if(selectedUploaders.length == 1)
            {
                $('#editUploader').prop('disabled', false);
                $('#deleteUploader').prop('disabled', false);
            }
            else if(selectedUploaders.length > 1)
            {
                $('#editUploader').prop('disabled', true);
                $('#deleteUploader').prop('disabled', false);
            }
            else
            {
                $('#editUploader').prop('disabled', true);
                $('#deleteUploader').prop('disabled', true);
            }

            $(this).toggleClass('active');
        });

        $(document).on("click", "#createUploader", function (event) {

            $("#uploaderID").val('');
            $("#uploaderUsername").val('');

            $("#upload-user-modal-label").text("Create In-Field User");
            $("#edit-upload-user").css('display', 'block');
            $("#delete-upload-user").css('display', 'none');
            $("#submit-upload-user").text("Create In-Field User");

            $("#upload-user-modal").modal("show");
        });

        $(document).on("click", "#editUploader", function (event)
        {
            $("#uploaderID").val(editRow.id);
            $("#uploaderUsername").val(editRow.username);

            $("#upload-user-modal-label").text("Edit In-Field User");
            $("#edit-upload-user").css('display', 'block');
            $("#delete-upload-user").css('display', 'none');
            $("#submit-upload-user").text("Edit In-Field User");

            $("#upload-user-modal").modal("show");
        });

        $(document).on("click", "#deleteUploader", function (event)
        {
            var message;

            if(selectedUploaders.length == 1)
            {
                message = "<p>Are you sure you wish to delete this in-field user?</p>";
                $("#upload-user-modal-label").text("Delete In-Field User");
                $("#delete-upload-users").text("Delete In-Field User");
            }
            else if(selectedUploaders.length > 1)
            {
                message = "<p>Are you sure you wish to delete these " + selectedUploaders.length + " in-field users?</p>";
                $("#upload-user-modal-label").text("Delete In-Field Users");
                $("#delete-upload-users").text("Delete In-Field Users");
            }

            $("#delete-upload-user .modal-body").html(message);

            $("#edit-upload-user").css('display', 'none');
            $("#delete-upload-user").css('display', 'block');

            $("#upload-user-modal").modal("show");
        });

        uploadersTable = $('#upload-users').dataTable( {
            "sDom": "<'row-fluid'<'span6'<'btn-group'>><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
            "oLanguage": {
                "sSearch": ""
            },
            "bRetrieve": true,
            "sAjaxSource": "/website-api/rest/study/uploaders",
            "sAjaxDataProp": "",
            "fnServerParams": function ( aoData ) {
                aoData.push( { "start": 0, "end": -1 } );
            },
            "aoColumns": [
                {
                    "mData": "username"
                }
            ]
        } );

        $('.dataTables_filter input').attr('placeholder', 'Search...');

        var editButtons =   '<button id="createUploader" class="createUploader btn">Create</button>' +
                '<button id="editUploader" class="editUploader btn" disabled="true">Edit</button>' +
                '<button id="deleteUploader" class="deleteUploader btn" disabled="true">Delete</button>';

        $("#upload-users_wrapper .btn-group").html(editButtons);

    });

</script>
<%@include file="upload-user.jspf" %>
<%@include file="../../WEB-INF/jspf/page/footer.jspf" %>