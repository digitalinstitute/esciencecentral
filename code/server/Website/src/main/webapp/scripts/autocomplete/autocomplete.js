/*
 Autocomplete component for example, allow a user to choose from a selection of friends.

 Example usage:

 <div>
 <label for="auto-complete-input">Friends: </label>
 <input id="auto-complete-input"/>
 </div>

 <div>
 Result:
 <div id="log"></div>
 </div>

$(document).ready(function()
{
  function selectCallback(event, ui)
  {
    $("<div/>").text("Selected: " + ui.item.value + " aka " + ui.item.id).prependTo("#log");
    $("#log").attr("scrollTop", 0);
  }

  var autocomplete = new AutoComplete("auto-complete-input",
      "../../servlets/autocomplete?type=friends",
      selectCallback);
});

*/

function AutoComplete(divName, url, selectCallback)
{
  $.getJSON(url, function(data, status, xhr)
  {
    $("#" + divName).autocomplete({
      minLength: 2,
      source: data,
      select: selectCallback
    });
  });
}
