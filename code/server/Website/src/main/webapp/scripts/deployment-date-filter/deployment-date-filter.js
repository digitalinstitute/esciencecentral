(function(){

    var deploymentDateFilter = {};

    var defaults = {
        eventCallbacks : {
            changeDate : function(){},
            changeDateUnitOrCount : function(){},
            ajaxSuccess : function(){}
        },
        loggerTypeIds : []
    };

    deploymentDateFilter = window.deploymentDateFilter ? $.extend(defaults, window.deploymentDateFilter) : defaults;

    function init(dataType) {

        dataType = dataType || 'counts'; // Other could be 'counts' to bring counts or 'loggers' to bring back all loggers

        // Add date picker
        var dpSettings = {
            format: 'dd/mm/yyyy',
            pickTime: false,
            autoclose: true,
            todayBtn: "linked",
            todayHighlight: true
        };

        $('#editAvailability').on('click', function(e){
            e.preventDefault();
            $('#avail-summary').css('display', 'none').next().css('display', 'block');
        });

        $('#cancelAvailabilityEdit').on('click', function(e){
            e.preventDefault();
            $('#avail-summary').css('display', 'block').next().css('display', 'none');
        });

        $('#showFilterByDate').on('click', function(e){
            e.preventDefault();
            getDeploymentData();
            $('#avail-summary').css('display', 'block').next().css('display', 'none').next().css('display', 'none');
        });

        $('#showAllLoggers').on('click', function(e){
            e.preventDefault();
            getDeploymentData(0,1);
            $('#avail-summary').css('display', 'none').next().css('display', 'none').next().css('display', 'block');
        });


        $('#startDateAvailability').datepicker(dpSettings);

        $('#startDateAvailability').on('changeDate', function(){

            if (moment($(this).val(), 'DD/MM/YYYY').isValid()) {
                setSessionDate();
                updateSummaryText();
                deploymentDateFilter.eventCallbacks.changeDate();
                getDeploymentData();
            }

        });

        $('#dateUnit, #dateUnitCount').on('change', function(){
            setSessionDate();
            updateSummaryText();
            deploymentDateFilter.eventCallbacks.changeDateUnitOrCount();
            getDeploymentData();
        });

        $('#dateUnitCount').on('change keyup', updateDateUnit);

        function setSessionDate() {
            var dateSettings = {
                startDate : $('#startDateAvailability').val(),
                dateUnitCount : $('#dateUnitCount').val(),
                dateUnit : $('#dateUnit').val()
            };

            return window.sessionStorage.setItem('deploymentDateSettings', JSON.stringify(dateSettings));
        }

        function getSessionDate() {

            var defaultSettings = {
                startDate : moment().format('DD/MM/YYYY'),
                dateUnitCount : '1',
                dateUnit :'months'
            };

            return window.sessionStorage.getItem('deploymentDateSettings') ? JSON.parse(window.sessionStorage.getItem('deploymentDateSettings')) : defaultSettings;
        }

        function updateDateUnit() {
            var text = Number($('#dateUnitCount').val()) === 1 ? ['Day','Week','Month'] : ['Days','Weeks','Months'];

            $('#dateUnit option').each(function(i){
                $(this).html(text[i]);
            });
        }

        function setDateFields() {
            var dateSettings = getSessionDate();

            $('#startDateAvailability').val(dateSettings.startDate);
            $('#dateUnitCount').val(dateSettings.dateUnitCount);
            $('#dateUnit').val(dateSettings.dateUnit);
            $('#startDateAvailability').datepicker('update');

            updateSummaryText();

            updateDateUnit();
        }

        function updateSummaryText() {
            var dateSettings = getSessionDate(),
                $dateSpans = $('#avail-summary strong');

            $dateSpans.eq(0).html(dateSettings.startDate);
            $dateSpans.eq(1).html(dateSettings.dateUnitCount + ' ' + (Number(dateSettings.dateUnitCount) > 1 ? dateSettings.dateUnit : dateSettings.dateUnit.substring(0, dateSettings.dateUnit.length - 1)));

        }

        function getStartEnd(dateType) {

            dateType = dateType || 'timestamp';

            var startDate, endDate, dateSettings = getSessionDate();

            if (dateSettings) {
                startDate =  moment(dateSettings.startDate, 'DD/MM/YYYY');
                endDate = startDate.clone().add(dateSettings.dateUnitCount, dateSettings.dateUnit);
            }

            if (dateType === 'timestamp') {
                return {
                    startDate : startDate.format('X'),
                    endDate: endDate.format('X')
                }
            } else {
                return {
                    startDate : startDate,
                    endDate: endDate
                }
            }
        }

        function getDeploymentData(start, end) {
            if (deploymentDateFilter.loggerTypeIds && deploymentDateFilter.loggerTypeIds.length) {

                var qs = '', endpoint = '';

                if (dataType === 'counts') {

                    endpoint = '/website-api/rest/logger/type/loggers/available';

                    $.each(deploymentDateFilter.loggerTypeIds, function(index, item){
                        qs = qs + 'typeIdList=' + item;

                        if (index !== deploymentDateFilter.loggerTypeIds.length - 1) {
                            qs = qs + '&';
                        }
                    });


                } else {

                    endpoint = '/website-api/rest/logger/type/'+deploymentDateFilter.loggerTypeIds[0]+'/loggers/available';
                }

                qs = qs + '&startDate=' + (start ? start : getStartEnd().startDate)*1000;
                qs = qs + '&endDate=' + ( end ? end : getStartEnd().endDate)*1000;



                $.ajax({
                    type: 'GET',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    url: endpoint,
                    data: qs,
                    success: function (data) {

                        deploymentDateFilter.eventCallbacks.ajaxSuccess(data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                    }
                });
            }
        }

        if ($('#startDateAvailability').val() !== '') {
            getDeploymentData();
        }

        setDateFields();


        deploymentDateFilter.getDeploymentData = getDeploymentData;
    }

    deploymentDateFilter.init = init;
    window.deploymentDateFilter = deploymentDateFilter;

})();
