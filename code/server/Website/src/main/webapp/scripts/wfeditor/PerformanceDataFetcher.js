/* 
 * This script can fetch performance data for workflow blocks
 */
function PerformanceDataFetcher(drawing){
    this.drawing = drawing;
    this.labelId = null;
    this.dataPresent = false;
    this.dataStale = true;
    this.fetching = false;
    this.autoFetch = false;
    this.countdownTimer = 10;
    setTimeout(this.timerTask, 1000, this);
}

/** Fetch the duration historam for a version of a block */
PerformanceDataFetcher.prototype.fetchDurationHistogram = function(serviceId, versionId, callback){
    asyncJsonGet(performanceUrl + "/chartdata/durationhistogram/" + serviceId + "/" + versionId, callback);
};

PerformanceDataFetcher.prototype.fetchDurationActualVersusPredicted = function(modelId, callback) {
    asyncJsonGet(performanceUrl + "/chartdata/historicalxy/" + modelId , callback);
};

PerformanceDataFetcher.prototype.fetchModelResults = function(id, callback){
    asyncJsonGet(performanceUrl + "/rest/models/" + id + "/results", callback);
};

/** Fetch the performance data by sumbitting the workflow data as a string
 * to the server */
PerformanceDataFetcher.prototype.modelDrawing = function(){
    this.autoFetch = true;
    var saver = new DrawingSaver();
    saver.drawing = this.drawing;
    var drawingJson = saver.createDrawingJson();
    var fetcher = this;
    
    var cb = function(o){
        fetcher.fetching = false;
        
        // Put the performance data into the drawing
        if(o.reports){
            fetcher.removePerformanceData();
            var reports = o.reports;
            var guid;
            var report;
            var block;
            var outputResults;
            var outputModel;
            var blockOutput;
            
            for(var i=0;i<reports.length;i++){
                report = reports[i];
                guid = report.blockGuid;
                block = fetcher.drawing.getBlockByGuid(guid);
                if(block){
                    // Sort the output sizes
                    outputResults = report.models.outputResults;
                    for(var j=0;j<outputResults.length;j++){
                        outputModel = outputResults[j];
                        blockOutput = block.getOutput(outputModel.outputName);
                        if(blockOutput){
                            blockOutput.sizeModel = outputModel;
                        }
                    }
                    
                    // Add the duration information
                    if(report.models.duration){
                        block.durationModel = report.models.duration;
                    }
                }
            }
            
            fetcher.dataPresent = true;
            fetcher.dataStale = false;

        } else {
            fetcher.dataPresent = false;
        }
        fetcher.displayDataStatus();
        fetcher.drawing.repaint();
    };
    
    // Set the label
    this.fetching = true;
    this.displayDataStatus();
    
    jsonCall(drawingJson, performanceUrl + "/rest/workflows/prediction", cb);
    
};

/** Display the status of the performance data */
PerformanceDataFetcher.prototype.displayDataStatus = function(){
    if(this.labelId){
        var label = document.getElementById(this.labelId);
        if(label){
            if(this.fetching){
                label.innerHTML = "Fetching...";
            } else {
                if(this.dataPresent){
                    if(this.dataStale){
                        if(this.countdownTimer>=0){
                            label.innerHTML = "Stale data. Update in: " + this.countdownTimer + " seconds";
                        } else {
                            label.innerHTML = "Stale data";
                        }
                            
                    } else {
                        label.innerHTML = "Up to date";
                    }
                } else {
                    label.innerHTML = "No data";
                }
            }
        }            
    }    
};

PerformanceDataFetcher.prototype.timerTask = function(fetcher){
    // Should we auto fetch 
    if(fetcher.countdownTimer>0){
        fetcher.countdownTimer--;
    } else {
        // At zero
        if(fetcher.dataPresent && fetcher.dataStale && fetcher.fetching==false && fetcher.autoFetch){
            fetcher.modelDrawing();
            fetcher.countdownTimer = 10;
        }
    }
    fetcher.displayDataStatus();
    setTimeout(fetcher.timerTask, 1000, fetcher);
};

PerformanceDataFetcher.prototype.dirtyData = function(){
    this.dataStale = true;
    this.countdownTimer = 10;
    this.displayDataStatus();
};

PerformanceDataFetcher.prototype.disableModelling = function(){
    this.autoFetch = false;
    this.removePerformanceData();
    this.displayDataStatus();
};

PerformanceDataFetcher.prototype.removePerformanceData = function(){
    this.dataPresent = false;
    if(this.drawing){
        var i;
        var j;
        var k;
        var block;
        for(i=0;i<this.drawing.blocks.size();i++){
            block = this.drawing.blocks.get(i);
            block.durationModel = undefined;
            for(j=0;j<block.outputList.size();j++){
                block.outputList.get(j).sizeModel = undefined;
            }
        }
        this.drawing.repaint();
    }
};
