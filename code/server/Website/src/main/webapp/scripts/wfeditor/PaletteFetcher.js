/* 
 * This object fetches the palette data from the server
 */
function PaletteFetcher()
{
    this.wfeditor = null;

    this.start = [];
    this.end = [];
    var fetcher = this;
    this.callback = function(o)
    {
        if (!o.error)
        {
            var paletteJson = o.palette;
            fetcher.displayPalette(paletteJson);
            fetcher.paletteFetchComplete();
        }
    };
}

PaletteFetcher.prototype.fetchPalette = function()
{
    var callData = {
        type: "dynamicServices"
    };
    jsonCall(callData, "../../servlets/workflow?method=getHierarchicalPalette", this.callback);
};

/** Build the palette menu */
PaletteFetcher.prototype.displayPalette = function(paletteJson)
{
    // Sort Alphabetically
    paletteJson.sort(function(a, b) { return ((a.name < b.name) ? -1 : ((a.name > b.name) ? 1 : 0)); });

    var menuDiv = document.getElementById(this.wfeditor.paletteDivName);
    menuDiv.innerHtml = "";

    var input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("id", "block-search");
    input.setAttribute("placeholder", "Filter block palette");
    input.setAttribute("style", "width:88%");


    //Make the jQuery Contains selector case in-sensitive
    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
        return function (elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    $(input).on('keyup', function(event) {
        var $searchInput = $(this),
            $palette = $('#drawingCanvas_palette'),
            $navHeadings = $palette.find('div.accordion-group div.accordion-heading'),
            $elementsToShow;

        // Hide everything first
        $('.accordion-body').removeClass('in').css('height', '0px');
        $navHeadings.addClass('hidden');
        $navHeadings.parent('div.accordion-group').addClass('hidden');
        $('.accordion-inner').find('li').addClass('hidden');

        function searchMatch() {
            var inputStrings = $searchInput.val().split(' '),
                $returnedElements = $palette.find('li, div.accordion-heading');

            $.each(inputStrings, function() {
                if (this.toString() !== ''){
                    $returnedElements = $returnedElements.filter(':contains("'+this.toString()+'")');
                }

            });

            return $returnedElements;
        }

        if ($searchInput.val().length > 2){

            $elementsToShow = searchMatch();

            $elementsToShow.removeClass('hidden');

            $elementsToShow.each(function(){
                $(this).parents('div.accordion-group').removeClass('hidden');
                $(this).filter('li').parents('div.accordion-body').prev('div.accordion-heading').removeClass('hidden');
                $(this).filter('li').parents('div.accordion-body').addClass('in').css('height', 'auto');
            });

            $('div.accordion-heading').filter(':visible').closest('div.accordion-body').prev('div.accordion-heading').removeClass('hidden');
            $('div.accordion-heading').filter(':visible').parents('.accordion-body').addClass('in').css('height', 'auto');
            $('div.accordion-heading').filter(':visible').each(function() {

                var $nxtAccordianGroup = $(this).next('div.accordion-body').children('div.accordion-inner').children('div.accordion-group');

                if ($(this).next('div.accordion-body').find('li:visible').length == 0) {
                    $(this).next('div.accordion-body').find('li').removeClass('hidden');
                    $(this).next('div.accordion-body').find('li').parents('div.accordion-body').css('hidden');
                }

                if ($nxtAccordianGroup.children('div.accordion-heading:visible').length == 0) {
                    $nxtAccordianGroup.children('div.accordion-heading').removeClass('hidden');
                    $nxtAccordianGroup.removeClass('hidden');
                }
            })



        } else {
            $('.accordion-body').removeClass('in').css('height', '0px');
            $navHeadings.removeClass('hidden');
            $navHeadings.parent('div.accordion-group').removeClass('hidden');
            $('.accordion-inner').find('li').removeClass('hidden');
        }



    });

    // Wrap the input and add to the menu

    var inputWrap = $("<div>").addClass("input-prepend").css("display", "block").append($("<span>").addClass("add-on").html('<i class="icon-filter" style="margin-top: 4px"></i>'));
    inputWrap.append(input);

    menuDiv.appendChild(inputWrap[0]);

    var category;

    for (var i = 0; i < paletteJson.length; i++)
    {
        category = this.createPaletteCategory(paletteJson[i]);

        var categoryGroup = document.createElement("div");
        categoryGroup.setAttribute("class", "accordion-group");

        categoryGroup.appendChild(category.title);
        categoryGroup.appendChild(category.services);

        menuDiv.appendChild(categoryGroup);
    }

    menuDiv.setAttribute("class", "accordion");

    $("#" + this.wfeditor.paletteDivName).collapse()
};

/** Create a piece of category html */
PaletteFetcher.prototype.createPaletteCategory = function(categoryJson)
{
    var categoryDiv = document.createElement("div"),
        catID = (categoryJson.blocks.length > 0 ? categoryJson.blocks[0].category : categoryJson.name).toLowerCase().replace(";", "-").toLowerCase().replace(" ", "-").replace(".", "-");
    categoryDiv.setAttribute("class", "accordion-heading");

    var title = document.createElement("a");
    title.setAttribute("href", "#accordion-" + (catID));
    title.setAttribute("class", "accordion-toggle");
    title.setAttribute("data-toggle", "collapse");
    title.setAttribute("data-parent", "#pallet");

    // Add folder icon
    var categoryIcon = document.createElement("span");
    categoryIcon.setAttribute("class", "icomoon-folder-open");
    title.appendChild(categoryIcon);

    title.appendChild(document.createTextNode(' ' + categoryJson.name));

    categoryDiv.wfeditor = this;
    categoryDiv.appendChild(title);

    if(categoryJson.children.length > 0)
    {
        var childrenDiv = document.createElement("div");
        childrenDiv.setAttribute("class", "accordion-group");

        for (var i = 0; i < categoryJson.children.length; i++)
        {
            childrenDiv.appendChild(this.createPaletteCategory(categoryJson.children[i]).title);
            childrenDiv.appendChild(this.createPaletteCategory(categoryJson.children[i]).services);
        }
    }

    var blocksDiv = document.createElement("ul");
    blocksDiv.setAttribute("class", "unstyled");

    for (var i = 0; i < categoryJson.blocks.length; i++)
    {
        var service = categoryJson.blocks[i];

        var serviceListItem;
        serviceListItem = document.createElement("li");
        serviceListItem.setAttribute("id", service.serviceId);
        serviceListItem.appendChild(document.createTextNode(service.name));

        var addBlock = document.createElement("span");
        addBlock.setAttribute("class", "icomoon-plus add-block");
        serviceListItem.appendChild(addBlock);

        blocksDiv.appendChild(serviceListItem);
    }

    var contentHolder = document.createElement("div");
    contentHolder.setAttribute("class", "accordion-inner");

    if(categoryJson.children.length > 0)
    {
        contentHolder.appendChild(childrenDiv);
    }

    contentHolder.appendChild(blocksDiv);

    var contentDiv = document.createElement("div");
    contentDiv.setAttribute("id", "accordion-" + (catID))
    if(categoryJson.name == "File Management")
    {
        contentDiv.setAttribute("class", "accordion-body collapse in");
    }
    else
    {
        contentDiv.setAttribute("class", "accordion-body collapse");
    }
    contentDiv.appendChild(contentHolder);


    return {title: categoryDiv, services: contentDiv};
};

/** Create a category block html */
PaletteFetcher.prototype.createPaletteCategoryBlocks = function(categoryJson)
{
    var categoryDiv = document.createElement("div");
    categoryDiv.setAttribute("class", "accordion-heading");

    var title = document.createElement("a");
    title.setAttribute("href", "#accordion-" + (categoryJson.name.toLowerCase()).replace(" ", "-"));
    title.setAttribute("class", "accordion-toggle");
    title.setAttribute("data-toggle", "collapse");
    title.setAttribute("data-parent", "#pallet");
    title.appendChild(document.createTextNode(categoryJson.name));
    categoryDiv.wfeditor = this;
    categoryDiv.appendChild(title);

    var listDiv = document.createElement("ul");
    listDiv.setAttribute("class", "unstyled");

    var i;
    var serviceArray = categoryJson.blocks;
    var serviceCount = serviceArray.length;
    var service;
    var serviceListItem;

    for (i = 0; i < serviceCount; i++)
    {
        service = serviceArray[i];

        serviceListItem = document.createElement("li");
        serviceListItem.setAttribute("id", service.serviceId);
        serviceListItem.appendChild(document.createTextNode(service.name));

        var addBlock = document.createElement("span");
        addBlock.setAttribute("class", "icomoon-plus add-block");
        serviceListItem.appendChild(addBlock);

        listDiv.appendChild(serviceListItem);
    }

    var contentHolder = document.createElement("div");
    contentHolder.setAttribute("class", "accordion-inner");
    contentHolder.appendChild(listDiv);

    var contentDiv = document.createElement("div");
    contentDiv.setAttribute("id", "accordion-" + (categoryJson.name.toLowerCase()).replace(" ", "-"))
    if(categoryJson.name == "File Management")
    {
        contentDiv.setAttribute("class", "accordion-body collapse in");
    }
    else
    {
        contentDiv.setAttribute("class", "accordion-body collapse");
    }
    contentDiv.appendChild(contentHolder);


    return {title: categoryDiv, services: contentDiv};
};

/** Palette has been retrieved OK */
PaletteFetcher.prototype.paletteFetchComplete = function()
{
    $("#" + this.wfeditor.paletteDivName + " li").draggable(
        {cursor: "pointer",
        opacity: 0.7,
        zIndex: 9999,
        appendTo: 'body',
        scroll: false,
        helper: function (event) {
            return $("<img src='../../scripts/wfeditor/images/block.png'/>");
        },
        cursorAt: { top: 32, left: 31 }
    });

    var params = [];
    params.wfeditor = this.wfeditor;

    $(".add-block").click(params, function()
    {
        var wfeditor = params.wfeditor;
        var left = 10;
        var top = 10;
        if (wfeditor.drawing.selectedObjects.size() > 0)
        {
            var selected = wfeditor.drawing.selectedObjects.get(0);
            if (selected instanceof DrawingBlock)
            {
                left = selected.left + selected.width + 60;
                top = selected.top;
            }
        }

        wfeditor.addBlock($(this).parent().attr("id"), top, left);
    });
    
    resizeEditor();
};
