/* 
 * This object writes a workflow diagram as a JSON object that is posted
 * back to the server where it is reconstructed into a drawing
 */
function DrawingSaver(){
    this.drawing = null;            // Drawing that will be populated
    this.editor = null;             // Editor that will be used to repaint the drawing
    this.width = 712;               // Width of the drawing canvas
    this.height = 400;              // Height of the drawing canvas
    this.documentId = "";           // ID of the saved document
    this.versionId = "";            // ID of the saved version
    this.saveComplete = function(){};    // Additional function to call when the drawing has been saved
    this.callback = function(o){

    };
}

DrawingSaver.prototype.saveDrawing = function(extraCallback){
    var url = rewriteAjaxUrl("../../servlets/workflow?method=putWorkflowJson");
    var callData = this.createDrawingJson();
    var callString = JSON.stringify(callData);
    this.drawing.displayLabel = "Saving...";
    this.drawing.displayingReports = true;
    this.drawing.repaint();
    var saver = this;
    
    var cb = function(o){
        if(!o.error){
            saver.drawing.displayLabel = null;
            saver.drawing.displayingReports = false;
            saver.drawing.documentId = o.documentId;
            saver.drawing.versionId = o.versionId;
            saver.drawing.versionNumber = o.versionNumber;
            saver.versionId = o.versionId;
            saver.documentId = o.documentId;
            saver.drawing.dirty = false;
            saver.drawing.repaint();
            if(saver.saveComplete()){
                saver.saveComplete();
            }
        } else {
            saver.drawing.displayLabel = null;
            saver.drawing.displayingReports = false;
            saver.drawing.repaint();
            $.jGrowl("Error saving workflow: " + o.message);
        }
        if(extraCallback){
            extraCallback(o);
        }
    };
    
    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      fetcher: this
    });
};

/** Assemble all of the individual pieces into a drawing json object */
DrawingSaver.prototype.createDrawingJson = function(){
    var connectionCount = this.drawing.getOutputConnectionCount(connectionCount);
    var connectionArray = new Array();
    var blockCount = this.drawing.blocks.size();
    var blockArray = new Array(blockCount);
    var connection;
    var block;
    var port;
    var i;
    var j;
    var k;
    connectionCount = 0;

    for(i=0;i<blockCount;i++){
        block = this.drawing.blocks.get(i);
        blockArray[i] = this.createBlockJson(block);
        for(j=0;j<block.outputList.size();j++){
            port = block.outputList.get(j);
            for(k=0;k<port.connections.size();k++){
                connection = port.connections.get(k);
                connectionArray[connectionCount] = this.createConnectionJson(connection);
                connectionCount++;
            }
        }
    }

    var blocksJson = {
        blockCount: blockCount,
        blockArray: blockArray
    };

    var connectionsJson = {
        connectionCount: connectionCount,
        connectionArray: connectionArray
    };

    var drawingJson = {
        description: this.drawing.getDescription(),
        name: this.drawing.getName(),
        versionId: this.drawing.versionId,
        externalDataSupported: this.drawing.isExternalDataSupported(),
        externalBlockName: this.drawing.getExternalBlockName(),
        deletedOnSuccess: this.drawing.isDeletedOnSuccess(),
        singleVMMode: this.drawing.isSingleVMMode(),
        externalService: this.drawing.isExternalService(),
        onlyFailedOutputsUploaded: this.drawing.isOnlyFailedOutputsUploaded(),
        dynamicEngine: this.drawing.dynamicEngine,
        width: this.width,
        height: this.height,
        versionNumber: this.drawing.versionNumber,
        documentId: this.drawing.documentId,
        blocks: blocksJson,
        connections: connectionsJson
    };
    
    return drawingJson;
};

/** Construct a json object for a connection */
DrawingSaver.prototype.createConnectionJson = function(connection){
    var pointCount = connection.pointList.size();
    var xPointArray = new Array(pointCount);
    var yPointArray = new Array(pointCount);
    var i;
    var p;

    for(i=0;i<pointCount;i++){
        p = connection.pointList.get(i);
        xPointArray[i] = p.x;
        yPointArray[i] = p.y;
    }

    var connectionJson = {
        destinationBlockGuid: connection.destinationPort.block.guid,
        destinationPortName: connection.destinationPort.name,
        sourceBlockGuid: connection.sourcePort.block.guid,
        sourcePortName: connection.sourcePort.name,
        pointCount: pointCount,
        xPoints: xPointArray,
        yPoints: yPointArray
    };
    return connectionJson;
};

/** Construct a json object for a block */
DrawingSaver.prototype.createBlockJson = function(block){
    // List the inputs
    var inputCount = block.inputList.size();
    var inputArray = new Array(inputCount);
    var typeArray;
    var typeCount;
    var i;
    var j;
    var input;
    var inputJson;

    for(i=0;i<inputCount;i++){
        input = block.inputList.get(i);
        typeCount = input.supportedTypes.size();
        typeArray = new Array(typeCount);
        for(j=0;j<typeCount;j++){
            typeArray[j] = input.supportedTypes.get(j);
        }
        inputJson = {
            name: input.name,
            location: input.location,
            offset: input.offset,
            optional: input.optional,
            streamable: input.streamable,
            typeArray: typeArray,
            typeCount: typeCount
        };

        inputArray[i] = inputJson;
    }

    var inputs = {
        inputCount: inputCount,
        inputArray: inputArray
    };

    // List the outputs
    var outputCount = block.outputList.size();
    var outputArray = new Array(outputCount);
    var output;
    var outputJson;

    for(i=0;i<outputCount;i++){
        output = block.outputList.get(i);
        typeCount = output.supportedTypes.size();
        typeArray = new Array(typeCount);
        for(j=0;j<typeCount;j++){
            typeArray[j] = output.supportedTypes.get(j);
        }
        outputJson = {
            name: output.name,
            location: output.location,
            offset: output.offset,
            typeArray: typeArray,
            typeCount: typeCount
        }
        outputArray[i] = outputJson;

    }
    
    var outputs = {
        outputCount: outputCount,
        outputArray: outputArray
    };

    // Block properties
    var properties = block.properties.createJson();

    var location = {
        top: block.top,
        width: block.width,
        left: block.left,
        height: block.height
    };

    // Tie everything together
    var blockJson = {
        guid: block.guid,
        name: block.name,
        description: block.description,
        caption: block.caption,
        label: block.label,
        serviceId: block.serviceId,
        versionId: block.versionId,
        versionNumber: block.versionNumber,
        latestVersion: block.useLatest,
        dynamicService: block.dynamicService,
        idempotent: block.idempotent,
        deterministic: block.deterministic,
        inputs: inputs,
        outputs: outputs,
        properties: properties,
        location: location
    };
    return blockJson;
};

