// This class provides a connection that can link a block output port
// to a block input port
function DrawingConnection(source, destination){
    this.sourcePort = source;             // Source block port
    this.destinationPort = destination;   // End block port
    this.pointList = new List();          // List of extra connection routing points
}

/** Draw this connection onto a canvas context */
DrawingConnection.prototype.renderConnection = function(ctx){
    var dashed = false;
    var selected = this.sourcePort.block.parentDrawing.isObjectSelected(this);
    var dataType = this.sourcePort.getDefaultType();

    if (selected) {
        ctx.strokeStyle = SELECTED_LINK_COLOR;
        ctx.lineWidth = SELECTED_LINK_WIDTH;
        if ("control-dependency" == dataType) {
            dashed = true;
        }
    } else {
        ctx.lineWidth = NON_SELECTED_LINK_WIDTH;

        // Get the color based on data type
        if("data-wrapper"==dataType){
            ctx.strokeStyle = DATA_LINK_COLOR;

        } else if("file-wrapper"==dataType){
            ctx.strokeStyle = FILE_LINK_COLOR;

        } else if("link-wrapper"==dataType){
            ctx.strokeStyle = REFERENCE_LINK_COLOR;

        } else if("properties-wrapper"==dataType){
            ctx.strokeStyle = PROPERTIES_LINK_COLOR;

        } else if("object-wrapper"==dataType) {
            ctx.strokeStyle = OBJECT_LINK_COLOR;

        } else if("control-dependency"==dataType) {
            ctx.strokeStyle = CONTROL_DEP_LINK_COLOR;
            dashed = true;

        } else {
            ctx.strokeStyle = NON_SELECTED_LINK_COLOR;
        }
    }

    var allPoints = this.getAllPoints();

    // Move to first point
    var p1;
    var p2;

    if (dashed) {
        ctx.save();
        ctx.setLineDash([3]);
    }
    
    for(i = 1; i < allPoints.size() ; i++) {
        p1 = allPoints.get(i - 1);
        p2 = allPoints.get(i);
        ctx.beginPath();
        ctx.moveTo(p1.x, p1.y);
        ctx.lineTo(p2.x, p2.y);
        ctx.stroke();
    }

    if (dashed) {
        ctx.restore();
    }

    // Selection handlers
    if(selected){
        var halfHandle = LINK_SELECTION_HANDLE_SIZE / 2;
        ctx.fillStyle = SELECTED_LINK_COLOR;
        for(i=0;i<allPoints.size();i++){
            p1 = allPoints.get(i);
            ctx.fillRect(p1.x - halfHandle, p1.y - halfHandle, LINK_SELECTION_HANDLE_SIZE, LINK_SELECTION_HANDLE_SIZE);
        }
    }

};

/** Is a co-ordinate within the bounds of this connection */
DrawingConnection.prototype.withinBounds = function(x, y){
    var allPoints = this.getAllPoints();

    var p1;
    var p2;
    var p3 = new DrawingPoint(x, y);
    var distance;

    // Work out the distance from the lines joining them
    var i;
    for(i=1;i<allPoints.size();i++){
        p1 = allPoints.get(i -1);
        p2 = allPoints.get(i);
        distance = this.distanceFromLine(p1, p2, p3);
        if(distance<CONNECTION_HIT_DISTANCE){
            return true;
        }
    }
    return false;
};

/** Is this connection contained by a box */
DrawingConnection.prototype.isContainedBy = function(){
    return false;
};

DrawingConnection.prototype.getAllPoints = function(){
    var allPoints = new List();

    // Add the start point
    allPoints.add(this.sourcePort.block.locatePortConnectionPoint(this.sourcePort));

    // Add the intermediate points
    var i;
    for(i=0;i<this.pointList.size();i++){
        allPoints.add(this.pointList.get(i));
    }

    // Add the end point
    allPoints.add(endPoint = this.destinationPort.block.locatePortConnectionPoint(this.destinationPort));
    return allPoints;
};

DrawingConnection.prototype.distanceFromLine = function(p1, p2, p3){
    var xDelta = p2.x - p1.x;
    var yDelta = p2.y - p1.y;

    if ((xDelta == 0) && (yDelta == 0)) {
        return p1.distanceFrom(p3);
    }

    var u = ((p3.x - p1.x) * xDelta + (p3.y - p1.y) * yDelta) / (xDelta * xDelta + yDelta * yDelta);

    var closestPoint;
    if (u < 0) {
        closestPoint = p1;
    } else if (u > 1) {
        closestPoint = p2;
    } else {
        closestPoint = new DrawingPoint(p1.x + u * xDelta, p1.y + u * yDelta);
    }

    return closestPoint.distanceFrom(p3);

};

/** Add / remove a join point from this connection */
DrawingConnection.prototype.toggleJoin = function(x, y){
    var p1;
    var p2;
    var p3 = new DrawingPoint(x, y);

    var closestPoint = null;
    var allPoints = this.getAllPoints();
    var minDistance = 1000000;
    var index = -1;
    var distance;
    var i;

    for(i=1;i<allPoints.size();i++){
        p1 = allPoints.get(i-1);
        p2 = allPoints.get(i);
        distance = this.distanceFromLine(p1, p2, p3);
        
        // Find the nearest segment
        if(distance<minDistance && distance<=CONNECTION_HIT_DISTANCE){
            index = i;
            minDistance = distance;
        }
    }

    if(index!=-1){
        p1 = allPoints.get(index);
        p2 = allPoints.get(index);
        
        if(p1.distanceFrom(p3)<=CONNECTION_HIT_DISTANCE){
            // Remove p1
            index = this.pointList.indexOf(p1);
            if(index!=-1){
                this.pointList.remove(index);
            }
            
        } else if(p2.distanceFrom(p3)<=CONNECTION_HIT_DISTANCE){
            // Remove p2
            index = this.pointList.indexOf(p2);
            if(index!=-1){
                this.pointList.remove(index);
            }
            
        } else {
            // Add a new point 
            this.pointList.add(p3, index - 1);
        }
    }
};

/** Move all points in this connection by a specified amount */
DrawingConnection.prototype.moveBy = function(xOffset, yOffset){
    var i;
    var p;
    for(i=0;i<this.pointList.size();i++){
        p = this.pointList.get(i);
        p.x = p.x + xOffset;
        p.y = p.y + yOffset;
    }
    this.sourcePort.block.parentDrawing.dirty = true;
};

/** Find the closest connection point to a mouse co-ordinate */
DrawingConnection.prototype.findClosestPoint = function(x, y){
    var distance;
    var mousePoint = new DrawingPoint(x, y);
    var minDistance = 1000000;
    var i;
    var p;
    var closestPoint;

    for(i=0;i<this.pointList.size();i++){
        p = this.pointList.get(i);
        distance = p.distanceFrom(mousePoint);
        if(distance<minDistance){
            closestPoint = p;
            minDistance = distance;
        }
    }

    if(minDistance<CONNECTION_HIT_DISTANCE){
        return closestPoint;
    } else {
        return null;
    }
};

/** Move the point closest to the mouse by a specified amount */
DrawingConnection.prototype.dragPoint = function(xStart, yStart, xOffset, yOffset){
    var p = this.findClosestPoint(xStart, yStart);
    if(p!=null){
        p.x = p.x + xOffset;
        p.y = p.y + yOffset;
    }
};
