/** This class manages drawing editing */
function DrawingEditor(){
    this.drawing = null;
    this.canvas = null;
    this.wfeditor = null;
    this.canvasContext = null;
	
    // Editor state variables
    this.currentState = EDITOR_IDLE;
    this.xStart = 0;
    this.yStart = 0;
    this.touchStartX = 0;
    this.touchStartY = 0;
    this.startPort = null;
    this.mouseDown = false;
}

/** Re-draw the drawing */
DrawingEditor.prototype.repaint = function(){
    var bounds = this.drawing.calculateDrawingBounds();
    if(bounds.width>this.drawing.minimumWidth){
        this.canvas.width = bounds.width;
    } else if(bounds.width<=this.drawing.minimumWidth){
        this.canvas.width = this.drawing.minimumWidth;
    }
    if(bounds.height>this.drawing.minimumHeight){
        this.canvas.height = bounds.height;
    } else if(bounds.height<=this.drawing.minimumHeight){
        this.canvas.height = this.drawing.minimumHeight;
    }
    this.drawing.renderDrawing(this.canvas, this.currentState);
};

/** Set up the mouse events for the canvas */
DrawingEditor.prototype.setupMouse = function(){
    this.canvas.drawing = this.drawing;
    this.canvas.editor = this;
    this.canvas.wfeditor = this.wfeditor;
    this.canvasContext = this.canvas.getContext("2d");

    // Double click handler
    var dblClickHandler = function(evt){
        if(!this.drawing.displayingReports){
            var evt2 = this.editor.getEvent(evt);
            var dp = this.editor.correctCoordinates(evt2);
            var x = dp.x;
            var y = dp.y;
            var selected = this.drawing.getObjectAtLocation(x, y);
            if(selected instanceof DrawingConnection){
                selected.toggleJoin(x, y);
                this.editor.repaint();
            } else if(selected instanceof DrawingBlock){
                this.editor.wfeditor.showBlockEditor(selected);
            }
        }

    };
    this.canvas.ondblclick = dblClickHandler;

    // Mouse Down handler
    var mouseDownHandler = function(evt){
        var evt2 = this.editor.getEvent(evt);
        var dp = this.editor.correctCoordinates(evt2);
        var x = dp.x;
        var y = dp.y;
        var block;
        var selected;
        var p;
	
        var selectedPort = this.drawing.getPortAtPoint(x, y);
        if(selectedPort!=null && selectedPort.canAcceptConnections()){
            block = selectedPort.parentBlock;
            this.drawing.linkStart = block.locatePortConnectionPoint(selectedPort);
            this.drawing.linkStartPort = selectedPort;
            this.drawing.linkEnd = p;

            this.editor.currentState = CONNECTING_PORTS;
            this.editor.mouseDown = true;
            this.drawing.clearSelectionRectangle();
            this.drawing.highlightPortDataType = selectedPort.getDefaultType();
            if(selectedPort.type=="input"){
                this.drawing.highlightPortType = "output";
            } else {
                this.drawing.highlightPortType = "input";
            }

        } else {
            this.drawing.linkStart = null;
            this.drawing.linkStartPort = null;
            this.drawing.highlightPortDataType = "";

            // Check to see if a block property editor has been clicked
            var inMenu = this.drawing.menu.withinBounds(x, y);
            if(inMenu && this.drawing.menuVisible){
                this.drawing.menu.executeCommandByCoOrdinate(x, y);

            } else {

                selected = this.drawing.getObjectAtLocation(x, y);
                if(selected!=null){
                    this.drawing.clearSelectionRectangle();
                    this.editor.xStart = x;
                    this.editor.yStart = y;

                    if(evt2.shiftKey==false && this.drawing.isObjectSelected(selected)==false){
                        this.drawing.clearSelectionList();
                    }
                    this.drawing.addToSelectionList(selected);

                    this.editor.currentState = DRAGGING_OBJECTS;
                    this.editor.mouseDown = true;


                } else {

                    this.drawing.clearSelectionList();
                    this.editor.currentState = DRAWING_BOX;
                    this.drawing.boxStart = new DrawingPoint(x, y);
                    this.drawing.boxEnd = new DrawingPoint(x, y);
                }
            }
            this.editor.displayBlockCaption();
            this.editor.repaint();
        }
        return false;
    }
    this.canvas.onmousedown = mouseDownHandler;

    // Mouse up handler
    var mouseUpHandler = function(evt){
        var dp = this.editor.correctCoordinates(this.editor.getEvent(evt));

        // Try and connect some ports
        if(this.editor.currentState==CONNECTING_PORTS){
            var sourcePort = this.drawing.linkStartPort;
            var targetPort = this.drawing.getPortAtPoint(dp.x, dp.y);
            if(sourcePort!=null && targetPort!=null){
                if(targetPort.canAcceptConnections() && sourcePort.canAcceptConnections() && targetPort.canConnectToPort(sourcePort)){
                    if(targetPort!=null && targetPort.type!=sourcePort.type && targetPort.block!=sourcePort.block){
                        this.drawing.joinPorts(sourcePort, targetPort);
                        this.wfeditor.dirtyPerformanceData();
                    }
                }
            }

            this.drawing.clearSelectionList();

        } else if(this.editor.currentState==DRAWING_BOX){
            this.drawing.selectAllObjectsInSelectionBox();
        }

        this.editor.mouseDown = false;
        this.editor.currentState = EDITOR_IDLE;
        this.drawing.linkStart = null;
        this.drawing.linkEnd = null;
        this.drawing.linkStartPort = null;
        this.drawing.clearSelectionRectangle();
        this.drawing.highlightPortDataType ="";
        this.editor.displayBlockCaption();
        this.editor.repaint();

    }
    this.canvas.onmouseup = mouseUpHandler;

    // Mouse down handler
    var mouseMoveHandler = function(evt){
        if(!this.drawing.displayingReports){
            var dp = this.editor.correctCoordinates(this.editor.getEvent(evt));

            if(this.editor.currentState==DRAGGING_OBJECTS && this.editor.mouseDown==true){
                var xDelta = dp.x - this.editor.xStart;
                var yDelta = dp.y - this.editor.yStart;

                if(this.drawing.selectedObjects.size()==1){
                    // Move a single object. Blocks are handled differently to connections
                    if(this.drawing.selectedObjects.get(0) instanceof DrawingConnection){
                        this.drawing.selectedObjects.get(0).dragPoint(this.editor.xStart, this.editor.yStart, xDelta, yDelta);
                    } else {
                        this.drawing.selectedObjects.get(0).moveBy(xDelta, yDelta);
                    }

                } else {
                    this.drawing.moveSelectedObjects(xDelta, yDelta);
                }

                // Reset the start points
                this.editor.xStart = dp.x;
                this.editor.yStart = dp.y;

                this.editor.repaint();

            } else if(this.editor.currentState==CONNECTING_PORTS && this.editor.mouseDown==true){
                this.drawing.linkEnd = new DrawingPoint(dp.x, dp.y);
                this.editor.repaint();
            } else if(this.editor.currentState==DRAWING_BOX ){
                this.drawing.boxEnd = new DrawingPoint(dp.x, dp.y);

                this.editor.repaint();
            }
        }
    };
    this.canvas.onmousemove = mouseMoveHandler;


    var touchStartHandler = function(evt)
    {
      if (evt.touches.length == 1)
      {
        var touch = evt.touches[0];
        var dp = this.editor.correctTouchCoordinates(touch);

        this.touchStartX = dp.x;
        this.touchStartY = dp.y;
        var p;

        var selectedPort = this.drawing.getPortAtPoint(dp.x, dp.y);
        var selected = this.drawing.getObjectAtLocation(this.touchStartX, this.touchStartY);
        var inMenu = this.drawing.menu.withinBounds(dp.x, dp.y);

        if (selectedPort != null && selectedPort.canAcceptConnections())
        {
          evt.preventDefault();
          var block = selectedPort.parentBlock;
          this.drawing.linkStart = block.locatePortConnectionPoint(selectedPort);
          this.drawing.linkStartPort = selectedPort;
          this.drawing.linkEnd = p;

          this.editor.currentState = CONNECTING_PORTS;
          this.editor.mouseDown = true;
          this.drawing.clearSelectionRectangle();
          this.drawing.highlightPortDataType = selectedPort.getDefaultType();
          if (selectedPort.type == "input")
          {
            this.drawing.highlightPortType = "output";
          }
          else
          {
            this.drawing.highlightPortType = "input";
          }
        }
        // Check to see if a block property editor has been clicked
        else if (inMenu && this.drawing.menuVisible)
        {
          this.drawing.menu.executeCommandByCoOrdinate(dp.x, dp.y);
        }
        else if (selected != null)
          {
            evt.preventDefault();

            this.drawing.clearSelectionList();
            this.editor.currentState = DRAGGING_OBJECTS;

            this.drawing.addToSelectionList(selected);
            this.editor.displayBlockCaption();
            this.editor.repaint();
          }
          else
          {
            this.drawing.clearSelectionList();
            this.editor.repaint();
          }
      }
    };
    this.canvas.ontouchstart = touchStartHandler;


    var touchMoveHandler = function(evt)
    {
      if (evt.touches.length == 1)
      {
        var touch = evt.changedTouches[0];
        var dp = this.editor.correctTouchCoordinates(touch);

        if (this.editor.currentState === DRAGGING_OBJECTS)
        {
          evt.preventDefault();

          var xDelta = dp.x - this.touchStartX;
          var yDelta = dp.y - this.touchStartY;

          //reset the starting point
          this.touchStartX = dp.x;
          this.touchStartY = dp.y;

          this.drawing.selectedObjects.get(0).moveBy(xDelta, yDelta);

          this.editor.repaint();
        }
        else if (this.editor.currentState == CONNECTING_PORTS && this.editor.mouseDown == true)
        {
          this.drawing.linkEnd = new DrawingPoint(dp.x, dp.y);
          this.editor.repaint();
        }
      }
    };

    this.canvas.ontouchmove = touchMoveHandler;


    var touchEndHandler = function(evt)
    {
      if (evt.changedTouches.length == 1)
      {
        var touch = evt.changedTouches[0];
        var dp = this.editor.correctTouchCoordinates(touch);
        // Try and connect some ports
        if (this.editor.currentState == CONNECTING_PORTS)
        {
          var sourcePort = this.drawing.linkStartPort;
          var targetPort = this.drawing.getPortAtPoint(dp.x, dp.y);
          if (sourcePort != null && targetPort != null)
          {
            if (targetPort.canAcceptConnections() && sourcePort.canAcceptConnections() && targetPort.canConnectToPort(sourcePort))
            {
              if (targetPort != null && targetPort.type != sourcePort.type && targetPort.block != sourcePort.block)
              {
                this.drawing.joinPorts(sourcePort, targetPort);
              }
            }
          }
          this.drawing.clearSelectionList();
        }

        this.editor.currentState = EDITOR_IDLE;
        this.drawing.linkStart = null;
        this.drawing.linkEnd = null;
        this.drawing.linkStartPort = null;
        this.drawing.highlightPortDataType = "";
//        this.editor.displayBlockCaption();
        this.editor.repaint();

      }
    };
    this.canvas.ontouchend = touchEndHandler;

};

DrawingEditor.prototype.correctCoordinates_old = function(evt){
    if(evt!=null){
        if(evt.pageX){
            return new DrawingPoint(evt.pageX - this.canvas.offsetLeft, evt.pageY - this.canvas.offsetTop);
        } else {
            return new DrawingPoint(evt.clientX - this.canvas.offsetLeft ,evt.clientY - this.canvas.offsetTop);
        }
    } else {
        return new DrawingPoint(0,0);
    }
};

/** Add a block to this drawing */
DrawingEditor.prototype.addBlock = function(id, e){
    var p = this.correctCoordinates(e);
    this.wfeditor.addBlock(id, p.y - 30, p.x - 30);
};

    /*
    * The position returned for bo in correctCoordinates is different on touch iPad than browsers and
    * needs the window.pageOffset removing.
    * */
DrawingEditor.prototype.correctTouchCoordinates = function(e){
  var screenCoordinates = this.correctCoordinates(e);
  var touchCoordinates  = [];
  touchCoordinates.x = screenCoordinates.x -= window.pageXOffset;
  touchCoordinates.y = screenCoordinates.y -= window.pageYOffset;
  return touchCoordinates;
};

DrawingEditor.prototype.correctCoordinates = function(e){
    var c = this.canvasContext;
	if(c) {
		var bo = this.getpos(this.canvas);
		var x = e.clientX - bo.x;// + wsp.scrollLeft;	//correct for canvas position, workspace scroll offset
		var y = e.clientY - bo.y;// + wsp.scrollTop;
    //  x -= window.pageXOffset;	//correct for window scroll offset
    //  y -= window.pageYOffset;
    x = (c.zoom) ? x/c.zoom : x; 	//correct for zoom
		y = (c.zoom) ? y/c.zoom : y;
		return new DrawingPoint(x, y); //-.5 prevents antialiasing of stroke lines
	}
};

DrawingEditor.prototype.getpos = function(o) {
//gets position of object o
	var bo, x, y, b;x = y = 0;
	if(document.getBoxObjectFor) {	//moz
		bo = document.getBoxObjectFor(o);
		x = bo.x;y = bo.y;
	} else if (o.getBoundingClientRect) { //ie (??)
		bo = o.getBoundingClientRect();
		x = bo.left;y = bo.top;
	} else { //opera, safari etc
		while(o && o.nodeName != 'BODY') {
			x += o.offsetLeft;
			y += o.offsetTop;
			b = parseInt(document.defaultView.getComputedStyle(o,null).getPropertyValue('border-width'));
			if(b > 0) {x += b;y +=b;}
			o = o.offsetParent;
		}
	}
	return {x:x, y:y}
}

DrawingEditor.prototype.displayBlockCaption = function(){
    
}

DrawingEditor.prototype.displayBlockCaption2 = function()
{   
  if (this.drawing.invocationId !== null)
  {
    if (this.drawing.selectedObjects.size() == 1)
    {
      var selected = this.drawing.selectedObjects.get(0);
      if (selected instanceof DrawingBlock)
      {

        var callData = {
          contextId: selected.guid,
          invocationId: selected.parentDrawing.invocationId
        };
        var callString = JSON.stringify(callData);

        $.ajax({
          type: 'POST',
          url: rewriteAjaxUrl("../../servlets/workflow?method=fetchBlockExecutionOutput"),
          data: callString,
          xhrFields: {
              withCredentials: true
          },           
          dataType: "json",
          success: function(data)
          {
            if (data.outputText !== "" || data.statusMessage !== "")
            {
              alert(data.statusMessage + "\n\n" + data.outputText);
            }
          }
        });
       }
    }
  }
};

DrawingEditor.prototype.getEvent = function(evt){
    if(window.event){
        return window.event;
    } else {
        return evt;
    }
};