/* 
 * This class provides the core of the interactive data shell
 */
function DrawingBlockDebugger(){
    this.divName = "";
    this.div = null;
    this.resultsArea = null;
    this.commandArea = null;
    this.invocationId = null;
    this.contextId = null;
    this.debugOutput = "";
    this.fetching = false;
    this.allowAutoRefresh = true;
    this.refreshEnabled = true;
    this.timeoutFunction = null;
    this.timeoutVar = null;
    this.timeoutInterval = 500;
    this.maxTimeoutInterval = 10000;
    this.commandHistory = new Array();
}

/** Initialise a data shell dialog as opposed to a div */
DrawingBlockDebugger.prototype.init = function(divName){
    this.divName = divName;
    this.div = document.getElementById(this.divName);
    this.createUI();
    var dialog = this;
    
    $('#' + divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 550,
        width: 800,
        title: "Block debugger",
        modal: true,
        buttons: {
            Close: function(event, ui){
                dialog.stopDebugging();
            }
        }
    });
    
    $('#' + divName).bind("dialogresize", function(){
        dialog.resizeUI();
    });
    
    this.timeoutFunction = function(){
        if(dialog.allowAutoRefresh){
            dialog.fetchLastDebugBuffer();
        }
    };

    var bp = $("#" + this.divName).parent(".ui-dialog").children(".ui-dialog-buttonpane")[0];
    var statusDiv = document.createElement("div");
    statusDiv.setAttribute("class", "ui-widget");
    statusDiv.setAttribute("style", "padding: 10px;");
    statusDiv.appendChild(document.createTextNode("Status: "));
    var statusLabel = document.createElement("span");
    statusLabel.setAttribute("id", this.divName + "_status_label");
    statusLabel.appendChild(document.createTextNode("Ok"));
    bp.appendChild(statusLabel);
    this.resizeUI();
};

DrawingBlockDebugger.prototype.isOpen = function(){
    if($('#' + this.divName).dialog('isOpen')==true){
        return true;
    } else {
        return false;
    }
};

DrawingBlockDebugger.prototype.clearDisplay = function(){
    this.debugOutput = "";
    this.resultsArea.innerHTML = "<pre></pre>";
}

DrawingBlockDebugger.prototype.startRefreshing = function(timeout){
    this.refreshEnabled = true;
    this.allowAutoRefresh = true;
    this.debugOutput = "";
    this.showStatusText("");
    if(timeout){
        this.timeoutVar = setTimeout(this.timeoutFunction, timeout);
    } else {
        this.timeoutVar = setTimeout(this.timeoutFunction, this.getNextTimeoutInterval());
    }
};

DrawingBlockDebugger.prototype.showDialogWithDebugging = function(block, invocationId){
    $('#' + this.divName).dialog('open');
    this.resizeUI();
    this.contextId = block.guid;
    this.resultsArea.innerHTML = "Fetching...";
    this.showStatusText("");
    this.commandHistory = new Array();
    if(invocationId){
        this.invocationId = invocationId;
    } else {
        this.invocationId = block.parentDrawing.invocationId;
    }
    this.startRefreshing();
};

DrawingBlockDebugger.prototype.showDialogWithOutput = function(block){
    this.resultsArea.innerHTML = "Fetching...";
    this.refreshEnabled = false;
    this.allowAutoRefresh = false;
    
    $('#' + this.divName).dialog('open');
    this.resizeUI();
    
    var callData = {
        contextId: block.guid,
        invocationId: block.parentDrawing.invocationId
    };
    var callString = JSON.stringify(callData);
    var dialog = this;
    $.ajax({
        type: 'POST',
        url: rewriteAjaxUrl("../../servlets/workflow?method=fetchBlockExecutionOutput"),
        data: callString,
        xhrFields: {
            withCredentials: true
        },         
        dataType: "json",
        success: function(data)
        {
            if(!data.error){
                if (data.outputText !== "" || data.statusMessage !== "")
                {
                    dialog.resultsArea.innerHTML = "<pre>" + data.outputText + "</pre>";
                }
                dialog.showStatusText("Ok");
            } else {
                dialog.showStatusText(data.message);
            }
        }
    });    
};

DrawingBlockDebugger.prototype.showStatusText = function(text){
    var folderNameLabel = document.getElementById(this.divName + "_status_label");
    folderNameLabel.innerHTML = text; 
};

/** Resize the UI so that it fits on the dialog */
DrawingBlockDebugger.prototype.resizeUI = function(){
    var divHeight;
    var divWidth;
    divHeight = this.div.clientHeight;
    divWidth = this.div.clientWidth;
    this.resultsArea.style.height = (divHeight - 80 - this.commandArea.clientHeight) + "px" ;
    this.commandArea.style.width = (this.resultsArea.clientWidth - 10) + "px";
};

DrawingBlockDebugger.prototype.fetchLastDebugBuffer = function(callback){
    if(!this.fetching && this.refreshEnabled){
        this.fetching = true;
        var callData = {
            contextId: this.contextId,
            invocationId: this.invocationId
        };
        var callString = JSON.stringify(callData);
        var dialog = this;
        this.showStatusText("Fetching data...");
        $.ajax({
            type: 'POST',
            url: rewriteAjaxUrl("../../servlets/workflow?method=getLastDebugBuffer"),
            data: callString,
            xhrFields: {
                withCredentials: true
            },             
            dataType: "json",
            success: function(data)
            {
                if(!data.error){
                    if (data.buffer!=undefined && !data.buffer=="") {
                        dialog.debugOutput = dialog.debugOutput + data.buffer;
                        dialog.resultsArea.innerHTML = "<pre>" + dialog.debugOutput + "</pre>";
                        dialog.resultsArea.scrollTop = dialog.resultsArea.scrollHeight;                   
                    }
                    if(dialog.allowAutoRefresh){
                        dialog.timeoutVar = setTimeout(dialog.timeoutFunction, dialog.getNextTimeoutInterval());
                    }                
                    dialog.showStatusText("Ok");
                } else {
                    dialog.showStatusText(data.message);
                }
                dialog.fetching = false;
            },
            failure: function(){
                dialog.fetching = false;
            }
        });        
    }
};

DrawingBlockDebugger.prototype.stopDebugging = function(){
    this.allowAutoRefresh = false;
    this.refreshEnabled = false;
    clearTimeout(this.timeoutVar);

    var callData = {
        contextId: this.contextId,
        invocationId: this.invocationId
    };
    var callString = JSON.stringify(callData);
    var dialog = this;
    $.ajax({
        type: 'POST',
        url: rewriteAjaxUrl("../../servlets/workflow?method=closeDebugger"),
        data: callString,
        xhrFields: {
            withCredentials: true
        },         
        dataType: "json",
        success: function(data) {
            $('#' + dialog.divName).dialog('close');
        },
        failure: function(){
            $('#' + dialog.divName).dialog('close');
        }
    });       
};


/** Place all of the ui elements onto a div */
DrawingBlockDebugger.prototype.createUI = function(){
    if(this.div){
        // Toolbar
        var toolbarDiv = document.createElement("div");
        toolbarDiv.setAttribute("class", "navbar clearfix");
        toolbarDiv.setAttribute("style", "margin-bottom: 0px;");

        var toolbarContainer = document.createElement("div");
        toolbarContainer.setAttribute("class", "navbar-inner");
        toolbarContainer.setAttribute("style", "padding-left: 0px;");

        var toolbarMenu = document.createElement("ul");
        toolbarMenu.setAttribute("class", "nav clearfix");
    
    
        var exec = document.createElement("li");
        var execButton = document.createElement("a");
        execButton.setAttribute("id", this.divName + "_exec");
        execButton.innerHTML = "<i class='icomoon-play-3'></i>Exec";
        exec.appendChild(execButton);
        toolbarMenu.appendChild(exec);     

        var stop = document.createElement("li");
        var stopButton = document.createElement("a");
        stopButton.setAttribute("id", this.divName + "_stop");
        stopButton.innerHTML = "<i class='icomoon-exit'></i>Quit";
        stop.appendChild(stopButton);
        toolbarMenu.appendChild(stop);
        
        var kill = document.createElement("li");
        var killButton = document.createElement("a");
        killButton.setAttribute("id", this.divName + "_kill");
        killButton.innerHTML = "<i class='icomoon-switch'></i>Kill";
        kill.appendChild(killButton);
        toolbarMenu.appendChild(kill);
        
        var clear = document.createElement("li");
        var clearButton = document.createElement("a");
        clearButton.setAttribute("id", this.divName + "_clear");
        clearButton.innerHTML = "<i class='icomoon-checkbox-unchecked'></i>Clear";
        clear.appendChild(clearButton);
        toolbarMenu.appendChild(clearButton);
        
        toolbarDiv.appendChild(toolbarContainer);
        toolbarContainer.appendChild(toolbarMenu);
        
        // Results area
        this.resultsArea = document.createElement("div");
        this.resultsArea.setAttribute("class", "ui-corner-all");
        this.resultsArea.setAttribute("style", "height: 270px;margin-bottom: 5px; margin-top: 5px; overflow:auto;border-width: 1px; border-style: solid; border-color: lightgrey;");
        this.resultsArea.setAttribute("disabled", "disabled");
        
        this.commandArea = document.createElement("input");
        this.commandArea.setAttribute("class", "ui-corner-all");
        this.commandArea.setAttribute("style", "height: 20px; border-width: 1px; border-style: solid; border-color: lightgrey;");
        this.commandArea.setAttribute("type", "text");

        var shell = this;

        this.commandArea.onkeypress = function(e){

            // look for window.event in case event isn't passed in
            if (window.event) {
                e = window.event;
            }
            if (e.keyCode == 13) {
              var cmd = shell.commandArea.value;
              shell.commandHistory[shell.commandHistory.length] = cmd;

              shell.resultsArea.innerHTML = shell.resultsArea.innerHTML + '<p style="color:blue;">> ' + cmd + '</p>\n';
              shell.sendCommand(cmd);
                
              shell.resultsArea.scrollTop = shell.resultsArea.scrollHeight;
              shell.commandArea.value = "";
            }

        };

        this.div.appendChild(toolbarDiv);
        this.div.appendChild(this.resultsArea);
        this.div.appendChild(this.commandArea);

        killButton.onclick = function(){
            shell.sendCommand("_++TERM");
        }
        
        execButton.onclick = function(){
            shell.sendCommand("_++EXEC");
        };
        

        stopButton.onclick = function(){
            shell.sendCommand("_++QUIT");
        };

        clearButton.onclick = function(){
            shell.clearDisplay();
        }
    }

};

DrawingBlockDebugger.prototype.sendCommand = function(cmd){
    var callData = {
        contextId: this.contextId,
        invocationId: this.invocationId,
        command: cmd

    };
    var callString = JSON.stringify(callData);
    var dialog = this;
    $.ajax({
        type: 'POST',
        url: rewriteAjaxUrl("../../servlets/workflow?method=sendDebugCommand"),
        data: callString,
        xhrFields: {
            withCredentials: true
        },         
        dataType: "json",
        success: function(data) {
            dialog.resultsArea.scrollTop = dialog.resultsArea.scrollHeight;
            clearTimeout(dialog.timeoutVar);
            dialog.timeoutInterval = 500;
            dialog.fetchLastDebugBuffer();
        }
    });       
};

DrawingBlockDebugger.prototype.getNextTimeoutInterval = function(){
    var interval = this.timeoutInterval;
    this.timeoutInterval = this.timeoutInterval * 2;
    if(this.timeoutInterval>this.maxTimeoutInterval){
        this.timeoutInterval = this.maxTimeoutInterval;
    }
    return interval;
};