// Port location constants 
LEFT_OF_BLOCK = 0;
TOP_OF_BLOCK = 1;
RIGHT_OF_BLOCK = 2;
BOTTOM_OF_BLOCK = 3;

// Property type constants
INTEGER_PROPERTY = "integer";
LONG_PROPERTY = "long";
STRING_PROPERTY = "string";
FILE_PROPERTY = "file";
FOLDER_PROPERTY = "folder";
DATE_PROPERTY = "date";

// Drawing state constants
EDITOR_IDLE = 0;
DRAGGING_OBJECTS = 1;
DRAWING_BOX = 2;
CONNECTING_PORTS = 3;
INPUT_PORT_SIZE = 5;
OUTPUT_PORT_SIZE = 4;
INPUT_PORT_HIGHLIGHT_SIZE = 6;
OUTPUT_PORT_HIGHLIGHT_SIZE = 5;
PROGRESS_PIE_RADIUS = 6;
PROGRESS_PIE_MARGIN = 12;

// Drawing tolerances
PORT_HIT_DISTANCE = 15;
CONNECTION_HIT_DISTANCE = 10;
SELECTION_HANDLE_SIZE = 4;
LINK_SELECTION_HANDLE_SIZE = 7;
SELECTED_LINK_WIDTH = 1.1;
NON_SELECTED_LINK_WIDTH = 1.1;
CONNECTING_LINE_WIDTH = 1.1;

// DRAWING COLORS
DRAWING_BACKGROUND_COLOR = "rgb(255,255,255)";
BLOCK_FILL_COLOR = "rgb(255,255,255)";
BLOCK_SELECTION_HANDLE_COLOR = "rgb(0,0,128)";
BLOCK_BORDER_COLOR = "rgb(0,0,0)";
BLOCK_SHADOW_COLOR = "rgb(150,150,150)";
BLOCK_TITLE_COLOR = "rgb(0, 0, 0)";
BLOCK_VERSION_TEXT_COLOR = "rgb(0,0,64)";
INPUT_PORT_COLOR = "rgb(32,32,32)";
INPUT_PORT_HIGHLIGHT_COLOR = "rgb(0,128,0)";
OUTPUT_PORT_COLOR = "rgb(32,32,32)";
OUTPUT_PORT_HIGHLIGHT_COLOR = "rgb(0,128,0)";
NON_SELECTED_LINK_COLOR = "rgb(32,32,32)";
SELECTED_LINK_COLOR = "rgb(0,0,128)";
CONNECTING_LINK_COLOR = "rgb(0,0,0)";
CONNECTING_CIRCLE_ALPHA = 0.5;

LINK_LABEL_COLOR = "rgb(0,0,0)";
//BLOCK_IMAGE_FILE="button.png";
BLOCK_IMAGE_FILE="button.jpg";
SELECTION_BOX_ALPHA = 0.4;
SELECTION_BOX_COLOR = "rgb(0,0,128)";
DISABLED_DRAWING_BACKGROUND_COLOR = "rgb(210,210,210)";
DRAWING_CONTEXT_MENU_BORDER_COLOR = "rgb(128,128,128)";
DRAWING_CONTEXT_MENU_FILL_COLOR = "rgb(240,240,240)";
DRAWING_PORT_ICON_ALPHA = 0.9;

// Colors for links
FILE_LINK_COLOR = "rgb(153,102,0)";
DATA_LINK_COLOR = "rgb(32,32,32)";
REFERENCE_LINK_COLOR = "rgb(0,102,51)";
PROPERTIES_LINK_COLOR = "rgb(51,0,153)";
OBJECT_LINK_COLOR = "rgb(51,51,0)";
CONTROL_DEP_LINK_COLOR = "rgb(150,50,50)";

// Text constants
MULTI_ROW_HEIGHT = 16;

// Invocation status flags
INVOCATION_WAITING = 0;
INVOCATION_RUNNING = 1;
INVOCATION_FINISHED_OK = 2;
INVOCATION_FINISHED_WITH_ERRORS = 3;