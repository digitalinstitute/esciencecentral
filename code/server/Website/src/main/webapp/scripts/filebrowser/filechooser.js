/* 
 * This script provides a file browser implementation that can be placed
 * onto any page.
 * ""
 */


//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-treeview/jquery.treeview.css">
//<script type="text/javascript" src="../../scripts/jquery-treeview/jquery.treeview.js"></script>
//<script type="text/javascript" src="../../scripts/filebrowser/filetree.async.js"></script>


function FileChooser(){
    this.divName = "";                      // Name of the div that will hold the dialog
    this.title = "Select a file";           // Title to show in dialog
    this.fileSelectCallback = null;         // Callback when a file is clicked
    this.folderSelectCallback = null;       // Callback when a folder is clicked
    this.fileDoubleClickCallback = null;    // Callback when a file is double clicked    
    this.okCallback = null;                 // Callback when the OK button is clicked
    this.cancelCallback = null;             // Callback when the Cancel button is clicked
    this.selectedFileId = null;             // ID of the last clicked file
    this.selectedFileName = null;
    this.selectedFolderId = null;           // ID of the last clicked folder
    this.selectedFolderName = null;
    this.selectedClassName = null;          // Classname of selected object
    this.fileList = new List();             // All of the file nodes in the tree
    this.cancelled = false;                 // Was the dialog cancelled
    this.dialogZIndex = 1000;               // Z-Index field for dialog
    this.creating = false;                  // Is this dialog creating a new file / folder
    this.folderMode = false;                // Is this dialog operating on folders
    this.servletUrl = rewriteAjaxUrl("../../servlets/filechooser");   // Location of the file browser servlet
}

FileChooser.prototype.init = function(divName){
    this.divName = divName;
    var c = this;
    
    // Set up Ok and Cancel functions
    var okFunction = function(){
        if(c!=null && c.okCallback){
            c.okCallback(c);
            c.cancelled = false
        }
        $(this).dialog('close');
    };

    var cancelFunction = function(){
        if(c!=null && c.cancelCallback){
            c.cancelCallback();
            c.cancelled = true;
        }
        $(this).dialog('close');
    }

    // Set up the dialog
    var div = document.getElementById(this.divName);
    var treeDiv = document.createElement("div");
    treeDiv.setAttribute("id", this.divName + "_tree");
    treeDiv.setAttribute("style", "overflow:auto; position: absolute; top: 50px; left:5px; width:100px; height: 100px; padding-top: 10px;");
    var toolbarDiv = document.createElement("div");
    toolbarDiv.setAttribute("id", this.divName + "_toolbar");
    toolbarDiv.setAttribute("style", "top:0px; left: 0px; width: 100%; height: 40px; border-bottom-width: 1px;border-bottom-style: solid;border-bottom-color: #AAA;");
    
    var table = document.createElement("table");
    var tr = document.createElement("tr");
    
    var td1 = document.createElement("td");
    td1.setAttribute("style", "padding-right: 5px;");
    var caption = document.createElement("div");
    caption.setAttribute("style", "font-size: 12px; position: relative; top: -5px; font-weight: bold;");
    if(this.folderMode){
        caption.appendChild(document.createTextNode("Selected folder: "));
    } else {
        caption.appendChild(document.createTextNode("Selected file: "));
    }
    td1.appendChild(caption);
    
    var td2 = document.createElement("td");
    var fileName = document.createElement("input");
    fileName.setAttribute("type", "text");
    fileName.setAttribute("style", "width: 250px; height:24px;font-size: 12px;border-width:1px;");
    fileName.setAttribute("class", "ui-corner-all");
    fileName.setAttribute("id", this.divName + "_filename");
    if(this.creating){
        fileName.disabled = false;
    } else {
        fileName.disabled = true;
    }
    td2.appendChild(fileName);
    
    var td3 = document.createElement("td");
    td3.setAttribute("id", this.divName + "_root_title");
    td3.setAttribute("style", "padding-left: 10px; padding-right: 5px;");
    
    
    var td4 = document.createElement("td");
    td4.setAttribute("id", this.divName + "_root_list");
    
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);
    table.appendChild(tr);
    toolbarDiv.appendChild(table);
    div.appendChild(toolbarDiv);
    div.appendChild(treeDiv);
    
    var dialog = this;
    $("#" + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 500,
        title: this.title,
        width: 750,
        zIndex: this.dialogZIndex,
        chooser: this,
        modal: true,
        buttons: {
                'Ok': okFunction,
                'Cancel' : cancelFunction
        },
        resize: function(event, ui){
            dialog.resize();
        }
    });
    
    // Add some text in the dialog
    var bp = $("#" + this.divName).parent(".ui-dialog").children(".ui-dialog-buttonpane")[0];
    var folderNameDiv = document.createElement("div");
    folderNameDiv.setAttribute("class", "ui-widget");
    folderNameDiv.setAttribute("style", "padding: 10px;");
    folderNameDiv.appendChild(document.createTextNode("Folder: "));
    var folderNameLabel = document.createElement("span");
    folderNameLabel.setAttribute("id", this.divName + "_folder_name_label");
    folderNameDiv.appendChild(folderNameLabel);
    bp.appendChild(folderNameDiv);
    
    this.resetFileTree();
    this.resize();
};

FileChooser.prototype.resize = function(){
    var div = document.getElementById(this.divName);
    var treeDiv = document.getElementById(this.divName + "_tree");
    treeDiv.style.width = (div.clientWidth - 5) + "px";
    treeDiv.style.height = (div.clientHeight - 60) + "px";
};

FileChooser.prototype.closeDialog = function(){
    jQuery("#" + this.divName).dialog('close');
};

FileChooser.prototype.showDialog = function(){
    this.cancelled = false;
    this.selectedFileId = null;
    this.selectedFolderId = null;
    this.selectedFileName = null;
    this.selectedFolderName = null;
    this.displayFileOrFolderDetails();
    jQuery("#" + this.divName).dialog('open');
    this.resize();
};

FileChooser.prototype.displayFileOrFolderDetails = function(){
    var filenameLabel;
    if(!this.folderMode){
        if(!this.creating){
            filenameLabel = document.getElementById(this.divName + "_filename");
            filenameLabel.value = this.selectedFileName;
        } else {
            // Don't set anything if creating
        }
    } else {
        if(!this.creating){
            filenameLabel = document.getElementById(this.divName + "_filename");
            filenameLabel.value = this.selectedFolderName;
        } else {
            // Don't set any name if files are being created
        }
    }    
    if(this.selectedFolderName){
        var folderNameLabel = document.getElementById(this.divName + "_folder_name_label");
        folderNameLabel.innerHTML = this.selectedFolderName;
    }
};

FileChooser.prototype.resetFileTree = function(){
    var html = '<ul id="_filechoosertreediv_' + this.divName + '" class="filetree"></ul>';
    this.fileList.clear();
    var div = document.getElementById(this.divName + "_tree");
    if(div!=null){
        div.innerHTML = html;
    }
};

FileChooser.prototype.resetSelection = function(){
    this.selectedFileId = null;
    this.selectedFileName = null;
    this.selectedFolderId = null;
    this.selectedFileName = null;
};

FileChooser.prototype.showHomeFolder = function(creating){
    if(creating!=undefined){
        this.creating = creating;
    }
  
    this.resetSelection();
    this.resetFileTree();
    this.fileList.clear();
    $("#_filechoosertreediv_" + this.divName).treeview({
            url: this.servletUrl,
            chooser: this
    });
    this.showDialog();
    
    var fileName = document.getElementById(this.divName + "_filename");
    fileName.value = "";
    if(this.creating){
        fileName.disabled = false;
    } else {
        fileName.disabled = true;
    }          
    this.fetchRootFolderList();
};

FileChooser.prototype.showFolder = function(folderId, creating, skipRootFolders){
    if(creating!=undefined){
        this.creating = creating;
    }
    
    this.resetSelection();
    this.resetFileTree();
    this.fileList.clear();
    $("#_filechoosertreediv_" + this.divName).treeview({
            url: this.servletUrl,
            chooser: this,
            root: folderId
    });
    this.showDialog();    
    
    var fileName = document.getElementById(this.divName + "_filename");
    fileName.value = "";
    if(this.creating){
        fileName.disabled = false;
    } else {
        fileName.disabled = true;
    }     
    
    if(!skipRootFolders){
        this.fetchRootFolderList();
    }
};

FileChooser.prototype.setSelectedFileId = function(fileId, fileName){
    this.selectedFileId = fileId;
    this.selectedFileName = fileName;
    this.displayFileOrFolderDetails();
    if(this.fileSelectCallback){
        this.fileSelectCallback(fileId, fileName);
    }
};

FileChooser.prototype.setSelectedFolderId = function(folderId, folderName){
    this.selectedFolderId = folderId;
    this.selectedFolderName = folderName;
    this.displayFileOrFolderDetails();
    if(this.folderSelectCallback){
        this.folderSelectCallback(folderId, folderName);
    }
};

FileChooser.prototype.doubleClickFileId = function(fileId, fileName){
    this.selectedFileId = fileId;
    this.selectedFileName = fileName;
    this.displayFileOrFolderDetails();
    if(this.fileDoubleClickCallback){
        this.fileDoubleClickCallback(fileId, fileName);
    }
}

FileChooser.prototype.addNodeToList = function(node){
    if(!this.fileList.contains(node)){
        this.fileList.add(node);
    }
};

FileChooser.prototype.highlightFileDiv = function(div){
    var i;
    var d;
    for(i=0;i<this.fileList.size();i++){
        d = this.fileList.get(i);
        if(d===div){
            // Highlight this one
            //d.style.backgroundColor = "#D64B1F";
            $(d).addClass("selected");
            
        } else {
            // Unhighlight this div
            //d.style.backgroundColor = "#FFFFFF";
            $(d).removeClass("selected");
            d.fileSelected = false;
        }
    }
};

/** Get the name entered in the text box at the top of the chooser */
FileChooser.prototype.getFileOrFolderName = function(){
    if(this.creating){
        var fileName = document.getElementById(this.divName + "_filename");
        if(fileName.value){
            return fileName.value.trim();
        } else {
            return null;
        }
    } else {
        throw "File chooser is not in creation mode";
    }
    
};

/** 
 * Fetch the list of root directories and display them in a list at the top of
 * the dialog box.
 */
FileChooser.prototype.fetchRootFolderList = function(){
    var chooser = this;
    var cb = function(o){
        var td3 = document.getElementById(chooser.divName + "_root_title");
        var td4 = document.getElementById(chooser.divName + "_root_list");

        var caption = document.createElement("div");
        caption.setAttribute("style", "font-size: 12px; position: relative; top: -5px; font-weight: bold;");
        if(this.folderMode){
            caption.appendChild(document.createTextNode("Data Source: "));
        } else {
            caption.appendChild(document.createTextNode("Data Source: "));
        }
        td3.innerHTML = "";
        td3.appendChild(caption);
        
        var select = document.createElement("select");
        select.setAttribute("id", chooser.divName + "_root_selector");
        select.setAttribute("style", "position: relative: top: -5px;");
        var option;
        var projectId = o.projectId;
        var indexToSelect = -1;
        
        for(var i=0;i<o.folders.length;i++){
            option = document.createElement("option");
            option.appendChild(document.createTextNode(o.folders[i].name));
            option.rootFolder = o.folders[i];
            if(o.folders[i].storageFolderId){
                option.storageFolderId = o.folders[i].storageFolderId;
            }
            if(o.folders[i].id===projectId){
                indexToSelect = i;
            }
            select.appendChild(option);
        }
        td4.innerHTML = "";
        td4.appendChild(select);
        
        // Set the correct selection
        if(indexToSelect!=-1){
            select.selectedIndex = indexToSelect;
        }
        
        select.onchange = function(){
            var option = this.options[this.selectedIndex];
            if(option.storageFolderId){
                chooser.showFolder(option.storageFolderId, chooser.creating, true);
            } else {
                chooser.showFolder(option.rootFolder.id, chooser.creating, true);
            }
        }
        

    }
    
    jsonCall({}, "../../servlets/viewer?method=getRootFolders", cb, null);
};
