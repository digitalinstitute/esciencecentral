/*
 * Async Treeview 0.1 - Lazy-loading extension for Treeview
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 *
 * Copyright (c) 2007 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id$
 *
 */

(function($) {

function load(settings, root, child, container) {
    var callback = function(response){
        function createNode(parent, settings) {
            var current;
            var baseDivId;
            if(settings && settings.chooser){
                baseDivId = settings.chooser.divName;
            } else {
                baseDivId = "";
            }
            
            
            
            // Addition to set selected file
            if(this.isFile){
                //current = $("<li/>").attr("id", this.id || "").html('<span><div id="sp_' + this.id + '" class="filebrowserfile" onclick="_appFileChooserSetSelectedFileId(\'' + this.id + '\')">' + this.text + '</div></span>').appendTo(parent);
                current = $("<li/>").attr("id",  this.id || "").html('<span><div id="sp_' + baseDivId + '_' + this.id + '" class="filebrowserfile">' + this.text + '</div></span>').appendTo(parent);
            } else {
                current = $("<li/>").attr("id",  this.id || "").html('<span id="sp_' + baseDivId + '_' + this.id + '">' + this.text + '</span>').appendTo(parent);
            }


            if (this.classes) {
                    current.children("span").addClass(this.classes);
            }

            var d = document.getElementById("sp_" + baseDivId + "_" + this.id);
            if(d!=null && settings!=null){
                if(settings && settings.chooser){
                    settings.chooser.addNodeToList(d);
                }
                
                d.div = d;
                d.fileId = this.id;
                d.fileName = this.text;
                d.containerId = this.containerId;
                d.chooser = settings.chooser;
                d.serverClassName = this.className;
                
                d.onclick = function(){
                    if(!this.fileSelected && this.chooser!=null){
                        if(this.isFile){
                            this.fileSelected = true;
                            this.chooser.setSelectedFileId(this.fileId, this.fileName);
                            this.chooser.setSelectedFolderId(this.containerId, "");
                            this.chooser.selectedClassName = this.serverClassName;
                            this.chooser.highlightFileDiv(this.div);

                        } else {
                            this.chooser.setSelectedFolderId(this.fileId, this.fileName);
                            this.chooser.highlightFileDiv(null);
                        }
                    }
                };
                
                d.ondblclick = function(){
                    if(this.isFile){
                        this.fileSelected = true;
                        this.chooser.setSelectedFolderId(this.containerId, "");
                        this.chooser.selectedClassName = this.serverClassName;
                        this.chooser.doubleClickFileId(this.fileId, this.fileName);
                        this.chooser.highlightFileDiv(this.div);
                    }
                };
                
                d.fileSelected = false;
                d.current = current;
                d.obj = this;
                d.isFile = this.isFile;
            }

            if (this.expanded) {
                    current.addClass("open");
            }
            if (this.hasChildren || this.children && this.children.length) {
                    var branch = $("<ul/>").appendTo(current);
                    if (this.hasChildren) {
                            current.addClass("hasChildren");
                            createNode.call({
                                    text:"placeholder",
                                    id:"placeholder",
                                    children:[]
                            }, branch);
                    }
                    if (this.children && this.children.length) {
                            $.each(this.children, createNode, [branch])
                    }
            }
        }

        $.each(response, createNode, [child, settings]);
        $(container).treeview({add: child});
    };
    callback.settings = settings;
    //$.getJSON(settings.url, {root: root}, callback);
    $.ajax({
        dataType: "json",
        url: rewriteAjaxUrl(settings.url),
        data: {root: root},
        success: callback,
        xhrFields: {
            withCredentials: true
        }        
    });
    
}

var proxied = $.fn.treeview;
$.fn.treeview = function(settings) {
	if (!settings.url) {
		return proxied.apply(this, arguments);
	}
	var container = this;
        
        // Set correct root folder for the tree
        if(settings.root){
            load(settings, settings.root, this, container);
        } else {
            load(settings, "source", this, container);
        }
	
	var userToggle = settings.toggle;
	return proxied.call(this, $.extend({}, settings, {
		collapsed: true,
		toggle: function() {
			var $this = $(this);
			if ($this.hasClass("hasChildren")) {
				var childList = $this.removeClass("hasChildren").find("ul");
				childList.empty();
				load(settings, this.id, childList, container);
			}
			if (userToggle) {
				userToggle.apply(this, arguments);
			}
		}
	}));
};

})(jQuery);