/* 
 * This script provides a viewer that can list the files in a particular folder
 * and display each one in a viewer panel
 */

//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jgrowl/jquery.jgrowl.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>
//<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>


function FolderViewer(){
    this.viewerDivName = "";
    this.folderId = "";
    this.dialog = null;
    this.viewer = new ViewerPanel();
    this.folderJson = null;
    this.listDiv = null;
    
    this.fetchFolderCallback = function(o){
        if(!o.error){
            this.viewer.folderJson = o.folder;
            this.viewer.displayFiles(o.files);
        } else {
            $.jGrowl("Error fetching folder data: " + o.message);
        }
    };
}

/** Initialise with a div */
FolderViewer.prototype.init = function(viewerDivName){
    this.viewerDivName = viewerDivName;
    this.dialog = $("#" + this.viewerDivName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 600,
        width: 800,
        title: "Folder Contents",
        modal: true,
        viewer: this,
        buttons: {
            'Close': function()
            {
                $(this).dialog('close');
            }

        },
        close: function()
        {

        }
    });
    this.createUI();
};

/** Create the UI elements */
FolderViewer.prototype.createUI = function(){
    var viewerDiv = document.getElementById(this.viewerDivName);
    if(viewerDiv){
        viewerDiv.innerHTML = "";
        this.listDiv = document.createElement("ul");
        this.listDiv.setAttribute("class", "fileviewerlist");
        viewerDiv.appendChild(this.listDiv);

        var panelDiv = document.createElement("div");
        panelDiv.setAttribute("id", this.viewerDivName + "_viewerpanel");
        panelDiv.setAttribute("class", "fileviewerpanel");
        viewerDiv.appendChild(panelDiv);
        this.viewer.init(this.viewerDivName + "_viewerpanel");
    }
};

/** List the files in a folder */
FolderViewer.prototype.displayFiles = function(files){
    this.listDiv.innerHTML = "";
    var i;
    var li;
    var ul = this.listDiv;

    for(i=0;i<files.length;i++){
        li = document.createElement("li");
        li.setAttribute("class", "fileitem");
        li.appendChild(document.createTextNode(files[i].name));
        li.fileJson = files[i];
        li.viewer = this;
        li.onclick = function(){
            this.viewer.viewer.showFile(this.fileJson.id);
            
            // Unselect all the other child nodes
            for(var i in ul.childNodes){
                var node = ul.childNodes[i];
                if(node.fileJson){
                    if(node.fileJson.id==this.fileJson.id){
                        // Select
                        $(node).addClass("fileselected");
                    } else {
                        // Unselect
                        $(node).removeClass("fileselected");
                    }
                }
            }
            
        };
        this.listDiv.appendChild(li);
    }
};

/** Show contents of a folder */
FolderViewer.prototype.openFolder = function(folderId){
    this.folderId = folderId;
    this.listDiv.innerHTML = "";
    this.viewer.clear();
    $("#" + this.viewerDivName).dialog('open');
    
    var url = rewriteAjaxUrl("../../servlets/viewer?method=getFolderAndContents");
    var callData = {
        folderId: folderId
    };

    var callString = JSON.stringify(callData);

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: this.fetchFolderCallback,
      dataType: "json",
      viewer: this
    });
};