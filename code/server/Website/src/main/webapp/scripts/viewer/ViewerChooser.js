/* 
 * This script provides a dialog that lets the user associate applications
 * with a file type
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/XMLDisplay.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/viewer/viewer.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/datatables/css/datatable.css">
//<script type="text/javascript" src="../../scripts/viewer/MimeType.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/XMLDisplay.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/MimeTypeManager.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerChooser.js"></script>
//<script type="text/javascript" src="../../scripts/datatables/jquery.dataTables.min.js"></script>
//<script type="text/javascript" src="../../scripts/viewer/ViewerPanel.js"></script>

function ViewerChooser(){
    this.mimeTypes = new List();
    this.divName = null;
    this.mimeManager = new MimeTypeManager();
    this.extension = "";
    this.applicationArray = null;
    this.viewerForExtension = null;
    this.okButtonCallback = null;
    this.cancelButtonCallback = null;
    this.saveOption = "none";
}

ViewerChooser.prototype.init = function(divName){
    this.divName = divName;
    var vcdiv = document.getElementById(this.divName);
    vcdiv.chooser = this;
    
    var dialog = $("#" + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 350,
        width: 300,
        title: "Editor Chooser",
        modal: true,
        chooser: this,
        buttons: {
            'Ok' : function()
            {
                /*
                this.chooser.saveViewerPreference();
                if(this.chooser.okButtonCallback){
                    this.chooser.okButtonCallback();
                }
                */

                var ch = this.chooser;
                var callback = function(){
                    if(ch.okButtonCallback){
                        ch.okButtonCallback();
                    }
                }
                this.chooser.saveViewerPreference(callback);
                $(this).dialog('close');
            },
            'Cancel': function()
            {
                if(vcdiv.chooser.cancelButtonCallback){
                    vcdiv.chooser.cancelButtonCallback();
                }
                $(this).dialog('close');
            }
        },
        close: function()
        {

        }
    });
    
};

/** Set up the mime manager and fetch the applications available to view a given extension */
ViewerChooser.prototype.fetchMimeData = function(){
    var vc = this;
    var callback = function(){
        vc.fetchApplicationsForExtension();
    };
    this.mimeManager.initMimeMap();
    this.mimeManager.fetchMimeTypeData(callback);   // Set up the mime manager
};

ViewerChooser.prototype.fetchApplicationsForExtension = function(){
    var vc = this;
    var callback = function(applications){
        vc.applicationArray = applications;
        vc.showApplications();
    }
    this.mimeManager.listApplicationsForExtension(this.extension, callback);
};

ViewerChooser.prototype.showApplications = function(){
    // Is there an internal viewer
    var contentsDiv = document.getElementById(this.divName);
    var chooser = this;
    contentsDiv.innerHTML = "";
    var currentViewer = this.mimeManager.getMimeObjectByExtension(this.extension);

    var ul;
    var li;
    var option;

    var allOption;
    var rememberOption;
    var noSaveOption;

    ul = document.createElement("ul");

    li = document.createElement("li");
    li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px; margin-bottom: 5px;");
    li.appendChild(document.createTextNode("Options:"));
    ul.appendChild(li);

    li = document.createElement("li");
    li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px;");
    allOption = document.createElement("input");
    allOption.setAttribute("type", "radio");
    allOption.setAttribute("name", "options");
    allOption.setAttribute("value", "all");
    allOption.onclick = function(){
        chooser.saveOption = "all";
    }
    li.appendChild(allOption);
    li.appendChild(document.createTextNode("Use this viewer for all documents"));
    ul.appendChild(li);

    li = document.createElement("li");
    li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px;");
    rememberOption = document.createElement("input");
    rememberOption.setAttribute("type", "radio");
    rememberOption.setAttribute("name", "options");
    rememberOption.setAttribute("value", "document");
    rememberOption.onclick = function(){
        chooser.saveOption = "document";
    }
    li.appendChild(rememberOption);
    li.appendChild(document.createTextNode("Remember choice for this document"));
    ul.appendChild(li);

    li = document.createElement("li");
    li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px; margin-bottom: 8px;");
    noSaveOption = document.createElement("input");
    noSaveOption.setAttribute("type", "radio");
    noSaveOption.setAttribute("name", "options");
    noSaveOption.setAttribute("value", "none");
    noSaveOption.onclick = function(){
        if(this.checked){
            chooser.saveOption = "none";
        }
    }
    noSaveOption.checked = true;

    li.appendChild(noSaveOption);
    li.appendChild(document.createTextNode("Do not save options"));
    ul.appendChild(li);

    var builtinMimeType = this.mimeManager.getBuiltinMimeTypeByExtension(this.extension);
    if(builtinMimeType){
        li = document.createElement("li");
        li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px; margin-bottom: 5px;");
        li.appendChild(document.createTextNode("Built in viewers:"));
        ul.appendChild(li);

        li = document.createElement("li");
        li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px;");
        
        option = document.createElement("input");
        option.setAttribute("type", "radio");
        option.setAttribute("name", "editor");
        option.setAttribute("value", builtinMimeType.internalViewerName);
        option.mimeType = builtinMimeType;

        option.onclick = function(){
            chooser.viewerForExtension = this.mimeType;
            chooser.viewerForExtension.builtin = true;
        };

        if(currentViewer && currentViewer.extension==this.extension && currentViewer.internalViewer){
            option.checked = true;
        }
        
        li.appendChild(option);
        li.appendChild(document.createTextNode(builtinMimeType.internalViewerName + "(built in)"));
        ul.appendChild(li);
    }

    // List the applications that can view the file
    var i;
    var applicationJson;
    var size;

    if(this.applicationArray){
        li = document.createElement("li");
        li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px; margin-top: 15px; margin-bottom: 5px;");
        li.appendChild(document.createTextNode("Applications supporting (" + this.extension + "):"));
        ul.appendChild(li);
        
        size = this.applicationArray.length;
        for(i=0;i<size;i++){
            applicationJson = this.applicationArray[i];
            li = document.createElement("li");
            li.setAttribute("style", "list-style-type: none; list-style-position: outside; margin-left: 0px;");
            
            option = document.createElement("input");
            option.setAttribute("type", "radio");
            option.setAttribute("name", "editor");
            option.setAttribute("value", applicationJson.name);
            option.mimeType = applicationJson;

            option.onclick = function(){
                chooser.viewerForExtension = this.mimeType;
                chooser.viewerForExtension.builtin = false;
            };

            if(currentViewer){
                if(currentViewer.applicationId==applicationJson.id){
                    option.checked = true;
                }
            }

            li.appendChild(option);
            li.appendChild(document.createTextNode(applicationJson.name));
            ul.appendChild(li);
        }
    }

    contentsDiv.appendChild(ul);
};

ViewerChooser.prototype.show = function(extension){
    var contentsDiv = document.getElementById(this.divName);
    this.viewerForExtension = null;
    contentsDiv.innerHTML = "";
    this.extension = extension;
    $("#" + this.divName).dialog('open');
    this.fetchMimeData();
};

ViewerChooser.prototype.saveViewerPreference = function(callback){
    if(this.extension && this.viewerForExtension){
        if(this.saveOption=="all"){
            if(this.viewerForExtension.builtin){
                // Built-in viewer
                this.mimeManager.assignApplicationToMimeType(this.extension, "", callback);
            } else {
                // External application
                this.mimeManager.assignApplicationToMimeType(this.extension, this.viewerForExtension.id, callback);
            }
        } else if(this.saveOption=="document"){
            if(this.viewerForExtension.builtin){
                // Built-in viewer
                this.mimeManager.assignApplicationToDocument(this.documentId, "", callback);
            } else {
                // External application
                this.mimeManager.assignApplicationToDocument(this.documentId, this.viewerForExtension.id, callback);
            }
        } else {
            if(callback){
                callback();
            }
        }
    }
};