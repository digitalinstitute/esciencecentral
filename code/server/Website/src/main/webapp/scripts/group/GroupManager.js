/* 
 * This script manages events and subscriptions
 */
//Requires
//<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
function GroupManager(fileChooserDiv)
{
    this.groupId = null;
    if(fileChooserDiv){
        this.fileChooserDiv = fileChooserDiv;
    } else {
        this.fileChooserDiv = "filechooser";
    }
    
    var div = document.getElementById(this.fileChooserDiv);
    var mgr = this;
    if(div){
        this.fileChooser = new FileChooser();
        this.fileChooser.init(this.fileChooserDiv);
        this.fileChooser.okCallback = function(){
            if(!mgr.fileChooser.cancelled){
                var documentId = mgr.fileChooser.selectedFileId;
                var documentName = mgr.fileChooser.selectedFileName;
                var callback = function(){
                    $.jGrowl("Shared file: " + documentName);
                };
                jsonCall({id: documentId, groupId: mgr.groupId}, "../../servlets/group?method=shareFile", callback);
            }
        };
    }
}

/** Create an empty event for a group */
GroupManager.prototype.createGroupEvent = function(name, description, groupId, startDate, endDate)
{
    var eventJson = {
        name: name,
        groupId: groupId,
        description: description,
        startDate: startDate,
        endDate: endDate
    };
    return eventJson;
};

/** Delete a group event */
GroupManager.prototype.deleteGroupEvent = function(id, callback){
    var callData = {id: id};
    var url = "../../servlets/group?method=deleteGroupEvent";
    jsonCall(callData, url, callback);
};

/** Save an event for a group */
GroupManager.prototype.saveGroupEvent = function(eventJson, callback)
{
    eventJson.startDateMillis = eventJson.startDate.getTime();
    eventJson.endDateMillis = eventJson.endDate.getTime();
    var callData = {
        event: eventJson
    };
    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/group?method=saveGroupEvent");

    var cb = function(o)
    {
        if (!o.error)
        {
            // Execute the callback
            if (callback)
            {
                callback(o.event);
            }
        }
        else
        {
            $.jGrowl("Error saving event: " + o.message);
        }

    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        }, 
        data: callString,
        success: cb,
        dataType: "json",
        panel: this
    });

};

/** List all of the events for a group */
GroupManager.prototype.listGroupEvents = function(groupId, callback)
{
    this.documentId = documentId;
    this.clear();
    var callData = {
        groupId: groupId
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/group?method=listGroupEvents");

    var cb = function(o)
    {
        if (!o.error)
        {
            var events = o.events;
            var i;
            var event;
            var startDate;
            var endDate;

            for (i = 0; i < events.length; i++)
            {
                event = events[i];
                startDate = new Date(event.startDateMillis);
                endDate = new Date(end.endDateMillis);
                event.startDate = startDate;
                event.endDate = endDate;
            }

            // Execute the callback
            if (callback)
            {
                callback(events);
            }
        }
        else
        {
            $.jGrowl("Error listing events: " + o.message);
        }

    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: cb,
        dataType: "json",
        panel: this
    });
};

/** Link a document to an event */
GroupManager.prototype.linkDocumentToEvent = function(documentId, eventId, callback)
{
    var callData = {
        documentId: documentId,
        eventId: eventId
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/group?method=linkDocumentToEvent");

    var cb = function(o)
    {
        if (!o.error)
        {
            // Execute the callback
            if (callback)
            {
                callback(o.link);
            }
        }
        else
        {
            $.jGrowl("Error linking document to event: " + o.message);
        }

    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: cb,
        dataType: "json",
        panel: this
    });
};

/** Link a document to a group */
GroupManager.prototype.linkDocumentToGroup = function(documentId, groupId, callback)
{
    var callData = {
        documentId: documentId,
        eventId: groupId
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/group?method=linkDocumentToGroup");

    var cb = function(o)
    {
        if (!o.error)
        {
            // Execute the callback
            if (callback)
            {
                callback(o.link);
            }
        }
        else
        {
            $.jGrowl("Error linking document to group: " + o.message);
        }

    };
    $.ajax({
        type: 'POST',
        url: url,
        xhrFields: {
            withCredentials: true
        },         
        data: callString,
        success: cb,
        dataType: "json",
        panel: this
    });
};

/** Share a file with a group */
GroupManager.prototype.shareFile = function(){
    this.fileChooser.showDialog();
    this.fileChooser.showHomeFolder();
};
