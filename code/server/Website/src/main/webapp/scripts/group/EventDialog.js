//Requires
//<script type="text/javascript" src="../../scripts/filebrowser/filechooser.js"></script>
//<script type="text/javascript" src="../../scripts/group/GroupManager.js"></script>
function EventDialog(groupId)
{
  this.groupId = groupId;
  this.groupManager = new GroupManager();
  this.groupManager.groupId = groupId;
  this.okCallback = null;
  this.event = null;
  this.divId = null;
}

EventDialog.prototype.init = function(divId)
{
  //create the dialg box
  this.divId = divId;
  var htmlString = '    <div id="' + divId + '_eventDialogError"></div>' +
                   '    <div id="' + divId + '_confirm_dialog"></div>' +
                   '    <form>' +
                   '        <div class="formInput">' +
                   '          <label for="name">Name <span class="red">*</span></label>' +
                   '          <input type="text" name="name" id="' + divId + '_name"/>' +
                   '          <input type="hidden" name="groupId" id="' + divId + '_groupId" value="' + this.groupId + '"/>' +
                   '        </div>' +
                   '        <div class="formInput">' +
                   '          <label for="description">Description</label>' +
                   '          <input type="text" name="description" id="' + divId + '_description" value=""/>' +
                   '        </div>' +
                   '        <div class="formInput">' +
                   '          <label for="startDate">Start Date <span class="red">*</span></label>' +
                   '          <input type="text" name="startDate" id="' + divId + '_startDate" value=""/>' +
                   '        </div>' +
                   '        <div class="formInput">' +
                   '          <label for="startTime">Start Time (e.g. 09:00) <span class="red">*</span></label>' +
                   '          <input type="text" name="startTime" id="' + divId + '_startTime" value=""/>' +
                   '        </div>' +
                   '        <div class="formInput">' +
                   '          <label for="endDate">End Date <span class="red">*</span></label>' +
                   '          <input type="text" name="endDate" id="' + divId + '_endDate" value=""/>' +
                   '        </div>' +
                   '        <div class="formInput">' +
                   '          <label for="endTime">End Time (e.g. 17:00) <span class="red">*</span></label>' +
                   '          <input type="text" name="endTime" id="' + divId + '_endTime" value=""/>' +
                   '        </div>' +
                   '    </form>';

  $('#' + divId).html(htmlString);

  //set the default date and times
  var now = new Date();

  $('#' + divId + "_startDate").val(now.getDate() + "/" + (now.getMonth() +1) + "/" + now.getFullYear());
  $('#' + divId + "_endDate").val(now.getDate() + "/" + (now.getMonth() +1) + "/" + now.getFullYear());
  $('#' + divId + "_startTime").val("09:00");
  $('#' + divId + "_endTime").val("17:00");

  //    Setup the date pickers so that the user can't select end date < start date
  var dates = $("#" + divId + "_startDate, #" + divId + "_endDate").datepicker({
    changeMonth: true,
    dateFormat: "d/m/yy",
    onSelect: function(selectedDate)
    {
      var option = this.id == divId + "_startDate" ? "minDate" : "maxDate",
          instance = $(this).data("datepicker");
      date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
      dates.not(this).datepicker("option", option, date);
    }
  });

  //handle to 'this' so that we can call the save method from the OK button
  var thisGM = {};
  thisGM.gm = this.groupManager;
  thisGM.cb = this;

  $("#" + divId + "_confirm_dialog").dialog({
      height: 180,
      width: 400,
      title: "Delete Event",
      autoOpen: false,
      resizable: true,
      buttons:{
          Yes: function(){
              thisGM.cb.deleteEvent();
              $(this).dialog('close');
          },
          No: function(){
              $(this).dialog('close');
          }
      }
  });
  $("#" + divId + "_confirm_dialog").html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This event will be permanently deleted and cannot be recovered. Are you sure?</p>');
  
  //create the dialog
  $("#" + divId).dialog({
    height: 420,
    autoOpen: false,
    width: 450,
    resizable: true,
    buttons: {
      "OK": function()
      {
        //get the field values
        var name = $('#' + divId + '_name').val();
        var description = $('#' + divId + '_description').val();
        var startDate = $('#' + divId + '_startDate').datepicker("getDate");
        var endDate = $('#' + divId + '_endDate').datepicker("getDate");
        var startTime = $('#' + divId + '_startTime').val();
        var endTime = $('#' + divId + '_endTime').val();

        //Validate the fields - filled in and positive time
        var startTimesplit = startTime.split(":");
        if (startTimesplit.length == 2)
        {
          startDate.setHours(startTimesplit[0]);
          startDate.setMinutes(startTimesplit[1]);
        }
        else
        {
          $("#" + divId + "_eventDialogError").html("<span class='red'>Please enter the time fields in 24Hr format, e.g. 13:45</span>");
          return;
        }

        var endTimeSplit = endTime.split(":");
        if (endTimeSplit.length == 2)
        {
          endDate.setHours(endTimeSplit[0]);
          endDate.setMinutes(endTimeSplit[1]);
        }
        else
        {
          $("#" + divId + "_eventDialogError").html("<span class='red'>Please enter the time fields in 24Hr format, e.g. 13:45</span>");
          return;
        }

        if(endDate < startDate)
        {
          $("#" + divId + "_eventDialogError").html("<span class='red'>End must be after start</span>");
          return;
        }

        var groupId = $('#' + divId + '_groupId').val();

        //check for required fields
        if (name !== undefined && name !== "" && isValidDate(startDate) && isValidDate(endDate))
        {
          //create the event
          var evt;
          if(thisGM.cb.event!=null){
              evt = thisGM.cb.event;
              evt.name = name;
              evt.description = description;
              evt.groupId = groupId;
              evt.startDate = startDate;
              evt.endDate = endDate;
          } else {
              evt = thisGM.gm.createGroupEvent(name, description, groupId, startDate, endDate);
          }
          
          thisGM.gm.saveGroupEvent(evt, function()
          {
            //notify user
            $.jGrowl(evt.name + " Saved");

            //clear form
            $('#' + divId + '_name').val("");
            $('#' + divId + '_description').val("");
            $('#' + divId + '_startDate').val("");
            $('#' + divId + '_endDate').val("");
            $('#' + divId + '_eventDialogError').html("");

            //close dialog
            $("#" + divId).dialog("close");

            thisGM.cb.event = null;
            thisGM.cb.okCallback();
          });
        }
        else
        {
          //must fill in required fields
          $("#" + divId + "_eventDialogError").html("<span class='red'>Please fill in all required fields</span>");
        }
      },

      Delete: function(){
          if(thisGM.cb.event){
              $("#" + divId + "_confirm_dialog").dialog('open');
          }
      },
      
      Cancel: function()
      {
        //reset and close the dialog
        thisGM.cb.reset();
        $(this).dialog("close");
      }
    }
  }, thisGM);
};

EventDialog.prototype.reset = function(){
    $('#' + this.divId + '_name').val("");
    $('#' + this.divId + '_description').val("");
    $('#' + this.divId + '_startDate').val("");
    $('#' + this.divId + '_endDate').val("");
    $('#' + this.divId + '_eventDialogError').html("");
};

EventDialog.prototype.loadEvent = function(id){
    var callData = {id: id};
    var dialog = this;
    var callback = function(o){
        if(!o.error){
            dialog.showEvent(o.event);
        }
    };
    jsonCall(callData, "../../servlets/group?method=getEvent", callback);
};

EventDialog.prototype.showEvent = function(event){
    this.event = event;
    $('#' + this.divId + '_name').val(event.name);
    $('#' + this.divId + '_description').val(event.description);
    
    var startDate = new Date();
    startDate.setTime(event.startDateMillis);
    var endDate = new Date();
    endDate.setTime(event.endDateMillis);
    $('#' + this.divId + '_startDate').val(startDate.getDate() + "/" + (startDate.getMonth() +1) + "/" + startDate.getFullYear());
    $('#' + this.divId + '_endDate').val(endDate.getDate() + "/" + (endDate.getMonth() +1) + "/" + endDate.getFullYear());
    $('#' + this.divId + '_startTime').val(startDate.getHours() + ":" + startDate.getMinutes());
    $('#' + this.divId + '_endTime').val(endDate.getHours() + ":" + endDate.getMinutes());  
};

EventDialog.prototype.deleteEvent = function(){
    if(this.event){
        var callData = {id: this.event.id};
        this.event = null;
        var dialog = this;
        var callback = function(){
            $("#" + dialog.divId + "_confirm_dialog").dialog('close');
            $("#" + dialog.divId).dialog('close');
            dialog.okCallback();
        };
        jsonCall(callData, "../../servlets/group?method=deleteEvent", callback);
    }
};

function isValidDate(d)
{
  if (Object.prototype.toString.call(d) !== "[object Date]")
    return false;
  return !isNaN(d.getTime());
}

