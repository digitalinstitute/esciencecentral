/* 
 * This script provides an object that manages metadata
 */
function MetadataManager(){
    this.divName = "";
    this.folder = null;
    this.propertyEditor = new PropertyEditor();
}

MetadataManager.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var manager = this;
        var dialog = $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 600,
            width: 800,
            title: "Edit Search Folder",
            modal: true,
            viewer: this,
            buttons: {
                'Ok': function(){
                    manager.saveSearchFolder();
                },
                'Cancel': function(){
                    $(this).dialog('close');
                }

            }
        });     
        
        var propertyEditorDiv = document.createElement("div");
        propertyEditorDiv.setAttribute("class", "dialog");
        propertyEditorDiv.setAttribute("id", this.divName + "_property_editor");
        div.appendChild(propertyEditorDiv);
        this.propertyEditor.init(this.divName + "_property_editor");
        
        this.propertyEditor.okCallback = function(properties){
            manager.mergeProperties(properties, this.itemIndex);
            manager.displaySearchFolder();
        }
    }
};

MetadataManager.prototype.editSearchFolder = function(id){
    var div = document.getElementById(this.divName);
    div.innerHTML = "Fetching...";
    $('#' + this.divName).dialog('open');
    
    var manager = this;
    var cb = function(o){
        if(!o.error){
            manager.folder = o.folder;
            manager.displaySearchFolder();
        }
    }
    jsonCall({id: id}, "../../servlets/metadata?method=getSearchFolder", cb, null);
};

MetadataManager.prototype.removeSearchFolder = function(id, cb){
    jsonCall({id: id}, "../../servlets/metadata?method=removeSearchFolder", cb, cb);
};

MetadataManager.prototype.createSearchFolder = function(name, cb){
    jsonCall({name: name}, "../../servlets/metadata?method=createSearchFolder", cb, cb);
};

MetadataManager.prototype.addQueryItem = function(id){
    var manager = this;
    var cb = function(o){
        if(manager.folder){
            if(!manager.folder.query.items){
                manager.folder.query.items = new Array();
            }
            manager.folder.query.items.push(o.item);
            manager.displaySearchFolder();
        }
    }
    jsonCall({itemId: id}, "../../servlets/metadata?method=createQueryItem", cb, null);
};

MetadataManager.prototype.displaySearchFolder = function(){
    if(this.folder){
        var div = document.getElementById(this.divName);

        var i;
        var item;
        var html='<div class="dialogheading">Search folder details:</div>';
        html+='<form onsubmit="return false;">';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="foldername">Search folder name:</label>';
        html+='<input name="foldername" type="text" id="' + this.divName + '_foldername" size="20" value="' + this.folder.name + '"/>';
        html+='</div>';
        html+='<div class="forminput">';
        html+='<label style="width: 170px; display: inline-table;" for="folderdescription">Folder description:</label>';
        html+='<input name="folderdescription" type="text" id="' + this.divName + '_folderdescription" size="20" value="' + this.folder.description + '"/>';
        html+='</div>';        
        html+='</form><hr class="smallHr">';
        html+='<div class="dialogheading">Add metadata items:</div>';
        html+='<form onsubmit="return false;">';
        html+='<div class="formInput">';

        html+='<label style="display: inline-block; width: 170px;" for="addmetadata">Metadata query item:</label>';
        html+='<select class="display: inline;" name="addmetadata" id="' + this.divName + '_item_type_select">';   
        html+='<option id="boolean-item">True/False value</option>';
        html+='<option id="date-range-item">Date range</option>';
        html+='<option id="numerical-value-item">Exact numerical value</option>';
        html+='<option id="numerical-range-item">Numerical range</option>';
        html+='<option id="text-value-item">Text value</option>';
        html+='</select>';
        html+='<input type="button" class="btn higherButton inlineButton" name="additem" value="Add" id="' + this.divName + '_add_item_button"/>';
        html+='</div>';
        html+='</form>';
        html+='<hr class="smallHr"/>';        
        
        // Table of the actual items
        html+='<table id="' + this.divName + '_item_table" style="width: 100%;" class="display"><thead>';
        html+='<tr><th>Label</th><th>Item Name</th><th>Action</th></tr></thead><tbody>';
        for(i=0;i<this.folder.query.items.length;i++){
            item = this.folder.query.items[i];
            html+='<tr>';
            html+='<td>' + item.Label + '</td>';
            html+='<td>' + item.ItemName + '</td>';
            html+='<td>';
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/metadata/images/cross.png") +'" id="' + this.divName + '_remove_' + i + '"/>';
            html+='<img style="cursor: pointer;" src="' + rewriteAjaxUrl("../../scripts/metadata/images/pencil.png") + '" id="' + this.divName + '_edit_' + i + '"/>';
            html+='</td>';
            html+='</tr>';
        }
        html+='</tbody></table>';

        div.innerHTML = html;
        
        // Setup table
        $('#' + this.divName + "_item_table").dataTable({"bJQueryUI":true});
        
        // Add handlers
        var dialog = this;
        var addButton = document.getElementById(this.divName + "_add_item_button");
        addButton.onclick = function(){
            var selector = document.getElementById(dialog.divName + "_item_type_select");
            var index = selector.selectedIndex;
            var typeId = selector.options[index].id;
            
            // Add correct type
            dialog.addQueryItem(typeId);
        }
        
        
        // Add the click handlers for the delete buttons
        var img;

        for(i=0;i<this.folder.query.items.length;i++){
            item = this.folder.query.items[i];
            img = document.getElementById(this.divName + '_remove_' + i);
            if(img){
                img.itemIndex = i;
                img.onclick = function(){
                    dialog.removeMetadataItem(this.itemIndex);
                }
            }
            
            img = document.getElementById(this.divName + "_edit_" + i);
            img.itemIndex = i;
            img.onclick = function(){
                dialog.editMetadataItem(this.itemIndex);
            }
        }
    }        
};

MetadataManager.prototype.removeMetadataItem = function(index){
    this.folder.query.items.splice(index, 1);
    this.displaySearchFolder();
};


MetadataManager.prototype.editMetadataItem = function(index){
    this.propertyEditor.itemIndex = index;
    this.propertyEditor.editProperties(this.folder.query.items[index]);
};

/** Merge some properties into the underlying formatter */
MetadataManager.prototype.mergeProperties = function(properties, index){
    var targetObject = this.folder.query.items[index];
    var names = Object.getOwnPropertyNames(targetObject).sort();
    for(var i=0;i<names.length;i++){
        if(properties[names[i]]){
            targetObject[names[i]] = properties[names[i]];
        }
    }
};

/** Save the query back to the server */
MetadataManager.prototype.saveSearchFolder = function(){
    // Update name & description
    var name = document.getElementById(this.divName + "_foldername").value;
    var description = document.getElementById(this.divName + "_folderdescription").value;    
    
    if(name){
        this.folder.name = name;
    }
    
    if(description){
        this.folder.description = description;
    }
    
    var callData = {
        folder: this.folder
    }
    
    var manager = this;
    var cb = function(){
        $('#' + manager.divName).dialog('close');
        location.reload();
    }
    
    jsonCall(callData, "../../servlets/metadata?method=saveSearchFolder", cb, null);
};
