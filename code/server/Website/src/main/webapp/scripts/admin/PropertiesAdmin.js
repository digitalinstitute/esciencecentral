/*
 * Provides support for system property editing
 */
function PropertiesAdmin(){
    this.divName = "";
    this.groupName = "";
    this.properties = {};
    this.propertiesSelected = false;
    this.propertyEditor = new PropertyEditor();
    this.propertyEditor.withoutDataTables = true;
}

PropertiesAdmin.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(this.divName);
    if(div){
        var html='<form action="#" onsubmit="return false;">';
        html+='<div class="input-append">';
        html+='<label for="groupselect">Property Group</label>';
        html+='<select id="' + this.divName + '_selector" name="groupselect"></select>';
        html+='</div>';
        html+='</form>';
        html+='<div id="' + this.divName + '_properties"></div>';
        
       
        div.innerHTML = html;
        
        $(this.divName + "_selector").selectBox();
        this.propertyEditor.init(this.divName + "_properties", true);
    };
};

PropertiesAdmin.prototype.populatePropertyGroupList = function(){
    var selectorName = this.divName + "_selector";
    var dialog = this;
    var cb = function(o){
        if(!o.error){
            var selector = document.getElementById(selectorName);
            if(selector){
                var selectHtml="";
                for(var i=0;i<o.names.length;i++){
                    selectHtml+='<option>' + o.names[i] + '</option>';
                }
                selector.innerHTML = selectHtml;  
                selector.onchange = function(){
                    dialog.fetchPropertyGroup(this.value);
                };
                
                // Set the first group
                if(o.names.length>0){
                    dialog.fetchPropertyGroup(o.names[0]);
                }
            }
        }
    };
    
    jsonCall({}, "../../servlets/admin?method=listPropertyGroups", cb);
};

PropertiesAdmin.prototype.fetchPropertyGroup = function(groupName){
    var dialog = this;
    var cb = function(o){
        if(!o.error){
            dialog.savePropertyGroup(dialog.groupName, dialog.propertyEditor.properties);
            dialog.properties = o.properties;
            dialog.groupName = groupName;
            dialog.propertiesSelected = true;
            dialog.propertyEditor.editProperties(o.properties);
        }
    };
    jsonCall({groupName: groupName}, "../../servlets/admin?method=getPropertyGroup", cb);
};

PropertiesAdmin.prototype.savePropertyGroup = function(saveSetName, propertySet){
    if(this.propertiesSelected){
        var cb = function(o){
        };
        
        jsonCall({groupName: saveSetName, properties: propertySet}, "../../servlets/admin?method=savePropertyGroup", cb);
    }
};

PropertiesAdmin.prototype.syncSavePropertyGroup = function(){
    if(this.propertiesSelected){
        try {
            jsonPost("../../servlets/admin?method=savePropertyGroup", {groupName: this.groupName, properties: this.propertyEditor.properties}); 
        } catch (err){
            console.log(err);
        }
    }
};