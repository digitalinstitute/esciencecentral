/* 
 * This script provides tools for managing data stores
 */
function DataStoreAdmin(){
    this.divName = "";
    this.storeTypeDialog = new MultiValueSelectDialog();
    
}

DataStoreAdmin.prototype.init = function(divName){
    this.divName = divName;
};

/** Edit the system data store object */
DataStoreAdmin.prototype.getSystemDatastore = function(callback){
    var cb = function(o){
        if(!o.error){
            callback(o.properties);
        }
    };
    jsonCall({}, "../../servlets/admin?method=getSystemDatastoreProperties", cb);
};

/** Create a new migration */
DataStoreAdmin.prototype.createMigration = function(storeType, callback){
    var cb = function(o){
        if(!o.error){
            callback(o.migration);
        }
    };
    jsonCall({storeType: storeType}, "../../servlets/admin?method=createMigration", cb);
};

/** Start a migration */
DataStoreAdmin.prototype.startMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=startMigration", cb);
};

DataStoreAdmin.prototype.stopMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=stopMigration", cb);
};

DataStoreAdmin.prototype.resetMigrationDataStore = function(migrationId, storeType, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId, storeType: storeType}, "../../servlets/admin?method=resetMigrationDataStore", cb);
};

DataStoreAdmin.prototype.setMigrationDataStoreProperties = function(migrationId, properties, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId, properties: properties}, "../../servlets/admin?method=setMigrationDataStoreProperties", cb);
};

DataStoreAdmin.prototype.setSystemDatastoreProperties = function(properties, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({properties: properties}, "../../servlets/admin?method=setSystemDatastoreProperties", cb);
};

DataStoreAdmin.prototype.getMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback(o.migration);
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=getMigration", cb);
};

DataStoreAdmin.prototype.deleteMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        } 
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=deleteMigration", cb);
};

DataStoreAdmin.prototype.retryFailedDocumentsForMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=retryFailedDocumentsForMigration", cb);
};

DataStoreAdmin.prototype.getMigrationState = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback(o.state);
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=getMigrationState", cb);
};
    
DataStoreAdmin.prototype.listMigrations = function(callback){
    var cb = function(o){
        if(!o.error){
            callback(o.migrations);
        }
    };
    jsonCall({}, "../../servlets/admin?method=listMigrations", cb);
};

DataStoreAdmin.prototype.finishMigration = function(migrationId, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({migrationId: migrationId}, "../../servlets/admin?method=finishMigration", cb);
};

DataStoreAdmin.prototype.resetAccessKey = function(callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({}, "../../servlets/admin?method=resetAccessKey", cb);
};
