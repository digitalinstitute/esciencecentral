/* 
 * This script provides template folder management
 */

function TemplateFolderAdmin()
{
  this.templateFolders = new Array();
}

/** Get a product by id */
TemplateFolderAdmin.prototype.getTemplateFolderById = function (id)
{
  for (var i = 0; i < this.templateFolders.length; i++)
  {
    if (this.templateFolders[i].id == id)
    {
      return this.templateFolders[i];
    }
  }
  return null;
};

/** Save a product */
TemplateFolderAdmin.prototype.createTemplateFolder = function (folderId, description, callback)
{
  var url = "../../servlets/admin?method=saveTemplateFolder";
  var callData = {folderId:folderId, description:description};
  var cb = function (o)
  {
    if (!o.error)
    {
      if (callback)
      {
        callback();
      }
    }
    else
    {
      alert(o.error);
    }
  };
  jsonCall(callData, url, cb);
};

/** List the available products */
TemplateFolderAdmin.prototype.listTemplateFolders = function (callback)
{
  var url = "../../servlets/admin?method=listTemplateFolders";
  var callData = {};
  var admin = this;

  var cb = function (o)
  {
    if (!o.error)
    {
      admin.templateFolders = o.templateFolders;
      if (callback)
      {
        callback();
      }
    }
  };
  jsonCall(callData, url, cb);
};

/** List the products in an array suitable for a datatable */
TemplateFolderAdmin.prototype.buildTemplateFolderArray = function ()
{
  var templateFoldersArray = new Array();
  var templateFolder;
  var templateFoldertRow;

  for (var i = 0; i < this.templateFolders.length; i++)
  {
    templateFolder = this.templateFolders[i];
    templateFoldertRow = new Array();
    templateFoldertRow[0] = templateFolder.id;
    templateFoldertRow[1] = templateFolder.folderId;
    templateFoldertRow[2] = templateFolder.name;
    templateFoldertRow[3] = templateFolder.description;
    templateFoldertRow[4] = templateFolder.id;

    templateFoldersArray[i] = templateFoldertRow;
  }
  return templateFoldersArray;
};

TemplateFolderAdmin.prototype.deleteTemplateFolder = function (folderId)
{
  var url = "../../servlets/admin?method=deleteTemplateFolder";
  var callData = {id:folderId};
  var admin = this;
  var cb = function (o)
  {
    if (!o.error)
    {
      admin.templateFolders = o.templateFolders;
    }
  };
  jsonCall(callData, url, cb);

};
