/* 
 * This script provides admin support to manage remote file system scanners
 * that injest data into e-SC
 */
function RemoteFilesystemScannerAdmin(){
    this.editDialogDivName = "";
    this.chooserDialogDivName = "";
    this.filesDialogDivName = "";
    this.chooser = new MultiValueSelectDialog();
    this.editor = new PropertyEditor();
    
    // List of scanner types for showing the create dialog
    this.scannerTypes = new Array();
    this.scannerTypes[0] = "Filesystem";
    this.scannerTypes[1] = "Azure";
    this.scannerTypes[2] = "AmazonS3";
    
    this.scannerList = new Array();         // Scanner list from last fetch
    this.scannerId = undefined;
}

RemoteFilesystemScannerAdmin.prototype.initList = function(divName){
    this.listDivName = divName;
};

RemoteFilesystemScannerAdmin.prototype.fetchUserScannerList = function(callback){
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.scannerList = o.scanners;
            if(callback){
                callback(admin.scannerList);
            }
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/admin?method=listUserScanners"), cb, null);    
};

RemoteFilesystemScannerAdmin.prototype.fetchScannerList = function(callback){
    var admin = this;
    var cb = function(o){
        if(!o.error){
            admin.scannerList = o.scanners;
            if(callback){
                callback(admin.scannerList);
            }
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/admin?method=listAllScanners"), cb, null);
};

RemoteFilesystemScannerAdmin.prototype.fetchSpecificScanner = function(id, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.scanner);
            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=getScanner"), cb, null);    
};

RemoteFilesystemScannerAdmin.prototype.deleteScanner = function(id, callback, studyId){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        }
    };
    
    if(studyId){
        jsonCall({id: id, studyId: studyId}, rewriteAjaxUrl("../../servlets/admin?method=deleteScanner"), cb, null);
    } else {
        jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=deleteScanner"), cb, null);
    }
};

RemoteFilesystemScannerAdmin.prototype.createScannerUsingChooser = function(callback, studyId){
    var admin = this;
    this.chooser.okCallback = function(value){
        admin.createScanner(value, studyId, callback);
    };    
    this.chooser.show(this.scannerTypes);
};

RemoteFilesystemScannerAdmin.prototype.createScanner = function(scannerType, studyId, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.scanner);
            }
        }
    };
    
    if(studyId){
        jsonCall({scannerType: scannerType, studyId: studyId}, rewriteAjaxUrl("../../servlets/admin?method=createScanner"), cb, null);
    } else {
        jsonCall({scannerType: scannerType}, rewriteAjaxUrl("../../servlets/admin?method=createScanner"), cb, null);
    }
};

RemoteFilesystemScannerAdmin.prototype.executeScanner = function(id, callback){
    var cb = function(o){
        if(callback){
            callback(o.scanner);
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl('../../servlets/admin?method=executeScanner'), cb, null);
};

RemoteFilesystemScannerAdmin.prototype.resetScanner = function(id, callback){
    var cb = function(o){
        if(callback){
            callback();
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl('../../servlets/admin?method=resetScanner'), cb, null);    
};

RemoteFilesystemScannerAdmin.prototype.setScannerFolder = function(scannerId, folderId, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.scanner);
            }
        }
    };
    jsonCall({id: scannerId, folderId: folderId}, rewriteAjaxUrl("../../servlets/admin?method=setScannerFolder"), cb, null);
};

RemoteFilesystemScannerAdmin.prototype.initEditDialog = function(editDialogDivName){
    this.editDialogDivName = editDialogDivName;
     this.editor.init(this.editDialogDivName);
};

RemoteFilesystemScannerAdmin.prototype.initChooserDialog = function(chooserDialogDivName){
    this.chooserDialogDivName = chooserDialogDivName;
    this.chooser.init(this.chooserDialogDivName);

};

RemoteFilesystemScannerAdmin.prototype.editScanner = function(scanner, callback){
    var admin = this;
    
    // Callback triggered when the properties are accepted
    this.editor.okCallback = function(properties){
        var scb = function(o){
            if(o){
                if(!o.error){
                    if(callback){
                        callback();
                    }
                }
            } else {
                if(callback){
                    callback();
                }
            }
        };
        if(this.scanner){
            admin.setScannerProperties(this.scanner.id, properties, scb);
        }
    };
    
    if(typeof scanner==="string"){
        // This is a scanner id
        
        this.editor.properties = null;
        var cb = function(result){
            if(result.properties){
                admin.editor.scanner = result;
                admin.editor.editProperties(result.properties);
            }
        };
        this.fetchSpecificScanner(scanner, cb);
        
    } else {
        // Edit as is
        if(scanner.properties){
            this.editor.scanner = scanner;
            this.editor.editProperties(scanner.properties);
        } else{
            this.editor.scanner = null;
        }
    }
};

RemoteFilesystemScannerAdmin.prototype.setScannerProperties = function(id, properties, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        }
    };
    jsonCall({id: id, properties: properties}, rewriteAjaxUrl("../../servlets/admin?method=setScannerProperties"), cb);
};

RemoteFilesystemScannerAdmin.prototype.enableScanner = function(id, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=enableScanner"), cb, null);
};

RemoteFilesystemScannerAdmin.prototype.disableScanner = function(id, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=disableScanner"), cb, null);
};

RemoteFilesystemScannerAdmin.prototype.initFilesDialog = function(divName){
    this.filesDialogDivName = divName;
    var div = document.getElementById(this.filesDialogDivName);
    if(div){
        var admin = this;
        $(div).dialog({
            bgiframe: true,
            autoOpen: false,
            height: 500,
            width: 700,
            title: "Filesystem Scanner Files",
            modal: true,
            buttons: {                
                'Refresh' : function(){
                    admin.showScannerFiles();
                },                
                
                'Ok': function() {
                    $(this).dialog('close');
                }
            }

        });
    }
};

RemoteFilesystemScannerAdmin.prototype.showScannerFiles = function(id){
    // Use this id and store it if it is defined
    if(id){
        this.scannerId = id;
    }
    
    var div = document.getElementById(this.filesDialogDivName);
    div.innerHTML = "Fetching...";
    var admin = this;
    
    var cb = function(files){
        // Create the table
        var html = '<table class="display" width="100%" id="' + admin.filesDialogDivName + '_table">';
        html+='<thead><tr><th>Name</th><th>Size</th><th>Status</th></tr></thead><tbody>';
        for(var i=0;i<files.length;i++){
            html+='<tr>';
            html+='<td>' + files[i].name + '</td>';
            html+='<td>' + files[i].size + '</td>';
            html+='<td>' + files[i].status + '</td>';
            html+='</tr>';
        }
        
        html+='</tbody></table>';
        div.innerHTML = html;
        
        $("#" + admin.filesDialogDivName + "_table").dataTable({"bJQueryUI": true});
    };

    if(this.scannerId){
        $("#" + this.filesDialogDivName).dialog('open');
        this.fetchScannerFiles(this.scannerId, cb);
    }
};

RemoteFilesystemScannerAdmin.prototype.fetchScannerFiles = function(id, callback){
    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.files);
            }
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=listScannerFiles"), cb, null);
};