/* 
 * This script provides admin tools for cloud credentials
 */
function CloudCredentialsAdmin(){
    this.editorDivName = "";
    this.credentialsList = new Array();
}

CloudCredentialsAdmin.prototype.initEditor = function(editorDivName){
    this.editorDivName = editorDivName;
    var div = document.getElementById(this.editorDivName);
    if(div){
        
        
    }
};

CloudCredentialsAdmin.prototype.listCredentialTypes = function(callback){
        var cb = function(o){
        if(!o.error && callback){
            callback(o.types);
        }
    };
    jsonCall({}, rewriteAjaxUrl("../../servlets/admin?method=listCredentialTypes"), cb);
};

CloudCredentialsAdmin.prototype.createCredentials = function(credentialsType, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    
    jsonCall({type: credentialsType}, rewriteAjaxUrl("../../servlets/admin?method=createCredentials"), cb);
};


CloudCredentialsAdmin.prototype.deleteCredentials = function(id, callback){
    var cb = function(o){
        if(!o.error){
            callback();
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=deleteCredentials"), cb);
};

CloudCredentialsAdmin.prototype.getCredentials = function(id, callback){
    var cb = function(o){
        if(!o.error && callback){
            callback(o.credentials);
        }
    };
    jsonCall({id: id}, rewriteAjaxUrl("../../servlets/admin?method=getCredentials"), cb);
};

CloudCredentialsAdmin.prototype.saveCredentials = function(credentials, callback){
    var cb = function(o){
        if(!o.error && callback){
            callback();
        }
    };
    jsonCall({credentials: credentials}, rewriteAjaxUrl("../../servlets/admin?method=saveCredentials"), cb);
};
