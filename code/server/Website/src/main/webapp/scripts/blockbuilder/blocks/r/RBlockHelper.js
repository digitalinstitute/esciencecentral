/* 
 * This script provides a helper that can create R workflow blocks
 */
function RBlockHelper(){
    this.block = null;
    this.supportScriptFolder = null;
    this.blockTypeLabel = "R";    
    this.isBlock = true;
    this.isDeployable = true;
}

RBlockHelper.prototype.createInstance = function(block){
    var helper = new RBlockHelper();
    if(block){
        helper.block = block;
        block.helper = helper;
        block.hasServiceXml = helper.isBlock;
    }
    return helper;
};

RBlockHelper.prototype.validate = function(wizardData){
    this.supportScriptFolder = globalBuilder.createFolderIfNotPresent(this.block.srcFolder, "scripts");
    
    // Build a list of parameter replacements
    var replacements;
    if(wizardData && wizardData.replacements){
        replacements = wizardData.replacements;
    } else {
        replacements = {
            names: ["name", "description","category"],
            values: ["NewBlock","R Block","My Services"]
        }
    }
    
    // Standard block files
    var contents = APIClient.prototype.getFolderContents(this.block.srcFolder);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "service.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/service.xml"), contents, replacements);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/library.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "dependencies.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/dependencies.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "init.r", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/init.r"), contents);
    globalBuilder.copyBlockIcon(this.block.srcFolder, "r.jpg");
    
    // Use the wizard data code if present
    if(wizardData && wizardData.blockCode){
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.r", wizardData.blockCode, contents);
    } else {
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.r", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/main.r"), contents);
    }    
    
};


RBlockHelper.prototype.prepareZip = function(){
    
};

RBlockHelper.prototype.getBlockMainCode = function(){
    if(this.block.srcFolder){
        var doc = APIClient.prototype.getFileContents(this.block.srcFolder, "main.r");
        return doc;
    } else {
        throw "No source folder present";
    }
};

RBlockHelper.prototype.getBlockMainCodeTemplateAsDocument = function(){
    var code = APIClient.prototype.getUrlContentAsString(rewriteAjaxUrl("../../scripts/blockbuilder/blocks/r/templates/main.r"));
    var doc = new DocumentRecord();
    doc.name = "main.r";
    doc.content = code;
    return doc;
};

globalBuilder.registerBlockType(new BlockType("R", "R is a programming language and software environment for statistical computing and graphics. The R language has become a de facto standard among statisticians for developing statistical software, and R is widely used for statistical software development and data analysis.", RBlockHelper, true));