/* 
 * This script represents a block property
 */
function BlockProperty(){
    this.name = "";
    this.defaultValue = "";
    this.description = "";
    this.dataType = "";
    this.category = "";
}

BlockProperty.prototype.createXmlTag = function(){
    return '<Property name="' + this.name + '" type="' + this.dataType + '" description="' + this.description + '" default="' + this.defaultValue + '" category="' + this.category + '"/>';
};