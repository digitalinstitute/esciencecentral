/* 
 * This script provides a file manager panel that is used to manage the files
 * in the block filesystem.
 */
//Requires
//$.rloader({type:'js', src:'../../scripts/filebrowser/filetree.js'});
//$.rloader({type:'js', src:'../../scripts/filebrowser/filechooser.js'});
//$.rloader({type: 'css', src: '../../scripts/blockbuilder/servicexmlui.css'});
function BlockFileManager(block, divName){
    this.divName = divName;
    this.block = block;
    this.fileTree = new FileTree();
    this.div = null;
    this.toolbarDiv = null;
    this.rightDiv = null;
    this.leftDiv = null;
    this.editorPanel = new SimpleEditorPanel();
    this.inputDialog = new InputDialog();
    this.confirmDialog = new ConfirmDialog();
    this.fileChooser = new FileChooser();
    
}

BlockFileManager.prototype.createFileManager = function(div){
    // Create the left hand side div
    this.div = div;
    
    // Create the dialog divs
    var inputDiv = document.createElement("div");
    inputDiv.setAttribute("id", this.divName + "_inputdialog");
    div.appendChild(inputDiv);
    this.inputDialog.init(this.divName + "_inputdialog");
    
    var confirmDiv = document.createElement("div");
    confirmDiv.setAttribute("id", this.divName + "_confirmdialog");
    div.appendChild(confirmDiv);
    this.confirmDialog.init(this.divName + "_confirmdialog");
    
    var chooserDialog = document.createElement("div");
    chooserDialog.setAttribute("id", this.divName + "_filechooser");
    div.appendChild(chooserDialog);
    this.fileChooser.init(this.divName + "_filechooser");
    
    // Create the toolbar div
    this.toolbarDiv = document.createElement("div");
    this.toolbarDiv.setAttribute("class", "fileManagerToolbar");
    this.toolbarDiv.setAttribute("id", this.divName + "_toolbar");
    this.createToolbar(this.toolbarDiv);
    div.appendChild(this.toolbarDiv); 

    // Create the contents div
    var contentDiv = document.createElement("div");
    contentDiv.setAttribute("style", "width: 100%");
    
    this.leftDiv = document.createElement("div");
    this.leftDiv.setAttribute("class", "ui-corner-all filemanagerLeft");
    this.leftDiv.setAttribute("id", this.divName + "_files");
    contentDiv.appendChild(this.leftDiv);   
    
    // Create the right hand div
    this.rightDiv = document.createElement("div");
    this.rightDiv.setAttribute("class", "fileManagerRight ui-corner-all");
    this.rightDiv.setAttribute("id", this.divName + "_editor");
    contentDiv.appendChild(this.rightDiv);
    div.appendChild(contentDiv);
    
    this.fileTree.init(this.divName + "_files");
    var fm = this;
    this.fileTree.fileDoubleClickCallback = function(fileId, fileName){
        fm.fileDoubleClick(fileId, fileName);
    }
    if(this.block && this.block.srcFolder){
        this.fileTree.showFolder(this.block.srcFolder.id);
    }
    
    this.editorPanel.init(this.divName + "_editor");
    
    this.editorPanel.onChangeFunction = function(panel){
        var editor = document.getElementById(fm.divName + "_editor");
        if(panel.isDirty){
            if(editor){
                editor.style.borderColor = "red";
            }
        } else {
            if(editor){
                editor.style.borderColor = "black";
            }
        }
        
    };
    
    this.resize();
};

BlockFileManager.prototype.resize = function(){
    var height = this.div.clientHeight;
    this.toolbarDiv.style.height = "35px";
    this.rightDiv.style.height = (height - 40) + "px";
    this.leftDiv.style.height = (height - 30) + "px";
    this.editorPanel.resize();
};

BlockFileManager.prototype.fileDoubleClick = function(fileId, fileName){
    if(this.editorPanel.isDirty){
        alert("Document has changed");
    }   
    this.openFile(fileId);
};

BlockFileManager.prototype.openFile = function(fileId){
    var editor = document.getElementById(this.divName + "_editor");
    if(editor){
        editor.style.borderColor = "black";
        
    }    
    this.editorPanel.openDocumentById(fileId);
    this.editorPanel.resize();
};


BlockFileManager.prototype.createToolbar = function(div){
    // New document
    var b1 = document.createElement("button");
    b1.setAttribute("id", this.bodyDivName + "_toolbar_button_new_file");
//    b1.appendChild(document.createTextNode("NewDocument"));
    div.appendChild(b1);


    // Delete document button
    var b2 = document.createElement("button");
    b2.setAttribute("id", this.bodyDivName + "_toolbar_button_delete_file");
    div.appendChild(b2);
    
    // Open document button
    var b3 = document.createElement("button");
    b3.setAttribute("id", this.bodyDivName + "_toolbar_button_open_file");
    div.appendChild(b3);

    // Save document button
    var b4 = document.createElement("button");
    b4.setAttribute("id", this.bodyDivName + "_toolbar_button_save_file");
    div.appendChild(b4);
    
    // Upload button
    var b5 = document.createElement("button");
    b5.setAttribute("id", this.bodyDivName + "_toolbar_button_upload_file");
    div.appendChild(b5);
    
    $(b1).button({
            text: true,
            label: "New",
            icons: {
                    primary: "ui-icon-document"
            }
    });

    $(b2).button({
            text: true,
            label: "Delete",
            icons: {
                    primary: "ui-icon-trash"
            }
    });
    
    $(b3).button({
            text: true,
            label: "Open",
            icons: {
                    primary: "ui-icon-folder-open"
            }
    });    
    
    $(b4).button({
            text: true,
            label: "Save",
            icons: {
                    primary: "ui-icon-disk"
            }
    });        
    
    $(b5).button({
            text: true,
            label: "Copy",
            icons: {
                    primary: "ui-icon-arrowstop-1-n"
            }
    });        

    var dialog = this;
    b1.onclick = function(){
        dialog.inputDialog.okCallback = function(name){
            APIClient.prototype.createFile(dialog.block.srcFolder, name);
            dialog.fileTree.showFolder(dialog.block.srcFolder.id);
        };
        dialog.inputDialog.show();
    };
    
    b2.onclick = function(){
        if(dialog.fileTree.selectedFileId){
            var id = dialog.fileTree.selectedFileId;
            dialog.confirmDialog.yesCallback = function(){
                APIClient.prototype.deleteFile(id);
                dialog.fileTree.showFolder(dialog.block.srcFolder.id);
            }
            dialog.confirmDialog.show("Are you sure you want to delete: " + dialog.fileTree.selectedFileName);
        }
    };
    
    b3.onclick = function(){
        if(dialog.fileTree.selectedFileId){
            dialog.openFile(dialog.fileTree.selectedFileId);
        }
    };
    
    b4.onclick = function(){
        dialog.editorPanel.save();
        dialog.editorPanel.triggerOnChangeFunction();
    };
    
    // Copy file from existing data store
    b5.onclick = function(){
        var callback = function(chooser){
            chooser.okCallback = null;
            var fileId = chooser.selectedFileId;
            var folderId = chooser.dialogObject.fileTree.selectedFolderId;
            if(fileId && folderId){
                APIClient.prototype.copyFile(fileId, folderId);
                chooser.dialogObject.fileTree.showFolder(chooser.dialogObject.block.srcFolder.id);
            }
        };
        dialog.fileChooser.okCallback = callback;
        dialog.fileChooser.dialogObject = dialog;
        dialog.fileChooser.showHomeFolder();
    };
};