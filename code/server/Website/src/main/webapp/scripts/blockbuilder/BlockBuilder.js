/* 
 * This script provides a block builder IDE type interface that sits
 * inside the main website
 */
function BlockBuilder(){
    this.divName = "";
    this.container = null;
    this.bodyDiv = null;
    this.blocktypes = new List();
    this.editors = new List();
    this.busyDialog = new BusyDialog();
    this.currentBlock = null;
    this.fileTree = new FileTree();
    this.fileChooser = new FileChooser();
    this.confirmDialog = new ConfirmDialog();
    this.inputDialog = new InputDialog();
    this.aclDialog = new AccessControl();
    this.versions = new VersionList();
    this.tabCounter = 0;
    this.selectedEditor = null;
}

BlockBuilder.prototype.init = function(divName){
    this.divName = divName;
    
    var div = document.getElementById(this.divName);
    if(div){
        var html = '<div class="ui-layout-center" style="overflow: hidden">';
            html+='<div class="tabs" id="' + this.divName + 'tabs" style="height: 99%"> ';

                    html+='<ul> ';
//                        html+='<li><a href="#tabs-1">MyTab</a></li> ';
                    html+='</ul> ';
//                    html+='<div id="tabs-1">';
//                    html+='<p>Some content</p>';
//                    html+='</div>';

            html+='</div>';
        html+='</div>';
        html+='<div class="ui-layout-north clearfix" style="overflow-y: none; border: none;">';
            html+='<div id="' + this.divName + '_top_toolbar" class="navbar clearfix">';
                html+='<div class="navbar-inner">';
                    html+='<ul class="nav clearfix">';
                        html+='<li style="float:right;"><a id="' + this.divName + 'security"><i class="icomoon-lock"></i>Security</a></li>';
                        html+='<li style="float:right;"><a id="' + this.divName + 'packageblock"><i class="icomoon-cogs"></i>Deploy</a></li>';
                        html+='<li><a id="' + this.divName + 'openblock"><i class="icomoon-folder-open"></i>Load</a></li>';
                        html+='<li><a id="' + this.divName + 'createblock"><i class="icomoon-wand"></i>New</a></li>';
                        html+='<li><a id="' + this.divName + 'savefiles"><i class="icomoon-disk"></i>Save</a></li>';
                    html+='</ul>';
                html+='</div>';
            html+='</div>';
        html+='</div>';
        html+='<div class="ui-layout-south" id="' + this.divName + 'console">';
        html+='</div>';
        html+='<div class="ui-layout-east" id="' + this.divName + 'versions">';    
        //html+='<div id="' + this.divName + 'versions"></div>';
        html+='</div>';
        html+='<div class="ui-layout-west">';
            html+='<div id="' + this.divName + '_inner_toolbar" class="navbar clearfix">';
                html+='<div class="navbar-inner">';
                    html+='<ul class="nav clearfix">';
                        html+='<li><a id="' + this.divName + 'refreshtree"><i class="icomoon-loop-2"></i></a></li>';
                        html+='<li><a id="' + this.divName + 'copyfile"><i class="icomoon-copy"></i></a></li>';
                        html+='<li><a id="' + this.divName + 'deletefile"><i class="icomoon-remove"></i></a></li>';
                        html+='<li><a id="' + this.divName + 'newdocument"><i class="icomoon-file-4"></i></a></li>';
                        html+='<li><a id="' + this.divName + 'editdocument"><i class="icomoon-pencil"></i></a></li>';
                    html+='</ul>';
                html+='</div>';
            html+='</div>';       
            html+='<div id="' + this.divName + 'filetree"></div>';
        html+='</div>';

        html+='</div>';
        html+='</div>';
        html+='<div id="' + this.divName + 'busyDialog"></div>';
        html+='<div id="' + this.divName + 'filechooser"></div>';
        html+='<div id="' + this.divName + 'propertiesDialog" class="dialog"></div>';
        html+='<div id="' + this.divName + 'inputsDialog" class="dialog"></div>';
        html+='<div id="' + this.divName + 'outputsDialog" class="dialog"></div>';
        html+='<div id="' + this.divName + 'settingsDialog" class="dialog"></div>';
        html+='<div id="' + this.divName + 'aclDialogHolder"></div>';
        html+='<div id="' + this.divName + 'confirmDialog"></div>';
        html+='<div id="' + this.divName + 'inputDialog"></div>';
        div.innerHTML = html;
        
        this.versions.init(this.divName + "versions");
        this.versions.builder = this;
        
        var builder = this;
        this.container = $('#' + this.divName).layout({ 
            applyDefaultStyles: false,
            north__size: 38,
            north__resizable: false,
            north__closable: false,
            north__spacing_open: 1,
            east__initClosed: true,
            south__resizable: true,
            south__size: 20,
            east__resizable: true,
            west__initClosed: true,
            east__onopen_end: function(){
                builder.versions.enable();
            },
            east__onclose_end: function(){
                builder.versions.disable();
            },
            center__onresize: function(){
                builder.resizeEditors();
            }
        });
        
        var $tabs = $( "#" + this.divName + "tabs").tabs({
                tabTemplate: "<li><a href='#{href}'>#{label}</a> <span style='float: left;' class='ui-icon ui-icon-close'>Remove Tab</span></li>",
                add: function( event, ui ) {
                        return $( ui.panel ).append( "<p></p>" );
                },
                activate: function(event, ui){
                    if(ui && ui.newPanel && ui.newPanel.length===1){
                        builder.tabSelected(ui.newPanel[0]);
                    }
                },
                remove: function(event, ui){
                    builder.tabRemoved(ui.panel);
                }
        });
        
        $('#' + this.divName + "tabs").removeClass("ui-corner-all");
        $('#' + this.divName + "tabs").css("border", "none");
                
        this.busyDialog.init(this.divName + "busyDialog");
        this.confirmDialog.init(this.divName + "confirmDialog");
        this.inputDialog.init(this.divName + "inputDialog");
        
        
        document.getElementById(this.divName + "createblock").onclick = function(){
            var wiz= new BlockWizard();
            wiz.createWindow();
        };
        
       
       
        document.getElementById(this.divName + "openblock").onclick = function(){
            builder.browseForBlock();
        };
       
        
        document.getElementById(this.divName + "packageblock").onclick = function(){
            builder.packageBlock();
        };
        
        document.getElementById(this.divName + "savefiles").onclick = function(){
            builder.saveFiles();
        };
       
        document.getElementById(this.divName + "security").onclick = function(){
            builder.showSecurityDialog();
        };
        
        document.getElementById(this.divName + "copyfile").onclick = function(){
            builder.copyFileIntoBlock();
        };
        
        document.getElementById(this.divName + "refreshtree").onclick = function(){
            builder.refreshFileList();
        };
        
        document.getElementById(this.divName + "deletefile").onclick = function(){
            builder.showDeleteDialog();
        };
        
        document.getElementById(this.divName + "newdocument").onclick = function(){
            builder.showCreateDialog();
        };          
        
        document.getElementById(this.divName + "editdocument").onclick = function(){
            if(builder.fileTree.selectedFileId){
                builder.editDocument(builder.fileTree.selectedFileId);
            }
        };

        this.fileTree.init(this.divName + "filetree");
        this.fileChooser.init(this.divName + "filechooser");
        

        
        $( "#" + this.divName + "tabs span.ui-icon-close" ).live( "click", function() {
            var index = $( "li", $tabs ).index( $( this ).parent() );
            var editor = builder.editors.get(index);
            if(editor){
                if(editor.isDirty){
                    builder.confirmDialog.yesCallback = function(){
                        $("#" + builder.divName + "tabs").tabs( "remove", index );
                    };
                    var name;
                    if(editor.doc){
                        name = editor.doc.name;
                    } else {
                        name = "This Document";
                    }
                    builder.confirmDialog.show(name + " has not been saved. Are you sure you want to close the editor");                    
                } else {
                    $("#" + builder.divName + "tabs").tabs( "remove", index );
                }
            } else {
                $("#" + builder.divName + "tabs").tabs( "remove", index );
            }
        });        
        
        this.fileTree.fileDoubleClickCallback = function(id, name){
            builder.editDocument(id);
        };
        
        this.setButtonStatus();
    }
};

/** Close all the editors */
BlockBuilder.prototype.closeAll = function(){
    while($("#" + this.divName + "tabs").tabs('length')>0){
        $("#" + this.divName + "tabs").tabs('remove', 0);
    }
};

/** A tab has been removed */
BlockBuilder.prototype.tabRemoved = function(tab){
    if(tab.editor){
        if(tab.editor.doc){
            var editorIndex = this.findIndexOfEditor(tab.editor.doc.id);
            if(editorIndex!=-1){
                this.editors.remove(editorIndex);
            }
        }
        
        if(tab.editor===this.selectedEditor){
            this.selectedEditor = null;
            this.versions.clearDocument();
        }
    }
};

/** A tab has been selected */
BlockBuilder.prototype.tabSelected = function(tab){
    if(tab && tab.editor){
        this.selectedEditor = tab.editor;
        
        // Refresh the versions
        this.versions.setDocument(tab.editor.doc);
    } else {
        this.versions.clearDocument();
    }
};

/** Package the current block */
BlockBuilder.prototype.packageBlock = function(){
    if(this.currentBlock!=null && this.currentBlock!=undefined){
        var builder = this;
        var execFunc = {
            func: function(){
                builder.currentBlock.uploadBlock();
            },
            caption: "Deploying block..."
        };
        
        this.runTaskInBusyDialog(execFunc);        
        //this.currentBlock.uploadBlock();
    }
};

BlockBuilder.prototype.showSecurityDialog = function(){
    if(this.currentBlock!=null && this.currentBlock.topFolder){
        $("#" + this.divName + "aclDialogHolder").remove(".acl");
        //add a new ACL
        $("#" + this.divName + "aclDialogHolder").append('<div id="' + this.divName + 'securitydialog" class="acl"></div>');        
        this.aclDialog.dialogWidth = 630;
        this.aclDialog.dialogHeight = 500;
        this.aclDialog.init(this.divName + "securitydialog", _userId, this.currentBlock.topFolder.id, _publicUserId, _enablePublic, _usersGroupId);
        this.aclDialog.saveCallback = function(id){
            APIClient.prototype.propagateFolderPermissions(id);
        }
        this.aclDialog.open();        
    }
};

/** A file has been saved. Need to check if it is a special file that needs some
 * extra processing */
BlockBuilder.prototype.fileSaved = function(doc){
    if(this.builder.currentBlock && this.builder.currentBlock.srcFolder){
        if(doc.containerId == this.builder.currentBlock.srcFolder.id){
            // Doc is in the src folder
            if(doc.name=="service.xml"){
                // This is the service XML file - reparse it so that it doesn't get overwritten
                this.builder.currentBlock.forceParseServiceXml();
            }
            
        }
    }
};

/** Reload the service xml file if it is open */
BlockBuilder.prototype.reloadServiceXml = function(){
    if(this.currentBlock && this.currentBlock.srcFolder){
        var doc;
        for(var i=0;i<this.editors.size();i++){
            if(this.editors.get(i) instanceof SimpleEditorPanel){
                doc = this.editors.get(i).doc;
                if(doc.containerId==this.currentBlock.srcFolder.id && doc.name=="service.xml"){
                    var reloadDoc = APIClient.prototype.getFileContentsById(doc.id);
                    this.editors.get(i).openDocument(reloadDoc);
                    this.resizeEditors();
                    
                    // Reload versions if this file is visible
                    if(this.versions.doc && this.versions.doc.id==doc.id){
                        if(doc.version){
                            this.versions.selectedVersionId = doc.version.id;
                        }
                        this.versions.refreshVersions();
                    }
                }
            }
        }
    }    
    
};

/** Save the open files */
BlockBuilder.prototype.saveFiles = function(){
    for(var i=0;i<this.editors.size();i++){
        if(this.editors.get(i).save){
            if(this.editors.get(i).isDirty){
                this.editors.get(i).save();
            }
        }
    }
};

/** Show the filechooser and select a block project file */
BlockBuilder.prototype.browseForBlock = function(){
    var builder = this;
    this.fileChooser.okCallback = function(chooser){
        var fileId = chooser.selectedFileId;
        var projectFile = APIClient.prototype.getFileById(fileId);
        if(projectFile instanceof DocumentRecord){
            builder.openBlock(projectFile);
        } else {
            builder.log("Error opening file");
        }
    };
    this.fileChooser.showHomeFolder(false);
};

BlockBuilder.prototype.copyFileIntoBlock = function(){
    var builder = this;
    this.fileChooser.okCallback = function(chooser){
        var fileId = chooser.selectedFileId;
        var folderId = builder.fileTree.selectedFolderId;
        if(!folderId){
            folderId = builder.currentBlock.srcFolder.id;
        }
        if(fileId && folderId){
            APIClient.prototype.copyFile(fileId, folderId);
            builder.fileTree.showFolder(builder.currentBlock.srcFolder.id);
        }
    };
    this.fileChooser.showHomeFolder(false);    
};

BlockBuilder.prototype.refreshFileList = function(){
    if(this.currentBlock && this.currentBlock.srcFolder){
        this.fileTree.showFolder(this.currentBlock.srcFolder.id);
    }
};

BlockBuilder.prototype.showDeleteDialog = function(){
    var builder = this;
    var fileId = this.fileTree.selectedFileId;
    var fileName = this.fileTree.selectedFileName;
    if(fileId){
        this.confirmDialog.yesCallback = function(){
            builder.deleteFile(fileId);
        };
        this.confirmDialog.show("Are you sure you want to delete: " + fileName);
    }
};

BlockBuilder.prototype.showCreateDialog = function(){
    if(this.currentBlock && this.currentBlock.srcFolder){
        this.inputDialog.title = "Create a new file in: " + this.fileTree.selectedFolderName;
        var builder = this;
        this.inputDialog.okCallback = function(value){
            var file = new DocumentRecord();
            file.name = value;
            file.content = "";
            file.containerId = builder.fileTree.selectedFolderId;
            file.id = "";
            var folder = new Folder();
            if(builder.fileTree.selectedFolderId){
                folder.id = builder.fileTree.selectedFolderId;
            } else {
                folder.id = builder.currentBlock.srcFolder.id;
            }
            var saved = APIClient.prototype.setFileContents(folder, file);
            builder.fileTree.showFolder(builder.currentBlock.srcFolder.id);
            builder.editDocument(saved.id);
        };
        this.inputDialog.show("");
    }
};

BlockBuilder.prototype.deleteFile = function(id){
    if(this.currentBlock && this.currentBlock.srcFolder){
        APIClient.prototype.deleteFile(id);
        this.fileTree.showFolder(this.currentBlock.srcFolder.id);
    }
};

BlockBuilder.prototype.openBlockUsingId = function(fileId){
    var projectFile = APIClient.prototype.getFileById(fileId);
    if(projectFile instanceof DocumentRecord){
        this.openBlock(projectFile);
    } else {
        this.log("Error opening file");
    }
};

BlockBuilder.prototype.resize = function(){
    var MZ = (document.getElementById ? true : false);
    var IE = (document.all ? true : false);
    var winHeight = IE ? document.body.clientHeight : window.innerHeight;
    if (winHeight > 230)
    {
        var newHeight = winHeight - 220;
        if (IE)
        {
            document.all[this.divName].style.height = newHeight + "px";
            document.all['positionPush'].style.height = newHeight + "px";
        }
        else if (MZ)
        {
            document.getElementById(this.divName).style.height = newHeight + "px";
            document.getElementById('positionPush').style.height = newHeight + "px";
        }
    }
    
    var winWidth = IE ? document.body.clientHeight : window.innerWidth;
    if(winWidth>1000){
        var newWidth = winWidth - 40;
    }
    if(IE){
        document.all[this.divName].style.width = newWidth + "px";
    } else if(MZ){
        document.getElementById(this.divName).style.width = newWidth + "px";
    }
    
    this.container.resizeAll();
    this.resizeEditors();
    this.versions.resize();
};

BlockBuilder.prototype.resizeEditors = function(){
    var tabnav = $("#" + this.divName + "tabs").children(".ui-tabs-nav")[0];
    var center = document.getElementById(this.divName + "tabs");
    var w = center.clientWidth - 30;
    var h = center.clientHeight - tabnav.clientHeight - 10;
    for(var i=0;i<this.editors.size();i++){
        if(this.editors.get(i).resizeTo){
            this.editors.get(i).resizeTo(w, h);
        }
    }    
};

/** Change the displayed version of the currently visible document */
BlockBuilder.prototype.switchDocumentVersion = function(version){
    if(this.isDocumentOpen(version.documentId) && this.selectedEditor instanceof SimpleEditorPanel && this.selectedEditor.doc!=null && this.selectedEditor.doc.id==version.documentId){
        // Ok to switch
        var doc = APIClient.prototype.getFileContentsById(this.selectedEditor.doc.id, version.id);
        this.selectedEditor.openDocument(doc);
        this.versions.refreshVersions();
        this.resizeEditors();
    }
};

/** Edit a document */
BlockBuilder.prototype.editDocument = function(id){
    if(!this.isDocumentOpen(id)){
        var doc = APIClient.prototype.getFileContentsById(id);
        var tab = this.createTab(doc.name);
        var editor = new SimpleEditorPanel();
        editor.init(tab.id);
        editor.openDocument(doc);
        editor.saveCallback = this.fileSaved;
        editor.builder = this;
        editor.containerTab = tab;
        tab.editor = editor;
        this.editors.add(editor);
        this.selectedEditor = editor;
        this.versions.setDocument(doc);
        this.resizeEditors();
    }
};

/** Is a document already open */
BlockBuilder.prototype.isDocumentOpen = function(id){
    for(var i=0;i<this.editors.size();i++){
        if(this.editors.get(i) instanceof SimpleEditorPanel){
            if(this.editors.get(i) && this.editors.get(i).doc && this.editors.get(i).doc.id==id){
                return true;
            }
        }
    }
    return false;
};

/** Find the index of a document editor */
BlockBuilder.prototype.findIndexOfEditor = function(id){
    for(var i=0;i<this.editors.size();i++){
        if(this.editors.get(i) instanceof SimpleEditorPanel){
            if(this.editors.get(i) && this.editors.get(i).doc && this.editors.get(i).doc.id==id){
                return i;
            }
        }
    }
    return -1;
};

/** Create a new tab */
BlockBuilder.prototype.createTab = function(name){
    this.tabCounter++;
    $("#" + this.divName + "tabs").tabs( "add", "#tabs-" + this.tabCounter, name);
    var tab = document.getElementById("tabs-" + this.tabCounter);
    var size = $("#" + this.divName + "tabs").tabs("length");
    $("#" + this.divName + "tabs").tabs("select", size - 1);
    tab.counter = this.tabCounter;
    return tab;
};

BlockBuilder.prototype.showGettingStarted = function(){
    var gs = new GettingStartedPanel();
    gs.builder = this;
    var tab = this.createTab("Welcome");
    gs.init(tab.id);
};

/** Create a new dialog window and add it to the window list */
BlockBuilder.prototype.createNewWindow = function(properties){
    var id = "window_" + new Date().getTime();
    var div = document.createElement("div");
    div.setAttribute("id", id);
    div.setAttribute("class", "dialog");
    
    var bodyDiv = document.getElementById(this.divName);
    bodyDiv.appendChild(div);

    var width;
    if(properties.width){
        width = properties.width;
    } else {
        width = 500;
    }

    var height;
    if(properties.height){
        height = properties.height;
    } else {
        height = 450;
    }

    var title;
    if(properties.title){
        title = properties.title;
    } else {
        title = "Window";
    }

    var buttons;
    if(properties.buttons){
        buttons = properties.buttons;
    } else {
        buttons = {};
    }

    var dialog = $('#' + id).dialog({
        bgiframe: true,
        autoOpen: false,
        height: height,
        width: width,
        title: title,
        modal: false,
        shell: this,
        buttons: buttons,
        close: function(event, ui){
            
        }
    });
    $('#' + id).dialog('open');

    return dialog;
};

BlockBuilder.prototype.registerBlockType = function(blockType){
    this.blocktypes.add(blockType);
};

BlockBuilder.prototype.getBlockTypeCount = function(){
    return this.blocktypes.size();
};

BlockBuilder.prototype.getBlockType = function(index){
    return this.blocktypes.get(index);
};

BlockBuilder.prototype.findBlockType = function(blockTypeLabel){
    for(var i=0;i<this.blocktypes.size();i++){
        if(this.blocktypes.get(i).label==blockTypeLabel){
            return this.blocktypes.get(i);
        } 
    }
    return null;
};

BlockBuilder.prototype.runTaskInBusyDialog = function(executeObj){
    this.busyDialog.show(executeObj);
};

BlockBuilder.prototype.log = function(message){
    var consoleDiv = document.getElementById(this.divName + "console");
    consoleDiv.innerHTML = consoleDiv.innerHTML + '<p style="margin-bottom: 0px;">' + message + '</p>';
    consoleDiv.scrollTop = consoleDiv.scrollHeight;    
};

BlockBuilder.prototype.copyBlockIcon = function(folder, fileName){
    APIClient.prototype.copyBlockIconFile(folder, fileName);
};

BlockBuilder.prototype.copyTemplateFileIfNotPresent = function(folder, fileName, sourceFile, fileList, replacements){
    var needToCopy = true;
    
    if(fileList){
        // List present, check this list
        for(var i=0;i<fileList.length;i++){
            if(fileList[i].name==fileName){
                needToCopy = false;
            }
        }
    } else {
        // Need to check file first using API
        if(APIClient.prototype.getFile(folder, fileName)==null){
            needToCopy = true;
        } else {
            needToCopy = false;
        }
    }
    
    var replacer;
    if(needToCopy){
        if(sourceFile instanceof DocumentRecord){
            sourceFile.description = "Created by block wizard";
            sourceFile.id = "";
            if(replacements){
                replacer = new StringReplacer(sourceFile.content);
                sourceFile.content = replacer.replace(replacements);
            }
            APIClient.prototype.setFileContents(folder, sourceFile);
            globalBuilder.log("Copied file: " + fileName);
            
        } else {
            var fileContent = APIClient.prototype.getUrlContentAsString(sourceFile);
            var doc = new DocumentRecord();
            doc.name = fileName;
            doc.description = "Created by block editor";
            if(replacements){
                replacer = new StringReplacer(fileContent);
                doc.content = replacer.replace(replacements);
            } else {
                doc.content = fileContent;
            }
            
            doc.id = "";
            APIClient.prototype.setFileContents(folder, doc);
            globalBuilder.log("Copied file: " + fileName);
        }
    }
};

BlockBuilder.prototype.createFolderIfNotPresent = function(parentFolder, folderName){
    var folder;
    try {
        folder = APIClient.prototype.getFolder(parentFolder, "child", folderName);
    } catch (err){
        folder = new Folder();
        folder.name = folderName;
        folder = APIClient.prototype.saveFolder(parentFolder, folder);
        globalBuilder.log("Created folder: " + folderName);
    }    
    return folder;
};

/** Open a block with a document */
BlockBuilder.prototype.openBlock = function(projectFile){
    this.closeAll();
    this.container.open('west');
    var newBlock = new Block();
    try {
        newBlock.initWithProjectFile(projectFile);
        this.setCurrentBlock(newBlock);
    } catch (err){
        this.log("Error opening block: " + err);
    }
};

/** Set and display the current block */
BlockBuilder.prototype.setCurrentBlock = function(block){
    this.currentBlock = block;
    this.closeAll();
    
    if(this.currentBlock!=null && this.currentBlock!=undefined){
        this.fileTree.showFolder(this.currentBlock.srcFolder.id);
    } else {
        
    }
    this.container.open('west');
    this.container.open('east');
    
    // Enable / disable buttons depending on block type
    this.setButtonStatus();
};

BlockBuilder.prototype.setButtonStatus = function(){
    // Enable / disable buttons depending on block type
    /*
    if(this.currentBlock){
        $("#" + this.divName + "savefiles").button("enable");        
        $("#" + this.divName + "security").button("enable");        
        if(this.currentBlock.isDeployable){
            $("#" + this.divName + "packageblock").button("enable");            
        } else {
            $("#" + this.divName + "packageblock").button("disable");            
        }
    } else {       
        $("#" + this.divName + "savefiles").button("disable");        
        $("#" + this.divName + "security").button("disable");        
        $("#" + this.divName + "packageblock").button("disable");        
    }
    */
};

var globalBuilder = new BlockBuilder();