/* 
 * This script provides a helper object for a binary library
 */
function BinaryLibraryHelper(){
    this.block = null;
    this.supportScriptFolder = null;
    this.blockTypeLabel = "BinaryLibrary"; 
    this.isBlock = false;
    this.isDeployable = true;
}

BinaryLibraryHelper.prototype.createInstance = function(block){
    var helper = new BinaryLibraryHelper();
    if(block){
        helper.block = block;
        block.helper = helper;
        block.hasServiceXml = helper.isBlock;
    }
    return helper;
};

BinaryLibraryHelper.prototype.validate = function(wizardData){    
    
    // Build a list of parameter replacements
    var replacements;
    if(wizardData && wizardData.replacements){
        replacements = wizardData.replacements;
    } else {
        replacements = {
            names: ["name"],
            values: ["NewLibrary"]
        }
    }
    
    // Standard block files
    var contents = APIClient.prototype.getFolderContents(this.block.srcFolder);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "dependencies.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/binarylibrary/templates/dependencies.xml"), contents);
    
    // Use the wizard data code if present
    if(wizardData && wizardData.blockCode){
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", wizardData.blockCode, contents, replacements);
    } else {
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/binarylibrary/templates/library.xml"), contents, replacements);
    }    
    
};


BinaryLibraryHelper.prototype.prepareZip = function(){
    
};

BinaryLibraryHelper.prototype.getBlockMainCode = function(){
    if(this.block.srcFolder){
        var doc = APIClient.prototype.getFileContents(this.block.srcFolder, "library.xml");
        return doc;
    } else {
        throw "No source folder present";
    }
};

BinaryLibraryHelper.prototype.getBlockMainCodeTemplateAsDocument = function(){
    var code = APIClient.prototype.getUrlContentAsString(rewriteAjaxUrl("../../scripts/blockbuilder/blocks/binarylibrary/templates/library.xml"));
    var doc = new DocumentRecord();
    doc.name = "library.xml";
    doc.content = code;
    return doc;
};

globalBuilder.registerBlockType(new BlockType("BinaryLibrary", "A library object that can contain binary code or call one of a number of package managers to install code into a workflow engine", BinaryLibraryHelper, false));


