/* 
 * This script encapsulates the service XML file for a block and provides
 * methods for parsing and writing it. It is also used as a data store
 * by the service XML ui panel
 */
function ServiceXML(block){
    this.block = block;
    this.retrieved = false;
    this.xmlDoc = null;
    this.xmlDocumentRecord = "";
    this.name = "";
    this.category = "";
    this.description = "";
    this.homepage = "";
    this.streamMode = "nostream";
    this.idempotent = true;
    this.deterministic = true;    
    this.serviceRoutine = "";
    this.serviceType = "AUTO";
    this.properties = new List();
    this.inputs = new List();
    this.outputs = new List();
}

ServiceXML.prototype.fetchIfNeeded = function(){
    if(!this.retrieved){
        this.fetch();
    }
    
    return this.retrieved;
};

ServiceXML.prototype.fetch = function(){
    if(this.block && this.block.srcFolder){
        this.xmlDocumentRecord = APIClient.prototype.getFileContents(this.block.srcFolder, "service.xml");
        this.xmlDoc = loadXMLString(this.xmlDocumentRecord.content, "text/xml");
        this.retrieved = true;
    }    
};

ServiceXML.prototype.parse = function(){
    if(this.retrieved){
        var name;
        var node = this.xmlDoc.childNodes[0];
        for(var i=0;i<node.childNodes.length;i++){
	
            name = node.childNodes[i].nodeName.toLowerCase();
            
            if(name=="name"){
                this.name = node.childNodes[i].textContent;
            } else if(name=="description"){
                this.description = node.childNodes[i].textContent;
            } else if(name=="category"){
                this.category = node.childNodes[i].textContent;
            } else if(name=="homepage"){
                this.homepage = node.childNodes[i].textContent;
            } else if(name=="streammode"){
                this.streamMode = node.childNodes[i].textContent;
            } else if(name=="serviceroutine"){
                this.serviceRoutine = node.childNodes[i].textContent;
            } else if(name=="streammode"){
                this.streamMode = node.childNodes[i].textContent;
            } else if(name=="idempotent"){
                if(node.childNodes[i].textContent.toLowerCase()=="true"){
                    this.idempotent = true;
                } else {
                    this.idempotent = false;
                }
            } else if(name=="deterministic"){
                if(node.childNodes[i].textContent.toLowerCase()=="true"){
                    this.deterministic = true;
                } else {
                    this.deterministic = false;
                }
            } else if(name=="properties"){
                this.processProperties(node.childNodes[i]);
            } else if(name=="inputs"){
                this.processInputs(node.childNodes[i]);
            } else if(name=="outputs"){
                this.processOutputs(node.childNodes[i]);
            }
        }        
    }
};

// Save and upload document
ServiceXML.prototype.save = function(){
    var xml = this.toXml();
    if(this.xmlDocumentRecord){
        this.xmlDocumentRecord.content = xml;
        APIClient.prototype.setFileContents(null, this.xmlDocumentRecord);
    }
};

// Save to an XML string
ServiceXML.prototype.toXml = function(){
    var xml = "<WorkflowService>\n";
    xml+="    <!-- Name of the service, and also the caption that will appear     -->\n";
    xml+="    <!-- in the top line of the block on the workflow editor            -->\n";
    xml+="    <Name>" +this.name + "</Name>\n\n";
    
    xml+="    <!-- Service description that appears at the bottom of the editor   -->\n";
    xml+="    <!-- window when the block is selected                              -->\n";
    xml+="    <Description>" + this.description + "</Description>\n\n";
    
    xml+="    <!-- Category to place the service in on the editor palette         -->\n";
    xml+="    <Category>" + this.category + "</Category>\n\n";
    
    xml+="    <!-- Homepage for block documentation                               -->\n";    
    xml+="    <Homepage>" + this.homepage + "</Homepage>\n\n";
    
    xml+="    <!-- Class name of the service. DO NOT CHANGE                       -->\n";
    xml+="    <ServiceRoutine>" + this.serviceRoutine + "</ServiceRoutine>\n\n";
    
    xml+="    <!-- Auto deployed service. Do NOT change for dynamically deployed  -->\n";
    xml+="    <!-- services that are uploaded via this editor                     -->\n";
    xml+="    <ServiceType>" + this.serviceType + "</ServiceType>\n\n";

    xml+="    <!-- Data streaming mode for this service. This can be one of:      -->\n";
    xml+="    <!--                                                                -->\n";
    xml+="    <!-- nostream   - Data is passed in one block through service       -->\n";
    xml+="    <!-- sequential - Data is streamed one connection at a time         -->\n";
    xml+="    <!-- parallel   - Data is streamed from all connections in parallel -->\n";
    xml+="    <StreamMode>" + this.streamMode + "</StreamMode>\n\n";
    
    xml+="    <!-- Is this service idempotent, i.e. does it have any side effects -->\n";
    xml+="    <!-- true - This service does not have any side effects             -->\n";
    xml+="    <!-- false - This service has side effects                          -->\n";
    if(this.idempotent){
        xml+="    <Idempotent>true</Idempotent>\n\n";
    } else {
        xml+="    <Idempotent>false</Idempotent>\n\n";
    }
   
    xml+="    <!-- Is this service deterministic.  Will it always return the same -->\n";
    xml+="    <!-- output for a given input?                                      -->\n";
    xml+="    <!-- true - This service is deterministic                           -->\n";
    xml+="    <!-- false - This service is not deterministic                      -->\n";
    if(this.deterministic){
        xml+="    <Deterministic>true</Deterministic>\n\n";
    } else {
        xml+="    <Deterministic>false</Deterministic>\n\n";
    }
    
    xml+="    <!-- Editable service parameters. These properties define what is   -->\n";
    xml+="    <!-- displayed in the properties panel when a block is selected in  -->\n";
    xml+="    <!-- the workflow editor. The format of properties is:              -->\n";
    xml+="    <!--                                                                -->\n";
    xml+='    <!-- <Property name="" type="" description="" default=""/>          -->\n';
    xml+="    <!--                                                                -->\n";
    xml+="    <!-- Where:     name = property name without spaces                 -->\n";
    xml+="    <!--            type = Document - file reference                    -->\n";
    xml+="    <!--                   Folder - folder reference                    -->\n";
    xml+="    <!--                   Integer - integer paramater                  -->\n";
    xml+="    <!--                   Boolean - true / false value                 -->\n";
    xml+="    <!--                   String - text parameter                      -->\n";
    xml+="    <!--                   Double - floating point value                -->\n";
    xml+="    <!--                   Date - java date parameter                   -->\n";
    xml+="    <!--                   StringList - vector of text values           -->\n";
    xml+="    <!--                   TwoColumnList - two columns of text values   -->\n";
    xml+="    <!--                   ExternalObject - Application defined object  -->\n";
    xml+="    <!--        category = Category section for block on editor         -->\n";
    xml+="    <Properties>\n";
    for(var i=0;i<this.properties.size();i++){
        xml+="        " + this.properties.get(i).createXmlTag() + "\n";
    }
    xml+="    </Properties>\n\n";
    
    xml+="    <!-- Definition of all of the inputs to a service. The format is:   -->\n";
    xml+="    <!--                                                                -->\n";
    xml+='    <!-- <Input name="" type="" streaming=""/>                          -->\n';
    xml+="    <!--                                                                -->\n";
    xml+="    <!-- Where:     name = name of input also displayed on connections  -->\n";
    xml+="    <!--            type = data-wrapper - mixed matrix of data          -->\n";
    xml+="    <!--                   file-wrapper - list of file names            -->\n";
    xml+="    <!--                   object-wrapper - Serialized Java object      -->\n";
    xml+="    <!--            streaming = true / false - is this a streaming link -->\n"; 
    xml+="    <Inputs>\n";
    for(var i=0;i<this.inputs.size();i++){
        xml+="        " + this.inputs.get(i).createXmlTag() + "\n";
    }
    xml+="    </Inputs>\n\n";
    
    xml+="    <!-- Definition of all of the outputs from service. The format is:  -->\n";
    xml+="    <!--                                                                -->\n";
    xml+='    <!-- <Output name="" type="" streaming=""/>                         -->\n';
    xml+="    <!--                                                                -->\n";
    xml+="    <!-- Where:     name = name of input also displayed on connections  -->\n";
    xml+="    <!--            type = data-wrapper - mixed matrix of data          -->\n";
    xml+="    <!--                   file-wrapper - list of file names            -->\n";
    xml+="    <!--                   object-wrapper - Serialized Java object      -->\n";
    xml+="    <!--            streaming = true / false - is this a streaming link -->\n";   
    xml+="    <Outputs>\n";
    for(var i=0;i<this.outputs.size();i++){
        xml+="        " + this.outputs.get(i).createXmlTag() + "\n";
    }    
    xml+="    </Outputs>\n\n";
    xml+="</WorkflowService>";
    
    return xml;
};

ServiceXML.prototype.processProperties = function(propertyNode){
    var node;
    var property;
    this.properties.clear();
    for(var i=0;i<propertyNode.childNodes.length;i++){
        node = propertyNode.childNodes[i];
        if(node.nodeName!=undefined && node.nodeName.toLowerCase()=="property"){
            property = new BlockProperty();
            property.name = node.getAttribute("name");
            property.dataType = node.getAttribute("type");
            property.defaultValue = node.getAttribute("default");
            property.description = node.getAttribute("description");
            if(node.getAttribute("category")!=null && node.getAttribute("category")!=""){
                property.category = node.getAttribute("category");
            } else {
                property.category = "";
            }
            this.properties.add(property);
        }
    }
};

ServiceXML.prototype.processInputs = function(inputsNode){
    var node;
    var input;
    this.inputs.clear();
    for(var i=0;i<inputsNode.childNodes.length;i++){
        node = inputsNode.childNodes[i];
        if(node.nodeName!=undefined && node.nodeName.toLowerCase()=="input"){
            input = new BlockIO();
            input.name = node.getAttribute("name");
            input.type = node.getAttribute("type");
            input.isInput = true;
            if(node.getAttribute("streaming") && node.getAttribute("streaming").trim().toLowerCase()=="true"){
                input.streaming = true;
            } else {
                input.streaming = false;
            }
            this.inputs.add(input);
        }
    }    
};

ServiceXML.prototype.processOutputs = function(outputsNode){
    var node;
    var output;
    this.outputs.clear();
    for(var i=0;i<outputsNode.childNodes.length;i++){
        node = outputsNode.childNodes[i];
        if(node.nodeName!=undefined && node.nodeName.toLowerCase()=="output"){
            output = new BlockIO();
            output.name = node.getAttribute("name");
            output.type = node.getAttribute("type");
            output.isInput = false;
            this.outputs.add(output);
        }
    }       
};

ServiceXML.prototype.getPropertyCount = function(){
    return this.properties.size();
};

ServiceXML.prototype.getProperty = function(index){
    return this.properties.get(index);
};

ServiceXML.prototype.addProperty = function(property){
    this.properties.add(property);
};

ServiceXML.prototype.removeProperty = function(property){
    this.properties.remove(property);
};

ServiceXML.prototype.getInputCount = function(){
    return this.inputs.size();
};

ServiceXML.prototype.getInput = function(index){
    return this.inputs.get(index);
};

ServiceXML.prototype.addInput = function(input){
    this.inputs.add(input);
};

ServiceXML.prototype.removeInput = function(input){
    this.inputs.remove(input);
};

ServiceXML.prototype.getOutputCount = function(){
    return this.outputs.size();
};

ServiceXML.prototype.getOutput = function(index){
    return this.outputs.get(index);
};

ServiceXML.prototype.addOutput = function(output){
    this.outputs.add(output);
};

ServiceXML.prototype.removeOutput = function(output){
    this.outputs.remove(output);
};