/* 
 * This script can create the required files to generate a Javascript block
 * in a user folder
 */
function JavascriptBlockHelper(){
    this.block = null;
    this.userScriptFolder = null;
    this.blockTypeLabel = "JavaScript";
    this.isBlock = true;
    this.isDeployable = true;
}

JavascriptBlockHelper.prototype.createInstance = function(block){
    var helper = new JavascriptBlockHelper();
    if(block){
        helper.block = block;
        block.helper = helper;
        block.hasServiceXml = helper.isBlock;
    }
    return helper;
};

JavascriptBlockHelper.prototype.validate = function(wizardData){
    this.userScriptFolder = globalBuilder.createFolderIfNotPresent(this.block.srcFolder, "scripts");
    
    // Build a list of parameter replacements
    var replacements;
    if(wizardData && wizardData.replacements){
        replacements = wizardData.replacements;
    } else {
        replacements = {
            names: ["name", "description","category"],
            values: ["NewBlock","Javascript block","My Services"]
        }
    }
    
    // Standard block files
    var contents = APIClient.prototype.getFolderContents(this.block.srcFolder);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "service.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/javascript/templates/service.xml"), contents, replacements);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/javascript/templates/library.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "dependencies.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/javascript/templates/dependencies.xml"), contents);
    globalBuilder.copyBlockIcon(this.block.srcFolder, "javascript.jpg");
    
    // Use the wizard data code if present
    if(wizardData && wizardData.blockCode){
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.js", wizardData.blockCode, contents);
    } else {
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.js", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/javascript/templates/main.js"), contents);
    }
};

JavascriptBlockHelper.prototype.prepareZip = function(){
    
};

JavascriptBlockHelper.prototype.getBlockMainCode = function(){
    if(this.block.srcFolder){
        var doc = APIClient.prototype.getFileContents(this.block.srcFolder, "main.js");
        return doc;
    } else {
        throw "No source folder present";
    }
};

JavascriptBlockHelper.prototype.getBlockMainCodeTemplateAsDocument = function(){
    var code = APIClient.prototype.getUrlContentAsString("../../scripts/blockbuilder/blocks/javascript/templates/main.js");
    var doc = new DocumentRecord();
    doc.name = "main.js";
    doc.content = code;
    return doc;
};

globalBuilder.registerBlockType(new BlockType("JavaScript", "JavaScript - is a cross-platform, object-based scripting language developed by Netscape for client and server applications. Workflow blocks created using Javascript are interpreted by the workflow engine and code can be prototyped and tested within the DataShell/Block Builder application", JavascriptBlockHelper, true));