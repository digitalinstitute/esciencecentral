/**
 * This javascript file contains the entry points for a user javascript block.
 * It contains the functions:
 * 
 * executionAboutToStart()
 * execute()
 * allDataProcessed();
 * 
 * These functions are called by the workflow engine and must be present here
 * in order for the block to function
 */

/** 
 * This function is called once when the block has been configured and all the
 * scripts have been parsed.
 */
function executionAboutToStart(){
    java.lang.System.out.println("Init");
}

/**
 * This function is called once for each chunk of data that is passed through the 
 * block.
 */
function execute(){
    var d = block.getInputDataSet("x");
    block.setOutputDataSet("y", d);
}

/**
 * This function is called once when all of the data has been processed and
 * the block is about to finish.
 */
function allDataProcessed(){
    java.lang.System.out.println("Terminate");
}