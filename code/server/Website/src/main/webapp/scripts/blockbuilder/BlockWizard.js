/* 
 * This script provides a simple wizard that is used to set the basic properties
 * of a block.
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/jquery-selectbox/jquery.selectBox.css">
//<script type="text/javascript" src="../../scripts/jquery-selectbox/jquery.selectBox.js"></script>

function BlockWizard(){
    this.editorWindow = null;      
    this.divId = "";
    this.mainDiv = null;
    this.titleDiv = null;
    this.titleText = null;
    this.blockName = "NewFile";
    this.blockDescription = "Project created by the project editor wizard";
    this.blockType = "JavaScript";
    this.blockCategory = "My Services";
    this.blockCodeDocument = null;
    this.helper = null;
    this.editorPanel = null;
    this.fileBrowser = null;
    this.targetFolderId = null;
    this.targetFolderName = "";
    this.wizardStage = 0;
}

BlockWizard.prototype.createWindow = function(){
    var dialog = this;
    var d = globalBuilder.createNewWindow({title: "New Project Wizard", width: 700, height:500, buttons: {'Cancel': function(){$(this).dialog('close');}, 'Back': function(){dialog.back();}, 'Next': function(){dialog.next();}}});

    this.editorWindow = d[0];
    this.editorWindow.ownerObject = this;
    this.divId = this.editorWindow.id;    
    
    this.mainDiv = document.createElement("div");
    this.mainDiv.setAttribute("style", "top: 40px; position: absolute;overflow:auto");
    this.mainDiv.setAttribute("id", this.divId + "_main_div");
    this.editorWindow.appendChild(this.mainDiv);
    
    this.titleDiv = document.createElement("div");
    this.titleDiv.setAttribute("style", "position: absolute; top: 2px; left:0px; height:30px; width: 99%; font-size:16px;");
    this.titleDiv.setAttribute("class", "ui-corner-all");
    this.titleText = document.createElement("span");
    this.titleText.setAttribute("style", "margin-left:11px; position:relative; top:7px;");
    this.titleDiv.appendChild(this.titleText);
    this.editorWindow.appendChild(this.titleDiv);
    
    d.bind("dialogresize", function(){
        dialog.resizeUI();
    });       
    var f = APIClient.prototype.getHomeFolder();
    this.targetFolderId = f.id;
    this.targetFolderName = f.name;
    this.displayStage();
    this.resizeUI();
};

BlockWizard.prototype.next = function(){
    if(this.wizardStage<4){
        this.storeStageData();
        this.wizardStage++;
        this.displayStage();
    } else if(this.wizardStage==4){
        this.saveBlock();
    }
};

BlockWizard.prototype.back = function(){
    if(this.wizardStage>0){
        this.storeStageData();
        this.wizardStage--;
        this.displayStage();
    }
};

BlockWizard.prototype.storeStageData = function(){
   switch(this.wizardStage){
       case 1:
           this.blockName = document.getElementById(this.divId + "_stage_0_name").value;
           this.blockDescription = document.getElementById(this.divId + "_stage_0_description").value;
           if(document.getElementById(this.divId + "_stage_0_category")){
               this.blockCategory = document.getElementById(this.divId + "_stage_0_category").value;
           } else {
               this.blockCategory = "";
           }
           break;
           
       case 0:
           var newBlockType = document.getElementById(this.divId + "_stage_1_language").value;
           if(newBlockType!=this.blockType || this.helper==null){
               // Need to create a new helper
               var bt = globalBuilder.findBlockType(newBlockType);
               if(bt){
                   this.blockType = newBlockType;
                   this.helper = bt.helper.prototype.createInstance();
                   if(this.helper && this.helper.getBlockMainCodeTemplateAsDocument){
                       this.blockCodeDocument = this.helper.getBlockMainCodeTemplateAsDocument();
                   } else {
                       this.blockCodeDocument = new DocumentRecord();
                       this.blockCodeDocument.name = "Unknown";
                       this.blockCodeDocument.content = "";
                   }
               }
           }
           
           break;
           
       case 2:
           if(this.blockCodeDocument){
               this.blockCodeDocument.content = this.editorPanel.getCode();
               this.editorPanel = null;
           }
           break;
           
       case 3:
           break;
       
       case 4:

           break;
       default: 

   };    
};

BlockWizard.prototype.displayStage = function(){
   this.mainDiv.innerHTML = "";
   var html = "";
   var dialog = this;
   switch(this.wizardStage){
       case 1:
           // Basic name and information
           if(this.helper.isBlock){
                this.titleText.innerHTML = "Basic block properties";
           } else if(this.helper.isDeployable){
                this.titleText.innerHTML = "Basic library properties";
           } else {
                this.titleText.innerHTML = "Basic properties";
           }
           html+='<form onSubmit="return false">';
           html+='<div class="formInput">';
           if(this.helper.isBlock){
                html+='<label for="name">Block Name</label>';
           } else if(this.helper.isDeployable){
                html+='<label for="name">Library Filename</label>';
           } else {
                html+='<label for="name">Project Name</label>';
           }
           html+='<input type="text" size="40" name="name" id="' + this.divId + '_stage_0_name" value="' + this.blockName + '"></input>';
           html+="</div>";
           
           if(this.helper.isBlock){
            html+='<div class="formInput">';
            html+='<label for="category">Palette Category</label>';
            html+='<input type="text" size="40" name="category" id="' + this.divId + '_stage_0_category" value="' + this.blockCategory + '"></input>';
            html+="</div>";           
            html+='<div class="formInput">';                
           }
           
           if(this.helper.isBlock){
                html+='<label for="description">Block Description</label>';
           } else if(this.helper.isDeployable){
                html+='<label for="description">Library Description</label>';
           } else {
               html+='<label for="description">Project Description</label>';
           }
           html+='<textarea name="description" id="' + this.divId + '_stage_0_description">' + this.blockDescription + '</textarea>';
           html+="</div>";
           html+="</form>";
           this.mainDiv.innerHTML = html;
           break;
           
       case 0:
           // Block language
           this.titleText.innerHTML = "Select Project type";
           html+='<form onSubmit="return false">';
           html+='<div class="formInput">';
           html+='<label for="language">Project Type</label>';
           html+='<select name="language" id="' + this.divId + '_stage_1_language">';
           
           for(var i=0;i<globalBuilder.getBlockTypeCount();i++){
               html+='<option value="' + globalBuilder.getBlockType(i).label + '">' + globalBuilder.getBlockType(i).label + '</option>';
           }
           html+='</select>';


           html+="</div>";
           html+="</form>";           
           html+='<div class="blocklanguagedescription" id="' + this.divId + '_stage_1_language_description"></div>';
           this.mainDiv.innerHTML = html;
           document.getElementById(this.divId + "_stage_1_language").value = this.blockType;
           document.getElementById(this.divId + "_stage_1_language_description").innerHTML = this.getLanguageDescription(this.blockType);
           document.getElementById(this.divId + "_stage_1_language").onchange = function(){
               var description = dialog.getLanguageDescription(this.value);
               document.getElementById(dialog.divId + "_stage_1_language_description").innerHTML = description;
           }           
           $('#' + this.divId + "_stage_1_language").selectBox(); 

           break;
           
       case 2:
           if(this.helper.isBlock){
                this.titleText.innerHTML = "Block code";
           } else if(this.helper.isDeployable) {
                this.titleText.innerHTML = "Library descriptor";
           } else {
                this.titleText.innerHTML = "Main code";
           }
           this.mainDiv.innerHTML = "";
           if(this.blockCodeDocument){
               this.editorPanel = new SimpleEditorPanel();
               this.editorPanel.init(this.divId + "_main_div");
               this.editorPanel.openDocument(this.blockCodeDocument);
           }
           this.resizeUI();
           break;
           
       case 3:
           if(this.helper.isBlock){
                this.titleText.innerHTML = "Block storage location";
           } else if(this.helper.isDeployable){
                this.titleText.innerHTML = "Library storage location";
           } else {
                this.titleText.innerHTML = "Project storage location";
           }
           this.mainDiv.innerHTML = "";
           this.fileBrowser = new FileTree();
           var dialog = this;
           this.fileBrowser.folderSelectCallback = function(folderId, folderName){
               dialog.targetFolderId = folderId;
               dialog.targetFolderName = folderName;
               if(dialog.helper.isBlock){
                    dialog.titleText.innerHTML = "Block storage location: " + folderName;
               } else {
                    dialog.titleText.innerHTML = "Library storage location: " + folderName;
               }
           };
           
           this.fileBrowser.init(this.divId + "_main_div");
           this.fileBrowser.showHomeFolder();
           break;           
       
       case 4:
           this.titleText.innerHTML = "Summary";
           html+='<form onSubmit="return false">';
           // Name
           html+='<div class="formInput">';
           if(this.helper.isBlock){
                html+='<label for="name">Block Name</label>';
           } else if(this.helper.isDeployable){
                html+='<label for="name">Library Filename</label>';
           } else {
               html+='<label for="name">Project Filename</label>';
           }
           html+='<input type="text" readonly size="40" name="name" value="' + this.blockName + '"></input>';           
           html+='</div>';
           
           // Type
           html+='<div class="formInput">';
           html+='<label for="language">Object type</label>';
           html+='<input type="text" readonly size="40" name="language" value="' + this.blockType + '"></input>';                 
           html+='</div>';           
           
           // Destination
           html+='<div class="formInput">';
           if(this.helper.isBlock){
                html+='<label for="destination">Block Destination folder</label>';
           } else if(this.helper.isDeployable){
                html+='<label for="destination">Library Destination folder</label>';
           } else {
               html+='<label for="destination">Destination folder</label>';
           }
           html+='<input type="text" readonly size="40" name="destination" value="' + this.targetFolderName + '/' + this.blockName + '"></input>';                 
           
           html+='</div>';
           
           html+="</form>";
           this.mainDiv.innerHTML = html;
           break;
       default: 

   };
   
};

BlockWizard.prototype.saveBlock = function(){
    var wizardData = {
        folderId: this.targetFolderId,
        blockName: this.blockName,
        blockType: this.blockType,
        blockCode: this.blockCodeDocument,
        replacements: {
            names: ["name", "description", "category"],
            values: [this.blockName, this.blockDescription, this.blockCategory]
        }
    };
    
    /*
    var block = new Block();
    block.createFromWizard(wizardData);
    var ui = new SimpleBlockUI(block);
    */
    var execFunc = {
        func: function(){
            var block = new Block();
            block.createFromWizard(this.wizardData);
            globalBuilder.setCurrentBlock(block);
        },
        wizardData: wizardData
    };
    if(this.helper.isBlock){
        execFunc.caption = "Creating block...";
    } else {
        execFunc.caption = "Creating library...";
    }
    globalBuilder.closeAll();
    globalBuilder.runTaskInBusyDialog(execFunc);
    
    $('#' + this.divId).dialog('close');
};

BlockWizard.prototype.resizeUI = function(){
    this.mainDiv.style.height = (this.editorWindow.clientHeight - 50) + "px";
    this.mainDiv.style.width = (this.editorWindow.clientWidth - 20) + "px";
    this.titleDiv.style.width = (this.editorWindow.clientWidth - 2) + "px";
    if(this.wizardStage==2){
        this.editorPanel.resize();
    }
};

BlockWizard.prototype.getLanguageDescription = function(language){
    var bt = globalBuilder.findBlockType(language);
    if(bt){
        if(bt.description){
            return bt.description;
        } else {
            return "No description available";
        }
    } else {
        return "No description available";
    }
    
};