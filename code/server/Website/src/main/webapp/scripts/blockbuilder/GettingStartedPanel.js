/* 
 * This script provides a default "getting started" tab that contains shortcuts
 * to the most commonly used functions.
 */
function GettingStartedPanel(){
    this.divName = "";
    this.builder = null;
}

GettingStartedPanel.prototype.init = function(divName){
    this.divName = divName;
    var i;
    var div = document.getElementById(this.divName);
    var html='<div style="width:310px; height:200px;margin-left:auto;margin-right:auto;margin-top: auto; margin-bottom:auto;">';
    html+='<p><button class="btn" style="width:300px;" id="' + this.divName + 'load">Load a project...</button></p>';
    var bt;
    for(i=0;i<this.builder.getBlockTypeCount();i++){
        bt = this.builder.getBlockType(i);
        if(bt.isBlock){
            html+='<p><button class="btn" style="width:300px;" id="' + this.divName + bt.label + '">Create new ' + bt.label + ' block</button></p>';
        } else {
            html+='<p><button class="btn" style="width:300px;" id="' + this.divName + bt.label + '">Create new ' + bt.label + '</button></p>';
        }
    }
    html+='</div>';
    div.innerHTML = html;

    /*
    $("#" + this.divName + "load").button({
        text: true,
        label: "Load a project"
    });
    */
   
    var panel = this;
    document.getElementById(this.divName + "load").onclick = function(){
        panel.builder.browseForBlock();
    }
    
    for(i=0;i<this.builder.getBlockTypeCount();i++){
        bt = this.builder.getBlockType(i);
        /*
        if(bt.isBlock){
            $("#" + this.divName + bt.label).button({
                text: true,
                label: "Create new " + bt.label + " block"
           });
        } else {
            $("#" + this.divName + bt.label).button({
                text: true,
                label: "Create new " + bt.label
           });            
        }
        */
        document.getElementById(this.divName + bt.label).bt = bt;
        document.getElementById(this.divName + bt.label).onclick = function(){
            var helper = this.bt.helper.prototype.createInstance();
            var wiz = new BlockWizard();
            wiz.blockType = this.bt.label;
            wiz.helper = helper;
            wiz.blockCodeDocument = wiz.helper.getBlockMainCodeTemplateAsDocument();
            wiz.createWindow();
        };
    }    
};
