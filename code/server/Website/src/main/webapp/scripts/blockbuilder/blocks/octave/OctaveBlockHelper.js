/* 
 * This script contains helper code to create and edit octave workflow blocks
 */
function OctaveBlockHelper(){
    this.block = null;
    this.supportScriptFolder = null;
    this.blockTypeLabel = "Octave";    
    this.isBlock = true;
    this.isDeployable = true;
}

OctaveBlockHelper.prototype.createInstance = function(block){
    var helper = new OctaveBlockHelper();
    if(block){
        helper.block = block;
        block.helper = helper;
        block.hasServiceXml = helper.isBlock;
    }
    return helper;
};

OctaveBlockHelper.prototype.validate = function(wizardData){
    this.supportScriptFolder = globalBuilder.createFolderIfNotPresent(this.block.srcFolder, "scripts");
    
   // Build a list of parameter replacements
    var replacements;
    if(wizardData && wizardData.replacements){
        replacements = wizardData.replacements;
    } else {
        replacements = {
            names: ["name", "description","category"],
            values: ["NewBlock","A new Octave service","My Services"]
        }
    }
    
    // Standard block files
    var contents = APIClient.prototype.getFolderContents(this.block.srcFolder);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "service.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/service.xml"), contents, replacements);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "library.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/library.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "dependencies.xml", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/dependencies.xml"), contents);
    globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "init.m", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/init.m"), contents);
    globalBuilder.copyBlockIcon(this.block.srcFolder, "octave.jpg");
    
    // Use the wizard data code if present
    if(wizardData && wizardData.blockCode){
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.m", wizardData.blockCode, contents);
    } else {
        globalBuilder.copyTemplateFileIfNotPresent(this.block.srcFolder, "main.m", rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/main.m"), contents);
    }
};


OctaveBlockHelper.prototype.prepareZip = function(){
    
};

OctaveBlockHelper.prototype.getBlockMainCode = function(){
    if(this.block.srcFolder){
        var doc = APIClient.prototype.getFileContents(this.block.srcFolder, "main.m");
        return doc;
    } else {
        throw "No source folder present";
    }
};

OctaveBlockHelper.prototype.getBlockMainCodeTemplateAsDocument = function(){
    var code = APIClient.prototype.getUrlContentAsString(rewriteAjaxUrl("../../scripts/blockbuilder/blocks/octave/templates/main.m"));
    var doc = new DocumentRecord();
    doc.name = "main.m";
    doc.content = code;
    return doc;
};


globalBuilder.registerBlockType(new BlockType("Octave", "GNU Octave is a high-level interpreted language, primarily intended for numerical computations. It provides capabilities for the numerical solution of linear and nonlinear problems, and for performing other numerical experiments. It also provides extensive graphics capabilities for data visualization and manipulation. Octave is normally used through its interactive command line interface, but it can also be used to write non-interactive programs. The Octave language is quite similar to Matlab so that most programs are easily portable.", OctaveBlockHelper, true));