/* 
 * This script provides a basic editor panel with no toolbar etc
 */
//Requires
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/CodeMirror-2.13/lib/codemirror.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/blockbuilder/CodeMirror-2.13/theme/default.css">
//
//<script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/lib/codemirror.js"></script>
//<script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/javascript/javascript.js"></script>
//<script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/xml/xml.js"></script>
//<script type="text/javascript" src="../../scripts/blockbuilder/CodeMirror-2.13/mode/r/r.js"></script>

function MimeType(initData){
    if(initData.extension){
        this.extension = initData.extension;
    } else {
        this.extension = "";
    }

    if(initData.mimeType){
        this.mimeType = initData.mimeType;
    } else {
        this.mimeType = "";
    }

    if(initData.editorMode){
        this.editorMode = initData.editorMode;
    } else {
        this.editorMode = "text";
    }
}
function SimpleEditorPanel(){
    this.doc = null;
    this.divName = "";
    this.mimeType = null;
    this.codeMirror = null;
    this.editorDiv = null;
    this.mimeType = null;
    this.isDirty = false;
    this.onChangeFunction = null;
    this.saveCallback = null;
    this.containerTab = null;
}

SimpleEditorPanel.prototype.init = function(divName){
    this.divName = divName;
    var div = document.getElementById(divName);
    var panel = this;
    if(div){
        div.innerHTML = "";
        this.editorDiv = document.createElement("textarea");
        this.editorDiv.setAttribute("id", this.divName + "_editor");
        this.editorDiv.setAttribute("style", "width: 100%; height: 95%; border: none; resize: none;");
        div.appendChild(this.editorDiv);
        
        $(div).bind("resize", function(){
            panel.resize();
        });   
        this.resize();
    }
};

SimpleEditorPanel.prototype.resizeTo = function(width, height){
    if(this.codeMirror){
        
        var cmd = this.codeMirror.getScrollerElement();
        cmd.style.height = height - 10 + "px";
        cmd.style.width = width - 10 + "px";
        //cmd.style.position = "absolute";
        
        this.codeMirror.refresh();
    }    
};

SimpleEditorPanel.prototype.resize = function(){
    if(this.codeMirror){
        var div = document.getElementById(this.divName);
        var divHeight = div.clientHeight;
        var divWidth = div.clientWidth;
        
        var cmd = this.codeMirror.getScrollerElement();
        cmd.style.height = divHeight - 10 + "px";
        cmd.style.width = divWidth - 10 + "px";
        //cmd.style.position = "absolute";
        
        this.codeMirror.refresh();
    }
};

SimpleEditorPanel.prototype.openDocumentById = function(id, versionId){
    var doc = APIClient.prototype.getFileContentsById(id, versionId);
    this.openDocument(doc);
};

SimpleEditorPanel.prototype.openDocument = function(doc){
    this.doc = doc;
    this.isDirty = false;
    this.init(this.divName);
    if(this.doc && this.doc.content!=null){
        this.editorDiv.innerHTML = this.doc.content;
    } else {
        this.editorDiv.innerHTML = "";
    }
    
    var fileExtension = this.getFileExtension(doc.name);
    this.mimeType = null;
    if(fileExtension){
        this.mimeType = this.getMimeObjectByExtension(fileExtension);
    }
        
    var panel = this;

    var mode;
    if(this.mimeType){
        mode = this.mimeType.mimeType;
    } else {
        mode = "text/plain";
    }
    this.codeMirror = CodeMirror.fromTextArea(this.editorDiv, {
        lineNumbers: true,
        matchBrackets: true,
        indentUnit: 1,
        indentWithTabs: false,
        mode: mode,
        onChange: function(editor){
            panel.isDirty = true;
            panel.displayDirtyStatus();
            if(panel.onChangeFunction){
                panel.onChangeFunction(panel);
            }
        }
    });
    this.codeMirror.refresh();

    this.resize();
};

SimpleEditorPanel.prototype.triggerOnChangeFunction = function(){
    if(this.onChangeFunction){
        this.onChangeFunction(this);
    }
};

SimpleEditorPanel.prototype.save = function(){
    if(this.doc){
        var textContent;
        if(this.codeMirror!=null){
            this.codeMirror.save();
            textContent = this.editorDiv.value;
        } else {
            textContent = this.editorDiv.value;
        }
        this.doc.content = textContent;
        this.doc = APIClient.prototype.setFileContents(null, this.doc);
        this.isDirty = false;
        this.displayDirtyStatus();
        
        if(this.saveCallback){
            this.saveCallback(this.doc);
        }
    }
};

SimpleEditorPanel.prototype.displayDirtyStatus = function(){
    if(this.containerTab){
        var tabCounter = this.containerTab.counter;
        var a = $(this.containerTab).parent().find('a[href="#tabs-' + tabCounter +'"]');
        
        if(this.isDirty){
            $(a).addClass("documentChanged");
        } else {
            $(a).removeClass("documentChanged");
        }
    }
};

SimpleEditorPanel.prototype.getCode = function(){
    if(this.codeMirror!=null){
        return this.codeMirror.getValue();
    } else {
        return this.editorDiv.value;
    }
};

/** Work out a file extension */
SimpleEditorPanel.prototype.getFileExtension = function(filename)
{
  var fn = filename.toString().toLowerCase();
  var ext = /^.+\.([^.]+)$/.exec(fn);
  //return ext == null ? "" : ext[1];
  return ext == null ? null : ext[1];
};

/** Get a mime object by file extension */
SimpleEditorPanel.prototype.getMimeObjectByExtension = function(extension){
    var mimeTypes = new List();
    mimeTypes.add(new MimeType({extension: "txt", mimeType: "text/plain", editorMode: "text"}));
    mimeTypes.add(new MimeType({extension: "csv", mimeType: "text/csv", editorMode: "text"}));
    mimeTypes.add(new MimeType({extension: "js", mimeType: "text/javascript", editorMode: "codemirror"}));
    mimeTypes.add(new MimeType({extension: "xml", mimeType: "application/xml", editorMode: "codemirror"}));
    mimeTypes.add(new MimeType({extension: "r", mimeType: "text/x-rsrc", editorMode: "codemirror"}));
    mimeTypes.add(new MimeType({extension: "htm", mimeType: "text/html", editorMode: "codemirror"}));
    mimeTypes.add(new MimeType({extension: "html", mimeType: "text/html", editorMode: "codemirror"}));
    var i;
    for(i=0;i<mimeTypes.size();i++){
        if(mimeTypes.get(i).extension==extension){
            return mimeTypes.get(i);
        }
    }
    return null;
};
