# This file is executed once before any data is passed through the
# Octave service. It is used to set up any data structures that are
# needed throughout the calculations.