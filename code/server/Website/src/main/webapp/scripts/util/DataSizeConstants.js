/* 
 * This script contains constants for displaying and managing size limits on data
 * loaders.
 */
function DataSizeConstants(){
}

DataSizeConstants.DefaultIndex = 1;
DataSizeConstants.TextValues = ["1 KB","100 KB", "200 KB", "500 KB", "1 MB"];
DataSizeConstants.ByteValues = [1024, 102400, 204800, 107200, 512000, 1024000];

/** Return the byte size for a text value */
DataSizeConstants.byteValueForText = function(text){
    for(var i=0;i<DataSizeConstants.TextValues.length;i++){
        if(text===DataSizeConstants.TextValues[i]){
            return DataSizeConstants.ByteValues[i];
        }
    }
    return DataSizeConstants.ByteValues[DataSizeConstants.DefaultIndex];
};