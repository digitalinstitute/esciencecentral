/* 
 * This class provides a simple event manager
 */
function EventManager(eventSource){
    this.eventMap = new Array();
    if(eventSource){
        this.eventSource = eventSource;
    } else {
        this.eventSource = null;
    }
}

/** Bind a function to an event */
EventManager.prototype.bind = function(name, handler, handlerParent){
    var holder = this.findEventHolder(name);
    if(holder==null){
        holder = new EventHolder();
        holder.eventName = name;
        this.eventMap[this.eventMap.length] = holder;
    }
    
    // Add a handler holder
    if(handlerParent){
        holder.addListener(new HandlerHolder(handler, handlerParent));
    } else {
        holder.addListener(new HandlerHolder(handler, null));
    }
};

/** Unbind all of the handlers related to a given parent */
EventManager.prototype.removeHandlersForParent = function(parent){
    for(var i=0;i<this.eventMap.length;i++){
        this.eventMap[i].removeHandlersForParent(parent);
    }
};

/** Trigger an event */
EventManager.prototype.trigger = function(name){
    var holder = this.findEventHolder(name);
    if(holder){
        holder.trigger(this);
    }
};

/** Find the an event holder in the event map */
EventManager.prototype.findEventHolder = function(name){
    for(var i=0;i<this.eventMap.length;i++){
        if(this.eventMap[i].eventName==name){
            return this.eventMap[i];
        }
    }
    return null;
};

/** Holder for an event handler function */
function HandlerHolder(handlerFunction, handlerParent){
    this.handlerFunction = handlerFunction;
    this.handlerParent = handlerParent;
}

/** Holder for and event */
function EventHolder(){
    this.eventName = "";
    this.listeners = new Array();
}

/** Add a listener */
EventHolder.prototype.addListener = function(listener){
    this.listeners[this.listeners.length] = listener;
};

/** Trigger all of the listener fuctions */
EventHolder.prototype.trigger = function(source){
    for(var i=0;i<this.listeners.length;i++){
        //this.listeners[i].handlerFunction(source, this.eventName);
        this.listeners[i].handlerFunction.call(this.listeners[i].handlerParent, source, this.eventName);
    }
};

/** Remove all of the handlers for a parent */
EventHolder.prototype.removeHandlersForParent = function(parent){
    // Get all of the handlers for a parent
    var handlersToRemove = new Array();
    var i;
    var count;
    for(i=0;i<this.listeners.length;i++){
        if(parent===this.listeners[i].handlerParent){
            handlersToRemove[count] = this.listeners[i];
            count++;
        }
    }
    
    // Now remove each from the handler list
    var index;
    for(i=0;i<handlersToRemove.length;i++){
        index = this.listeners.indexOf(handlersToRemove[i]);
        if(index!=-1){
            this.listeners.splice(index, 1);
        }
    }
    
};