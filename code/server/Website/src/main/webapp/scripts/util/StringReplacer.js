/* 
 * This script provides a replacer that can replace all of the marked
 * instances of some text in a string
 */
function StringReplacer(value){
    this.value = value;
}

StringReplacer.prototype.replace = function(replacements){
    var n;
    var v;
    var newValue = this.value;
    for(var i=0;i<replacements.names.length;i++){
        n = replacements.names[i];
        v = replacements.values[i];
        newValue = newValue.replace('${' + n + '}', v);
    }
    return newValue;
};