/**
 * This is a ACL component that can be included on a page that needs to set the ACL for an object.
 * Author: Simon
 * Date: October 2010
 */


function AccessControl() {
    this.divName = "";  // Name of the div that will hold the dialog
    this.userId = ""; //User Id of the currently logged in user
    this.soid = ""; //Id of the object
    this.publicId = ""; //Id of the public user
    this.enablePublic = "false";
    this.usersGroupId = "";  //Id of the main Users group
    this.saveCallback = null; // Function to call if the save was OK
    this.dialogWidth = 602;
    this.dialogHeight = 530;
}

AccessControl.prototype.init = function (divName, userId, serverObjectId, publicUserId, enablePublic, usersGroupId) {
    //Set the member variables for this object
    this.divName = divName;
    this.userId = userId;
    this.soid = serverObjectId;
    this.publicId = publicUserId;
    this.usersGroupId = usersGroupId;
    this.enablePublic = enablePublic;

    //handle to this ACL that can be passed into jQuery callbacks
    var thisAcl = {};
    thisAcl.acl = this;

    buildHtml(this.divName, enablePublic);

    //add tabs to the dialog box
    $('#' + this.divName + '_acl_info').tabs({
        show: function (event, ui) {
            //check the custom button when moving to the advanced tab
            if (ui.panel.id === thisAcl.acl.divName + "_advanced_acl") {
                $('#' + thisAcl.acl.divName + '_customRadio').attr('checked', 'checked');

                //update the scrollbars as the lists are drawn with size 0
                thisAcl.acl.updateScrollBars();
            }
        },
        selected: 0
    }, thisAcl);

    //crate the dialog box to hold the tabs
    $('#' + this.divName).dialog({
        resizable: false,
        autoOpen: false,
        draggable: true,
        modal: true,
        height: this.dialogHeight,
        width: this.dialogWidth,
        buttons: {
            "Save": function () {
                thisAcl.acl.save();
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    }, thisAcl);

    //hide the title bar for this dialog as we're displaying tabs as the title
    $('#' + this.divName).siblings('.ui-dialog-titlebar').hide();

    //when the user clicks on the custom button, select the advanced tab
    $("input:radio[id=" + this.divName + "_customRadio]").click(function () {
        $('#' + thisAcl.acl.divName + '_acl_info').tabs('select', 1);
    });

    //Get the current ACL for the object in terms of public, private or custom
    $.ajax({
        url: rewriteAjaxUrl('../../acl/' + this.soid + '/public'),
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data !== null) {
                var accessLevel = data.accessLevel;

                if (accessLevel === 'public') {
                    $('#' + thisAcl.acl.divName + '_publicRadio').attr('checked', 'checked');
                }
                else if (accessLevel === 'private') {
                    $('#' + thisAcl.acl.divName + '_privateRadio').attr('checked', 'checked');
                }
                else if (accessLevel === 'custom') {
                    $('#' + thisAcl.acl.divName + '_customRadio').attr('checked', 'checked');
                    $('#' + thisAcl.acl.divName + '_acl_info').tabs('select', 1);
                }
            }
        }
    }, thisAcl);

    //Get the list of friends
    $.ajax({
        url: rewriteAjaxUrl('../../people/' + this.userId + '/friends'),
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data !== null) {
                //add one row to the table of friends per item returned
                for (var i = 0; i < data.length; i++) {
                    $('#' + thisAcl.acl.divName + '_permsPeople').append('<li id="' + thisAcl.acl.divName + '_' + data[i].id + '">' + data[i].label + '</li>');
                }
                $('#' + thisAcl.acl.divName + '_permsPeople').selectable();
                thisAcl.acl.updateScrollBars();
            }
        }
    }, thisAcl);

    //Get the list of groups that the user is in
    $.ajax({
        url: rewriteAjaxUrl('../../people/' + this.userId + '/groups'),
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data !== null) {
                //add a row for each group
                for (var i = 0; i < data.length; i++) {
                    $('#' + thisAcl.acl.divName + '_permsGroups').append('<li id="' + thisAcl.acl.divName + '_' + data[i].id + '">' + data[i].label + '</li>');
                }
                $('#' + thisAcl.acl.divName + '_permsGroups').selectable();
                thisAcl.acl.updateScrollBars();
            }
        }
    }, thisAcl);

    //get the current ACL for this object
    $.ajax({
        url: rewriteAjaxUrl('../../acl/' + this.soid),
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        success: function (data) {
            if (data !== null && !data[0].error) {
                for (var i = 0; i < data.length; i++) {
                    //the public read/write isn't in the table, so set the values directly
                    if (data[i].id === thisAcl.acl.publicId) {
                        for (var j = 0; j < data[i].access.length; j++) {
                            if (data[i].access[j] === 'read') {
                                $('#' + thisAcl.acl.divName + '_publicRead').attr('checked', 'checked');
                            }
                        }
                    }

                    //the any logged in user isn't in the table, so set the values directly
                    else if (data[i].id === thisAcl.acl.usersGroupId) {
                        for (var j = 0; j < data[i].access.length; j++) {
                            if (data[i].access[j] === 'read') {
                                $('#' + thisAcl.acl.divName + '_loggedInUsers').attr('checked', 'checked');
                            }
                        }
                    }

                    else {
                        //add a row to the table for each person
                        $('#' + thisAcl.acl.divName + '_permsCurrent').append('<div type="' + data[i].type + '" class="permsRow" id="' + thisAcl.acl.divName + '_' + data[i].id + 'Current">' +
                            '<div class="permsWrite"><input type="checkbox" id="' + thisAcl.acl.divName + '_' + data[i].id + 'Write"/></div>' +
                            '<div class="permsRead"><input type="checkbox" id="' + thisAcl.acl.divName + '_' + data[i].id + 'Read"/></div>' +
                            '<div class="permsName">' + data[i].name + '</div>' +
                            '</div>');

                        //set the personId or groupId - this is necessary so that when the row is removed the item goes back
                        //into the right list
                        if (data[i].type === 'person') {
                            $('#' + thisAcl.acl.divName + '_' + data[i].id + 'Current').attr('personId', data[i].id);
                        }
                        else if (data[i].type === 'group') {
                            $('#' + thisAcl.acl.divName + '_' + data[i].id + 'Current').attr('groupId', data[i].id);
                        }

                        //add the read/write checkbox checks - will ignore add/execute perms
                        for (j = 0; j < data[i].access.length; j++) {
                            if (data[i].access[j] === 'read') {
                                $('#' + thisAcl.acl.divName + '_' + data[i].id + 'Read').attr('checked', 'checked');
                            }
                            else if (data[i].access[j] === 'write') {
                                $('#' + thisAcl.acl.divName + '_' + data[i].id + 'Write').attr('checked', 'checked');
                            }
                        }
                    }
                }
            }

            //make the list of current permissions selectable
            $('#' + thisAcl.acl.divName + '_permsCurrent').selectable();
            thisAcl.acl.updateScrollBars();
        }
    }, thisAcl);


    /************************/
    /* advanced ACL        */
    /************************/

        //bind the add and remove actions to the buttons in the advanced toolabar
    $('#' + this.divName + '_advancedAclRemoveButton').button();
    $('#' + this.divName + '_advancedAclAddButton').click(function () {
        thisAcl.acl.add();
    });

    $('#' + this.divName + '_advancedAclAddButton').button();
    $('#' + this.divName + '_advancedAclRemoveButton').click(function () {
        thisAcl.acl.remove();
    });

    // Add the striped styling to the lists
    $('.striped li:even').addClass('blue');
    $('.permsRow:even').addClass('blue');
    $('#' + this.divName + '_permsTable tbody tr:even').addClass('blue');

};

//Add a the currently selected rows from the users/groups to the current permissions and then remove them
//from the list
AccessControl.prototype.add = function () {
    var thisAcl = {};
    thisAcl.acl = this;

    $("#" + this.divName + "_permsPeople .ui-selected").each(function () {
        $('#' + thisAcl.acl.divName + '_permsCurrent').append(
            '<div class="permsRow" id="' + $(this).attr('id') + 'Current" type="person" personId="' + $(this).attr('id') + '">' +
                '<div class="permsWrite"><input type="checkbox" id="' + $(this).attr('id') + 'Write"/></div>' +
                '<div class="permsRead"><input type="checkbox" id="' + $(this).attr('id') + 'Read"/></div>' +
                '<div class="permsName">' + $(this).html() + '</div>' +
                '</div>');

        $(this).remove();
    }, null, thisAcl);

    $("#" + this.divName + "_permsGroups .ui-selected").each(function () {
        $('#' + thisAcl.acl.divName + '_permsCurrent').append(
            '<div class="permsRow"  id="' + $(this).attr('id') + 'Current" type="group" groupId="' + $(this).attr('id') + '">' +
                '<div class="permsWrite"><input type="checkbox" id="' + $(this).attr('id') + 'Write"/></div>' +
                '<div class="permsRead"><input type="checkbox" id="' + $(this).attr('id') + 'Read"/></div>' +
                '<div class="permsName">' + $(this).html() + '</div>' +
                '</div>');
        $(this).remove();
    }, null, thisAcl);

    $('#' + this.divName + '_permsCurrent').selectable();
    this.updateScrollBars();
};

//Remove the currently selected people from the current ACL list and put them back in the users/groups
AccessControl.prototype.remove = function () {
    var thisAcl = {};
    thisAcl.acl = this;

    $("#" + this.divName + "_permsCurrent .ui-selected").each(function () {
        if ($(this).attr('type') === 'person') {
            $('#' + thisAcl.acl.divName + '_permsPeople').append('<li id="' + $(this).attr('personId') + '">' + $(this).children('.permsName').html() + '</li>');
            $(this).remove();
        }
        else if ($(this).attr('type') === 'group') {
            $('#' + thisAcl.acl.divName + '_permsGroups').append('<li id="' + $(this).attr('groupId') + '">' + $(this).children('.permsName').html() + '</li>');
            $(this).remove();
        }
    }, null, thisAcl);

    this.updateScrollBars();
};


//Function to update the scrollbars.  This is necessary as they are drawn offscreen and have size 0.
//Also necessary to be called when things are added/removed
AccessControl.prototype.updateScrollBars = function () {
    //Perform the update
    var peopleScrollBar = $('#' + this.divName + '_peopleScrollBar');
    peopleScrollBar.tinyscrollbar();
    peopleScrollBar.update();

    var groupsScrollBar = $('#' + this.divName + '_groupsScrollBar');
    groupsScrollBar.tinyscrollbar();
    groupsScrollBar.update();

    var currentScrollBar = $('#' + this.divName + '_currentScrollBar');
    currentScrollBar.tinyscrollbar();
    currentScrollBar.update();

    //remove the current styling
    $('#' + this.divName + '_permsPeople li').removeClass('blue');
    $('#' + this.divName + '_permsGroups li').removeClass('blue');
    $('#' + this.divName + '_permsCurrent .permsRow').removeClass('blue');

    //add the alternating blue lines
    $('#' + this.divName + '_permsPeople li:even').addClass('blue');
    $('#' + this.divName + '_permsGroups li:even').addClass('blue');
    $('#' + this.divName + '_permsCurrent .permsRow:even').addClass('blue');

};

//Open the dialog - probably bound to a button like this:
AccessControl.prototype.open = function () {
    $('#' + this.divName).dialog('open');
};

//Close the ACL dialog
AccessControl.prototype.close = function () {
    $('#' + this.divName).dialog('close');
};

//Function to save the current contents of the ACL dialog back to the server
AccessControl.prototype.save = function () {
    //handle to this ACL object so that the dialog can be closed in the callback
    var thisAcl = {};
    thisAcl.acl = this;

    //array to hold the data to be sent back to the server
    var data = new Array();

    //Get the currently selected tab.
    var $tabs = $('#' + this.divName + '_acl_info').tabs();
    var selectedTab = $tabs.tabs('option', 'selected');

    //If we're on the basic tab and public is selected
    if (selectedTab === 0 && $('#' + this.divName + '_publicRadio').is(':checked')) {
        //Just send back the public user id
        data = new Array();
        if (this.enablePublic && (this.enablePublic === true || this.enablePublic === "true")) {
            data.push({id: this.publicId, 'permission': 'read'});
        }
    }
    else if (selectedTab === 0 && $('#' + this.divName + '_privateRadio').is(':checked')) //basic tab and private selected
    {
        //Send back an empty array
        data = new Array();
    }
    else //we're on the custom tab
    {
        //get the list of read permissions and add them to the data to be sent back
        $('#' + this.divName + '_permsCurrent .permsRead input:checkbox').each(function () {
            if ($(this).is(':checked')) {
                var divNameLength = thisAcl.acl.divName.length + 1;
                data.push({id: $(this).attr('id').substr(divNameLength, $(this).attr('id').length - 4 - divNameLength), 'permission': 'read'});
            }
        }, null, thisAcl);

        //add the write permissions to the data array
        $('#' + this.divName + '_permsCurrent .permsWrite input:checkbox').each(function () {
            if ($(this).is(':checked')) {
                var divNameLength = thisAcl.acl.divName.length + 1;
                data.push({id: $(this).attr('id').substr(divNameLength, $(this).attr('id').length - 5 - divNameLength), permission: 'write'});
            }
        }, null, thisAcl);

        //deal with the public read/write separately
        if (this.enablePublic && (this.enablePublic === true || this.enablePublic === "true")) {

            if ($('#' + this.divName + '_publicRead').is(':checked')) {
                data.push({id: this.publicId, permission: 'read'});
            }
        }

        //Make it available to all logged in users
        if ($('#' + this.divName + '_loggedInUsers').is(':checked')) {
            data.push({id: this.usersGroupId, permission: 'read'});
        }
    }

    //JSON-ify the data to be sent back
    var callString = JSON.stringify(data);


    //send the data back to the server - will show a dialog on failure
    $.ajax({
        type: "POST",
        url: rewriteAjaxUrl("../../acl/" + this.soid),
        data: callString,
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },
        error: function (msg) {
            alert("Problem saving ACL: " + msg.result);
        },
        success: function (data) {
            if (data.result === 'ok') {
                if (thisAcl.acl.saveCallback) {
                    thisAcl.acl.saveCallback(thisAcl.acl.soid);
                }
                $('#' + thisAcl.acl.divName).dialog('close');
            }
            else if (data.error === 'true') {
                alert(data.message);
            }
        }
    }, thisAcl);
};

AccessControl.prototype.destroy = function () {
    $('#' + this.divName).children().remove();
};

function buildHtml(div, enablePublic) {

    var html = "<div id='" + div + "_acl_info' class='acl_info'>" +
        "      <ul id='" + div + "_acl_tabs'>" +
        "        <li class='aclTab'><a href='#" + div + "_basic_acl'>Basic</a></li>" +
        "        <li class='aclTab'><a href='#" + div + "_advanced_acl'>Custom</a></li>" +
        "      </ul>" +
        "      <div class='acl_content'>" +
        "        <div id='" + div + "_basic_acl' class='basic_acl'>" +
        "" +
        "          <form action='#' class='simpleACLForm' name='simpleACLForm' autocomplete='off'>";

    if (enablePublic && (enablePublic === true || enablePublic === "true")) {
        html += "            <div class='aclOption'>" +
            "              <label for='publicRadio'><span class='orange'>Public</span>" +
            "                <br/><br/>" +
            "                This will be visible to everyone, whether logged in or not and will be can be found on search" +
            "                engines</label>" +
            "              <input id='" + div + "_publicRadio' type='radio' name='basicVisibility' value='public'>" +
            "            </div>";
    }

    html += "            <div class='aclOption'>" +
        "              <label for='privateRadio'><span class='orange'>Private</span>" +
        "                <br/><br/>" +
        "                This is only visible to you" +
        "              </label>" +
        "              <input id='" + div + "_privateRadio' type='radio' name='basicVisibility' value='private'>" +
        "            </div>" +
        "            <div class='aclOption'>" +
        "              <label for='customRadio'><span class='orange'>Custom</span>" +
        "                <br/><br/>" +
        "                Set specific permissions." +
        "              </label>" +
        "              <input id='" + div + "_customRadio' type='radio' name='basicVisibility' value='custom'>" +
        "            </div>" +
        "          </form>" +
        "        </div>" +
        "" +
        "        <div id='" + div + "_advanced_acl' class='advanced_acl'>" +
        "" +
        "          <div class='advanced_acl_group_wrapper'>" +
        "            <div class='advanced_acl_right'>" +
        "              <div>" +
        "                Current Permissions" +
        "              </div>" +
        "              <div id='" + div + "_currentScrollBar' style='width:250px;' class='sb longscrollbar'>" +
        "                <div class='scrollbar'>" +
        "                  <div class='track'>" +
        "                    <div class='thumb'>" +
        "                      <div class='end'></div>" +
        "                    </div>" +
        "                  </div>" +
        "                </div>" +
        "                <div class='viewport' style='width:230px;height:210px;'>" +
        "                  <div class='overview'>" +
        "                    <div class='permsHead'>" +
        "                      <div class='permsWrite' >Write</div>" +
        "                      <div class='permsRead' >Read</div>" +
        "                      <div class='permsName'>Name</div>" +
        "                    </div>" +
        "                    <div id='" + div + "_permsCurrent' class='permsCurrent'>" +
        "                    </div>" +
        "                  </div>" +
        "                </div>" +
        "              </div>" +
        "" +
        "              <input type='button' id='" + div + "_advancedAclRemoveButton' value='Remove'/>" +
        "";

    if (enablePublic && (enablePublic === true || enablePublic === "true")) {
        html += "              <form action='#' autocomplete='off'>" +
            "                <div class='currentPublic'>" +
            "                  <div>Public User</div>" +
            "                  <label for='publicRead'>Read" +
            "                    <input type='checkbox' id='" + div + "_publicRead'/>" +
            "                  </label>" +
            "                </div>" +
            "              </form>";
    }
    else {
        html += "              <form action='#' autocomplete='off'>" +
            "                <div class='currentPublic'>" +
            "                  <div>Any Logged In User</div>" +
            "                  <label for='publicRead'>Read" +
            "                    <input type='checkbox' id='" + div + "_loggedInUsers'/>" +
            "                  </label>" +
            "                </div>" +
            "              </form>";
    }

    html += "            </div>" +
        "" +
        "" +
        "            <div class='advanced_acl_left'>" +
        "              <div>" +
        "                People" +
        "              </div>" +
        "              <div id='" + div + "_peopleScrollBar' class='sb'>" +
        "                <div class='scrollbar'>" +
        "                  <div class='track'>" +
        "                    <div class='thumb'>" +
        "                      <div class='end'></div>" +
        "                    </div>" +
        "                  </div>" +
        "                </div>" +
        "                <div class='viewport'>" +
        "                  <div class='overview'>" +
        "                    <ul id='" + div + "_permsPeople' class='permsPeople'>" +
        "                    </ul>" +
        "                  </div>" +
        "                </div>" +
        "              </div>" +
        "" +
        "              <div>" +
        "                Groups" +
        "              </div>" +
        "              <div id='" + div + "_groupsScrollBar' class='sb'>" +
        "                <div class='scrollbar'>" +
        "                  <div class='track'>" +
        "                    <div class='thumb'>" +
        "                      <div class='end'></div>" +
        "                    </div>" +
        "                  </div>" +
        "                </div>" +
        "                <div class='viewport'>" +
        "                  <div class='overview'>" +
        "                    <ul class='permsGroups' id='" + div + "_permsGroups' >" +
        "                    </ul>" +
        "                  </div>" +
        "                </div>" +
        "              </div>" +
        "" +
        "              <input type='button' id='" + div + "_advancedAclAddButton' value='Add'/>" +
        "" +
        "            </div>" +
        "          </div>" +
        "          <!-- end left -->" +
        "          <div class='advancedAclHint'>" +
        "            Hint: Press Ctrl to select multiple people and groups, then click Add" +
        "          </div>" +
        "        </div>" +
        "      </div>" +
        "    </div>";

    $('#' + div).append(html);


}
