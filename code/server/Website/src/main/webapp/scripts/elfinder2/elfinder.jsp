<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>elFinder 2.0</title>

  <!-- jQuery and jQuery UI (REQUIRED) -->
  <link rel="stylesheet" type="text/css" media="screen"
        href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

  <!-- elFinder CSS (REQUIRED) -->
  <link rel="stylesheet" type="text/css" media="screen" href="css/elfinder.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/theme.css">


  <%--ACL--%>
  <link rel="stylesheet" href="../../scripts/accesscontrol/accesscontrol.css" type="text/css" media="screen"
        charset="utf-8"/>

  <!-- elFinder JS (REQUIRED) -->
  <script type="text/javascript" src="js/elfinder.full.js"></script>

  <!-- elFinder translation (OPTIONAL) -->
  <script type="text/javascript" src="js/i18n/elfinder.ru.js"></script>

  <!-- V1 Support -->
  <script src="js/proxy/elFinderSupportVer1.js" type="text/javascript" charset="utf-8"></script>


  <%--ACL--%>
  <script type="text/javascript" src="../../scripts/accesscontrol/accesscontrol.js"></script>
  <script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.js"></script>


  <!-- elFinder initialization (REQUIRED) -->
  <script type="text/javascript" charset="utf-8">


    $().ready(function ()
    {
      var elf = $('#elfinder').elfinder({
        url : '../../servlets/finder',
        transport : new elFinderSupportVer1()
      }).elfinder('instance');
    });
  </script>
</head>
<body>

<!-- Element where elFinder will be created (REQUIRED) -->
<div id="elfinder"></div>

</body>
</html>
