function loadTimeline(dataURL, topInterval, bottomInterval, centreDate)
{
  if (topInterval === undefined)
  {
    topInterval = "Timeline.DateTime.WEEK";
  }
  if (bottomInterval === undefined)
  {
    bottomInterval = "Timeline.DateTime.MONTH";
  }

  SimileAjax.History.enabled = false;
  
  var timeline;
  var eventSource = new Timeline.DefaultEventSource();
  var theme = Timeline.ClassicTheme.create();
  theme.event.track.height = 20;
  theme.event.track.gap = 4;


  var bandInfos = [
    Timeline.createBandInfo({
      eventSource:    eventSource,
      width:          "80%",
      intervalUnit:   topInterval,
      intervalPixels: 250,
      theme:theme
    }),
    Timeline.createBandInfo({
      overview:       true,
      eventSource:    eventSource,
      width:          "20%",
      intervalUnit:   bottomInterval,
      intervalPixels: 250
    })
  ];
  bandInfos[1].syncWith = 0;
  bandInfos[1].highlight = true;

  var timelineDiv = document.getElementById("timeline");
  timeline = Timeline.create(timelineDiv, bandInfos);

  Timeline.loadJSON(dataURL, function(json, url)
  {
    eventSource.loadJSON(json, url);
  });

  
  timeline.getBand(0).setCenterVisibleDate(Timeline.DateTime.parseGregorianDateTime(centreDate));

}


