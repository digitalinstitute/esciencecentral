var timeline;

function createBandInfos5(eventSource) {
    var bandInfos = [
        //create the two bands
//        Timeline.createHotZoneBandInfo({
//            timeZone:       -5,
//            date:           "Jun 28 2006 00:00:00 GMT",
//            width:          "70%",
//            intervalUnit:   Timeline.DateTime.MONTH,
//            intervalPixels: 100,
//            eventSource:    eventSource
//            zones: [
//                {   start:    "Aug 01 2006 00:00:00 GMT-0500",
//                    end:      "Sep 01 2006 00:00:00 GMT-0500",
//                    magnify:  10,
//                    unit:     Timeline.DateTime.WEEK
//                },
//                {   start:    "Aug 02 2006 00:00:00 GMT-0500",
//                    end:      "Aug 04 2006 00:00:00 GMT-0500",
//                    magnify:  7,
//                    unit:     Timeline.DateTime.DAY
//                },
//                {   start:    "Aug 02 2006 06:00:00 GMT-0500",
//                    end:      "Aug 02 2006 12:00:00 GMT-0500",
//                    magnify:  5,
//                    unit:     Timeline.DateTime.HOUR
//                }
//            ]
//        }),
        Timeline.createBandInfo({
            timeZone:       -5,
            date:           "Jun 28 2006 00:00:00 GMT",
            width:          "30%",
            intervalUnit:   Timeline.DateTime.YEAR,
            intervalPixels: 200,
            showEventText:  false,
            trackHeight:    0.5,
            trackGap:       0.2,
            eventSource:    eventSource,
            overview:       true
        })
    ];
    bandInfos[1].syncWith = 0;
    bandInfos[1].highlight = true;
    return bandInfos;
}

function createBandInfos6(eventSource) {
    var bandInfos = createBandInfos5(eventSource);
    bandInfos[1] =
        Timeline.createHotZoneBandInfo({
//            timeZone:       -5,
            date:           "Jun 28 2006 00:00:00 GMT",
            width:          "30%",
            intervalUnit:   Timeline.DateTime.YEAR,
            intervalPixels: 200,
            showEventText:  false,
            trackHeight:    0.5,
            trackGap:       0.2,
            eventSource:    eventSource,
//            zones: [
//                {   start:    "Aug 01 2006 00:00:00 GMT-0500",
//                    end:      "Sep 01 2006 00:00:00 GMT-0500",
//                    magnify:  20,
//                    unit:     Timeline.DateTime.WEEK
//                }
//            ],
            overview:       true
        });
    bandInfos[1].syncWith = 0;
    bandInfos[1].highlight = true;
    return bandInfos;
}

function onLoad() {

    var eventSource2 = new Timeline.DefaultEventSource();

    var bandInfos6 = createBandInfos6(eventSource2);
    timeline = Timeline.create(document.getElementById("tl6"), bandInfos6);

    Timeline.loadXML("../timeline/webapp/docs/example2.xml", 
        function(xml, url) { eventSource2.loadXML(xml, url); });
}

var resizeTimerID = null;
function onResize() {
    if (resizeTimerID == null) {
        resizeTimerID = window.setTimeout(function() {
            resizeTimerID = null;
                timeline.layout();
            
        }, 500);
    }
}
