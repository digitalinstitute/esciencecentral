/* 
 * This script provides a popup window for displaying comments
 */
//Requires
// $.rloader({type: 'js', src: '../../scripts/comments/CommentsPanel.js'});
function CommentsDialog(){
    this.panel = new CommentsPanel;
    this.divName = null;
    this.div = null;
    this.panelDiv = null;
}

CommentsDialog.prototype.init = function(divName){
    this.divName = divName;
    this.div = document.getElementById(this.divName);

    var toolbarDiv = document.createElement("div");
    toolbarDiv.setAttribute("style", "position: absolute; top:2px; left:2px; width:99% ; height: 30px");

    var addButton = document.createElement("div");
    addButton.appendChild(document.createTextNode("Add Comment"));
    $(addButton).button({icons:{primary: "ui-icon-plusthick"}});
    toolbarDiv.appendChild(addButton);
    addButton.onclick = function(){
        this.viewer.addAndEditComment();
    };
    addButton.viewer = this;

    this.div.appendChild(toolbarDiv);
    this.panelDiv = document.createElement("div");
    this.panelDiv.setAttribute("id", this.divName + "_panel");
    this.panelDiv.setAttribute("style", "overflow: auto; position: absolute; top: 37px; left:2px; width:99%;");
    
    this.div.appendChild(this.panelDiv);
    this.panel.init(this.divName + "_panel");
    var commentDialog = this;

    var dialog = $('#' + this.divName).dialog({
        bgiframe: true,
        autoOpen: false,
        height: 600,
        width: 800,
        title: "Comments",
        modal: true,
        viewer: this,
        buttons: {
            'Close': function()
            {
                commentDialog.panel.saveCurrentComment();
                commentDialog.panel.removeAllCommentEditors();
                $(this).dialog('close');
            }

        }
    });
    this.div.viewer = this;

    dialog.bind("dialogresize", function(){
        this.viewer.resizeCommentsPanel();
    });
    this.resizeCommentsPanel();
};

/** Resize the comments panel */
CommentsDialog.prototype.resizeCommentsPanel = function(){
    this.panelDiv.style.height = (this.div.clientHeight - 40) + "px";
};

/** Add a new comment */
CommentsDialog.prototype.addComment = function(){
    this.panel.addComment("New comment");
};

CommentsDialog.prototype.addAndEditComment = function(){
    var p = this.panel;
    var callback = function(){
        p.scrollToLastComment();
    }
    this.panel.addAndEditComment(callback);
    
}

/** Show the dialog for an object */
CommentsDialog.prototype.showObjectComments = function(objectId){
    this.panel.clearComments();
    this.panel.setObjectId(objectId);
    $('#' + this.divName).dialog('open');
    this.resizeCommentsPanel();
    this.panel.fetchObjectComments();
};