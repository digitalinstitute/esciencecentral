/* 
 * This javascript file provides an object to manage comments for obejcts
 */
function CommentsManager(){
    
}

/** Fetch all of the comments for an object */
CommentsManager.prototype.fetchComments = function(objectId, callback){
    var callData = {
        objectId: objectId
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/comments?method=fetchComments");

    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.comments);
            }
        } else {
            $.jGrowl("Error fetching comments: " + o.message);
        }
    };

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      panel: this
    });
};

/** Add a comment to an object */
CommentsManager.prototype.addComment = function(objectId, text, callback){
    var callData = {
        objectId: objectId,
        text: text
    };

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/comments?method=addComment");

    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.comment);
            }
        } else {
            $.jGrowl("Error saving comment: " + o.message);
        }
    };

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      panel: this
    });
};

CommentsManager.prototype.deleteComment = function(commentId, callback){
    var callData = {
        commentId: commentId
    };
    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/comments?method=deleteComment");

    var cb = function(o){
        if(!o.error){
            if(callback){
                callback();
            }
        } else {
            $.jGrowl("Error deleting comment: " + o.message);
        }
    };

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      panel: this
    });
};

CommentsManager.prototype.updateComment = function(commentId, text, callback, objectId){
    var callData;
    if(objectId){
        callData = {
            commentId: commentId,
            text: text,
            objectId: objectId
        };

    } else {
        callData = {
            commentId: commentId,
            text: text
        };
    }

    var callString = JSON.stringify(callData);
    var url = rewriteAjaxUrl("../../servlets/comments?method=updateComment");

    var cb = function(o){
        if(!o.error){
            if(callback){
                callback(o.comment);
            }
        } else {
            $.jGrowl("Error saving comment: " + o.message);
        }
    };

    $.ajax({
      type: 'POST',
      url: url,
      xhrFields: {
          withCredentials: true
      },       
      data: callString,
      success: cb,
      dataType: "json",
      panel: this
    });
};