/* 
 * This function provides a text view for the data viewer
 */
function TextView(){
    this.data = null;
    this.divName = "";
}

/** Update this view with data */
TextView.prototype.update = function(divName, data){
    if(divName){
        this.divName = divName;
    }
    if(data){
        this.data = data;
    }
    
    // Show the data
    var div = document.getElementById(this.divName);
    if(div){
        div.setAttribute("style", "overflow: auto;");
        if(this.data && this.data.retainCsvData){
            // Have CSV data 
            div.innerHTML = "<pre>" + this.data.csvData + "</pre>";
        } else {
            // Don't have CSV data
            div.innerHTML = "<pre>No CSV data available</pre>";
        }
    }
};

visualiserViewManager.registerView("TextView", "Simple", function(){return new TextView();});