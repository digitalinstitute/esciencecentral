/* 
 * This function provides a CSV view for the viewer
 */
function TableView(){
    this.data = null;
    this.divName = null;
}

TableView.prototype.update = function(divName, data){
    if(divName){
        this.divName = divName;
    }
    if(data){
        this.data = data;
    }
    
    // Show the data
    var div = document.getElementById(this.divName);
    if(div){
        div.setAttribute("style", "overflow: auto;");

        var html='<table class="display" style="width: 100%;">';
        var rowCount = this.data.getRows();
        var columnCount = this.data.getColumns();
        var i;
        var j;
        

        // Header
        html+="<thead><tr>";
        for(j=0;j<columnCount;j++){
            html+="<th>(" + j + ")</th>";
        }
        html+="</tr><tr>";
        for(j=0;j<columnCount;j++){
            html+="<th>" + this.data.column(j).name + "</th>";
        }
        html+="</tr></thead><tbody>";

        // Body
        for(i=1;i<rowCount;i++){
            
            html+="<tr>";
            for(j=0;j<columnCount;j++){
                html+="<td>" + this.data.column(j).getValue(i) + "</td>";
            }
            html+="</tr>";
        }
        html+="</tbody>";



        html+="</table>";
        div.innerHTML = html;
    }    
};

visualiserViewManager.registerView("TableView", "Simple", function(){return new TableView();});