/**
 * This is a ACL component that can be included on a page that needs to set the ACL for an object.
 * Author: Simon
 * Date: October 2010
 */
//Requires
//<script type="text/javascript" src="../../scripts/tinyscrollbar/jquery.tinyscrollbar.min.js"></script>
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/tinyscrollbar/tinyscrollbar.css">
//<link rel="stylesheet" type="text/css" media="screen" href="../../scripts/messages/friendPanel.css">
function FriendPanel() {
    this.divName = "";  // Name of the div that will hold the dialog
    this.userId = ""; //User Id of the currently logged in user
}

FriendPanel.prototype.init = function(divName, userId) {
    //Set the member variables for this object
    this.divName = divName;
    this.userId = userId;

    //handle to this ACL that can be passed into jQuery callbacks
    var thisAcl = {};
    thisAcl.acl = this;

    //todo: put back into JS
    //  buildHtml(this.divName);

    //Get the list of friends
    $.ajax({
        url: rewriteAjaxUrl('../../people/' + this.userId + '/friends'),
        dataType: 'json',
        xhrFields: {
            withCredentials: true
        },         
        success: function(data) {
            if (data !== null) {
                var peopleDiv = $('#' + thisAcl.acl.divName + '_permsPeople');
                //add one row to the table of friends per item returned
                for (var i = 0; i < data.length; i++) {
                    peopleDiv.append('<li id="' + data[i].id + '">' + data[i].label + '</li>');
                }
                $('#' + thisAcl.acl.divName + '_permsPeople').selectable();

                //remove the current styling
                $('#' + thisAcl.acl.divName + '_permsPeople li').removeClass('blue');

                //add the alternating blue lines
                $('#' + thisAcl.acl.divName + '_permsPeople li:even').addClass('blue');

            }
        }
    }, thisAcl);

    $("#" + divName).dialog({
        title: "Friends",
        height:350,
        autoOpen: false,
        buttons: {
            "Ok": function() {
                var selectedPeople = new Array();
                $("#" + thisAcl.acl.divName + "_permsPeople .ui-selected").each(function() {
                    var person = {id: $(this).attr('id'), name: $(this).html()};
                    selectedPeople.push(person);
                });
                thisAcl.acl.callback(selectedPeople);
                selectedPeople = new Array();
                $("#" + thisAcl.acl.divName + "_permsPeople .ui-selected").removeClass("ui-selected");
                $(this).dialog("close");
            },
            Cancel: function() {
                alert("close");
                $(this).dialog("close");
            }
        }

    }, thisAcl);
};

FriendPanel.prototype.callback = function(friendList) {

};

//Function to update the scrollbars.  This is necessary as they are drawn offscreen and have size 0.
//Also necessary to be called when things are added/removed
FriendPanel.prototype.updateScrollBars = function() {
    //Perform the update
    var peopleScrollBar = $('#' + this.divName + '_peopleScrollBar');
    peopleScrollBar.tinyscrollbar();
    peopleScrollBar.update();

    //remove the current styling
    $('#' + this.divName + '_permsPeople li').removeClass('blue');

    //add the alternating blue lines
    $('#' + this.divName + '_permsPeople li:even').addClass('blue');
};

//Open the dialog
FriendPanel.prototype.open = function() {
    $('#' + this.divName).dialog('open');
};

//Close the ACL dialog
FriendPanel.prototype.close = function() {
    selectedPeople =new Array();
    $('#' + this.divName).dialog('close');
};


FriendPanel.prototype.destroy = function() {
    $('#' + this.divName).children().remove();
};
