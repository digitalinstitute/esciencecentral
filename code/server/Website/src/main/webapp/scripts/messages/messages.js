function Messages()
{
  this.divName = "";  // Name of the div that will hold the dialog
  this.userId = ""; //User Id of the currently logged in user
  this.recipients = new Array();
  this.start = 0;
  this.pageSize = 10;
}

Messages.prototype.init = function(divName, userId, start, pageSize)
{
  this.divName = divName;
  this.userId = userId;
  this.start = start;
  this.pageSize = pageSize;
};

Messages.prototype.getMessageList = function()
{
  var thisMessages = {};
  thisMessages.message = this;
  thisMessages.userId = this.userId;

  //Get the list of messages
  $.ajax({
    url: rewriteAjaxUrl('../../servlets/messages/' + this.userId + "/" + this.start + "/" + this.pageSize) ,
    dataType: 'json',
    xhrFields: {
        withCredentials: true
    },     
    success: function(data)
    {
      if (data !== null)
      {
        var listDiv = $('#' + thisMessages.message.divName);
        //add one row to the table of friends per item returned
        for (var i = 0; i < data.messages.length; i++)
        {
          var message = data.messages[i];

          console.log(message);

          var listMainText;
          var html;

          if (message.type === 'textMessage')
          {
              var timestamp = moment(message.date, 'MMM D YYYY at hh:mm A').format('X');
              var displayTime;

              if(moment(message.date, 'MMM D YYYY at hh:mm A').isSame(new Date(), 'day')){
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('HH:mm');
              }
              else {
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('D MMM')
              }

              html = '<div class="accordion-group">' +
                        '<div class="accordion-heading">' +
                            '<a class="accordion-toggle" href="viewmessage.jsp?id=' + message.threadId + '">' +
                                '<h6><img src="../../servlets/image?soid=' + message.senderProfileId + '&type=small profile">' + message.title + '<span class="pull-right">' + displayTime + '</span></h6>' +
                            '</a>' +
                        '</div>' +
                    '</div>';
          }
          else if (message.type === 'friendRequest')
          {
            var displayTime;

              if(moment(message.date, 'MMM D YYYY at hh:mm A').isSame(new Date(), 'day')){
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('HH:mm');
              }
              else {
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('D MMM')
              }

              var unread = message.isRead ? '' : 'unread';

              html =  '<div id="connect-' + message.reqId + '" class="accordion-group" class="' + unread + '">' +
                  '<div class="accordion-heading">' +
                  '<a class="accordion-toggle" data-toggle="collapse" href="#' + message.reqId + '">' +
                  '<h6><img class="profile-' + message.senderId + '" src="../../servlets/image?soid=' + message.senderProfileId + '&type=small profile">' + message.title + '<span class="pull-right">' + displayTime + '</span></h6>' +
                  '</a>' +
                  '</div>' +
                  '<div id="' + message.reqId + '" class="accordion-body collapse">' +
                  '<div class="accordion-inner">' +
                  '<p>Do you want to connect to <a href="' + rewriteAjaxUrl("../../pages/profile/profile.jsp?id=" + message.senderId) + '">' + message.senderName + '</a></p>' +
                  '<button id="acceptConnect-' + message.reqId + '" class="btn btn-success">Accept</button>&nbsp;&nbsp;' +
                  '<button id="rejectConnect-' + message.reqId + '" class="btn btn-danger">Reject</button>' +
                  '</div>' +
                  '</div>' +
                  '</div>';

            $(document).on('click', '#acceptConnect-' + message.reqId, function()
            {
                var reqId = $(this).attr('id').replace('acceptConnect-','');
                var postData = {reqId: reqId};
                var message = _.find(data.messages, { reqId: reqId});

                $.ajax({
                  url: rewriteAjaxUrl('../../servlets/messages/' + thisMessages.userId + '/accept/') ,
                  type: 'POST',
                  dataType: 'json',
                  xhrFields: {
                      withCredentials: true
                  },
                  data: JSON.stringify(postData),
                  success: function()
                  {
                    $.jGrowl("You are now connected to " + message.senderName, {life:2000});

                    $("#connect-" + reqId).remove();

                  },
                  error: function()
                  {
                    alert('Error - Unable to accept connection to ' + message.senderName);
                  }
                })
            });

            $(document).on('click', '#rejectConnect-' + message.reqId, function()
            {
                var reqId = $(this).attr('id').replace('rejectConnect-','');
                var postData = {reqId: reqId};
                var message = _.find(data.messages, { reqId: reqId});

                $.ajax({
                  url: rewriteAjaxUrl('../../servlets/messages/' + thisMessages.userId + '/reject/'),
                  type: 'POST',
                  dataType: 'json',
                  xhrFields: {
                      withCredentials: true
                  },
                  data: JSON.stringify(postData),
                  success: function()
                  {
                    $.jGrowl("You have rejected a conntection to to " + message.senderName, {life:2000});

                      $("#connect-" + reqId).remove();
                  },
                  error: function()
                  {
                    alert('Error - Unable to reject connection to ' + message.senderName);
                  }
                })
            });

            message.isRead = false;

          }
          else if (message.type === 'joinGroupRequest')
          {
              var displayTime;

              if(moment(message.date, 'MMM D YYYY at hh:mm A').isSame(new Date(), 'day')){
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('HH:mm');
              }
              else {
                  displayTime = moment(message.date, 'MMM D YYYY at hh:mm A').format('D MMM')
              }

              var unread = message.isRead ? '' : 'unread';

            html =  '<div id="group-' + message.reqId + '" class="accordion-group" class="' + unread + '">' +
                        '<div class="accordion-heading">' +
                            '<a class="accordion-toggle" data-toggle="collapse" href="#' + message.reqId + '">' +
                                '<h6><img class="profile-' + message.senderId + '" src="../../servlets/image?soid=' + message.senderProfileId + '&type=small profile">' + message.title + '<span class="pull-right">' + displayTime + '</span></h6>' +
                            '</a>' +
                        '</div>' +
                        '<div id="' + message.reqId + '" class="accordion-body collapse">' +
                            '<div class="accordion-inner">' +
                                '<p>Do you want to allow <a href="' + rewriteAjaxUrl("../../pages/profile/profile.jsp?id=" + message.senderId) + '">' + message.senderName + '</a> to join ' + message.groupName + '?</p>' +
                                '<button id="acceptGroup-' + message.reqId + '" class="btn btn-success">Accept</button>&nbsp;&nbsp;' +
                                '<button id="rejectGroup-' + message.reqId + '" class="btn btn-danger">Reject</button>' +
                            '</div>' +
                        '</div>' +
                  '</div>';

            $(document).on('click', '#acceptGroup-' + message.reqId, function()
            {
                var reqId = $(this).attr('id').replace('acceptGroup-','');
                var postData = {reqId: reqId};
                var message = _.find(data.messages, { reqId: reqId});

                $.ajax({
                  url: rewriteAjaxUrl('../../servlets/messages/' + thisMessages.userId + '/accept/') ,
                  type: 'POST',
                  xhrFields: {
                      withCredentials: true
                  },
                  dataType: 'json',
                  data: JSON.stringify(postData),
                  success: function()
                  {
                    $.jGrowl(message.senderName + " is now a member of " + message.groupName, {life:2000});

                      $("#group-" + reqId).remove();

                  },
                  error: function()
                  {
                    alert('Error - Unable to allow ' + message.senderName + ' to join ' + message.groupName);
                  }
                });
            });

            $(document).on('click', '#rejectGroup-' + message.reqId, function()
            {
                var reqId = $(this).attr('id').replace('rejectGroup-','');
                var postData = {reqId: reqId};
                var message = _.find(data.messages, { reqId: reqId});

                $.ajax({
                  url: rewriteAjaxUrl('../../servlets/messages/' + thisMessages.userId + '/reject/'),
                  type: 'POST',
                  xhrFields: {
                       withCredentials: true
                  },
                  dataType: 'json',
                  data: JSON.stringify(postData),
                  success: function()
                  {
                    $.jGrowl("You have not allowed " + message.senderName + " to join " + message.groupName, {life:2000});

                      $("#group-" + reqId).remove();
                  },
                  error: function()
                  {
                    alert('Error - Something went wrong when rejecting the request ');
                  }
                })
            });

            message.isRead = false;

          }
          /*else if (message.type === 'notification')
          {
            listMainText = $('<div></div>').addClass("listMainText");
            messageText = $('<p>' + message.text + "</p>");

            var del = $("<img/>").attr('src', rewriteAjaxUrl("../../styles/common/images/delete.gif")).css("margin", "0 10px 0 0").css("float", "right");

            del.click(function(userId, notificationId)
            {
              return function()
              {
                var postData = {reqId: notificationId};
                $.ajax({
                  url: rewriteAjaxUrl('../../servlets/messages/' + userId + '/delete/') ,
                  type: 'POST',
                  dataType: 'json',
                  xhrFields: {
                      withCredentials: true
                  },                   
                  data: JSON.stringify(postData),
                  success: function()
                  {
                    $("#" + senderId + "-undefined").next().remove();
                    $("#" + senderId + "-undefined").remove();
                  },
                  error: function()
                  {
                    alert('Error - Unable to delete notification');
                  }
                })
              };
            }(thisMessages.userId, message.notId));

            messageText.append(del);
            listMainText.append(messageText);

            message.isRead = false;
          }*/

          //$('#messageList').append(html);


          /*var listElement = makeListElement(message.senderId,
              message.threadId,
              message.isRead,
              message.title,
              message.senderName,
              message.senderProfileId,
              listMainText,
              message.date);*/

          listDiv.append(html);
        }
      }
    }
  }, thisMessages);
};

Messages.prototype.getMessageThread = function(threadId)
{
  var thisMessages = {};
  thisMessages.message = this;
  thisMessages.userId = this.userId;

  $.ajax({
    url: rewriteAjaxUrl('../../servlets/messages/' + this.userId + '/thread/' + threadId),
    dataType: 'json',
    xhrFields: {
        withCredentials: true
    },     
    success: function(data)
    {
      if (data !== null)
      {
        $("#messageTitle").html(data.messageTitle);

        var listDiv = $('#' + thisMessages.message.divName);
        //add one row to the table of friends per item returned
        for (var i = 0; i < data.messages.length; i++)
        {
          var message = data.messages[i];
          var listMainText = $('<div></div>').addClass("listMainText");
          listMainText.html("<p>" + message.text + "</p>");

          thisMessages.message.addRecipient(message.senderId);

          var listElement = makeListElement(message.senderId,
              threadId,
              false,
              message.title,
              message.senderName,
              message.senderProfileId,
              listMainText,
              message.date);

          listDiv.append(listElement);
        }

        $('.accordion-body', listDiv).last().addClass('in');
        console.log($('.accordion-body', listDiv).last());
      }
    }
  }, thisMessages);
};


Messages.prototype.initEditor = function(divName)
{
  // Set up the editor
  tinyMCE.init({
    // General options
    mode : "none",
    theme : "advanced",
    plugins : "spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
    theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,insertdate,inserttime,preview,|,forecolor,backcolor",
    theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
    theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,link,unlink,anchor,image,cleanup,code",

    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom"
  });

  // Add to text area
  tinyMCE.execCommand("mceAddControl", true, divName);
};

Messages.prototype.addRecipient = function(userId)
{
  var found = false;
  for (var i = 0; i < this.recipients.length; i++)
  {
    if (this.recipients[i] === userId)
    {
      found = true;
    }
  }
  if (!found)
  {
    this.recipients.push(userId);
  }
};

Messages.prototype.removeRecipient = function(userId)
{
  var index = this.recipients.indexOf(userId);
  if (index >= 0)
  {
    this.recipients.splice(index, 1);
  }
};

Messages.prototype.sendMessage = function(editorDivName, title, threadId)
{
  var ed = tinyMCE.get('editor');
  ed.setProgressState(1);

  if (title.length <= 0)
  {
    alert("Please add a title");
  }
  else
  {
    if (this.recipients.length <= 0)
    {
      alert("Please add a some recipients");
    }
    else
    {
      if (ed.getContent().length <= 0)
      {
        alert("Please write a message");
      }
      else
      {
        var msgToSend = new Object();

        msgToSend.recipients = this.recipients;
        msgToSend.title = title;
        msgToSend.threadId = threadId;
        msgToSend.body = ed.getContent();

        $.ajax({
          url: rewriteAjaxUrl('../../servlets/messages/' + this.userId + '/send/'),
          type: 'POST',
          dataType: 'json',
          xhrFields: {
              withCredentials: true
          },           
          data: JSON.stringify(msgToSend),
          success: function(result)
          {
            $.jGrowl("Message Sent", {life:2000});
            if (threadId === "")
            {
              window.location = rewriteAjaxUrl("../../pages/messages/viewmessage.jsp?id=" + result.Result.threadId);
            }
            else
            {
              window.location.reload();
            }
          },
          error: function()
          {
            alert('Something went wrong');
          }
        })
      }
    }
  }
  ed.setProgressState(0);

};


function makeListElement(senderId, threadId, isRead, title, senderName, senderProfileId, summary, date)
{
    var timestamp = moment(date, 'MMM D YYYY at hh:mm A').format('X');
    var displayTime;

    if(moment(date, 'MMM D YYYY at hh:mm A').isSame(new Date(), 'day')){
        displayTime = moment(date, 'MMM D YYYY at hh:mm A').format('HH:mm');
    }
    else {
        displayTime = moment(date, 'MMM D YYYY at hh:mm A').format('D MMM')
    }

    var unread = isRead ? '' : 'unread';

    var html =  '<div class="accordion-group thread-' + threadId + ' ' + unread + '">' +
                    '<div class="accordion-heading">' +
                        '<a class="accordion-toggle" data-toggle="collapse" href="#' + timestamp + '">' +
                            '<h6><img class="profile-' + senderId + '" src="../../servlets/image?soid=' + senderProfileId + '&type=small profile">' + senderName + '<span class="pull-right">' + displayTime + '</span></h6>' +
                        '</a>' +
                    '</div>' +
                    '<div id="' + timestamp + '" class="accordion-body collapse">' +
                        '<div class="accordion-inner">' + summary.html() + '</div>' +
                    '</div>' +
                '</div>';

    var messageTemplate = $(html);

    return messageTemplate;
}