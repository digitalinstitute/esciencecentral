<%--
  Terms and Conditions
  Author: Simon
--%>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <%@ include file="../../WEB-INF/jspf/page.jspf" %>

  <script type="text/javascript">
    $(document).ready(function()
    {
      clearTabs();
    });
  </script>
</head>
<body>
<div id="wrapper" class="container_12 clearfix">
  <%@include file="../../WEB-INF/jspf/page/top.jspf" %>

  <!-- Caption Line -->
  <h2 class="grid_12 caption">Terms and Conditions</h2>

  <div class="hr grid_12 clearfix">&nbsp;</div>

  <%-- Main Content Left Hand Side--%>
  <div class="grid_12 content">

   <p>
     By registering, you are indicating that you have read and agree that: You are responsible for any content that you upload. You may remove your content at any time. We may make copies for operational purposes. This service is governed by the JANET Acceptable Use Policy. Takedown requests will be reviewed by the e-Science Central team. We will take every care to preserve data but we will not be liable for loss or damage to data while it is stored within e-Science Central. We receive and store certain types of information whenever you interact with us which we will use only for operational purposes and to support academic research into the use of e-Science Central. We will not make personally identifiable data available to third parties without consent. Please contact us if you have any queries about our terms or our privacy policy.
   </p>
  </div>


  <!-- Footer -->
  <div id="footer">
    <div class="container">

      <div id="partners" class="clearfix">
        <div class="partner-logo">
          <img src="../../styles/esc/images/science-city.png" alt="Newcastle Science City"/>
        </div>
        <div class="partner-logo">
          <img src="../../styles/esc/images/jisc.png" alt="JISC"/>
        </div>
        <div class="partner-logo">
          <img src="../../styles/esc/images/epsrc.png" alt="EPSRC"/>
        </div>
        <div class="partner-logo">
          <img src="../../styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
        </div>
        <div class="partner-logo">
          <img src="../../styles/esc/images/side.png" alt="SiDE"/>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
