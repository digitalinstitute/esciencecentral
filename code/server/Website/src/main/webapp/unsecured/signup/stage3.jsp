<%@ page import="com.connexience.server.util.SessionUtils" %>
<%@ page import="com.connexience.server.web.util.FlashUtils" %><%

    //delete the JSESSIONID cookie as they're not logged into SSO
    Cookie jsessionid = SessionUtils.getCookie(request, "JSESSIONID");
    if (jsessionid != null) {

        jsessionid.setMaxAge(-1);
        jsessionid.setPath(request.getContextPath());
        response.addCookie(jsessionid);

    }

    if (session != null) {
        session.invalidate();
    }

    FlashUtils.setSuccess(request, "Thank you for registering, please log in.");
    response.sendRedirect(request.getContextPath() + "/pages/front/index.jsp");
%>