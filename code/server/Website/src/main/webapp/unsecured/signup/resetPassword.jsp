<%@page import="com.connexience.server.web.util.ScriptMerger" %>
<%
    ScriptMerger _merger = new ScriptMerger("", "../../scripts");
    String jQueryVersion = "1.8.3";//1.7.2"; //"1.4.2"
    String jQueryUIVersion = "1.9.2"; //"1.8.24";// "1.8.5";
%>
<!DOCTYPE HTML>
<html>
<head>
    <%@include file="../../WEB-INF/jspf/page/ticket.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/theme.jspf" %>
    <%@include file="../../WEB-INF/jspf/page/organisationredirect.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery/<%=jQueryVersion%>/jquery.js"></script>
    <%@include file="../../WEB-INF/jspf/includes/jquery-ui.jspf" %>
    <%@include file="../../WEB-INF/jspf/includes/scriptloader.jspf" %>
    <script type="text/javascript" src="../../scripts/jquery-validate/jquery.validate.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#resetForm").validate({
                rules: {
                    email: {
                        required:true,
                        email: true
                    }
                },
                messages: {
                    email: {
                        email: "Your email address must be in the format of name@domain.com"
                    }
                }
            });
        });
    </script>
</head>
<body>
<div id="wrapper" class="container clearfix">
    <div class="row-fluid">
        <div class="span6" style="margin:25px 0;">
            <a href="../../pages/front/index.jsp">
                <h1 id="logo" class="grid_6">
                    <img src="../../styles/<%=themeFolder%>/images/logo_head.png"/>
                </h1>
                <h4 id="tagline"><%=tagLine%>
                </h4>
            </a>
        </div>
        <hr style="clear:both;">
        <h2>Reset Your Password</h2>

        <div class="row-fluid">
            <%-- Main Content Left Hand Side--%>
            <div class="span12">
                <div class="dialog">
                    <form action="sendPasswordEmail.jsp" method="POST" id="resetForm">
                        <div class="control-group">
                            <label class="control-label">Email Address</label>
                            <div class="controls">
                                <input type="text" size="75" placeholder="Email" id="email" name="email" class="input-xlarge"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <input type="submit" value="Reset" class="btn"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="push"></div>
    </div>
</div>
<!-- Footer -->
<div id="footer">
    <div class="container">
        <p class="copyright">&copy; Copyright <%=siteName%> </p>

        <div id="partners" class="clearfix">
            <div class="partner-logo">
                <img src="../../styles/esc/images/science-city.png" alt="Newcastle Science City"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/jisc.png" alt="JISC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/epsrc.png" alt="EPSRC"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/newcastle-university.png" alt="Newcastle University"/>
            </div>
            <div class="partner-logo">
                <img src="../../styles/esc/images/side.png" alt="SiDE"/>
            </div>
        </div>
    </div>
</div>

</body>
</html>