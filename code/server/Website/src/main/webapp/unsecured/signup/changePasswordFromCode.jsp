<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="com.connexience.server.ejb.directory.UserDirectoryRemote" %>
<%@ page import="com.connexience.server.web.util.FlashUtils" %>
<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">



  <%
    UserDirectoryRemote ud = EJBLocator.lookupUserDirectoryBean();

    String password1 = request.getParameter("password1");
    String password2 = request.getParameter("password2");
    String code = request.getParameter("code");

    if (!StringUtils.isEmpty(password1) && !StringUtils.isEmpty(password2) && !StringUtils.isEmpty(code))
    {
      if (ud.validatePasswordRecoveryCode(code))
      {
        if (password1.equals(password2))
        {
          boolean success = ud.changePasswordFromCode(code, password1);
          if (success)
          {
            FlashUtils.setInfo(request, "Password changed, please login above.");
            response.sendRedirect("../../pages/front/index.jsp");
          }
          else
          {
            FlashUtils.setError(request, "Error resetting password");
            response.sendRedirect("../../unsecured/signup/resetPassword.jsp");
          }
        }
        else
        {
          FlashUtils.setError(request, "Passwords supplied do not match");
          response.sendRedirect("../../unsecured/signup/validatePasswordCode.jsp?code=" + code);
        }
      }
      else
      {
        FlashUtils.setError(request, "Invalid link to reset password");
        response.sendRedirect("../../unsecured/signup/resetPassword.jsp");
      }
    }
    else
    {
      FlashUtils.setError(request, "Error resetting password");
      response.sendRedirect("../../unsecured/signup/resetPassword.jsp");
    }

  %>

</html>
