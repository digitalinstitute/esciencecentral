<%@ page import="com.connexience.server.ejb.util.EJBLocator" %>
<%--
    Document   : index
    Created on : 27-Jun-2008, 12:07:39
    Author     : hugo
--%>



<h2>Creating Organisation...</h2>
<div class="row-fluid">
    <div class="span12">
    <%
        //String p1 = request.getParameter("username");
        //String p2 = request.getParameter("password");
        String p3 = request.getParameter("orgname");
        String p4 = request.getParameter("storageFolder");
        String p5 = request.getParameter("awsAccessKey");
        String p6 = request.getParameter("awsSecretKey");
        String p7 = request.getParameter("awsDomainName");
        String p8 = request.getParameter("awsGlacierVaultName");
        String p9 = request.getParameter("adminGroup");
        String p10 = request.getParameter("usersGroup");
        String p11 = request.getParameter("firstName");
        String p12 = request.getParameter("lastName");
        String p13 = request.getParameter("username");
        String p14 = request.getParameter("password");
        String gmailUser = request.getParameter("gmailUsername");
        String gmailPassword = request.getParameter("gmailPassword");
        if(EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(null) == null)
        {
          if(p3!=null && p4!=null && p5!=null && p6!=null && p7!=null && p8!=null && p9!=null && p10!=null){
              EJBLocator.lookupOrganisationDirectoryBean().setupNewOrganisation(p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, gmailUser, gmailPassword);
          }
        }
        response.sendRedirect("../../index.jsp");
    %>
    </div>
</div>


