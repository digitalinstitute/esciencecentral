package com.connexience.server.web.util;

/**
 * Author: Simon
 * Date: Jan 5, 2010
 */
public class SearchResult
{
  private String name;

  private String link;

  private String details;

  private String image;

  private String imageAlt;
  
  private String javascript;

  public SearchResult()
  {
  }

  public SearchResult(String name, String link, String details, String image, String imageAlt)
    {
      this.name = name;
      this.link = link;
      this.details = details;
      this.image = image;
      this.imageAlt = imageAlt;

    }


  public SearchResult(String name, String link, String details, String image, String imageAlt, String javascript)
  {
    this.name = name;
    this.link = link;
    this.details = details;
    this.image = image;
    this.imageAlt = imageAlt;
    this.javascript = javascript;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getLink()
  {
    return link;
  }

  public void setLink(String link)
  {
    this.link = link;
  }

  public String getDetails()
  {
    return details;
  }

  public void setDetails(String details)
  {
    this.details = details;
  }

  public String getImage()
  {
    return image;
  }

  public void setImage(String image)
  {
    this.image = image;
  }

  public String getImageAlt()
  {
    return imageAlt;
  }

  public void setImageAlt(String imageAlt)
  {
    this.imageAlt = imageAlt;
  }

  public String getJavascript()
  {
    return javascript;
  }

  public void setJavascript(String javascript)
  {
    this.javascript = javascript;
  }
}
