package com.connexience.server.web.provenance;

import com.connexience.server.ejb.provenance.JSONReportRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.web.APIUtils;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("serial")
public class ProvenanceReportServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String versionId = request.getParameter("versionId");
            String documentId = request.getParameter("documentId");
            String objectType = request.getParameter("objectType");
            String invocationId = request.getParameter("invocationId");

            //default to existing behaviour of document
            if (objectType == null || objectType.equals("")) {
                objectType = "document";
            }

            String jsonProv;

            //lookup the provenance of a document
            switch (objectType) {
                case "document":
                    jsonProv = EJBLocator.lookupJSONProvenanceBean().getDataProvenance(documentId, versionId, request.getServerName() + ":" + request.getServerPort());
                    break;
                case "invocation":
                    jsonProv = EJBLocator.lookupJSONProvenanceBean().getWorkflowProvenance(invocationId, request.getServerName() + ":" + request.getServerPort());
                    break;
                default:
                    throw new Exception("Unknown object type parameter passed to provenance servlet: " + objectType);
            }

            APIUtils.setSecurityHeaders(response);
            response.getWriter().write(jsonProv);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
