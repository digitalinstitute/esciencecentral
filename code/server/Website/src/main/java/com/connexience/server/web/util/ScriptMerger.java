/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.web.util;

import com.yahoo.platform.yui.compressor.CssCompressor;
import com.yahoo.platform.yui.compressor.JavaScriptCompressor;
import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.mozilla.javascript.ErrorReporter;
import org.mozilla.javascript.EvaluatorException;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import java.io.*;
import java.security.MessageDigest;
import java.util.ArrayList;
/**
 * This class merges a set of script resources into a single file that can be served
 * more efficiently
 * @author hugo
 */
public class ScriptMerger {
    private static final Logger logger = Logger.getLogger(ScriptMerger.class);
    private final static File tmpDir;
    private static Boolean staticProductionFlag = null;
    static {
        File baseTemp = new File(System.getProperty("java.io.tmpdir"));
        
        tmpDir = new File(baseTemp, "scripts");
        if(!tmpDir.exists()){
            logger.info("Creating scripts dir: " + tmpDir);
            tmpDir.mkdir();
        } else {
            logger.info("Using compressed scripts dir: " + tmpDir);
        }

        // Empty tmpDir
        if(tmpDir.exists()){
            // Empty the temporary directory
            File[] contents = tmpDir.listFiles();
            for(int i=0;i<contents.length;i++){
                if(contents[i].isFile()){
                    contents[i].delete();
                }
            }
        }
    }
    
    /** List of script resources */
    private ArrayList<ScriptResource> javascriptResources = new ArrayList<>();
            
    /** List of CSS resources */
    private ArrayList<CssResource> cssResources = new ArrayList<>();
    
    /** Base scripts directory */
    private String baseDir;

    /** Relative URL */
    private String relativeUrl;
    
    /** Hash of Javascript names */
    private MessageDigest jsNamesHash;
    
    /** Hash of CSS names */
    private MessageDigest cssNamesHash;
    
    public ScriptMerger(String relativeUrl, String baseDir) {
        this.baseDir = baseDir;
        this.relativeUrl = relativeUrl;
        try {
            jsNamesHash = MessageDigest.getInstance("MD5");
            cssNamesHash = MessageDigest.getInstance("MD5");
        } catch (Exception e){
            logger.error("Error creating message digest in ScriptMerger: " + e.getMessage());
        }
    }
    
    public void addCssResource(String url, boolean compressed){
        CssResource resource = new CssResource(url, url, compressed);
        cssResources.add(resource);
        updateCSSHash(resource);
    }
    
    public void addCssResource(String baseUrl, String url, boolean compressed){
        CssResource resource = new CssResource(url, baseUrl + "/" + url, compressed);
        resource.setBaseUrlOverridden(true);
        resource.setOverriddenBaseUrl(baseUrl);
        updateCSSHash(resource);
        cssResources.add(resource);
    }
    
    public void addJavascriptResource(String url, boolean compressed){
        ScriptResource resource = new ScriptResource(url, url, compressed);
        javascriptResources.add(resource);
        updateJSHash(resource);
    }      
       
    public void addScriptTag(String tag){
        try {
            String scriptName = parseScriptTag(tag);
            ScriptResource resource = new ScriptResource(scriptName, tag, true);
            javascriptResources.add(resource);
            updateJSHash(resource);
            
        } catch (Exception e){
            logger.error("Error adding script tag: " + e.getMessage());
        }
    }
    
    public void addMinifiedScriptTag(String tag){
        try {
            String scriptName = parseScriptTag(tag);
            ScriptResource resource = new ScriptResource(scriptName, tag, false);
            javascriptResources.add(resource);
            updateJSHash(resource);
            
        } catch (Exception e){
            logger.error("Error adding script tag: " + e.getMessage());
        }        
    }
    
    public void sendCss(ServletContext ctx, JspWriter jspOut) throws IOException {
        boolean inProductionMode = ScriptMerger.checkProductionFlag(ctx);

        if(inProductionMode){
            String fileDigest = new String(Hex.encodeHex(cssNamesHash.digest()));
            File compressedFile = new File(tmpDir, fileDigest + ".css");
            if(compressedFile.exists()){
                jspOut.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../../servlets/script/" + compressedFile.getName() + "\"/>");
            } else {
                // Compress the resources
                logger.info("Compressing CSS resources for file digest: " + fileDigest);
                InputStream stream = null;
                String resourceLocation;
                String line;

                
                PrintWriter out = new PrintWriter(compressedFile);
                for(CssResource r : cssResources){
                    try {
                        if(r.isBaseUrlOverridden()){
                            resourceLocation = r.getOverriddenBaseUrl() + r.getUrl();
                        } else {
                            resourceLocation = baseDir + r.getUrl();
                        }
                        stream = ctx.getResourceAsStream(resourceLocation);
                        
                        if(stream!=null){
                            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

                            if(r.isCompressed()){
                                // Compress
                                CssCompressor c = new CssCompressor(reader);
                                c.compress(out, 20000);

                            } else {
                                // Don't compress
                                while((line=reader.readLine())!=null){
                                    out.println(line);
                                }

                            }
                            reader.close();
                            stream.close();
                            stream = null;
                        } else {
                            logger.error("Cannot locate resource for compression: " + resourceLocation);
                        }

                    } catch (IOException e){
                        logger.error("Error writing resource: " + e.getMessage());
                    } finally {
                        if(stream!=null){
                            stream.close();
                        }
                    }
                }                
                
                out.flush();
                compressedFile.deleteOnExit();
                jspOut.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"../../servlets/script/" + compressedFile.getName() + "\"/>");
                
            }
        } else {
            for(CssResource r : cssResources){
                jspOut.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + relativeUrl + baseDir + r.getUrl() + "\"/>");
            }
        }
    }
    
    public void sendScripts(ServletContext ctx, JspWriter jspOut) throws IOException {
        // Get production mode from servlet init param
        boolean inProductionMode = ScriptMerger.checkProductionFlag(ctx);
        if(inProductionMode){
            String fileDigest = new String(Hex.encodeHex(jsNamesHash.digest()));

            File compressedFile = new File(tmpDir, fileDigest + ".js");
            if(compressedFile.exists()){
                // Just send the script tag
                jspOut.println("<script type=\"text/javascript\" src=\"../../servlets/script/" + compressedFile.getName() + "\"></script>");

            } else {
                // Compress the file
                logger.info("Compressing resources for file digest: " + fileDigest);
                InputStream stream = null;
                String resourceLocation;
                String line;
                // Compress using YUI
                ErrorReporter reporter = new ErrorReporter() {

                    @Override
                    public void warning(String string, String string1, int i, String string2, int i1) {
                        logger.warn(string + ": " + string1 + ": " + i + ": " + string2);
                    }

                    @Override
                    public void error(String string, String string1, int i, String string2, int i1) {
                        logger.error(string + ": " + string1 + ": " + i + ": " + string2);
                    }

                    @Override
                    public EvaluatorException runtimeError(String string, String string1, int i, String string2, int i1) {
                        return new EvaluatorException(string + ": " + string1 + ": " + i + ": " + string2);
                    }
                };

                PrintWriter out = new PrintWriter(compressedFile);
                for(ScriptResource r : javascriptResources){
                    try {
                        resourceLocation = baseDir + r.getUrl();
                        stream = ctx.getResourceAsStream(resourceLocation);
                        if(stream!=null){
                            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

                            if(r.isCompressed()){
                                // Compress
                                JavaScriptCompressor c = new JavaScriptCompressor(reader, reporter);
                                c.compress(out, 20000, true, false, false, false);

                            } else {
                                // Don't compress
                                while((line=reader.readLine())!=null){
                                    out.println(line);
                                }

                            }
                            reader.close();
                            stream.close();
                            stream = null;
                        } else {
                            logger.error("Cannot locate resource for compression: " + resourceLocation);
                        }

                    } catch (IOException e){
                        logger.error("Error writing resource: " + e.getMessage());
                        //out.println("<script type=\"text/javascript\" src=\"" + baseDir + "/" + r.getUrl() + "\"></script>");
                    } finally {
                        if(stream!=null){
                            stream.close();
                        }
                    }
                }
                out.flush();
                compressedFile.deleteOnExit();
                jspOut.println("<script type=\"text/javascript\" src=\"../../servlets/script/" + compressedFile.getName() + "\"></script>");

            }
        } else {
            for(ScriptResource r : javascriptResources){
                jspOut.println("<script type=\"text/javascript\" src=\"" + relativeUrl + baseDir + r.getUrl() + "\"></script>");
            }
        }
    }
    
    private static boolean checkProductionFlag(ServletContext ctx){
        if(staticProductionFlag==null){
            String prodFlag = ctx.getInitParameter("minifyScripts");
            
            if(prodFlag!=null && prodFlag.equalsIgnoreCase("true")){
                staticProductionFlag = true;
            } else {
                staticProductionFlag = false;
            }            
        }
        
        if(staticProductionFlag!=null){
            return staticProductionFlag.booleanValue();
        } else {
            return false;
        }
    }
    
    private void updateJSHash(ScriptResource resource){
        jsNamesHash.update(resource.getRawUrl().getBytes());
    }
    
    private void updateCSSHash(CssResource resource){
        cssNamesHash.update(resource.getRawUrl().getBytes());
    }
    
    private String parseScriptTag(String tag) throws IOException {
        int startPos = tag.indexOf("src=\"");
        int endPos = tag.indexOf("\"", startPos + 5);
        if(startPos!=-1 && endPos>startPos){
            String src = tag.substring(startPos + 5, endPos);
            
            // Strip off the base directory
            if(relativeUrl!=null && src.startsWith(relativeUrl)){
                src = src.substring(relativeUrl.length());
            }
            
            // Strip off the base directory
            if(baseDir!=null && src.startsWith(baseDir)){
                src = src.substring(baseDir.length());
            }

            return src;
        } else {
            throw new IOException("Invalid tag structure");
        }
    }
    
    private class CssResource {
        /** Resource name */
        private String url;
        
        /** Should this resource be compressed */
        private boolean compressed;
        
        /** Raw declaration */
        private String rawUrl;

        /** Override the default base URL */
        private boolean baseUrlOverridden = false;
        
        /** Base url if overridden */
        private String overriddenBaseUrl = "";
        
        public CssResource(String url, String rawUrl, boolean compressed) {
            this.url = url;
            if(!this.url.startsWith("/")){
                this.url = "/" + this.url;
            }            
            this.rawUrl = rawUrl;
            this.compressed = compressed;
        }

        public boolean isBaseUrlOverridden() {
            return baseUrlOverridden;
        }

        public void setBaseUrlOverridden(boolean baseUrlOverridden) {
            this.baseUrlOverridden = baseUrlOverridden;
        }

        public String getOverriddenBaseUrl() {
            return overriddenBaseUrl;
        }

        public void setOverriddenBaseUrl(String overriddenBaseUrl) {
            this.overriddenBaseUrl = overriddenBaseUrl;
        }

        public String getRawUrl() {
            return rawUrl;
        }

        public String getUrl() {
            return url;
        }

        public boolean isCompressed() {
            return compressed;
        }
    }
    
    private class ScriptResource {
        /** Declared URL */
        private String url;

        /** Compress this resource */
        private boolean compressed;
        
        /** String to use for the digest */
        private String rawUrl;
        
        public ScriptResource(String url, String rawUrl, boolean compressed) {
            this.url = url;
            if(!this.url.startsWith("/")){
                this.url = "/" + this.url;
            }
            this.compressed = compressed;
            this.rawUrl = rawUrl;
        }

        public String getRawUrl() {
            return rawUrl;
        }

        public String getUrl() {
            return url;
        }

        public boolean isCompressed() {
            return compressed;
        }
    }
}