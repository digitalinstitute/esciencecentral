package com.connexience.server.web.tags;

import com.connexience.server.model.social.TagCloudElement;

import java.util.List;

public interface FontSizeComputationStrategy
{
  public void computeFontSize(List<TagCloudElement> elements);
}
