/*
 * RESTArchiveServlet.java
 */

package com.connexience.server.web.archive;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class RESTArchiveServlet extends HttpServlet
{
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        response.setContentType("application/octet-stream");

        try
        {
            String restArchiveDir = getServletConfig().getInitParameter("RESTArchiveDir");
                    
            String documentId = request.getParameter("documentId");
            String versionId  = request.getParameter("versionId");

            System.out.println("restArchiveDir = [" + restArchiveDir + "]");
            System.out.println("documentId     = [" + documentId + "]");
            System.out.println("versionId      = [" + versionId + "]");
            
            if ((restArchiveDir != null) & (documentId != null) & (versionId != null))
            {
                File restArchiveDirFile = new File(restArchiveDir);
                if (! restArchiveDirFile.isDirectory())
                {
                    File restArchiveParentDirFile = restArchiveDirFile.getParentFile();
                    if (! restArchiveParentDirFile.isDirectory())
                        throw new IOException("Can't create restArchiveDir [" + restArchiveDirFile + "], parent [" + restArchiveParentDirFile + "] not a directory");

                    if (! restArchiveDirFile.mkdir())
                        throw new IOException("Can't create restArchiveDir [" + restArchiveDirFile + "]");
                }

                File archiveFile = new File(restArchiveDirFile, documentId + "-" + versionId);

                InputStream      archiveFileInputStream  = request.getInputStream();
                FileOutputStream archiveFileOutputStream = new FileOutputStream(archiveFile);

                byte[] readBuffer = new byte[4096];
                int    readLength = archiveFileInputStream.read(readBuffer);
                while (readLength != -1)
                {
                    archiveFileOutputStream.write(readBuffer, 0, readLength);
                    readLength = archiveFileInputStream.read(readBuffer);
                }
                archiveFileInputStream.close();
            }
            else
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
         throws ServletException, IOException
    {
        response.setContentType("application/octet-stream");

        try
        {
            String restArchiveDir = getServletConfig().getInitParameter("RESTArchiveDir");
            
            String documentId = request.getParameter("documentId");
            String versionId  = request.getParameter("versionId");

            System.out.println("restArchiveDir = [" + restArchiveDir + "]");
            System.out.println("documentId     = [" + documentId + "]");
            System.out.println("versionId      = [" + versionId + "]");

            if ((restArchiveDir != null) & (documentId != null) & (versionId != null))
            {
                File archiveFile = new File(new File(restArchiveDir), documentId + "-" + versionId);
                
                if (archiveFile.isFile() && archiveFile.canRead())
                {
                    FileInputStream archiveFileInputStream  = new FileInputStream(archiveFile);
                    OutputStream    archiveFileOutputStream = response.getOutputStream();
                    
                    byte[] readBuffer = new byte[4096];
                    int    readLength = archiveFileInputStream.read(readBuffer);
                    while (readLength != -1)
                    {
                        archiveFileOutputStream.write(readBuffer, 0, readLength);
                        readLength = archiveFileInputStream.read(readBuffer);
                    }
                    archiveFileInputStream.close();
                }
                else
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
            else
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
