/**
 *
 */
package com.connexience.server.web.tags;

import com.connexience.server.model.social.TagCloudElement;

import java.util.Collections;
import java.util.List;

/**
 * This represents a tag cloud.  The elements are sorted and font sizes computed according to the required strategy
 */
public class TagCloudImpl implements TagCloud
{
  private List<TagCloudElement> elements = null;

  public TagCloudImpl(List<TagCloudElement> elements, FontSizeComputationStrategy strategy)
  {
    this.elements = elements;
    strategy.computeFontSize(this.elements);
    Collections.sort(this.elements);
  }

  public List<TagCloudElement> getTagCloudElements()
  {
    return this.elements;
  }

}
