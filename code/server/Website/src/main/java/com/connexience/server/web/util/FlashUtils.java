package com.connexience.server.web.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class FlashUtils {
    public static final String errorKey = "errors";
    public static final String warningsKey = "warnings";
    public static final String successKey = "successes";
    public static final String infoKey = "infos";

    public static void setError(HttpServletRequest request, String error) {
        HttpSession session = request.getSession();
        List<String> errors;

        if (session.getAttribute(errorKey) != null) {
            errors = (List<String>) session.getAttribute(errorKey);
        } else {
            errors = new ArrayList<>();
        }
        errors.add(error);
        session.setAttribute(errorKey, errors);
    }

    public static void setWarning(HttpServletRequest request, String warning) {
        HttpSession session = request.getSession();
        List<String> warnings;

        if (session.getAttribute(warningsKey) != null) {
            warnings = (List<String>) session.getAttribute(warningsKey);
        } else {
            warnings = new ArrayList<>();
        }
        warnings.add(warning);
        session.setAttribute(warningsKey, warnings);
    }

    public static void setSuccess(HttpServletRequest request, String success) {
        HttpSession session = request.getSession();
        List<String> successes;

        if (session.getAttribute(successKey) != null) {
            successes = (List<String>) session.getAttribute(successKey);
        } else {
            successes = new ArrayList<>();
        }
        successes.add(success);
        session.setAttribute(successKey, successes);

    }

    public static void setInfo(HttpServletRequest request, String info) {
        HttpSession session = request.getSession();
        List<String> infos;

        if (session.getAttribute(infoKey) != null) {
            infos = (List<String>) session.getAttribute(infoKey);
        } else {
            infos = new ArrayList<>();
        }
        infos.add(info);
        session.setAttribute(infoKey, infos);

    }

    public static String getFlash(HttpServletRequest request) {
        List<String> errors;
        List<String> warnings;
        List<String> successes;
        List<String> infos;

        String output = "<script type=\"text/javascript\">$(document).ready(function() {";

        if (request.getSession().getAttribute(errorKey) != null) {
            errors = (List<String>) request.getSession().getAttribute(errorKey);
            for (String error : errors) {
                output += "flashMessage(\"" + error + "\",\"error\");";
            }
            request.getSession().removeAttribute(errorKey);
        }

        if (request.getSession().getAttribute(warningsKey) != null) {
            warnings = (List<String>) request.getSession().getAttribute(warningsKey);
            for (String warning : warnings) {
                output += "flashMessage(\"" + warning + "\",\"warning\");";
            }
            request.getSession().removeAttribute(warningsKey);
        }

        if (request.getSession().getAttribute(successKey) != null) {
            successes = (List<String>) request.getSession().getAttribute(successKey);
            for (String success : successes) {
                output += "flashMessage(\"" + success + "\",\"success\");";
            }
            request.getSession().removeAttribute(successKey);
        }

        if (request.getSession().getAttribute(infoKey) != null) {
            infos = (List<String>) request.getSession().getAttribute(infoKey);
            for (String info : infos) {
                output += "flashMessage(\"" + info + "\",\"info\");";
            }
            request.getSession().removeAttribute(infoKey);
        }

        output += "});</script>";
        return output;

    }


    public static String getFrontPageFlash(HttpServletRequest request) {
        List<String> errors;
        List<String> warnings;
        List<String> successes;
        List<String> infos;

        String output = "<script type=\"text/javascript\">$(document).ready(function() {";

        if (request.getSession().getAttribute(errorKey) != null) {
            errors = (List<String>) request.getSession().getAttribute(errorKey);
            for (String error : errors) {
                output += "flashMessage(\"" + error + "\",\"error\",$(\"#splash-logo span\"));";
            }
            request.getSession().removeAttribute(errorKey);
        }

        if (request.getSession().getAttribute(warningsKey) != null) {
            warnings = (List<String>) request.getSession().getAttribute(warningsKey);
            for (String warning : warnings) {
                output += "flashMessage(\"" + warning + "\",\"warning\",$(\"#splash-logo span\"));";
            }
            request.getSession().removeAttribute(warningsKey);
        }

        if (request.getSession().getAttribute(successKey) != null) {
            successes = (List<String>) request.getSession().getAttribute(successKey);
            for (String success : successes) {
                output += "flashMessage(\"" + success + "\",\"success\",$(\"#splash-logo span\"));";
            }
            request.getSession().removeAttribute(successKey);
        }

        if (request.getSession().getAttribute(infoKey) != null) {
            infos = (List<String>) request.getSession().getAttribute(infoKey);
            for (String info : infos) {
                output += "flashMessage(\"" + info + "\",\"info\",$(\"#splash-logo span\"));";
            }
            request.getSession().removeAttribute(infoKey);
        }

        output += "});</script>";
        return output;

    }
}
