/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.study;

import com.connexience.api.model.EscEvent;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.events.EscEventCursor;
import com.connexience.server.events.AbstractEventStore;
import com.connexience.server.events.EventStoreManager;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.EventsToData;
import com.connexience.server.util.SessionUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.CSVDataExporter;

/**
 * This servlet allows data for a device to be downloaded
 * @author hugo
 */
public class DeviceMonitoringDownloadServlet extends HttpServlet {
    
    private static final Logger logger = Logger.getLogger(DeviceMonitoringDownloadServlet.class);

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = null;        
        try {
            out = response.getWriter();
            String documentType = request.getParameter("datatype");
            String eventType = request.getParameter("eventtype");
            String gatewayId = request.getParameter("id");
            String studyId = request.getParameter("studyId");
            String startDateParam = request.getParameter("start");
            String endDateParam = request.getParameter("end");
            String limitParam = request.getParameter("maxrows");
            
            Date startDate;
            if(startDateParam!=null && !startDateParam.isEmpty()){
                startDate = new Date(Long.parseLong(startDateParam));
            } else {
                startDate = new Date();
            }
            
            Date endDate;
            if(endDateParam!=null && !endDateParam.isEmpty()){
                endDate = new Date(Long.parseLong(endDateParam));
            } else {
                endDate = new Date();
            }
                
                
            int maxRows ;
            if(limitParam!=null && !limitParam.isEmpty()){
                maxRows = Integer.valueOf(limitParam);
            } else {
                maxRows = 10000;
            }
            
            WebTicket ticket = SessionUtils.getTicket(request);
            
            FlatStudy study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, Integer.valueOf(studyId));
            FlatGateway gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, Integer.valueOf(gatewayId));
            
            if(documentType.equals("csv")){
                response.setContentType("text/csv");
                List<EscEvent> events = EJBLocator.lookupEventStoreBean().query(ticket, gateway.getExternalId(), study.getExternalId(), eventType, startDate, endDate, maxRows);
                Data dataSet = new EventsToData(events).toData();
                CSVDataExporter exporter = new CSVDataExporter(dataSet);
                exporter.setMissingValueText("");
                exporter.writeToPrintWriter(out);
                out.flush();
                
            } else if(documentType.equals("eventtypes")){
                // Return a list of the event types
                response.setContentType("application/json");
                List<String> eventTypes = EJBLocator.lookupEventStoreBean().listEventTypes(ticket, gateway.getExternalId(), study.getExternalId());
                JSONArray results = new JSONArray();
                for(String t : eventTypes){
                    results.put(t);
                }
                results.write(out);
                out.flush();
                       
            } else if(documentType.equals("mostrecent")){
                response.setContentType("application/json");
                EscEvent event = EJBLocator.lookupEventStoreBean().getMostRecent(ticket, gateway.getExternalId(), study.getExternalId(), eventType);
                if(event!=null){
                    out.println(event.toJsonObject().toString(1));
                } else {
                    out.println("{}");
                }
                out.flush();
                
            } else if(documentType.equals("trend")){
                // Data trend
                response.setContentType("application/json");
                List<EscEvent> events = EJBLocator.lookupEventStoreBean().query(ticket, gateway.getExternalId(), study.getExternalId(), eventType, startDate, endDate, maxRows);

                JSONArray results = new JSONArray();

                for(EscEvent event : events){
                    results.put(event.toJsonObject());
                }
                out.write(results.toString(1));
                out.flush();
                
            } else if(documentType.equals("gpx")){
                // Create a trace file for the map
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                response.setContentType("text/xml");
                List<EscEvent> events = EJBLocator.lookupEventStoreBean().query(ticket, gateway.getExternalId(), study.getExternalId(), "Location", startDate, endDate, maxRows);
                
                out.println("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" creator=\"Oregon 400t\" version=\"1.1\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\">");
                out.println("<metadata>");
                out.println("<link href=\"http://www.esciencecentral.co.ul\"><text>e-Science Central</text></link>");
                out.println("<time>" + format.format(new Date()) + "</time>");
                out.println("</metadata>");
                out.println("<trk><name>Track</name><trkseg>");
                
                for(EscEvent event : events){
                    out.println("<trkpt lat=\"" + event.doubleValue("Latitude") + "\" lon=\"" + event.doubleValue("Longitude") + "\">");
                    out.println("<time>" + format.format(new Date(event.getTimestamp())) + "</time>");
                    out.println("</trkpt>");
                }
                out.println("</trkseg></trk>");
                out.println("</gpx>");
                out.flush();
            }
           
        } catch (Exception e){
            if(!response.isCommitted()){
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            } else {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Montior data download";
    }

}
