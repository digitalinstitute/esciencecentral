/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.web.compressor;

import com.connexience.server.web.APIUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
/**
 * This Servlet sends out compressed scripts
 * @author hugo
 */
public class CompressedScriptServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(CompressedScriptServlet.class);
    private static final File tmpDir;
    static {
        File baseTemp = new File(System.getProperty("java.io.tmpdir"));
        
        tmpDir = new File(baseTemp, "scripts");
        if(!tmpDir.exists()){
            logger.info("Creating scripts dir in: " + System.getProperty("java.io.tmpdir"));
            tmpDir.mkdir();
        }
    }    

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
        String fileName = sections[sections.length - 1];

        File script = new File(tmpDir, fileName);
        if(script.exists()){
            if(fileName.toLowerCase().endsWith(".js")){
                response.setContentType("text/javascript");
            } else {
                response.setContentType("text/css");
            }
            
            response.setContentLength((int)script.length());
            PrintWriter writer = response.getWriter();
            // Now send this file
            FileReader reader = new FileReader(script);
            char[] buffer = new char[16384];
            int len;
            while((len=reader.read(buffer))!=-1){
                writer.write(buffer, 0, len);
            }
            writer.flush();
            reader.close();              
        }
    }

    @Override
    protected long getLastModified(HttpServletRequest request) {
        String[] sections = APIUtils.splitRequestPath(request.getPathInfo());
        String fileName = sections[sections.length - 1];

        File script = new File(tmpDir, fileName);
        if(script.exists()){
            return script.lastModified();
        } else {
            return super.getLastModified(request);
        }     
    }

    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet for delivering compressed scripts";
    }
}
