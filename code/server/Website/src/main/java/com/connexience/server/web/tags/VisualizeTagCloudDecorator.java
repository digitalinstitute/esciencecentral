package com.connexience.server.web.tags;

public interface VisualizeTagCloudDecorator {
    public String decorateTagCloud(TagCloud tagCloud);
}
