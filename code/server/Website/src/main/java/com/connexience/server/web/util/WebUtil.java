/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.util;

import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Event;

import javax.servlet.http.HttpServletRequest;

/** @author nsjw7 */
public class WebUtil {

    public static String getHostname(HttpServletRequest request) {

        String hostname = request.getHeader("x-forwarded-host");
        if (hostname == null || hostname.equals("")) {
            hostname = request.getServerName().toString() + ":" + request.getServerPort();
        }

        return "http://" + hostname;
    }

    public static String getClientIP(HttpServletRequest request) {

        String clientIP = request.getHeader("x-forwarded-for");   //If it's been forwarded by Apache
        if (clientIP == null || clientIP.equals("")) {
            clientIP = request.getRemoteAddr();
        }

        return clientIP;
    }

    public static boolean checkParam(String param) {
        return param != null && !param.equals("");
    }

    /*
   * Method to construct a link to the social object for use within the web based system.  Used to format a link in the news item
   * */
    public static String constructLink(ServerObject so) {
        if (so instanceof Event) {
            return "<a href='../secure/events.jsp?id=" + so.getId() + "'>" + so.getName() + "</a>";
        } else if (so instanceof User) {
            User u = (User) so;
            return "<a href='../../pages/profile/profile.jsp?id=" + so.getId() + "'>" + u.getDisplayName() + "</a>";
        } else if (so instanceof Group) {
            return "<a href='../secure/groups.jsp?groupId=" + so.getId() + "'>" + so.getName() + "</a>";
        } else if (so instanceof DocumentRecord) {
            return "<a style='curor:pointer;' onclick='quickview.showFile(\"" + so.getId() + "\");'>" + so.getName() + "</a>";
        } else if (so instanceof Folder) {
            return so.getName();
        } else {
            return "<a style='curor:pointer;' onclick='quickview.showFile(\"" + so.getId() + "\");'>" + so.getName() + "</a>";
        }
    }

}