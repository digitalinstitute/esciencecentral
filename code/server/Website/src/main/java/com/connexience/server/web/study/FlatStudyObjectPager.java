/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.study;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.flatstudy.FlatStudyObject;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.JSONCredentials;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.web.APIUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author hugo
 */
public class FlatStudyObjectPager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        APIUtils.setSecurityHeaders(response);
        WebTicket ticket = SessionUtils.getTicket(request);
        
        try (PrintWriter out = response.getWriter()) {
            
            String sa = request.getParameter("showPropertiesButton");
            String sd = request.getParameter("showDataButton");
            String sm = request.getParameter("showManageButton");
            String sr = request.getParameter("showDeleteButton");
            String db = request.getParameter("showDownloadButton");
            String mb = request.getParameter("showMapButton");
            String ed = request.getParameter("showEventDownloadButton");
            String om = request.getParameter("showMoveButton");

            boolean propertiesButton;
            if(sa!=null && !sa.isEmpty()){
                propertiesButton = Boolean.parseBoolean(sa);
            } else {
                propertiesButton = false;
            }

            boolean dataButton;
            if(sd!=null && !sd.isEmpty()){
                dataButton = Boolean.parseBoolean(sd);
            } else {
                dataButton = false;
            }
            
            boolean manageButton;
            if(sm!=null && !sm.isEmpty()){
                manageButton = Boolean.parseBoolean(sm);
            } else {
                manageButton = false;
            }
            
            boolean mapButton;
            if(mb!=null && !mb.isEmpty()){
                mapButton = Boolean.parseBoolean(mb);
            } else {
                mapButton = false;
            }
            
            boolean deleteButton;
            if(sr!=null && !sr.isEmpty()){
                deleteButton = Boolean.parseBoolean(sr);
            } else {
                deleteButton = false;
            }
            
            boolean downloadButton;
            if(db!=null && !db.isEmpty()){
                downloadButton = Boolean.parseBoolean(db);
            } else {
                downloadButton = false;
            }
            
            boolean eventDownloadButton;
            if(ed!=null && !ed.isEmpty()){
                eventDownloadButton = Boolean.parseBoolean(ed);
            } else {
                eventDownloadButton = false;
            }
            
            boolean moveButton;
            if(om!=null && !om.isEmpty()){
                moveButton = Boolean.parseBoolean(om);
            } else {
                moveButton = false;
            }
            
            String ds = request.getParameter("iDisplayStart");
            int startPos;
            if(ds!=null && !ds.isEmpty()){
                startPos = Integer.parseInt(ds);
            } else {
                startPos = 0;
            }
            
            String ps = request.getParameter("iDisplayLength");
            int pageSize;
            if(ps!=null && !ps.isEmpty()){
                pageSize = Integer.parseInt(ps);
            } else {
                pageSize = 10;
            }
            
            String sid = request.getParameter("sid");
            Integer studyId;
            if(sid!=null && ! sid.isEmpty()){
                studyId = Integer.parseInt(sid);
            } else {
                throw new ServletException("Cannot obtain study id");
            }
            
            String cn = request.getParameter("cn");
            Class objectClass;
            if(cn!=null && !cn.isEmpty()){
                try {
                    objectClass = Class.forName(cn);
                } catch (Exception e){
                    throw new ServerException("Cannot create object class: " + e.getMessage());
                }
            } else {
                throw new ServletException("Unrecognised class");
            }
                
            String st = request.getParameter("sSearch");
            String searchText;
            if(st!=null && !st.isEmpty()){
                searchText = st.trim();
            } else {
                searchText = null;
            }
            
            List results;
            long size;
            
            if(searchText==null){
                results = EJBLocator.lookupFlatStudyBean().getObjects(ticket, studyId, objectClass, startPos, pageSize);
                size = EJBLocator.lookupFlatStudyBean().getObjectCount(ticket, studyId, objectClass);
            } else {
                results = EJBLocator.lookupFlatStudyBean().getObjects(ticket, studyId, objectClass, searchText, startPos, pageSize);
                
                // Search based on hash as well
                if(!searchText.isEmpty()){
                    String hashText = SignatureUtils.getHashedPassword(searchText);
                    List hashResults = EJBLocator.lookupFlatStudyBean().getObjects(ticket, studyId, objectClass, hashText, startPos, pageSize);
                    for(Object r : hashResults){
                        if(!results.contains(r)){
                            results.add(r);
                        }
                    }
                }
                
                size = EJBLocator.lookupFlatStudyBean().getObjectCount(ticket, studyId, objectClass, searchText);
            }
            
            
            JSONObject resultJson = new JSONObject();
            resultJson.put("sEcho", request.getParameter("sEcho"));
            resultJson.put("iTotalRecords", size);
            resultJson.put("iTotalDisplayRecords", size);
            JSONArray data = new JSONArray();

            FlatStudyObject fso;
            JSONArray row;
            DateFormat fmt = SimpleDateFormat.getDateTimeInstance();
            HashMap<String, String> fields;
            
            for(Object o : results){
                fso = (FlatStudyObject)o;
                row = new JSONArray();
                row.put(fmt.format(fso.getCreationDate()));
                row.put(fso.getExternalId());
                fields = fso.getAdditionalDisplayFields();
                for(String key : fields.keySet()){
                    row.put(fields.get(key));
                }
                
                String html = "";

                if(propertiesButton){
                    html = "<i class=\"icomoon-info2\" style=\"cursor:pointer;float:right;\" onclick=\"showProperties('" + fso.getAdditionalDisplayFields() + "');\">&nbsp;</i>";
                }

                if(dataButton){
                    html += "<i class=\"icomoon-folder-open\" style=\"cursor:pointer;float:right;\" onclick=\"showFolder('" + fso.getId() + "');\">&nbsp;</i>";
                }
                
                if(manageButton){
                    html += "<i class=\"fa fa-line-chart\" style=\"cursor:pointer;float:right;\" onclick=\"manageDevice('" + fso.getId()+ "');\">&nbsp;</i>";
                }
                
                if(mapButton){
                    html += "<i class=\"fa fa-map\" style=\"cursor:pointer;float:right;\" onclick=\"mapDevice('" + fso.getId()+ "');\">&nbsp;</i>";
                }
                
                if(eventDownloadButton){
                    html += "<i class=\"fa fa-database\" style=\"cursor:pointer;float:right;\" onclick=\"downloadEvents('" + fso.getId()+ "');\">&nbsp;</i>";
                }
                
                if(deleteButton){
                    html += "<i class=\"icomoon-remove\" style=\"cursor:pointer;float:right;\" onclick=\"deleteDevice('" + fso.getId()+ "','" + fso.getExternalId() + "');\">&nbsp;</i>";
                }
                
                if(downloadButton){
                    html += "<i class=\"icomoon-cloud-download\" style=\"cursor:pointer;float:right;\" onclick=\"downloadRegistration('" + fso.getId()+ "');\">&nbsp;</i>";
                }
                
                if(moveButton){
                    html += "<i class=\"icomoon-share\" style=\"cursor:pointer;float:right;\" onclick=\"moveObject('" + fso.getId()+ "','" + fso.getExternalId() + "');\">&nbsp;</i>";
                }                
                
                if(html!=null && !html.isEmpty()){
                    row.put(html);
                }
                
                data.put(row);
            }

            resultJson.put("aaData", data);
            resultJson.write(out);
            out.flush();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Returns lists of flat study objects";
    }

}
