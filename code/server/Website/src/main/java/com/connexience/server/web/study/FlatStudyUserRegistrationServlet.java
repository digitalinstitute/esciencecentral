package com.connexience.server.web.study;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatPerson;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.util.SignatureUtils;
import com.connexience.server.web.APIUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This servlet registers a user in a FlatStudy
 * @author hugo
 */
@WebServlet(name = "StudyUserRegistrationServlet", urlPatterns = {"/servlets/studyuserregistration"})
public class FlatStudyUserRegistrationServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //header to stop IE caching GET requests as we're not doing 100% pure REST
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");
        APIUtils.setSecurityHeaders(response);
        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;

        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");
                String path = request.getParameter("path");
                String id = request.getParameter("id");

                if (method != null) {
                    //get any post data that might be there (needs to be in JSON format)
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    }
                    
                    if(method.equalsIgnoreCase("registerUser")){
                        // Register a new user
                        registerUser(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("registerUserList")){
                        // Register a set of users
                        registerUserList(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("deleteUser")){
                        // Delete a person
                        deleteUser(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("listFlatStudies")){
                        // List studies that a user can move to
                        listFlatStudies(ticket, dataObject, resultObject);
                        
                    } else if(method.equalsIgnoreCase("moveUser")){
                        // Move a user to a new study
                        moveUser(ticket, dataObject, resultObject);
                        
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user logged in");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        } finally {
            try {
                resultObject.write(out);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            out.close();
        }

    }
    
    private void moveUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            int userId = dataObject.getInt("userId");
            int targetStudyId = dataObject.getInt("targetStudyId");
            FlatPerson person = EJBLocator.lookupFlatStudyBean().getFlatPerson(ticket, userId);
            if(person!=null && !person.getStudyId().equals(targetStudyId)){
                EJBLocator.lookupFlatStudyBean().moveFlatPerson(ticket, userId, targetStudyId);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "Cannot move person");
            }
            
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void listFlatStudies(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            List<Project> results = EJBLocator.lookupProjectsBean().getVisibleProjects(ticket, 0, Integer.MAX_VALUE);

            JSONArray studyList = new JSONArray();
            JSONArray studyNames = new JSONArray();
            
            JSONObject flatStudyJson;
            for(Project p : results){
                if(p instanceof FlatStudy){
                    flatStudyJson = new JSONObject();
                    flatStudyJson.put("id", p.getId());
                    flatStudyJson.put("externalId", p.getExternalId());
                    studyList.put(flatStudyJson);
                    studyNames.put(p.getExternalId());
                }
            }
            resultObject.put("studies", studyList);
            resultObject.put("studyNames", studyNames);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void deleteUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            Integer userId = dataObject.getInt("userId");
            EJBLocator.lookupFlatStudyBean().removeFlatPerson(ticket, userId);
            APIUtils.setSuccess(resultObject);
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void registerUser(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String userId = dataObject.getString("userId");
            Integer studyId = dataObject.getInt("studyId");
            FlatPerson existing = EJBLocator.lookupFlatStudyBean().getFlatPersonByExternalId(ticket, studyId, userId);
            if(existing==null){
                FlatPerson newPerson = EJBLocator.lookupFlatStudyBean().createFlatPerson(ticket, studyId, userId);
                resultObject.put("userId", userId);
                resultObject.put("studyId", studyId);
                resultObject.put("id", newPerson.getId());
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "User: " + userId + " already existing in study");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void registerUserList(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            JSONArray userIds = dataObject.getJSONArray("userIds");
            Integer studyId = dataObject.getInt("studyId");
            for(int i=0;i<userIds.length();i++){
                String userId = userIds.getString(i);
                
                // Do hashing if needed
                if(dataObject.has("hashIds") && dataObject.getBoolean("hashIds")==true){
                    StringTokenizer tokenizer = new StringTokenizer(userId, "_");
                    if(tokenizer.countTokens()==2){
                        // There is a email_name format
                        String email = tokenizer.nextToken();
                        String name = tokenizer.nextToken();
                        userId = SignatureUtils.getHashedPassword(email) + "_" + SignatureUtils.getHashedPassword(name);
                    } else if(tokenizer.countTokens()==1){
                        // Just a string
                        userId = SignatureUtils.getHashedPassword(userId);
                    } else {
                        throw new Exception("Incorrectly formatted ID string");
                    }
                }    
                
                if(userId!=null && !userId.isEmpty()){                    
                    FlatPerson existing = EJBLocator.lookupFlatStudyBean().getFlatPersonByExternalId(ticket, studyId, userId);
                    if(existing==null){
                        FlatPerson newPerson = EJBLocator.lookupFlatStudyBean().createFlatPerson(ticket, studyId, userId);
                        resultObject.put("userId", userId);
                        resultObject.put("studyId", studyId);
                        resultObject.put("id", newPerson.getId());
                    }
                }
            }
            APIUtils.setSuccess(resultObject);
            
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }        
    }
}