/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.web.study;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.RandomGUID;
import com.connexience.server.util.SessionUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author hugo
 */
@WebServlet(name = "DeviceRegistrationServlet", urlPatterns = {"/servlets/deviceregistration"})
public class DeviceRegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = null;        
        try {
            String hostname;
            int port;
            boolean secure;
            
            // Heuristics to get hostname
            if(request.getHeader("Referer")!=null){
                // There is a referrer, so a link was clicked on - this will have the url
                URL u = new URL(request.getHeader("Referer"));
                hostname = u.getHost();
                
                if(u.getProtocol().equalsIgnoreCase("https")){
                    secure = true;
                } else {
                    secure = false;
                }
                
                if(u.getPort()!=-1){
                    port = u.getPort();
                } else {
                    if(secure){
                        port = 443;
                    } else {
                        port = 80;
                    }
                }    
                
            } else if(request.getHeader("X-Forwarded-Host")!=null){
                // This came from Apache
                hostname = request.getHeader("X-Forwarded-Host");
                URL u = new URL(request.getRequestURL().toString());
                secure = request.isSecure();
                
                if(u.getPort()!=-1){
                    port = u.getPort();
                } else {
                    if(secure){
                        port = 443;
                    } else {
                        port = 80;
                    }
                }
                
                
            } else {
                // Direct connection
                URL u = new URL(request.getRequestURL().toString());
                hostname = u.getHost();
                secure = request.isSecure();
                
                if(u.getPort()!=-1){
                    port = u.getPort();
                } else {
                    if(secure){
                        port = 443;
                    } else {
                        port = 80;
                    }
                }
                
            }
            
            Enumeration<String> headers = request.getHeaderNames();
            String name;
            while(headers.hasMoreElements()){
                name = headers.nextElement();
                log(name + ": " + request.getHeaders(name));
            }
            
            out = response.getWriter();
            String method = request.getParameter("method");
            WebTicket ticket = SessionUtils.getTicket(request);
            
            if(method!=null && method.equals("download")){
                String studyId = request.getParameter("studyId");
                String deviceId = request.getParameter("deviceId");
                FlatStudy study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, Integer.valueOf(studyId));
                FlatGateway gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, Integer.valueOf(deviceId));
                
                response.setContentType("application/json");
                JSONObject registrationJson = new JSONObject();
                registrationJson.put("hostname", hostname);
                registrationJson.put("port", port);
                registrationJson.put("secure", secure);
                
                registrationJson.put("studyCode", study.getExternalId());
                registrationJson.put("deviceId", gateway.getExternalId());
                registrationJson.put("devicePassword", gateway.getDevicePassword());
                registrationJson.put("deviceType", gateway.getGatewayType());
                registrationJson.write(out);              
                
            } else {
                String studyId = request.getParameter("studyId");
                String deviceType = request.getParameter("deviceType");
                
                // Get the correct study
                FlatStudy study = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, Integer.valueOf(studyId));
                EJBLocator.lookupFlatStudyBean().checkFlatStudyCodeFolders(ticket, study.getId());
                String deviceId = new RandomGUID().toString();
                FlatGateway gateway = EJBLocator.lookupFlatStudyBean().createFlatGateway(ticket, study.getId(),deviceId);
                String devicePassword = new RandomGUID().toString();
                gateway.setDevicePassword(devicePassword);
                gateway.setGatewayType(deviceType);
                gateway = EJBLocator.lookupFlatStudyBean().saveFlatGateway(ticket, gateway);            

                response.setContentType("application/json");
                JSONObject registrationJson = new JSONObject();
                registrationJson.put("hostname", hostname);
                registrationJson.put("port", port);
                registrationJson.put("secure", secure);

                
                registrationJson.put("studyCode", study.getExternalId());
                registrationJson.put("deviceId", deviceId);
                registrationJson.put("devicePassword", devicePassword);
                registrationJson.write(out);                
            }
                    

        } catch (Exception e) {
            if(!response.isCommitted()){
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
            } else {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } finally {
            out.flush();
        }
    }


    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Registration servlet";
    }
}