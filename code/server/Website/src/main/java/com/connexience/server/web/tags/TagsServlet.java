/*
 * TagsServlet.java
 */
package com.connexience.server.web.tags;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.social.Tag;
import com.connexience.server.util.SessionUtils;
import com.connexience.server.web.APIUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;

/**
 * This class provides a servlet that manages object tags
 * @author hugo
 */
public class TagsServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();

        try {
            if (ticket != null) {
                String method = request.getParameter("method");
                String id = request.getParameter("id");

                if (method != null) {


                    if (method.equalsIgnoreCase("listTags")) {
                        // List the tags for an object
                        listTags(ticket, id, resultObject);


                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setHeader("Cache-Control", "max-age=0,no-cache,no-store,post-check=0,pre-check=0");
        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT");

        PrintWriter out = response.getWriter();
        WebTicket ticket = SessionUtils.getTicket(request);
        HttpSession session = request.getSession(true);
        JSONObject resultObject = new JSONObject();
        JSONObject dataObject = null;
        
        try {
            if (ticket != null) {
                String data = APIUtils.extractString(request.getInputStream());
                String method = request.getParameter("method");

                if (method != null) {
                    if (data != null && !data.equals("")) {
                        dataObject = new JSONObject(data);
                    } else {
                        dataObject = new JSONObject();
                    }
                    
                    if (method.equalsIgnoreCase("addTag")) {
                        // Add a tag to an object
                        addTag(ticket, dataObject, resultObject);

                    } else if(method.equalsIgnoreCase("removeTag")){
                        // Remove a tag from an object
                        removeTag(ticket, dataObject, resultObject);
                        
                    }
                }
            } else {
                APIUtils.setError(resultObject, "No user is logged on");
            }
        } catch (Exception e) {
            APIUtils.populateExceptionData(resultObject, e);
        }

        try {
            resultObject.write(out);
        } catch (Exception e) {
        }
        out.flush();    
    }
    
    private void removeTag(Ticket ticket, JSONObject dataObject, JSONObject resultObject){
        try {
            String tagId = dataObject.getString("tagid");
            String id = dataObject.getString("id");
            
            ServerObject s = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, id, ServerObject.class);
            
            if(s!=null){
                Tag t = EJBLocator.lookupTagBean().getTag(ticket, tagId);
                if(t!=null){
                    EJBLocator.lookupTagBean().removeTag(ticket, s, t);
                    APIUtils.setSuccess(resultObject);
                } else {
                    APIUtils.setError(resultObject, "No such tag");
                }

            } else {
                APIUtils.setError(resultObject, "No such object");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void addTag(Ticket ticket, JSONObject dataObject, JSONObject resultObject) {
        try {
            String id = dataObject.getString("id");
            String tagText = dataObject.getString("text").toLowerCase();
            ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, id, ServerObject.class);
            if(obj!=null){
                EJBLocator.lookupTagBean().addTag(ticket, obj, tagText);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such object");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
    
    private void listTags(Ticket ticket, String id, JSONObject resultObject){
        try {
            ServerObject obj = EJBLocator.lookupObjectDirectoryBean().getServerObject(ticket, id, ServerObject.class);
            if(obj!=null){
                Collection tags = EJBLocator.lookupTagBean().getTags(ticket, obj);
                Tag t;
                JSONArray tagsArray = new JSONArray();
                JSONObject tagJson;
                Iterator i = tags.iterator();
                while(i.hasNext()){
                    t = (Tag)i.next();
                    tagJson = new JSONObject();
                    tagJson.put("id", t.getId());
                    tagJson.put("text", t.getTagText());
                    tagsArray.put(tagJson);
                }
                resultObject.put("tags", tagsArray);
                APIUtils.setSuccess(resultObject);
            } else {
                APIUtils.setError(resultObject, "No such object");
            }
        } catch (Exception e){
            APIUtils.populateExceptionData(resultObject, e);
        }
    }
}
