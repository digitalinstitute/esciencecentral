package com.connexience.server.web.tags;

import com.connexience.server.model.social.TagCloudElement;

import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This outputs the HTML for the tag cloud according to the buckets defined in the font size calculator
 * */
public class HTMLTagCloudDecorator implements VisualizeTagCloudDecorator
{
  private Map<String, String> fontMap = null;

  public HTMLTagCloudDecorator()
  {
    getFontMap();
  }

  private void getFontMap()
  {
    this.fontMap = new HashMap<>();
    fontMap.put("font-size: 0", "font-size: 12px");
    fontMap.put("font-size: 1", "font-size: 18px");
    fontMap.put("font-size: 2", "font-size: 24px");
  }

  public String decorateTagCloud(TagCloud tagCloud)
  {
    StringWriter sw = new StringWriter();
    List<TagCloudElement> elements = tagCloud.getTagCloudElements();
    for (TagCloudElement tce : elements)
    {
      try
      {
        sw.append("&nbsp;<a href='../../pages/search/tagsearch.jsp?t=" + URLEncoder.encode(tce.getTagText(), "UTF-8") + "' style=\"" + this.fontMap.get(tce.getFontSize()) + ";\">");
        sw.append(tce.getTagText() + "</a> ");
      }
      catch (Exception e)
      {
        //do nothing - catches a URL encoder error
      }

    }
    return sw.toString();
  }

}
