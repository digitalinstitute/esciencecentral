#!/bin/sh
mvn -f pom.xml package || exit 1

#Create a symlink if the file doesn't exist
if [ ! -h $JBOSS_HOME/standalone/deployments/website.war ]
  then
    ln -s $CONNEXIENCE_HOME/code/server/Website/target/website-3.1-SNAPSHOT.war $JBOSS_HOME/standalone/deployments/website.war
fi

#Create the file which will trigger the deployment
touch $JBOSS_HOME/standalone/deployments/website.war.dodeploy
