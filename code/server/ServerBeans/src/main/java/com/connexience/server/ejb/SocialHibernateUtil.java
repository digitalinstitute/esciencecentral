/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import org.hibernate.SessionFactory;

/**
 * This class provides basic utility methods for access the social networking
 * database via Hibernate.
 * @author hugo
 */
public abstract class SocialHibernateUtil {
    /** Session factory */
    private static SessionFactory factory;
    
    /** Set up the session factory */
    static {
        try {
            // Use default hibernate.cfg.xml file
            factory = new org.hibernate.cfg.Configuration().configure().buildSessionFactory();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println("Error creating factory: " + e.getMessage());
        }
    };
    
    /** Get the created session factory */
    public static SessionFactory getSessionFactory(){
        return factory;
    }    
}