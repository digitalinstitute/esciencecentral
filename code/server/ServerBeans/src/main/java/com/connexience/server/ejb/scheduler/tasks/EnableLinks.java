/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.workflow.WorkflowDeploymentUtils;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.EarExtractor;
import com.connexience.server.workflow.util.ZipUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This task searches deployed WARs for default documents (i.e. example study
 * spreadsheets and sets a preference that enables the download link.
 *
 * @author simon
 */
public class EnableLinks extends SchedulerTask {
    Logger logger = Logger.getLogger(EnableLinks.class);
    private Ticket adminTicket = null;

    public EnableLinks(SchedulerBean parentBean) {
        super(parentBean);
        setEnabled(true);
        setName("EnableLinks");
        setRepeating(false);
        setStartDelayed(false);
    }

    @Override
    public void run() {
        logger.info("Running EnableLinks task");

        try {
            adminTicket = parentBean.getDefaultOrganisationAdminTicket();

            String workingDir = System.getProperty("jboss.server.temp.dir") + File.separator + "enableLinks" + File.separator + "contents";

            File wd = new File(workingDir);
            if (!wd.exists()) {
                wd.mkdirs();
            }

            String deployDir = System.getProperty("jboss.server.base.dir") + File.separator + "deployments";
            String earName = "esc.ear";

            logger.info("Extracting .ear into: " + workingDir);

            EarExtractor extractor = new EarExtractor(earName, deployDir, workingDir + File.separator + "lib");
            extractor.addNamePattern("*example-spreadsheet*.war");

            ArrayList<File> extractedFiles = extractor.extract();

            if (extractedFiles.size() > 0) {
                //found the example Spreadsheet
                logger.info("Found example-spreadsheet.war");
                EJBLocator.lookupPreferencesBean().add("StudyManagement", "EnableSpreadsheetDownload", true);
            } else {
                logger.info("Not Found example-spreadsheet.war");
                EJBLocator.lookupPreferencesBean().add("StudyManagement", "EnableSpreadsheetDownload", false);
            }

            File workingDirFile = new File(workingDir);
            if(workingDirFile.exists()){
                workingDirFile.delete();
            }

        } catch (Exception e) {
            logger.error("Error enabling links" + e.getMessage());
        }
    }
}