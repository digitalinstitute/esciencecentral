/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler;

import com.connexience.server.util.JSONEditable;
import java.util.Calendar;
import java.util.Timer;
import org.json.JSONObject;

import java.util.TimerTask;

/**
 * This class extends the basic Java timer task to add a few extra features to 
 * make it work better in e-SC
 * @author hugo
 */
public abstract class SchedulerTask extends TimerTask implements JSONEditable {
    /** Is this a repeating task */
    protected boolean repeating = false;
    
    /** Is the start delayed */
    protected boolean startDelayed = false;
    
    /** Repeat interval */
    protected int repeatInterval = 60000;
            
    /** Task name */
    protected String name = "Task";

    /** Is this task enabled */
    protected boolean enabled = true;
    
    /** Time delay before starting in milliseconds */
    protected int startDelay = 10000;
    
    /** Parent scheduler bean */
    protected SchedulerBean parentBean;
    
    /** Timer for this task */
    protected Timer timer;

    public SchedulerTask() {
    }

    public SchedulerTask(SchedulerBean parentBean){
        super();
        this.timer = new Timer(true);
        this.parentBean = parentBean;
    }
    
    public boolean isRepeating() {
        return repeating;
    }

    public void setRepeating(boolean repeating) {
        this.repeating = repeating;
    }

    public boolean isStartDelayed() {
        return startDelayed;
    }

    public void setStartDelayed(boolean startDelayed) {
        this.startDelayed = startDelayed;
    }

    public int getStartDelay() {
        return startDelay;
    }

    public void setStartDelay(int startDelay) {
        this.startDelay = startDelay;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRepeatInterval(int repeatInterval) {
        this.repeatInterval = repeatInterval;
    }

    public int getRepeatInterval() {
        return repeatInterval;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    public void removeFromParent(){
        parentBean.taskFinished(this);
    }

    @Override
    public boolean cancel() {
        timer.cancel();
        return super.cancel();
    }
    
    
    /** Schedule this task for execution */
    public void scheduleTask(){
        Calendar startTime = Calendar.getInstance();

        // Delay the start time if needed
        if(startDelayed){
            startTime.add(Calendar.MILLISECOND, startDelay);
        }

        // Schedule task
        if(repeating){
            // Repeating task
            timer.schedule(this, startTime.getTime(), repeatInterval);

        } else {
            // One-shot task
            timer.schedule(this, startTime.getTime());
        }        
    }

    @Override
    public JSONObject toJson() throws Exception {
        JSONObject json = new JSONObject();
        json.put("Enabled", enabled);
        json.put("Name", name);
        json.put("RepeatInterval", repeatInterval);
        json.put("Repeating", repeating);
        json.put("DelayedStart", startDelayed);
        json.put("StartDelay", startDelay);
        return json;
    }

    @Override
    public void readJson(JSONObject json) throws Exception {
        enabled = json.getBoolean("Enabled");
        name = json.getString("Name");
        repeatInterval = json.getInt("RepeatInterval");
        repeating = json.getBoolean("Repeating");
        startDelayed = json.getBoolean("DelayedStart");
        startDelay = json.getInt("StartDelay");
    }
}