/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import javax.ejb.EJBLocalHome;
import org.apache.log4j.Logger;
/**
 * This tasks runs at startup and checks to see if the config folder exists
 * @author hugo
 */
public class CheckConfigFolder extends SchedulerTask {
    private static Logger logger = Logger.getLogger(CheckConfigFolder.class);
    
    public CheckConfigFolder(SchedulerBean parent) {
        super(parent);
        repeating = false;
        startDelayed = false;
        enabled = true;        
    }

    
    @Override
    public void run() {
        logger.info("Starting CheckConfigFolder task");
        try {
            if(parentBean.defaultOrganisationExists()){
                Ticket ticket = parentBean.getDefaultOrganisationAdminTicket();
                Organisation org = parentBean.getCachedDefaultOrganisation();
                Folder configFolder;
                if(org.getConfigFolderId()==null || org.getConfigFolderId().isEmpty()){
                    // No folder set
                    configFolder = new Folder();
                    configFolder.setName("config");
                    configFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, org.getDataFolderId(), configFolder);
                    org.setConfigFolderId(configFolder.getId());
                    org = EJBLocator.lookupOrganisationDirectoryBean().saveOrganisation(ticket, org);
                    logger.info("Creating config folder");
                } else {
                    configFolder = EJBLocator.lookupStorageBean().getFolder(ticket, org.getConfigFolderId());
                    if(configFolder==null){
                        configFolder = new Folder();
                        configFolder.setName("config");
                        configFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, org.getDataFolderId(), configFolder);    
                        org.setConfigFolderId(configFolder.getId());
                        org = EJBLocator.lookupOrganisationDirectoryBean().saveOrganisation(ticket, org);                        
                        logger.info("Creating config folder");
                    }
                }
                
                // Check permission
                
                User publicUser = EJBLocator.lookupUserDirectoryBean().getUser(ticket, parentBean.getPublicUserId(org.getId()));
                if(publicUser!=null){
                    if(!EJBLocator.lookupAccessControlBean().permissionExists(parentBean.getPublicUserId(org.getId()), configFolder, Permission.READ_PERMISSION)) {
                        EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getId(), configFolder.getId(), Permission.READ_PERMISSION);
                        logger.info("Added public read permission to config folder");
                    }
                    
                    if(!EJBLocator.lookupAccessControlBean().permissionExists(parentBean.getPublicUserId(org.getId()), configFolder, Permission.WRITE_PERMISSION)) {
                        EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getId(), configFolder.getId(), Permission.WRITE_PERMISSION);
                        logger.info("Added public write permission to config folder");
                    }                    
                }
                
                // Now check individual folders
                Folder engineConfig = EJBLocator.lookupStorageBean().getNamedFolder(ticket, configFolder.getId(), "engine");
                if(engineConfig==null){
                    engineConfig = new Folder();
                    engineConfig.setName("engine");
                    engineConfig = EJBLocator.lookupStorageBean().addChildFolder(ticket, configFolder.getId(), engineConfig);
                    logger.info("Created engine config folder");
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getId(), engineConfig.getId(), Permission.READ_PERMISSION);
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getDefaultGroupId(), engineConfig.getId(), Permission.READ_PERMISSION);
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getId(), engineConfig.getId(), Permission.WRITE_PERMISSION);
                    EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUser.getDefaultGroupId(), engineConfig.getId(), Permission.WRITE_PERMISSION);
                    
                }
                
            }
        } catch (Exception e){
            logger.error("Error checking config folder: " + e.getMessage(), e);
        }
    }
    
}
