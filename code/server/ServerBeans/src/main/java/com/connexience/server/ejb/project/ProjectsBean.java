package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.acl.AccessControlRemote;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.remove.ObjectRemovalRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:17
 */
@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/ProjectsBean", beanInterface = ProjectsRemote.class)
public class ProjectsBean extends HibernateSessionContainer implements ProjectsRemote {
    @EJB
    private GroupDirectoryRemote groupBean;

    @EJB
    private UserDirectoryRemote userBean;

    @EJB
    private ObjectRemovalRemote objectRemovalBean;

    @EJB
    private StorageRemote storageBean;

    @EJB
    private AccessControlRemote aclBean;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    @Override
    public Project getProject(Ticket ticket, Integer projectId) throws ConnexienceException {
        Project project = em.find(Project.class, projectId);

        if (project == null) {
            throw new ConnexienceException("Project id='" + projectId + "' not found in DB.");
        }

        if (project.isPrivateProject() && !isProjectMember(ticket, project)) {
            if (!isOrganisationAdminTicket(ticket)) {
                throw new ConnexienceException("No access to project " + projectId);
            }
        }

        return project;
    }

    @Override
    public List<Project> searchProjects(final Ticket ticket, final String searchTerm) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId")
                .setParameter("userId", ticket.getUserId())
                .getResultList();

        return em.createQuery("SELECT p FROM Project p WHERE (p.adminGroupId IN :groupMemberships OR p.membersGroupId IN :groupMemberships OR p.privateProject = false) AND (lower(p.name) LIKE :searchString OR lower(p.description) LIKE :searchString OR lower(p.externalId) LIKE :searchString)")
                .setParameter("groupMemberships", groupMemberships)
                .setParameter("searchString", "%" + searchTerm.toLowerCase() + "%")
                .getResultList();
    }

    @Override
    public Project saveProject(Ticket ticket, Project project) throws ConnexienceException {
        if (project.getId() == null) {
            //Save it so that we've got the id
            project.setOwnerId(ticket.getUserId());
            em.persist(project);
            project = em.merge(project);

            if (StringUtils.isBlank(project.getAdminGroupId())) {
                Group adminGroup = new Group();

                adminGroup.setName(project.getName() + " - Admins");
                adminGroup.setAdminApproveJoin(true);
                adminGroup.setNonMembersList(true);
                adminGroup = groupBean.saveGroup(ticket, adminGroup);

                project.setAdminGroupId(adminGroup.getId());
            }

            if (StringUtils.isBlank(project.getMembersGroupId())) {
                Group memberGroup = new Group();

                memberGroup.setName(project.getName() + " - Members");
                memberGroup.setAdminApproveJoin(true);
                memberGroup.setNonMembersList(true);
                memberGroup = groupBean.saveGroup(ticket, memberGroup);

                project.setMembersGroupId(memberGroup.getId());
            }

            if (StringUtils.isBlank(project.getDataFolderId())) {
                Folder dataFolder = new Folder();

                final String rootDataFolderId = getCachedOrganisation(ticket.getOrganisationId()).getDataFolderId();

                dataFolder.setName(generateFolderName(project));
                dataFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, rootDataFolderId, dataFolder);
                project.setDataFolderId(dataFolder.getId());

                dataFolder.setProjectId(String.valueOf(project.getId()));
                dataFolder = em.merge(dataFolder);
                em.persist(dataFolder);

                aclBean.grantAccess(ticket, project.getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
                aclBean.grantAccess(ticket, project.getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
                aclBean.grantAccess(ticket, project.getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
                aclBean.grantAccess(ticket, project.getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
            }

            if (StringUtils.isBlank(project.getWorkflowFolderId())) {
                Folder workflowFolder = new Folder();

                workflowFolder.setName("Workflows");
                workflowFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, project.getDataFolderId(), workflowFolder);
                project.setWorkflowFolderId(workflowFolder.getId());

                workflowFolder.setProjectId(String.valueOf(project.getId()));
                workflowFolder = em.merge(workflowFolder);
                em.persist(workflowFolder);
            }

            em.persist(project);
            project = em.merge(project);
        } else {
            if (isProjectAdmin(ticket, project)) {
                if (StringUtils.isNotBlank(project.getDataFolderId())) {
                    // Ensure name is correct
                    Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, project.getDataFolderId());
                    dataFolder.setName(generateFolderName(project));

                    dataFolder = em.merge(dataFolder);

                    // Ensure ACL is correct
                    aclBean.grantAccess(ticket, project.getAdminGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
                    aclBean.grantAccess(ticket, project.getAdminGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
                    aclBean.grantAccess(ticket, project.getMembersGroupId(), dataFolder.getId(), Permission.WRITE_PERMISSION);
                    aclBean.grantAccess(ticket, project.getMembersGroupId(), dataFolder.getId(), Permission.READ_PERMISSION);
                }

                project = em.merge(project);
            } else {
                throw new ConnexienceException("You must be a project admin to update its information.");
            }
        }

        return project;
    }

    @Override
    public void deleteProject(Ticket ticket, Integer projectId) throws ConnexienceException {
        deleteProject(ticket, projectId, false);
    }

    @Override
    public void deleteProject(final Ticket ticket, final Integer projectId, final Boolean deleteFiles) throws ConnexienceException {
        final Project project = em.merge(getProject(ticket, projectId));

        if (isProjectAdmin(ticket, project)) {
            //remove the groups
            Ticket adminTicket = getInternalTicket(); //Get an admin ticket in case we're not the owner

            if (deleteFiles) {
                storageBean.removeFolderTree(ticket, project.getDataFolderId(), true);
            } else {
                Folder folder = storageBean.getFolder(ticket, project.getDataFolderId());

                // put study folder in Owner's home directory
                folder.setName("MIGRATED: " + folder.getName());
                folder.setContainerId(userBean.getUser(adminTicket, project.getOwnerId()).getHomeFolderId());

                em.merge(folder);
            }

            //remove the scanner if it exists
            if (project.getRemoteScannerId() != null) {
                RemoteFilesystemScanner scanner = EJBLocator.lookupScannerBean().getScanner(adminTicket, project.getRemoteScannerId());
                if (scanner != null) {
                    objectRemovalBean.remove(scanner);
                }
            }

            em.remove(project);

            objectRemovalBean.remove(groupBean.getGroup(adminTicket, project.getAdminGroupId()));
            objectRemovalBean.remove(groupBean.getGroup(adminTicket, project.getMembersGroupId()));
        } else {
            throw new ConnexienceException("Must be a Project Admin to delete it.");
        }
    }

    @Override
    public Integer getPublicProjectCount(final Ticket ticket) throws ConnexienceException {
        return ((Number) em.createNamedQuery("Project.public.count").getResultList().get(0)).intValue();
    }

    @Override
    public List<Project> getPublicProjects(final Ticket ticket, final Integer start, final Integer maxResults, final String orderBy, final String direction) throws ConnexienceException {

        if (orderBy.toUpperCase().equals("NAME")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.public.name.asc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.public.name.desc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else if (orderBy.toUpperCase().equals("DATE")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.public.date.asc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.public.date.desc").setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else {
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }


    }

    @Override
    public Long getMemberProjectCount(Ticket ticket) {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId")
                .setParameter("userId", ticket.getUserId())
                .getResultList();

        return (Long) em.createNamedQuery("Project.member.count")
                .setParameter("groupMemberships", groupMemberships)
                .getResultList()
                .get(0);
    }

    @Override
    public List<Project> getMemberProjects(Ticket ticket, Integer start, Integer maxResults, final String orderBy, final String direction) throws ConnexienceException {
        return getMemberProjects(ticket, ticket.getUserId(), start, maxResults, orderBy, direction);
    }

    public List<Project> getMemberProjects(Ticket ticket, Integer start, Integer maxResults) throws ConnexienceException {
        return getMemberProjects(ticket, ticket.getUserId(), start, maxResults, "NAME", "ASC");
    }

    @Override
    public List<Project> getMemberProjects(Ticket ticket, String userId, Integer start, Integer maxResults, final String orderBy, final String direction) throws ConnexienceException {

        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        if (orderBy.toUpperCase().equals("NAME")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.member.name.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.member.name.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else if (orderBy.toUpperCase().equals("DATE")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.member.date.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.member.date.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else {
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }
    }

    @Override
    public Integer getVisibleProjectCount(final Ticket ticket) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId")
                .setParameter("userId", ticket.getUserId())
                .getResultList();

        Long value = (Long) em.createNamedQuery("Project.visible.count")
                .setParameter("groupMemberships", groupMemberships)
                .getResultList()
                .get(0);

        return (int) value.longValue();
    }

    @Override
    public List<Project> getVisibleProjects(final Ticket ticket, final Integer start, final Integer maxResults) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId")
                .setParameter("userId", ticket.getUserId())
                .getResultList();

        return em.createNamedQuery("Project.visible")
                .setParameter("groupMemberships", groupMemberships)
                .setFirstResult(start)
                .setMaxResults(maxResults)
                .getResultList();
    }

    @Override
    public List<Project> getProjectsByExternalId(final Ticket ticket, final String externalId) throws ConnexienceException {
        List<Project> results = new ArrayList<>();

        List<Project> projects = em.createQuery("SELECT p FROM Project p WHERE p.externalId = :externalId")
                .setParameter("externalId", externalId)
                .getResultList();

        // add matching public projects, or projects ticket holder has access to
        for (final Project project : projects) {
            if (!project.isPrivateProject() || isProjectMember(ticket, project)) {
                results.add(project);
            }
        }

        return results;
    }

    @Override
    public List<Project> getMemberActiveProjects(Ticket ticket, Integer start, Integer maxResults, String orderBy, String direction) throws ConnexienceException {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        if (orderBy.toUpperCase().equals("NAME")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.member.active.name.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.member.active.name.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else if (orderBy.toUpperCase().equals("DATE")) {

            if (direction.toUpperCase().equals("ASC")) {
                return em.createNamedQuery("Project.member.active.date.asc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else if (direction.toUpperCase().equals("DESC")) {
                return em.createNamedQuery("Project.member.active.date.desc").setParameter("groupMemberships", groupMemberships).setFirstResult(start).setMaxResults(maxResults).getResultList();
            } else {
                throw new ConnexienceException("Sorting direction is required. Can be either ASC or DESC");
            }
        } else {
            throw new ConnexienceException("Sorting property is required. Can be either NAME or DATE");
        }
    }

    @Override
    public Long getMemberActiveProjectCount(Ticket ticket) {
        final List<String> groupMemberships = em.createQuery("SELECT gm.groupId FROM GroupMembership gm where gm.userId = :userId").setParameter("userId", ticket.getUserId()).getResultList();

        return (Long) em.createNamedQuery("Project.member.count.active").setParameter("groupMemberships", groupMemberships).getResultList().get(0);
    }

    @Override
    public Project createProject(final Ticket ticket, final String projectName) throws ConnexienceException {
        return saveProject(ticket, new Project(projectName, ticket.getUserId()));
    }

    @Override
    public Project createProject(final Ticket ticket, final String projectName, final String escOwnerId) throws ConnexienceException {
        return saveProject(ticket, new Project(projectName, escOwnerId));
    }

    @Override
    public void addProjectMember(Ticket ticket, Integer projectId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        //Must be admin
        if (isProjectAdmin(ticket, project)) {
            // Get an admin ticket so that we don't need join requests
            Ticket adminTicket = getInternalTicket();

            groupBean.addUserToGroup(adminTicket, userId, project.getMembersGroupId());
        } else {
            throw new ConnexienceException("Must be an admin to add users to project");
        }
    }

    @Override
    public void removeProjectMember(Ticket ticket, Integer projectId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        // Can only remove if you are an admin or removing yourself
        if (isProjectAdmin(ticket, project) || ticket.getUserId().equals(userId)) {
            //Creator cannot leave at the moment
            Group members = groupBean.getGroup(ticket, project.getMembersGroupId());
            if (members.getCreatorId().equals(userId)) {
                throw new ConnexienceException("Creator of Project cannot leave it");
            }

            Ticket adminTicket = getInternalTicket(); //Get an admin ticket so that we don't need join requests
            groupBean.removeUserFromGroup(adminTicket, userId, project.getMembersGroupId());
        } else {
            throw new ConnexienceException("Must be an admin to remove users from project");
        }
    }

    @Override
    public void addProjectAdmin(Ticket ticket, Integer projectId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        //Must be admin
        if (isProjectAdmin(ticket, project)) {
            Ticket adminTicket = getInternalTicket(); //Get an admin ticket so that we don't need join requests
            groupBean.addUserToGroup(adminTicket, userId, project.getAdminGroupId());
        } else {
            throw new ConnexienceException("Must be an admin to add admins to Project");
        }
    }

    @Override
    public void removeProjectAdmin(Ticket ticket, Integer projectId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        //Must be admin
        if (isProjectAdmin(ticket, project)) {

            //Creator cannot leave at the moment
            Group admins = groupBean.getGroup(ticket, project.getAdminGroupId());
            if (admins.getCreatorId().equals(userId)) {
                throw new ConnexienceException("Creator of Project cannot leave at present");
            }

            Ticket adminTicket = getInternalTicket(); //Get an admin ticket so that we don't need join requests
            groupBean.removeUserFromGroup(adminTicket, userId, project.getAdminGroupId());
        } else {
            throw new ConnexienceException("Must be an admin to remove admins from Project");
        }
    }

    public boolean isProjectAdmin(Ticket ticket, Project project) throws ConnexienceException {
        project = em.merge(project);
        return userBean.isUserGroupMember(ticket, ticket.getUserId(), project.getAdminGroupId());
    }

    public boolean isProjectMember(Ticket ticket, Project project) throws ConnexienceException {
        return isProjectAdmin(ticket, project) || userBean.isUserGroupMember(ticket, ticket.getUserId(), project.getMembersGroupId());
    }

    @Override
    public void addGroupMember(Ticket ticket, Integer projectId, String groupId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        //Must be admin
        if (isProjectAdmin(ticket, project)) {
            Ticket adminTicket = getInternalTicket(); //Get an admin ticket so that we don't need join requests
            if (groupId.equals(project.getAdminGroupId()) || groupId.equals(project.getMembersGroupId())) {
                groupBean.addUserToGroup(adminTicket, userId, groupId);
            }
            else{
                throw new ConnexienceException("Can only add users to admin or member groups");
            }

        } else {
            throw new ConnexienceException("Must be an admin to add admins to Project");
        }
    }

    @Override
    public void removeProjectMember(Ticket ticket, Integer projectId, String groupId, String userId) throws ConnexienceException {
        final Project project = getProject(ticket, projectId);

        //Must be admin
        if (isProjectAdmin(ticket, project)) {

            //Creator cannot leave at the moment
            Group group = groupBean.getGroup(ticket, groupId);
            if (group.getCreatorId().equals(userId)) {
                throw new ConnexienceException("Creator of Project cannot leave at present");
            }

            Ticket adminTicket = getInternalTicket(); //Get an admin ticket so that we don't need join requests
            groupBean.removeUserFromGroup(adminTicket, userId, groupId);
        } else {
            throw new ConnexienceException("Must be an admin to remove admins from Project");
        }
    }



    private String generateFolderName(final Project project) {
        final StringBuilder result = new StringBuilder("Project: ");
        result.append(project.getName());

        if (StringUtils.isNotBlank(project.getExternalId())) {
            result.append(" (").append(project.getExternalId()).append(")");
        }

        return result.toString();
    }

    @Override
    public long countProjectsWithMatchingExternalId(String externalId) throws ConnexienceException {
        try {
            Query q = em.createNamedQuery("Project.externalid.count", Long.class);
            q.setParameter("externalid", externalId);
            return (long) q.getResultList().get(0);
        } catch (Exception e) {
            throw new ConnexienceException("Error counting projects with matching id: " + e.getMessage(), e);
        }
    }

    @Override
    public long countProjectsWithMatchingName(String name) throws ConnexienceException {
        try {
            Query q = em.createNamedQuery("Project.name.count", Long.class);
            q.setParameter("name", name);
            return (long) q.getResultList().get(0);
        } catch (Exception e) {
            throw new ConnexienceException("Error counting projects with matching id: " + e.getMessage(), e);
        }
    }


}
