/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import org.hibernate.Session;
import org.jboss.logging.Logger;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class HibernateSessionWatcher
{
    public static class SessionTimerTask extends TimerTask
    {
        private AtomicInteger count = new AtomicInteger();

        private final Exception exception;

        private final Session session;

        public SessionTimerTask(Session session, Exception exception)
        {
            this.session = session;
            this.exception = exception;
        }

        @Override
        public void run()
        {
            int c = count.incrementAndGet();
            if (session.isOpen())
            {
                Logger.getLogger(HibernateSessionContainer.class).error("Hibernate session still open (ms=" + (c * INTERVAL) + ")", exception);
            }
            else if (session.isConnected())
            {
                Logger.getLogger(HibernateSessionContainer.class).error("Hibernate session still connected (ms=" + (c * INTERVAL) + ")", exception);
            }
            else
            {
                cancel();
            }
        }
    }

    private static final int INTERVAL = 30000;

    private static Timer timer = new Timer();

    public static void monitor(Session session)
    {
        timer.schedule(new SessionTimerTask(session, new Exception()), INTERVAL, INTERVAL);
    }
}
