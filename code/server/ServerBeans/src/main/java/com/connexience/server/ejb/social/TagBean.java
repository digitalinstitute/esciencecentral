/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.social;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Tag;
import com.connexience.server.model.social.TagToObject;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Collection;
import java.util.Vector;

/**
 * Author: Simon
 * Date: Jun 3, 2009
 * <p/>
 * This bean deals with adding and removing tags from objects
 */
@Stateless
@EJB(name = "java:global/ejb/TagBean", beanInterface = TagRemote.class)
public class TagBean extends HibernateSessionContainer implements TagRemote
{

  /*
  * Add a com.connexience.server.social.tag to a server object
  * */
  public Tag addTag(Ticket ticket, ServerObject so, String tagText) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      User user = (User) getObject(ticket.getUserId(), User.class);
      //if the user can write to the object, add the com.connexience.server.social.tag
      if (user != null && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.WRITE_PERMISSION))
      {
        //if there is an existing com.connexience.server.social.tag with that name, return that com.connexience.server.social.tag
        Query q = session.createQuery("FROM Tag AS t WHERE t.tagText = :tagText");
        q.setString("tagText", tagText);
        Tag existingTag = (Tag) q.uniqueResult();

        if (existingTag != null)
        {
          //check to see if the object is already tagged with that string.  If not, add a mapping
          q = session.createQuery("FROM TagToObject as tto WHERE tto.serverObjectId = :soid AND tto.tagId = :tagId");
          q.setString("soid", so.getId());
          q.setString("tagId", existingTag.getId());

          TagToObject existingTTO = (TagToObject) q.uniqueResult();
          if (existingTTO == null)
          {
            TagToObject newTTO = new TagToObject(so.getId(), existingTag.getId(), ticket.getUserId(), TagToObject.USER_GENERATED);
            savePlainObject(newTTO);
          }

          return existingTag;
        }
        else //otherwise create a new com.connexience.server.social.tag and mapping
        {
          Tag t = new Tag(tagText);
          t.setId((String) session.save(t));
          TagToObject tto = new TagToObject(so.getId(), t.getId(), ticket.getUserId(), TagToObject.USER_GENERATED);
          tto.setId((String) session.save(tto));

          return t;
        }
      }
      else
      {
        return null;
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error creating com.connexience.server.social.tag: ", e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /*
  * Get all of the tags for a server object
  * */
  public Collection getTags(Ticket ticket, ServerObject so) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      User user = (User) getObject(ticket.getUserId(), User.class);
      //if the user can write to the object, add the com.connexience.server.social.tag
      if (user != null && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.READ_PERMISSION))
      {
        Query q = session.createQuery("FROM Tag AS t WHERE t.id IN " +
            "(SELECT tto.tagId FROM TagToObject AS tto WHERE tto.serverObjectId = :soid)");
        q.setString("soid", so.getId());
        return q.list();
      }
      else
      {
        return new Vector();
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error getting tags for: " + so.getId(), e);
    }
    finally
    {
      closeSession(session);
    }

  }

  /*
  * Get a com.connexience.server.social.tag from an id
  * */
  public Tag getTag(Ticket ticket, String tagId) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();

      Query q = session.createQuery("FROM Tag AS t WHERE t.id = :tagId");
      q.setString("tagId", tagId);
      return (Tag) q.uniqueResult();
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error getting com.connexience.server.social.tag:" + tagId, e);
    }
    finally
    {
      closeSession(session);
    }
  }

  /*
  * Remove a com.connexience.server.social.tag from a server object
  * */
  public void removeTag(Ticket ticket, ServerObject so, Tag tag) throws ConnexienceException
  {
    Session session = null;
    try
    {
      session = getSession();
      User user = (User) getObject(ticket.getUserId(), User.class);

      //remove all tags on that server object with that name
      if (user != null && EJBLocator.lookupAccessControlBean().canTicketAccessResource(ticket, so, Permission.WRITE_PERMISSION))
      {
          EJBLocator.lookupObjectRemovalBean().remove(so.getId(), tag);
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
      throw new ConnexienceException("Error removing tags for: " + so.getId(), e);
    }
    finally
    {
      closeSession(session);
    }
  }


}
