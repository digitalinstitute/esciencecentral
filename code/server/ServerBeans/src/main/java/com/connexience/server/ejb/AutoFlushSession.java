/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb;

import org.hibernate.*;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.stat.SessionStatistics;

import java.io.Serializable;
import java.sql.Connection;

/*
 * This wrapper of hibernate's session object ensures that flush is invoked on close() if and only if any changes have been made.
 * 
 * I've assumed that all the methods make changes apart from those methods that are currently used by Inkspot that I know to be read-only.
 * I did this as it didn't seem feasible to understand exactly what all the methods did, given the high number of these methods and the fact
 * that the majority are never used.
 * 
 * The wrapper is safe, in that it is coded to invoke the flush for those methods that I have not analysed.
 */
public class AutoFlushSession implements Session
{
    private Session session;

    private boolean readOnly = true;


    public AutoFlushSession(Session session)
    {
        this.session = session;
    }

    private void madeChange()
    {
        readOnly = false;
    }

    private void flushed()
    {
        readOnly = true;
    }
    
    public Transaction beginTransaction() throws HibernateException
    {
        return session.beginTransaction();
    }

    public LockRequest buildLockRequest(LockOptions arg0)
    {
        madeChange();
        return session.buildLockRequest(arg0);
    }

    public void cancelQuery() throws HibernateException
    {
        madeChange();
        session.cancelQuery();
    }

    public void clear()
    {
        madeChange();
        session.clear();
    }

    public Connection close() throws HibernateException
    {        
        Connection c;
        try
        {
            if (!readOnly)
            {
                session.flush();
            }
        }
        finally
        {
            c = session.close();
        }
        return c;
    }

    public boolean contains(Object arg0)
    {
        madeChange();
        return session.contains(arg0);
    }

    public Criteria createCriteria(Class arg0, String arg1)
    {
        madeChange();
        return session.createCriteria(arg0, arg1);
    }

    public Criteria createCriteria(Class arg0)
    {
        madeChange();
        return session.createCriteria(arg0);
    }

    public Criteria createCriteria(String arg0, String arg1)
    {
        madeChange();
        return session.createCriteria(arg0, arg1);
    }

    public Criteria createCriteria(String arg0)
    {
        madeChange();
        return session.createCriteria(arg0);
    }

    public Query createFilter(Object arg0, String arg1) throws HibernateException
    {
        madeChange();
        return session.createFilter(arg0, arg1);
    }

    public Query createQuery(String arg0) throws HibernateException
    {
        return session.createQuery(arg0);
    }

    public SQLQuery createSQLQuery(String arg0) throws HibernateException
    {
        return session.createSQLQuery(arg0);
    }

    public void delete(Object arg0) throws HibernateException
    {
        madeChange();
        session.delete(arg0);
    }

    public void delete(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        session.delete(arg0, arg1);
    }

    public void disableFetchProfile(String arg0) throws UnknownProfileException
    {
        madeChange();
        session.disableFetchProfile(arg0);
    }

    public void disableFilter(String arg0)
    {
        madeChange();
        session.disableFilter(arg0);
    }

    public Connection disconnect() throws HibernateException
    {
        madeChange();
        return session.disconnect();
    }

    public void doWork(Work arg0) throws HibernateException
    {
        madeChange();
        session.doWork(arg0);
    }

    public void enableFetchProfile(String arg0) throws UnknownProfileException
    {
        madeChange();
        session.enableFetchProfile(arg0);
    }

    public Filter enableFilter(String arg0)
    {
        madeChange();
        return session.enableFilter(arg0);
    }

    public void evict(Object arg0) throws HibernateException
    {
        madeChange();
        session.evict(arg0);
    }

    public void flush() throws HibernateException
    {
        session.flush();
        flushed();
    }

    @Deprecated
    public Object get(Class arg0, Serializable arg1, LockMode arg2) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1, arg2);
    }

    public Object get(Class arg0, Serializable arg1, LockOptions arg2) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1, arg2);
    }

    public Object get(Class arg0, Serializable arg1) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1);
    }

    @Deprecated
    public Object get(String arg0, Serializable arg1, LockMode arg2) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1, arg2);
    }

    public Object get(String arg0, Serializable arg1, LockOptions arg2) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1, arg2);
    }

    public Object get(String arg0, Serializable arg1) throws HibernateException
    {
        madeChange();
        return session.get(arg0, arg1);
    }

    public CacheMode getCacheMode()
    {
        madeChange();
        return session.getCacheMode();
    }

    public LockMode getCurrentLockMode(Object arg0) throws HibernateException
    {
        madeChange();
        return session.getCurrentLockMode(arg0);
    }

    public Filter getEnabledFilter(String arg0)
    {
        madeChange();
        return session.getEnabledFilter(arg0);
    }

    public String getEntityName(Object arg0) throws HibernateException
    {
        madeChange();
        return session.getEntityName(arg0);
    }

    public FlushMode getFlushMode()
    {
        madeChange();
        return session.getFlushMode();
    }

    public Serializable getIdentifier(Object arg0) throws HibernateException
    {
        madeChange();
        return session.getIdentifier(arg0);
    }

    public LobHelper getLobHelper()
    {
        madeChange();
        return session.getLobHelper();
    }

    public Query getNamedQuery(String arg0) throws HibernateException
    {
        madeChange();
        return session.getNamedQuery(arg0);
    }

    public SessionFactory getSessionFactory()
    {
        madeChange();
        return session.getSessionFactory();
    }

    public SessionStatistics getStatistics()
    {
        madeChange();
        return session.getStatistics();
    }

    public Transaction getTransaction()
    {
        madeChange();
        return session.getTransaction();
    }

    public TypeHelper getTypeHelper()
    {
        madeChange();
        return session.getTypeHelper();
    }

    public boolean isConnected()
    {
        madeChange();
        return session.isConnected();
    }

    public boolean isDefaultReadOnly()
    {
        madeChange();
        return session.isDefaultReadOnly();
    }

    public boolean isDirty() throws HibernateException
    {
        return session.isDirty();
    }

    public boolean isFetchProfileEnabled(String arg0) throws UnknownProfileException
    {
        madeChange();
        return session.isFetchProfileEnabled(arg0);
    }

    public boolean isOpen()
    {
        return session.isOpen();
    }

    public boolean isReadOnly(Object arg0)
    {
        madeChange();
        return session.isReadOnly(arg0);
    }

    @Deprecated
    public Object load(Class arg0, Serializable arg1, LockMode arg2) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1, arg2);
    }

    @Deprecated
    public Object load(Class arg0, Serializable arg1, LockOptions arg2) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1, arg2);
    }

    public Object load(Class arg0, Serializable arg1) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1);
    }

    public void load(Object arg0, Serializable arg1) throws HibernateException
    {
        madeChange();
        session.load(arg0, arg1);
    }

    @Deprecated
    public Object load(String arg0, Serializable arg1, LockMode arg2) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1, arg2);
    }

    public Object load(String arg0, Serializable arg1, LockOptions arg2) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1, arg2);
    }

    public Object load(String arg0, Serializable arg1) throws HibernateException
    {
        madeChange();
        return session.load(arg0, arg1);
    }

    @Deprecated
    public void lock(Object arg0, LockMode arg1) throws HibernateException
    {
        madeChange();
        session.lock(arg0, arg1);
    }

    @Deprecated
    public void lock(String arg0, Object arg1, LockMode arg2) throws HibernateException
    {
        madeChange();
        session.lock(arg0, arg1, arg2);
    }

    public Object merge(Object arg0) throws HibernateException
    {
        madeChange();
        return session.merge(arg0);
    }

    public Object merge(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        return session.merge(arg0, arg1);
    }

    public void persist(Object arg0) throws HibernateException
    {
        madeChange();
        session.persist(arg0);
    }

    public void persist(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        session.persist(arg0, arg1);
    }

    public void reconnect(Connection arg0) throws HibernateException
    {
        madeChange();
        session.reconnect(arg0);
    }

    @Deprecated
    public void refresh(Object arg0, LockMode arg1) throws HibernateException
    {
        madeChange();
        session.refresh(arg0, arg1);
    }

    public void refresh(Object arg0, LockOptions arg1) throws HibernateException
    {
        madeChange();
        session.refresh(arg0, arg1);
    }

    public void refresh(Object arg0) throws HibernateException
    {
        madeChange();
        session.refresh(arg0);
    }

    public void replicate(Object arg0, ReplicationMode arg1) throws HibernateException
    {
        madeChange();
        session.replicate(arg0, arg1);
    }

    public void replicate(String arg0, Object arg1, ReplicationMode arg2) throws HibernateException
    {
        madeChange();
        session.replicate(arg0, arg1, arg2);
    }

    public Serializable save(Object arg0) throws HibernateException
    {
        madeChange();
        return session.save(arg0);
    }

    public Serializable save(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        return session.save(arg0, arg1);
    }

    public void saveOrUpdate(Object arg0) throws HibernateException
    {
        madeChange();
        session.saveOrUpdate(arg0);
    }

    public void saveOrUpdate(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        session.saveOrUpdate(arg0, arg1);
    }

    public void setCacheMode(CacheMode arg0)
    {
        madeChange();
        session.setCacheMode(arg0);
    }

    public void setDefaultReadOnly(boolean arg0)
    {
        madeChange();
        session.setDefaultReadOnly(arg0);
    }

    public void setFlushMode(FlushMode arg0)
    {
        madeChange();
        session.setFlushMode(arg0);
    }

    public void setReadOnly(Object arg0, boolean arg1)
    {
        madeChange();
        session.setReadOnly(arg0, arg1);
    }

    public void update(Object arg0) throws HibernateException
    {
        madeChange();
        session.update(arg0);
    }

    public void update(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        session.update(arg0, arg1);
    }

    @Override
    public String getTenantIdentifier()
    {
        return session.getTenantIdentifier();
    }

    @Override
    public <T> T doReturningWork(ReturningWork<T> arg0) throws HibernateException
    {

        madeChange();
        return session.doReturningWork(arg0);
    }

    @Override
    public void refresh(String arg0, Object arg1) throws HibernateException
    {
        madeChange();
        session.refresh(arg0, arg1);
    }

    @Override
    public void refresh(String arg0, Object arg1, LockOptions arg2) throws HibernateException
    {
        madeChange();
        session.refresh(arg0, arg1, arg2);
    }

    @Override
    public SharedSessionBuilder sessionWithOptions()
    {
        return session.sessionWithOptions();
    }
}
