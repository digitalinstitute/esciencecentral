/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.api.model.EscEvent;
import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.events.AbstractEventStore;
import com.connexience.server.events.EscEventCursor;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.model.security.Ticket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jboss.logging.Logger;

/**
 * This bean provides access to the event store 
 * @author hugo
 */
@Stateless
@EJB(name= "java:global/ejb/EventStoreBean", beanInterface = EventStoreRemote.class)
public class EventStoreBean implements EventStoreRemote {
    private static final Logger logger = Logger.getLogger(EventStoreBean.class);
    
    @EJB
    private EventManagerHolder eventManagerHolder;

    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;
    
    @Override
    public void pushEvent(Ticket ticket, String deviceId, String studyCode, EscEvent event) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){
            try {
                AbstractEventStore store = eventManagerHolder.getManager().getFlatStudyStore(studyCode);
                store.pushEvent(deviceId, event);
                if(store.getExportJMSTopicName()!=null && ! store.getExportJMSTopicName().isEmpty()){
                    // JMS Export
                    pushEventsToTopic(ticket, studyCode, deviceId, Arrays.asList(new EscEvent[]{event}));
                }
            } catch (Exception e){
                throw new ConnexienceException("Error pushing event: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException("Cannot obtain or create event store for study: " + studyCode);
        }
    }

    @Override
    public void pushEvents(Ticket ticket, String deviceId, String studyCode, List<EscEvent> events) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){    
            try {
                AbstractEventStore store = eventManagerHolder.getManager().getFlatStudyStore(studyCode);
                store.pushEvents(deviceId, events);
                if(store.getExportJMSTopicName()!=null && ! store.getExportJMSTopicName().isEmpty()){
                    // JMS Export
                    pushEventsToTopic(ticket, studyCode, deviceId, events);
                }                        
            } catch (Exception e){
                throw new ConnexienceException("Error pushing event: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException("Cannot obtain or create event store for study: " + studyCode);
        }
    }

    @Override
    public List<EscEvent> query(Ticket ticket, String deviceId, String studyCode, Date startDate, Date endDate, int maxEvents) throws ConnexienceException {
        List<EscEvent> results= new ArrayList<>();
        EscEventCursor cursor = null;
        try {
            int count = 0;
            EscEvent event;
            
            cursor = getEventStore(ticket, studyCode).getCursor(deviceId, startDate, endDate);
            while(cursor.hasNext() && count<=maxEvents){
                event = cursor.next();
                if(event!=null){
                    results.add(event);
                }
                count++;
            }
            return results;
            
        } catch (Exception e){
            throw new ConnexienceException("Error querying event store: " + e.getMessage(), e);
        } finally {
            if(cursor!=null){
                cursor.close();
            }
        }
    }

    @Override
    public List<EscEvent> query(Ticket ticket, String deviceId, String studyCode, String eventType, Date startDate, Date endDate, int maxEvents) throws ConnexienceException {
        List<EscEvent> results= new ArrayList<>();
        EscEventCursor cursor = null;
        try {
            int count = 0;
            EscEvent event;
            
            cursor = getEventStore(ticket, studyCode).getCursor(deviceId, eventType, startDate, endDate);
            while(cursor.hasNext() && count<=maxEvents){
                event = cursor.next();
                if(event!=null){
                    results.add(event);
                }
                count++;
            }
            return results;
            
        } catch (Exception e){
            throw new ConnexienceException("Error querying event store: " + e.getMessage(), e);
        } finally {
            if(cursor!=null){
                cursor.close();
            }
        }
    }

    @Override
    public List<String> listEventTypes(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException {
        AbstractEventStore store = getEventStore(ticket, studyCode);
        try {
            return store.listEventTypes(deviceId);
        } catch (Exception e){
            throw new ConnexienceException("Error getting event types: " + e.getMessage(), e);
        }
    }

    @Override
    public List<String> listEventTypes(Ticket ticket, String studyCode) throws ConnexienceException {
        AbstractEventStore store = getEventStore(ticket, studyCode);
        try {
            return store.listEventTypes();
        } catch (Exception e){
            throw new ConnexienceException("Error getting event types: " + e.getMessage(), e);
        }
    }
    
    @Override
    public EscEvent getMostRecent(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException {
        AbstractEventStore store = getEventStore(ticket, studyCode);
        try {
            return store.getMostRecent(deviceId);
        } catch (Exception e){
            throw new ConnexienceException("Error getting most recent event: " + e.getMessage(), e);
        }
    }

    @Override
    public EscEvent getMostRecent(Ticket ticket, String deviceId, String studyCode, String eventType) throws ConnexienceException {
        AbstractEventStore store = getEventStore(ticket, studyCode);
        try {
            return store.getMostRecent(deviceId, eventType);
        } catch (Exception e){
            throw new ConnexienceException("Error getting most recent event: " + e.getMessage(), e);
        }
    }

    private AbstractEventStore getEventStore(Ticket ticket, String studyCode) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){
            try {
                ArrayList<EscEvent> results = new ArrayList<>();
                return eventManagerHolder.getManager().getFlatStudyStore(studyCode);
            } catch (Exception e){
                throw new ConnexienceException("Error pushing event: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException("Cannot obtain or create event store for study: " + studyCode);
        }               
    }

    @Override
    public void dropEvents(Ticket ticket, String deviceId, String studyCode) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){
            try {
                eventManagerHolder.getManager().getFlatStudyStore(studyCode).dropEvents(deviceId);
            } catch (Exception e){
                throw new ConnexienceException("Error dropping events for device: " + e.getMessage(), e);
            }
        }
    }

    @Override
    public void dropEvents(Ticket ticket, String deviceId, String studyCode, String eventType) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){
            try {
                eventManagerHolder.getManager().getFlatStudyStore(studyCode).dropEvents(deviceId, eventType);
            } catch (Exception e){
                throw new ConnexienceException("Error dropping events for device: " + e.getMessage(), e);
            }
        }
    }

    @Override
    public void resetAllEventStores(Ticket ticket) throws ConnexienceException {
        eventManagerHolder.getManager().resetAll();
    }

    @Override
    public void resetStudyEventStore(Ticket ticket, String studyCode) throws ConnexienceException {
        eventManagerHolder.getManager().reset(studyCode);
    }

    @Override
    public boolean eventStorePresent(Ticket ticket, String studyCode) throws ConnexienceException {
        return eventManagerHolder.getManager().containsFlatStudyStore(studyCode);
    }
    
    public void createEventStore(Ticket ticket, String studyCode) throws ConnexienceException {
        eventManagerHolder.checkEventStore(ticket, studyCode);
    }
    
    private void pushEventsToTopic(Ticket ticket, String studyCode, String deviceId, List<EscEvent>events) throws ConnexienceException {
        if(eventManagerHolder.checkEventStore(ticket, studyCode)){
            AbstractEventStore store = eventManagerHolder.getManager().getFlatStudyStore(studyCode);
            if(store.getExportJMSTopicName()!=null && !store.getExportJMSTopicName().isEmpty()){
                MessageProducer producer;
                Session session;
                
                if(store.getJmsProducer()!=null){
                    // Send to the producer
                    producer = store.getJmsProducer();
                    session = store.getJmsSession();
                    
                } else {
                    // Need to connect and then send
                    try {
                        String activeMQServer = EJBLocator.lookupPreferencesBean().stringValue("ActiveMQ", "Server", "localhost");
                        int activeMQPort = EJBLocator.lookupPreferencesBean().intValue("ActiveMQ", "Port", 61616);
                        String activeMQUsername = EJBLocator.lookupPreferencesBean().stringValue("ActiveMQ", "Username", "");
                        String activeMQPassword = EJBLocator.lookupPreferencesBean().stringValue("ActiveMQ", "Password", "");
                        String url = "tcp://" + activeMQServer + ":" + activeMQPort;

                        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(activeMQUsername, activeMQPassword, url);
                        Connection connection = factory.createConnection();
                        connection.start();
                        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                        String topicName = store.getExportJMSTopicName().trim();
                        Topic topic = session.createTopic(topicName);               
                        producer = session.createProducer(topic);
                        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
                        store.setJmsConnection(connection, producer, session);
                        
                    } catch (Exception e){
                        producer = null;
                        session = null;
                    }
                }
                
                // Send the message
                if(producer!=null && session!=null){
                    for(EscEvent e : events){
                        try {
                            e.getMetadata().put("DeviceID", deviceId);
                            e.getMetadata().put("StudyID", studyCode);
                            TextMessage message = session.createTextMessage(e.toJsonString());
                            producer.send(message);                    
                        } catch (Exception ex){
                            logger.error("Error sending Event to Topic: " + ex.getMessage());
                        }
                    }
                }
                
            }
        } else {
            throw new ConnexienceException("Cannot locate store for study: " + studyCode);
        }
    }
}
