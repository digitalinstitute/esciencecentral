/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.TreeHashGenerator;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.storage.DocumentVersionManager;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.archive.glacier.ArchiveMap;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.storage.DataStore;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@MessageDriven(mappedName="queue/AWSGlacierArchivingQueue", activationConfig =
{
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/AWSGlacierArchivingQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class GlacierArchivingWorker extends HibernateSessionContainer implements MessageListener
{
    private static final Logger logger = Logger.getLogger(GlacierArchivingWorker.class.getName());

    @Resource
    private MessageDrivenContext messageDrivenContext;

    /**
     * Creates a new instance of GlacierArchivingWorker
     */
    public GlacierArchivingWorker()
    {
    }

    public void onMessage(Message message)
    {
        logger.info("GlacierArchivingWorker: onMessage");
        try
        {
            int opcode = message.getIntProperty(MessageUtils.OPCODE_PROPERTYNAME);

            if (opcode == MessageUtils.ARCHIVE_REQUEST_OPCODEVALUE)
            {
                String accessKey      = message.getStringProperty(MessageUtils.ACCESSKEY_PROPERTYNAME);
                String secretKey      = message.getStringProperty(MessageUtils.SECRETKEY_PROPERTYNAME);
                String domainName     = message.getStringProperty(MessageUtils.DOMAINNAME_PROPERTYNAME);
                String vaultName      = message.getStringProperty(MessageUtils.VAULTNAME_PROPERTYNAME);
                String documentId     = message.getStringProperty(MessageUtils.DOCUMENTID_PROPERTYNAME);
                String dataStoreId    = message.getStringProperty(MessageUtils.DATASTOREID_PROPERTYNAME);
                String archiveStoreId = message.getStringProperty(MessageUtils.ARCHIVESTOREID_PROPERTYNAME);

                logger.debug("Message received: ARCHIVE_REQUEST_OPCODEVALUE");
                logger.debug("    accessKey   = [" + ClientUtils.secretPrefix(accessKey) + "]");
                logger.debug("    secretKey   = [" + ClientUtils.secretPrefix(secretKey) + "]");
                logger.debug("    domainName  = [" + domainName + "]");
                logger.debug("    vaultName   = [" + vaultName + "]");
                logger.debug("    documentId  = [" + documentId + "]");
                logger.debug("    dataStoreId = [" + dataStoreId + "]");

                DocumentRecord documentRecord       = null;
                ServerObject   documentRecordObject = getObject(documentId, DocumentRecord.class);
                if (documentRecordObject instanceof DocumentRecord)
                    documentRecord = (DocumentRecord) documentRecordObject;
                else
                    logger.warn("Unable to get document record (" + documentId + ")");

                DataStore    dataStore       = null;
                ServerObject dataStoreObject = getObject(dataStoreId, DataStore.class);
                if (dataStoreObject instanceof DataStore)
                    dataStore = (DataStore) dataStoreObject;
                else
                    logger.warn("Unable to get data store (" + dataStoreId + ")");

                if ((documentRecord != null) && (dataStore != null))
                {
                    try
                    {
                        DocumentVersionManager documentVersionManager = new DocumentVersionManager(documentRecord);

                        List<DocumentVersion> documentVersions = new LinkedList<>();
                        Session sessionList = null; // TODO: Improve session management
                        try
                        {
                            sessionList = getSession();

                            documentVersions = documentVersionManager.getVersions(sessionList);
                        }
                        finally
                        {
                            closeSession(sessionList);
                        }

                        AmazonGlacierClient amazonGlacierClient = ClientUtils.obtainGlacierClient(accessKey, secretKey, domainName);
                        try
                        {
                            int partSize = 1024 * 1024;

                            Session sessionMaps = null;
                            List<ArchiveMap>             archiveMaps         = new LinkedList<>();
                            Map<String, DocumentVersion> documentVersionMaps = new HashMap<>();
                            try {
                                sessionMaps = getSession();
 
                                for (DocumentVersion documentVersion: documentVersions)
                                {
                                    logger.info("documentVersion: [" + documentVersion.getId() + "], version = " + documentVersion.getVersionNumber());

                                    ArchiveMap archiveMap = EJBLocator.lookupArchiveMapBean().getArchiveMapForDocumentRecordAndDocumentVersion(documentRecord.getId(), documentVersion.getId());
                                    if (archiveMap == null)
                                    {
                                        String uploadId = UploadUtils.initiateUpload(amazonGlacierClient, vaultName, generateDescription(documentRecord, documentVersion), partSize);
                                        archiveMaps.add(EJBLocator.lookupArchiveMapBean().createArchiveMapAsUploading(documentRecord.getId(), documentVersion.getId(), dataStoreId, archiveStoreId, uploadId));
                                        documentVersionMaps.put(uploadId, documentVersion);
                                    }
                                    else if (archiveMap.getStatus() != ArchiveMap.UPLOADED_STATUS)
                                    {
                                        String uploadId = UploadUtils.initiateUpload(amazonGlacierClient, vaultName, generateDescription(documentRecord, documentVersion), partSize);
                                        archiveMaps.add(EJBLocator.lookupArchiveMapBean().updateArchiveMapToReuploaded(archiveMap, uploadId));
                                        documentVersionMaps.put(uploadId, documentVersion);
                                    }
                                }
                            }
                            finally
                            {
                                closeSession(sessionMaps);
                            }

                            for (ArchiveMap archiveMap: archiveMaps)
                            {
                                try
                                {
                                    DocumentVersion documentVersion = documentVersionMaps.get(archiveMap.getUploadId());
                                    
                                    logger.info("archiveMap: record = [" + archiveMap.getDocumentRecordId() + "], version = " + archiveMap.getDocumentVersionId());
                                    long        inputStreamLength = dataStore.getRecordSize(documentRecord, documentVersion);
                                    InputStream inputStream       = dataStore.getInputStream(documentRecord, documentVersion);

                                    String       archiveId       = null;
                                    List<byte[]> binaryChecksums = UploadUtils.doUpload(amazonGlacierClient, archiveMap.getUploadId(), vaultName, inputStream, partSize, inputStreamLength);
                                    if (binaryChecksums != null)
                                    {
                                        String fullChecksum = TreeHashGenerator.calculateTreeHash(binaryChecksums);

                                        archiveId  = UploadUtils.completeUpload(amazonGlacierClient, archiveMap.getUploadId(), vaultName, fullChecksum, inputStreamLength);
                                        archiveMap = EJBLocator.lookupArchiveMapBean().updateArchiveMapToUploaded(archiveMap, archiveId, fullChecksum);
                                    }

                                    if (archiveMap.getStatus() == ArchiveMap.UPLOADED_STATUS)
                                    {
                                        String fourOhFourMessage = "This document has been archive to AWS Glacier, as archiveId = " + archiveId;
                                        ByteArrayInputStream messageInputStream = new ByteArrayInputStream(fourOhFourMessage.getBytes());
                                        dataStore.readFromStream(documentRecord, documentVersion, messageInputStream, documentVersion.getSize());
                                        messageInputStream.close();
                                    }
                                    else
                                        logger.error("Status not 'Uploaded' archiveId=" + archiveId);
                                }
                                catch (Throwable throwable)
                                {
                                    logger.error("Problem uploading document, record = [" + archiveMap.getDocumentRecordId() + "], version = " + archiveMap.getDocumentVersionId(), throwable);
                                }
                            }
                        }
                        finally
                        {
                            ClientUtils.shutdownGlacierClient(amazonGlacierClient);
                        }

                        documentRecord = EJBLocator.lookupArchiveMapBean().checkAndUpdateDocumentRecordIfUploadComplete(documentRecord);
                    }
                    catch (Throwable throwable)
                    {
                        logger.warn("Problem uploading document version: ", throwable);
                        documentRecord = EJBLocator.lookupArchiveMapBean().updateDocumentRecordToArchivingFailed(documentRecord);
                        throw throwable;
                    }
                }
                else if (documentRecord != null)
                    documentRecord = EJBLocator.lookupArchiveMapBean().updateDocumentRecordToArchivingFailed(documentRecord);
            }
            else
                logger.warn("Message with unexpected opcode: " + opcode);
        }
        catch (JMSException jmsException)
        {
            jmsException.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
    }

    private String generateDescription(DocumentRecord documentRecord, DocumentVersion documentVersion)
        throws JSONException
    {
        JSONObject descriptionObject = new JSONObject();
        
        descriptionObject.put("documentid", documentRecord.getId());
        descriptionObject.put("documentname", documentRecord.getName());
        descriptionObject.put("versionid", documentVersion.getId());
        descriptionObject.put("versionnumber", documentVersion.getVersionNumber());

        return descriptionObject.toString();
    }
}
