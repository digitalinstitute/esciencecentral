/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.storage.DocumentVersionManager;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.archive.glacier.ArchiveMap;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.storage.DataStore;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.io.InputStream;
import java.util.List;

@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
@MessageDriven(mappedName="queue/AWSGlacierUnarchivingQueue", activationConfig =
{
    @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/AWSGlacierUnarchivingQueue"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
    @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")
})
public class GlacierUnarchivingWorker extends HibernateSessionContainer implements MessageListener
{
    private static final Logger logger = Logger.getLogger(GlacierUnarchivingWorker.class.getName());

    @Resource
    private MessageDrivenContext messageDrivenContext;

    /**
     * Creates a new instance of GlacierUnarchivingWorker
     */
    public GlacierUnarchivingWorker()
    {
    }

    public void onMessage(Message message)
    {
        logger.info("GlacierUnarchivingWorker: onMessage");
        try
        {
            int opcode = message.getIntProperty(MessageUtils.OPCODE_PROPERTYNAME);

            if (opcode == MessageUtils.UNARCHIVE_REQUEST_OPCODEVALUE)
            {
                String accessKey   = message.getStringProperty(MessageUtils.ACCESSKEY_PROPERTYNAME);
                String secretKey   = message.getStringProperty(MessageUtils.SECRETKEY_PROPERTYNAME);
                String domainName  = message.getStringProperty(MessageUtils.DOMAINNAME_PROPERTYNAME);
                String vaultName   = message.getStringProperty(MessageUtils.VAULTNAME_PROPERTYNAME);
                String documentId  = message.getStringProperty(MessageUtils.DOCUMENTID_PROPERTYNAME);
                String dataStoreId = message.getStringProperty(MessageUtils.DATASTOREID_PROPERTYNAME);

                logger.debug("Message received: UNARCHIVE_REQUEST_OPCODEVALUE");
                logger.debug("    accessKey   = [" + ClientUtils.secretPrefix(accessKey) + "]");
                logger.debug("    secretKey   = [" + ClientUtils.secretPrefix(secretKey) + "]");
                logger.debug("    domainName  = [" + domainName + "]");
                logger.debug("    vaultName   = [" + vaultName + "]");
                logger.debug("    documentId  = [" + documentId + "]");
                logger.debug("    dataStoreId = [" + dataStoreId + "]");

                DocumentRecord documentRecord       = null;
                ServerObject   documentRecordObject = getObject(documentId, DocumentRecord.class);
                if (documentRecordObject instanceof DocumentRecord)
                    documentRecord = (DocumentRecord) documentRecordObject;
                else
                    logger.warn("Unable to get document record (" + documentId + ")");

                DataStore    dataStore       = null;
                ServerObject dataStoreObject = getObject(dataStoreId, DataStore.class);
                if (dataStoreObject instanceof DataStore)
                    dataStore = (DataStore) dataStoreObject;
                else
                    logger.warn("Unable to get data store (" + dataStoreId + ")");

                if ((documentRecord != null) && (dataStore != null))
                {
                    try
                    {
                        AmazonGlacierClient amazonGlacierClient = ClientUtils.obtainGlacierClient(accessKey, secretKey, domainName);
                        try
                        {
                            List<ArchiveMap> archiveMaps = EJBLocator.lookupArchiveMapBean().getArchivesForDocumentRecord(documentRecord.getId());

                            for (ArchiveMap archiveMap: archiveMaps)
                            {
                                logger.info("Request archive: " + archiveMap.getArchiveId());
                                if (archiveMap.getStatus() != ArchiveMap.DOWNLOADED_STATUS)
                                {
                                    String downloadId = DownloadUtils.initiateDownload(amazonGlacierClient, vaultName, archiveMap.getArchiveId());
                                    logger.info("Request archive, downloadId:" + downloadId);
                                    EJBLocator.lookupArchiveMapBean().updateArchiveMapToDownloading(archiveMap, downloadId);
                                }
                                else
                                    logger.info("Skipped Request of: " + archiveMap.getArchiveId());
                            }
                        }
                        finally
                        {
                            ClientUtils.shutdownGlacierClient(amazonGlacierClient);
                        }
                    }
                    catch (Throwable throwable)
                    {
                        logger.warn("Problem download document version: ", throwable);
                        EJBLocator.lookupArchiveMapBean().updateDocumentRecordToUnarchivingFailed(documentRecord);
                        throw throwable;
                    }
                }
                else
                    EJBLocator.lookupArchiveMapBean().updateDocumentRecordToUnarchivingFailed(documentRecord);
            }
            else if (opcode == MessageUtils.DOWNLOAD_REQUEST_OPCODEVALUE)
            {
                String accessKey     = message.getStringProperty(MessageUtils.ACCESSKEY_PROPERTYNAME);
                String secretKey     = message.getStringProperty(MessageUtils.SECRETKEY_PROPERTYNAME);
                String domainName    = message.getStringProperty(MessageUtils.DOMAINNAME_PROPERTYNAME);
                String vaultName     = message.getStringProperty(MessageUtils.VAULTNAME_PROPERTYNAME);
                String downloadId    = message.getStringProperty(MessageUtils.DOWNLOADID_PROPERTYNAME);
                String queueURL      = message.getStringProperty(MessageUtils.QUEUEURL_PROPERTYNAME);
                String receiptHandle = message.getStringProperty(MessageUtils.RECEIPTHANDLE_PROPERTYNAME);

                logger.debug("Message received: DOWNLOAD_REQUEST_OPCODEVALUE");
                logger.debug("    accessKey     = [" + ClientUtils.secretPrefix(accessKey) + "]");
                logger.debug("    secretKey     = [" + ClientUtils.secretPrefix(secretKey) + "]");
                logger.debug("    domainName    = [" + domainName + "]");
                logger.debug("    vaultName     = [" + vaultName + "]");
                logger.debug("    downloadId    = [" + downloadId + "]");
                logger.debug("    queueURL      = [" + queueURL + "]");
                logger.debug("    receiptHandle = [" + receiptHandle + "]");

                AmazonGlacierClient amazonGlacierClient = ClientUtils.obtainGlacierClient(accessKey, secretKey, domainName);
                try
                {
                    ArchiveMap archiveMap = EJBLocator.lookupArchiveMapBean().getArchiveMapForDownload(downloadId);

                    if (archiveMap != null)
                    {
                        logger.info("Archive Map: dataStoreId = " + archiveMap.getDataStoreId());
                        InputStream bodyInputStream = DownloadUtils.doDownload(amazonGlacierClient, downloadId, vaultName, archiveMap.getSHA256TreeHash());
                        if (bodyInputStream != null)
                        {
                            DataStore    dataStore       = null;
                            ServerObject dataStoreObject = getObject(archiveMap.getDataStoreId(), DataStore.class);
                            if (dataStoreObject instanceof DataStore)
                            {
                                dataStore = (DataStore) dataStoreObject;

                                ServerObject documentRecordObject = getObject(archiveMap.getDocumentRecordId(), DocumentRecord.class);
                                if (documentRecordObject instanceof DocumentRecord)
                                {
                                    DocumentRecord documentRecord = (DocumentRecord) documentRecordObject;

                                    DocumentVersion documentVersion;
                                    Session session = null;
                                    try
                                    {
                                        session = getSession();

                                        DocumentVersionManager documentVersionManager = new DocumentVersionManager(documentRecord);

                                        documentVersion = documentVersionManager.getVersion(archiveMap.getDocumentVersionId(), session);
                                    }
                                    finally
                                    {
                                        session.close();
                                    }

                                    if (documentVersion != null)
                                    {
                                        dataStore.readFromStream(documentRecord, documentVersion, bodyInputStream, documentVersion.getSize());

                                        archiveMap = EJBLocator.lookupArchiveMapBean().updateArchiveMapToDownloaded(archiveMap);

                                        documentRecord = EJBLocator.lookupArchiveMapBean().checkAndUpdateDocumentRecordIfDownloadComplete(documentRecord);

                                        AmazonSQSClient amazonSQSClient = ClientUtils.obtainSQSClient(accessKey, secretKey, domainName);
                                        try
                                        {
                                            JobUtils.deleteJobCompletion(amazonSQSClient, queueURL, receiptHandle);
                                        }
                                        finally
                                        {
                                            ClientUtils.shutdownSQSClient(amazonSQSClient);
                                        }
                                    }
                                    else
                                        logger.warn("Unable to get document version (" + archiveMap.getDocumentVersionId() + ")");
                                }
                                else
                                    logger.warn("Unable to get document record (" + archiveMap.getDocumentRecordId() + ")");
                            }
                            else
                                logger.warn("Unable to get data store (" + archiveMap.getDataStoreId() + ")");

                            bodyInputStream.close();
                        }
                        else
                            logger.warn("No body input stream");
                    }
                    else
                        logger.warn("Unable obtain ArchiveMap, for downloadId=[" + downloadId + "]");
                }
                finally
                {
                    ClientUtils.shutdownGlacierClient(amazonGlacierClient);
                }
            }
            else
                logger.warn("Message with unexpected opcode: " + opcode);
        }
        catch (JMSException jmsException)
        {
            jmsException.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            messageDrivenContext.setRollbackOnly();
        }
    }
}
