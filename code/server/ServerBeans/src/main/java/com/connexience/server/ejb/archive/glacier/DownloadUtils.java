/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.model.*;
import org.apache.log4j.Logger;

import java.io.InputStream;

public class DownloadUtils
{
    private static final Logger logger = Logger.getLogger(DownloadUtils.class.getName());

    public static String initiateDownload(AmazonGlacierClient amazonGlacierClient, String vaultName, String archiveId)
    {
        String jobId = null;
        try
        {
            JobParameters jobParameters = new JobParameters();
            jobParameters.withArchiveId(archiveId);
            jobParameters.withDescription("Archive retrieval: " + archiveId);
            jobParameters.withType("archive-retrieval");

            InitiateJobRequest initiateJobRequest = new InitiateJobRequest();
            initiateJobRequest.withVaultName(vaultName);
            initiateJobRequest.withJobParameters(jobParameters);

            InitiateJobResult initiateJobResult = amazonGlacierClient.initiateJob(initiateJobRequest);
            if (initiateJobResult != null)
                jobId = initiateJobResult.getJobId();
            else
                logger.warn("Unable of obtain upload id");
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ", throwable);
        }

        return jobId;
    }

    public static InputStream doDownload(AmazonGlacierClient amazonGlacierClient, String downloadId, String vaultName, String sha256TreeHash)
    {
        try
        {
            GetJobOutputRequest getJobOutputRequest = new GetJobOutputRequest();
            getJobOutputRequest.withVaultName(vaultName);
            getJobOutputRequest.withJobId(downloadId);
            
            GetJobOutputResult getJobOutputResult = amazonGlacierClient.getJobOutput(getJobOutputRequest);
            if (getJobOutputResult != null)
            {
                if (sha256TreeHash.equals(getJobOutputResult.getChecksum()))
                    return getJobOutputResult.getBody();
                else
                    logger.error("Checksum mismatch!");
            }
            else
                logger.warn("Unable of obtain download request output");
        }
        catch (AmazonServiceException amazonServiceException)
        {
            if ("ResourceNotFoundException".equals(amazonServiceException.getErrorCode()))
                logger.info("Download job not available");
            else
                logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ",throwable);
        }
        
        return null;
    }

    public static boolean anyPossibleDownloadJobsExist(AmazonGlacierClient amazonGlacierClient, String vaultName)
    {
        boolean result = true;
        try
        {
            ListJobsRequest listJobsRequest = new ListJobsRequest();
            listJobsRequest.withVaultName(vaultName);
            ListJobsResult listJobsResult = amazonGlacierClient.listJobs(listJobsRequest);

            result = (listJobsResult != null) && (listJobsResult.getJobList().size() > 0);
        }
        catch (AmazonServiceException amazonServiceException)
        {
            logger.warn("AmazonServiceException: ", amazonServiceException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.warn("IllegalArgumentException: ", illegalArgumentException);
        }
        catch (AmazonClientException amazonClientException)
        {
            logger.warn("AmazonClientException: ", amazonClientException);
        }
        catch (Throwable throwable)
        {
            logger.warn("Throwable: ",throwable);
        }

        return result;
    }
}
