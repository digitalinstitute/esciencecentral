/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.provenance;

import com.connexience.server.model.provenance.events.GraphOperation;
import org.jboss.logging.Logger;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * User: nsjw7
 * Date: 01/08/2012
 * Store the provenance from a queue into the Relational DB and Neo4j
 */
@MessageDriven(name = "ProvenanceMDB",
        activationConfig =
                {
                        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
                        @ActivationConfigProperty(propertyName = "destination", propertyValue = "topic/provenanceevents"),
                        @ActivationConfigProperty(propertyName = "maxSession", propertyValue = "1")
                }, mappedName = "topic/provenanceevents")
public class ProvenanceMDB implements MessageListener {
    private static final Logger logger = Logger.getLogger(ProvenanceMDB.class.getName());

    @PersistenceContext(unitName = "provenance")
    private EntityManager em;

    /**
     * Load all of the properties from the server.xml file
     */
    private void loadProperties(String rootPath) {
        createDefaultProperties();
        File propertiesFile = new File(
                (rootPath != null ? rootPath : System.getProperty("user.home") + File.separator + ".inkspot")
                        + File.separator + "server.xml");

        if (!PreferenceManager.loadPropertiesFromFile(propertiesFile)) {
            createDefaultProperties();
            PreferenceManager.saveProperties();
        } else {
            PreferenceManager.saveProperties();
        }
    }

    /* Default Properties for the monitor server*/
    public void createDefaultProperties() {
        PreferenceManager.getSystemPropertyGroup("Provenance").add("DBEnabled", true);
    }

    public ProvenanceMDB() {
            loadProperties(null);
    }

    @Override
    public void onMessage(Message message) {

        BytesMessage bm = (BytesMessage) message;
        try {
            //Get the JMS Message
            bm.reset();
            byte[] messageData = new byte[(int) bm.getBodyLength()];
            bm.readBytes(messageData);

            //Deserialise and interpret the object
            ByteArrayInputStream buffer = new ByteArrayInputStream(messageData);
            ObjectInputStream stream = new ObjectInputStream(buffer);
            Object payload = stream.readObject();
            stream.close();

            if (payload instanceof GraphOperation) {

                //DO NOT STORE THE REPLAYS IN THE POSTGRES DATABASE
                if (!((GraphOperation) payload).isReplay()) {

                    boolean saveInDB = PreferenceManager.getSystemPropertyGroup("Provenance").booleanValue("DBEnabled", true);
                    if (saveInDB) {
                        //save the operation in the postgres database
                        em.persist(payload);
                        em.flush();
                    }
                } else {
                    logger.info("Received replay message with id: " + ((GraphOperation) payload).getId());
                }
            }
        } catch (JMSException jmse) {
            logger.error("JMS Exception: " + jmse);
        } catch (IOException ioe) {
            logger.error("IO Exception: " + ioe);
        } catch (ClassNotFoundException cnfe) {
            logger.error("Class not found: " + cnfe);
        }
    }
}
