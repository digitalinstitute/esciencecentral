package com.connexience.server.ejb.provenance;

import com.connexience.server.model.provenance.events.*;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@Stateless
@EJB(name = "java:global/ejb/JSONReportBean", beanInterface = JSONReportRemote.class)
@SuppressWarnings("unchecked")
public class JSONReportBean implements JSONReportRemote {

    @PersistenceContext(unitName = "provenance")
    private EntityManager em;

    public JSONReportBean() {
    }

    public String getDataProvenance(String documentId, String versionId, String hostname) {
        try {

            JSONObject results = new JSONObject();

            HashMap<String, String> workflowNames = new HashMap<>();

            List<JSONObject> jsonWorkflowWrites = new ArrayList<>();
            List<WorkflowDataWriteOperation> workflowWrites = em.createQuery("From WorkflowDataWriteOperation as w WHERE w.documentId = :documentId OR w.versionId = :versionId")
                    .setParameter("documentId", documentId)
                    .setParameter("versionId", versionId)
                    .getResultList();
            for (WorkflowDataWriteOperation write : workflowWrites) {
                JSONObject w = write.toDetailedJSON(getWorkflowName(workflowNames, write.getInvocationId()), hostname);
                jsonWorkflowWrites.add(w);
            }

            List<JSONObject> jsonWorkflowReads = new ArrayList<>();
            List<WorkflowDataReadOperation> reads = em.createQuery("From WorkflowDataReadOperation as r WHERE r.documentId = :documentId OR r.versionId = :versionId")
                    .setParameter("documentId", documentId)
                    .setParameter("versionId", versionId)
                    .getResultList();
            for (WorkflowDataReadOperation read : reads) {
                JSONObject r = read.toDetailedJSON(getWorkflowName(workflowNames, read.getInvocationId()), hostname);
                jsonWorkflowReads.add(r);
            }

            List<JSONObject> jsonUserWrites = new ArrayList<>();
            List<UserWriteOperation> userWrites = em.createQuery("From UserWriteOperation as w WHERE w.documentId = :documentId OR w.versionId = :versionId")
                    .setParameter("documentId", documentId)
                    .setParameter("versionId", versionId)
                    .getResultList();
            for (UserWriteOperation write : userWrites) {
                jsonUserWrites.add(write.toJSON());
            }

            List<JSONObject> jsonUserReads = new ArrayList<>();
            List<UserReadOperation> userReads = em.createQuery("From UserReadOperation as r WHERE r.documentId = :documentId OR r.versionId = :versionId")
                    .setParameter("documentId", documentId)
                    .setParameter("versionId", versionId)
                    .getResultList();
            for (UserReadOperation read : userReads) {

                jsonUserReads.add(read.toJSON());
            }

            results.put("userWrites", jsonUserWrites);
            results.put("userReads", jsonUserReads);
            results.put("workflowWrites", jsonWorkflowWrites);
            results.put("workflowReads", jsonWorkflowReads);

            return results.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getWorkflowProvenance(String invocationId, String hostname) {

        try {
            HashMap<String, String> workflowNames = new HashMap<>();
            //Get the execution details
            WorkflowExecuteOperation exec = (WorkflowExecuteOperation) em.createQuery("From WorkflowExecuteOperation as e WHERE e.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getSingleResult();

            JSONObject execDetails = exec.toDetailedJSON(hostname);


            //Add the name of the workflow to the map of names
            workflowNames.put(exec.getInvocationId(), exec.getName());

            List<JSONObject> jsonServiceExecs = new ArrayList<>();
            List<WorkflowDataServiceOperation> serviceExecs = em.createQuery("From WorkflowDataServiceOperation as e WHERE e.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getResultList();
            for (WorkflowDataServiceOperation serviceExec : serviceExecs) {
                jsonServiceExecs.add(serviceExec.toJSON());
            }


            List<JSONObject> jsonWorkflowWrites = new ArrayList<>();
            List<WorkflowDataWriteOperation> workflowWrites = em.createQuery("From WorkflowDataWriteOperation as w WHERE w.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getResultList();
            for (WorkflowDataWriteOperation write : workflowWrites) {
                JSONObject w = write.toDetailedJSON(getWorkflowName(workflowNames, write.getInvocationId()), hostname);
                jsonWorkflowWrites.add(w);
            }

            List<JSONObject> jsonWorkflowReads = new ArrayList<>();
            List<WorkflowDataReadOperation> reads = em.createQuery("From WorkflowDataReadOperation as r WHERE r.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getResultList();
            for (WorkflowDataReadOperation read : reads) {
                JSONObject r = read.toDetailedJSON(getWorkflowName(workflowNames, read.getInvocationId()), hostname);
                jsonWorkflowReads.add(r);
            }

            List<JSONObject> jsonTransfers = new ArrayList<>();
            List<WorkflowDataTransferOperation> transfers = em.createQuery("From WorkflowDataTransferOperation as o WHERE o.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getResultList();
            for (WorkflowDataTransferOperation transfer : transfers) {
                JSONObject t = transfer.toJSON();
                jsonTransfers.add(t);
            }

            //Build the JSON Object
            JSONObject results = new JSONObject();
            results.put("execDetails", execDetails);
            results.put("serviceExecutions", jsonServiceExecs);
            results.put("workflowReads", jsonWorkflowReads);
            results.put("workflowWrites", jsonWorkflowWrites);
            results.put("transfers", jsonTransfers);

            return results.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * This methd gets a workflow name from an invocation Id.  It will look it up if the id is not present
     * in the cache.  This can result in many DB calls for large provenance traces.  However, getting all
     * invocationIds every time hits the performance of both small and large traces.
     */
    private String getWorkflowName(HashMap<String, String> cache, String invocationId) {

        if (cache.containsKey(invocationId)) {
            return cache.get(invocationId);
        } else {

            List<WorkflowExecuteOperation> workflowExecutions = em.createQuery("From WorkflowExecuteOperation as e WHERE e.invocationId = :invocationId")
                    .setParameter("invocationId", invocationId)
                    .getResultList();
            for (WorkflowExecuteOperation exec : workflowExecutions) {
                if (exec.getName() != null && !exec.getName().equals("")) {
                    cache.put(invocationId, exec.getName());
                    return exec.getName();
                }
            }
        }
        return "UNKOWN PROCESS";
    }


}

