/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.notifications;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.notifcations.Notification;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.properties.PropertyItem;
import com.connexience.server.model.provenance.events.GraphOperation;
import com.connexience.server.model.provenance.events.LogonEvent;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.util.NameCache;
import org.json.JSONException;
import org.json.JSONObject;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jms.*;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA. User: martyn Date: 17-Nov-2009 Time: 10:37:24
 */

@Stateless
@EJB(name = "java:global/ejb/NotificationsBean", beanInterface = NotificationsRemote.class)
@SuppressWarnings("unchecked")
public class NotificationsBean extends HibernateSessionContainer implements NotificationsRemote {

    @Resource(mappedName = "java:/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @PersistenceContext(unitName = "provenance")
    private EntityManager provEm;

    /**
     * Get Activity from the provenance store for all projects of which this user is a member.  Will
     * include workflows which have been run, data read and written.
     */
    public List<GraphOperation> getAllProjectActivity(Ticket ticket, String userId, int start, int numResults) throws ConnexienceException {

        //Get a list of the project Ids for this user
        List<Project> projects = EJBLocator.lookupProjectsBean().getMemberProjects(ticket, userId, 0, 0, "NAME", "ASC");
        List<String> projectIds = new ArrayList<>();
        for (Project p : projects) {
            projectIds.add(String.valueOf(p.getId()));
        }

        //Get all of the events for this project
        return provEm.createQuery("From GraphOperation AS g WHERE " +
                "(g.class = 'WORKFLOWEXECUTE' OR g.class = 'USERDATAREAD' OR g.class = 'USERDATAWRITE' )" +
                "AND g.projectId IN :projectIds ORDER BY timestamp DESC")
                .setFirstResult(start)
                .setMaxResults(numResults)
                .setParameter("projectIds", projectIds)
                .getResultList();
    }

    /**
     * Get Activity from the provenance store for a single project.  Will
     * include workflows which have been run, data read and written.
     */
    public List<GraphOperation> getSingleProjectActivity(Ticket ticket, String userId, String projectId, int start, int numResults) throws ConnexienceException {

        //Get all of the events for this project
        return provEm.createQuery("From GraphOperation AS g WHERE " +
                "(g.class = 'WORKFLOWEXECUTE' OR g.class = 'USERDATAREAD' OR g.class = 'USERDATAWRITE' )" +
                "AND g.projectId = :projectId ORDER BY timestamp DESC")
                .setFirstResult(start)
                .setMaxResults(numResults)
                .setParameter("projectId", projectId)
                .getResultList();
    }


    /**
     * Get the activity of a user from the provenance store. Will return workflows which they've executed,
     * data that they have read/written.
     */
    public List<GraphOperation> getUserActivity(Ticket ticket, String id, int start, int numResults) throws ConnexienceException {

            //Get all of the events for this project
            return provEm.createQuery("From GraphOperation AS g WHERE " +
                    "(g.class = 'WORKFLOWEXECUTE' OR g.class = 'USERDATAREAD' OR g.class = 'USERDATAWRITE' )" +
                    "AND g.userId = :userId ORDER BY timestamp DESC")
                    .setFirstResult(start)
                    .setMaxResults(numResults)
                    .setParameter("userId", ticket.getUserId())
                    .getResultList();


    }

    @Override
    public List<WorkflowInvocationFolder> getWorkflowActivity(Ticket ticket, String userId, int start, int numResults) throws ConnexienceException {

            List<WorkflowInvocationFolder> invocations = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, userId, WorkflowInvocationFolder.class, start, numResults);

            //Reverse as comes in chronological order
            Collections.reverse(invocations);
            return invocations;

    }

    @Override
    public WorkflowInvocationFolder getInvocationDetails(Ticket ticket, String invocationId) throws ConnexienceException {
            return WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId);
    }

    @Override
    public long getLastLogonTime(Ticket ticket, String userId, String username) throws ConnexienceException{

        long lastLogon = 0;
        List<Timestamp> logonEvents = provEm.createQuery("SELECT MAX(l.timestamp) FROM LogonEvent AS l WHERE l.username= :username")
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList();

        if(!logonEvents.isEmpty()){
            lastLogon = logonEvents.get(0).getTime();
        }

        long lastActive = 0;
        List<Timestamp> lastActivity = provEm.createQuery("SELECT MAX(g.timestamp) FROM  GraphOperation AS g WHERE g.userId = :userId")
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();

        if(!lastActivity.isEmpty()){
            lastActive = lastActivity.get(0).getTime();
        }

        return Math.max(lastActive, lastLogon);
    }





    /* TODO: IS THE FOLLOWING USED? */

    public ArrayList<String> getNotificationKeys() {
        ArrayList<String> notificationKeys = new ArrayList<>();
        notificationKeys.add(EMAIL_ON_MESSAGE_RECIEVE);
        notificationKeys.add(EMAIL_ON_WORKFLOW_COMPLETION);
        notificationKeys.add(MESSAGE_ON_WORKFLOW_COMPLETION);
        notificationKeys.add(EMAIL_ON_BLOG_POST_COMMENT);
        notificationKeys.add(MESSAGE_ON_BLOG_POST_COMMENT);
        notificationKeys.add(EMAIL_ON_BLOG_POST_COMMENT_COMMENT);
        notificationKeys.add(MESSAGE_ON_BLOG_POST_COMMENT_COMMENT);

        return notificationKeys;
    }

    public void sendNotification(Notification notification) throws ConnexienceException {
        Connection connection = null;
        Session session;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword())
                    : connectionFactory.createConnection();
            session = connection.createSession(false, QueueSession.AUTO_ACKNOWLEDGE);
            Queue queue = session.createQueue("notificationsQueue");
            MessageProducer sender = session.createProducer(queue);
            sender.send(session.createObjectMessage(notification));
        } catch (Exception e) {
            throw new ConnexienceException("Could not add notification to Queue", e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean getNotificationValue(String notificationKey, String userId) throws ConnexienceException {
        try {
            Ticket ticket = getInternalTicket();
            PropertyItem notifcationProperty = EJBLocator.lookupPropertiesBean().getProperty(ticket, userId, "notifications", notificationKey);
            return Boolean.parseBoolean(notifcationProperty.getValue());
        } catch (NullPointerException e) {
            return false;
        }

    }

    public void setNotificationValue(String notificationKey, String userId, String value) throws ConnexienceException {
        Ticket ticket = getInternalTicket();
        EJBLocator.lookupPropertiesBean().setProperty(ticket, userId, "notifications", notificationKey, value);
    }
}
