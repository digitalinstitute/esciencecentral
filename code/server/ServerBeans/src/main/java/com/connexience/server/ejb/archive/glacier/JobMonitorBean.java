/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.archive.GlacierArchiveStore;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.LinkedList;
import java.util.List;

@Singleton
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@EJB(name = "java:global/ejb/JobMonitorBean", beanInterface = JobMonitorRemote.class)
public class JobMonitorBean extends HibernateSessionContainer implements JobMonitorRemote
{
    private static final Logger logger = Logger.getLogger(JobMonitorBean.class.getName());

    private List<JobMonitorWorker> jobMonitorWorkers = new LinkedList<>();

    public void startWorkers()
    {
        logger.info("Job Monitor: startWorker, start");

        try
        {
            List<GlacierArchiveStore> glacierArchiveStores = EJBLocator.lookupArchiveMapBean().getGlacierArchiveStores();

            for (GlacierArchiveStore glacierArchiveStore: glacierArchiveStores)
            {
                logger.info("GlacierArchiveStore: [" + glacierArchiveStore.getDomainName() + "]");
                JobMonitorWorker jobMonitorWorker = new JobMonitorWorker("AMS Glacier: Job Monitor", glacierArchiveStore);
                jobMonitorWorker.start();

                jobMonitorWorkers.add(jobMonitorWorker);
            }
        }
        catch (Throwable throwable) 
        {
            logger.error("Job Monitor: startWorker, problem ", throwable);
        }

        logger.info("Job Monitor: startWorker, stop");
    }

    public void shutdownWorkers()
    {
        logger.info("Job Monitor: shutdownWorker, start");

        try
        {
            for (JobMonitorWorker jobMonitorWorker: jobMonitorWorkers)
                jobMonitorWorker.shutdown();
            jobMonitorWorkers.clear();
        }
        catch (Throwable throwable) 
        {
            logger.error("Job Monitor: shutdownWorker, problem ", throwable);
        }

        logger.info("Job Monitor: shutdownWorker, end");
    }
}
