/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.directory;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.StoredCredentials;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.credentials.AmazonCredentials;
import com.connexience.server.model.security.credentials.AzureCredentials;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides methods to manage stored credentials
 *
 * @author hugo
 */
@SuppressWarnings("unchecked")
@Stateless
@EJB(name = "java:global/ejb/CredentialsDirectoryBean", beanInterface = CredentialsDirectoryRemote.class)
public class CredentialsDirectoryBean extends HibernateSessionContainer implements CredentialsDirectoryRemote {

    @Override
    public List listCredentials(Ticket ticket) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q;
            q = session.createQuery("FROM StoredCredentials");

            List<StoredCredentials> creds = q.list();
            for (StoredCredentials cred : creds) {
                if (cred instanceof AzureCredentials) {
                    AzureCredentials ac = (AzureCredentials) cred;
                    ac.setAccountKey("");
                    ac.setAccountName("");
                } else if (cred instanceof AmazonCredentials) {
                    AmazonCredentials ac = (AmazonCredentials) cred;
                    ac.setAccessKey("");
                    ac.setAccessKeyId("");
                }
            }

            return creds;

        } catch (Exception e) {
            throw new ConnexienceException("Error listing credentials: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public StoredCredentials saveCredentials(Ticket ticket, StoredCredentials credentials) throws ConnexienceException {
        return (StoredCredentials) savePlainObject(credentials);
    }

    @Override
    public StoredCredentials getCredentials(Ticket ticket, String id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();
            Query q;
            q = session.createQuery("FROM StoredCredentials WHERE id = :id");
            q.setString("id", id);
            return (StoredCredentials) q.list().get(0);

        } catch (Exception e) {
            throw new ConnexienceException("Error listing credentials: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    public void removeCredentials(Ticket ticket, String id) throws ConnexienceException {
        Session session = null;
        try {
            session = getSession();

            StoredCredentials c = getCredentials(ticket, id);
            if (c != null) {
                if (EJBLocator.lookupUserDirectoryBean().isUserOrganisationAdmin(ticket, ticket.getUserId(), c.getOrganisationId())) {
                    session.delete(c);
                } else if (c.getCreatorId().equals(ticket.getUserId())) {
                    session.delete(c);
                }
                else{
                    throw new ConnexienceException("Only admin or creator can delete credentials");
                }
            }
        } catch (Exception e) {
            throw new ConnexienceException("Error deleting credentials: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }
}
