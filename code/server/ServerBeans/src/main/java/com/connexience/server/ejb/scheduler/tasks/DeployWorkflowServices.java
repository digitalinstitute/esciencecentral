/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.HibernateUtil;
import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.workflow.WorkflowDeploymentUtils;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.WorkflowUtils;
import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/**
 * This task scans the service deployment directory for new files
 * @author hugo
 */
public class DeployWorkflowServices extends SchedulerTask {
    private static Logger logger = Logger.getLogger(DeployWorkflowServices.class);
    private File serviceDeployDirectory;
    private File libraryDeployDirectory;
    private HashMap<String, FileRecord> deployedServices = new HashMap<>();
    private HashMap<String, FileRecord> deployedLibraries = new HashMap<>();
    private Ticket adminTicket = null;
    private Ticket publicTicket = null;

    
    public DeployWorkflowServices(SchedulerBean parentBean){
        super(parentBean);
        startDelayed = true;
        startDelay = 10000;
        repeatInterval = 4000;
        repeating = true;
        name = "DeployServices";
        serviceDeployDirectory = new File(System.getProperty("jboss.home.dir") + File.separator + "esc" + File.separator + "autodeploy" + File.separator + "services");
        libraryDeployDirectory = new File(System.getProperty("jboss.home.dir") + File.separator + "esc" + File.separator + "autodeploy" + File.separator + "libraries");
    }
    
    @Override
    public void run() {
        if(parentBean.defaultOrganisationExists() && !HibernateUtil.isPostgres){
            logger.debug("Scanning service deployment directory: " + serviceDeployDirectory.getPath());
            if(!serviceDeployDirectory.exists()){
                logger.info("Creating service deployment directory: " + serviceDeployDirectory.getPath());
                serviceDeployDirectory.mkdirs();
            }

            logger.debug("Scanning library deployment directory: " + libraryDeployDirectory.getPath());
            if(!libraryDeployDirectory.exists()){
                logger.info("Creating library deployment directory: " + libraryDeployDirectory.getPath());
                libraryDeployDirectory.mkdirs();
            }
            
            if(adminTicket==null){
                try {
                    adminTicket = parentBean.getDefaultOrganisationAdminTicket();
                } catch (Exception e){
                    logger.error("Error creating admin ticket: " + e.getMessage());
                }
            }

            if(publicTicket==null && adminTicket!=null){
                try {
                    publicTicket = EJBLocator.lookupTicketBean().createPublicWebTicket();
                } catch (Exception e){
                    logger.error("Error getting public user id: " + e.getMessage());
                }
            }

            // Look at the services
            File[] contents = serviceDeployDirectory.listFiles();
            FileRecord record;
            for(File f : contents){
                if(!f.getName().startsWith(".")){
                    if(deployedServices.containsKey(f.getPath())){
                        record = deployedServices.get(f.getPath());

                        if(f.lastModified()>record.getFile().lastModified()){
                            // Need to reset the file
                            record.reset();
                        }

                    } else {
                        record = new FileRecord(f);
                        deployedServices.put(f.getPath(), record);
                    }


                    if(record.isStable()){
                        // File size has stabilised
                        if(!record.isDeployed()){
                            if(adminTicket!=null && publicTicket!=null){
                                try {
                                    record.setDeployed(true);
                                    logger.info("Checking deployment for service: " + record.getFile().getName());
                                    WorkflowDeploymentUtils.deployServiceFile(record.getFile(), adminTicket, publicTicket);
                                } catch (Exception e){
                                    logger.error("Error doing deployment: " + e.getMessage());
                                }
                            }
                        }

                    } else {
                        record.sizeCheck();
                    }
                }
            }
            
            // Look at the libraries
            contents = libraryDeployDirectory.listFiles();
            for(File f : contents){
                if(!f.getName().startsWith(".")){
                    if(deployedLibraries.containsKey(f.getPath())){
                        record = deployedLibraries.get(f.getPath());

                        if(f.lastModified()>record.getFile().lastModified()){
                            // Need to reset the file
                            record.reset();
                        }

                    } else {
                        record = new FileRecord(f);
                        deployedLibraries.put(f.getPath(), record);
                    }

                    if(record.isStable()){
                        // File size has stabilised
                        if(!record.isDeployed()){
                            if(adminTicket!=null && publicTicket!=null){
                                try {
                                    record.setDeployed(true);
                                    logger.info("Checking deployment for library: " + record.getFile().getName());
                                    WorkflowDeploymentUtils.deployLibraryFile(record.getFile(), adminTicket, publicTicket);
                                } catch (Exception e){
                                    logger.error("Error doing deployment: " + e.getMessage());
                                }
                            }
                        }

                    } else {
                        record.sizeCheck();
                    }
                }
            }
        }
    }
    
    private class FileRecord {
        private File file;
        private boolean stable = false;
        private long lastSize = 0;
        private boolean deployed = false;
        
        public FileRecord(File file) {
            this.file = file;
        }
        
        public void sizeCheck(){
            if(file.length()==lastSize){
                stable = true;
            } else {
                lastSize = file.length();
            }
        }

        public boolean isStable() {
            return stable;
        }

        public File getFile() {
            return file;
        }

        public void setDeployed(boolean deployed) {
            this.deployed = deployed;
        }

        public boolean isDeployed() {
            return deployed;
        }
        
        public void reset(){
            lastSize = 0;
            stable = false;
            deployed = false;
        }
        
        
    }
       
}
