/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.security.Ticket;
import java.util.Date;
import java.util.List;
import javax.xml.crypto.Data;

import org.apache.log4j.Logger;

/**
 * This class provides a scheduler task that polls remote filesystems for updated 
 * files.
 * @author hugo
 */
public class ScanRemoteFilesystems extends SchedulerTask {
    Logger logger = Logger.getLogger(ScanRemoteFilesystems.class);
    private Ticket adminTicket = null;
    
    public ScanRemoteFilesystems(SchedulerBean parentBean) {
        super(parentBean);
        setEnabled(true);
        setName("ScanRemoteFilesystems");
        setRepeatInterval(5000);
        setRepeating(true);
        setStartDelayed(true);
        setStartDelay(10000);
    }
    
    @Override
    public void run() {
        if(parentBean.defaultOrganisationExists()){
            logger.debug("Scanning remote filesystems");
            if(adminTicket==null){
                try {
                    adminTicket = parentBean.getDefaultOrganisationAdminTicket();
                } catch (Exception e){
                    logger.error("Error creating admin ticket: " + e.getMessage());
                }
            }       

            // List all of the filesystem scanners
            List allScanners = null;
            try {
                allScanners = EJBLocator.lookupScannerBean().listScanners(adminTicket); 
            } catch (Exception e){
                logger.error("Error listing remote filesystems: " + e.getMessage());
            }

            if(allScanners!=null){
                RemoteFilesystemScanner scanner;
                for(Object o : allScanners){
                    scanner = (RemoteFilesystemScanner)o;
                    if(scanner.isEnabled()){
                        try {
                            if(scanner.isAutoscanEnabled() && scanner.dueForUpdate()){
                                // Send the scan message
                                EJBLocator.lookupScannerBean().scanForChanges(scanner);
                            }
                        } catch (Exception e){
                            logger.error("Error scanning filesystem: " + e.getMessage());
                        }
                    }
                }
            }
        }
    }
}