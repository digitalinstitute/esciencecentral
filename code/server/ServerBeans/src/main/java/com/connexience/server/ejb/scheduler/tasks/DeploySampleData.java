/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateUtil;
import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.ejb.workflow.WorkflowDeploymentUtils;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.folder.TemplateFolder;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.util.WorkflowUtils;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * This class deploys the sample data archives from the autodeploy/samples folders.
 * Sample data folders are defined as the first level of child folder within the
 * autodeploy/samples folder. The folder last modification time is used to do the
 * deployment and the folders are deployed into the organisation top level
 * data folder.
 * @author hugo
 */
public class DeploySampleData extends SchedulerTask {
    private static Logger logger = Logger.getLogger(DeploySampleData.class);
    
    private File deployDirectory;
    private HashMap<String, FolderRecord> deployedFolders = new HashMap<>();
    private Ticket adminTicket = null;
    private Ticket publicTicket = null;
    
    public DeploySampleData(SchedulerBean parentBean) {
        super(parentBean);
        startDelay = 10000;
        startDelayed = true;
        repeatInterval = 4000;
        repeating = true;
        name = "DeploySampleData";
        deployDirectory = new File(System.getProperty("jboss.home.dir") + File.separator + "esc" + File.separator + "autodeploy" + File.separator + "samples");
    }

    @Override
    public void run() {
        // Only run if we are not using postgres
        if(parentBean.defaultOrganisationExists() && !HibernateUtil.isPostgres){
            logger.debug("Scanning deployment directory: " + deployDirectory.getPath());
            if(!deployDirectory.exists()){
                logger.info("Creating deployment directory: " + deployDirectory.getPath());
                deployDirectory.mkdirs();
            }
            
            
            if(adminTicket==null){
                try {
                    adminTicket = parentBean.getDefaultOrganisationAdminTicket();
                } catch (Exception e){
                    logger.error("Error creating admin ticket: " + e.getMessage());
                }
            }

            if(publicTicket==null && adminTicket!=null){
                try {
                    publicTicket = EJBLocator.lookupTicketBean().createPublicWebTicket();
                } catch (Exception e){
                    logger.error("Error getting public user id: " + e.getMessage());
                }
            }

            File[] contents = deployDirectory.listFiles();
            FolderRecord record;

            for(File f : contents){
                if(f.isDirectory() && !f.getName().startsWith(".")){
                    if(deployedFolders.containsKey(f.getPath())){
                        record = deployedFolders.get(f.getPath());

                        if(f.lastModified()>record.getFolder().lastModified()){
                            // Need to reset the file
                            record.reset();
                        }

                    } else {
                        record = new FolderRecord(f);
                        deployedFolders.put(f.getPath(), record);
                    }


                    if(record.isStable()){
                        // File size has stabilised
                        if(!record.isDeployed()){
                            if(adminTicket!=null && publicTicket!=null){
                                try {
                                    record.setDeployed(true);
                                    deployFolder(record.getFolder(), adminTicket, publicTicket);

                                } catch (Exception e){
                                    logger.error("Error doing deployment: " + e.getMessage());
                                }
                            }
                        }

                    } else {
                        record.sizeCheck();
                    }
                }            
            }
        }
    }    
    
    /** Deploy the folder into the system */
    private void deployFolder(File folder, Ticket ticket, Ticket publicUserTicket) throws ConnexienceException {
        // Find the corresponding folder in the system
        User u = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
        Folder uploadUserHome;
        if(u.getHomeFolderId()!=null && !u.getHomeFolderId().isEmpty()){
            uploadUserHome = EJBLocator.lookupStorageBean().getFolder(ticket, u.getHomeFolderId());
        } else {
            uploadUserHome = EJBLocator.lookupStorageBean().getDataFolder(ticket);
        }        
        
        Folder samplesFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, uploadUserHome.getId(), "SampleFiles");
        if(samplesFolder==null){
            samplesFolder = new Folder();
            samplesFolder.setName("SampleFiles");
            samplesFolder.setDescription("Automatically created by samples deployer");
            samplesFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, uploadUserHome.getId(), samplesFolder);
            EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), samplesFolder.getId(), Permission.READ_PERMISSION);
            logger.info("Crated SampleFiles folder");
        } else {
            if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(publicUserTicket, samplesFolder, Permission.READ_PERMISSION)){
                EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicUserTicket.getUserId(), samplesFolder.getId(), Permission.READ_PERMISSION);
                logger.info("Fixed permission on SampleFiles folder");
            }
        }        
        
        // Now do the upload
        Folder target = copyFolder(ticket, publicUserTicket, samplesFolder, folder);
        if(target!=null){
            // Make the folder into a template
            List templates = EJBLocator.lookupStorageBean().listTemplateFolders(ticket, "");
            TemplateFolder template;
            boolean needToCreateTemplate = true;

            for(int i=0;i<templates.size();i++){
                template = (TemplateFolder)templates.get(i);
                if(template.getFolderId().equals(target.getId())){
                    needToCreateTemplate= false;
                }
            }
            if(needToCreateTemplate){
                EJBLocator.lookupStorageBean().createTemplateFolder(ticket, target, "Example Data Files");
            }            
        }
        
    }
    
    /** Deploy a local folder to a remote folder */
    private Folder copyFolder(Ticket ticket, Ticket publicUserTicket, Folder parentFolder, File localFolder) throws ConnexienceException {
        // Is there a corresponding folder?
        Folder targetFolder = EJBLocator.lookupStorageBean().getNamedFolder(ticket, parentFolder.getId(), localFolder.getName());
        if(targetFolder==null){
            targetFolder = new Folder();
            targetFolder.setName(localFolder.getName());
            targetFolder.setContainerId(parentFolder.getId());
            targetFolder = EJBLocator.lookupStorageBean().addChildFolder(ticket, parentFolder.getId(), targetFolder);
            grantPublicAccess(ticket, publicUserTicket, targetFolder);
            logger.info("Created subfolder: " + targetFolder.getName());
        }        
        
        // Go through the local contents
        File[] localFiles = localFolder.listFiles();
        DocumentRecord remoteDocument;
        
        for(File f : localFiles){
            if(!f.getName().startsWith(".")){
                if(f.isDirectory()){
                    // Recurse into the directory
                    copyFolder(ticket, publicUserTicket, targetFolder, f);

                } else {
                    // Check file is uploaded
                    remoteDocument = EJBLocator.lookupStorageBean().getNamedDocumentRecord(ticket, targetFolder.getId(), f.getName());
                    if(remoteDocument!=null){
                        // Existing document, check times
                        if(f.lastModified()>remoteDocument.getTimeInMillis() || f.length()!=remoteDocument.getCurrentVersionSize()){
                            logger.info("Refreshing: " + f.getName());
                            remoteDocument.setTimeInMillis(f.lastModified());
                            remoteDocument = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, remoteDocument);
                            DocumentVersion v = StorageUtils.upload(ticket, f, remoteDocument, "Uploaded from deployer");
                            grantPublicAccess(ticket, publicUserTicket, remoteDocument);                        
                        }

                    } else {
                        // Upload
                        if(WorkflowDeploymentUtils.isFileWorkflowDocument(f)){
                            logger.info("Uploading workflow: " + f.getName());
                            remoteDocument = new WorkflowDocument();
                            remoteDocument.setName(f.getName());
                            remoteDocument.setContainerId(targetFolder.getId());
                            remoteDocument.setDescription("");
                            remoteDocument = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(ticket, (WorkflowDocument)remoteDocument);
                            remoteDocument.setTimeInMillis(f.lastModified());
                            remoteDocument = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(ticket, (WorkflowDocument)remoteDocument);
                            DocumentVersion v = StorageUtils.upload(ticket, f, remoteDocument, "Uploaded from deployer");
                            grantPublicAccess(ticket, publicUserTicket, remoteDocument);                            
                                    
                        } else {
                            logger.info("Uploading plain file: " + f.getName());
                            remoteDocument = new DocumentRecord();
                            remoteDocument.setName(f.getName());
                            remoteDocument.setContainerId(targetFolder.getId());
                            remoteDocument.setDescription("");
                            remoteDocument = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, remoteDocument);
                            remoteDocument.setTimeInMillis(f.lastModified());
                            remoteDocument = EJBLocator.lookupStorageBean().saveDocumentRecord(ticket, remoteDocument);
                            DocumentVersion v = StorageUtils.upload(ticket, f, remoteDocument, "Uploaded from deployer");
                            grantPublicAccess(ticket, publicUserTicket, remoteDocument);
                        }
                    }

                }

            }
        }
        return targetFolder;
    }
    
    private void grantPublicAccess(Ticket ticket, Ticket publicTicket, ServerObject target)  throws ConnexienceException {
        if(!EJBLocator.lookupAccessControlBean().canTicketAccessResource(publicTicket, target, Permission.READ_PERMISSION)){
            EJBLocator.lookupAccessControlBean().grantAccess(ticket, publicTicket.getUserId(), target.getId(), Permission.READ_PERMISSION);
        }
    }
    
    
    
    private class FolderRecord {
        private File folder;
        private boolean stable = false;
        private long lastSize = 0;
        private boolean deployed = false;
        
        public FolderRecord(File folder) {
            this.folder = folder;
        }
        
        public void sizeCheck(){
            long size = calculateSize();
            if(size==lastSize){
                stable = true;
            } else {
                lastSize = size;
            }
        }

        public boolean isStable() {
            return stable;
        }

        public File getFolder() {
            return folder;
        }

        public void setDeployed(boolean deployed) {
            this.deployed = deployed;
        }

        public boolean isDeployed() {
            return deployed;
        }
        
        public void reset(){
            lastSize = 0;
            stable = false;
            deployed = false;
        }
        
        private long calculateSize(){
            return calculateSize(folder, 0);
        }
        
        private long calculateSize(File dir, long runningTotal){
            File[] contents = dir.listFiles();
            long total = runningTotal;
            for(int i=0;i<contents.length;i++){
                if(contents[i].isDirectory()){
                    total+=calculateSize(contents[i], 0);
                } else {
                    total+=contents[i].length();
                }
            }
            return total;
            
        }
        
    }    
}