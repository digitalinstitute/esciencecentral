package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.StorageUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Business logic for Hubs that IoT devices use to send data into e-SC
 */
@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/HubBean", beanInterface = HubRemote.class)
public class HubBean extends HibernateSessionContainer implements HubRemote {

    @EJB
    private SubjectsRemote subjectsBean;

    @EJB
    private LoggersRemote loggersBean;

    @EJB
    private HubRemote hubBean;

    @EJB
    private StorageRemote storageBean;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    /**
     * CRUD HUBTYPE
     **/

    public HubType createHubType(Ticket ticket, String name) throws ConnexienceException {

        //find a type with this name if it exists
        HubType existing = getHubType(ticket, name);
        if(existing != null){
            return existing;
        }

        HubType h = new HubType();
        h.setName(name);

        //create a folder for the hub types in this User's home directory
        Folder hubsFolder = StorageUtils.getOrCreateFolderPath(ticket, storageBean.getHomeFolder(ticket, ticket.getUserId()), "Hubs");
        Folder thisHubTypeFolder = StorageUtils.getOrCreateFolderPath(ticket, hubsFolder, h.getName());
        h.setDataFolderId(thisHubTypeFolder.getId());

        //Make the folder public
        EJBLocator.lookupAccessControlBean().grantUniversalAccess(ticket, getPublicUser(ticket).getId(), thisHubTypeFolder.getId(), Permission.READ_PERMISSION);

        em.persist(h);
        return h;
    }

    public HubType getHubType(Ticket ticket, String name){
        List<HubType> hubTypes = em.createNamedQuery("HubType.findByName").setParameter("name", name).getResultList();
        if(hubTypes.size() == 0){
            return null;
        }
        else if(hubTypes.size() > 1){
            System.err.println("WARNING: Multiple HubTypes with name = " + name);
        }
        return hubTypes.get(0);
    }

    public HubType getHubType(Ticket ticket, int id) {
        return em.find(HubType.class, id);
    }

    public long countHubTypes(Ticket ticket) {
        return (long) em.createQuery("SELECT count(*) from HubType ht").getResultList().get(0);
    }

    public List<HubType> getHubTypes(Ticket ticket, final int start, final int maxResults) {
        List<HubType> hts = em.createQuery("SELECT ht from  HubType ht ORDER BY ht.id")
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hts == null) {
            hts = new ArrayList<>();
        }

        return hts;
    }

    public HubType updateHubType(Ticket ticket, HubType hubType) throws ConnexienceException {
        HubType retrieved = em.find(HubType.class, hubType.getId());
        if (retrieved != null) {
            //make sure the hub instances aren't deleted
            hubType.setInstances(retrieved.getInstances());
            hubType = em.merge(hubType);

            return hubType;
        } else {
            throw new ConnexienceException("Can't find HubType with id: " + hubType.getId());
        }
    }

    public void deleteHubtype(Ticket ticket, HubType hubType) throws ConnexienceException {
        hubType = em.merge(hubType);
        storageBean.removeFolderTree(ticket, hubType.getDataFolderId());
        em.remove(hubType);
    }


    /************************/
    /*  CRUD HUB INSTANCES  */
    /************************/

    public Hub createHub(Ticket ticket, HubType hubType, String serialNumber) throws ConnexienceException {

        List<Hub> existingHubs = em.createNamedQuery("Hub.findBySerialNumber").setParameter("serialNumber", serialNumber).getResultList();
        if(existingHubs.size() > 0){
            return existingHubs.get(0);
        }

        //create the hub instance
        Hub h = new Hub();
        h.setSerialNumber(serialNumber);
        hubType = em.merge(hubType);
        hubType.addInstance(h);
        h.setHubType(hubType);

        //create the folder under the hub type
        Folder hubTypeFolder = storageBean.getFolder(ticket, hubType.getDataFolderId());
        Folder hubInstanceFolder = StorageUtils.getOrCreateFolderPath(ticket, hubTypeFolder, serialNumber);
        EJBLocator.lookupAccessControlBean().grantUniversalAccess(ticket, getPublicUser(ticket).getId(), hubInstanceFolder.getId(), Permission.READ_PERMISSION);

        List<DocumentRecord> hubTypeSettingsFiles = storageBean.getFolderDocumentRecords(ticket, hubTypeFolder.getId());
        for (DocumentRecord file : hubTypeSettingsFiles) {
            DocumentRecord thisSettingsFile = StorageUtils.getOrCreateDocumentRecord(ticket, hubInstanceFolder.getId(), file.getName());
            DocumentVersion version = storageBean.getLatestVersion(ticket, file.getId());
            StorageUtils.copyDocumentData(ticket, file, version, thisSettingsFile);
            EJBLocator.lookupAccessControlBean().grantUniversalAccess(ticket, getPublicUser(ticket).getId(), thisSettingsFile.getId(), Permission.READ_PERMISSION);
        }

        //set the data folder for the hub
        h.setDataFolderId(hubInstanceFolder.getId());

        em.persist(hubType);
        em.persist(h);
        return h;
    }


    public Hub getHub(Ticket ticket, int hubId) throws ConnexienceException {
        return em.find(Hub.class, hubId);
    }

    public long countHubs(Ticket ticket) {
        return (long) em.createQuery("SELECT count(*) from Hub h").getResultList().get(0);
    }

    public List<Hub> getHubs(Ticket ticket, final int start, final int maxResults) {
        List<Hub> hubs = em.createQuery("SELECT h from  Hub h ORDER BY h.id")
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hubs == null) {
            hubs = new ArrayList<>();
        }

        return hubs;
    }

    public Hub updateHub(Ticket ticket, Hub hub) throws ConnexienceException {
        Hub retrieved = em.find(Hub.class, hub.getId());
        if (retrieved != null) {
            //make sure the deployments aren't deleted
            hub.setDeployments(retrieved.getDeployments());
            hub = em.merge(hub);
            return hub;
        } else {
            throw new ConnexienceException("Can't find Hub with id: " + hub.getId());
        }
    }

    public void deleteHub(Ticket ticket, Hub hub) throws ConnexienceException{
        hub = em.merge(hub);
        storageBean.removeFolderTree(ticket, hub.getDataFolderId());
        em.remove(hub);
    }

    /* ************************/
    /*  CRUD HUB DEPLOYMENTS */
    /*************************/

    public HubDeployment deployHub(Ticket ticket, Hub hub, SubjectGroup sg, Date startDate, Date endDate) throws ConnexienceException {

        hub = em.merge(hub);
        sg = em.merge(sg);

        //Create the deployment
        HubDeployment dep = new HubDeployment();
        dep.setStartDate(startDate);
        dep.setEndDate(endDate);
        dep.setSubjectGroup(sg);
        dep.setActive(true);
        hub.addDeployment(dep);

        em.persist(hub);
        em.persist(dep);
        em.persist(sg);
        return dep;
    }

    public void undeployHub(Ticket ticket, Hub hub, SubjectGroup sg){
        hub = em.merge(hub);
        sg = em.merge(sg);

        for(HubDeployment dep: sg.getHubDeployments()){
            if(dep.getHub().getSerialNumber().equals(hub.getSerialNumber())){
                dep.setActive(false);
                em.persist(dep);
            }
        }
    }

    public HubDeployment getDeployment(Ticket ticket, int deploymentId) throws ConnexienceException {
        return em.find(HubDeployment.class, deploymentId);
    }

    public long countHubDeployments(Ticket ticket) {
        return (long) em.createQuery("SELECT count(*) from HubDeployment hd").getResultList().get(0);
    }

    public long countHubDeployments(Ticket ticket, SubjectGroup sg) {
        return (long) em.createQuery("SELECT count(*) from HubDeployment hd WHERE hd.subjectGroup = :sg")
                .setParameter("sg", sg)
                .getResultList()
                .get(0);
    }


    public List<HubDeployment> getHubDeployments(Ticket ticket, final int start, final int maxResults) {
        List<HubDeployment> hubDeployments = em.createQuery("SELECT hd from  HubDeployment hd ORDER BY hd.id")
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hubDeployments == null) {
            hubDeployments = new ArrayList<>();
        }

        return hubDeployments;
    }

    public List<HubDeployment> getAllHubDeployments(Ticket ticket, SubjectGroup sg, final int start, final int maxResults) {
        List<HubDeployment> hubDeployments = em.createQuery("SELECT hd from  HubDeployment hd WHERE hd.subjectGroup = :sg ORDER BY hd.id")
                .setParameter("sg", sg)
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hubDeployments == null) {
            hubDeployments = new ArrayList<>();
        }

        return hubDeployments;
    }

    public List<HubDeployment> getActiveHubDeployments(Ticket ticket, SubjectGroup sg, final int start, final int maxResults) {
        List<HubDeployment> hubDeployments = em.createQuery("SELECT hd from HubDeployment hd " +
                "WHERE hd.subjectGroup = :sg " +
                "AND hd.active = true " +
                "ORDER BY hd.id")
                .setParameter("sg", sg)
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hubDeployments == null) {
            hubDeployments = new ArrayList<>();
        }

        return hubDeployments;
    }

    public List<HubDeployment> getInactiveHubDeployments(Ticket ticket, SubjectGroup sg, final int start, final int maxResults) {
        List<HubDeployment> hubDeployments = em.createQuery("SELECT hd from HubDeployment hd " +
                "WHERE hd.subjectGroup = :sg " +
                "AND hd.active = false " +
                "ORDER BY hd.id")
                .setParameter("sg", sg)
                .setFirstResult(0)
                .setMaxResults(0)
                .getResultList();

        if (hubDeployments == null) {
            hubDeployments = new ArrayList<>();
        }

        return hubDeployments;
    }

    public HubDeployment updateHubDeployment(Ticket ticket, HubDeployment hubDeployment) {
        return em.merge(hubDeployment);
    }

    public void deleteHubDeployment(Ticket ticket, HubDeployment hubDeployment) throws ConnexienceException{
        hubDeployment = em.merge(hubDeployment);
        hubDeployment.getSubjectGroup().getHubDeployments().remove(hubDeployment);
        em.remove(hubDeployment);
    }

    public Hub saveHub(Ticket ticket, Hub hub) {
        hub = em.merge(hub);
        em.persist(hub);
        return hub;
    }


}
