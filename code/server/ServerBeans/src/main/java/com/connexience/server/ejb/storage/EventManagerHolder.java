/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.storage;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.events.EventStoreManager;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.security.Ticket;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.log4j.Logger;

/**
 * This singleton keeps hold of an event store manager
 * @author hugo
 */
@Singleton
@Startup
public class EventManagerHolder {
    public static final Logger logger = Logger.getLogger(EventManagerHolder.class);
    private Boolean lock = true;
    private EventStoreManager manager;

    public EventManagerHolder() {
    }
    
    public boolean checkEventStore(Ticket t, String studyCode) throws ConnexienceException {
        if(!manager.containsFlatStudyStore(studyCode)){
            synchronized(lock){
                List<Project> results = EJBLocator.lookupProjectsBean().getProjectsByExternalId(t, studyCode);
                if(results.size()==1 && results.get(0) instanceof FlatStudy){
                    FlatStudy study = (FlatStudy)results.get(0);
                    EJBLocator.lookupFlatStudyBean().checkFlatStudyProperties(t, study.getId());
                    String hostname = study.getAdditionalProperty("EventStoreHost");
                    String username = study.getAdditionalProperty("EventStoreUsername");
                    String password = study.getAdditionalProperty("EventStorePassword");
                    String database = study.getAdditionalProperty("EventStoreDatabase");
                    int port = Integer.valueOf(study.getAdditionalProperty("EventStorePort"));
                    String collection = study.getAdditionalProperty("EventStoreTable");
                    String eventStoreType = study.getAdditionalProperty("EventStoreType");
                    String topicName = study.getAdditionalProperty("EventExportTopic");

                    if(manager.addFlatStudyStore(eventStoreType, studyCode, hostname, port, username, password, database, collection, topicName)){
                        logger.info("Created a new: " + eventStoreType + " backend for study: " + studyCode);
                        return true;
                    } else {
                        logger.error("Error creating event store for study: " + studyCode);
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return true;
        }     
    }        

    public EventStoreManager getManager() {
        return manager;
    }
    
    @PostConstruct
    public void init(){
        logger.info("Starting up event store manager");
        manager = new EventStoreManager();
    }
    
    @PreDestroy
    public void shutdown(){
        logger.info("Shutting down event store manager");
        if(manager!=null){
            manager.close();
        }
    }
}
