/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.quota;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.model.quota.UserQuota;
import com.connexience.server.model.security.Ticket;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

/**
 * This class provides an EJB that manages the quota for a user.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/QuotaBean", beanInterface = QuotaRemote.class)
public class QuotaBean extends HibernateSessionContainer implements QuotaRemote {

    @Override
    public UserQuota getOrCreateUserQuota(Ticket ticket, String userId) throws ConnexienceException {
        Session session = null;
        try {
            if(ticket.getUserId().equals(userId) || isOrganisationAdminTicket(ticket)){
                session = getSession();
                Query q = session.createQuery("from UserQuota as q where q.userId=:userId");
                q.setString("userId", userId);
                List results = q.list();
                if(results.size()>0){
                    // Existing quota
                    return (UserQuota)results.get(0);
                } else {
                    // Create
                    UserQuota quota = createDefaultQuota(userId);
                    quota = (UserQuota)savePlainObject(quota, session);
                    return quota;
                }
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } catch (Exception e){
            throw new ConnexienceException("Error getting user quota data: " + e.getMessage(), e);
        } finally {
            closeSession(session);
        }
    }

    private UserQuota createDefaultQuota(String userId) throws ConnexienceException {
        try {
            Properties props = new Properties();
            File quotaProperties = new File(System.getProperty("user.home") + File.separator + ".inkspot", "quota.properties");
            if(quotaProperties.exists() && quotaProperties.isFile()){
                // Load file
                props.load(new FileReader(quotaProperties));
            } else {
                // Load default
                props.load(getClass().getResourceAsStream("/com/connexience/server/ejb/quota/quota.properties"));            
            }
        
            UserQuota q = new UserQuota();
            if(props.getProperty("DefaultQuotaEnabled", "false").equals("true")){
                // Quota on
                q.setStorageQuotaEnforced(true);
                Long quotaValue = Long.parseLong(props.getProperty("DefaultQuotaSize", "0"));
                q.setStorageQuota(quotaValue);
                q.setUserId(userId);
            } else {
                // Quota off
                q.setStorageQuotaEnforced(false);
                q.setStorageQuota(0);
                q.setUserId(userId);
            }
            return q;
        } catch (Exception e){
            throw new ConnexienceException("Error creating default quota object: " + e.getMessage(), e);
        }
        
    }
    
    @Override
    public UserQuota saveUserQuota(Ticket ticket, UserQuota quota) throws ConnexienceException {
        if(ticket.getUserId().equals(quota.getUserId()) || isOrganisationAdminTicket(ticket)){
            Session session = null;
            try {
                session = getSession();
                if(quota.getId()!=0){
                    return (UserQuota)session.merge(quota);
                } else {
                    session.persist(quota);
                    return quota;
                }
                
            } catch (Exception e){
                throw new ConnexienceException("Error saving quota: " + e.getMessage());
            } finally {
                closeSession(session);
            }
            
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public boolean userHasStorageQuota(Ticket ticket, String userId) throws ConnexienceException {
        UserQuota q = getOrCreateUserQuota(ticket, userId);
        if(q!=null){
            return q.isStorageQuotaEnforced();
        } else {
            return false;
        }
    }

    @Override
    public long getStorageQuotaUsed(Ticket ticket, String userId) throws ConnexienceException {
        if(ticket.getUserId().equals(userId)){
            Connection c = null;
            ResultSet r = null;
            PreparedStatement s = null;
            try {
                c = getSQLConnection();
                s = c.prepareStatement("select coalesce(sum(size),0) from documentversions where userid=?");
                s.setString(1,userId);
                r = s.executeQuery();
                if(r.next()){
                    return r.getLong(1);
                } else {
                    throw new Exception("No data returned");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error checking storage quota: " + e.getMessage(), e);
            } finally {
                try {r.close();}catch(Exception e){}
                try {s.close();}catch(Exception e){}
                try {c.close();}catch(Exception e){}
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public long getStorageQuotaUsedInProject(Ticket ticket, String userId, String projectId) throws ConnexienceException {
        if(ticket.getUserId().equals(userId)){
            Connection c = null;
            ResultSet r = null;
            PreparedStatement s = null;
            try {
                c = getSQLConnection();
                String queryString  = "select coalesce(sum(size),0) from documentversions as dv, objectsflat as dr where " +
                        "dr.objecttype = 'DOCUMENTRECORD' AND " +
                        "dr.id = dv.documentrecordid AND " +
                        "dv.userid = ? ";

                if(projectId == null)
                {
                    queryString += " AND dr.projectid IS NULL";
                    s = c.prepareStatement(queryString);
                }
                else
                {
                    queryString += "AND dr.projectid = ?";
                    s = c.prepareStatement(queryString);
                    s.setString(2, projectId);
                }


                s.setString(1, userId);

                r = s.executeQuery();
                if(r.next()){
                    return r.getLong(1);
                } else {
                    throw new Exception("No data returned");
                }
            } catch (Exception e){
                throw new ConnexienceException("Error checking storage quota: " + e.getMessage(), e);
            } finally {
                try {r.close();}catch(Exception e){}
                try {s.close();}catch(Exception e){}
                try {c.close();}catch(Exception e){}
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
        
    @Override
    public long getAvailableStorageQuota(Ticket ticket, String userId) throws ConnexienceException {
        UserQuota quota = getOrCreateUserQuota(ticket, userId);
        if(quota!=null){
            if(quota.isStorageQuotaEnforced()){
                long used = getStorageQuotaUsed(ticket, userId);
                return quota.getStorageQuota() - used;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}