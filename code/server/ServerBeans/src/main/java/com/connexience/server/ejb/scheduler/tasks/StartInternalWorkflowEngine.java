/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.workflow.InternalWorkflowEngine;
import org.apache.log4j.Logger;

/**
 * Tries to start the internal workflow engine
 * @author hugo
 */
public class StartInternalWorkflowEngine extends SchedulerTask { 
    private static Logger logger = Logger.getLogger(StartInternalWorkflowEngine.class);
    
    public StartInternalWorkflowEngine(SchedulerBean parentBean) {
        super(parentBean);
        repeating = false;
        startDelayed = true;
        startDelay = 10000;
        enabled = true;
    }

    @Override
    public void run() {
        logger.info("Checking internal workflow engine startup");
        if(parentBean.defaultOrganisationExists()){
            try {
                InternalWorkflowEngine.ENGINE_INSTANCE.startInternalWorkflowEngine();
            } catch (Exception e){
                logger.error("Error starting internal worflow engine: " + e.getMessage());
            }
        }
        removeFromParent();
    }
    
}
