/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.certificate;

import com.connexience.server.ConnexienceException;
import com.connexience.server.crypto.ServerObjectKeyManager;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.KeyData;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.util.ByteArrayCompare;
import com.connexience.server.util.SignatureUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.security.MessageDigest;
import java.security.cert.X509Certificate;

/**
 * This EJB provides means to access X509 certificates for objects, check whether certificates
 * belong to who they say they do etc.
 * @author hugo
 */
@Stateless
@EJB(name = "java:global/ejb/CertificateAccessBean", beanInterface = CertificateAccessRemote.class)
public class CertificateAccessBean extends HibernateSessionContainer implements CertificateAccessRemote {
    /** Key manager object for getting information from the database */
    private ServerObjectKeyManager keyManager;
    
    /** Creates a new instance of CertificateAccessBean */
    public CertificateAccessBean() {
        keyManager = new ServerObjectKeyManager();
    }
    


	public byte[] getOrganisationKey() throws ConnexienceException {
		final Ticket ticket = getDefaultOrganisationAdminTicket();
		final Organisation defaultOrganisation = EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(ticket);

		ServerObjectKeyManager mgr = new ServerObjectKeyManager();
		KeyData kd = mgr.getObjectKeyData(defaultOrganisation.getId());

		if(kd!=null){
			return kd.getKeyStoreData();
		} else {
			throw new ConnexienceException("No key exists for default organisation.");
		}
	}
}
