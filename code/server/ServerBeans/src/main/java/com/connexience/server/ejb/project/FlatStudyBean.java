/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.project;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.workflow.WorkflowManagementBean;
import com.connexience.server.jms.InkspotConnectionFactory;
import com.connexience.server.jms.JMSProperties;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.model.project.flatstudy.FlatDevice;
import com.connexience.server.model.project.flatstudy.FlatGateway;
import com.connexience.server.model.project.flatstudy.FlatPerson;
import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.project.flatstudy.FlatStudyConstants;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.util.StorageUtils;
import org.hibernate.Hibernate;

import java.util.*;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.apache.log4j.Logger;

/**
 * This EJB provides flat study management functions.
 * @author hugo
 */
@SuppressWarnings({"unchecked", "unused"})
@Stateless
@EJB(name = "java:global/ejb/FlatStudyBean", beanInterface = FlatStudyRemote.class)
public class FlatStudyBean extends HibernateSessionContainer implements FlatStudyRemote, FlatStudyConstants{
    private static Logger logger =  Logger.getLogger(WorkflowManagementBean.class);
    @Inject
    @InkspotConnectionFactory
    private ConnectionFactory connectionFactory;
    
    @PersistenceContext(unitName = "primary")
    private EntityManager em;
    
    @EJB
    private ProjectsRemote projectBean;
    
    @EJB
    private StorageRemote storageBean;

    @Override
    public FlatStudy getFlatStudy(Ticket ticket, Integer studyId) throws ConnexienceException {
        FlatStudy study = em.find(FlatStudy.class, studyId);
        if(study!=null){
            if(ticket.isSuperTicket() || projectBean.isProjectMember(ticket, study)){
                return study;
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } else {
            return null;
        }
    }
    
    @Override
    public FlatStudy saveFlatStudy(Ticket ticket, FlatStudy study) throws ConnexienceException {

        //Manually add all of the upload users and phases as these are not passed through the API
        //Uploaders are removed from a study by a different API call
        if (study.getId() != null) {
            study.getUploaders();
            //Get the exising study from the database
            FlatStudy existingStudy = em.merge(getFlatStudy(ticket, study.getId()));

            // Reset the event store for this study
            EJBLocator.lookupEventStoreBean().resetStudyEventStore(ticket, existingStudy.getExternalId());

            //Initialise the lazily loaded uploaders and phases collections
            Hibernate.initialize(existingStudy.getUploaders());

            //copy the collection as otherwise it will be deleted by em.merge(study)
            Collection<Uploader> existingUploaders = new ArrayList(existingStudy.getUploaders());

            //This is necessary to get the study into the session and force the uploaders to be loaded
            // even though it will be an empty set
            study = em.merge(study);
            Hibernate.initialize(study.getUploaders());

            //Re-add all of the uploaders and phases that we cached earlier
            for (Uploader uploader : existingUploaders) {
                study.addUploader(uploader);
            }
        } else {
            // Do a reset anyway
            EJBLocator.lookupEventStoreBean().resetStudyEventStore(ticket, study.getExternalId());
        }
        
        checkFlatStudyProperties(study);

        study = (FlatStudy)projectBean.saveProject(ticket, study);
        em.merge(study);
        
        // Check the correct data folders exist
        Folder dataFolder = storageBean.getFolder(ticket, study.getDataFolderId());
        if(dataFolder!=null){
            Folder devicesFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "data/devices");
            Folder peopleFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "data/people");
            Folder gatewaysFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "data/gateways");
            
            Folder codeFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, FlatStudy.CODE_FOLDER_NAME);
            Folder defaultFolder = StorageUtils.getOrCreateFolderPath(ticket, codeFolder, FlatStudy.DEFAULT_CODE_FOLDER_NAME);
            study.setCodeFolderId(codeFolder.getId());            
        }
        
        return study;
    }

    @Override
    public void checkFlatStudyCodeFolders(Ticket ticket, Integer studyId) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, s.getDataFolderId());
            if(dataFolder!=null){
                Folder codeFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, FlatStudy.CODE_FOLDER_NAME);
                Folder defaultFolder = StorageUtils.getOrCreateFolderPath(ticket, codeFolder, FlatStudy.DEFAULT_CODE_FOLDER_NAME);
                Folder dataflowsFolder = StorageUtils.getOrCreateFolderPath(ticket, codeFolder, "dataflows");
                s.setCodeFolderId(codeFolder.getId());
                saveFlatStudy(ticket, s);
            } else {
                throw new ConnexienceException("Study does not have a data folder");
            }
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }   
    }

    @Override
    public void checkFlatStudyProperties(Ticket ticket, Integer studyId) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            checkFlatStudyProperties(s);
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }
    }
    
    private void checkFlatStudyProperties(FlatStudy s){
        Map<String, String> props = s.getAdditionalProperties();
        
        if(!props.containsKey("EventExportTopic")){
            props.put("EventExportTopic", "EventExport");
        }
        
        if(!props.containsKey("EventStoreHost")){
            props.put("EventStoreHost", "localhost");
        }

        if(!props.containsKey("EventStorePort")){
            props.put("EventStorePort", "27017");
        }

        if(!props.containsKey("EventStoreUsername")){
            props.put("EventStoreUsername", "user");
        }

        if(!props.containsKey("EventStorePassword")){
            props.put("EventStorePassword", "password");
        }

        if(!props.containsKey("EventStoreTable")){
            props.put("EventStoreTable", "STUDY" + s.getId());
        }        
        
        if(!props.containsKey("EventStoreDatabase")){
            props.put("EventStoreDatabase", "StudyData");
        }
        
        if(!props.containsKey("EventStoreType")){
            props.put("EventStoreType", "mongo");
        }
        
        if(!props.containsKey("RegistrationCode")){
            props.put("RegistrationCode", "");
        }
    }
    
    @Override
    public void removeFlatStudy(Ticket ticket, Integer studyId, boolean removeStorage) throws ConnexienceException {
        // Remove all of the devices
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            if(projectBean.isProjectAdmin(ticket, s)){
                Query removeDeviceQuery = em.createNamedQuery("FlatDevice.deleteForStudy");
                removeDeviceQuery.setParameter("studyId", studyId);
                removeDeviceQuery.executeUpdate();

                Query removePersonQuery = em.createNamedQuery("FlatPerson.deleteForStudy");
                removePersonQuery.setParameter("studyId", studyId);
                removePersonQuery.executeUpdate();
                        
                Query removeGatewayQuery = em.createNamedQuery("FlatGateway.deleteForStudy");
                removeGatewayQuery.setParameter("studyId", studyId);
                removeGatewayQuery.executeUpdate();
                
                // Now remove the project itself
                projectBean.deleteProject(ticket, studyId, removeStorage);
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        }
    }

    @Override
    public FlatDevice saveFlatDevice(Ticket ticket, FlatDevice device) throws ConnexienceException {
        FlatDevice result;
        if(device.getId()!=null){
            result = em.merge(device);
            
        } else {
            em.persist(device);
            result = device;
        }
        return result;
    }

    private Folder createContainerForFlatStudyObject(Ticket ticket, Folder parent, String rootFolder, Date timestamp) throws ConnexienceException {
        Calendar c = Calendar.getInstance();
        c.setTime(timestamp);
        
        StringBuilder path = new StringBuilder();
        path.append(rootFolder);
        path.append("/");
        path.append(c.get(Calendar.YEAR));
        path.append("/");
        path.append(c.get(Calendar.MONTH));
        path.append("/");
        path.append(c.get(Calendar.DAY_OF_MONTH));
        path.append("/");
        path.append(c.get(Calendar.HOUR_OF_DAY));
        Folder f = StorageUtils.getOrCreateFolderPath(ticket, parent, path.toString());
        return f;        
    }
    
    private Folder createFolderForFlatStudyObject(Ticket ticket, Folder parent, String rootFolder, Date timestamp, Integer objectId) throws ConnexienceException {
        Calendar c = Calendar.getInstance();
        c.setTime(timestamp);
        
        StringBuilder path = new StringBuilder();
        path.append(rootFolder);
        path.append("/");
        path.append(c.get(Calendar.YEAR));
        path.append("/");
        path.append(c.get(Calendar.MONTH));
        path.append("/");
        path.append(c.get(Calendar.DAY_OF_MONTH));
        path.append("/");
        path.append(c.get(Calendar.HOUR_OF_DAY));
        path.append("/");
        path.append(objectId);
        Folder f = StorageUtils.getOrCreateFolderPath(ticket, parent, path.toString());
        return f;
    }
    
    @Override
    public FlatDevice createFlatDevice(Ticket ticket, Integer studyId, String externalDeviceId) throws ConnexienceException {
        FlatDevice d = new FlatDevice();
        Project p = projectBean.getProject(ticket, studyId);
        if(p instanceof FlatStudy){
            if(projectBean.isProjectAdmin(ticket, p)){
                d.setStudyId(p.getId());
                d.setExternalId(externalDeviceId);
                em.persist(d);
                
                // Get a folder for this device
                Folder dataFolder = storageBean.getFolder(ticket, p.getDataFolderId());
                Folder deviceFolder = createFolderForFlatStudyObject(ticket, dataFolder, "data/devices", d.getCreationDate(), d.getId());
                d.setFolderId(deviceFolder.getId());

                return d;
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } else {
            throw new ConnexienceException("Project:" + studyId + " is not a flat study");
        }
    }

    @Override
    public FlatDevice getFlatDevice(Ticket ticket, Integer deviceId) throws ConnexienceException {
        return em.find(FlatDevice.class, deviceId);
    }

    @Override
    public FlatDevice getFlatDeviceByExternalId(Ticket ticket, Integer studyId, String externalDeviceId) throws ConnexienceException{
        Project p = projectBean.getProject(ticket, studyId);
        if(projectBean.isProjectMember(ticket, p)){
            try {
                Query q = em.createNamedQuery("FlatDevice.findByExternalId", FlatDevice.class);
                q.setParameter("externalId", externalDeviceId);
                q.setParameter("studyId", studyId);
                List results = q.getResultList();
                if(results.size()>0){
                    return (FlatDevice)results.get(0);
                } else {
                    return null;
                }
            } catch (Exception e){
                throw new ConnexienceException("Cannot execute query: " + e.getMessage(), e);
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
  
    }

    @Override
    public FlatGateway getFlatGatewayByExternalIdAndStudyCode(Ticket ticket, String studyCode, String externalGatewayId) throws ConnexienceException {
        Query studyIdQuery = em.createNamedQuery("Project.getIdFromExternalId");
        studyIdQuery.setMaxResults(1);
        studyIdQuery.setParameter("externalId", studyCode);
        
        if(studyIdQuery.getSingleResult()!=null){
            Integer studyId = (Integer)studyIdQuery.getSingleResult();
            Query gatewayQuery = em.createNamedQuery("FlatGateway.findByExternalId");
            gatewayQuery.setMaxResults(1);
            gatewayQuery.setParameter("externalId", externalGatewayId);
            gatewayQuery.setParameter("studyId", studyId);
            if(gatewayQuery.getSingleResult() instanceof FlatGateway){
                return (FlatGateway)gatewayQuery.getSingleResult();
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException("No such study code: " + studyCode);
        }
    }

    @Override
    public void moveFlatDevice(Ticket ticket, Integer deviceId, Integer targetStudyId) throws ConnexienceException {

    }
    
    @Override
    public void removeFlatDevice(Ticket ticket, Integer deviceId) throws ConnexienceException {
        FlatDevice d = getFlatDevice(ticket, deviceId);
        if(d!=null){
            FlatStudy s = getFlatStudy(ticket, d.getStudyId());
            if(s!=null){
                if(projectBean.isProjectAdmin(ticket, s)){
                    // Delete the gateway data folders
                    EJBLocator.lookupStorageBean().removeFolderTree(ticket, d.getFolderId());
                    
                    Query q = em.createNamedQuery("FlatDevice.deleteById");
                    q.setParameter("id", deviceId);
                    q.executeUpdate();                    
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else {
                // Hanging device
                Query q = em.createNamedQuery("FlatDevice.deleteById");
                q.setParameter("id", deviceId);
                q.executeUpdate();
            }
        }
    }

    @Override
    public FlatPerson createFlatPerson(Ticket ticket, Integer studyId, String externalPersonId) throws ConnexienceException {
        FlatPerson person = new FlatPerson();
        Project p = projectBean.getProject(ticket, studyId);
        if(p instanceof FlatStudy){
            if(projectBean.isProjectAdmin(ticket, p)){
                person.setStudyId(p.getId());
                person.setExternalId(externalPersonId);
                em.persist(person);
                
                // Get a folder for this person
                Folder dataFolder = storageBean.getFolder(ticket, p.getDataFolderId());
                Folder personFolder = createFolderForFlatStudyObject(ticket, dataFolder, "data/people", person.getCreationDate(), person.getId());
                person.setFolderId(personFolder.getId());
                
                return person;
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } else {
            throw new ConnexienceException("Project:" + studyId + " is not a flat study");
        }
    }

    @Override
    public FlatPerson saveFlatPerson(Ticket ticket, FlatPerson person) throws ConnexienceException {
        if(person.getId()!=null){
            FlatPerson existing = em.merge(person);
            return existing;
        } else {
            em.persist(person);
            return person;
        }
    }

    @Override
    public void moveFlatPerson(Ticket ticket, Integer personId, Integer targetStudyId) throws ConnexienceException {
        FlatPerson person = EJBLocator.lookupFlatStudyBean().getFlatPerson(ticket, personId);
        FlatStudy sourceStudy = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, person.getStudyId());
        FlatStudy targetStudy = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, targetStudyId);
        
        // Get the owner ticket for the study that the gateway is being moved to
        Ticket targetStudyTicket = EJBLocator.lookupTicketBean().createStudyTicket(targetStudy.getId());
        
        // Find the data folder
        Folder personFolder = EJBLocator.lookupStorageBean().getFolder(ticket, person.getFolderId());
        
        // Get / Create a new container folder for the data folder
        Folder targetStudyFolder = EJBLocator.lookupStorageBean().getFolder(targetStudyTicket, targetStudy.getDataFolderId());
        Folder targetContainerFolder = createContainerForFlatStudyObject(targetStudyTicket, targetStudyFolder, "data/people", person.getCreationDate());
        
        // Move the data there
        personFolder.setContainerId(targetContainerFolder.getId());
        personFolder = EJBLocator.lookupStorageBean().updateFolder(ticket, personFolder);
        EJBLocator.lookupAccessControlBean().propagateOwnership(ticket, personFolder.getId(), targetStudyTicket.getUserId());
        person.setStudyId(targetStudy.getId());
        person = saveFlatPerson(ticket, person);
        
        // Send an event message
        HashMap<String, Object> props = new HashMap<>();
        props.put(NEW_STUDY_ID_PROPERTY, targetStudy.getId());
        props.put(STUDY_ID_PROPERTY, sourceStudy.getId());
        props.put(OBJECT_ID_PROPERTY, person.getId());
        props.put(STUDY_CODE_PROPERTY, targetStudy.getExternalId());
        props.put(OBJECT_EXTERNAL_ID_PROPERTY, person.getExternalId());
        sendFlatStudyEventMessage(ticket, PERSON_MOVED_MESSAGE, props);
    }
    
    @Override
    public void removeFlatPerson(Ticket ticket, Integer personId) throws ConnexienceException {
        FlatPerson p = getFlatPerson(ticket, personId);
        if(p!=null){
            String externalId = p.getExternalId();
            FlatStudy s = getFlatStudy(ticket, p.getStudyId());
            if(s!=null){
                if(projectBean.isProjectAdmin(ticket, s)){
                    // Delete the person data folders
                    EJBLocator.lookupStorageBean().removeFolderTree(ticket, p.getFolderId());
                    
                    Query q = em.createNamedQuery("FlatPerson.deleteById");
                    q.setParameter("id", personId);
                    q.executeUpdate();    
                    
                    // Send an event message
                    HashMap<String, Object> props = new HashMap<>();
                    props.put(STUDY_ID_PROPERTY, s.getId());
                    props.put(OBJECT_ID_PROPERTY, personId);
                    props.put(STUDY_CODE_PROPERTY, s.getExternalId());
                    props.put(OBJECT_EXTERNAL_ID_PROPERTY, externalId);
                    sendFlatStudyEventMessage(ticket, PERSON_REMOVED_MESSAGE, props);                    
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else {
                // Hanging device
                Query q = em.createNamedQuery("FlatPerson.deleteById");
                q.setParameter("id", personId);
                q.executeUpdate();
            }
        }
    }

    @Override
    public FlatPerson getFlatPerson(Ticket ticket, Integer personId) throws ConnexienceException {
        return em.find(FlatPerson.class, personId);
    }

    @Override
    public FlatPerson getFlatPersonUsingDocumentId(Ticket ticket, String documentId) throws ConnexienceException {
        DocumentRecord doc = EJBLocator.lookupStorageBean().getDocumentRecord(ticket, documentId);
        if(doc!=null){
            Query q = em.createNamedQuery("FlatPerson.findByFolderId", FlatPerson.class);
            q.setParameter("folderId", doc.getContainerId());
            return (FlatPerson)q.getSingleResult();
        } else {
            throw new ConnexienceException("No such document: " + documentId);
        }
    }

    @Override
    public FlatPerson getFlatPersonByExternalIdOnly(Ticket ticket, String externalPersonId) throws ConnexienceException {
        if(isOrganisationAdminTicket(ticket)){
            Query q = em.createNamedQuery("FlatPerson.findByExternalIdOnly", FlatPerson.class);
            q.setParameter("externalId", externalPersonId);
            List results = q.getResultList();
            if(results.size()==1){
                return (FlatPerson)results.get(0);
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    @Override
    public FlatPerson getFlatPersonByExternalId(Ticket ticket, Integer studyId, String externalPersonId) throws ConnexienceException {
        Project p = projectBean.getProject(ticket, studyId);
        if(projectBean.isProjectMember(ticket, p)){
            Query q = em.createNamedQuery("FlatPerson.findByExternalId", FlatPerson.class);
            q.setParameter("externalId", externalPersonId);
            q.setParameter("studyId", studyId);
            List results = q.getResultList();
            if(results.size()>0){
                return (FlatPerson)results.get(0);
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }

    @Override
    public String getFlatGatewayFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        Query q = em.createNamedQuery("FlatGateway.getFolderId");
        q.setParameter("id", gatewayId);
        return (String)q.getSingleResult();
    }

    @Override
    public String getFlatGatewayCodeFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        Query q = em.createNamedQuery("FlatGateway.getCodeFolderId");
        q.setParameter("id", gatewayId);
        return (String)q.getSingleResult();
    }

    @Override
    public String getFlatGatewayDownloadsFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        Query q = em.createNamedQuery("FlatGateway.getDownloadsFolderId");
        q.setParameter("id", gatewayId);
        return (String)q.getSingleResult();
    }

    @Override
    public String getFlatGatewayUploadsFolderId(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        Query q = em.createNamedQuery("FlatGateway.getUploadsFolderId");
        q.setParameter("id", gatewayId);
        return (String)q.getSingleResult();
    }

    @Override
    public FlatGateway getFlatGateway(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        FlatGateway gateway = em.find(FlatGateway.class, gatewayId);
        return gateway;
    }

    @Override
    public FlatGateway getFlatGatewayByExternalId(Ticket ticket, Integer studyId, String externalGatewayId) throws ConnexienceException {
        Project p = projectBean.getProject(ticket, studyId);
        if(projectBean.isProjectMember(ticket, p)){
            Query q = em.createNamedQuery("FlatGateway.findByExternalId", FlatGateway.class);
            q.setParameter("externalId", externalGatewayId);
            q.setParameter("studyId", studyId);
            List results = q.getResultList();
            if(results.size()>0){
                FlatGateway gateway = (FlatGateway)results.get(0);
                return gateway;
            } else {
                return null;
            }
        } else {
            throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
        }
    }
    
    @Override
    public FlatGateway createFlatGateway(Ticket ticket, Integer studyId, String externalGatewayId) throws ConnexienceException {
        FlatGateway g = new FlatGateway();
        Project p = projectBean.getProject(ticket, studyId);
        if(p instanceof FlatStudy){
            if(projectBean.isProjectAdmin(ticket, p)){
                g.setStudyId(p.getId());
                g.setExternalId(externalGatewayId);
                em.persist(g);
                
                // Get a folder for this gateway
                Folder dataFolder = storageBean.getFolder(ticket, p.getDataFolderId());
                Folder deviceFolder = createFolderForFlatStudyObject(ticket, dataFolder, "data/gateways", g.getCreationDate(), g.getId());
                g.setFolderId(deviceFolder.getId());
                
                checkFlatGatewayFolders(ticket, g.getId());
                return g;
            } else {
                throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
            }
        } else {
            throw new ConnexienceException("Project:" + studyId + " is not a flat study");
        }
    }

    
    @Override
    public void checkFlatGatewayFolders(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        FlatGateway gateway = em.find(FlatGateway.class, gatewayId);
        if(gateway!=null){
            Folder dataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, gateway.getFolderId());
            Folder codeFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "code");
            gateway.setCodeFolderId(codeFolder.getId());
            
            Folder uploadsFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "uploads");
            gateway.setUploadsFolderId(uploadsFolder.getId());
            
            Folder downloadsFolder = StorageUtils.getOrCreateFolderPath(ticket, dataFolder, "downloads");
            gateway.setDownloadsFolderId(downloadsFolder.getId());
            
            // Common code folders
            if(gateway.getGatewayType()!=null && !gateway.getGatewayType().isEmpty()){
                FlatStudy s = getFlatStudy(ticket, gateway.getStudyId());
                if(s!=null){
                    checkFlatStudyCodeFolders(ticket, s.getId());
                    Folder studyDataFolder = EJBLocator.lookupStorageBean().getFolder(ticket, s.getDataFolderId());
                    Folder deviceSpecificCodeFolder = StorageUtils.getOrCreateFolderPath(ticket, studyDataFolder, FlatStudy.CODE_FOLDER_NAME + "/" + gateway.getGatewayType());
                } else {
                    throw new ConnexienceException("Gateway study does not exist");
                }
            }
        }
    }
    
    @Override
    public FlatGateway saveFlatGateway(Ticket ticket, FlatGateway gateway) throws ConnexienceException {
        if(gateway.getId()!=null){
            FlatGateway existing = em.merge(gateway);
            return existing;
        } else {
            em.persist(gateway);
            return gateway;
        }
    }

    @Override
    public void moveFlatGateway(Ticket ticket, Integer gatewayId, Integer targetStudyId) throws ConnexienceException {
        
        // Get the relevant object
        /*
        FlatGateway gateway = EJBLocator.lookupFlatStudyBean().getFlatGateway(ticket, gatewayId);
        FlatStudy sourceStudy = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, gateway.getStudyId());
        FlatStudy targetStudy = EJBLocator.lookupFlatStudyBean().getFlatStudy(ticket, targetStudyId);
        
        // Get the owner ticket for the study that the gateway is being moved to
        Ticket targetStudyTicket = EJBLocator.lookupTicketBean().createStudyTicket(targetStudy.getId());
              
        
        // Set the study code in the gateway
        
        // Find the data folder
        Folder gatewayFolder = EJBLocator.lookupStorageBean().getFolder(ticket, gateway.getFolderId());
        
        // Create a new container folder for the data folder
        Folder targetStudyFolder = EJBLocator.lookupStorageBean().getFolder(targetStudyTicket, targetStudy.getDataFolderId());
        Folder targetContainerFolder = createContainerForFlatStudyObject(targetStudyTicket, targetStudyFolder, "data/gateways", gateway.getCreationDate());
        
        // Move the data there
        gatewayFolder.setContainerId(targetContainerFolder.getId());
        gatewayFolder = EJBLocator.lookupStorageBean().updateFolder(ticket, gatewayFolder);
        gateway.setStudyId(targetStudy.getId());
        gateway = saveFlatGateway(ticket, gateway);
        
        // Change owner ID to the ID of the owner of the target study
        */
        throw new ConnexienceException("Not implemented yet");
    }

    
    @Override
    public void removeFlatGateway(Ticket ticket, Integer gatewayId) throws ConnexienceException {
        FlatGateway g = getFlatGateway(ticket, gatewayId);
        if(g!=null){
            FlatStudy s = getFlatStudy(ticket, g.getStudyId());
            if(s!=null){
                if(projectBean.isProjectAdmin(ticket, s)){
                    // Delete the gateway data folders
                    EJBLocator.lookupStorageBean().removeFolderTree(ticket, g.getFolderId());
                    
                    // Now delete the gateway
                    Query q = em.createNamedQuery("FlatGateway.deleteById");
                    q.setParameter("id", gatewayId);
                    q.executeUpdate();                    
                } else {
                    throw new ConnexienceException(ConnexienceException.ACCESS_DENIED_MESSAGE);
                }
            } else {
                // Hanging device
                EJBLocator.lookupStorageBean().removeFolderTree(ticket, g.getFolderId());                
                Query q = em.createNamedQuery("FlatGateway.deleteById");
                q.setParameter("id", gatewayId);
                q.executeUpdate();
            }
        }
    }

    @Override
    public long getObjectCount(Ticket ticket, Integer studyId, Class objectClass) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            String queryName = objectClass.getSimpleName() + ".countForStudy";
            Query q = em.createNamedQuery(queryName, Long.class);
            q.setParameter("studyId", studyId);
            return (Long)q.getSingleResult();
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }
    }

    @Override
    public long getObjectCount(Ticket ticket, Integer studyId, Class objectClass, String searchText) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            String queryName = objectClass.getSimpleName() + ".countMatchingForStudy";
            Query q = em.createNamedQuery(queryName, Long.class);
            q.setParameter("studyId", studyId);
            q.setParameter("search", "%" + searchText + "%");
            return (Long)q.getSingleResult();
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }
    }

    @Override
    public List getObjects(Ticket ticket, Integer studyId, Class objectClass, String searchText, int startPosition, int pageSize) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            String queryName = objectClass.getSimpleName() + ".searchForStudy";
            Query q = em.createNamedQuery(queryName);
            q.setParameter("studyId", studyId);
            q.setParameter("search", "%" + searchText + "%");
            q.setFirstResult(startPosition);
            if(pageSize!=-1){
                q.setMaxResults(pageSize);
            }
            
            return q.getResultList();
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }     
    }
    
    @Override
    public List getObjects(Ticket ticket, Integer studyId, Class objectClass, int startPosition, int pageSize) throws ConnexienceException {
        FlatStudy s = getFlatStudy(ticket, studyId);
        if(s!=null){
            String queryName = objectClass.getSimpleName() + ".listForStudy";
            Query q = em.createNamedQuery(queryName);
            q.setParameter("studyId", studyId);
            q.setFirstResult(startPosition);
            if(pageSize!=-1){
                q.setMaxResults(pageSize);
            }
            
            return q.getResultList();
        } else {
            throw new ConnexienceException("No such study: " + studyId);
        }      
    }

    @Override
    public void sendFlatStudyEventMessage(Ticket ticket, String message, HashMap<String, Object> extraParameters) throws ConnexienceException {
        javax.jms.Connection connection = null;
        try {
            connection = JMSProperties.isUser() ? connectionFactory.createConnection(JMSProperties.getUsername(), JMSProperties.getPassword()) : connectionFactory.createConnection();
            javax.jms.Session jmsSession = connection.createSession(true, javax.jms.Session.SESSION_TRANSACTED);
            Topic topic = jmsSession.createTopic("StudyEvents");

            MessageProducer publisher = jmsSession.createProducer(topic);
            publisher.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            // Create the message
            TextMessage tm = jmsSession.createTextMessage();
            tm.setText(message);
            if(extraParameters!=null){
                for(String key : extraParameters.keySet()){
                    tm.setObjectProperty(key, extraParameters.get(key));
                }
            }
            publisher.setTimeToLive((10 * 1000));
            publisher.send(tm);
            
        } catch (Exception e){
            throw new ConnexienceException("Error sending event message: " + e.getMessage(), e);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println("Error closing JMS connection: " + e.getMessage());
            }
        }        
    }
}
