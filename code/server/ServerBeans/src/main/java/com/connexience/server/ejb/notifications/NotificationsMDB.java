/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.notifications;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.HibernateSessionContainer;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.notifcations.ResetPasswordNotification;
import com.connexience.server.model.notifcations.TextMessageNotification;
import com.connexience.server.model.notifcations.WorkflowNotification;
import com.connexience.server.model.security.Ticket;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 * Created by IntelliJ IDEA.
 * User: martyn
 * Date: 18-Nov-2009
 * Time: 14:14:03
 */

@MessageDriven(activationConfig =
        {
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/notifications")
        }, mappedName = "queue/notifications")
public class NotificationsMDB extends HibernateSessionContainer implements MessageListener {
    public void onMessage(Message message) {
        try {
            ObjectMessage objectMessage = (ObjectMessage) message;
            Object notification = objectMessage.getObject();

            if (notification instanceof TextMessageNotification) {
                sendTextMessageNotification((TextMessageNotification) notification);
            } else if (notification instanceof WorkflowNotification) {
                //do nothing
            } else if (notification instanceof ResetPasswordNotification) {
                resetPassword((ResetPasswordNotification) notification);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendTextMessageNotification(TextMessageNotification notification) throws ConnexienceException {
        try {

            if (EJBLocator.lookupNotificationsBean().getNotificationValue(NotificationsBean.EMAIL_ON_MESSAGE_RECIEVE, notification.getUserId())) {
                EJBLocator.lookupSMTPBean().sendTextMessageReceivedEmail(notification.getTicket(), notification.getUserId(), notification.getTextMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetPassword(ResetPasswordNotification notification) throws ConnexienceException {
        try {
            //Get an admin ticket to change the password
            Ticket adminTicket = getInternalTicket();

            //Generate a code for this user
            String code = EJBLocator.lookupUserDirectoryBean().addPasswordRecoveryCode(adminTicket, notification.getUserId());

            String websiteURL = notification.getWebsiteURL();
            //Send the email
            EJBLocator.lookupSMTPBean().sendResetPasswordEmail(notification.getTicket(), notification.getUserId(), code, websiteURL);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
