/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.preferences;

import com.connexience.server.util.InternalProvenanceLoggerClient;
import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.performance.client.PerformanceLoggerClient;
import com.connexience.server.ejb.preferences.defaults.AccessControlDefaults;
import com.connexience.server.ejb.preferences.defaults.StudyManagementDefaults;
import com.connexience.server.ejb.preferences.defaults.WebsiteDefaults;
import com.connexience.server.ejb.storage.EventStoreDefaults;
import com.connexience.server.util.ZipUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.security.auth.x500.X500Principal;
import org.apache.log4j.Logger;
import org.bouncycastle.openssl.PEMWriter;

import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.prefs.PreferenceManager;
/**
 * This class loads preferences using the preference manager
 * @author hugo
 */
@Startup
@Singleton
public class PreferenceStore {
    private static Logger logger = Logger.getLogger(PreferenceStore.class);
    private int keySize = 512;
    private String macAddress = "";
    private boolean storageEnabled = false;
    private PublicKey jwtPublicKey = null;
    private PrivateKey jwtPrivateKey = null;
    private X509Certificate jwtCertificate = null;
    

    private String macToString(byte[] mac)
    {
        if (mac == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
        }
        return sb.toString();
    }


    @PostConstruct
    public void init(){
        logger.info("Loading server preferences file");
        createDefaultProperties();
        if (!PreferenceManager.loadPropertiesFromHomeDir(".inkspot", "server.xml")) {
            createDefaultProperties();
            PreferenceManager.saveProperties();
        } else {
            PreferenceManager.saveProperties();
        }

        logger.info("Determining MAC address...");
        macAddress = null;
        // Try the usual way...
        try {
            InetAddress ip = InetAddress.getLocalHost();
            logger.info("Using IP address: " + ip.getHostAddress() + " to determine MAC");
            NetworkInterface intf = NetworkInterface.getByInetAddress(ip);
            if (intf != null) {
                byte[] mac = intf.getHardwareAddress();
                macAddress = macToString(mac);
            }
        } catch (UnknownHostException | SocketException x) {
            logger.warn("Error determining the MAC address", x);
        }

        // If the usual way fails, pick whatever MAC you can find...
        if (macAddress == null) {
            logger.warn("Cannot get the MAC address based on the local host name. Trying another way...");
            try {
                Enumeration<NetworkInterface> intfs = NetworkInterface.getNetworkInterfaces();
                while (intfs.hasMoreElements() && macAddress == null) {
                    NetworkInterface intf = intfs.nextElement();
                    try {
                        byte[] mac = intf.getHardwareAddress();
                        macAddress = macToString(mac);
                        logger.info("Using MAC of interface: " + intf);
                    } catch (SocketException x) {
                        logger.warn("Cannot access MAC address of interface: " + intf, x);
                    }
                }
            } catch (SocketException x) {
                logger.error("Cannot ", x);
            }
        }

        if (macAddress != null) {
            logger.info("MAC: " + macAddress);
        } else {
            macAddress = "";
            logger.error("Unable to find any hardware interface with a MAC address. Check you network configuration -- storage will not work properly.");
        }
        
        // Load the keys
        File privateKeyFile = PreferenceManager.getFileFromHomeDir(".inkspot", "PrivateJWTKey.pem");
        File publicKeyFile = PreferenceManager.getFileFromHomeDir(".inkspot", "PublicJWTKey.pem");
        File certificateFile = PreferenceManager.getFileFromHomeDir(".inkspot", "JWTCertificate.crt");
        
        if(privateKeyFile.exists() && publicKeyFile.exists()){
            if(!loadKeys(publicKeyFile, privateKeyFile, certificateFile)){
                logger.info("Creating new JWT Signing KeyPair [LOADERROR]");
                createKeys(publicKeyFile, privateKeyFile, certificateFile);
            } else {
                logger.info("Loaded JWT Signing KeyPair");
            }
        } else {
            logger.info("Creating new JWT SigningPair");
            createKeys(publicKeyFile, privateKeyFile, certificateFile);
        }
        
    }

    public PublicKey getJwtPublicKey() {
        return jwtPublicKey;
    }

    public PrivateKey getJwtPrivateKey() {
        return jwtPrivateKey;
    }
    
    private boolean loadKeys(File publicKeyFile, File privateKeyFile, File certificateFile){
        try {
            String encodedJwtPrivateKey = ZipUtils.readFileIntoString(privateKeyFile);
            String encodedJwtPublicKey = ZipUtils.readFileIntoString(publicKeyFile);
            
            byte[] privateBytes = Base64.getDecoder().decode(encodedJwtPrivateKey.trim());
            byte[] publicBytes = Base64.getDecoder().decode(encodedJwtPublicKey.trim());
            
            KeyFactory factory = KeyFactory.getInstance("RSA");
            
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateBytes);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicBytes);
            
            jwtPrivateKey = factory.generatePrivate(privateKeySpec);
            jwtPublicKey = factory.generatePublic(publicKeySpec);
            jwtCertificate = generateSelfSignedX509Certificate(jwtPublicKey, jwtPrivateKey);
            
            if(!certificateFile.exists()){
                writeCertificateToFile(jwtCertificate, certificateFile);
            }
            return true;
                    
        } catch (Exception e){
            jwtPrivateKey = null;
            jwtPublicKey = null;
            logger.error("Error loading keys: " + e.getMessage(), e);
            return false;
        }
    }

    private void createKeys(File publicKeyFile, File privateKeyFile, File certificateFile){
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(keySize);
            KeyPair kp = generator.genKeyPair();
            
            String encodedPrivate = Base64.getEncoder().encodeToString(kp.getPrivate().getEncoded());
            X509EncodedKeySpec x509Spec = new X509EncodedKeySpec(kp.getPublic().getEncoded());
            String encodedPublic = Base64.getEncoder().encodeToString(x509Spec.getEncoded());
            ZipUtils.writeSingleLineFile(publicKeyFile, encodedPublic);
            ZipUtils.writeSingleLineFile(privateKeyFile, encodedPrivate);
            jwtPrivateKey = kp.getPrivate();
            jwtPublicKey = kp.getPublic();
            jwtCertificate = generateSelfSignedX509Certificate(jwtPublicKey, jwtPrivateKey);
            writeCertificateToFile(jwtCertificate, certificateFile);
        } catch (Exception e){
            logger.error("Cannot generate keys: " + e.getMessage(), e);
        }
    }
    
    private void writeCertificateToFile(X509Certificate cert, File certificateFile) throws IOException {
        try(PrintWriter writer = new PrintWriter(certificateFile)){
            try(PEMWriter pemWriter = new PEMWriter(writer)){
                pemWriter.writeObject(cert);
            }
        }
    }
    
    @PreDestroy
    public void terminate(){
        if(PreferenceManager.isLoadPerformed()){
            logger.info("Saving server preferences file");
            saveProperties();
        }
    }
    
    public String getMacAddress(){
        return macAddress;
    }
    
    public void saveProperties(){
        PreferenceManager.saveProperties();
    }
    
    public XmlDataStore getPropertyGroup(String groupName) {
        return PreferenceManager.getSystemPropertyGroup(groupName);
    }
    
    public List<String> listPropertyGroupNames(){
        return PreferenceManager.getSystemPropertyGroupNames();
    }
    
    public XmlDataStore getAllProperties() throws Exception {
        return PreferenceManager.getAllProperties();
    }

    public void setStorageEnabled(boolean storageEnabled) {
        this.storageEnabled = storageEnabled;
    }

    public boolean isStorageEnabled() {
        return storageEnabled;
    }
    
    private void createDefaultProperties(){
        // Scheduler properties
        SchedulerBean.createDefaultProperties();
        
        // Performance logging
        PerformanceLoggerClient.createDefaultProperties();
        
        // Provenance capture
        InternalProvenanceLoggerClient.createDefaultProperties();
        
        // Access control defaults
        AccessControlDefaults.createDefaultProperties();
        
        // Study management
        StudyManagementDefaults.createDefaultProperties();
        
        // Website
        WebsiteDefaults.createDefaultProperties();
        
        // ActiveMQ Events
        EventStoreDefaults.createDefaultProperties();
    }
    
    public X509Certificate generateSelfSignedX509Certificate(PublicKey publicKey, PrivateKey privateKey) throws Exception {

        // yesterday
        Date validityBeginDate = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
        // in 20 years
        Calendar c = Calendar.getInstance();
        c.setTime(validityBeginDate);
        c.add(Calendar.YEAR, 20);
        Date validityEndDate = c.getTime();

        // GENERATE THE X509 CERTIFICATE
        X509V1CertificateGenerator certGen = new X509V1CertificateGenerator();
        X500Principal dnName = new X500Principal("CN=Inkspot Science JWT");
       
        certGen.setSerialNumber(BigInteger.valueOf(System.currentTimeMillis()));
        certGen.setSubjectDN(dnName);
        certGen.setIssuerDN(dnName); // use the same
        certGen.setNotBefore(validityBeginDate);
        certGen.setNotAfter(validityEndDate);
        certGen.setPublicKey(publicKey);
        certGen.setSignatureAlgorithm("SHA256WithRSAEncryption");

        X509Certificate cert = certGen.generate(privateKey, "BC");
        return cert;
    }    
}
