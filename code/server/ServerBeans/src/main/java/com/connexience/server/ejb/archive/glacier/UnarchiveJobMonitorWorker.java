/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.archive.glacier;

import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.archive.GlacierArchiveStore;
import com.connexience.server.model.document.DocumentRecord;
import org.apache.log4j.Logger;

import java.util.List;

public class UnarchiveJobMonitorWorker extends Thread
{
    private static final Logger logger = Logger.getLogger(UnarchiveJobMonitorWorker.class.getName());

//    private static final long DEFAULT_TIMEOUT = 120 * 60 * 1000; // 120min
    private static final long DEFAULT_TIMEOUT = 20 * 60 * 1000; // TODO: Remove after debugging

    private volatile boolean done;
    private long             timeout = DEFAULT_TIMEOUT;

    private String archiveStoreId;
    private String accessKey;
    private String secretKey;
    private String domainName;
    private String vaultName;

    public UnarchiveJobMonitorWorker(String name, GlacierArchiveStore glacierArchiveStore)
    {
        super(name);

        archiveStoreId = glacierArchiveStore.getId();
        accessKey      = glacierArchiveStore.getAccessKey();
        secretKey      = glacierArchiveStore.getSecretKey();
        domainName     = glacierArchiveStore.getDomainName();
        vaultName      = glacierArchiveStore.getVaultName();
    }

    public void run()
    {
        logger.info("Unarchive Job Monitor Worker: run, start");

        try
        {
            done = false;
            while (! done)
            {
                // Look for downloads without active jobs
                try
                {
                    AmazonGlacierClient amazonGlacierClient = ClientUtils.obtainGlacierClient(accessKey, secretKey, domainName);
                    try
                    {
                        boolean anyPossibleDownloadJobsExist = DownloadUtils.anyPossibleDownloadJobsExist(amazonGlacierClient, vaultName);
                        if (! anyPossibleDownloadJobsExist)
                        {
                            List<DocumentRecord> orphanUnarchivingDocumentRecords = EJBLocator.lookupArchiveMapBean().getOrphanUnarchivingDocumentRecord(archiveStoreId);

                            for (DocumentRecord orphanUnarchivingDocumentRecord: orphanUnarchivingDocumentRecords)
                            {
                                logger.info("Unarchive Job Monitor Worker: Marking as failed Unarchiving of " + orphanUnarchivingDocumentRecord.getId());

                                EJBLocator.lookupArchiveMapBean().updateDocumentRecordToUnarchivingFailed(orphanUnarchivingDocumentRecord);
                            }
                        }
                    }
                    finally
                    {
                        ClientUtils.shutdownGlacierClient(amazonGlacierClient);
                    }

                    if (! done)
                    {
                        logger.info("Unarchive Job Monitor Worker: run, sleeping");
                        Thread.sleep(timeout);
                    }
                }
                catch (InterruptedException interruptedException)
                {
                }
                catch (Throwable throwable)
                {
                    logger.error("Unarchive Job Monitor Worker: Loop problem " + throwable);
                }
            }
        }
        catch (Throwable throwable)
        {
            logger.error("Unarchive Job Monitor Worker: Problem " + throwable);
        }

        logger.info("Unarchive Job Monitor Worker: run, end");
    }

    public void shutdown()
        throws InterruptedException
    {
        logger.debug("Unarchive Job Monitor Worker: shutdown, start");
        done = true;
        interrupt();
        join();
        logger.debug("Unarchive Job Monitor Worker: shutdown, end");
    }

    public long getTimeout()
    {
        return timeout;
    }

    public void setTimeout(long timeout)
    {
        this.timeout = timeout;
    }
}
