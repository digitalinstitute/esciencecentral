/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.ejb.scheduler.tasks;

import com.connexience.server.ejb.scheduler.SchedulerBean;
import com.connexience.server.ejb.scheduler.SchedulerTask;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.storage.DataStore;
import com.connexience.server.util.StorageUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import java.util.List;

/**
 * This task periodically scans the database for documents which don't have the
 * timestamp of the latest version
 * @author simon
 */
public class AddCurrentVersionTimestamp extends SchedulerTask {
    Logger logger = Logger.getLogger(AddCurrentVersionTimestamp.class);
    private Ticket adminTicket = null;

    public AddCurrentVersionTimestamp(SchedulerBean parentBean) {
        super(parentBean);
        setEnabled(true);
        setName("AddCurrentVersionTimestamp");
        setRepeating(false);
        setStartDelayed(false);
    }

    @Override
    public void run() {
        logger.info("Running AddCurrentVersionTimestamp task");
        if (parentBean.defaultOrganisationExists()) {
            if (adminTicket == null) {
                try {
                    adminTicket = parentBean.getDefaultOrganisationAdminTicket();
                } catch (Exception e) {
                    logger.error("Error creating admin ticket: " + e.getMessage());
                }
            }

            if (adminTicket != null) {
                Session session = null;
                try {
                    session = parentBean.getSession();
                    Query q = session.createQuery("FROM DocumentRecord AS doc WHERE doc.currentVersionTimestamp = 0");
                    List<DocumentRecord> results = q.list();
                    logger.info("Found " + results.size() + " DocumentRecords without timestamp.");


                    for (DocumentRecord doc : results) {

                        logger.info("Found " + doc.getName() + " without CurrentVersionTimestamp");
                        Query latestVersionQuery = session.createQuery("FROM DocumentVersion AS dv " +
                                "WHERE dv.documentRecordId = :docid AND dv.versionNumber = :versionNum");
                        latestVersionQuery.setString("docid", doc.getId());
                        latestVersionQuery.setInteger("versionNum", doc.getCurrentVersionNumber());

                        List<DocumentVersion> latestVersions = latestVersionQuery.list();
                        if (latestVersions.size() > 0) {
                            DocumentVersion latestVersion = latestVersions.get(0);
                            if (latestVersion != null) {
                                logger.info("Found latest version of " + doc.getName() + ". Setting currentVersionTimestamp to " + latestVersion.getTimestamp());

                                Query setTimeQuery = session.createQuery("UPDATE DocumentRecord AS dr " +
                                        " SET dr.currentVersionTimestamp = :timestamp " +
                                        "WHERE dr.id = :docid");
                                setTimeQuery.setString("docid", doc.getId());
                                setTimeQuery.setLong("timestamp", latestVersion.getTimestamp());
                                setTimeQuery.executeUpdate();
                            }
                        }
                        else{
                            logger.error("Cannot find version for document: " + doc.getName());
                        }
                    }
                } catch (Exception e) {
                    logger.error("Error in loop adding timestamps to current version: " + e.getMessage());
                }

                parentBean.closeSession(session);

            } else {
                logger.error("No admin ticket available in CurrentVersionTimestamp task");
            }
        }
    }
}