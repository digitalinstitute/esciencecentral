UPDATE objectsflat
SET currentversiontimestamp = 0
WHERE (objecttype = 'DOCUMENTRECORD'
OR objecttype = 'WORKFLOWDOCUMENTRECORD'
OR objecttype = 'DYNAMICSERVICES'
OR objecttype = 'DYNAMICLIBRARIES') AND currentversiontimestamp IS NULL;
