-- Add start and end dates to the logger deployments

UPDATE loggerdeployments  AS ld
SET
  startdate = p.startdate,
  enddate = p.enddate
FROM
  projects  p
WHERE
  ld.study_id = p.id
  AND
  ld.enddate is null