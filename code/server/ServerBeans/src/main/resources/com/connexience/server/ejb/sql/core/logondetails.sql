update logondetails set enabled = true where enabled is null;
update logondetails set failurecount = 0 where failurecount is null;
update logondetails set creationtime = CURRENT_TIMESTAMP where creationtime is null;
update logondetails set lockreason = '' where lockreason is null;
update logondetails set lastattempttime = CURRENT_TIMESTAMP where lastattempttime is null;