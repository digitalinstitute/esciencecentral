update remotefilesystemscanners set autoscan  = false where autoscan is null;
update remotefilesystemscanners set scaninterval  = 3600 where scaninterval is null;
update remotefilesystemscanners set enabled  = false where enabled is null;
update remotefilesystemscanners set deleteuploaded  = false where deleteuploaded is null;
update remotefilesystemscanners set autoworkflow  = false where autoworkflow is null;
update remotefilesystemscanners set studyscanner  = false where studyscanner is null;
update remotefilesystemscanners set studyid  = 0 where studyid is null;
update remotefilesystemscanners set importexportseparation  = false where importexportseparation is null;
update remotefilesystemscanners set exportdatasuffix  = 'export' where exportdatasuffix is null;
update remotefilesystemscanners set importdatasuffix  = 'import' where importdatasuffix is null;