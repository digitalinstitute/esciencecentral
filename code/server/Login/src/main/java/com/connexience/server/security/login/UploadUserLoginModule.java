/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.security.login;

import com.connexience.server.util.HexUtils;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.acl.Group;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.naming.InitialContext;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

/**
 * This class provides a login module that uses the upload users to authenticate
 * a user. It returns a list of project IDs that the user is allowed to view.
 * Tickets can then be created to access specific projects.
 * @author hugo
 */
public class UploadUserLoginModule extends UsernamePasswordLoginModule {

    @Override
    protected String getUsersPassword() throws LoginException {
        Principal p = getIdentity();
        if(p!=null){
            Connection c = null;
            PreparedStatement s = null;
            ResultSet r = null;
            try {
                c = getSQLConnection();
                s = c.prepareStatement("SELECT * FROM uploaders WHERE LOWER(username)=?");
                s.setString(1, p.getName().toLowerCase());
                r = s.executeQuery();
                if(r.next()){
                    return r.getString("hashedpassword");
                } else {
                    throw new LoginException("No such user");
                }
                
            } catch (Exception e){
                throw new LoginException("Error logging in: " + e.getMessage());
            } finally {
                try {if(r!=null)r.close();}catch(Exception ignored){}
                try {if(s!=null)s.close();}catch(Exception ignored){}
                try {if(c!=null)c.close();}catch(Exception ignored){}
            }
                    
        } else {
            throw new LoginException("No identity available");
        }
    }

    @Override
    protected Group[] getRoleSets() throws LoginException {
        SimpleGroup group = new SimpleGroup("Roles");
        group.addMember(new SimplePrincipal("Users"));
        /*
        Principal p = getIdentity();
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;

        try {
            c = getSQLConnection();
            s = c.prepareStatement("select projects_id from projects_uploaders where uploaders_id in(select id from uploaders where LOWER(username)=?)");
            s.setString(1, p.getName().toLowerCase());
            r = s.executeQuery();
            while(r.next()){
                group.addMember(new SimplePrincipal(Integer.toString(r.getInt("projects_id"))));
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {if(r!=null)r.close();}catch(Exception ignored){}
            try {if(s!=null)s.close();}catch(Exception ignored){}
            try {if(c!=null)c.close();}catch(Exception ignored){}
        }
        */
        
        return new Group[] { group };
    }

    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {
        try{
            
            return SecureHashUtils.check(inputPassword, expectedPassword);
        } catch (Exception e){
            return false;
        }
    }
        
    
    /** Get a database connection */
    private Connection getSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ConnexienceDB");
        return source.getConnection();
    }        
}
