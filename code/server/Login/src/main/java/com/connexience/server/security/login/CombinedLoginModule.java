/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.security.login;

import com.connexience.server.model.jaas.EscJaaSGroup;
import com.connexience.server.model.jaas.EscJaaSPrincipal;
import com.connexience.server.util.HexUtils;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.acl.Group;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.naming.InitialContext;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import org.jboss.logging.Logger;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

/**
 * This login module can authenticate against both the internal and upload
 * users databases and return the correct type of principal depending on
 * where a users is stored.
 * @author hugo
 */
public class CombinedLoginModule extends UsernamePasswordLoginModule {
    private static final Logger logger = Logger.getLogger(CombinedLoginModule.class);
    
    @Override
    protected Principal getIdentity() {
        Principal p = super.getIdentity();
        try {
            EscJaaSPrincipal.UserType userType = getUserType(p.getName());
            EscJaaSPrincipal escPrincipal = new EscJaaSPrincipal(p.getName(), userType);
            return escPrincipal;
        } catch (Exception e){
            logger.error("Error getting identity data: " + e.getMessage());
            return p;
        }
    }

    @Override
    protected boolean validatePassword(String inputPassword, String expectedPassword) {
        try{
            Principal p = getIdentity();
            if(p instanceof EscJaaSPrincipal && ((EscJaaSPrincipal)p).getUserType()==EscJaaSPrincipal.UserType.UPLOAD_USER){
                // This is an upload user
                return SecureHashUtils.check(inputPassword, expectedPassword);
            } else {
                // Use standard e-SC hash
                return hashPassword(inputPassword).equals(expectedPassword);
            }       
        } catch (Exception e){
            return false;
        }
    }
    
    @Override
    protected String getUsersPassword() throws LoginException {
        Principal p = getIdentity();
        if(p!=null){
            try {
                EscJaaSPrincipal.UserType userType = getUserType(p.getName());
                if(userType==EscJaaSPrincipal.UserType.ESC_USER){
                    return getEscUserPassword(p.getName());
                } else if(userType==EscJaaSPrincipal.UserType.UPLOAD_USER){
                    return getUploadUserPassword(p.getName());
                } else {
                    throw new LoginException("No login details");
                }
            } catch (Exception e){
                throw new LoginException("Cannot get user type: " + e.getMessage());
            }        
        } else {
            throw new LoginException("Identity not available");
        }
    }

    @Override
    protected Group[] getRoleSets() throws LoginException {
        Principal p = getIdentity();
        if(p instanceof EscJaaSPrincipal){
            EscJaaSPrincipal escPrincipal = (EscJaaSPrincipal)p;
            if(escPrincipal.getUserType()==EscJaaSPrincipal.UserType.ESC_USER){
                try {
                    return getEscUserRoles(p.getName());
                } catch (Exception e){
                    throw new LoginException("Error getting Esc user roles: " + e.getMessage());
                }
            } else if(escPrincipal.getUserType()==EscJaaSPrincipal.UserType.UPLOAD_USER){
                return getUploadUserRoles();
            } else {
                return new Group[0];
            }
        } else {
            // Get database roles as a fallback
            try {
                return getEscUserRoles(p.getName());
            } catch (Exception e){
                throw new LoginException("Error getting Esc user roles: " + e.getMessage());
            }
        }
    }
    
    /** Get the roles of a local user */
    private Group[] getEscUserRoles(String logonName) throws Exception {
        // select name from objectsflat where id in(select groupid from groupmembership where userid='8a808282328c961d01328c9a99130000')
        EscJaaSGroup group = new EscJaaSGroup("Roles", EscJaaSGroup.GroupType.USERS);
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        
        try {
            c = getSQLConnection();
            s = c.prepareStatement("select name from objectsflat where id in(select groupid from groupmembership where userid in (select userid from logondetails where LOWER(logonname)=?))");
            s.setString(1, logonName.toLowerCase());
            r = s.executeQuery();
            while(r.next()){
                group.addMember(new EscJaaSGroup(r.getString("name"), EscJaaSGroup.GroupType.USERS));
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally {
            try {if(r!=null)r.close();}catch(Exception ignored){}
            try {if(s!=null)s.close();}catch(Exception ignored){}
            try {if(c!=null)c.close();}catch(Exception ignored){}
        }
        
        return new Group[] { group };      
    }
    
    private Group[] getUploadUserRoles(){
        /*
        EscJaaSGroup group = new EscJaaSGroup("Roles", EscJaaSGroup.GroupType.UPLOAD_USERS);
        group.addMember(new SimpleGroup("UploadUsers"));
        */
        SimpleGroup group = new SimpleGroup("Roles");
        group.addMember(new SimplePrincipal("UploadUsers"));        
        return new Group[]{group};
    }
    
    /** Get a local user password */
    private String getEscUserPassword(String logonName) throws Exception {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("SELECT * FROM logondetails WHERE LOWER(logonname)=?");
            s.setString(1, logonName.toLowerCase());
            r = s.executeQuery();
            if(r.next()){
                return r.getString("hashedpassword");
            } else {
                throw new LoginException("No such user");
            }

        } catch (Exception e){
            throw new LoginException("Error logging in: " + e.getMessage());
        } finally {
            try {if(r!=null)r.close();}catch(Exception ignored){}
            try {if(s!=null)s.close();}catch(Exception ignored){}
            try {if(c!=null)c.close();}catch(Exception ignored){}
        }
    }
    
    /** Get an upload user password */
    private String getUploadUserPassword(String logonName) throws Exception {
        Connection c = null;
        PreparedStatement s = null;
        ResultSet r = null;
        try {
            c = getSQLConnection();
            s = c.prepareStatement("SELECT * FROM uploaders WHERE LOWER(username)=?");
            s.setString(1, logonName.toLowerCase());
            r = s.executeQuery();
            if(r.next()){
                return r.getString("hashedpassword");
            } else {
                throw new LoginException("No such user");
            }

        } catch (Exception e){
            throw new LoginException("Error logging in: " + e.getMessage());
        } finally {
            try {if(r!=null)r.close();}catch(Exception ignored){}
            try {if(s!=null)s.close();}catch(Exception ignored){}
            try {if(c!=null)c.close();}catch(Exception ignored){}
        }
    }
    
    /** Work out whether a user is in the internal database or is an upload user */
    private EscJaaSPrincipal.UserType getUserType(String logonName) throws Exception {
        EscJaaSPrincipal.UserType userType = EscJaaSPrincipal.UserType.UNKNOWN;
        
        try (Connection c = getSQLConnection()){
            try (PreparedStatement s = c.prepareStatement("SELECT * FROM logondetails WHERE LOWER(logonname)=?")){
                s.setString(1, logonName.toLowerCase());
                try (ResultSet r = s.executeQuery()){
                    if(r.next()){
                        userType = EscJaaSPrincipal.UserType.ESC_USER;
                    }
                }
            }
        }
        
        // If anything found then return
        if(userType!=EscJaaSPrincipal.UserType.UNKNOWN){
            return userType;
        }
        
        try (Connection c = getSQLConnection()){
            try (PreparedStatement s = c.prepareStatement("SELECT * FROM uploaders WHERE LOWER(username)=?")){
                s.setString(1, logonName.toLowerCase());
                try (ResultSet r = s.executeQuery()){
                    if(r.next()){
                        userType = EscJaaSPrincipal.UserType.UPLOAD_USER;
                    }
                }
            }
        }
        // If anything found then return
        if(userType!=EscJaaSPrincipal.UserType.UNKNOWN){
            return userType;
        }
        
        // If we get this far, there isn't a user
        return EscJaaSPrincipal.UserType.NO_USER;
    }
        
    /** Get a database connection */
    private Connection getSQLConnection() throws Exception {
        InitialContext ctx = new InitialContext();
        DataSource source = (DataSource) ctx.lookup("java:jboss/datasources/ConnexienceDB");
        return source.getConnection();
    }      

    /** Hash a password in the same way as the user directory bean */
    private String hashPassword(String plainPassword) throws Exception {
        //compute the SHA-1 hash of the password
        MessageDigest msgDigest = MessageDigest.getInstance("SHA-1");
        msgDigest.update(plainPassword.getBytes());
        byte rawByte[] = msgDigest.digest();
        return HexUtils.getHexString(rawByte);
    }        
}