package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Hub;
import com.connexience.server.model.project.study.HubType;

import java.util.ArrayList;
import java.util.List;

public class WebsiteHubTypeMin
{
	private Integer id;

	private String name;

	private String dataFolderId;


	protected WebsiteHubTypeMin()
	{
	}

	public WebsiteHubTypeMin(HubType hubType)
	{
		this.setId(hubType.getId());
		this.name = hubType.getName();
		this.dataFolderId = hubType.getDataFolderId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataFolderId() {
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId) {
		this.dataFolderId = dataFolderId;
	}
}
