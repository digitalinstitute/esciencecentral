package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.rest.model.project.study.WebsiteHub;
import com.connexience.server.rest.model.project.study.WebsiteHubDeployment;
import com.connexience.server.rest.model.project.study.WebsiteHubType;
import com.connexience.server.rest.model.project.study.WebsiteLoggerDeployment;

import javax.ejb.Local;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/logger
 * <p/>
 * User: nsjw7
 * Date: 30/11/2012
 * Time: 09:00
 */
@Local
public interface IHubAPI {

    @GET
    @Path("/{studyId}/{groupId}/deployments")
    @Produces("application/json")
    Collection<WebsiteHubDeployment> getSubjectGroupHubDeployments(@PathParam("studyId") int studyId, @PathParam("groupId") int groupId, @DefaultValue("false") @QueryParam("includeInactive") Boolean includeInactive) throws ConnexienceException;

    @POST
    @Path("/{studyId}/{subjectGroupId}/deployment")
    @Consumes("application/json")
    @Produces("application/json")
    WebsiteHubDeployment saveHubDeployment(@PathParam("studyId") int studyId, @PathParam("subjectGroupId") int subjectGroupId, WebsiteHubDeployment websiteHubDeployment) throws ConnexienceException;

    @POST
    @Path("/deployment/{deploymentId}/remove")
    @Produces("application/json")
    Response removeHubDeployment(@PathParam("deploymentId") int deploymentId) throws ConnexienceException;

    @GET
    @Path("/hubs")
    @Produces("application/json")
    List<WebsiteHub> getHubs() throws ConnexienceException;

    @GET
    @Path("/types")
    @Produces("application/json")
    List<WebsiteHubType> getHubTypes() throws ConnexienceException;

}