package com.connexience.server.rest.model;

/**
 * Created by markturner on 21/02/2016.
 */
public class WebsiteWorkflowBlockMin extends WebsiteObject {

    private String name;
    private String category;
    private String description;
    private WebsiteUserMin creator;

    public WebsiteWorkflowBlockMin(){

    }

    public WebsiteWorkflowBlockMin(String name, String category, String description, String id, WebsiteUserMin creator){
        this.setId(id);
        this.name = name;
        this.category = category;
        this.description = description;
        this.creator = creator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WebsiteUserMin getCreator() {
        return creator;
    }

    public void setCreator(WebsiteUserMin creator) {
        this.creator = creator;
    }
}
