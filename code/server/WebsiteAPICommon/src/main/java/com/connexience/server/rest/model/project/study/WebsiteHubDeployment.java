package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.HubDeployment;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WebsiteHubDeployment {
    private Integer id;

    private Integer subjectGroupId;

    private WebsiteHubMin hub;

    private Date startDate;

    private Date endDate;

    private boolean active = true;

    private Map<String, String> additionalProperties = new HashMap<>();


    public WebsiteHubDeployment() {
    }

    public WebsiteHubDeployment(HubDeployment deployment) {
        this.setId(deployment.getId());
        this.active = deployment.isActive();
        this.hub = new WebsiteHubMin(deployment.getHub());

        // cyclic, use IDs at other side
        this.subjectGroupId = deployment.getSubjectGroup().getId();
        this.startDate = deployment.getStartDate();
        this.endDate = deployment.getEndDate();
        // not cyclic
        for (String key : deployment.getAdditionalProperties().keySet())
        {
            this.additionalProperties.put(key, deployment.getAdditionalProperties().get(key));
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSubjectGroupId() {
        return subjectGroupId;
    }

    public void setSubjectGroupId(Integer subjectGroupId) {
        this.subjectGroupId = subjectGroupId;
    }

    public WebsiteHubMin getHub() {
        return hub;
    }

    public void setHub(WebsiteHubMin hub) {
        this.hub = hub;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }
}
