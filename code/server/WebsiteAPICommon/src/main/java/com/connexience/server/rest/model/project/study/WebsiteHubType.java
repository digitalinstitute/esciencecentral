package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.*;

import java.util.ArrayList;
import java.util.List;

public class WebsiteHubType
{
	private Integer id;

	private String name;

	private String dataFolderId;

	private List<Integer> instanceIds = new ArrayList<>();

	protected WebsiteHubType()
	{
	}

	public WebsiteHubType(HubType hubType)
	{
		this.setId(hubType.getId());
		this.name = hubType.getName();
		this.dataFolderId = hubType.getDataFolderId();

		// cyclic, use IDs at this side
		for (Hub hub: hubType.getInstances())
		{
			this.instanceIds.add(hub.getId());
		}

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDataFolderId() {
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId) {
		this.dataFolderId = dataFolderId;
	}

	public List<Integer> getInstanceIds() {
		return instanceIds;
	}

	public void setInstanceIds(List<Integer> instanceIds) {
		this.instanceIds = instanceIds;
	}
}
