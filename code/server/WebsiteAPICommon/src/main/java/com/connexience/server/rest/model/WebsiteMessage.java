/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * User: nsjw7
 * Date: 06/12/2012
 * Time: 15:35
 */
public class WebsiteMessage extends WebsiteObject {

    public enum MailboxFolder {
        Inbox, Sent, Draft, Trash
    }

    /** Time that this request was made */
    private Date timestamp;

    /** Subject of the message */
    private String subject;

    /** Message is read */
    private Boolean isRead;

    /** Text of the message */
    private String message;

    /** The sender */
    private WebsiteUserMin sender;

    /** The recipient */
    private List<WebsiteUserMin> recipients;

    private String threadId;

    private MailboxFolder folder;

    public WebsiteMessage() {

    }
    
    public WebsiteMessage(Message message, Ticket ticket) throws ConnexienceException {
        
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        this.setId(message.getId());
        this.timestamp = message.getTimestamp();
        this.sender = new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, message.getSenderId()), false, ticket);
        this.message = message.getMessage();

        List<WebsiteUserMin> recipients = new ArrayList<>();

        for(String recipientId : message.getRecipientId().split(" ")){
            if(!recipientId.equals(user.getId())){
                recipients.add(new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, recipientId), false, ticket));
            }
        }

        this.recipients = recipients;

        if(message.getContainerId().equals(user.getInboxFolderId())){
            this.folder = WebsiteMessage.MailboxFolder.Inbox;
        }
        else if(message.getContainerId().equals(user.getSentMessagesFolderId())){
            this.folder = WebsiteMessage.MailboxFolder.Sent;
        }
        else if(message.getContainerId().equals(user.getDraftMessagesFolderId())){
            this.folder = WebsiteMessage.MailboxFolder.Draft;
        }
        else if(message.getContainerId().equals(user.getTrashedMessagesFolderId())){
            this.folder = WebsiteMessage.MailboxFolder.Trash;
        }

        if (message instanceof TextMessage) {
            TextMessage tm = (TextMessage) message;
            this.subject = tm.getTitle();
            this.threadId = tm.getThreadId();
            this.isRead = tm.isRead();
        }
    }

    public WebsiteMessage(Date timestamp, String subject, String message, WebsiteUserMin sender, List<WebsiteUserMin> recipients, String threadId, MailboxFolder folder) {
        this.timestamp = timestamp;
        this.subject = subject;
        this.message = message;
        this.sender = sender;
        this.recipients = recipients;
        this.threadId = threadId;
        this.folder = folder;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public WebsiteUserMin getSender() {
        return sender;
    }

    public void setSender(WebsiteUserMin sender) {
        this.sender = sender;
    }

    public List<WebsiteUserMin> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<WebsiteUserMin> recipients) {
        this.recipients = recipients;
    }

    public MailboxFolder getFolder() {
        return folder;
    }

    public void setFolder(MailboxFolder folder) {
        this.folder = folder;
    }

    public Boolean getRead() {
        return isRead;
    }

    public void setRead(Boolean read) {
        isRead = read;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public String toString() {
        return "sender: " + this.sender + "; recipients: " + this.recipients + "; message: " + this.message;
    }
}
