/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

/**
 * This class contains a list of workflow parameters
 * @author hugo
 */
public class WebsiteWorkflowParameterList {
    private WebsiteWorkflowParameter[] values;

    public WebsiteWorkflowParameterList() {
    }
    
    public WebsiteWorkflowParameter[] getValues() {
        return values;
    }

    public void addParameter(String blockName, String parameterName, String parameterValue){
        if(values==null){
            values = new WebsiteWorkflowParameter[1];
            values[0] = new WebsiteWorkflowParameter(blockName, parameterName, parameterValue);
        } else {
            WebsiteWorkflowParameter[] newValues = new WebsiteWorkflowParameter[values.length + 1];
            for(int i=0;i<values.length;i++){
                newValues[i] = values[i];
            }
            newValues[newValues.length - 1] = new WebsiteWorkflowParameter(blockName, parameterName, parameterValue);
            values = newValues;
        }
    }
    
    public void setValues(WebsiteWorkflowParameter[] values) {
        this.values = values;
    }
}