package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.*;

public class WebsiteLogger
{
	private Integer id;

	private String serialNumber;

	private String location;

	private WebsiteLoggerType loggerType;

	private List<Integer> loggerDeployments = new ArrayList<>();

	private Map<String, String> additionalProperties = new HashMap<>();

	protected WebsiteLogger()
	{

	}

	public WebsiteLogger(Logger logger)
	{
		this.setId(logger.getId());
		this.serialNumber = logger.getSerialNumber();
		this.location = logger.getLocation();

		// cyclic, use ID at OTHER side
		this.loggerType = new WebsiteLoggerType(logger.getLoggerType());

		// cyclic, use ID at OTHER side
		for (LoggerDeployment deployment : logger.getLoggerDeployments())
		{
			this.loggerDeployments.add(deployment.getId());
		}

		for (String key : logger.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, logger.getAdditionalProperties().get(key));
		}
	}

	public String getSerialNumber()
	{
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public WebsiteLoggerType getLoggerType()
	{
		return loggerType;
	}

	public void setLoggerType(WebsiteLoggerType loggerType)
	{
		this.loggerType = loggerType;
	}

	public List<Integer> getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void setLoggerDeployments(List<Integer> loggerDeployments)
	{
		this.loggerDeployments = loggerDeployments;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		this.additionalProperties = additionalProperties;
	}
}
