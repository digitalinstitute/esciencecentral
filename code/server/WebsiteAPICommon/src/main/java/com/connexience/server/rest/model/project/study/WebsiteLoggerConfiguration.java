package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.LoggerConfiguration;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteLoggerConfiguration
{
	private Integer id;

	private String name;

	private String description;

	private Integer sampleFrequency;

	private String application;

	private String minimumFirmwareVersion;

	private String firmwareLocation;

	private WebsiteLoggerTypeMin loggerType;

	private List<WebsiteLoggerDeploymentMin> loggerDeployments = new ArrayList<>();

	private Map<String, String> additionalProperties = new HashMap<>();

	protected WebsiteLoggerConfiguration()
	{

	}

	public WebsiteLoggerConfiguration(LoggerConfiguration configuration)
	{
		this.setId(configuration.getId());
		this.name = configuration.getName();
		this.description = configuration.getDescription();
		this.sampleFrequency = configuration.getSampleFrequency();
		this.application = configuration.getApplication();
		this.minimumFirmwareVersion = configuration.getMinimumFirmwareVersion();
		this.firmwareLocation = configuration.getFirmwareLocation();

		// cyclic, use IDs at OTHER side
		this.loggerType = new WebsiteLoggerTypeMin(configuration.getLoggerType());

		// cyclic, use ID at other side
		for (LoggerDeployment deployment : configuration.getLoggerDeployments())
		{
			loggerDeployments.add(new WebsiteLoggerDeploymentMin(deployment));
		}

		// not cyclic
		for (String key : configuration.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, configuration.getAdditionalProperties().get(key));
		}
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getSampleFrequency()
	{
		return sampleFrequency;
	}

	public void setSampleFrequency(Integer sampleFrequency)
	{
		this.sampleFrequency = sampleFrequency;
	}

	public String getApplication()
	{
		return application;
	}

	public void setApplication(String application)
	{
		this.application = application;
	}

	public String getMinimumFirmwareVersion()
	{
		return minimumFirmwareVersion;
	}

	public void setMinimumFirmwareVersion(String minimumFirmwareVersion)
	{
		this.minimumFirmwareVersion = minimumFirmwareVersion;
	}

	public String getFirmwareLocation()
	{
		return firmwareLocation;
	}

	public void setFirmwareLocation(String firmwareLocation)
	{
		this.firmwareLocation = firmwareLocation;
	}

	public WebsiteLoggerTypeMin getLoggerType()
	{
		return loggerType;
	}

	public void setLoggerType(WebsiteLoggerTypeMin loggerType)
	{
		this.loggerType = loggerType;
	}

	public List<WebsiteLoggerDeploymentMin> getLoggerDeployments()
	{
		return loggerDeployments;
	}

	public void setLoggerDeployments(List<WebsiteLoggerDeploymentMin> loggerDeployments)
	{
		this.loggerDeployments = loggerDeployments;
	}

	public Map<String, String> getAdditionalProperties()
	{
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties)
	{
		this.additionalProperties = additionalProperties;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}
