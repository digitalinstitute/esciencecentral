/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wrapper for internal group object
 * @author hugo
 */
public class WebsiteGroup extends WebsiteObject{
    private String name;
    private String description;
    private WebsiteUserMin creator;
    private long created;
    private List<WebsiteUserMin> members = new ArrayList<>();
    private String dataFolder;
    private String eventsFolder;
    private Map<String, Integer> statistics = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public WebsiteUserMin getCreator() {
        return creator;
    }

    public void setCreator(WebsiteUserMin creator) {
        this.creator = creator;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public List<WebsiteUserMin> getMembers() {
        return members;
    }

    public void setMembers(List<WebsiteUserMin> members) {
        this.members = members;
    }

    public boolean add(WebsiteUserMin websiteUser) {
        return getMembers().add(websiteUser);
    }

    public String getDataFolder() {
        return dataFolder;
    }

    public void setDataFolder(String dataFolder) {
        this.dataFolder = dataFolder;
    }

    public String getEventsFolder() {
        return eventsFolder;
    }

    public void setEventsFolder(String eventsFolder) {
        this.eventsFolder = eventsFolder;
    }

    public Map<String, Integer> getStatistics() {
        return statistics;
    }

    public void setStatistics(Map<String, Integer> statistics) {
        this.statistics = statistics;
    }

    public WebsiteGroup() {}

    public WebsiteGroup(Group group, Boolean userMetadata, Ticket ticket) throws ConnexienceException {
        this.setId(group.getId());
        this.name = group.getName();
        this.description = group.getDescription();
        this.creator = new WebsiteUserMin(EJBLocator.lookupUserDirectoryBean().getUser(ticket, group.getCreatorId()), userMetadata, ticket);
        this.created = group.getTimeInMillis();
        this.dataFolder = group.getDataFolder();
        this.eventsFolder = group.getEventsFolder();
        this.statistics = EJBLocator.lookupGroupDirectoryBean().getGroupStatistics(ticket, group.getId());

        List<User> users = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, group.getId());

        for(User user : users)
        {
            this.members.add(new WebsiteUserMin(user, userMetadata, ticket));
        }
    }

    public static WebsiteGroup saveGroup(WebsiteGroup group, Ticket ticket) throws ConnexienceException {

        Group connexienceGroup;

        if(group.getId() != null){

            connexienceGroup = EJBLocator.lookupGroupDirectoryBean().getGroup(ticket, group.getId());
            connexienceGroup.setName(group.getName());
            connexienceGroup.setDescription(group.getDescription());
        }
        else {
            connexienceGroup = new Group();
            connexienceGroup.setName(group.getName());
            connexienceGroup.setDescription(group.getDescription());
        }

        connexienceGroup = EJBLocator.lookupGroupDirectoryBean().saveGroup(ticket, connexienceGroup);

        return new WebsiteGroup(connexienceGroup, false, ticket);
    }
}