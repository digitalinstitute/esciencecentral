package com.connexience.server.rest.api;

import com.connexience.server.rest.model.WebsiteGroup;
//import com.wordnik.swagger.annotations.Api;
//import com.wordnik.swagger.annotations.ApiOperation;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

public interface IGroupsAPI {

    @GET
    @Path("/")
    @Produces("application/json")
    public List<WebsiteGroup> getGroups(@DefaultValue("") @QueryParam("q") String query);

    @PUT
    @Path("/")
    @Produces("application/json")
    @Consumes("application/json")
    public WebsiteGroup createGroup( WebsiteGroup group);

    @GET
    @Path("/{groupId}")
    @Produces("application/json")
    public WebsiteGroup getGroup(@PathParam("groupId") String groupId, @DefaultValue("false") @QueryParam("userMetadata") Boolean userMetadata);

    @POST
    @Path("/{groupId}")
    @Produces("application/json")
    @Consumes("application/json")
    public WebsiteGroup updateGroup(@PathParam("groupId") String groupId, WebsiteGroup group);

    @DELETE
    @Path("/{groupId}")
    @Produces("application/json")
    public Response deleteGroup(@PathParam("groupId") String groupId);

    @GET
    @Path("/find/{groupName}")
    @Produces("application/json")
    public WebsiteGroup getGroupByName(@PathParam("groupName") String groupName, @DefaultValue("false") @QueryParam("userMetadata") Boolean userMetadata);

    @PUT
    @Path("/{groupId}/{userId}")
    @Produces("application/json")
    public WebsiteGroup addUserToGroup(@PathParam("groupId") String groupId, @PathParam("userId") String userId);

    @DELETE
    @Path("/{groupId}/{userId}")
    @Produces("application/json")
    public WebsiteGroup removeUserFromGroup(@PathParam("groupId") String groupId, @PathParam("userId") String userId);
}
