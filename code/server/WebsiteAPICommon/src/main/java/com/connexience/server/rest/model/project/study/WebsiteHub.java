package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Hub;
import com.connexience.server.model.project.study.HubDeployment;
import com.connexience.server.model.project.study.Logger;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteHub
{
	private Integer id;

	private String serialNumber;

	private String dataFolderId;

	private WebsiteHubType hubType;

	private List<Integer> hubDeployments = new ArrayList<>();

	private Map<String, String> additionalProperties = new HashMap<>();

	protected WebsiteHub()
	{

	}

	public WebsiteHub(Hub hub)
	{
		this.setId(hub.getId());
		this.serialNumber = hub.getSerialNumber();
		this.dataFolderId = hub.getDataFolderId();

		// cyclic, use ID at OTHER side
		this.hubType = new WebsiteHubType(hub.getHubType());

		// cyclic, use ID at OTHER side
		for (HubDeployment deployment : hub.getDeployments())
		{
			this.hubDeployments.add(deployment.getId());
		}

		for (String key : hub.getAdditionalProperties().keySet())
		{
			this.additionalProperties.put(key, hub.getAdditionalProperties().get(key));
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getDataFolderId() {
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId) {
		this.dataFolderId = dataFolderId;
	}

	public WebsiteHubType getHubType() {
		return hubType;
	}

	public void setHubType(WebsiteHubType hubType) {
		this.hubType = hubType;
	}

	public List<Integer> getHubDeployments() {
		return hubDeployments;
	}

	public void setHubDeployments(List<Integer> hubDeployments) {
		this.hubDeployments = hubDeployments;
	}

	public Map<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
}
