package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.LoggerData;
import com.connexience.server.model.project.study.LoggerDeployment;

import java.util.*;

public class WebsiteLoggerDeploymentMin {
    private Integer id;

    private int studyId;

    private String studyName;

    private String dataFolderId;

    private boolean active = false;

    private Integer subjectGroupId;

    private String subjectGroupName;

    private WebsiteLoggerMin logger;

    private Integer loggerConfigurationId;

    private Date startDate;

    private Date endDate;

    private Map<String, String> additionalProperties = new HashMap<>();

    protected WebsiteLoggerDeploymentMin() {
    }

    public WebsiteLoggerDeploymentMin(LoggerDeployment deployment) {
        this.setId(deployment.getId());

        this.subjectGroupId = deployment.getSubjectGroup().getId();

        this.active = deployment.isActive();

        this.dataFolderId = deployment.getDataFolderId();

        this.loggerConfigurationId = deployment.getLoggerConfiguration().getId();

        // not cyclic (no reverse reference)
        this.studyId = deployment.getStudy().getId();

        this.studyName = deployment.getStudy().getName();

        this.subjectGroupName = deployment.getSubjectGroup().getDisplayName();

        this.startDate = deployment.getStartDate();

        this.endDate = deployment.getEndDate();

        this.logger = new WebsiteLoggerMin(deployment.getLogger());

        for (String key : deployment.getAdditionalProperties().keySet())
        {
            this.additionalProperties.put(key, deployment.getAdditionalProperties().get(key));
        }
    }


    public int getStudyId() {
        return studyId;
    }

    public void setStudyId(int studyId) {
        this.studyId = studyId;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public WebsiteLoggerMin getLogger() {
        return logger;
    }

    public void setLogger(WebsiteLoggerMin logger) {
        this.logger = logger;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public Map<String, String> getAdditionalProperties()
    {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Integer getSubjectGroupId() {
        return subjectGroupId;
    }

    public void setSubjectGroupId(Integer subjectGroupId) {
        this.subjectGroupId = subjectGroupId;
    }

    public Integer getLoggerConfigurationId() {
        return loggerConfigurationId;
    }

    public void setLoggerConfigurationId(Integer loggerConfigurationId) {
        this.loggerConfigurationId = loggerConfigurationId;
    }
    public String getStudyName() {
        return studyName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getSubjectGroupName() {
        return subjectGroupName;
    }

    public void setSubjectGroupName(String subjectGroupName) {
        this.subjectGroupName = subjectGroupName;
    }
}
