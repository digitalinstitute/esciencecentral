package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.*;
import com.connexience.server.rest.model.project.WebsiteProject;

import javax.ejb.EJB;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WebsitePhase {
    private Integer id;

    private Integer studyId;

    private String name;

    private List<WebsiteSubjectGroup> subjectGroups = new ArrayList<>();

    public WebsitePhase() {
    }

    public WebsitePhase(Phase phase) {
        this.setId(phase.getId());
        this.setName(phase.getName());

        if (phase.getStudy() != null) {
            this.setStudyId(phase.getStudy().getId());
        }

        if(phase.getSubjectGroups()!=null){
            for(SubjectGroup subjectGroup : phase.getSubjectGroups()){
                this.getSubjectGroups().add(new WebsiteSubjectGroup(subjectGroup));
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudyId() {
        return studyId;
    }

    public void setStudyId(Integer studyId) {
        this.studyId = studyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<WebsiteSubjectGroup> getSubjectGroups() {
        return subjectGroups;
    }

    public void setSubjectGroups(List<WebsiteSubjectGroup> subjectGroups) {
        this.subjectGroups = subjectGroups;
    }
}
