package com.connexience.server.rest;

/**
 * User: nsjw7
 * Date: 18/06/2013
 * Time: 09:44
 */
public class WorkflowStats {
    private long success;
    private long failed;
    private long running;
    private long waiting;

    public WorkflowStats() {
    }

    public long getSuccess() {
        return success;
    }

    public void setSuccess(long success) {
        this.success = success;
    }

    public long getFailed() {
        return failed;
    }

    public void setFailed(long failed) {
        this.failed = failed;
    }

    public long getRunning() {
        return running;
    }

    public void setRunning(long running) {
        this.running = running;
    }

    public long getWaiting() {
        return waiting;
    }

    public void setWaiting(long waiting) {
        this.waiting = waiting;
    }
}
