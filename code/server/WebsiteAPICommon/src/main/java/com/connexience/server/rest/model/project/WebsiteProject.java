package com.connexience.server.rest.model.project;

import com.connexience.server.model.project.Project;
import java.util.Date;

import java.util.HashMap;
import java.util.Map;

/**
 * Class description here.
 *
 * @author ndjm8
 */
public class WebsiteProject {

    private Integer id;

    private String externalId;

    private String name;

    private String description;

    private boolean privateProject = false;

    private String ownerId = null;

    private String adminGroupId = null;

    private String membersGroupId = null;

    private String workflowFolderId = null;

    private String dataFolderId = null;

    private String remoteScannerId = null;
    
    private String javaClassName = "";

    private Date startDate = null;

    private Date endDate = null;

    private boolean visibleOnExternalSite = false;    
    
    private Map<String, String> additionalProperties = new HashMap<>();

    public WebsiteProject() {
    }

    public WebsiteProject(Project project) {
        this.id = project.getId();
        this.externalId = project.getExternalId();
        this.ownerId = project.getOwnerId();
        this.adminGroupId = project.getAdminGroupId();
        this.membersGroupId = project.getMembersGroupId();

        this.workflowFolderId = project.getWorkflowFolderId();
        this.dataFolderId = project.getDataFolderId();

        this.name = project.getName();
        this.description = project.getDescription();
        this.privateProject = project.isPrivateProject();
        this.javaClassName = project.getClass().getName();

        this.setVisibleOnExternalSite(project.isVisibleOnExternalSite());
        this.startDate = project.getStartDate();
        this.endDate = project.getEndDate();
        
        for (String key : project.getAdditionalProperties().keySet()) {
            this.additionalProperties.put(key, project.getAdditionalProperties().get(key));
        }
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isVisibleOnExternalSite() {
        return visibleOnExternalSite;
    }

    public void setVisibleOnExternalSite(boolean visibleOnExternalSite) {
        this.visibleOnExternalSite = visibleOnExternalSite;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isPrivateProject() {
        return privateProject;
    }

    public void setPrivateProject(boolean privateProject) {
        this.privateProject = privateProject;
    }

    public Map<String, String> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, String> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    public String getAdminGroupId() {
        return adminGroupId;
    }

    public void setAdminGroupId(String adminGroupId) {
        this.adminGroupId = adminGroupId;
    }

    public String getMembersGroupId() {
        return membersGroupId;
    }

    public void setMembersGroupId(String membersGroupId) {
        this.membersGroupId = membersGroupId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(final String externalId) {
        this.externalId = externalId;
    }

    public String getWorkflowFolderId() {
        return workflowFolderId;
    }

    public void setWorkflowFolderId(final String workflowFolderId) {
        this.workflowFolderId = workflowFolderId;
    }

    public String getDataFolderId() {
        return dataFolderId;
    }

    public void setDataFolderId(final String dataFolderId) {
        this.dataFolderId = dataFolderId;
    }

    public String getRemoteScannerId() {
        return remoteScannerId;
    }

    public void setRemoteScannerId(String remoteScannerId) {
        this.remoteScannerId = remoteScannerId;
    }
    
    public void populateProject(Project p){
        if (getId() != null) {
            p.setId(getId());
        }

        if (getExternalId() != null) {
            p.setExternalId(getExternalId());
        }

        if (getDescription() != null) {
            p.setDescription(getDescription());
        }

        if (this.startDate != null) {
            p.setStartDate(this.startDate);
        }

        if (this.endDate != null) {
            p.setEndDate(this.endDate);
        }        
        
        p.setPrivateProject(isPrivateProject());

        p.setVisibleOnExternalSite(isVisibleOnExternalSite());

        if (getAdminGroupId() != null) {
            p.setAdminGroupId(getAdminGroupId());
        }

        if (getMembersGroupId() != null) {
            p.setMembersGroupId(getMembersGroupId());
        }

        if (getWorkflowFolderId() != null) {
            p.setWorkflowFolderId(getWorkflowFolderId());
        }

        if (getDataFolderId() != null) {
            p.setDataFolderId(getDataFolderId());
        }

        for (String key : this.getAdditionalProperties().keySet()) {
            p.getAdditionalProperties().put(key, this.getAdditionalProperties().get(key));
        }

        if (getRemoteScannerId() != null) {
            p.setRemoteScannerId(Long.parseLong(getRemoteScannerId()));
        }        
    }
}
