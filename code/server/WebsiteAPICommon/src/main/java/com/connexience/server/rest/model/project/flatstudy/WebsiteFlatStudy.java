package com.connexience.server.rest.model.project.flatstudy;

import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.rest.model.project.WebsiteProject;

/**
 * Website API visible flat study object
 * @author hugo
 */
public class WebsiteFlatStudy extends WebsiteProject {

    public WebsiteFlatStudy(FlatStudy study) {
        super(study);
        
    }
    
    public FlatStudy toFlatStudy(){
        FlatStudy study = new FlatStudy();
        study.setName(getName());
        study.setOwnerId(getOwnerId());
        populateProject(study);
        return study;
    }
}
