/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

/**
 * User: nsjw7
 * Date: 06/12/2012
 * Time: 15:00
 */
public class WebsiteMessageSummary {

    private int numNewMessages = 0;

    private int numWorkflowMessages = 0;

    private int numGroupInvitations = 0;

    private int numPeopleInvitations = 0;

    public WebsiteMessageSummary() {
    }

    public int getNumNewMessages() {
        return numNewMessages;
    }

    public void setNumNewMessages(int numNewMessages) {
        this.numNewMessages = numNewMessages;
    }

    public int getNumWorkflowMessages() {
        return numWorkflowMessages;
    }

    public void setNumWorkflowMessages(int numWorkflowMessages) {
        this.numWorkflowMessages = numWorkflowMessages;
    }

    public int getNumGroupInvitations() {
        return numGroupInvitations;
    }

    public void setNumGroupInvitations(int numGroupInvitations) {
        this.numGroupInvitations = numGroupInvitations;
    }

    public int getNumPeopleInvitations() {
        return numPeopleInvitations;
    }

    public void setNumPeopleInvitations(int numPeopleInvitations) {
        this.numPeopleInvitations = numPeopleInvitations;
    }

    @Override
    public String toString() {
        return "WebsiteMessageSummary{" +
                "numNewMessages=" + numNewMessages +
                ", numWorkflowMessages=" + numWorkflowMessages +
                ", numGroupInvitations=" + numGroupInvitations +
                ", numPeopleInvitations=" + numPeopleInvitations +
                '}';
    }
}
