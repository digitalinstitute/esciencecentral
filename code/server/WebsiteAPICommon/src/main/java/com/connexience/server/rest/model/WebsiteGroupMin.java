/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Wrapper for internal group object
 * @author hugo
 */
public class WebsiteGroupMin extends WebsiteObject{
    private String name;
    private String creatorId;
    private String creatorName;
    private long created;
    private List<String> memberIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public List<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(List<String> memberIDs) {
        this.memberIds = memberIDs;
    }

    public WebsiteGroupMin() {}

    public WebsiteGroupMin(Group group, Ticket ticket) throws ConnexienceException {

        this.setId(group.getId());
        this.name = group.getName();
        this.creatorId = group.getCreatorId();
        this.creatorName = group.getDisplayName();
        this.created = group.getTimeInMillis();

        List<User> users = EJBLocator.lookupGroupDirectoryBean().listGroupMembers(ticket, group.getId());

        if(users != null){

            List<String> ids = new ArrayList<String>();

            for(User user : users)
            {
                if(user.getId().equals(group.getCreatorId())){
                    this.creatorName = user.getDisplayName();
                }

                ids.add(user.getId());
            }

            this.memberIds = ids;
        }
        else {
            this.memberIds = new ArrayList<String>();
        }


    }
}