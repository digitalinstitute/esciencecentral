/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;

import java.util.*;

/**
 * A notificaiton that can be displayed in the browser
 */
public class WebsiteNotification {

    public enum WEBSITE_NOTIFICATION_TYPE{
        USER_READ, USER_WRITE, WORKFLOW_EXECUTE
    }

    private long timestamp;

    private WEBSITE_NOTIFICATION_TYPE type;

    private HashMap<String, Object> properties = new HashMap<>();

    public WebsiteNotification() {
    }

    public WebsiteNotification(long timestamp, WEBSITE_NOTIFICATION_TYPE type) {
        this.timestamp = timestamp;
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public WEBSITE_NOTIFICATION_TYPE getType() {
        return type;
    }

    public void setType(WEBSITE_NOTIFICATION_TYPE type) {
        this.type = type;
    }

    public HashMap<String, Object> getProperties() {
        return properties;
    }

    public void setProperties(HashMap<String, Object> properties) {
        this.properties = properties;
    }

    public Object get(Object key) {
        return properties.get(key);
    }

    public boolean containsKey(Object key) {
        return properties.containsKey(key);
    }

    public Object put(String key, Object value) {
        return properties.put(key, value);
    }

    public Object remove(Object key) {
        return properties.remove(key);
    }

    public Set<String> keySet() {
        return properties.keySet();
    }

    public int size() {
        return properties.size();
    }
}
