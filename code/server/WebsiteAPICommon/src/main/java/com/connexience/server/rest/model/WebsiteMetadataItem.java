/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model;

import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;

/**
 * This class defines a single piece of metadata
 * @author hugo
 */
public class WebsiteMetadataItem extends WebsiteObject {
    public enum METADATA_TYPE {
        BOOLEAN,DATE,NUMERICAL,TEXT
    }
    
    private METADATA_TYPE metadataType = METADATA_TYPE.TEXT;
    private String name;
    private String category;
    private String stringValue;
    private String objectId;
    private String id;
    
    public WebsiteMetadataItem() {
    }

    public WebsiteMetadataItem(MetadataItem metadataItem){
        this.name = metadataItem.getName();
        this.category = metadataItem.getCategory();
        this.stringValue = metadataItem.getStringValue();
        this.objectId = metadataItem.getObjectId();
        this.id = String.valueOf(metadataItem.getId());

        if(metadataItem instanceof BooleanMetadata) this.metadataType = METADATA_TYPE.BOOLEAN;
        else if(metadataItem instanceof DateMetadata) this.metadataType = METADATA_TYPE.DATE;
        else if(metadataItem instanceof NumericalMetadata) this.metadataType = METADATA_TYPE.NUMERICAL;
        else if(metadataItem instanceof TextMetadata) this.metadataType = METADATA_TYPE.TEXT;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
    
    public METADATA_TYPE getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(METADATA_TYPE metadataType) {
        this.metadataType = metadataType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}