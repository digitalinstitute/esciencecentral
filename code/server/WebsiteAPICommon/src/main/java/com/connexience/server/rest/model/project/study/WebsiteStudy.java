package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.flatstudy.FlatStudy;
import com.connexience.server.model.project.study.Phase;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.rest.model.project.WebsiteProject;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WebsiteStudy extends WebsiteProject implements Serializable {

    private List<WebsitePhase> phases = new ArrayList();

    public WebsiteStudy() {
    }

    public WebsiteStudy(Study study) {
        super(study);

        for (Phase phase : study.getPhases()) {
            this.phases.add(new WebsitePhase(phase));
        }
    }



    public List<WebsitePhase> getPhases() {
        return phases;
    }

    public void setPhases(List<WebsitePhase> phases) {
        this.phases = phases;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WebsiteStudy)) {
            return false;
        }

        WebsiteStudy study = (WebsiteStudy) o;

        if (!(getId().equals(study.getId()))) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    public FlatStudy toFlatSudy(){
        FlatStudy study = new FlatStudy();
        study.setName(getName());
        study.setOwnerId(getOwnerId());
        populateProject(study);
        return study;
    }
    
    public Study toStudy() {
        Study study = new Study(getName(), getOwnerId());
        populateProject(study);

        if (this.phases != null) {
            List<Phase> phases = new ArrayList<>();

            for (WebsitePhase phase : this.phases) {
                Phase p = new Phase();

                p.setId(p.getId());
                p.setName(p.getName());
                p.setStudy(study);
                p.setSubjectGroups(p.getSubjectGroups());

                phases.add(p);
            }

            study.setPhases(phases);
        }
        return study;
    }
}
