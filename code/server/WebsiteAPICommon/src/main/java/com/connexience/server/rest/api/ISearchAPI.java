/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.api;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.rest.model.WebsiteUserMin;

import javax.ws.rs.*;
import java.util.List;

/**
 * Deals with User management in the API - listing, retrieving, updating
 * <p/>
 * The default implementation for this API is at <context-root>/rest/users
 * <p/>
 * User: nsjw7
 * Date: 30/11/2012
 * Time: 09:00
 */
public interface ISearchAPI {
    @GET
    @Path("/users")
    @Produces("application/json")
    public List<WebsiteUserMin> searchUsers(
            @QueryParam("query") String query,
            @DefaultValue("id") @QueryParam("orderBy") OrderBy orderBy,
            @DefaultValue("ASC") @QueryParam("searchOrder") SearchOrder searchOrder,
            @DefaultValue("1") @QueryParam("pageNum") int pageNum,
            @DefaultValue("10") @QueryParam("pageSize") int pageSize,
            @DefaultValue("false") @QueryParam("metadata") Boolean metadata) throws ConnexienceException;

}
