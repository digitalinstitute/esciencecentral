package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Sensor;
import com.connexience.server.rest.model.project.WebsiteFileType;

public class WebsiteSensor
{
	private Integer id;

	private String name;

	private WebsiteFileType fileType;

	private Integer loggerType;

	protected WebsiteSensor()
	{
	}

	public WebsiteSensor(Sensor sensor)
	{
		this.setId(sensor.getId());

        this.setName(sensor.getName());

		// cyclic, use IDs at OTHER side
		this.fileType = new WebsiteFileType(sensor.getFileType());

		// cyclic, use ID here instead
		this.loggerType = sensor.getLoggerType().getId();
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public WebsiteFileType getFileType()
	{
		return fileType;
	}

	public void setFileType(WebsiteFileType fileType)
	{
		this.fileType = fileType;
	}

	public Integer getLoggerType()
	{
		return loggerType;
	}

	public void setLoggerType(Integer loggerType)
	{
		this.loggerType = loggerType;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(final Integer id)
	{
		this.id = id;
	}
}
