/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.model.project;

import com.connexience.server.rest.model.WebsiteObject;import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Wrapper for internal Project object
 *
 * @author hugo
 */

public class UploadUserProject extends WebsiteObject {
    private String scannerType = "";
    private String name = "";
    private String blobStoreAccount = "";
    private String blobStoreKey = "";
    private String container = "";
    private String description = "";
    private List<String> adminUserIds = new ArrayList<String>();

    public UploadUserProject() {
    }

    public UploadUserProject(String id, String name, String blobStoreAccount, String blobStoreKey, String container, String description, List<String> adminUserIds) {
        super(id);
        this.name = name;
        this.blobStoreAccount = blobStoreAccount;
        this.blobStoreKey = blobStoreKey;
        this.container = container;
        this.description = description;
        this.adminUserIds = adminUserIds;
    }

    public String getScannerType() {
        return scannerType;
    }

    public void setScannerType(String scannerType) {
        this.scannerType = scannerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBlobStoreAccount() {
        return blobStoreAccount;
    }

    public void setBlobStoreAccount(String blobStoreAccount) {
        this.blobStoreAccount = blobStoreAccount;
    }

    public String getBlobStoreKey() {
        return blobStoreKey;
    }

    public void setBlobStoreKey(String blobStoreKey) {
        this.blobStoreKey = blobStoreKey;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getAdminUserIds() {
        return adminUserIds;
    }

    public void setAdminUserIds(List<String> adminUserIds) {
        this.adminUserIds = adminUserIds;
    }

    public boolean addAdminUserId(String s) {
        return adminUserIds.add(s);
    }

    public boolean removeAdminUserId(Object o) {
        return adminUserIds.remove(o);
    }


}