/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package com.connexience.server.rest.api;

import com.connexience.server.model.provenance.events.GraphOperation;
import com.connexience.server.rest.model.WebsiteNotification;
import com.connexience.server.rest.model.WebsiteWorkflowInvocation;
import com.connexience.server.rest.model.WebsiteWorkflowMin;
import com.connexience.server.rest.model.WebsiteWorkflowParameterList;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;


public interface INotificationsAPI {

    //These methods deliberately ignore the userId sent in and use the ticket user
    //so that other logged in users can't see the activity of another.

    @GET
    @Path("/{id}/projects")
    @Produces("application/json")
    List<WebsiteNotification> getProjectActivity(@QueryParam("start") @DefaultValue("0") int start, @QueryParam("numResults") @DefaultValue("10") int numResults) throws Exception;

    @GET
    @Path("/{userid}/projects/{projectid}")
    @Produces("application/json")
    List<WebsiteNotification> getSingleProjectActivity(@PathParam("projectid") String projectId, @QueryParam("start") @DefaultValue("0") int start, @QueryParam("numResults") @DefaultValue("10") int numResults) throws Exception;

    @GET
    @Path("/{id}/user")
    @Produces("application/json")
    List<WebsiteNotification> getUserActivity(@QueryParam("start") @DefaultValue("0") int start, @QueryParam("numResults") @DefaultValue("10") int numResults) throws Exception;

    @GET
    @Path("/{id}/workflows")
    @Produces("application/json")
    List<WebsiteWorkflowInvocation> getWorkflowActivity( @QueryParam("start") @DefaultValue("0") int start, @QueryParam("numResults") @DefaultValue("10") int numResults) throws Exception;

    @GET
    @Path("{userid}/workflows/{invocationId}")
    @Produces("application/json")
    WebsiteWorkflowInvocation getInvocationDetails(@PathParam("invocationId") String invocationId) throws Exception;


}
