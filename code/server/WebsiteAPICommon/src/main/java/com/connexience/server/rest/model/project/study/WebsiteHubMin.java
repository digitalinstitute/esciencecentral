package com.connexience.server.rest.model.project.study;

import com.connexience.server.model.project.study.Hub;
import com.connexience.server.model.project.study.HubDeployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteHubMin
{
	private Integer id;

	private String serialNumber;

	private String dataFolderId;

	private WebsiteHubTypeMin hubType;

	protected WebsiteHubMin()
	{
	}

	public WebsiteHubMin(Hub hub)
	{
		this.setId(hub.getId());
		this.serialNumber = hub.getSerialNumber();
		this.dataFolderId = hub.getDataFolderId();
		this.hubType = new WebsiteHubTypeMin(hub.getHubType());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getDataFolderId() {
		return dataFolderId;
	}

	public void setDataFolderId(String dataFolderId) {
		this.dataFolderId = dataFolderId;
	}

	public WebsiteHubTypeMin getHubType() {
		return hubType;
	}

	public void setHubType(WebsiteHubTypeMin hubType) {
		this.hubType = hubType;
	}
}
