/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.util;

/**
 * Sanitize parameters to remove javascript
 * @author hugo
 */
public class ParameterSanitizer {
    private static final int maxLength = 255;
    
    public static String sanitize(String string) {
        if(string!=null && string.length()<=maxLength){

            return string
                .replaceAll("\n", "").replaceAll("\r", "")
                .replaceAll("(?i)<script.*?>.*?</script.*?>", "") // case 1
                .replaceAll("(?i)<.*?javascript:.*?>.*?</.*?>", "") // case 2
                .replaceAll("(?i)<.*?\\s+on.*?>.*?</.*?>", "");     // case 3
               
        } else {
            return "";
        }
    }     
    
    public static String sanitizeNumerical(String string){
        String jsSanitized = sanitize(string);
        try {
            Long.parseLong(jsSanitized);
            return jsSanitized;
        } catch (Exception e){
            return "";
        }
    }
}
