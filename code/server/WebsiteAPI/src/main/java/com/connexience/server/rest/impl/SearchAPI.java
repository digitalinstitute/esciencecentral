/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.api.ISearchAPI;
import com.connexience.server.rest.exceptions.ConnexienceWebApplicationException;
import com.connexience.server.rest.model.WebsiteUserMin;
import com.connexience.server.rest.util.APIUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * User: nsjw7 Date: 28/05/2013 Time: 10:20
 * <p/>
 * To access lazily fetched members use
 */
@SuppressWarnings("unchecked")
@Path("/rest/search")
public class SearchAPI implements ISearchAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@EJB(lookup = "java:global/esc/server-beans/UserDirectoryBean")
	UserDirectoryRemote userBean;

	@Override
	public List<WebsiteUserMin> searchUsers(String query, OrderBy orderBy, SearchOrder searchOrder, int pageNum, int pageSize, Boolean metadata) {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            List<User> users = userBean.searchForUsers(ticket, query, orderBy, searchOrder, APIUtils.getFirstResult(pageNum, pageSize), pageSize);
            List<WebsiteUserMin> results = new ArrayList<>();
            for (User user : users) {

                results.add(new WebsiteUserMin(user, metadata, ticket));
            }
            return results;
        }
        catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }
	}
}
