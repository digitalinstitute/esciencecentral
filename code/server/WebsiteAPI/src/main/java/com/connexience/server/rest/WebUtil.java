/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.rest;

import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.Group;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.Event;

import javax.servlet.http.HttpServletRequest;

/** @author nsjw7 */
public class WebUtil {


    public static String getClientIP(HttpServletRequest request) {

        String clientIP = request.getHeader("x-forwarded-for");   //If it's been forwarded by Apache
        if (clientIP == null || clientIP.equals("")) {
            clientIP = request.getRemoteAddr();
        }

        return clientIP;
    }
}