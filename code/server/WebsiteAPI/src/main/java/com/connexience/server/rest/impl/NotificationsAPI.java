package com.connexience.server.rest.impl;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.provenance.events.GraphOperation;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.rest.api.INotificationsAPI;
import com.connexience.server.rest.model.WebsiteNotification;
import com.connexience.server.rest.model.WebsiteWorkflowInvocation;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.rest.util.WebsiteObjectFactory;
import com.connexience.server.util.NameCache;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.List;

@Path("/rest/notifications")
@Stateless
@SuppressWarnings("unchecked")
public class NotificationsAPI implements INotificationsAPI {

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    //Cache of names of obejcts
    private NameCache nameCache = new NameCache(100);


    @Override
    public List<WebsiteNotification> getProjectActivity(int start, int numResults) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        //Get the activity of project members which this user is associated with
        List<GraphOperation> ops = EJBLocator.lookupNotificationsBean().getAllProjectActivity(ticket, ticket.getUserId(), start, numResults);

        List<WebsiteNotification> notifications = new ArrayList<>();
        for (GraphOperation op : ops) {
            notifications.add(WebsiteObjectFactory.createNotification(op));
        }

        return notifications;
    }

    @Override
    public List<WebsiteNotification> getSingleProjectActivity(String projectId, int start, int numResults) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        Project p = EJBLocator.lookupProjectsBean().getProject(ticket, Integer.parseInt(projectId));
        if (p != null) {

            if (EJBLocator.lookupProjectsBean().isProjectMember(ticket, p)) {

                //Get the activity of project members which this user is associated with
                List<GraphOperation> ops = EJBLocator.lookupNotificationsBean().getSingleProjectActivity(ticket, ticket.getUserId(), projectId, start, numResults);

                List<WebsiteNotification> notifications = new ArrayList<>();
                for (GraphOperation op : ops) {
                    notifications.add(WebsiteObjectFactory.createNotification(op));
                }

                return notifications;
            } else {
                throw new IllegalArgumentException("Must be project member to see activity for project: " + projectId);
            }
        } else {
            throw new IllegalArgumentException("Can't find project with Id: " + projectId);
        }
    }

    @Override
    public List<WebsiteNotification> getUserActivity(int start, int numResults) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        //Get the activity for just this user
        List<GraphOperation> ops = EJBLocator.lookupNotificationsBean().getUserActivity(ticket, ticket.getUserId(), start, numResults);

        List<WebsiteNotification> notifications = new ArrayList<>();
        for (GraphOperation op : ops) {
            notifications.add(WebsiteObjectFactory.createNotification(op));
        }

        return notifications;
    }

    @Override
    public List<WebsiteWorkflowInvocation> getWorkflowActivity(int start, int numResults) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        //Get the activity for just this user
        List<WorkflowInvocationFolder> invs = EJBLocator.lookupNotificationsBean().getWorkflowActivity(ticket, ticket.getUserId(), start, numResults);

        List<WebsiteWorkflowInvocation> notifications = new ArrayList<>();

        for (WorkflowInvocationFolder inv : invs) {
            WebsiteWorkflowInvocation websiteInv = WebsiteObjectFactory.createEscWorkflowInvocation(inv);

            websiteInv.setWorkflowName(nameCache.getObjectName(ticket, websiteInv.getWorkflowId()));
            websiteInv.setUsername(nameCache.getObjectName(ticket, websiteInv.getUserId()));

            notifications.add(websiteInv);
        }

        return notifications;
    }

    @Override
    public WebsiteWorkflowInvocation getInvocationDetails(String invocationId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);


        //Get the activity for just this user
        WorkflowInvocationFolder inv = EJBLocator.lookupNotificationsBean().getInvocationDetails(ticket, invocationId);

        if (inv != null) {
            WebsiteWorkflowInvocation websiteInv = WebsiteObjectFactory.createEscWorkflowInvocation(inv);

            websiteInv.setWorkflowName(nameCache.getObjectName(ticket, websiteInv.getWorkflowId()));
            websiteInv.setUsername(nameCache.getObjectName(ticket, websiteInv.getUserId()));

            return websiteInv;
        } else {
            throw new IllegalArgumentException("Can't find invocation with Id: " + invocationId);
        }
    }
}
