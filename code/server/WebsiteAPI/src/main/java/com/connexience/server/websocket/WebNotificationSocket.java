/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.websocket;

import javax.ejb.Stateless;
import javax.servlet.http.HttpSession;
import javax.websocket.CloseReason;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * Websocket backend which sends notifications to logged on users
 * @author hugo
 */
@ServerEndpoint(value="/notificationsocket", configurator = WebNotificationSocketConfigurator.class)
public class WebNotificationSocket {
    private HttpSession session;
    private Session wsSession;
    
    @OnOpen
    public void open(Session session, EndpointConfig config){
        this.wsSession = session;
        if(config.getUserProperties().get(HttpSession.class.getName())!=null){
            this.session = (HttpSession)config.getUserProperties().get(HttpSession.class.getName());
        } else {
            this.session = null;
        }
    }
    
    @OnClose
    public void close(Session session, CloseReason reason){
        
    }
    
}