/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.social.MessageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.messages.Message;
import com.connexience.server.model.messages.TextMessage;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.rest.api.IMessagesAPI;
import com.connexience.server.rest.model.WebsiteMessage;
import com.connexience.server.rest.model.WebsiteMessageSummary;
import com.connexience.server.rest.model.WebsiteUserMin;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.google.common.collect.Iterables;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Methods of the API relating to messaging and notifications
 * <p/>
 * User: nsjw7 Date: 06/12/2012 Time: 14:58
 */
@Path("/rest/messages")
public class MessagesAPI implements IMessagesAPI {
	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	private static final String MSG_SUMMARY = "MSG_SUMMARY";

	public WebsiteMessageSummary getSummary() throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);
		HttpSession session = request.getSession();
		if (session.getAttribute(MSG_SUMMARY) == null) {
			WebsiteMessageSummary summary = new WebsiteMessageSummary();

			MessageRemote msgBean = EJBLocator.lookupMessageBean();
			Long numMessages = msgBean.getNumberOfUnreadMessages(ticket);
			summary.setNumNewMessages(numMessages.intValue());

			session.setAttribute(MSG_SUMMARY, summary);
			//todo: implement other message types
			return summary;
		} else {
			return (WebsiteMessageSummary) session.getAttribute(MSG_SUMMARY);
		}
	}

	public List<WebsiteMessage> getMessages(int start, int numResults) throws Exception {
		Ticket ticket = TransitionSessionUtils.getTicket(request);

		//remove the summary from the session as we're reading messages
		HttpSession session = request.getSession();
		if (session.getAttribute(MSG_SUMMARY) != null) {
			session.removeAttribute(MSG_SUMMARY);
		}

        List<WebsiteMessage> wsMessages = new ArrayList<>();

        try {

            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

            Collection<Message> inbox = EJBLocator.lookupMessageBean().getTextMessages(ticket, ticket.getUserId(), user.getInboxFolderId(), start, numResults);
            Collection<Message> sent = EJBLocator.lookupMessageBean().getTextMessages(ticket, ticket.getUserId(), user.getSentMessagesFolderId(), start, numResults);
            Collection<Message> drafts = EJBLocator.lookupMessageBean().getTextMessages(ticket, ticket.getUserId(), user.getDraftMessagesFolderId(), start, numResults);
            Collection<Message> trash = EJBLocator.lookupMessageBean().getTextMessages(ticket, ticket.getUserId(), user.getTrashedMessagesFolderId(), start, numResults);

            Iterable<Message> messages = Iterables.unmodifiableIterable(Iterables.concat(inbox, sent, drafts, trash));

            for (Message msg : messages) {
                wsMessages.add(new WebsiteMessage(msg, ticket));
            }
        }
        catch (ConnexienceException ex){
            ex.printStackTrace();
        }

        return wsMessages;
	}

    @Override
    public WebsiteMessage sendMessage(WebsiteMessage message) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        WebsiteUserMin minUser = new WebsiteUserMin(user, false, ticket);

        WebsiteMessage wsMessage = new WebsiteMessage();

        try {

            List<WebsiteUserMin> recipients = message.getRecipients();

            if(!recipients.contains(minUser)){
                recipients.add(minUser);
            }

            StringBuilder builder = new StringBuilder();

            for(WebsiteUserMin recipient : message.getRecipients()){
                builder.append(recipient.getId() + " ");
            }

            TextMessage createdMessage = EJBLocator.lookupMessageBean().createTextMessage(ticket, message.getRecipients().get(0).getId(), builder.toString().trim(), message.getThreadId(), message.getSubject(), message.getMessage());

            TextMessage txtMessage = EJBLocator.lookupMessageBean().addSentTextMessageToFolder(ticket, builder.toString().trim(), createdMessage.getThreadId(), message.getSubject(), message.getMessage());

            wsMessage = new WebsiteMessage(txtMessage, ticket);

        } catch (ConnexienceException e) {
            e.printStackTrace();
        }

        return wsMessage;
    }

    @Override
    public WebsiteMessage saveMessage(WebsiteMessage message) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        WebsiteUserMin minUser = new WebsiteUserMin(user, false, ticket);

        WebsiteMessage wsMessage = new WebsiteMessage();

        try {

            List<WebsiteUserMin> recipients = message.getRecipients();

            if(!recipients.contains(minUser)){
                recipients.add(minUser);
            }

            StringBuilder builder = new StringBuilder();

            for(WebsiteUserMin recipient : message.getRecipients()){
                builder.append(recipient.getId() + " ");
            }

            TextMessage txtMessage = EJBLocator.lookupMessageBean().addSavedTextMessageToFolder(ticket, builder.toString().trim(), message.getThreadId(), message.getSubject(), message.getMessage());

            wsMessage = new WebsiteMessage(txtMessage, ticket);

        } catch (ConnexienceException e) {
            e.printStackTrace();
        }

        return wsMessage;
    }

    @Override
    public Response moveThread(String threadId, WebsiteMessage.MailboxFolder folder) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        String folderId = null;

        if(folder.equals(WebsiteMessage.MailboxFolder.Inbox)){
            folderId = user.getInboxFolderId();
        }

        if(folder.equals(WebsiteMessage.MailboxFolder.Sent)){
            folderId = user.getSentMessagesFolderId();
        }

        if(folder.equals(WebsiteMessage.MailboxFolder.Draft)){
            folderId = user.getDraftMessagesFolderId();
        }

        if(folder.equals(WebsiteMessage.MailboxFolder.Trash)){
            folderId = user.getTrashedMessagesFolderId();
        }

        EJBLocator.lookupMessageBean().moveThreadToFolder(ticket, threadId, folderId);

        return Response.status(Response.Status.OK).entity("{\"message\":\" Successfully moved thread " + threadId + " to the trash folder.\"}").build();
    }

    @Override
    public Response deleteThread(String threadId) throws Exception {
        Ticket ticket = TransitionSessionUtils.getTicket(request);
        EJBLocator.lookupMessageBean().addTrashedTextMessageThreadToFolder(ticket, threadId);

        return Response.status(Response.Status.OK).entity("{\"message\":\" Successfully moved thread " + threadId + " to the trash folder.\"}").build();
    }

    @Override
    public List<WebsiteMessage> getThread(String threadId, Boolean markAsRead) throws Exception{

        Ticket ticket = TransitionSessionUtils.getTicket(request);
        User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());

        List<Message> messages = EJBLocator.lookupMessageBean().getMessageThread(ticket, threadId, 0, Integer.MAX_VALUE);

        List<WebsiteMessage> wsMessages = new ArrayList<>();

        for (Message msg : messages) {
            wsMessages.add(new WebsiteMessage(msg, ticket));
        }

        if(markAsRead){
            EJBLocator.lookupMessageBean().markThreadAsRead(ticket, threadId);
        }

        return wsMessages;

    }

    @Override
    public void markThreadAsRead(String threadId){

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            EJBLocator.lookupMessageBean().markThreadAsRead(ticket, threadId);
        } catch (ConnexienceException e) {
            e.printStackTrace();
        }

    }
}
