/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.provenance.events.WorkflowSaveOperation;
import com.connexience.server.model.security.PermissionList;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.*;
import com.connexience.server.rest.api.IWorkflowsAPI;
import com.connexience.server.rest.model.*;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.rest.util.WebsiteObjectFactory;
import com.connexience.server.util.InternalProvenanceLoggerClient;
import com.connexience.server.util.SerializationUtils;
import com.connexience.server.util.StorageUtils;
import com.connexience.server.workflow.json.JSONBlockCreator;
import com.connexience.server.workflow.json.JSONDrawingExporter;
import com.connexience.server.workflow.json.JSONDrawingImporter;
import com.connexience.server.workflow.util.ZipUtils;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pipeline.core.drawing.DrawingModel;
import org.pipeline.core.drawing.model.DefaultDrawingModel;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.io.XmlDataStoreStreamReader;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import javax.imageio.ImageIO;
import org.apache.commons.lang.StringUtils;
import org.pipeline.core.drawing.gui.DefaultDrawingRenderer;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.io.XmlDataStoreByteArrayIO;

@Path("/rest/workflows")
@Stateless
@SuppressWarnings("unchecked")
public class WorkflowsAPI implements IWorkflowsAPI {

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @Override
    public WebsiteWorkflowMin[] listWorkflows() throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List workflows = EJBLocator.lookupObjectDirectoryBean().getAllObjectsUserHasAccessTo(ticket, ticket.getUserId(), WorkflowDocument.class, 0, Integer.MAX_VALUE);
        WebsiteWorkflowMin[] results = new WebsiteWorkflowMin[workflows.size()];

        for (int i = 0; i < workflows.size(); i++) {

            WorkflowDocument wf = (WorkflowDocument) workflows.get(i);

            PermissionList permissions = EJBLocator.lookupAccessControlBean().getObjectPermissions(ticket, wf.getId());

            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, wf.getCreatorId());

            results[i] = WebsiteObjectFactory.createEscWorkflowMin(wf, user, permissions);
        }

        return results;
    }

    @Override
    public WebsiteDocumentVersion saveWorkflow(String workflowId) throws Exception {
        try {
            Ticket ticket = TransitionSessionUtils.getTicket(request);

            WorkflowDocument document = null;

            JSONObject dataObject = new JSONObject(new String(ZipUtils.copyStreamToByteArray(request.getInputStream())));

            if (dataObject.has("documentId") && !dataObject.getString("documentId").equals("")) {
                document = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, dataObject.getString("documentId"));
            }

            if (document == null) {
                document = new WorkflowDocument();
                String folderId = null;
                if (ticket.getDefaultProjectId() != null) {
                    if (StringUtils.isNotEmpty(ticket.getDefaultProjectId())) {
                        Study study = EJBLocator.lookupStudyBean().getStudy(ticket, Integer.parseInt(ticket.getDefaultProjectId()));
                        folderId = study.getWorkflowFolderId();
                    }
                } else {
                    folderId = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId()).getWorkflowFolderId();
                }
                if (folderId != null) {
                    document.setContainerId(folderId);
                }
            }

            // Existing document
            if (dataObject.has("name")) {
                document.setName(dataObject.getString("name"));
            }
            if (dataObject.has("description")) {
                document.setDescription(dataObject.getString("description"));
            }
            if (dataObject.has("externalDataSupported")) {
                document.setExternalDataSupported(dataObject.getBoolean("externalDataSupported"));
            }
            if (dataObject.has("externalBlockName")) {
                document.setExternalDataBlockName(dataObject.getString("externalBlockName"));
            }
            if (dataObject.has("dynamicEngine")) {
                if (dataObject.getBoolean("dynamicEngine") == true) {
                    document.setEngineType(WorkflowDocument.DYNAMIC_ENGINE);
                } else {
                    document.setEngineType(WorkflowDocument.STATIC_ENGINE);
                }
            }
            if (dataObject.has("singleVMMode")) {
                document.setSingleVmMode(dataObject.getBoolean("singleVMMode"));
            } else {
                document.setSingleVmMode(true);
            }
            if (dataObject.has("deletedOnSuccess")) {
                if (dataObject.getBoolean("deletedOnSuccess") == true) {
                    document.setDeletedOnSuccess(true);
                } else {
                    document.setDeletedOnSuccess(false);
                }
            } else {
                document.setDeletedOnSuccess(false);
            }

            if (dataObject.has("onlyFailedOutputsUploaded")) {
                if (dataObject.getBoolean("onlyFailedOutputsUploaded") == true) {
                    document.setOnlyFailedOutputsUploaded(true);
                } else {
                    document.setOnlyFailedOutputsUploaded(false);
                }
            } else {
                document.setOnlyFailedOutputsUploaded(false);
            }

            if (dataObject.has("externalService")) {
                document.setExternalService(dataObject.getBoolean("externalService"));
            } else {
                document.setExternalService(false);
            }

            // Save the document
            document = WorkflowEJBLocator.lookupWorkflowManagementBean().saveWorkflowDocument(ticket, document);

            // Try and read the drawing JSON data
            JSONDrawingImporter importer = new JSONDrawingImporter(dataObject);
            DrawingModel drawing = importer.loadDrawing();

            // Try and render this drawing onto an Image
            int width;
            int height;
            if (dataObject.has("width")) {
                width = dataObject.getInt("width");
            } else {
                width = 640;
            }

            if (dataObject.has("height")) {
                height = dataObject.getInt("height");
            } else {
                height = 480;
            }

            BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            Graphics g = img.createGraphics();
            DefaultDrawingRenderer renderer = new DefaultDrawingRenderer(drawing);
            renderer.renderDrawing(g, width, height);

            ByteArrayOutputStream imgStream = new ByteArrayOutputStream();
            ImageIO.write(img, "jpg", imgStream);
            imgStream.flush();
            imgStream.close();
            EJBLocator.lookupObjectDirectoryBean().setImageForServerObject(ticket, document.getId(), imgStream.toByteArray(), ImageData.WORKFLOW_PREVIEW);

            XmlDataStoreByteArrayIO writer = new XmlDataStoreByteArrayIO(((XmlStorable) drawing).storeObject());
            writer.setDescriptionIncluded(true);
            byte[] drawingData = writer.toByteArray();
            DocumentVersion version = WorkflowEJBLocator.lookupWorkflowManagementBean().uploadWorkflowDocumentData(ticket, document.getId(), drawingData);
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            WorkflowSaveOperation op = new WorkflowSaveOperation(version.getDocumentRecordId(), version.getId(), version.getVersionNumber(), document.getName(), user.getDisplayName(), new Date());
            InternalProvenanceLoggerClient loggerClient = new InternalProvenanceLoggerClient();
            loggerClient.log(op);
            return WebsiteObjectFactory.createEscDocumentVersion(version);
            
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    @Override
    public Response getWorkflow(@PathParam(value = "id") String workflowId, String versionId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {

            WorkflowDocument workflow = WorkflowEJBLocator.lookupWorkflowManagementBean().getWorkflowDocument(ticket, workflowId);

            DocumentVersion version;

            if (versionId != null) {
                version = EJBLocator.lookupStorageBean().getVersion(ticket, workflowId, versionId);
            } else {
                version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, workflowId);
            }

            InputStream inStream = StorageUtils.getInputStream(ticket, workflow, version);
            XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(inStream);
            XmlDataStore workflowData = reader.read();
            inStream.close();

            DrawingModel drawing = new DefaultDrawingModel();
            ((DefaultDrawingModel) drawing).recreateObject(workflowData);
            JSONDrawingExporter exporter = new JSONDrawingExporter(drawing);
            exporter.setDescription(workflow.getDescription());
            exporter.setDocumentId(workflow.getId());
            exporter.setExternalBlockName(workflow.getExternalDataBlockName());
            exporter.setExternalDataSupported(workflow.isExternalDataSupported());
            exporter.setExternalService(workflow.isExternalService());
            exporter.setName(workflow.getName());
            exporter.setVersionId(version.getId());
            exporter.setVersionNumber(version.getVersionNumber());
            exporter.setDeletedOnSuccess(workflow.isDeletedOnSuccess());
            exporter.setOnlyFailedOutputsUploaded(workflow.isOnlyFailedOutputsUploaded());
            exporter.setSingleVmMode(workflow.isSingleVmMode());

            JSONObject drawingJson = new JSONObject();
            drawingJson.put("drawing", exporter.saveToJson());

            final String response = exporter.saveToJson().toString();
            return Response.status(Response.Status.OK).entity(response).build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Response getWorkflowThumbnail(@PathParam(value = "id") String workflowId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        ImageData image;

        try {
            image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, workflowId);

            if (image == null) {
                response.sendRedirect("../styles/common/images/workflow-example.png");
            } else {
                final ImageData profileImage = image;

                return Response.ok().entity(new StreamingOutput() {
                    @Override
                    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                        outputStream.write(profileImage.getData());
                        outputStream.close();
                    }
                }).build();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ConnexienceException e) {
            try {
                response.sendRedirect("../styles/common/images/workflow-example.png");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return Response.serverError().build();
    }

    @Override
    public Response deleteWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            WorkflowEJBLocator.lookupWorkflowManagementBean().deleteWorkflowDocument(ticket, workflowId);
        } catch (ConnexienceException ex) {
            ex.printStackTrace();

            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(response).build();
        }

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public WebsiteWorkflowInvocation executeWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation executeWorkflow(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam(value = "id") String workflowId, String documentId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
        msg.setTargetFileId(documentId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation executeWorkflowOnDocument(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId, String documentId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);
        msg.setTargetFileId(documentId);
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, WebsiteWorkflowParameterList parameters) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);
        WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);
        msg.setParameterXmlData(SerializationUtils.serialize(params));
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    /*@Override
    public String executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, String foo) throws Exception {

        System.out.println("Method Hit");

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId);

        //JSONObject object = new JSONObject(parameters);

        //WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);
        //msg.setParameterXmlData(SerializationUtils.serialize(params));
        //String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        //return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));

        return "Success";
    }*/
    @Override
    public WebsiteWorkflowInvocation executeWorkflowWithParameters(@PathParam(value = "id") String workflowId, @PathParam(value = "versionid") String versionId, WebsiteWorkflowParameterList parameters) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        WorkflowInvocationMessage msg = new WorkflowInvocationMessage(ticket, workflowId, versionId);

        WorkflowParameterList params = WebsiteObjectFactory.createEscWorkflowParameterList(parameters);

        msg.setParameterXmlData(SerializationUtils.serialize(params));
        String invocationId = WorkflowEJBLocator.lookupWorkflowEnactmentBean().startWorkflow(ticket, msg);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation[] listInvocationsOfWorkflow(@PathParam(value = "id") String workflowId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List results = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolders(ticket, workflowId);
        WebsiteWorkflowInvocation[] invocations = new WebsiteWorkflowInvocation[results.size()];

        for (int i = 0; i < results.size(); i++) {
            invocations[i] = WebsiteObjectFactory.createEscWorkflowInvocation((WorkflowInvocationFolder) results.get(i));
        }

        return invocations;
    }

    @Override
    public WebsiteWorkflowInvocation getInvocation(@PathParam(value = "id") String invocationId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public WebsiteWorkflowInvocation terminateInvocation(@PathParam(value = "id") String invocationId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        //Todo: should this return a report instead?
//		WorkflowTerminationReport report = WorkflowEJBLocator.lookupWorkflowManagementBean().terminateInvocation(ticket, invocationId);
        return WebsiteObjectFactory.createEscWorkflowInvocation(WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(ticket, invocationId));
    }

    @Override
    public Response getBlocks() throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        JSONArray root = new JSONArray();

        List<DynamicWorkflowService> services = WorkflowEJBLocator.lookupWorkflowManagementBean().listDynamicWorkflowServices(ticket);

        Collections.sort(services, new Comparator<DynamicWorkflowService>() {
            @Override
            public int compare(DynamicWorkflowService o1, DynamicWorkflowService o2) {
                return o1.getCategory().compareTo(o2.getCategory());
            }
        });

        for (DynamicWorkflowService service : services) {

            final String rawCategories = service.getCategory() == null ? "MISSING" : service.getCategory();
            final String[] categories = rawCategories.split(";");

            for (final String category : categories) {
                final String[] categoryPath = category.split("\\.");

                JSONObject parent = findOrCreateChild(root, categoryPath[0]);

                // Traverse down the way if we need
                for (int i = 1; i < categoryPath.length; i++) {
                    parent = findOrCreateChild(parent.optJSONArray("children"), categoryPath[i]);
                }

                JSONObject block = new JSONObject();
                block.put("name", service.getName());
                block.put("category", rawCategories);
                block.put("description", service.getDescription());
                block.put("serviceId", service.getId());
                block.put("creatorId", service.getCreatorId());

                if (service.getProjectFileId() != null && !service.getProjectFileId().isEmpty()) {
                    block.put("projectFileId", service.getProjectFileId());
                }

                parent.optJSONArray("blocks").put(block);
            }
        }

        JSONObject palette = new JSONObject();
        palette.put("palette", root);
        final String response = palette.toString();

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @Override
    public Response getBlock(String blockId) throws Exception {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        JSONBlockCreator creator = new JSONBlockCreator(ticket, blockId, null);
        JSONObject resultObject = new JSONObject();
        resultObject.put("block", creator.createBlockJson());

        return Response.status(Response.Status.OK).entity(resultObject.toString()).build();
    }

    // Find or Create a child object
    private JSONObject findOrCreateChild(final JSONArray list, final String needleName) throws JSONException {
        for (int i = 0; i < list.length(); i++) {
            if (list.optJSONObject(i) != null) {
                final JSONObject child = list.optJSONObject(i);

                if (needleName.equals(child.optString("name"))) {
                    return list.optJSONObject(i);
                }
            }
        }

        // If we're here, we found no child, create one.
        final JSONObject child = new JSONObject();
        child.put("name", needleName);
        child.put("children", new JSONArray());
        child.put("blocks", new JSONArray());

        list.put(child);

        return child;
    }

}
