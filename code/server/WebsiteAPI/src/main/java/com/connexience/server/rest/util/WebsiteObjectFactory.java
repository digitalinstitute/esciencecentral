/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.util;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.model.provenance.events.GraphOperation;
import com.connexience.server.model.provenance.events.UserReadOperation;
import com.connexience.server.model.provenance.events.UserWriteOperation;
import com.connexience.server.model.provenance.events.WorkflowExecuteOperation;
import com.connexience.server.model.security.PermissionList;
import com.connexience.server.model.security.User;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameter;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.rest.model.*;
import com.connexience.server.workflow.blocks.processor.DataProcessorBlock;
import com.connexience.server.workflow.json.JSONPropertiesExporter;
import com.connexience.server.workflow.json.StandardPropertiesCategoriser;
import com.connexience.server.workflow.service.DataProcessorIODefinition;
import com.connexience.server.workflow.service.DataProcessorServiceDefinition;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.RandomGUID;
import org.pipeline.core.xmlstorage.XmlDataStore;

import java.util.Vector;

/**
 * This class creates WebsiteXX objects from the standard database model objects
 *
 * @author hugo
 */
public class WebsiteObjectFactory {
    public static WebsiteDocumentVersion createEscDocumentVersion(DocumentVersion v) {
        WebsiteDocumentVersion escVersion = new WebsiteDocumentVersion();
        escVersion.setId(v.getId());
        escVersion.setDocumentRecordId(v.getDocumentRecordId());
        escVersion.setComments(v.getComments());
        escVersion.setUserId(v.getUserId());
        escVersion.setVersionNumber(v.getVersionNumber());
        escVersion.setSize(v.getSize());
        escVersion.setTimestamp(v.getTimestamp());
        escVersion.setDownloadPath("/data/" + v.getDocumentRecordId() + "/" + v.getId());
        return escVersion;
    }

	/*public static WebsiteGroup createEscGroupWithoutUsers(Group g) {
        WebsiteGroup group = new WebsiteGroup();
		group.setDescription(g.getDescription());
		group.setId(g.getId());
		group.setMembers(new ArrayList<WebsiteUser>());
		group.setName(g.getName());
		return group;
	}*/

    public static WebsiteFolder createEscFolder(Folder f) {
        WebsiteFolder escFolder = new WebsiteFolder();
        escFolder.setId(f.getId());
        escFolder.setName(f.getName());
        escFolder.setDescription(f.getDescription());
        escFolder.setCreatorId(f.getCreatorId());
        escFolder.setContainerId(f.getContainerId());
        escFolder.setProjectId(f.getProjectId());
        return escFolder;
    }

	/*public static WebsiteUser createEscUser(User u) {
		WebsiteUser escUser = new WebsiteUser();
		escUser.setId(u.getId());
		escUser.setFirstname(u.getName());
		escUser.setLastname(u.getSurname());
		return escUser;
	}*/

    public static WebsiteMetadataItem createMetadataItem(MetadataItem md) {
        WebsiteMetadataItem escMetadataItem = new WebsiteMetadataItem();
        escMetadataItem.setObjectId(md.getObjectId());
        escMetadataItem.setName(md.getName());
        escMetadataItem.setCategory(md.getCategory());
        escMetadataItem.setStringValue(md.getStringValue());
        escMetadataItem.setId(String.valueOf(md.getId()));

        if (md instanceof TextMetadata) {
            escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
        } else if (md instanceof BooleanMetadata) {
            escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.BOOLEAN);
        } else if (md instanceof NumericalMetadata) {
            escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.NUMERICAL);
        } else if (md instanceof DateMetadata) {
            escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.DATE);
        } else {
            escMetadataItem.setMetadataType(WebsiteMetadataItem.METADATA_TYPE.TEXT);
        }

        return escMetadataItem;
    }



    public static WebsiteWorkflow createEscWorkflow(WorkflowDocument wf, JSONObject drawing, User creator, PermissionList permissions) {

        WebsiteWorkflow websiteWorkflow = new WebsiteWorkflow();
        websiteWorkflow.setId(wf.getId());
        websiteWorkflow.setName(wf.getName());
        websiteWorkflow.setContainerId(wf.getContainerId());
        websiteWorkflow.setDescription(wf.getDescription());
        websiteWorkflow.setCreatorId(wf.getCreatorId());
        websiteWorkflow.setCreatorName(creator.getFirstName() + " " + creator.getSurname());
        websiteWorkflow.setCurrentVersionNumber(wf.getCurrentVersionNumber());
        websiteWorkflow.setCurrentVersionSize(wf.getCurrentVersionSize());
        websiteWorkflow.setProjectId(wf.getProjectId());
        websiteWorkflow.setTimestamp(wf.getCurrentVersionTimestamp());
        websiteWorkflow.setDrawing(drawing.toString());

        if (permissions.getSize() > 0) {
            websiteWorkflow.setShared(true);
        } else {
            websiteWorkflow.setShared(false);
        }

        return websiteWorkflow;
    }

    public static WebsiteWorkflowMin createEscWorkflowMin(WorkflowDocument wf, User creator, PermissionList permissions) {

        WebsiteWorkflowMin websiteWorkflow = new WebsiteWorkflowMin();
        websiteWorkflow.setId(wf.getId());
        websiteWorkflow.setName(wf.getName());
        websiteWorkflow.setContainerId(wf.getContainerId());
        websiteWorkflow.setDescription(wf.getDescription());
        websiteWorkflow.setCreatorId(wf.getCreatorId());
        websiteWorkflow.setCreatorName(creator.getFirstName() + " " + creator.getSurname());
        websiteWorkflow.setCurrentVersionNumber(wf.getCurrentVersionNumber());
        websiteWorkflow.setCurrentVersionSize(wf.getCurrentVersionSize());
        websiteWorkflow.setProjectId(wf.getProjectId());
        websiteWorkflow.setTimestamp(wf.getCurrentVersionTimestamp());

        if (permissions.getSize() > 0) {
            websiteWorkflow.setShared(true);
        } else {
            websiteWorkflow.setShared(false);
        }

        return websiteWorkflow;
    }

    public static WebsiteWorkflowInvocation createEscWorkflowInvocation(WorkflowInvocationFolder folder) {
        WebsiteWorkflowInvocation websiteWorkflowInvocation = new WebsiteWorkflowInvocation();
        websiteWorkflowInvocation.setId(folder.getId());
        websiteWorkflowInvocation.setWorkflowId(folder.getWorkflowId());
        websiteWorkflowInvocation.setVersionId(folder.getVersionId());
        websiteWorkflowInvocation.setInvocationDate(folder.getInvocationDate());
        websiteWorkflowInvocation.setCurrentBlockId(folder.getCurrentBlockId());
        websiteWorkflowInvocation.setEngineId(folder.getEngineId());
        websiteWorkflowInvocation.setMessage(folder.getMessage());
        websiteWorkflowInvocation.setQueuedTime(folder.getQueuedTime());
        websiteWorkflowInvocation.setDequeuedTime(folder.getDequeuedTime());
        websiteWorkflowInvocation.setExecutionStartTime(folder.getExecutionStartTime());
        websiteWorkflowInvocation.setExecutionEndTime(folder.getExecutionEndTime());
        websiteWorkflowInvocation.setName(folder.getName());
        websiteWorkflowInvocation.setContainerId(folder.getContainerId());
        websiteWorkflowInvocation.setDescription(folder.getDescription());
        websiteWorkflowInvocation.setCreatorId(folder.getCreatorId());
        websiteWorkflowInvocation.setProjectId(folder.getProjectId());
        websiteWorkflowInvocation.setPercentComplete(folder.getPercentComplete());
        websiteWorkflowInvocation.setUserId(folder.getCreatorId());

        switch (folder.getInvocationStatus()) {
            case WorkflowInvocationFolder.INVOCATION_FINISHED_OK:
                websiteWorkflowInvocation.setStatus("Finished");
                break;

            case WorkflowInvocationFolder.INVOCATION_FINISHED_WITH_ERRORS:
                websiteWorkflowInvocation.setStatus("ExecutionError");
                break;

            case WorkflowInvocationFolder.INVOCATION_RUNNING:
                websiteWorkflowInvocation.setStatus("Running");
                break;

            case WorkflowInvocationFolder.INVOCATION_WAITING:
                websiteWorkflowInvocation.setStatus("Queued");
                break;

            case WorkflowInvocationFolder.INVOCATION_WAITING_FOR_DEBUGGER:
                websiteWorkflowInvocation.setStatus("Debugging");
                break;

            default:
                websiteWorkflowInvocation.setStatus("Unknown");
        }

        return websiteWorkflowInvocation;
    }

    public static WorkflowParameter createEscWorkflowParameter(WebsiteWorkflowParameter websiteWorkflowParameter) {
        WorkflowParameter p = new WorkflowParameter();
        p.setBlockName(websiteWorkflowParameter.getBlockName());
        p.setName(websiteWorkflowParameter.getName());
        p.setValue(websiteWorkflowParameter.getValue());
        return p;
    }

    public static WorkflowParameterList createEscWorkflowParameterList(WebsiteWorkflowParameterList websiteWorkflowParameterList) {
        WorkflowParameterList p = new WorkflowParameterList();
        WebsiteWorkflowParameter[] values = websiteWorkflowParameterList.getValues();
        for (int i = 0; i < values.length; i++) {
            p.add(values[i].getBlockName(), values[i].getName(), values[i].getValue());
        }
        return p;
    }


    public static WebsiteNotification createNotification(GraphOperation g) {
        WebsiteNotification n = new WebsiteNotification();
        n.put("userId", g.getUserId());
        n.setTimestamp(g.getTimestamp().getTime());


        if (g instanceof UserReadOperation) {

            n.setType(WebsiteNotification.WEBSITE_NOTIFICATION_TYPE.USER_READ);

            UserReadOperation op = (UserReadOperation) g;
            n.put("documentId", op.getDocumentId());
            n.put("versionId", op.getVersionId());
            n.put("documentName", op.getDocumentName());
            n.put("extension", op.getDocumentName().substring(op.getDocumentName().lastIndexOf('.') + 1));
            n.put("versionNum", op.getVersionNumber());
            n.put("userId", op.getUserId());
            n.put("username", op.getUsername());
            n.put("projectId", op.getProjectId());

        } else if (g instanceof UserWriteOperation) {

            n.setType(WebsiteNotification.WEBSITE_NOTIFICATION_TYPE.USER_WRITE);

            UserWriteOperation op = (UserWriteOperation) g;
            n.put("documentId", op.getDocumentId());
            n.put("versionId", op.getVersionId());
            n.put("documentName", op.getDocumentName());
            n.put("extension", op.getDocumentName().substring(op.getDocumentName().lastIndexOf('.') + 1));
            n.put("versionNum", op.getVersionNumber());
            n.put("userId", op.getUserId());
            n.put("username", op.getUsername());
            n.put("projectId", op.getProjectId());

        } else if (g instanceof WorkflowExecuteOperation) {

            n.setType(WebsiteNotification.WEBSITE_NOTIFICATION_TYPE.WORKFLOW_EXECUTE);

            WorkflowExecuteOperation op = (WorkflowExecuteOperation) g;
            n.put("workflowName", op.getName());
            n.put("workflowId", op.getWorkflowId());
            n.put("versionId", op.getVersionId());
            n.put("parentWorkflowId", op.getParentWorkflowId());
            n.put("parentInvocationId", op.getParentInvocationId());
            n.put("parentVersionId", op.getParentWorkflowVersionId());
            n.put("workflowId", op.getWorkflowId());
            n.put("projectId", op.getProjectId());

        } else {
            throw new IllegalArgumentException("Not yet implemented for " + g.getClass().getSimpleName());
        }

        return n;
    }


}
