/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.util;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.SessionUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * TransitionSessionUtils allows API requests to function using both the JWT style Authorization (stateless) and the
 * HTTP Session based auth (stateful).
 * <p/>
 * Eventually we will transition away from the dependence on SessionUtils behind the scenes.
 */
public class TransitionSessionUtils {
	private final static String ATTRIBUTE_TICKET = "com.connexience.ticket";
	private final static String ATTRIBUTE_JWT = "com.connexience.jwt";

	/**
	 * Get the current WebTicket
	 */
	public static WebTicket getTicket(HttpServletRequest request) {
		if (request.getAttribute(ATTRIBUTE_TICKET) != null) {
			return (WebTicket) request.getAttribute(ATTRIBUTE_TICKET);
		} else {
			return SessionUtils.getTicket(request);
		}
	}

	/**
	 * Get the current organisation
	 */
	public static Organisation getOrganisation(HttpServletRequest request) {
		try {
			return EJBLocator.lookupOrganisationDirectoryBean().getDefaultOrganisation(getTicket(request));
		} catch (ConnexienceException e) {
			return null;
		}
	}
}
