/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.storage.MetaDataRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.rest.api.IImageAPI;
import com.connexience.server.rest.api.IUsersAPI;
import com.connexience.server.rest.exceptions.ConnexienceWebApplicationException;
import com.connexience.server.rest.model.WebsiteSignupUser;
import com.connexience.server.rest.model.WebsiteUser;
import com.connexience.server.rest.util.APIUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.DigestBuilder;
import com.connexience.server.util.StorageUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This class implements the user management in the website API. Listing,
 * updating, etc.
 * <p/>
 * User: nsjw7 Date: 29/11/2012 Time: 15:20
 */
@SuppressWarnings("unchecked")
@Path("/rest/image")
public class ImageAPI implements IImageAPI {

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/MetaDataBean")
    MetaDataRemote metaDataBean;

    @EJB(lookup = "java:global/esc/server-beans/GroupDirectoryBean")
    GroupDirectoryRemote groupDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/UserDirectoryBean")
    UserDirectoryRemote userDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;


    @Override
    public Response getImage(@PathParam("id") String objectId, @Context HttpServletRequest request) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        ImageData image = null;

        try
        {
            image = EJBLocator.lookupObjectDirectoryBean().getImageForServerObject(ticket, objectId);

            if (image == null)
            {
                String currentPath = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

                if(EJBLocator.lookupPreferencesBean().booleanValue("Website", "UseGravatar", true)){
                    try {
                        UserProfile userProfile = EJBLocator.lookupUserDirectoryBean().getUserProfile(ticket, objectId);

                        String baseUrl = "http://www.gravatar.com/avatar/";
                        String referer = request.getHeader("referer");
                        if(request.isSecure() || (referer != null && referer.startsWith("https"))){
                            baseUrl = "https://www.gravatar.com/avatar/";
                        }

                        String hash = DigestBuilder.calculateMD5(userProfile.getEmailAddress());

                        if(request.getServerName().equals("localhost")){
                            response.sendRedirect(baseUrl + hash);
                        }
                        else {
                            String defaultImage = currentPath + "/styles/common/images/profile.300x300.png";
                            response.sendRedirect(baseUrl + hash + "&d=" + defaultImage);
                        }
                    }
                    catch(Exception e) {
                        response.sendRedirect("../styles/common/images/profile.300x300.png");
                    }
                }
                else {
                    response.sendRedirect("../styles/common/images/profile.300x300.png");
                }
            }
            else {
                final ImageData profileImage = image;

                return Response.ok().entity(new StreamingOutput() {
                    @Override
                    public void write(OutputStream outputStream) throws IOException, WebApplicationException {
                        outputStream.write(profileImage.getData());
                        outputStream.close();
                    }
                }).build();
            }
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        catch (ConnexienceException e)
        {
            try {
                response.sendRedirect("../styles/common/images/profile.png");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return Response.serverError().build();
    }

    @Override
    public Response saveProfilePicture(@PathParam("userId") String userId, @Context HttpServletRequest request) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        System.out.println("File for userID " + userId);

        if (request != null) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            long contentLength = Long.parseLong(request.getHeader("Content-Length"));

            try {
                List items = upload.parseRequest(request);
                Iterator iter = items.iterator();

                ArrayList<String> fileNames = new ArrayList<>();
                ArrayList<Long> sizes = new ArrayList<>();

                while (iter.hasNext()) {
                    FileItem item = (FileItem) iter.next();

                    if (!item.isFormField()) {
                        fileNames.add(item.getName());
                        sizes.add(item.getSize());

                        User user = userDirectoryBean.getUser(ticket, userId);
                        String dataFolderId = user.getHomeFolderId();

                        DocumentRecord doc = new DocumentRecord();
                        doc.setName(item.getName());
                        doc.setContainerId(dataFolderId);
                        doc.setCreatorId(ticket.getUserId());
                        doc.setProjectId(ticket.getDefaultProjectId());
                        doc = storageBean.saveDocumentRecord(ticket, doc);

                        StorageUtils.upload(ticket, item.getInputStream(), contentLength, doc, "Saved From User " + user.getDisplayName());
                    }
                }

                //todo: March 2016: This JSON response is what was required by JQueryUpload and can be changed as required
                String response = "{\"files\": [";

                for (int i = 0; i < fileNames.size(); i++) {
                    String filename = fileNames.get(i);
                    long size = sizes.get(i);
                    response += "{"
                            + "\"name\": \"" + filename + "\","
                            + "\"size\": " + size + ","
                            + "\"url\": \"http://example.org/files/picture1.jpg\","
                            + "\"thumbnailUrl\": \"http://example.org/files/thumbnail/picture1.jpg\","
                            + "\"deleteUrl\": \"http://example.org/files/picture1.jpg\","
                            + "\"deleteType\": \"DELETE\""
                            + "},";
                }

                response = response.substring(0, response.length() - 1);

                response += "]}";

                return Response.status(Response.Status.OK).entity(response).build();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        } else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        return Response.status(Response.Status.NO_CONTENT).build();
    }
}
