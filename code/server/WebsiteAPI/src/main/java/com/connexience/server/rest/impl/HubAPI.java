/**
 * e-Science Central
 * Copyright (C) 2008-2016 Inkspot Science Ltd.
 * <p>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.project.HubRemote;
import com.connexience.server.ejb.project.LoggersRemote;
import com.connexience.server.ejb.project.StudyRemote;
import com.connexience.server.ejb.project.SubjectsRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.model.project.study.*;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.rest.api.IHubAPI;
import com.connexience.server.rest.model.project.study.WebsiteHub;
import com.connexience.server.rest.model.project.study.WebsiteHubDeployment;
import com.connexience.server.rest.model.project.study.WebsiteHubType;
import com.connexience.server.rest.model.project.study.WebsiteLoggerDeployment;
import com.connexience.server.rest.util.TransitionSessionUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA. User: nmdt3 Date: 27/08/2013 Time: 20:41
 */
@SuppressWarnings("unchecked")
@Path("/rest/hub")
@Stateless
public class HubAPI implements IHubAPI {
    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;

    @EJB(lookup = "java:global/esc/server-beans/HubBean")
    HubRemote hubBean;

    @EJB(lookup = "java:global/esc/server-beans/StudyBean")
    StudyRemote studyBean;

    @EJB(lookup = "java:global/esc/server-beans/SubjectsBean")
    SubjectsRemote subjectBean;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;


    @Override
    public Collection<WebsiteHubDeployment> getSubjectGroupHubDeployments(int studyId,
                                                                          int groupId,
                                                                          Boolean includeInactive) throws ConnexienceException {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        final Collection<WebsiteHubDeployment> deployments = new ArrayList<>();

        for (HubDeployment deployment : em.merge(subjectBean.getSubjectGroup(ticket, groupId)).getHubDeployments()) {

            if (includeInactive) {
                deployments.add(new WebsiteHubDeployment(em.merge(deployment)));
            } else if (deployment.isActive()) {
                deployments.add(new WebsiteHubDeployment(em.merge(deployment)));
            }
        }

        return deployments;
    }

    @Override
    public WebsiteHubDeployment saveHubDeployment(int studyId, int subjectGroupId, WebsiteHubDeployment websiteHubDeployment) throws ConnexienceException {
        System.out.println("HubAPI.saveHubDeployment");

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        HubDeployment deployment = null;

        Integer deploymentId = websiteHubDeployment.getId();

        if (websiteHubDeployment.getId() != null) {

            //update
            deployment = hubBean.getDeployment(ticket, deploymentId);

            if(deployment != null){
                deployment .setAdditionalProperties(websiteHubDeployment.getAdditionalProperties());
                deployment = em.merge(deployment );
            }
            else {
                throw new ConnexienceException("Cannot find hub deployment with Id: " + deployment.getId());
            }

        } else {

            //create new

            Hub hub = em.find(Hub.class, websiteHubDeployment.getHub().getId());
            SubjectGroup subjectGroup = em.find(SubjectGroup.class, subjectGroupId);
            deployment = em.merge(hubBean.deployHub(ticket, hub, subjectGroup, websiteHubDeployment.getStartDate(), websiteHubDeployment.getEndDate()));
        }

        return new WebsiteHubDeployment(deployment);
    }

    @Override
    public Response removeHubDeployment( int deploymentId) throws ConnexienceException {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        HubDeployment deployment = em.merge(hubBean.getDeployment(ticket, deploymentId));

        deployment.setActive(false);

        deployment.setActive(false);
        hubBean.updateHubDeployment(ticket, deployment);

        return Response.status(Response.Status.OK).build();
    }

    @Override
    public List<WebsiteHub> getHubs() throws ConnexienceException{
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteHub> results = new ArrayList<>();
        List<Hub> hubs = hubBean.getHubs(ticket, 0, 0);

        for(Hub hub : hubs){
            hub = em.merge(hub);
            results.add(new WebsiteHub(hub));
        }
        return results;
    }

    @Override
    public List<WebsiteHubType> getHubTypes() throws ConnexienceException{
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteHubType> results = new ArrayList<>();
        List<HubType> hubtypes = hubBean.getHubTypes(ticket, 0, 0);

        for(HubType hubtype : hubtypes){
            hubtype = em.merge(hubtype);
            results.add(new WebsiteHubType(hubtype));
        }
        return results;
    }


}
