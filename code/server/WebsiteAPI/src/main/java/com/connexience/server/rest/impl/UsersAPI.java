/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.rest.impl;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.directory.GroupDirectoryRemote;
import com.connexience.server.ejb.directory.UserDirectoryRemote;
import com.connexience.server.ejb.storage.MetaDataRemote;
import com.connexience.server.ejb.storage.StorageRemote;
import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.image.ImageData;
import com.connexience.server.model.metadata.SearchOrder;
import com.connexience.server.model.metadata.types.OrderBy;
import com.connexience.server.model.project.Project;
import com.connexience.server.model.project.Uploader;
import com.connexience.server.model.project.study.Study;
import com.connexience.server.model.scanner.RemoteFilesystemScanner;
import com.connexience.server.model.scanner.filesystems.AzureScanner;
import com.connexience.server.model.scanner.filesystems.S3Scanner;
import com.connexience.server.model.security.StoredCredentials;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.security.User;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.model.security.credentials.AmazonCredentials;
import com.connexience.server.model.security.credentials.AzureCredentials;
import com.connexience.server.model.social.profile.UserProfile;
import com.connexience.server.rest.api.IUsersAPI;
import com.connexience.server.rest.exceptions.ConnexienceWebApplicationException;
import com.connexience.server.rest.model.WebsiteSignupUser;
import com.connexience.server.rest.model.WebsiteUser;
import com.connexience.server.rest.model.project.UploadUserProject;
import com.connexience.server.rest.util.APIUtils;
import com.connexience.server.rest.util.TransitionSessionUtils;
import com.connexience.server.util.DigestBuilder;
import com.connexience.server.util.StorageUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * This class implements the user management in the website API. Listing,
 * updating, etc.
 * <p/>
 * User: nsjw7 Date: 29/11/2012 Time: 15:20
 */
@SuppressWarnings("unchecked")
@Path("/rest/users")
public class UsersAPI implements IUsersAPI {

    @Context
    HttpServletRequest request;

    @Context
    HttpServletResponse response;

    @EJB(lookup = "java:global/esc/server-beans/MetaDataBean")
    MetaDataRemote metaDataBean;

    @EJB(lookup = "java:global/esc/server-beans/GroupDirectoryBean")
    GroupDirectoryRemote groupDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/UserDirectoryBean")
    UserDirectoryRemote userDirectoryBean;

    @EJB(lookup = "java:global/esc/server-beans/StorageBean")
    StorageRemote storageBean;

    @Override
    public List<WebsiteUser> getUsers(String q, OrderBy orderBy, SearchOrder ascDesc, int pageNum, int pageSize, Boolean metadata, List<String> userId) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        List<WebsiteUser> users = new ArrayList<>();
        List<User> connexienceUsers = new ArrayList<>();

        try {

            if (!q.equals("")) {
                connexienceUsers = EJBLocator.lookupUserDirectoryBean().searchForUsers(ticket, q, orderBy, ascDesc, pageNum, pageSize);
            } else if (userId.size() > 0) {
                for (String connexienceId : userId) {
                    connexienceUsers.add(EJBLocator.lookupUserDirectoryBean().getUser(ticket, connexienceId));
                }
            } else {

                connexienceUsers = EJBLocator.lookupOrganisationDirectoryBean().listNonProtectedOrganisationUsers(ticket, orderBy, ascDesc, APIUtils.getFirstResult(pageNum, pageSize), pageSize);
            }

            for (User connexienceUser : connexienceUsers) {
                users.add(new WebsiteUser(connexienceUser, metadata, ticket));
            }

            return users;
        } catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.BAD_REQUEST);
        }
    }

    @Override
    public WebsiteUser registerUser(WebsiteSignupUser user) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            User connexienceUser = new User();
            connexienceUser.setFirstName(user.getFirstname());
            connexienceUser.setSurname(user.getLastname());

            return new WebsiteUser(userDirectoryBean.createAccount(connexienceUser, user.getEmail(), user.getPassword()), false, ticket);
        } catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public WebsiteUser getUser(String userId, Boolean metadata) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            return new WebsiteUser(EJBLocator.lookupUserDirectoryBean().getUser(ticket, userId), metadata, ticket);
        } catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public WebsiteUser saveUser(String userId, WebsiteUser user) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            return WebsiteUser.saveUser(user, ticket);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public WebsiteUser findUser(@PathParam("username") String username, Boolean metadata) {

        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            return new WebsiteUser(EJBLocator.lookupUserDirectoryBean().getUserFromLogonName(username), metadata, ticket);
        } catch (ConnexienceException ex) {
            ex.printStackTrace();
            throw new ConnexienceWebApplicationException(ex.getMessage(), Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public WebsiteUser getCurrentUser(Boolean metadata) {
        Ticket ticket = TransitionSessionUtils.getTicket(request);

        try {
            User user = EJBLocator.lookupUserDirectoryBean().getUser(ticket, ticket.getUserId());
            Long messageCount = EJBLocator.lookupMessageBean().getNumberOfTextMessgesInFolder(ticket, user.getInboxFolderId());
        }
        catch(ConnexienceException ex){

        }

        WebsiteUser user = getUser(ticket.getUserId(), metadata);



        String publicUserId = TransitionSessionUtils.getOrganisation(request).getDefaultUserId();

        //Throw a 401 exception if the user is the public user
        if (user.getId().equals(publicUserId)) {
            throw new WebApplicationException(HttpURLConnection.HTTP_UNAUTHORIZED);
        } else {
            return getUser(ticket.getUserId(), metadata);
        }
    }
}
