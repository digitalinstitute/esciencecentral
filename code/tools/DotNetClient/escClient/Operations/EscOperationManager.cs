﻿using System;
using System.Collections.Generic;

namespace Esc.Client {
	public class EscOperationManager {
		private List<EscOperation> operations = new List<EscOperation>();
		private string hostname;
		private int port;
		private bool secure;
		private	string username;
		private string password;

		private StorageAPI storageClient = null;
		public EscOperationManager (string hostname, int port, bool secure, string username, string password) {
			this.hostname = hostname;
			this.port = port;
			this.secure = secure;
			this.username = username;
			this.password = password;
		}

		public EscOperationManager(GenericClient client){
			hostname = client.Hostname;
			port = client.Port;
			secure = client.Secure;
			username = client.Username;
			password = client.Password;
		}

		public void PerformOperation(EscOperation operation){
			operations.Add (operation);
			operation.SetupFromManager (this);
			operation.OperationFinished += this.OperationCompleted;
			operation.StartOperation ();
		}

		public void OperationCompleted(EscOperation operation){
			operations.Remove (operation);
		}

		public StorageAPI CreateStorageClient(){
			if (storageClient != null) {
				return storageClient;
			} else {
				storageClient = new StorageAPI (hostname, port, secure, username, password);
				return storageClient;
			}
		}
	}
}