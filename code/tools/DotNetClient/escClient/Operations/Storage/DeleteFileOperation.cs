﻿using System;

namespace Esc.Client
{
	public class DeleteFileOperation : StorageOperation {
		private string documentId;

		public DeleteFileOperation (EscDocument document) : base() {
			documentId = document.id;
		}

		public DeleteFileOperation(string documentId) : base(){
			this.documentId = documentId;
		}

		protected override void PerformOperation(){
			Client.DeleteDocument(documentId);
		}
	}
}