﻿using System;
using System.Collections.Generic;

namespace Esc.Client {
	public class ListChildFoldersOperation : StorageOperation {
		string parentFolderId;

		private List<EscFolder> results = new List<EscFolder>();
		public List<EscFolder> Results {
			get{ return results; }
		}

		public ListChildFoldersOperation (EscFolder parentFolder) : base() {
			this.parentFolderId = parentFolder.id;
		}

		public ListChildFoldersOperation (string parentFolderId) : base() {
			this.parentFolderId = parentFolderId;
		}

		protected override void PerformOperation () {
			EscFolder[] folders = Client.ListChildFolders (parentFolderId);
			results = new List<EscFolder> ();
			foreach (EscFolder f in folders) {
				results.Add (f);
			}
		}
	}
}

