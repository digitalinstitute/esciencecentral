﻿using System;
using System.IO;
using System.Net;

namespace Esc.Client
{
	public class DownloadFileOperation : StorageOperation
	{
		private EscDocument document;
		private EscDocumentVersion version = null;
		private DirectoryInfo targetDir;
		private FileInfo result;
		public FileInfo Result {
		 	get{ return result; }
		}

		public DownloadFileOperation (EscDocument document, DirectoryInfo targetDir) : base () {
			this.targetDir = targetDir;
			this.document = document;
		}

		public DownloadFileOperation(EscDocument document, string targetPath) : base(){
			this.targetDir = new DirectoryInfo (targetPath);
			this.document = document;
		}

		public DownloadFileOperation (EscDocument document, EscDocumentVersion version, DirectoryInfo targetDir) : base () {
			this.targetDir = targetDir;
			this.document = document;
			this.version = version;
		}

		public DownloadFileOperation(EscDocument document, EscDocumentVersion version, string targetPath) : base(){
			this.targetDir = new DirectoryInfo (targetPath);
			this.document = document;
			this.version = version;
		}
			
		protected override void PerformOperation() {
			string filePath = targetDir.FullName + Path.DirectorySeparatorChar + document.name;
			result = new FileInfo (filePath);
			FileStream outStream = result.OpenWrite ();
			HttpWebRequest request;
			if (version == null) {
				// Latest version
				request = Client.createRequest (document.downloadPath);
			} else {
				// Specific version
				if(version.documentRecordId.Equals(document.id)){
					request = Client.createRequest (version.downloadPath);
				} else {
					throw new Exception("Document ID does not match ID specified in version");
				}
			}

			request.Method = "GET";
			Stream rs = request.GetResponse ().GetResponseStream ();
			double totalWork = (double)request.GetResponse ().ContentLength;
			long totalRead = 0;
			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = rs.Read(buffer, 0, buffer.Length)) != 0) {
				outStream.Write(buffer, 0, bytesRead);
				totalRead += bytesRead;
				UpdatePercentage (totalWork, (double)totalRead);
			}
			Console.Out.WriteLine (totalRead + " bytes processed");
			outStream.Close ();
			rs.Close ();
		}
	}
}