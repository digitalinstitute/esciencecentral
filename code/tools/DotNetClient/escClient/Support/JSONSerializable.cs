using System;
using System.Collections;

namespace Esc.Client
{
	public interface JSONSerializable
	{
		string ToJson();
		void ParseJson(string jsonString);
		void ParseHashtable(Hashtable ht);
	}
}

