using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Text;
using Procurios.Public;

namespace Esc.Client
{
	public class GenericClient
	{
		protected string urlBase;
		protected string username;
		public string Username{
			get{ return username; }
		}


		protected string password;
		public string Password {
			get{ return password; }
		}
			
		protected string hostname;
		public string Hostname {
			get{ return hostname; }
		}

		protected int port;
		public int Port {
			get{return port;}
		}

		protected bool secure;
		public bool Secure {
			get{return secure;}
		}

		public GenericClient (string urlBase, string hostname, int port, bool secure, string username, string password)
		{
			this.urlBase = urlBase;
			this.hostname = hostname;
			this.port = port;
			this.secure = secure;
			this.username = username;
			this.password = password;
		}

		public HttpWebRequest createRequest(string path){

			HttpWebRequest req;
			if (secure) {
				req = (HttpWebRequest)WebRequest.Create ("https://" + hostname + ":" + port + urlBase + path);
			} else {
				req = (HttpWebRequest)WebRequest.Create ("http://" + hostname + ":" + port + urlBase + path);
			}
			string authData = username + ":" + password;
			byte[] authBytes = System.Text.Encoding.ASCII.GetBytes (authData);
			string encodedAuth = Convert.ToBase64String (authBytes);
			req.Headers.Add ("Authorization:Basic " + encodedAuth);
			return req;
		}

		public string retrieveJson(string path){
			HttpWebRequest request = createRequest (path);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string result = reader.ReadToEnd ();
			return result;
		}


		public ArrayList retrieveJsonArray(string path){
			HttpWebRequest request = createRequest (path);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string result = reader.ReadToEnd ();
			object jsonObject = JSON.JsonDecode (result);

			if (jsonObject is ArrayList) {
				return (ArrayList)jsonObject;
			} else {
				return null;
			}
		}

		public void postText(string path, string postData){
			HttpWebRequest request = createRequest (path);
			request.Method = "POST";
			request.ContentType = "text/plain";
			byte[] byteArray = Encoding.UTF8.GetBytes (postData);
			request.ContentLength = byteArray.Length;
			Stream dataStream = request.GetRequestStream ();
			dataStream.Write (byteArray, 0, byteArray.Length);
			dataStream.Close ();
			WebResponse response = request.GetResponse ();

		}

		public string postTextRetrieveJson(string path, string postData){
			HttpWebRequest request = createRequest (path);
			request.Method = "POST";
			request.ContentType = "text/plain";
			byte[] byteArray = Encoding.UTF8.GetBytes (postData);
			request.ContentLength = byteArray.Length;
			Stream dataStream = request.GetRequestStream ();
			dataStream.Write (byteArray, 0, byteArray.Length);
			dataStream.Close ();
			WebResponse response = request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string result = reader.ReadToEnd ();
			return result;
		}

		public string postJsonRetrieveJson(string path, string postData){
			HttpWebRequest request = createRequest (path);
			request.Method = "POST";
			request.ContentType = "application/json";
			byte[] byteArray = Encoding.UTF8.GetBytes (postData);
			request.ContentLength = byteArray.Length;
			request.SendChunked = false;
			Stream dataStream = request.GetRequestStream ();
			dataStream.Write (byteArray, 0, byteArray.Length);
			dataStream.Close ();
			WebResponse response = request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string result = reader.ReadToEnd ();
			return result;
		}
	}
}

