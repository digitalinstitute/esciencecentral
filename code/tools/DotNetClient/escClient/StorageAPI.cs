using System;
using System.Net;
using System.IO;
using System.Collections;

namespace Esc.Client
{
	public class StorageAPI : GenericClient
	{
		public StorageAPI(string hostname, int port, bool secure, string username, string password) : base ("/api/public/rest/v1/storage", hostname, port, secure, username, password) {

		} 

		public EscFolder HomeFolder() {
			string json = retrieveJson ("/specialfolders/home");
			return new EscFolder (json);
		}

		public EscDocument[] FolderDocuments(string id){
			ArrayList list = retrieveJsonArray ("/folders/" + id + "/documents");
			EscDocument[] results = new EscDocument[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscDocument ((Hashtable)list [i]);
			}
			return results;
		}

		public EscFolder[] ListChildFolders(string id){
			ArrayList list = retrieveJsonArray ("/folders/" + id + "/children");
			EscFolder[] results = new EscFolder[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscFolder ((Hashtable)list [i]);
			}
			return results;
		}

		public EscDocument GetDocument(string id){
			string json = retrieveJson ("/documents/" + id);
			return new EscDocument (json);
		}

		public EscFolder GetFolder(string id){
			string json = retrieveJson ("/folders/" + id);
			return new EscFolder (json);
		}

		public EscUser CurrentUser(){
			return new EscUser(retrieveJson("/currentuser"));
		}

		public EscDocumentVersion[] ListDocumentVersions(string id){
			ArrayList list = retrieveJsonArray("/documents/" + id + "/versions");
			EscDocumentVersion[] results = new EscDocumentVersion[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscDocumentVersion ((Hashtable)list [i]);
			}
			return results;
		}

		public EscDocumentVersion GetDocumentVersion(string id){
			return new EscDocumentVersion (retrieveJson ("/documentversions/" + id));
		}

		public EscProject[] ListProjects(){
			ArrayList list = retrieveJsonArray ("/projects");
			EscProject[] results = new EscProject[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscProject ((Hashtable)list [i]);
			}
			return results;
		}

		public EscFolder CreateChildFolder(string id, string name){
			return new EscFolder(postTextRetrieveJson("/folders/" + id, name));
		}

		public EscDocument CreateDocumentInFolder(string id, string name){
			return new EscDocument (postTextRetrieveJson ("/folders/" + id + "/documents/create", name));
		}

		public EscDocumentVersion GetLatestDocumentVersion(string documentId){
			return new EscDocumentVersion(retrieveJson("/documents/" + documentId + "/versions/latest"));
		}

		public EscDocument UpdateDocument(EscDocument document){
			if (document.id!=null) {
				return new EscDocument (postTextRetrieveJson ("/ptdocuments" , document.ToJson ()));
			} else {
				return null;
			}
		}

		public EscFolder UpdateFolder(EscFolder folder){
			if (folder.id!=null) {
				return new EscFolder (postTextRetrieveJson ("/ptfolders" , folder.ToJson ()));
			} else {
				return null;
			}
		}

		public void DeleteDocument(string id){
			postText ("/deletedocument", id);
		}

		public void DeleteFolder(string id){
			postText ("/deletefolder", id);
		}

		public EscMetadataItem[] GetDocumentMetadata(string id) {
			ArrayList list = retrieveJsonArray("/documents/" + id + "/metadata");
			EscMetadataItem[] results = new EscMetadataItem[list.Count];
			for (int i = 0; i < list.Count; i++) {
				results [i] = new EscMetadataItem ((Hashtable)list [i]);
			}
			return results;
		}

		public EscMetadataItem AddMetadataToDocument(string id, EscMetadataItem metadataItem) {
			metadataItem.objectId = id;
			string sendJson = metadataItem.ToJson ();
			Console.Out.WriteLine (sendJson);
			string json = postTextRetrieveJson ("/documents/" + id + "/ptmetadata", sendJson);
			return new EscMetadataItem (json);
		}

		public long GetTimestamp(){
			return long.Parse (retrieveJson ("/timestamp"));
		}

		public EscDocumentVersion UploadFile(EscDocument document, string file){
			FileStream f = new FileStream (file, FileMode.Open);
			HttpWebRequest request = createRequest ("/data/" + document.id);
			request.Method = "POST";
			request.ContentType = "application/data";
			request.ContentLength = f.Length;
			Stream rs = request.GetRequestStream();

			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = f.Read(buffer, 0, buffer.Length)) != 0) {
				rs.Write(buffer, 0, bytesRead);
			}
			f.Close ();
			rs.Close ();

			WebResponse response = request.GetResponse ();
			Stream stream = response.GetResponseStream ();
			StreamReader reader = new StreamReader (stream);
			string result = reader.ReadToEnd ();
			response.Close ();
			return GetDocumentVersion (result);
		}
	}
}