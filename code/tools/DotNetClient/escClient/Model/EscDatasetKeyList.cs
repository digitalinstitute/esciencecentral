using System;
using System.Collections;
using Procurios.Public;


namespace Esc.Client {
	public class EscDatasetKeyList : JSONSerializable {
		private ArrayList keys = new ArrayList();

		public EscDatasetKeyList () {
		}

		public EscDatasetKeyList(string json){
			ParseJson (json);
		}

		public EscDatasetKeyList(Hashtable ht){
			ParseHashtable (ht);
		}

		public void AddKey(string key){
			keys.Add (key);
		}

		public int KeyCount(){
			return keys.Count;
		}

		public string getKey(int index){
			return (String)keys [index];
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("keys", keys);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);

		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}