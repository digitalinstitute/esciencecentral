using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client {
	public class EscDataset : JSONSerializable {
		public string id;
		public string name;
		public string description;
		public string projectId;
		public string creatorId;

		public EscDataset () {
		}

		public EscDataset(string json){
			ParseJson (json);
		}

		public EscDataset(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("creatorId", creatorId);
			ht.Add ("projectId", projectId);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			creatorId = ht.getString ("creatorId", "");
			projectId = ht.getString ("projectId", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}