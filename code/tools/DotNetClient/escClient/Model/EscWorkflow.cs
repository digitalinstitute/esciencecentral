using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscWorkflow : JSONSerializable
	{
		public String id;
		public String name;
		public String containerId;
		public String description;
		public String creatorId;
		public long currentVersionSize = 0;
		public int currentVersionNumber = 0;
		public String projectId;


		public EscWorkflow ()
		{
		}

		public EscWorkflow(string json){
			ParseJson (json);
		}

		public EscWorkflow(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("containerId", containerId);
			ht.Add ("creatorId", creatorId);
			ht.Add ("currentVersionNumber", currentVersionNumber);
			ht.Add ("currentVersionSize", currentVersionSize);
			ht.Add ("projectId", "");
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			containerId = ht.getString ("containerId", "");
			creatorId = ht.getString ("creatorId", "");
			currentVersionSize = ht.getLong ("currentVersionSize", 0L);
			currentVersionNumber = ht.getInt ("currentVersionNumber", 0);
			projectId = ht.getString ("projectId", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}

