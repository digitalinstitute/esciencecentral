using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client {
	public class EscDatasetItem	: JSONSerializable {
		/** The basic format of the item */
		public enum DATASET_ITEM_TYPE {
			MULTI_ROW, SINGLE_ROW
		}

		/** The strategy that will be applied to updates */
		public enum DATASET_ITEM_UPDATE_STRATEGY {
			REPLACE, MINIMUM, MAXIMUM, AVERAGE, SUM
		}

		public long id;
		public string name;
		public string datasetId;
		public DATASET_ITEM_TYPE itemType = DATASET_ITEM_TYPE.MULTI_ROW;
		public DATASET_ITEM_UPDATE_STRATEGY updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.REPLACE;

		public EscDatasetItem () {
		}

		public EscDatasetItem(string json){
			ParseJson (json);
		}

		public EscDatasetItem(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("datasetId", datasetId);

			if (itemType == DATASET_ITEM_TYPE.SINGLE_ROW) {
				ht.Add ("itemType", "singlerow");
			} else if (itemType == DATASET_ITEM_TYPE.MULTI_ROW) {
				ht.Add ("itemType", "multirow");
			}

			if (updateStrategy == DATASET_ITEM_UPDATE_STRATEGY.AVERAGE) {
				ht.Add ("updateStrategy", "average");

			} else if (updateStrategy == DATASET_ITEM_UPDATE_STRATEGY.MAXIMUM) {
				ht.Add ("updateStrategy", "maximum");

			} else if (updateStrategy == DATASET_ITEM_UPDATE_STRATEGY.MINIMUM) {
				ht.Add ("updateStrategy", "minimum");

			} else if (updateStrategy == DATASET_ITEM_UPDATE_STRATEGY.REPLACE) {
				ht.Add ("updateStrategy", "replace");
			
			} else if(updateStrategy==DATASET_ITEM_UPDATE_STRATEGY.SUM){
				ht.Add ("updateStrategy", "sum");

			} else {
				ht.Add ("updateStrategy", "replace");

			}

			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getLong ("id", 0L);
			name = ht.getString ("name", "");
			datasetId = ht.getString ("datasetId", "");
			string typeString = ht.getString ("itemType", "");
			if ("multirow".Equals (typeString)) {
				itemType = DATASET_ITEM_TYPE.MULTI_ROW;
			} else if ("singlerow".Equals (typeString)) {
				itemType = DATASET_ITEM_TYPE.SINGLE_ROW;
			} else {
				itemType = DATASET_ITEM_TYPE.MULTI_ROW;
			}

			string strategyString = ht.getString ("updateStrategy", "");
			if ("average".Equals (strategyString)) {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.AVERAGE;

			} else if ("maximum".Equals (strategyString)) {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.MAXIMUM;

			} else if ("minimum".Equals (strategyString)) {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.MINIMUM;

			} else if ("replace".Equals (strategyString)) {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.REPLACE;

			} else if ("sum".Equals (strategyString)) {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.SUM;

			} else {
				updateStrategy = DATASET_ITEM_UPDATE_STRATEGY.REPLACE;
			}

		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}

	}
}