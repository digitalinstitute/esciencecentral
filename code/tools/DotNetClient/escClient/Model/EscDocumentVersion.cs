using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscDocumentVersion : JSONSerializable
	{
		public string id;
		public string documentRecordId;
		public string comments;
		public string userId;
		public int versionNumber;
		public long size;
		public long timestamp;
		public string downloadPath;
		public string md5;

		public EscDocumentVersion ()
		{
		}

		public EscDocumentVersion (string json)
		{
			ParseJson (json);
		}

		public EscDocumentVersion (Hashtable ht)
		{
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("documentRecordId", documentRecordId);
			ht.Add ("comments", comments);
			ht.Add ("userId", userId);
			ht.Add ("versionNumber", versionNumber);
			ht.Add ("size", size);
			ht.Add ("timestamp", timestamp);
			ht.Add ("downloadPath", downloadPath);
			ht.Add ("md5", md5);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			documentRecordId = ht.getString ("documentRecordId", "");
			comments = ht.getString ("comments", "");
			userId = ht.getString ("userId", "");
			versionNumber = ht.getInt ("versionNumber", 0);
			size = ht.getLong ("size", 0L);
			timestamp = ht.getLong ("timestamp", 0L);
			downloadPath = ht.getString ("downloadPath", "");
			md5 = ht.getString ("md5", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}