using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscDocument : JSONSerializable
	{
		public String id;
		public String name;
		public String description;
		public String organisationId;
		public String containerId;
		public String creatorId;
		public long currentVersionSize = 0;
		public int currentVersionNumber = 0;
		public String downloadPath;
		public String uploadPath;
		public String projectId;


		public EscDocument ()
		{
		}

		public EscDocument(string json){
			ParseJson (json);
		}

		public EscDocument(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("containerId", containerId);
			ht.Add ("organisationId", organisationId);
			ht.Add ("creatorId", creatorId);
			ht.Add ("currentVersionNumber", currentVersionNumber);
			ht.Add ("currentVersionSize", currentVersionSize);
			ht.Add ("downloadPath", "");
			ht.Add ("uploadPath", "");
			ht.Add ("projectId", "");
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			containerId = ht.getString ("containerId", "");
			organisationId = ht.getString ("organisationId", "");
			creatorId = ht.getString ("creatorId", "");
			currentVersionSize = ht.getLong ("currentVersionSize", 0L);
			currentVersionNumber = ht.getInt ("currentVersionNumber", 0);
			downloadPath = ht.getString ("downloadPath", "");
			projectId = ht.getString ("projectId", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}

