using System;
using Procurios.Public;
using System.Collections;

namespace Esc.Client {
	public class EscWorkflowParameterList : JSONSerializable {
		private ArrayList values = new ArrayList();

		public EscWorkflowParameterList () {
		}

		public EscWorkflowParameterList (string json) {
			ParseJson (json);
		}

		public EscWorkflowParameterList (Hashtable ht) {
			ParseHashtable (ht);
		}

		public void AddParameter(string blockName, string parameterName, string parameterValue){
			EscWorkflowParameter p = new EscWorkflowParameter ();
			p.blockName = blockName;
			p.name = parameterName;
			p.value = parameterValue;
			values.Add (p);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();

			ArrayList htValues = new ArrayList ();
			for (int i = 0; i < values.Count; i++) {
				htValues.Add (((EscWorkflowParameter)values [i]).ToHashTable ());
			}
			ht.Add ("values", htValues);
			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);

		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);
			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}