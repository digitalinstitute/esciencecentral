using System;
using System.Collections;
using Procurios.Public;

namespace Esc.Client
{
	public class EscProject : JSONSerializable
	{
		public string id;
		public string name;
		public string description;
		public string workflowFolderId;
		public string dataFolderId;
		public string creatorId;
		public EscProject ()
		{
		}

		public EscProject(string json){
			ParseJson (json);
		}

		public EscProject(Hashtable ht){
			ParseHashtable (ht);
		}

		public string ToJson(){
			Hashtable ht = new Hashtable ();
			ht.Add ("id", id);
			ht.Add ("name", name);
			ht.Add ("description", description);
			ht.Add ("creatorId", creatorId);
			ht.Add ("dataFolderId", dataFolderId);
			ht.Add ("workflowFolderId", workflowFolderId);

			return JSON.JsonEncode (ht);
		}

		public void ParseHashtable(Hashtable result) {
			HashtableWrapper ht = new HashtableWrapper (result);
			id = ht.getString ("id", "");
			name = ht.getString ("name", "");
			description = ht.getString ("description", "");
			dataFolderId = ht.getString ("dataFolderId", "");
			workflowFolderId = ht.getString ("workflowFolderId", "");
			creatorId = ht.getString ("creatorId", "");
		}

		public void ParseJson(string json){
			object result = JSON.JsonDecode (json);

			if(result is Hashtable){
				ParseHashtable ((Hashtable)result);
			}
		}
	}
}

