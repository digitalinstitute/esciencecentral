﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Esc.Client;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
			//Create client libs for the Storage and Workflow API
			String eSCHostname = "demo.escapp.net"; //hostname of the e-SC server
			int eSCPort = 8080; //Port number of the e-SC server
			String username = "";  //e-SC Username
			String password = ""; //e-SC password
			String fileName = ""; //File that will be uploaded
			String pathToFile = "";   //Directory which file is in
			String workflowId = "workflows-xbim-demo";  //The Id of the workflow we want to run on the uploaded file
			String emailRecipient = ""; //The email address you want the files sent to

			StorageAPI s = new StorageAPI(eSCHostname, eSCPort, false, username, password);
			WorkflowAPI w = new WorkflowAPI(eSCHostname, eSCPort, false,username, password);

			//We will upload to the user's home folder
			EscFolder homeFolder = s.HomeFolder ();

			//Create the file holder in e-SC
			EscDocument doc = s.CreateDocumentInFolder (homeFolder.id, fileName);

			//Upload the contents of the file
			EscDocumentVersion version = s.UploadFile (doc, pathToFile + fileName);
			Console.WriteLine ("Created DocumentVersion: " + version.ToJson());


			//Set the parameters of the workflow
			EscWorkflowParameterList parameters = new EscWorkflowParameterList();

			//This currently supports one input file, we can add more if necessary
			parameters.AddParameter ("ifc_input", "Source", doc.id);
			parameters.AddParameter ("email_files", "To", emailRecipient);

			//Execute the workflow.  This is async and so will return before the workflow completes
			//It is possible to poll for completion if necessary.  Let me know if you want to do this
			EscWorkflowInvocation inv = w.ExecuteWorkflowWithParameters (workflowId, parameters);

			Console.WriteLine ("Started Workflow Invocation: " + inv.ToJson());


        }
    }
}
