/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

import java.io.Console;
import java.net.URI;
import java.util.Arrays;
import java.util.regex.Pattern;


public abstract class Common
{
    public static String[] PrepareCredentials(URI uri, String defaultUser, String defaultPass)
    {
        String userInfo = uri.getUserInfo();
        String[] namePass;
        int length;

        if (userInfo != null) {
            namePass = uri.getUserInfo().split(":", 3);
            length = namePass.length;

            if (length > 2) {
                throw new IllegalArgumentException("Malformed user info for URI: " + uri);
            } else if (length > 0) {
                // Change ! into @
                namePass[0] = namePass[0].replace('!', '@');
            }
        } else {
            namePass = new String[2];
            length = 0;
        }

        if (length < 2) {
            namePass = Arrays.copyOf(namePass, 2);
            //Console c = System.console();
            if (length == 0) {
                System.out.println("Missing credentials for: " + uri.toString());
                if (defaultUser != null) {
                    System.out.println("Using default user name: " + defaultUser);
                    namePass[0] = defaultUser;
                } else {
                    Console c = System.console();
                    if (c == null) {
                        throw new RuntimeException("Cannot attach to console to read the user name.");
                    }
                    namePass[0] = c.readLine("user name:");
                }
                if (defaultPass != null) {
                    System.out.println("Using default password");
                    namePass[1] = defaultPass;
                } else {
                    Console c = System.console();
                    if (c == null) {
                        throw new RuntimeException("Cannot attach to console to read the user password.");
                    }
                    namePass[1] = new String(c.readPassword("password:"));
                }
            } else if (length == 1) {
                if (namePass[0].equals(defaultUser) && defaultPass != null) {
                    System.out.println("Using default password for user " + namePass[0]);
                    namePass[1] = defaultPass;
                } else {
                    Console c = System.console();
                    if (c == null) {
                        throw new RuntimeException("Cannot attach to console to read the user password.");
                    }
                    namePass[1] = new String(c.readPassword("password for %s:", namePass[0]));
                }
            }
        }

        return namePass;
    }


    /**
     * Convert URI to String such that UserInfo is not included.
     *
     * @param uri the Uri to convert
     * @return Uri without user information.
     */
    public static String ToSafeString(URI uri)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(uri.getScheme());
        sb.append("://");
        sb.append(uri.getHost());
        if (uri.getPort() > 0) {
            sb.append(":");
            sb.append(uri.getPort());
        }
        sb.append(uri.getPath());

        return sb.toString();
    }


    public static boolean WildCardMatch(String text, String pattern)
    {
        return CreateRegexFromGlob(pattern).matcher(text).matches();
    }


    public static boolean IsWildcard(String glob)
    {
        int len = glob.length();
        boolean escaping = false;

        for (int i = 0; i < len; i++) {
            char c = glob.charAt(i);
            switch (c) {
                case '*':
                case '?':
                    if (escaping) {
                        escaping = false;
                    } else {
                        return true;
                    }
                    break;
                case '\\':
                    if (escaping) {
                        escaping = false;
                    } else {
                        escaping = true;
                    }
                    break;
                default:
                    // it's a regular character, clear the escaping state and look further.
                    escaping = false;
                    break;
            }
        }

        return false;
    }


    /**
     * <p>This method creates a Java regular expression pattern from a glob pattern used to describe POSIX filenames (e.g. *.txt, [a-z]*.txt).</p>
     *
     * <p>Thanks to Dave Ray and Paul Tombin for their
     * <a href="http://stackoverflow.com/questions/1247772/is-there-an-equivalent-of-java-util-regex-for-glob-type-patterns">stackoverflow answer</a>
     * modified to simplify handling of '{', '}' and ','.</p>
     *
     * <p>TODO: test it thoroughly and check conformance with POSIX
     * <a href="http://www.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html#tag_02_13">pattern matching notation</a>.</p>
     *
     * @param glob
     * @return
     */
    public static Pattern CreateRegexFromGlob(String glob)
    {
        int len = glob.length();
        StringBuilder sb = new StringBuilder(len);
        sb.append('^');
        boolean escaping = false;

        for (int i = 0; i < len; i++) {
            char c = glob.charAt(i);
            switch (c) {
                case '*':
                    sb.append(escaping ? "\\*" : ".*");
                    escaping = false;
                    break;
                case '?':
                    sb.append(escaping ? "\\?" : ".");
                    escaping = false;
                    break;
                case '.':
                case '(':
                case ')':
                case '+':
                case '|':
                case '^':
                case '$':
                case '@':
                case '%':
                case '{':
                case '}':
                case ',':
                    sb.append("\\");
                    sb.append(c);
                    escaping = false;
                    break;
                case '\\':
                    if (escaping) {
                        sb.append("\\\\");
                        escaping = false;
                    } else {
                        escaping = true;
                    }
                    break;
                default:
                    sb.append(c);
                    escaping = false;
                    break;
            }
        }
        sb.append('$');

        return Pattern.compile(sb.toString());
    }
}
