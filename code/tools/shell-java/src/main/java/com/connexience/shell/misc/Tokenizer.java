/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell.misc;

/**
 * A simple string tokenizer (simplified version of StringTokenizer). Written to provide a copy constructor that is
 * useful for recursive traversal and the getPrefix method that returns text before the current position.
 *
 * Created by Jacek on 14/09/2016.
 */
public class Tokenizer
{
    private final String _text;
    private final char _delimiter;

    private int _pos;


    public Tokenizer(String text, char delimiter)
    {
        _text = text;
        _delimiter = delimiter;

        _pos = 0;
    }

    public Tokenizer(Tokenizer t) {
        _text = t._text;
        _delimiter = t._delimiter;

        _pos = t._pos;
    }


    public boolean hasMoreTokens()
    {
        _skipDelimiters();
        return _text.length() > _pos;
    }


    public String nextToken()
    {
        _skipDelimiters();

        int newPos = _pos;
        while (newPos < _text.length() && _text.charAt(newPos) != _delimiter) {
            newPos++;
        }

        String token = _text.substring(_pos, newPos);
        _pos = newPos;

        return token;
    }


    public String getPrefix()
    {
        return _text.substring(0, _pos);
    }


    private void _skipDelimiters()
    {
        while (_pos < _text.length() && _text.charAt(_pos) == _delimiter) {
            _pos++;
        }
    }
}
