/**
 * e-Science Central
 * Copyright (C) 2008-2016 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.shell;

public class Size
{
    public static final long k  = 1000L;
    public static final long M  = 1000000L;
    public static final long G  = 1000000000L;
    public static final long T  = 1000000000000L;
    public static final long P  = 1000000000000000L;
    public static final long E  = 1000000000000000000L;

    public static final long Ki = 0x400L;
    public static final long Mi = 0x100000L;
    public static final long Gi = 0x40000000L;
    public static final long Ti = 0x10000000000L;
    public static final long Pi = 0x4000000000000L;
    public static final long Ei = 0x1000000000000000L;

}
