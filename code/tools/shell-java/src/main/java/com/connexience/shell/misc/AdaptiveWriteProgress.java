package com.connexience.shell.misc;

import com.connexience.api.misc.WriteProgress;


/**
 * Gives a little better percent progress information which can adapt to the size of the
 * output and how quickly the progress is made.
 * 
 * @author njc97
 *
 */
public class AdaptiveWriteProgress extends WriteProgress
{
    private long _lastPercent;
    private int _precisionLog;
    private int _precision;

    public AdaptiveWriteProgress()
    {
        super();

        _lastPercent = 0;
        _precision = 1;
        _precisionLog = 0;
    }


    @Override
    protected String getPercentString(long currentLength, long totalLength)
    {
        long percent = (long)((double)currentLength / totalLength * 100 * _precision + 0.5);
        if (_lastPercent == percent && _precisionLog < 4) {
            _precision *= 10;
            _precisionLog++;
            percent = (long)((double)currentLength / totalLength * 100 * _precision + 0.5);
        } else if (_precisionLog > 0 && percent - _lastPercent >= 50) {
            _precision /= 10;
            _precisionLog--;
            percent = (long)((double)currentLength / totalLength * 100 * _precision + 0.5);
        }
        _lastPercent = percent;

        // TODO: Think about how to avoid String.format in this simple case.
        // The issue is how to get a simple way to add leading zeros after 
        // the decimal point depending on the precisionLog. 
        //return percent / _precision + "." + percent % _precision;
        return String.format("%." + _precisionLog + "f", (double)percent/_precision);
    }
}
