/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains a set of selected objects and also the component that
 * contains them. i.e. the folder tree or the folder view
 * @author Hugo
 */
public class ObjectSelection {
    private Object sourceComponent;
    private List selectedObjects;

    public ObjectSelection() {
        this.selectedObjects = new ArrayList();
    }

    
    public ObjectSelection(Object sourceComponent){
        this.sourceComponent = sourceComponent;
        this.selectedObjects  = new ArrayList();
    }

    public ObjectSelection(Object sourceComponent, EscObject object){
        this.sourceComponent = sourceComponent;
        this.selectedObjects = new ArrayList();
        this.selectedObjects.add(object);
    }
    
    public ObjectSelection(Object sourceComponent, List selectedObjects){
        this.sourceComponent = sourceComponent;
        this.selectedObjects = selectedObjects;
    }

    public Object getSourceComponent() {
        return sourceComponent;
    }

    public List getSelectedObjects() {
        return selectedObjects;
    }

    public boolean isSingleFolderSelected(){
        if(selectedObjects.size()==1 && selectedObjects.get(0) instanceof EscFolder){
            return true;
        } else {
            return false;
        }
    }
            
    public void add(Object o){
        selectedObjects.add(o);
    }
    
    public void clear(){
        selectedObjects.clear();
    }
    
    @Override
    public String toString() {
        if(selectedObjects.size()==1){
            Object selectedItem = selectedObjects.get(0);
            if(selectedItem instanceof EscFolder){
                return "Folder: " + ((EscFolder)selectedItem).getName();

            } else if(selectedItem instanceof EscDocument){
               return "Document: " + ((EscDocument)selectedItem).getName();

            } else if(selectedItem instanceof EscWorkflow){
                return "Workflow: " + ((EscWorkflow)selectedItem).getName();

            } else if(selectedItem instanceof EscWorkflowInvocation){
                return "Workflow run: " + ((EscWorkflowInvocation)selectedItem).getName();

            } else if(selectedItem instanceof EscObject){
                return "Object: " + ((EscObject)selectedItem).getName();

            } else {
                return "Unknown object";
            }
        } else {
            return selectedObjects.size() + " Objects";            

        }
    }
    
    
}
