/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common.dnd;

import com.connexience.api.StorageClient;
import com.connexience.api.model.EscObject;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.swing.JComponent;

/**
 *
 * @author hugo
 */
public class EscObjectListTransferrable implements Transferable, Serializable {
    public static final int FILE_TREE = 0;
    public static final int FOLDER_VIEW = 1;
    EscObject[] objects;
    StorageClient client;
    String sourceComponentGuid;
    int sourceComponentType = FOLDER_VIEW;
    
    public EscObjectListTransferrable(String sourceComponentGuid, StorageClient client) {
        objects = new EscObject[0];
        this.client = client;
        this.sourceComponentGuid = sourceComponentGuid;
    }

    public EscObjectListTransferrable(String sourceComponentGuid, StorageClient client, EscObject[] objects) {
        this.objects = objects;
        this.client = client;
        this.sourceComponentGuid = sourceComponentGuid;
    }
    
    public EscObjectListTransferrable(String sourceComponentGuid, StorageClient client, List transferList){
        this.client = client;
        this.sourceComponentGuid = sourceComponentGuid;
        objects = new EscObject[transferList.size()];
        for(int i=0;i<transferList.size();i++){
            objects[i] = (EscObject)transferList.get(i);
        }
    }

    public void setSourceComponentType(int sourceComponentType) {
        this.sourceComponentType = sourceComponentType;
    }

    public int getSourceComponentType() {
        return sourceComponentType;
    }

    public EscObject[] getObjects() {
        return objects;
    }

    public StorageClient getClient() {
        return client;
    }

    public String getSourceComponentGuid() {
        return sourceComponentGuid;
    }

    @Override
    public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        if(flavor.equals(EscObjectDataFlavor.escObjectFlavor)){
            return this;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        DataFlavor[] supported = new DataFlavor[1];
        supported[0] = EscObjectDataFlavor.escObjectFlavor;
        return supported;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        if(flavor.equals(EscObjectDataFlavor.escObjectFlavor)){
            return true;
        } else {
            return false;
        }
    }
}