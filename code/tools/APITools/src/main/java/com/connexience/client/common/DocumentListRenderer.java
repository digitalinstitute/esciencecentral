/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.client.common;

import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

/**
 * Renderer for an EscDocument in a JList
 * @author hugo
 */
public class DocumentListRenderer extends JPanel implements ListCellRenderer{
    ImagePanel iconPanel;
    JLabel label;
    
    public DocumentListRenderer() {
        iconPanel = new ImagePanel();
        iconPanel.setPreferredSize(new Dimension(32, 32));
        label = new JLabel();
        setLayout(new BorderLayout());
        add(iconPanel, BorderLayout.WEST);
        add(label, BorderLayout.CENTER);
        
        JPanel topBorder = new JPanel();
        topBorder.setPreferredSize(new Dimension(1, 1));
        
        JPanel bottomBorder = new JPanel();
        bottomBorder.setPreferredSize(new Dimension(1, 1));
        add(topBorder, BorderLayout.NORTH);
        add(bottomBorder, BorderLayout.SOUTH);
        setOpaque(false);
        iconPanel.setOpaque(false);
        label.setOpaque(false);
        topBorder.setOpaque(false);
        bottomBorder.setOpaque(false);
    }
    
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if(value instanceof EscDocument){
            EscDocument doc = (EscDocument)value;
            label.setText(doc.getName());
            ImageIcon icon;
            if(doc.getInternalClassName()!=null && doc.getInternalClassName().endsWith("ServerObject") || doc.getInternalClassName().endsWith("DocumentRecord")){
                icon = getIconForFilename(doc.getName());
                
            } else if(doc.getInternalClassName()!=null && doc.getInternalClassName().endsWith("WorkflowDocument")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/sitemap_application.png"));
                
            } else if(doc.getInternalClassName()!=null && doc.getInternalClassName().endsWith("DynamicWorkflowService")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/box_down.png"));
                
            } else if(doc.getInternalClassName()!=null && doc.getInternalClassName().endsWith("Dataset")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/database.png"));
                
            } else {
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/page_white.png"));
            }
            
            iconPanel.setImage(icon);
        } else if (value instanceof EscWorkflowInvocation){
            EscWorkflowInvocation invocation = (EscWorkflowInvocation)value;
            
            ImageIcon icon;
            if(invocation.getStatus().equals("ExecutionError")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/workflow_failed.png"));
                label.setText(invocation.getWorkflowName() + " (Failed at: " + invocation.getPercentComplete() + "%)");
                
            } else if(invocation.getStatus().equals("Finished")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/workflow_completed.png"));
                label.setText(invocation.getWorkflowName() + " (Finished)");
                
            } else if(invocation.getStatus().equals("Queued")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/workflow_queued.png"));
                label.setText(invocation.getWorkflowName() + " (Waiting)");
                
            } else if(invocation.getStatus().equals("Running")){
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/workflow_running.png"));
                label.setText(invocation.getWorkflowName() + " (" + invocation.getPercentComplete() + "%)");
                
            } else {
                icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/sitemap_application.png"));
                label.setText(invocation.getWorkflowName());
                
            }
            iconPanel.setImage(icon); 
        } else if(value instanceof EscFolder){
            /*
            Icon icon = UIManager.getIcon("Tree.closedIcon");
            iconPanel.setIcon(icon);
            */
            ImageIcon icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/folder.png"));
            iconPanel.setImage(icon);
            label.setText(((EscFolder)value).getName());
            
        } else {
            // Waiting text
            label.setText("Fetching");
            ImageIcon icon = new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/hourglass.png"));
            iconPanel.setImage(icon);
        }
        
        if(isSelected){
            setOpaque(true);
            iconPanel.setOpaque(true);
            label.setOpaque(true);        
            
            label.setBackground(UIManager.getDefaults().getColor("List.selectionBackground"));
            label.setForeground(UIManager.getDefaults().getColor("List.selectionForeground"));
            iconPanel.setBackground(UIManager.getDefaults().getColor("List.selectionBackground"));
            iconPanel.setForeground(UIManager.getDefaults().getColor("List.selectionForeground"));
            
        } else {
            setOpaque(false);
            iconPanel.setOpaque(false);
            label.setOpaque(false);   
            label.setBackground(UIManager.getDefaults().getColor("List.background"));
            label.setForeground(UIManager.getDefaults().getColor("List.foreground"));          
            iconPanel.setBackground(UIManager.getDefaults().getColor("List.background"));
            iconPanel.setForeground(UIManager.getDefaults().getColor("List.foreground"));              
        }
        return this;
    }
    
    private ImageIcon getIconForFilename(String filename){
        int location = filename.indexOf(".");
        if(location!=-1){
            String extension = filename.substring(location + 1);
            String resourceName = "/com/connexience/uploader/icons/file_extension_" + extension.trim() + ".png";
            if(getClass().getResource(resourceName) != null){
                return new javax.swing.ImageIcon(getClass().getResource(resourceName));
            } else {
                // Check for special cases
                if(extension!=null && !extension.isEmpty()){
                    if(extension.equalsIgnoreCase("csv")){
                        return new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/file_extension_xls.png"));
                    } else if(extension.equalsIgnoreCase("r") || extension.equalsIgnoreCase("m")){
                        return new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/file_extension_txt.png"));
                    } else {
                        return new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/page_white.png"));
                    }
            } else {
                    return new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/page_white.png"));
                }
            }
        } else {
            return new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/icons/page_white.png"));
        }
    }
    

}