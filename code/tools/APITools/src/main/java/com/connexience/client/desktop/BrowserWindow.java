/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.desktop;

import com.connexience.api.async.ApiTaskManager;
import com.connexience.api.async.ApiTask;
import com.connexience.api.async.ApiTaskAdapter;
import com.connexience.api.async.ApiTaskManager;
import com.connexience.api.async.ApiTaskManagerListener;
import com.connexience.api.async.tasks.CreateChildFolderTask;
import com.connexience.api.async.tasks.DeleteObjectsTask;
import com.connexience.api.async.tasks.DownloadFileTask;
import com.connexience.api.async.tasks.ListStudiesTask;
import com.connexience.api.async.tasks.TransferTask;
import com.connexience.api.async.tasks.UploadFileToFolderTask;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscFolder;
import com.connexience.api.model.EscObject;
import com.connexience.api.model.EscProject;
import com.connexience.api.model.net.GenericClient;

import com.connexience.client.common.FolderTree;
import com.connexience.client.common.FolderView;
import com.connexience.client.common.FolderViewListener;
import com.connexience.client.common.ImagePanel;
import com.connexience.client.common.ObjectSelection;

import com.connexience.client.IconManager;
import com.connexience.client.common.uitasks.DownloadFolderTask;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.io.File;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author hugo
 */
public class BrowserWindow extends javax.swing.JInternalFrame implements ApiTaskManagerListener, FolderViewListener {
    FolderTree tree;
    FolderView folderView;
    FileManager parentFileManager;
    ApiTaskManager manager;
    ObjectSelection selectedItem;
    ImagePanel statusPanel;
    String windowGuid;
    
    ImageIcon idleIcon = new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "world.png"));
    ImageIcon runningIcon = new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "world_link.png"));
    
    /**
     * Creates new form BrowserWindow
     */
    public BrowserWindow(FileManager parentFileManager, ApiTaskManager manager, String windowGuid) {
        setResizable(true);
        setClosable(true);
        setIconifiable(true);
        setMaximizable(false);
        this.windowGuid = windowGuid;
        this.parentFileManager = parentFileManager;
        this.manager = manager;
        initComponents();
        
        // Set the icons
        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "arrow_refresh.png")));
        deleteButtton.setIcon(new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "cross.png")));
        addFolderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "folder_add.png")));
        downloadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "download_cloud.png")));
        upButton.setIcon(new javax.swing.ImageIcon(getClass().getResource(IconManager.ICON_DIRECTORY + "arrow_up.png")));
        
        statusPanel = new ImagePanel();
        statusPanel.setImage(idleIcon);
        statusPanel.setPreferredSize(new Dimension(IconManager.ICON_SIZE, IconManager.ICON_SIZE));
        topPanel.add(statusPanel, BorderLayout.EAST);
        manager.addUploaderTaskManagerListener(this);
        
        tree = new FolderTree(manager);
        tree.addFolderViewListener(this);
        folderView = new FolderView(manager);
        folderView.setWindowGuid(windowGuid);
        folderView.addFolderViewListener(this);
        folderView.listenToTree(tree);
        splitter.setLeftComponent(tree);
        splitter.setRightComponent(folderView);
        setTitle(getConnectionLabel());
        selectItem(new ObjectSelection());
        //System.out.println(( (javax.swing.plaf.basic.BasicInternalFrameUI) getUI()).getNorthPane().
        fetchProjectList();

    }

    @Override
    public void errorMessageReceived(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void logMessageReceived(String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
    
    public ApiTaskManager getTaskManager(){
        return manager;
    }

    public String getWindowGuid() {
        return windowGuid;
    }

    
    @Override
    public void transferStarted(Component sourceComponent, TransferTask transfer) {
        parentFileManager.addTransferTask(transfer);
    }

    @Override
    public void itemSelected(ObjectSelection item) {
        selectItem(item);
    }

    @Override
    public void objectDoubleClicked(EscObject object) {
        if(object instanceof EscFolder){
            // Open the folder
            //tree.selectAndExpandFolder(object.getId());
            tree.selectAndExpandFolder((EscFolder)object);
        } else if(object instanceof EscDocument){
            // View the document details
            EscDocument doc = (EscDocument)object;

        } 
    }

    @Override
    public void folderContentsChanged(EscFolder folder) {
        // Notify the file manager app so that any copies of this folder can be updated
        parentFileManager.folderViewFolderContentsChanged(this, folder);
    }
    
    public void folderContentsChangedExternally(EscFolder folder){
        folderView.fetchFolderFilesIfFolderVisible(folder);
    }
    
    public void selectItem(ObjectSelection item){
        selectedItem = item;
        selectedItemLabel.setText(item.toString());
    }
    
    @Override
    public void transferCompleted(Component sourceComponent, TransferTask transfer) {
        if(sourceComponent==tree){
            System.out.println("FolderView transfer completed");
            if(transfer instanceof UploadFileToFolderTask){
                UploadFileToFolderTask uploadTask = (UploadFileToFolderTask)transfer;
                if(folderView.getFolder()!=null && folderView.getFolder().getId().equals(uploadTask.getFolder().getId())){
                    // Refresh view
                    folderView.fetchFolderFiles();
                }
            }

        } else if(sourceComponent==folderView){
        }
    }
    
    private void fetchProjectList(){
        ListStudiesTask task = new ListStudiesTask();
        manager.performTask(task, new ApiTaskAdapter(){

            @Override
            public void taskCompleted(ApiTask task) {
                EscProject[] projects = ((ListStudiesTask)task).getStudies();
                DefaultComboBoxModel m = new DefaultComboBoxModel();
                
                EscProject homeProject = new EscProject();
                homeProject.setName("Home Folder");
                homeProject.setId("_HOME_");
                homeProject.setCreatorId("");
                homeProject.setDataFolderId("");
                homeProject.setDescription("");
                homeProject.setExternalId("");
                homeProject.setWorkflowFolderId("");
                m.addElement(homeProject);
                
                for(EscProject p : projects){
                    m.addElement(p);
                }
                studyList.setModel(m);
            }
            
            
        });
    }
    
    public String getConnectionLabel(){
        try {
            return manager.getStorageClient().getUsername() + "@" + manager.getStorageClient().getHostname();
        } catch (Exception e){
            return "";
        }
    }

    @Override
    public void uploaderTaskManagerStatusChanged(ApiTaskManager mgr) {
        if(mgr.getActiveTaskCount()>0){
            SwingUtilities.invokeLater(new Runnable(){
                public void run(){
                    statusPanel.setImage(runningIcon);
                }
            });
        } else {
            SwingUtilities.invokeLater(new Runnable(){
                public void run(){
                    statusPanel.setImage(idleIcon);
                }
            });            
        }
    }
    
    public void refreshView(){
        if(selectedItem!=null && selectedItem.getSourceComponent()!=null){
            if(selectedItem.getSourceComponent()==folderView){
                // Refresh the folder view
                folderView.fetchFolderFiles();
            } else if(selectedItem.getSourceComponent()==tree){
                // Refresh the tree
                tree.repopulateSelected();
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        splitter = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        selectedItemLabel = new javax.swing.JLabel();
        topPanel = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        upButton = new javax.swing.JButton();
        refreshButton = new javax.swing.JButton();
        deleteButtton = new javax.swing.JButton();
        addFolderButton = new javax.swing.JButton();
        downloadButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jLabel2 = new javax.swing.JLabel();
        studyList = new javax.swing.JComboBox();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        splitter.setResizeWeight(0.3);
        getContentPane().add(splitter, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.BorderLayout());

        jLabel1.setText("Selected item:");
        jPanel1.add(jLabel1, java.awt.BorderLayout.WEST);
        jPanel1.add(selectedItemLabel, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.SOUTH);

        topPanel.setLayout(new java.awt.BorderLayout());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        upButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/exclamation.png"))); // NOI18N
        upButton.setBorderPainted(false);
        upButton.setFocusable(false);
        upButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        upButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(upButton);

        refreshButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/exclamation.png"))); // NOI18N
        refreshButton.setBorderPainted(false);
        refreshButton.setFocusable(false);
        refreshButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        refreshButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        refreshButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(refreshButton);

        deleteButtton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/exclamation.png"))); // NOI18N
        deleteButtton.setBorderPainted(false);
        deleteButtton.setFocusable(false);
        deleteButtton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        deleteButtton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        deleteButtton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButttonActionPerformed(evt);
            }
        });
        jToolBar1.add(deleteButtton);

        addFolderButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/exclamation.png"))); // NOI18N
        addFolderButton.setBorderPainted(false);
        addFolderButton.setFocusable(false);
        addFolderButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addFolderButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        addFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addFolderButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(addFolderButton);

        downloadButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/connexience/uploader/exclamation.png"))); // NOI18N
        downloadButton.setBorderPainted(false);
        downloadButton.setFocusable(false);
        downloadButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        downloadButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        downloadButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downloadButtonActionPerformed(evt);
            }
        });
        jToolBar1.add(downloadButton);
        jToolBar1.add(jSeparator1);

        jLabel2.setText("Select Study:");
        jToolBar1.add(jLabel2);

        studyList.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        studyList.setPreferredSize(new java.awt.Dimension(150, 27));
        studyList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studyListActionPerformed(evt);
            }
        });
        jToolBar1.add(studyList);

        topPanel.add(jToolBar1, java.awt.BorderLayout.CENTER);

        getContentPane().add(topPanel, java.awt.BorderLayout.NORTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if(parentFileManager!=null){
            parentFileManager.removeBrowserWindow(this);
        }
    }//GEN-LAST:event_formInternalFrameClosing

    private void refreshButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshButtonActionPerformed
        refreshView();

    }//GEN-LAST:event_refreshButtonActionPerformed

    private void deleteButttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButttonActionPerformed
        if(selectedItem!=null){
            if(JOptionPane.showConfirmDialog(this, "Are you sure you want to delete: " + selectedItem.toString() + "?", "Question", JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION){
                
                DeleteObjectsTask task = new DeleteObjectsTask();

                List objects = selectedItem.getSelectedObjects();
                for(Object o : objects){
                    if(o instanceof EscObject){               
                        task.addObject((EscObject)o);
                    } else {
                        System.out.println("Object: " + o.getClass().getSimpleName() + " is not an EscObject");
                    }
                }

                final EscFolder treeFolder;
                if(selectedItem.getSourceComponent()==tree && objects.size()==1 && objects.get(0) instanceof EscFolder){
                    treeFolder = (EscFolder)objects.get(0);
                    selectItem(new ObjectSelection());
                } else {
                    treeFolder = null;
                }

                parentFileManager.addTransferTask(task);
                manager.performTask(task, new ApiTaskAdapter(){

                    @Override
                    public void taskCompleted(ApiTask task) {

                        if(treeFolder!=null){
                            String parentId = treeFolder.getContainerId();
                            tree.selectFolder(parentId);
                            tree.repopulateFolder(parentId);
                        } else {
                            refreshView();
                        }
                        
                        // Notify change of contents
                        folderContentsChanged(folderView.getFolder());
                    }

                    @Override
                    public void taskFailed(ApiTask task, Exception cause) {
                        cause.printStackTrace();
                        refreshView();
                    }

                });                
                
            }

        }
    }//GEN-LAST:event_deleteButttonActionPerformed

    private void addFolderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFolderButtonActionPerformed
        if(selectedItem!=null && selectedItem.isSingleFolderSelected()){
            final EscFolder f = (EscFolder)selectedItem.getSelectedObjects().get(0);
            String name = JOptionPane.showInputDialog(this, "Enter new Folder name");
            if(name!=null && !name.isEmpty()){
                CreateChildFolderTask task = new CreateChildFolderTask(f, name);
                final BrowserWindow self = this;
                manager.performTask(task, new ApiTaskAdapter(){

                    @Override
                    public void taskCompleted(ApiTask task) {
                        tree.repopulateFolder(f);
                        
                        // If this folder is shown in the folder view update it
                        if(folderView.getFolder()!=null && folderView.getFolder().getId()!=null && folderView.getFolder().getId().equals(f.getId())){
                            folderView.fetchFolderFiles();
                        }
                        
                        // Notify the parent that a folder has changed so that any
                        // views can be updated
                        parentFileManager.folderViewFolderContentsChanged(self, f);
                    }
                    
                });
            }
        }
    }//GEN-LAST:event_addFolderButtonActionPerformed

    private void downloadButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downloadButtonActionPerformed
        // Download the selected files
        if(selectedItem!=null){
            final JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setMultiSelectionEnabled(false);

            
            if(chooser.showSaveDialog(this)==JFileChooser.APPROVE_OPTION){    
                final BrowserWindow c = this;
                final ApiTask.Callback transferCallback = new ApiTask.Callback() {

                    @Override
                    public void taskStarted(ApiTask task) {
                        transferStarted(c, (TransferTask)task);
                    }

                    @Override
                    public void taskCompleted(ApiTask task) {
                        transferCompleted(c, (TransferTask)task);
                    }

                    @Override
                    public void taskFailed(ApiTask task, Exception cause) {
                        transferCompleted(c, (TransferTask)task);
                    }

                    @Override
                    public void taskProgress(int percentage) {
                        
                    }
                };
                        
                List objects = selectedItem.getSelectedObjects();
                for(final Object o : objects){
                    if(o instanceof EscFolder){
                        SwingUtilities.invokeLater(new Runnable() {

                            @Override
                            public void run() {
                                DownloadFolderTask task = new DownloadFolderTask(chooser.getSelectedFile(), (EscFolder)o);
                                task.setDesktop(FileManager.GLOBAL_INSTANCE.getDesktop());
                                task.setTransferCallback(transferCallback);
                                manager.performLowPriorityTask(task, new ApiTaskAdapter());
                            }
                        });
                        
                    } else if(o instanceof EscDocument){
                        SwingUtilities.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                File localFile = new File(chooser.getSelectedFile(), ((EscDocument)o).getName());
                                DownloadFileTask task = new DownloadFileTask(localFile, (EscDocument)o);
                                manager.performLowPriorityTask(task, transferCallback);
                            }
                        });                        
                    }
                }
            } 
        }
    }//GEN-LAST:event_downloadButtonActionPerformed

    private void studyListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studyListActionPerformed
        // TODO add your handling code here:
        if(studyList.getSelectedItem()!=null){            
            System.out.println("Study changed: " + studyList.getSelectedItem());
            EscProject project = (EscProject)studyList.getSelectedItem();
            if(!project.getId().equals("_HOME_")){
                GenericClient c = getTaskManager().getBaseClient();
                getTaskManager().clearClients();
                c.setProjectContext(project.getExternalId());
                getTaskManager().setBaseClient(c);  
                tree.getModel().resetTopFolder();
            } else {
                GenericClient c = getTaskManager().getBaseClient();
                getTaskManager().clearClients();
                c.setProjectContext(null);
                getTaskManager().setBaseClient(c);  
                tree.getModel().resetTopFolder();                
            }
        }
        
    }//GEN-LAST:event_studyListActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addFolderButton;
    private javax.swing.JButton deleteButtton;
    private javax.swing.JButton downloadButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton refreshButton;
    private javax.swing.JLabel selectedItemLabel;
    private javax.swing.JSplitPane splitter;
    private javax.swing.JComboBox studyList;
    private javax.swing.JPanel topPanel;
    private javax.swing.JButton upButton;
    // End of variables declaration//GEN-END:variables
}
