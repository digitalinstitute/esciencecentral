/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.client.common;

/**
 * Listens to folder tree events
 * @author hugo
 */
public interface FolderTreeModelListener {
    public void rootNodeFetched(FolderTreeModel.FolderNode node);
}