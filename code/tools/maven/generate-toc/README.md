# Generate TOC maven plugin

This plugin is meant to help with generating a table of contents for workflow blocks, libraries and also workflows.
To run the plugin just change to the block or library directory, e.g. `code/webflow/WorkflowItems/blocks` and issue:

    > mvn com.connexience:generatetoc-maven-plugin:generate

This command should produce the `README.md` file with links to all blocks/libraries that are referenced by the current project's `pom.xml`.

The plugin is built together with maven archetypes for blocks, so if you need to build it run:

    > cd code/tools/maven
    > mvn clean install -P all

or, alternatively, to build just the plugin

    > cd code/tools/maven/generate-toc
    > mvn clean install
