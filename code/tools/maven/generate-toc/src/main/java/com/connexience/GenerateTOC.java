/**
 * e-Science Central
 * Copyright (C) 2008-2015 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import org.apache.maven.model.Profile;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Mojo(name = "generate", defaultPhase = LifecyclePhase.PROCESS_RESOURCES, aggregator = true)
public class GenerateTOC extends AbstractMojo
{
    /**
     * Path to the output file where the TOC will be generated.
     */
    @Parameter(defaultValue = "README.md")
    private String outputFile;

    /**
     * Path to the footer file that will be added at the end of the output file.
     */
    @Parameter(defaultValue = "README.footer.md")
    private String footerFile;

    /**
     * Path to the header file that will be added at the beginning of the output file.
     */
    @Parameter(defaultValue = "README.header.md")
    private String headerFile;

    /**
     * The project which the plugin is called from.
     */
    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;


    public void execute() throws MojoExecutionException
    {
        Map<String, ArrayList<String>> moduleMap = _listModules();

        // Generate the output readme file
        //
        Path tocFile = project.getBasedir().toPath().resolve(outputFile);

        try {
            // Create the output directory if needed
            Files.createDirectories(tocFile.getParent());

            // Find the root for the modules if the outputDirectory is different than the project basedir.
            Path root = tocFile.getParent().relativize(project.getBasedir().toPath());

            try (BufferedWriter writer = Files.newBufferedWriter(tocFile, StandardCharsets.UTF_8)) {
                // Copy in the header file if exists
                if (Files.exists(Paths.get(headerFile))) {
                    writer.write(new String(
                            Files.readAllBytes(Paths.get(headerFile)),
                            StandardCharsets.UTF_8));
                }

                // Write the list of modules grouped by module path
                String separator = System.getProperty("file.separator");

                for (Map.Entry<String, ArrayList<String>> e : moduleMap.entrySet()) {
                    Collections.sort(e.getValue());

                    writer.newLine();
                    if (!"".equals(e.getKey())) {
                        writer.write("## Group: " + e.getKey());
                        writer.newLine();
                        writer.newLine();
                    }

                    // Prepare module root (do it once for each module on the list
                    String moduleRoot = root.resolve(e.getKey()).toString();
                    if ("\\".equals(separator)) {
                        moduleRoot = moduleRoot.replace('\\', '/');
                    }
                    if (!moduleRoot.endsWith("/")) {
                        moduleRoot = moduleRoot + "/";
                    }

                    for (String module : e.getValue()) {
                        writer.write(String.format("* [%s](%s)", module, moduleRoot + module));
                        writer.newLine();
                    }
                }

                // Copy in the footer file if exists
                if (Files.exists(Paths.get(footerFile))) {
                    writer.write(new String(
                            Files.readAllBytes(Paths.get(footerFile)),
                            StandardCharsets.UTF_8));
                }
            }
        }catch (IOException x) {
            throw new MojoExecutionException("An IOException occurred: " + x);
        }
    }


    private Map<String, ArrayList<String>> _listModules()
    {
        // Maps a module path to the list of modules under the path
        TreeMap<String, ArrayList<String>> map = new TreeMap<>();
        for (Profile profile : project.getActiveProfiles()) {
            for (String m : profile.getModules()) {
                int slash = m.lastIndexOf('/');
                String group = (slash <= 0 ? "" : m.substring(0, slash));
                ArrayList<String> modules = map.get(group);
                if (modules == null) {
                    modules = new ArrayList<>();
                    map.put(group, modules);
                }
                modules.add(m.substring(slash + 1));
            }
        }

        return map;
    }
}
