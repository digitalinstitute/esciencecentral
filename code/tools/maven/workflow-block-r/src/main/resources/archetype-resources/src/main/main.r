#
# This R script is called once for each block
# of data that is passed through this service.
#

# Input and output data is passed between e-Science Central and R using 
# variables with the same names as the block inputs and outputs.
#
# Inputs of type:
#  - data-wrapper (data tables) are encapsulated in R data.frames
#  - file-wrapper (file lists) are encapsulated in R character vectors using c()

# Block properties are stored in variable 'properties' and can be accessed 
# directly:
#    'properties["PROP_NAME",]$value'
#
# or using function 
#    'getProperty("PROP_NAME")'
#
# The function returns a value of property PROP_NAME coerced to the appropriate 
# type.
# Special property 'InvocationId' is defined that uniquely identifies 
# the current invocation of this service.


# This command simply copies input x to output y
y <- x;

# To report progress back to the server, use the following:
#
# cat(paste("escprogress:", percent, "\n", sep=""));
#
# where 'percent' is a value 0-100 calculated by the code author
# reflecting the percentage completion of this block