<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 */

// Include the client code
include 'escStorageClient.php';

// Basic setup parameters - add server hostname, port, username password
$host = "localhost";
$port = 8080;
$username = "__YOUR_USERNAME_HERE__";
$password = "__YOUR_PASSWORD_HERE__";

// Create the client and get the users home directory
$client = new escStorageClient($host, $port, $username, $password);

echo "---------------------------------\r\n";
echo "Dump of home directory variables:\r\n";
echo "---------------------------------\r\n";
$homedir = $client->homeFolder();
var_dump($homedir);

// List all of the documents in the users home directory
echo "\n\n";
echo "----------------------------------------\r\n";
echo "Listing of files in user home directory:\r\n";
echo "----------------------------------------\r\n";
$docs = $client->folderDocuments($homedir->id);
var_dump($docs);

// List all of the subfolders in the users home directory
echo "\n\n";
echo "-----------------------------------------------------\r\n";
echo "Listing of all subdirectories in user home directory:\r\n";
echo "-----------------------------------------------------\r\n";
$subfolders = $client->listChildFolders($homedir->id);
var_dump($subfolders);

// List all of the projects / studies the user can see
echo "\n\n";
echo "---------------------------------------\r\n";
echo "Studies / Projects visible to the user:\r\n";
echo "---------------------------------------\r\n";
$projects = $client->listProjects();
var_dump($projects);

// Create a new subfolder in the users home directory
echo "\n\n";
echo "---------------------------------------------------\r\n";
echo "Newly created subdirectory in users home directory:\r\n";
echo "---------------------------------------------------\r\n";
$folder = $client->createChildFolder($homedir->id, "New Folder");
var_dump($folder);
$client->deleteFolder($folder->id); // Now delete that folder

// Create a new document in the home directory
echo "\n\n";
echo "Newly created document in user home directory:\r\n";
echo "----------------------------------------------\r\n";
$doc = $client->createDocumentInFolder($homedir->id, "Document.csv");
var_dump($doc);

// Upload a document to that location. This returns an EscDocumentVersion object
// that contains all of the details about the file uploaded. This includes the
// MD5 hash, so that the upload can be verified if required
echo "\n\n";
echo "EscDocumentVersion representing newly uploaded document:\r\n";
echo "--------------------------------------------------------\r\n";
$uploadedVersion = $client->upload($doc, "/Users/hugo/data.csv");   // Change this to the file you want to upload
var_dump($uploadedVersion);

// Get the metadata for the document that has just been created and uploaded.
echo "\n\n";
echo "-------------------------------------\r\n";
echo "Metadata for newly uploaded document:\r\n";
echo "-------------------------------------\r\n";
$metadata = $client->getDocumentMetadata($doc->id);
var_dump($metadata);

// Create a new piece of metadata to be attached to the uploaded document
$md = new EscMetadataItem();
$md->category = "C56";          // Category name for the metadata
$md->metadataType = "BOOLEAN";  // Valid types are: BOOLEAN, DATE, TEXT, NUMERICAL
$md->name = "MyBooleanMD";      // Name of the piece of metadata
$md->stringValue = "true";      // Set the value as a string

// Attach this piece of metadata
echo "\n\n";
echo "---------------------------------\r\n";
echo "Newly attached piece of metadata:\r\n";
echo "---------------------------------\r\n";
$md = $client->addMetadataToDocument($doc->id, $md);
var_dump($md);                  // Print out the returned value - this will now include the database ID

// Find the latest version of the document that has been uploaded. This should be
// the same as that returned when the document was uploaded unless another version
// has been uploaded in the meantime
echo "\n\n";
echo "------------------------------------------\r\n";
echo "Latest version of newly uploaded document:\r\n";
echo "------------------------------------------\r\n";
$latestVersion = $client->getLatestDocumentVersion($doc->id);
var_dump($latestVersion);

// Now download the latest version of the document we originally uploaded
$fileName = "/Users/hugo/download.csv";
$client->download($doc, $fileName);

// List all of the versions of the uploaded document
echo "\n\n";
echo "----------------------------------------\r\n";
echo "All versions of newly uploaded document:\r\n";
echo "----------------------------------------\r\n";
$versions = $client->listDocumentVersions($doc->id);
var_dump($versions);

// remove the uploaded document
$client->deleteDocument($doc->id);


?>
