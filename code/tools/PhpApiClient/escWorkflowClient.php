<?php

/*
 * This file provides a client for the workflow API exposed by e-Science Central
 */
include_once 'escDataTypes.php';
class escWorkflowClient {
    private $_host;
    private $_port;
    private $_username;
    private $_password;
    
    function escWorkflowClient($host, $port, $username, $password){
        $this->_host=$host;
        $this->_port=$port;
        $this->_username=$username;
        $this->_password=$password;
    }
    
    private function createUrl($location){
        $url = "http://" . $this->_username . ":" . $this->_password . "@" . $this->_host . ":" . $this->_port . "/api/public/rest/v1/workflow" . $location;
        return $url;
    }
    
    private function populateObject($object, $json){
        foreach ($json as $key => $value) {
             $object->$key = $value;
        }           
        return $object;
    }    
}
?>
