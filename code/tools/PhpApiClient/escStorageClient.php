<?php
/**
 * This file provides a PHP implementation of the e-Science Central storage
 * client.
 *
 * @author hugo
 */
include_once 'escDataTypes.php';
class escStorageClient {
    private $_host;
    private $_port;
    private $_username;
    private $_password;
    
    function escStorageClient($host, $port, $username, $password){
        $this->_host=$host;
        $this->_port=$port;
        $this->_username=$username;
        $this->_password=$password;
    }
    
    private function createUrl($location){
        $url = "http://" . $this->_username . ":" . $this->_password . "@" . $this->_host . ":" . $this->_port . "/api/public/rest/v1/storage" . $location;
        return $url;
    }
    
    private function populateObject($object, $json){
        foreach ($json as $key => $value) {
             $object->$key = $value;
        }           
        return $object;
    }
    
    function homeFolder(){
        return $this->populateObject(new EscFolder(), json_decode(file_get_contents($this->createUrl("/specialfolders/home"))));
    }
    
    function createChildFolder($id, $name){
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: text/plain\r\n" . "Content-Length: ". strlen($name) . "\r\n",  
            "content" => $name)));  
        $url = $this->createUrl("/folders/" . $id);
        return $this->populateObject(new EscFolder(), json_decode(file_get_contents($url, false, $context)));
    }
    
    function createDocumentInFolder($id, $name){
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: text/plain\r\n" . "Content-Length: ". strlen($name) . "\r\n",  
            "content" => $name)));  
        $url = $this->createUrl("/folders/" . $id . "/documents/create");
        return $this->populateObject(new EscDocument(), json_decode(file_get_contents($url, false, $context)));
    }
    
    function upload($escDocument, $file){
        $fileHandle = fopen($file, "rb");
        $fileContents = stream_get_contents($fileHandle);
        $length = filesize($file);
        
        fclose($fileHandle);        
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: application/data\r\n" . "Content-Length: ". $length . "\r\n",  
            "content" => $fileContents)));  
        $url = $this->createUrl("/data/" . $escDocument->id);

        $versionId = intval(file_get_contents($url, false, $context));
        $version = $this->getDocumentVersion($versionId);
        return $version;
    }
    
    function currentUser() {
        return $this->populateObject(new EscUser(), json_decode(file_get_contents($this->createUrl("/currentuser"))));
    }

    function folderDocuments($id) {
        $jsonArray = json_decode(file_get_contents($this->createUrl("/folders/" . $id . "/documents")));
        $size = count($jsonArray);
        $results = array();

        for($i=0;$i<$size;$i++){
            $results[$i] = $this->populateObject(new EscDocument(), $jsonArray[$i]);
        }
        return $results;
    }

    function getDocument($id) {
        return $this->populateObject(new EscDocument(), json_decode(file_get_contents($this->createUrl("/documents/". $id))));
    }
    
    function getFolder($id) {
        return $this->populateObject(new EscFolder(), json_decode(file_get_contents($this->createUrl("/folders/". $id))));
    }
    
    function listChildFolders($id) {
        $jsonArray = json_decode(file_get_contents($this->createUrl("/folders/" . $id . "/children")));
        $size = count($jsonArray);
        $results = array();

        for($i=0;$i<$size;$i++){
            $results[$i] = $this->populateObject(new EscFolder(), $jsonArray[$i]);
        }
        return $results;        
    }

    function listDocumentVersions($id) {
        $jsonArray = json_decode(file_get_contents($this->createUrl("/documents/" . $id . "/versions")));
        $size = count($jsonArray);
        $results = array();

        for($i=0;$i<$size;$i++){
            $results[$i] = $this->populateObject(new EscDocumentVersion(), $jsonArray[$i]);
        }
        return $results;           
    }
    
    function getDocumentVersion($id) {
        return $this->populateObject(new EscDocumentVersion(), json_decode(file_get_contents($this->createUrl("/documentversions/". $id))));
    }

    function listProjects() {
        $jsonArray = json_decode(file_get_contents($this->createUrl("/projects")));
        $size = count($jsonArray);
        $results = array();

        for($i=0;$i<$size;$i++){
            $results[$i] = $this->populateObject(new EscProject(), $jsonArray[$i]);
        }
        return $results;           
    }
    
    function updateDocument($document) {
        $documentJson = json_encode($document);
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: application/json\r\n" . "Content-Length: ". strlen($documentJson) . "\r\n",  
            "content" => $documentJson))); 
        $url = $this->createUrl("/documents/". $document->id);
        return $this->populateObject(new EscDocument(), json_decode(file_get_contents($url, false, $context)));
    }

    function updateFolder($folder){
        $folderJson = json_encode($folder);
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: application/json\r\n" . "Content-Length: ". strlen($folderJson) . "\r\n",  
            "content" => $folderJson))); 
        $url = $this->createUrl("/folders/". $folder->id);
        return $this->populateObject(new EscFolder(), json_decode(file_get_contents($url, false, $context)));        
    }
    
    function getLatestDocumentVersion($documentId) {
        return $this->populateObject(new EscDocumentVersion(), json_decode(file_get_contents($this->createUrl("/documents/" . $documentId . "/versions/latest"))));
    }

    function deleteDocument($documentId) {
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: text/plain\r\n" . "Content-Length: ". strlen($documentId) . "\r\n",  
            "content" => $documentId))); 
        $url = $this->createUrl("/deletedocument");
        $result = file_get_contents($url, false, $context);
    }   
    
    function deleteFolder($folderId) {
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: text/plain\r\n" . "Content-Length: ". strlen($folderId) . "\r\n",  
            "content" => $folderId))); 
        $url = $this->createUrl("/deletefolder");
        $result = file_get_contents($url, false, $context);
    }
    
    function getDocumentMetadata($id) {
        $jsonArray = json_decode(file_get_contents($this->createUrl("/documents/" . $id . "/metadata")));
        $size = count($jsonArray);
        $results = array();

        for($i=0;$i<$size;$i++){
            $results[$i] = $this->populateObject(new EscMetadataItem(), $jsonArray[$i]);
        }
        return $results;         
    }

    function addMetadataToDocument($id, $metadataItem) {
        $metadataJson = json_encode($metadataItem);
        $context = stream_context_create(array("http"=>array(
            "method" => "POST",
            "header" => "Content-Type: application/json\r\n" . "Content-Length: ". strlen($metadataJson) . "\r\n",  
            "content" => $metadataJson))); 
        $url = $this->createUrl("/documents/". $id . "/metadata");
        return $this->populateObject(new EscMetadataItem(), json_decode(file_get_contents($url, false, $context)));         
    }

    function getTimestamp() {
        return file_get_contents($this->createUrl("/timestamp"));
    }
    
    function download($documentObject, $fileName){
        // Get the document
        $fileHandle = fopen($this->createUrl($documentObject->downloadPath), "rb");
        $write = fopen($fileName, 'w');
        while (!feof($fileHandle)) {
            $buffer = fread($fileHandle, 4096);
            fwrite($write, $buffer);
        }
 
        fclose($fileHandle);
        fclose($write);        
    }
    
    function downloadVersion($versionObject, $fileName){
        // Get the document
        $fileHandle = fopen($this->createUrl($versionObject->downloadPath), "rb");
        $write = fopen($fileName, 'w');
        while (!feof($fileHandle)) {
            $buffer = fread($fileHandle, 4096);
            fwrite($write, $buffer);
        }
 
        fclose($fileHandle);
        fclose($write);        
    }    
}


?>
