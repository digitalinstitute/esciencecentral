﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using com.connexience.server.model;
using com.connexience.server.model.folder;
using com.connexience.server.model.document;


namespace e_SC.Test
{
    [TestClass]
    public class StorageAPITest
    {
        [TestInitialize]
        public void Init()
        {
            _apiRef = new StorageAPI("http://localhost:8080", "cala", "pass123");
        }

        [TestMethod]
        public void TestGetHomeFolder()
        {
            Folder f = _apiRef.GetHomeFolderAsync().Result;
            Assert.IsNotNull(f);
            Assert.IsTrue(f.name.EndsWith("Home Folder"));
        }

        [TestMethod]
        public void TestDocument_Create()
        {
            Folder homeFolder = _apiRef.GetHomeFolderAsync().Result;
            DocumentRecord doc1 = _apiRef.CreateDocumentAsync(homeFolder.id, "Test-" + new Guid().ToString()).Result;
            DocumentRecord doc2 = _apiRef.GetDocumentRecordAsync(doc1.id).Result;
            _apiRef.DeleteDocumentAsync(doc1.id).Wait();

            Assert.AreEqual(doc1, doc2);
        }

        [TestMethod]
        public void TestDocument_Delete()
        {
            Folder homeFolder = _apiRef.GetHomeFolderAsync().Result;
            DocumentRecord doc1 = _apiRef.CreateDocumentAsync(homeFolder.id, "Test-" + new Guid().ToString()).Result;
            _apiRef.DeleteDocumentAsync(doc1.id).Wait();
            try
            {
                doc1 = _apiRef.GetDocumentRecordAsync(doc1.id).Result;
                Assert.Fail();
            }
            catch (Exception)
            {
                // It is ok to get exception here
            }
        }

        private StorageAPI _apiRef;
    }
}
