﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace e_SC.Test
{
    [TestClass]
    public class XStorageAPITest
    {
        [TestMethod]
        public void TestUploadBlock()
        {
            XStorageAPI apiRef = new XStorageAPI("http://localhost:8080", "cala", "pass123");
            using (var data = new MemoryStream(new byte[] { 100 }))
            {
                apiRef.UploadBlock("63", "64", 0, data);
            }
        }
    }
}
