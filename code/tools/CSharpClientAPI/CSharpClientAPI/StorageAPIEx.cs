﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

using com.connexience.server.model;
using com.connexience.server.model.document;
using com.connexience.server.model.folder;
using com.connexience.server.model.metadata;
using com.connexience.server.model.workflow;

namespace e_SC
{
    /// <summary>
    /// This part includes some convenience methods that are not directly available through the REST API
    /// but are useful when interacting with the system.
    /// However, they should be used with care as they usually incur some additional costs by contacting
    /// a remote system multiple times. For example, CreateFolders will need to make several remote calls
    /// until it is able to create a folder deeper in the folder structure.
    /// </summary>
    public partial class StorageAPI
    {
        public async Task<List<T>> GetFolderContentsByIdAsync<T>(string folderId, Func<ServerObject, T> mappingFunc)
        {
            ServerObject[] folderContents = await GetFolderContentsByIdAsync<ServerObject>(folderId);
            var output = new List<T>();
            foreach (var obj in folderContents)
            {
                output.Add(mappingFunc(obj));
            }
            return output;
        }


        /// <summary>
        /// This is an expensive operation that requires calling a remote e-SC systems several times to get the actual folder.
        /// Therefore, whenever possible GetFolderById should be used instead.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<Folder> GetFolderByPathAsync(string path)
        {
            if (path.Length == 0 || path[0] != '/' && path[0] != '#')
                throw new ArgumentException("Path must be absolute (must start with '/') or must start with folder id (must start with '#folder_id'", path);

            string[] pathTokens = path.Split('/');
            if (path[0] == '/')
            {
                HttpResponseMessage response = await _clientRef.GetAsync("/api/rest/storage/specialfolders/home");
                response.EnsureSuccessStatusCode();

                Folder f = await response.Content.ReadContentAsAsync<Folder>();

                if (pathTokens.Length == 1)
                    return f;
                else
                    return await _GetFolderAsync(f, 1, pathTokens);
            }
            else // if (path[0] == '#')
            {
                return await _GetFolderAsync(await GetFolderByIdAsync(pathTokens[0].Substring(1)), 1, pathTokens);
            }
        }


        /// <summary>
        /// This is a helper function that underneath uses GetFolderByPathAsync. It is an expensive operation which requires
        /// a remote e-SC system to be called multiple times to get the actual folder. Consider using 
        /// GetFolderContentsByIdAsync whenever possible.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="mappingFunc"></param>
        /// <returns></returns>
        public async Task<List<T>> GetFolderContentsByPathAsync<T>(string path, Func<ServerObject, T> mappingFunc)
        {
            Folder f = await GetFolderByPathAsync(path);
            return await GetFolderContentsByIdAsync(f.id, mappingFunc);
        }


        /// <summary>
        /// This operation is expensive as going down the folder structure requires multiple calls to a remote system.
        /// However, there's no native support for this in the e-SC APIs.
        /// </summary>
        /// <param name="documentPath"></param>
        /// <returns></returns>
        public async Task<string> GetDocumentIdByPathAsync(string path)
        {
            if (path.Length == 0 || path[0] != '/' && path[0] != '#')
                throw new ArgumentException("Path must be absolute (must start with '/') or must start with folder id (must start with '#folder_id'", path);

            Folder parentFolder;
            string[] pathTokens = path.Split('/');
            if (path[0] == '/')
            {
                parentFolder = await GetHomeFolderAsync();

                if (pathTokens.Length == 1)
                    return parentFolder.id;
            }
            else // if (path[0] == '#')
            {
                parentFolder = await GetFolderByIdAsync(pathTokens[0].Substring(1));
            }

            return await _GetDocumentIdAsync(parentFolder, 1, path.Split('/'));
        }


        /// <summary>
        /// This operation is expensive since it requires calling a remote e-SC instance multiple times as it goes down
        /// the path. There's no native support for this in the e-SC APIs.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<Folder> CreateFoldersAsync(string path)
        {
            if (path.Length == 0 || path[0] != '/' && path[0] != '#')
                throw new ArgumentException("Path must be absolute (must start with '/') or must start with folder id (must start with '#folder_id'", path);

            Folder parentFolder;
            string[] pathTokens = path.Split('/');
            if (path[0] == '/')
            {
                if (pathTokens.Length == 1)
                    throw new ArgumentException("Invalid path");

                parentFolder = await GetHomeFolderAsync();
            }
            else // if (path[0] == '#')
            {
                parentFolder = await GetFolderByIdAsync(pathTokens[0].Substring(1));
            }

            return await _GetOrCreateFoldersAsync(parentFolder, 1, pathTokens);
        }


        /// <summary>
        /// This is a convenience method to upload document given it's name and contents.
        /// </summary>
        /// <param name="parentFolderId"></param>
        /// <param name="name"></param>
        /// <param name="inputStream"></param>
        public async Task UploadDocumentAsync(string parentFolderId, string name, Stream inputStream)
        {
            var doc = await CreateDocumentAsync(parentFolderId, name);
            await UploadDocumentAsync(doc.id, inputStream);
        }

        #region Implementation Area

        private HttpClient _clientRef;


        private async Task<Folder> _GetOrCreateFoldersAsync(Folder parent, int index, string[] path)
        {
            if (index == path.Length)
                return parent;

            Folder[] contents = await GetFolderContentsByIdAsync<Folder>(parent.id);
            foreach (var f in contents)
            {
                if (path[index].Equals(f.name))
                    return await _GetOrCreateFoldersAsync(f, index + 1, path);
            }
            return await _GetOrCreateFoldersAsync(await CreateChildFolderAsync(parent.id, path[index]), index + 1, path);
        }


        private async Task<string> _GetDocumentIdAsync(Folder parent, int index, string[] documentPath)
        {
            if (index == documentPath.Length - 1)
            {
                ServerObject[] contents = await GetFolderContentsByIdAsync<ServerObject>(parent.id);

                foreach (var obj in contents)
                {
                    if (documentPath[index].Equals(obj.name))
                        return obj.id;
                }
            }
            else
            {
                Folder[] contents = await GetFolderContentsByIdAsync<Folder>(parent.id);

                foreach (var f in contents)
                {
                    if (documentPath[index].Equals(f.name))
                        return await _GetDocumentIdAsync(f, index + 1, documentPath);
                }
            }

            return null;
        }


        private async Task<Folder> _GetFolderAsync(Folder parent, int index, string[] path)
        {
            if (index == path.Length)
                return parent;
            else
            {
                Folder[] contents = await GetFolderContentsByIdAsync<Folder>(parent.id);
                foreach (var f in contents)
                {
                    if (path[index].Equals(f.name))
                    {
                        return await _GetFolderAsync(f, index + 1, path);
                    }
                }
                return null;
            }
        }

        #endregion Implementation Area
    }
}
