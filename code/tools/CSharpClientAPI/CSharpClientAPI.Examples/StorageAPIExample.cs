﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using com.connexience.server.model.folder;
using com.connexience.server.model.document;


namespace e_SC.Examples
{
    class StorageAPIExample
    {
        static void Main(string[] args)
        {
            StorageAPI apiRef = null;
            DocumentRecord doc1 = null;
            apiRef = new StorageAPI("http://localhost:8080", "cala", "pass123");

            Folder homeFolder = apiRef.GetHomeFolderAsync().Result;
            doc1 = apiRef.CreateDocumentAsync(homeFolder.id, "Test-" + Guid.NewGuid().ToString()).Result;
            DocumentRecord doc2 = apiRef.GetDocumentRecordAsync(doc1.id).Result;
            apiRef.DeleteDocumentAsync(doc1.id).Wait();
            try
            {
                doc1 = apiRef.GetDocumentRecordAsync(doc1.id).Result;
                throw new ApplicationException("Zonk 2!");
            }
            catch (Exception x)
            {
                // It is ok to get exception here
                Console.WriteLine(x);
            }
        }

    }
}
