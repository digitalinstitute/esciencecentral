/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience;

import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.EscDocument;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import com.connexience.api.model.json.JSONObject;
import java.util.HashMap;
import junit.framework.TestCase;

/**
 *
 * @author hugo
 */
public class ExternallyCallableWorkflowTest extends TestCase {
    public void testListExternallyCallableWorkflows() throws Exception {
        WorkflowClient c = new WorkflowClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
        
        EscWorkflow[] results = c.listCallableWorkflows();
        for(int i=0;i<results.length;i++){
            System.out.println("Workflow: " + i + ": " + results[i].getName());
            
            HashMap<String,String> params = c.listCallableWorkflowParameters(results[i].getId());
            for(String key : params.keySet()){
                System.out.println(key + ": " + params.get(key));
            }
        }
    }
    
    public void testExternalWorkflowCall() throws Exception {
        WorkflowClient c = new WorkflowClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
        StorageClient sc = new StorageClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
        
        EscWorkflow wf = c.getWorkflow(TestSettingsManager.getProperty("externallycallableworkflowtest.workflowid", "6321"));
        if(wf!=null){
            System.out.println("Found workflow: " + wf.getName());
            
            EscDocument dataFile = sc.getDocument(TestSettingsManager.getProperty("externallycallableworkflowtest.fileid", "341"));
            System.out.println("Found data file: " + dataFile.getName());
            
            JSONObject parametersJson = new JSONObject();
            parametersJson.put("Source", dataFile.getId());
            parametersJson.put("SplitLocation", 30);
            
            EscWorkflowInvocation invocation = c.executeCallableWorkflow(wf.getId(), parametersJson.toString());
        } else {
            throw new Exception("Workflow does not exist");
        }
    }
}