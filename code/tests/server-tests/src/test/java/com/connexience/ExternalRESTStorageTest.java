/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import java.io.File;
import junit.framework.TestCase;
import com.connexience.api.model.*;
import com.connexience.api.*;

/**
 * This class tests the external REST storage service that uses a simplified
 * data model.
 * @author hugo
 */
public class ExternalRESTStorageTest extends TestCase {
    @SuppressWarnings("unchecked")
    public void testStorageClient() throws Exception {
            StorageClient c = new StorageClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());
            EscUser u = c.currentUser();
            System.out.println(u.toJsonObject().toString(2));
            
            EscFolder home = c.homeFolder();
            EscFolder fetchedFolder = c.getFolder(home.getId());
            System.out.println(fetchedFolder.toJsonObject().toString(2));
            
            /*
            System.out.println(home.toJsonObject().toString(2));
            */
            
            EscFolder testFolder = c.createChildFolder(home.getId(), TestSettingsManager.getProperty("resttestfoldername", "Test Folder"));
            System.out.println(testFolder.toJsonObject().toString(2));
            c.deleteFolder(testFolder.getId());
            EscDocument[] contents = c.folderDocuments(home.getId());
            
            for(int i=0;i<contents.length;i++){
                System.out.println(contents[i].toJsonObject().toString(2));
                
                EscDocument fetched = c.getDocument(contents[i].getId());
                System.out.println(fetched.toJsonObject());
                
                EscDocumentVersion[] versions = c.listDocumentVersions(fetched.getId());
                for(int j=0;j<versions.length;j++){
                    System.out.println(versions[j].toJsonObject().toString(2));
                }
                
                EscMetadataItem[] md = c.getDocumentMetadata(fetched.getId());
                if(md.length>0){
                    for(int j=0;j<md.length;j++){
                        System.out.println(md[j].toJsonObject().toString(2));
                    }
                }
                
                if(fetched.getName().equals(TestSettingsManager.getProperty("restdownloadfile", "data.csv"))){
                    File tmpFile = new File(TestSettingsManager.getDataDirectory(), TestSettingsManager.getProperty("restdownloadfile", "data.csv"));
                    if(tmpFile.exists()){
                        tmpFile.delete();
                    }
                    c.download(fetched, tmpFile);
                }
            }
            
            EscFolder[] children = c.listChildFolders(home.getId());
            for(int i=0;i<children.length;i++){
                System.out.println(children[i].toJsonObject());
            }
            
            //c.deleteDocument("12941");
            EscDocumentVersion v = c.upload(home, new File(TestSettingsManager.getDataDirectory(), TestSettingsManager.getProperty("restuploadfile","test.m")));
            System.out.println("Uploaded: " + v);        
    }
}
