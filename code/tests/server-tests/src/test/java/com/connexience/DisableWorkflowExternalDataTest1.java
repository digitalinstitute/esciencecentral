/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience;

import com.connexience.api.StorageClient;
import com.connexience.api.WorkflowClient;
import com.connexience.api.model.EscDocumentVersion;
import com.connexience.api.model.EscWorkflow;
import com.connexience.api.model.EscWorkflowInvocation;
import com.connexience.api.model.EscWorkflowParameterList;
import junit.framework.TestCase;

/**
 * Tests whether it is possible to enable / disable external data support
 * for a workflow via tha API
 * @author hugo
 */
public class DisableWorkflowExternalDataTest1 extends TestCase {
    public void testWorkflowService() throws Exception {
        WorkflowClient c = new WorkflowClient(TestSettingsManager.getHostname(), TestSettingsManager.getHttpPort(), false, TestSettingsManager.getUsername(), TestSettingsManager.getPassword());

        EscWorkflow[] wfs = c.listWorkflows();
        for(int i=0;i<wfs.length;i++){
            System.out.println(wfs[i].toJsonObject().toString(2));
        }

        if(wfs.length>0){
            EscWorkflow wf = c.getWorkflow(wfs[0].getId());
            System.out.println("Fetched: " + wf.toJsonObject());
            
            c.disableWorkflowExternalDataSupport(wf.getId());
        }

    }    
}
