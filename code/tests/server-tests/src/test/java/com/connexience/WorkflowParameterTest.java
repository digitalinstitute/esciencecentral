/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;
import com.connexience.api.model.*;
import junit.framework.TestCase;

/**
 * Very basic test of EscWorkflowParameters and EscWorkflowParameterLists
 * @author hugo
 */
public class WorkflowParameterTest extends TestCase {
    public void testWorkflowParameter() throws Exception {
        EscWorkflowParameter p = new EscWorkflowParameter();
        p.setBlockName("MyBlock");
        p.setName("Interations");
        p.setValue("55");
        System.out.println(p.toJsonObject().toString(2));
    }
    
    public void testWorkflowParameterList() throws Exception {
        EscWorkflowParameterList list = new EscWorkflowParameterList();
        
        list.addParameter("Block1", "Iterations", "45");
        list.addParameter("Block1", "Neurons", "3");
        list.addParameter("Block3", "FileName", "data.csv");

        
        System.out.println(list.toJsonObject().toString(2));
    }
}
