/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience;

import com.connexience.server.rest.FileTypeStats;
import com.connexience.server.rest.QuotaStats;
import com.connexience.server.rest.WorkflowStats;
import com.connexience.server.rest.api.ILogonAPI;
import com.connexience.server.rest.api.IStatsAPI;
import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.http.impl.client.DefaultHttpClient;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.ProxyFactory;
import org.jboss.resteasy.client.core.executors.ApacheHttpClient4Executor;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import javax.ws.rs.core.Response;

/**
 * Unit test for simple App.
 */
public class StatsAPITest extends TestCase {

    private String baseURL = "http://localhost:8080/beta/rest/";

    private String user1Id = "52";
    private String user1UserName = "simon.woodman@ncl.ac.uk";
    private String user1Password = "password";
    private String user1FirstName = "Simon";

    private String user2Id = "176";
    private String user2UserName = "h.g.hiden@ncl.ac.uk";
    private String user2Password = "password";

    private String dataId = "161";


    IStatsAPI statsAPI = null;
    ILogonAPI logonAPI = null;


    static {
        // this initialization only needs to be done once per VM
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
    }

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public StatsAPITest(String testName) {
        super(testName);

        //Use the same httpClient for all of the APIs so that we share the session
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ClientExecutor clientExecutor = new ApacheHttpClient4Executor(httpClient);

        statsAPI = ProxyFactory.create(IStatsAPI.class, baseURL + "stats/", clientExecutor);
        logonAPI = ProxyFactory.create(ILogonAPI.class, baseURL + "account/", clientExecutor);

    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(StatsAPITest.class);
    }

    @SuppressWarnings("unchecked")
    public void testGetWorkflowStats() throws Exception {

        //Login
        ClientResponse loginResponse = (ClientResponse) logonAPI.login(user1UserName, user1Password);
        Assert.assertEquals(loginResponse.getStatus(), Response.Status.OK.getStatusCode());
        String sessionId = (String) loginResponse.getEntity(String.class);
        Assert.assertNotNull(sessionId);
        loginResponse.releaseConnection();

        WorkflowStats stats = statsAPI.getWorkflowStats();
        Assert.assertTrue(stats.getSuccess() > 0);

        QuotaStats quotaStats = statsAPI.getQuotaStats();
        Assert.assertTrue(quotaStats.getUsed() > 0);

        FileTypeStats fileTypeStats = statsAPI.getFileTypes();
        Assert.assertTrue(fileTypeStats.getFiletypes().keySet().size() > 0);
    }


}
