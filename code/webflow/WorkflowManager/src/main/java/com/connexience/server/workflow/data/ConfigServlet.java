/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.connexience.server.workflow.data;

import com.connexience.server.ejb.util.EJBLocator;
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentType;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.organisation.Organisation;
import com.connexience.server.model.security.Permission;
import com.connexience.server.model.security.WebTicket;
import com.connexience.server.util.StorageUtils;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author hugo
 */
public class ConfigServlet extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OutputStream out = null;
        try {
            WebTicket ticket = EJBLocator.lookupTicketBean().createPublicWebTicket();
            Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, org.getConfigFolderId());

            ServerObject resource = EJBLocator.lookupStorageBean().getResourceByURI(ticket, f.getId(), request.getPathInfo(), Permission.READ_PERMISSION);
            if (resource instanceof DocumentRecord) {
                DocumentRecord doc = (DocumentRecord) resource;

                DocumentVersion version = EJBLocator.lookupStorageBean().getLatestVersion(ticket, resource.getId());

                // Sort out the mime type
                if (doc.getName().endsWith(".htm") || doc.getName().endsWith(".html")) {
                    // Send html as the correct type
                    response.setContentType("text/html");
                } else {
                    if (doc.getDocumentTypeId() != null) {
                        DocumentType docType = EJBLocator.lookupStorageBean().getDocumentType(ticket, doc.getDocumentTypeId());
                        if (docType != null) {
                            response.setContentType(docType.getMimeType());
                        } else {
                            response.setContentType("application/data");
                        }
                    } else {
                        response.setContentType("application/data");
                    }
                }
                out = response.getOutputStream();
                StorageUtils.downloadFileToOutputStream(ticket, doc, version, out);
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                if (!response.isCommitted()) {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } catch (Exception e) {
            if (!response.isCommitted()) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

        } finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OutputStream out = null;
        try {
            WebTicket ticket = EJBLocator.lookupTicketBean().createPublicWebTicket();
            Organisation org = EJBLocator.lookupOrganisationDirectoryBean().getOrganisation(ticket, ticket.getOrganisationId());
            ticket.setGroupIds(new String[]{org.getAdminGroupId()});
            ticket.setSuperTicket(true);
            Folder f = EJBLocator.lookupStorageBean().getFolder(ticket, org.getConfigFolderId());

            //ServerObject resource = EJBLocator.lookupStorageBean().getResourceByURI(ticket, f.getId(), request.getPathInfo(), Permission.READ_PERMISSION);
            ServerObject resource = EJBLocator.lookupStorageBean().getOrCreateResourceAtURI(ticket, f.getId(), request.getPathInfo(), true);
            if (resource instanceof DocumentRecord) {
                DocumentRecord doc = (DocumentRecord) resource;
                StorageUtils.upload(ticket, request.getInputStream(), request.getContentLength(), doc, "Uploaded from config servlet");
                response.setStatus(HttpServletResponse.SC_NO_CONTENT);
            } else {
                if (!response.isCommitted()) {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        } catch (Exception e) {
            if (!response.isCommitted()) {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }

        } finally {
            if (out != null) {
                out.flush();
                out.close();
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
    

}