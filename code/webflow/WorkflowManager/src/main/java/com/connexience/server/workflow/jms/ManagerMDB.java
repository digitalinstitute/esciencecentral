/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.workflow.jms;

import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.util.Base64;
import com.connexience.server.util.SerializationUtils;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.log4j.*;
/**
 *
 * @author hugo
 */
@MessageDriven(name = "ManagerMDB",
        activationConfig =
                {
                        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
                        @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/WorkflowManagerQueue")
                }, mappedName = "queue/WorkflowManagerQueue")
public class ManagerMDB implements MessageListener {
    private Logger logger = Logger.getLogger(ManagerMDB.class);
    @Override
    public void onMessage(Message msg) {
        try {
            if(msg instanceof TextMessage){
                TextMessage tm = (TextMessage)msg;
                if(tm.getText().equals("setCurrentBlock")){
                    setCurrentBlock(tm);
                    
                } else if(tm.getText().equals("updateServiceLog")){
                    updateServiceLog(tm);
                    
                } else if(tm.getText().equals("updateServiceLogMessage")){
                    updateServiceLogMessage(tm);
                    
                } else if(tm.getText().equals("notifyWorkflowDequeued")){
                    notifyWorkflowDequeued(tm);

                } else if(tm.getText().equals("notifyWorkflowStarted")){
                    notifyWorkflowStarted(tm);
                    
                } else if(tm.getText().equals("notifyWorkflowFinished")){
                    notifyWorkflowFinished(tm);    
                    
                } else if(tm.getText().equals("setWorkflowStatus")){
                    setWorkflowStatus(tm);
                    
                } else if(tm.getText().equals("setInvocationEngineId")){
                    setInvocationEngineId(tm);
                    
                }
            }
        } catch (Exception e){
            logger.error("Error workflow update message: " + e.getMessage());
        }
    }
    
    private void setInvocationEngineId(TextMessage tm) throws Exception {
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        String invocationId = tm.getStringProperty("invocationId");
        String engineId = tm.getStringProperty("engineId");
        WorkflowEJBLocator.lookupWorkflowManagementBean().setDynamicWorkflowEngineForInvocation(t, invocationId, engineId);
    }
    
    private void setWorkflowStatus(TextMessage tm) throws Exception {
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        String invocationId = tm.getStringProperty("invocationId");
        int status = tm.getIntProperty("status");
        String message = tm.getStringProperty("message");
        WorkflowEJBLocator.lookupWorkflowManagementBean().setWorkflowStatus(t, invocationId, status, message);
    }
    
    private void notifyWorkflowDequeued(TextMessage tm) throws Exception {
        String invocationId = tm.getStringProperty("invocationId");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        WorkflowEJBLocator.lookupWorkflowManagementBean().logWorkflowDequeued(t, invocationId);
    }
    
    private void notifyWorkflowStarted(TextMessage tm) throws Exception {
        String invocationId = tm.getStringProperty("invocationId");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        WorkflowEJBLocator.lookupWorkflowManagementBean().logWorkflowExecutionStarted(t, invocationId);
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId);
        WorkflowEJBLocator.lookupWorkflowLockBean().updateLockMember(t, invocation);        
    }    
    
    private void notifyWorkflowFinished(TextMessage tm) throws Exception {
        String invocationId = tm.getStringProperty("invocationId");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        WorkflowEJBLocator.lookupWorkflowManagementBean().logWorkflowComplete(t, invocationId, "");
        WorkflowInvocationFolder invocation = WorkflowEJBLocator.lookupWorkflowManagementBean().getInvocationFolder(t, invocationId);
        WorkflowEJBLocator.lookupWorkflowLockBean().updateLockMember(t, invocation);
    }
    
    private void setCurrentBlock(TextMessage tm) throws Exception {
        // Need to check if the workflow is debugging. If it is, then this method should
        // Ignore the current block message
        String contextId = tm.getStringProperty("contextId");
        String invocationId = tm.getStringProperty("invocationId");
        int percentComplete = tm.getIntProperty("percentComplete");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));
        WorkflowEJBLocator.lookupWorkflowManagementBean().setCurrentBlockForInvocation(t, invocationId, contextId, percentComplete);
    }
    
    private void updateServiceLog(TextMessage tm) throws Exception {
        String contextId = tm.getStringProperty("contextId");
        String invocationId = tm.getStringProperty("invocationId");
        String outputData = tm.getStringProperty("outputData");
        String statusText = tm.getStringProperty("statusText");
        String statusMessage = tm.getStringProperty("statusMessage");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));     
        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceLog(t, invocationId, contextId, outputData, statusText, statusMessage);
    }
    
    private void updateServiceLogMessage(TextMessage tm) throws Exception {
        String contextId = tm.getStringProperty("contextId");
        String invocationId = tm.getStringProperty("invocationId");
        String statusText = tm.getStringProperty("statusText");
        String statusMessage = tm.getStringProperty("statusMessage");
        Ticket t = (Ticket) SerializationUtils.deserialize(Base64.decode(tm.getStringProperty("ticketData")));     
        WorkflowEJBLocator.lookupWorkflowManagementBean().updateServiceLogMessage(t, invocationId, contextId, statusText, statusMessage);
        
    }
}
