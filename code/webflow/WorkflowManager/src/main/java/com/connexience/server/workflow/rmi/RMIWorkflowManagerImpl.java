/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.rmi;

import com.connexience.server.ConnexienceException;
import com.connexience.server.ejb.util.WorkflowEJBLocator;
import com.connexience.server.model.security.Ticket;
import com.connexience.server.workflow.api.impl.RMIService;
import com.connexience.server.workflow.api.impl.RMIWorkflowManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.log4j.*;

import java.rmi.registry.*;

/**
 * This class provides the workflow manager implementation
 * @author hugo
 */
@Singleton
@Startup
public class RMIWorkflowManagerImpl extends UnicastRemoteObject implements RMIWorkflowManager
{
    /**
     * Class version UID.
     * 
     * Please increment this value whenever your changes may cause 
     * incompatibility with the previous version of this class. If unsure, ask 
     * one of the core development team or read:
     *   http://docs.oracle.com/javase/6/docs/api/java/io/Serializable.html
     * and
     *   http://docs.oracle.com/javase/6/docs/platform/serialization/spec/version.html#6678
     */
    private static final long serialVersionUID = 1L;

    private static Logger logger = Logger.getLogger(RMIWorkflowManagerImpl.class);
    private Registry registry;
    private int port = 2199;
    
    public RMIWorkflowManagerImpl() throws RemoteException {
        logger.info("STARTING RMI Workflow Manager Service");
        logger.info("Looking for Workflow manager registry on port: " + port);
        Registry r = null;
        try {
            r = LocateRegistry.getRegistry("127.0.0.1", port);  
            r.list();
            logger.info("Found registry on port: " + port);
        } catch (RemoteException ex){
            logger.info("Attempting to create registry on port: " + port);
            r = LocateRegistry.createRegistry(port);
            logger.info("Created registry on port: " + port);    
        }
        
        if(r!=null){
            registry = r;
        }
        
        try {
            if(registry!=null){
               try {
                   registry.unbind("RMIWorkflowManager");
               } catch (Exception e){}
               registry.bind("RMIWorkflowManager", this);
               logger.info("Registered RMIWorkflowManager in RMI Registry");
            } else {
                logger.error("No workflow manager RMI registry on port: " + port);
            }
            
        } catch (Exception e){
            logger.error("Cannot register RMIWorkflowManger in RMI Registry: " + e.getMessage());
        }
        
        // Reset RMI comms
        try {
            logger.info("Resetting RMI communications on engines");
            WorkflowEJBLocator.lookupWorkflowManagementBean().resetEngineRmiCommumications();
        } catch (Exception e){
            logger.error("Error resetting RMI communications on engines");
        }
    }
    
    @PreDestroy
    public void terminate(){
        logger.info("STOPPING RMI Workflow Manager Service");
        try {
            registry.unbind("RMIWorkflowManager");
            logger.info("Unregistered RMIWorkflowManager from RMI Registry");
        } catch (Exception e){
            logger.error("Cannot unregister RMIWorkflowManager from RMI Registry: " + e.getMessage());   
        }
        
        try {
            UnicastRemoteObject.unexportObject(this, true);
            logger.info("Unexported RMIWorkflowManager");
        } catch (Exception e){
            logger.error("Error unexporting RMIWorkflowManager: " + e.getMessage());
        }
    }
    
    @Override
    public RMIService createService(Ticket ticket) throws RemoteException, ConnexienceException {
        return new RMIServiceImpl(ticket);
    }

    @Override
    public RMIService createService() throws RemoteException, ConnexienceException {
        return new RMIServiceImpl();
    }    
}