#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -Djava.rmi.server.hostname=localhost -Dlog4j.configuration=enginedebug.properties -Djava.library.path=$DIR/lib  -cp "$DIR/../lib/*"  com.connexience.server.workflow.cloud.CloudWorkflowEngine
