@echo off

if [%1]==[-dy] (
  set DEBUG=-agentlib:jdwp=transport=dt_socket,address=5004,server=y,suspend=y
) else if [%1]==[-d] (
  set DEBUG=-agentlib:jdwp=transport=dt_socket,address=5004,server=y,suspend=n
) else (
  set DEBUG=
)

set SIGAR=%~dp0\lib

set LOGCONF=-Dlog4j.configuration=enginedebug.properties
rem set LOGCONF=-Dlog4j.configuration=file:///%~dp0..\src\main\resources\log4j-debug.properties 

java %DEBUG% %LOGCONF% -Djava.library.path="%SIGAR%" -cp "%~dp0..\lib\*" com.connexience.server.workflow.cloud.CloudWorkflowEngine
