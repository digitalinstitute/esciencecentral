#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ENGINE_DIR=$DIR/download

./engine.sh $1 $DIR $ENGINE_DIR
while [ $? -ne 0 -a  $? -ne 130 ];do
	./engine.sh $1 $DIR $ENGINE_DIR
done
