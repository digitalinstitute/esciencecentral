#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

java -Dlog4j.configuration=enginelogging.properties -Djava.library.path=$DIR/lib -cp "$DIR/../lib/*" com.connexience.server.workflow.util.SigarData
