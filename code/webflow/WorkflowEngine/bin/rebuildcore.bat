@echo off

if [%1]==[-dy] (
  set DEBUG=-Xrunjdwp:transport=dt_socket,address=5004,server=y,suspend=y
) else if [%1]==[-d] (
  set DEBUG=-Xrunjdwp:transport=dt_socket,address=5004,server=y,suspend=n
) else (
  set DEBUG=
)

set LOGCONF=-Dlog4j.configuration=enginedebug.properties

java -cp "%~dp0..\lib\*" %DEBUG% %LOGCONF% com.connexience.server.workflow.cloud.cmd.RebuildCoreLibrary "%~dp0..\lib"
