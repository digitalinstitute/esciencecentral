/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.test.workflow;

import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("unchecked")
public class FileWrapperTest
{

	@Test
	public void testBasicFileWrapper() throws Exception
	{
        File tempFile = File.createTempFile("test", "csv");
        FileWriter fw = new FileWriter(tempFile);
        fw.write("1,2,3\n");
        fw.close();

        FileWrapper wrapper = new FileWrapper();
        wrapper.addFile(tempFile);

        assertEquals(1, wrapper.getFileCount());

        FileWrapper wrapper2 = new FileWrapper();
        wrapper2.addFile(wrapper.getFile(0));
        assertEquals(wrapper.getBaseDir(), wrapper2.getBaseDir());
        assertEquals(wrapper.getFilePath(0), wrapper2.getFilePath(0));


	}
}
