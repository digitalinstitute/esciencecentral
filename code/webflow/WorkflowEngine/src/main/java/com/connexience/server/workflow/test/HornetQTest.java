package com.connexience.server.workflow.test;

import java.util.HashMap;
import java.util.Map;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.Topic;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.jms.HornetQJMSClient;
import org.hornetq.api.jms.JMSFactoryType;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;

/**
 * Test non-naming connection to HornetQ server
 * @author hugo
 */
public class HornetQTest implements Runnable {

    public HornetQTest() {
        try {

            Topic topic = HornetQJMSClient.createTopic("WorkflowControl");
            Map connectionParameters = new HashMap();
            connectionParameters.put(TransportConstants.HOST_PROP_NAME, "localhost");
            connectionParameters.put(TransportConstants.PORT_PROP_NAME, "5445");

            TransportConfiguration config = new TransportConfiguration(NettyConnectorFactory.class.getName(), connectionParameters);
            
            ConnectionFactory cf = HornetQJMSClient.createConnectionFactoryWithoutHA(JMSFactoryType.CF, config);
            Connection c = cf.createConnection("connexience", "1234");
            c.start();
            
            Session session = c.createSession();
            Destination dest = session.createTopic("WorkflowControl");
            MessageConsumer consumer = session.createConsumer(dest);
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    System.out.println(message);
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }        
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(100);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    
    
    public static void main(String[] args){
        HornetQTest q = new HornetQTest();
        new Thread(q).start();
    }
}
