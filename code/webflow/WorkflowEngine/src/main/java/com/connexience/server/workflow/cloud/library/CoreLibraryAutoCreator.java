/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.cloud.library;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.document.DocumentVersion;
import com.connexience.server.workflow.util.ZipUtils;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;

/**
 * This class automatically populates the service library with a core library
 * derived from the classpath of the server.
 * @author hugo
 */
public class CoreLibraryAutoCreator {
    private static final Logger logger = Logger.getLogger(CoreLibraryAutoCreator.class);
    
    private ServiceLibrary library;

    public CoreLibraryAutoCreator(ServiceLibrary library) {
        this.library = library;
    }
    
    public CloudWorkflowServiceLibraryItem prepare() throws Exception {
        logger.debug("Automatically preparing core library");
        
        // Create an empty directory to hold the library
        File workingDir = new File(library.getLibraryDirectory(), "autocore");
        if(!workingDir.exists()){
            logger.debug("Creating auto core library folder: " + workingDir.getPath());
            workingDir.mkdir();
        }
        
        // Copy the library.xml file
        File libraryXml = new File(workingDir, "library.xml");
        ZipUtils.copyStreamToFile(getClass().getResourceAsStream("/corelibrary/library.xml"), libraryXml);
        logger.debug("Copied library.xml to: " + libraryXml.getPath());
        
        File dependenciesXml = new File(workingDir, "dependencies.xml");
        ZipUtils.copyStreamToFile(getClass().getResourceAsStream("/corelibrary/dependencies.xml"), dependenciesXml);        
        logger.debug("Copied dependencies.xml to: " + dependenciesXml.getPath());
        
        // Lib directory
        File libDir = new File(workingDir, "lib");
        if(!libDir.exists()){
            libDir.mkdir();
            logger.debug("Created lib folder: " + libDir.getPath());
        }
        
        // Find the correct files from the classpath
        String classpath = System.getProperty("java.class.path");
        StringTokenizer tokens = new StringTokenizer(classpath, File.pathSeparator);
        
        String token;
        while(tokens.hasMoreTokens()){
            token = tokens.nextToken();
            if(token.contains("server-common") || token.contains("log4j") || token.contains("workflow-engine") || token.contains("server-utils")){
                logger.debug("Adding file: " + token + " to required jars");
                File jarFile = new File(token);
                File targetFile = new File(libDir, jarFile.getName());
                if(jarFile.exists()){
                    ZipUtils.copyFile(jarFile, targetFile);
                    logger.debug("Copied to: " + targetFile);
                }
            }
        }
        
        
        
        DocumentRecord zipRecord = new DocumentRecord();
        zipRecord.setName("DUMMYDOCUMENT");
        zipRecord.setId("auto");
        
        DocumentVersion zipVersion = new DocumentVersion();
        zipVersion.setDocumentRecordId(zipRecord.getId());
        zipVersion.setVersionNumber(0);
        zipVersion.setId("core");
        
        CloudWorkflowServiceLibraryItem item = new CloudWorkflowServiceLibraryItem(library, zipRecord, zipVersion);
        item.setupFromUnpackedDir(workingDir, new LibraryPreparationReport());
        
        return item;
        
    }
}