package com.connexience.server.workflow.engine.datatypes;

import org.pipeline.core.drawing.DataType;

/**
 * This class describes the data type for a standard set of Data that
 * gets passed between blocks
 * @author hugo
 */
public class DataWrapperDataType extends DataType {
    
    /** Creates a new instance of DataWrapperDataType */
    public DataWrapperDataType() {
        super("data-wrapper", "Standard set of data", DataWrapper.class);
    }
}
