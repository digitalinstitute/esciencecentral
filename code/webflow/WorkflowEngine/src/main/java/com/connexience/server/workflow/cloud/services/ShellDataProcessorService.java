/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.cloud.services;

import com.connexience.server.workflow.cloud.library.CloudWorkflowServiceLibraryItem;
import com.connexience.server.workflow.cloud.library.LibraryWrapper;
import com.connexience.server.workflow.cloud.library.installer.Installer;
import com.connexience.server.workflow.cloud.library.types.BinaryLibrary;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.engine.datatypes.DataWrapper;
import com.connexience.server.workflow.service.DataProcessorCallMessage;
import com.connexience.server.workflow.service.DataProcessorException;
import com.connexience.server.workflow.service.DataProcessorIODefinition;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.data.io.*;
import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.TransferData;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.xmldatatypes.*;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;


public class ShellDataProcessorService extends CloudDataProcessorService
{
    private PrintWriter  _stdIn;
    private DumperThread _stdOut;
    private DumperThread _stdErr;

    private ShellWrapper _shellWrapper;
    private Process _shellProcess;

    private static final String _shellLibraryName = "shell-bin";


    private interface ShellWrapper
    {
        Process startProcess() throws Exception;
        void assignProperties() throws Exception;
        void assignNonStreamingData() throws Exception;
        void defineFunctions() throws Exception;
        void startMainScript();
        void processOutputs() throws Exception;
    }


    @Override
    public void executionAboutToStart() throws Exception {
        // Pick a suitable shell wrapper for the host OS.
        String osName = Installer.getOsName();
        // switch does not like nulls
        if (osName == null) {
            osName = "unknown";
        }
        switch (osName) {
            case "windows": {
                Path scriptsPath = getLibraryItem().getOriginalUnpackedDir().toPath().resolve("scripts");
                // Pick the right wrapper: either Batch or PowerShell
                if (Files.exists(scriptsPath.resolve("main.ps1"))) {
                    _shellWrapper = new PowerShellWrapper();
                } else if (Files.exists(scriptsPath.resolve("main.cmd"))) {
                    _shellWrapper = new WindowsBatchWrapper();
                } else {
                    throw new Exception("Missing the PowerShell or Windows batch main script!");
                }
                break;
            }

            case "linux":
            case "macos":
                _shellWrapper = new BashWrapper();
                break;

            default:
                throw new Exception("Unsupported OS type: " + osName);
        }

        // Start the shell process, so we can set various properties and functions for the user shell script
        _shellProcess = _shellWrapper.startProcess();

        // Make sure that the process is terminated when the JVM is destroyed.
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                _shellProcess.destroy();
            }
        });

        // Attach the standard input, output, and error streams.
        _stdIn = new PrintWriter(_shellProcess.getOutputStream());
        _stdOut = new DumperThread(_shellProcess.getInputStream(), System.out, false);
        _stdErr = new DumperThread(_shellProcess.getErrorStream(), System.err, false);

        // Start the dumper threads, so the output and error are passed to this processor service System.out and System.err,
        // respectively.
        _stdOut.start();
        _stdErr.start();

        // Work with the shell process to assign things which cannot be easily passed as environment variables
        // (e.g. arrays, file-wrapper inputs).
        _shellWrapper.assignProperties();
        _shellWrapper.assignNonStreamingData();
        _shellWrapper.defineFunctions();
    }


    @Override
    public void execute() throws Exception
    {
        _shellWrapper.startMainScript();

        // Wait for the main script to terminate
        int exitCode = _shellProcess.waitFor();

        // Wait for the dumper threads to process all the data
        _stdOut.join();
        _stdErr.join();

        if (exitCode != 0) {
            throw new Exception("Shell script terminated with non-zero exit status: " + exitCode);
        }
    }

    @Override
    public void allDataProcessed() throws Exception
    {
        _shellWrapper.processOutputs();
    }


    /**
     * This method generates a name that is acceptable by Windows Batch, PowerShell and Linux Bash as an environment
     * variable name.
     *
     * @param name A property, input or output name which will be converted into an environment variable name.
     * @return A valid environment variable name.
     */
    private static String _toSimpleVariableName(String name)
    {
        StringBuilder sb = new StringBuilder();
        boolean illegal = false;

        for (char c : name.toCharArray()) {
            if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '_') {
                sb.append(c);
                illegal = false;
            } else if (c >= '0' && c <= '9') {
                if (sb.length() == 0) {
                    sb.append('_');
                }
                sb.append(c);
                illegal = false;
            } else {
                if (illegal) {
                    // Produce only a single underscore for more than one consecutive illegal character
                    continue;
                }
                sb.append('_');
                illegal = true;
            }
        }

        return sb.toString();
    }


    private void _marshalPropertiesOutput(File propFile, String outputName)
    throws IOException, XmlStorageException, DataProcessorException
    {
        Properties p = new Properties();
        try (Reader reader = Files.newBufferedReader(propFile.toPath(), StandardCharsets.UTF_8)) {
            p.load(reader);
        }
        System.out.println(String.format("Output %s imported from file %s: %d %s loaded", outputName, propFile.toString(), p.size(), p.size() == 1 ? "property" : "properties"));

        PropertiesWrapper wrapper = new PropertiesWrapper();
        // This is a very simple property marshaler.
        // TODO: improve it by adding some type specification to output properties; e.g. 'propertyname[type] = value' could be an option.
        for (Map.Entry<Object, Object> entry : p.entrySet()) {
            wrapper.properties().add(entry.getKey().toString(), entry.getValue());
        }

        setOutputData(outputName, wrapper);
    }


    private void _marshalCSVOutput(File csvFile, String outputName)
    throws IOException, DataImportException, DataException, XmlStorageException, DataProcessorException
    {
        DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();
        importer.setChunkSize(5000);
        importer.setLimitRows(false);
        importer.setDataStartRow(1);
        importer.setForceTextImport(true);

        try (FileInputStream inStream = new FileInputStream(csvFile)) {
            importer.resetWithInputStream(inStream);

            int chunkCount = 0;
            int rowCount = 0;
            String dataName = csvFile.getName();

            while (!importer.isFinished()) {
                Data data = importer.importNextChunk();
                data.createEmptyProperties();
                data.setName(dataName);

                setOutputDataSet(outputName, data);

                chunkCount++;
                rowCount += data.getLargestRows();
            }

            System.out.println(String.format("Output %s imported from file %s: %d row(s) of data in %d chunk(s)", outputName, dataName, rowCount, chunkCount));
        } finally {
            importer.terminateRead();
        }
    }



    static String _wrapInQuotes(String s)
    {
        StringBuilder output = new StringBuilder();
        int len = s.length();
        boolean escaping = false;

        if (len > 0 && s.charAt(0) == '\'') {
            escaping = true;
        } else {
            output.append('\'');
        }
        for (int i = 0; i < len; i++) {
            if (s.charAt(i) == '\'') {
                if (escaping) {
                    output.append("\\'");
                } else {
                    output.append("'\\'");
                    escaping = true;
                }
            } else if (escaping) {
                output.append('\'');
                output.append(s.charAt(i));
                escaping = false;
            } else {
                output.append(s.charAt(i));
            }
        }

        if (!escaping) {
            output.append('\'');
        }

        return output.toString();
    }


    // Inner helper class
    abstract class GenericWrapper implements ShellWrapper
    {
        private final String[] _shellCommandWithArgs;

        HashMap<String, XmlStorableDataObject> _propsToAssign;
        HashMap<String, FileWrapper> _inputsToAssign;
        HashMap<String, File> _outputsToProcess;


        GenericWrapper(String... shellCommandWithArgs)
        {
            _shellCommandWithArgs = shellCommandWithArgs;
            _propsToAssign = new HashMap<>(1);
            _inputsToAssign = new HashMap<>(1);
            _outputsToProcess = new HashMap<>(1);
        }


        public Process startProcess() throws Exception
        {
            ProcessBuilder pb = new ProcessBuilder();

            Map<String, String> env = pb.environment();
            _prepareProperties(env);
            _prepareDependencies(env);
            _prepareInputs(env);
            _prepareOutputs(env);

            pb.command(_shellCommandWithArgs);

            return pb.start();
        }


        public void assignProperties() throws Exception
        {
            for (Map.Entry<String, XmlStorableDataObject> e : _propsToAssign.entrySet()) {
                if (e.getValue().getValue() instanceof StringListWrapper) {
                    StringListWrapper slw = (StringListWrapper)e.getValue().getValue();
                    StringBuilder sb = new StringBuilder();
                    sb.append(e.getKey());
                    sb.append("=(");
                    for (String s : slw) {
                        sb.append(_wrapInQuotes(s));
                        sb.append(' ');
                    }
                    sb.append(")");
                    _stdIn.println(sb.toString());
                } else if (e.getValue().getValue() instanceof StringPairListWrapper) {
                    StringPairListWrapper splw = (StringPairListWrapper)e.getValue().getValue();
                    StringBuilder sb = new StringBuilder();
                    sb.append(e.getKey());
                    sb.append("=(");
                    for (String[] sp : splw) {
                        sb.append(_wrapInQuotes(sp[0]));
                        sb.append(' ');
                        sb.append(_wrapInQuotes(sp[1]));
                        sb.append(' ');
                    }
                    sb.append(")");
                    _stdIn.println(sb.toString());
                } else {
                    throw new Exception("Unsupported type of the property value: " + e.getValue().getValue().getClass());
                }
            }
        }


        public void processOutputs() throws Exception {
            DataProcessorCallMessage message = getCallMessage();
            Map.Entry<String, File> currentEntry = null;

            try {
                for (Map.Entry<String, File> entry : _outputsToProcess.entrySet()) {
                    currentEntry = entry;
                    String type = message.getDataOutputType(entry.getKey());
                    switch (type) {
                        case "file-wrapper":
                            // The output file should include a list of files which can directly be fed into the file-wrapper output
                            FileWrapper wrapper = new FileWrapper();
                            try (InputStream inStream = Files.newInputStream(entry.getValue().toPath())) {
                                wrapper.loadFromInputStream(inStream);
                            }

                            setOutputData(entry.getKey(), wrapper);
                            break;
                        case "data-wrapper":
                            // The output file should be a csv which needs to be parsed into a Data object
                            _marshalCSVOutput(entry.getValue(), entry.getKey());
                            break;
                        case "properties-wrapper":
                            // The output file should be a java properties file which needs to be parsed into a PropertiesWrapper object
                            _marshalPropertiesOutput(entry.getValue(), entry.getKey());
                            break;
                        default:
                            throw new UnsupportedOperationException("CmdLineWrapper cannot handle output-type: " + type);
                    }
                }
            } catch (Exception x) {
                if (currentEntry != null) {
                    throw new Exception("Cannot produce output via port " + currentEntry.getKey() + ": " + x);
                }
            }
        }


        public void _prepareProperties(Map<String, String> processEnv) throws XmlStorageException
        {
            XmlDataStore properties = getCallMessage().getProperties();

            for (Object name : properties.getNames()) {
                String propName = "PROP__" + _toSimpleVariableName(name.toString());
                XmlDataObject propValue = properties.get(name.toString());
                String valueString;

                if (propValue instanceof XmlStringDataObject) {
                    valueString = ((XmlStringDataObject) propValue).stringValue();
                } else if (propValue instanceof XmlDoubleDataObject) {
                    valueString = Double.toString(((XmlDoubleDataObject) propValue).doubleValue());
                } else if (propValue instanceof XmlIntegerDataObject) {
                    valueString = Integer.toString(((XmlIntegerDataObject) propValue).intValue());
                } else if (propValue instanceof XmlLongDataObject) {
                    valueString = Long.toString(((XmlLongDataObject) propValue).longValue());
                } else if (propValue instanceof XmlBooleanDataObject) {
                    if (((XmlBooleanDataObject) propValue).booleanValue()) {
                        valueString = Boolean.TRUE.toString();
                    } else {
                        valueString = "";
                    }
                } else if (propValue instanceof XmlStorableDataObject &&
                        (propValue.getValue() instanceof StringListWrapper || propValue.getValue() instanceof StringPairListWrapper)) {
                    // String list properties can be set once the wrapper process is started (using assignProperties)
                    _propsToAssign.put(propName, (XmlStorableDataObject)propValue);
                    continue;
                } else {
                    // Other non-simple data types such as DocumentRecord, Folder, etc. are not currently supported.
                    throw new UnsupportedOperationException("Property conversion for type: " + propValue.getClass() + " has not been implemented.");
                }

                processEnv.put(propName, valueString);
            }
        }


        /**
         * This method exposes commands from all dependencies of this service which ar BinaryLibraries.
         *
         * @param processEnvironment
         */
        private void _prepareDependencies(Map<String, String> processEnvironment)
        {
            processEnvironment.put("BLOCK_HOME", getLibraryItem().getOriginalUnpackedDir().toString());

            Iterator<CloudWorkflowServiceLibraryItem> iter = getLibraryItem().resolvedDependencies();
            while (iter.hasNext()) {
                String depPrefix = null; // Prefix used in the variable name. Initialized on the first command of
                // the binary library to avoid unnecessary calls for other types of library.
                CloudWorkflowServiceLibraryItem depLib = iter.next();
                LibraryWrapper wrapper = depLib.getWrapper();
                if (wrapper instanceof BinaryLibrary) {
                    BinaryLibrary binLib = (BinaryLibrary)wrapper;
                    for (String execName : binLib.getExecutableNames()) {
                        BinaryLibrary.Executable execCmd = binLib.getExecutable(execName);
                        File execPath;
                        if (execCmd.isAbsolute()) {
                            execPath = new File(execCmd.getRelativeCmd());
                        } else {
                            execPath = binLib.getFile(execCmd.getRelativeCmd());
                        }
                        if (depPrefix == null) {
                            depPrefix = "DEP__" + _toSimpleVariableName(depLib.getLibraryName()) + "__";
                        }
                        processEnvironment.put(depPrefix + execName, execPath.toString());
                    }
                }
            }
        }

        private void _prepareInputs(Map<String, String> processEnv)
                throws IOException, DataExportException, DataProcessorException, XmlStorageException
        {
            String[] inputNames = getCallMessage().getDataSources();
            String[] inputModes = getCallMessage().getDataSourceModes();
            TransferData inputDataObject;

            for (int i = 0; i < inputNames.length; i++) {
                // Skip optional and unconnected inputs
                if (!isInputConnected(inputNames[i])) {
                    continue;
                }

                String propInputName = "INPUT__" + _toSimpleVariableName(inputNames[i]);
                inputDataObject = getInputData(inputNames[i]);

                // Check input mode
                if (inputModes[i].equals(DataProcessorIODefinition.STREAMING_CONNECTION)) {
                    throw new DataProcessorException("ShellDataProcessorService cannot handle streaming connections; check input: " + inputNames[i]);
                }

                if (inputDataObject instanceof DataWrapper) {
                    // TODO: This can potentially be optimized because the inputData is unnecessarily read from the source and stored here
                    File tmpFile = createTempFile("tmp-" + inputNames[i], new File("."));
                    CSVDataExporter exporter = null;
                    try {
                        exporter = new CSVDataExporter();
                        exporter.openFile(tmpFile);
                        exporter.appendData(getInputDataSet(inputNames[i]));
                    } finally {
                        if (exporter != null) {
                            exporter.closeFile();
                        }
                    }
                    processEnv.put(propInputName, tmpFile.toString());
                } else if (inputDataObject instanceof FileWrapper) {
                    FileWrapper f = (FileWrapper) inputDataObject;
                    if (f.getFileCount() > 0) {
                        processEnv.put(propInputName, f.getRelativeFilePath(0));
                        // FileWrapper array variables will be assigned after the process is started
                        _inputsToAssign.put(propInputName, (FileWrapper) inputDataObject);
                    }
                } else if (inputDataObject instanceof PropertiesWrapper) {
                    // Do nothing -- properties should be overridden by the DataProcessorService
                } else {
                    throw new DataProcessorException("Unsupported input type: " + inputDataObject.getClass());
                }
            }
        }


        private void _prepareOutputs(Map<String, String> processEnv)
        throws IOException, DrawingException, DataProcessorException
        {
            DataProcessorCallMessage message = getCallMessage();
            String[] outputNames = message.getDataOutputs();

            for (String outputName : outputNames) {
                String outputType = message.getDataOutputType(outputName);
                if ("control-dependency".equals(outputType)) {
                    // Do nothing, control dependencies are managed by the engine.
                    continue;
                }

                File outfile = createTempFile("output.tmp", new File("."));
                if ("file-wrapper".equals(outputType)) {
                    // For file-wrapper outputs prepare the output file with the base directory.
                    // TODO: could be improved with some minor changes to the FileWrapper class
                    FileWrapper wrapper = new FileWrapper();
                    try (OutputStream outStream = Files.newOutputStream(outfile.toPath())) {
                        wrapper.saveToOutputStream(outStream);
                    }
                } else if ("data-wrapper".equals(outputType) || "properties-wrapper".equals(outputType)) {
                    // Do nothing, the temp file should be enough.
                } else {
                    throw new DataProcessorException("Unsupported output type: " + outputType);
                }
                processEnv.put("OUTPUT__" + _toSimpleVariableName(outputName), outfile.toString());
                _outputsToProcess.put(outputName, outfile);
            }
        }
    }


    private class BashWrapper extends GenericWrapper
    {
        public BashWrapper()
        {
            super("bash");
        }

        @Override
        public void startMainScript()
        {
           Path mainScript = getLibraryItem().getOriginalUnpackedDir().toPath().resolve("scripts").resolve("main.sh");
            _stdIn.println("source " + mainScript);
            // Terminate the script
            _stdIn.println();
            _stdIn.println("exit $?");
            _stdIn.flush();
        }

        @Override
        public void defineFunctions() throws IOException
        {
            CloudWorkflowServiceLibraryItem dependencyLib = getDependencyItem(_shellLibraryName);
            if (dependencyLib == null) {
                System.out.println("Running in the backward compatibility mode. Please update your block by regenerating the maven archetype.");
                // The old way: import functions and definitions from the shell-init.sh script embedded in the engine resources.
                ClassLoader cl = getClass().getClassLoader();
                try (InputStreamReader r = new InputStreamReader(cl.getResourceAsStream("com/connexience/server/workflow/cloud/services/shell-init.sh"), StandardCharsets.UTF_8)) {
                    char[] buffer = new char[4096];
                    int n;
                    while ((n = r.read(buffer)) > 0) {
                        _stdIn.write(buffer, 0, n);
                    }
                    _stdIn.flush();
                }
            } else {
                // The new way: import functions and definitions from the shell-bin library.
                LibraryWrapper shellLibrary = dependencyLib.getWrapper();
                _stdIn.println("source " + shellLibrary.getFile("shell-init.sh").getAbsolutePath());
                _stdIn.flush();
            }
        }


        public void assignNonStreamingData() throws DataProcessorException
        {
            Path workingDirectory = getWorkingDirectory().toPath().toAbsolutePath();

            // Make sure that the return files are relative to the current working directory
            for (Map.Entry<String, FileWrapper> e : _inputsToAssign.entrySet()) {
                Path relativeBaseDir = workingDirectory.relativize(e.getValue().getBaseDir().toPath().toAbsolutePath());
                StringBuilder sb = new StringBuilder();
                sb.append(e.getKey());
                sb.append("__LIST=(");
                for (File f : e.getValue().relativeFiles()) {
                    sb.append(_wrapInQuotes(relativeBaseDir.resolve(f.getPath()).toString()));
                    sb.append(' ');
                }
                sb.append(")");
                _stdIn.println(sb.toString());
            }
        }
    }


    private class WindowsBatchWrapper extends GenericWrapper
    {
        public WindowsBatchWrapper()
        {
            super("cmd.exe", "/Q", "/V:ON");
        }

        @Override
        public void startMainScript() {
            Path mainScript = getLibraryItem().getOriginalUnpackedDir().toPath().resolve("scripts").resolve("main.cmd");
            _stdIn.println(mainScript);
            _stdIn.println();
            _stdIn.println("exit");
            _stdIn.flush();
        }

        @Override
        public void defineFunctions() throws IOException
        {
            CloudWorkflowServiceLibraryItem dependencyLib = getDependencyItem(_shellLibraryName);
            if (dependencyLib == null) {
                throw new RuntimeException("Cannot run the ShellWrapper block. Missing library: " + _shellLibraryName);
            }
            LibraryWrapper shellLibrary = dependencyLib.getWrapper();

            // Windows Batch does not support functions. However, we can set environmental variables to point to
            // the scripts included in the shell library.
            _stdIn.println("SET FUNC__MkTempName=" + shellLibrary.getFile("mktempname.cmd").getAbsolutePath());
            _stdIn.flush();
        }

        public void assignNonStreamingData() throws DataProcessorException
        {
            Path workingDirectory = getWorkingDirectory().toPath().toAbsolutePath();

            // Make sure that the return files are relative to the current working directory
            for (Map.Entry<String, FileWrapper> e : _inputsToAssign.entrySet()) {
                Path relativeBaseDir = workingDirectory.relativize(e.getValue().getBaseDir().toPath().toAbsolutePath());
                // Set the list number variable
                StringBuilder sb = new StringBuilder();
                sb.append("SET ");
                sb.append(e.getKey());
                int prefixLength = sb.length(); // Store index for "SET <VARIABLE_NAME>"
                sb.append("[#]=");
                sb.append(e.getValue().getFileCount());
                _stdIn.println(sb.toString());

                // Set list index variables for each element on the file-wrapper list
                int i = 1;
                for (File f : e.getValue().relativeFiles()) {
                    sb.setLength(prefixLength);
                    sb.append("[");
                    sb.append(i++);
                    sb.append("]=");
                    sb.append(relativeBaseDir.resolve(f.getPath()).toString());
                    _stdIn.println(sb.toString());
                }
            }
        }
    }


    private static class PowerShellWrapper implements ShellWrapper
    {
        public Process startProcess() throws IOException {
            throw new UnsupportedOperationException("The PowerShellWrapper has not yet been implemented.");
        }

        public void assignProperties() {}

        public void assignNonStreamingData() {}

        public void defineFunctions() {}

        public void startMainScript() {}

        public void processOutputs() {}
    }


    private static class DumperThread extends Thread
    {
        InputStream inStream;
        File outFile;
        OutputStream outStream;
        Object lockObject;
        boolean closeOutput;


        public DumperThread(InputStream inputStream, File outputFile)
        {
            this.inStream = inputStream;
            this.outFile = outputFile;
            this.closeOutput = true;
        }

        public DumperThread(InputStream inputStream, OutputStream outputStream, boolean closeOutput)
        {
            this.inStream = inputStream;
            this.outStream = outputStream;
            this.closeOutput = closeOutput;
        }

        public DumperThread(InputStream inputStream, OutputStream outputStream, Object outputLockObject)
        {
            this.inStream = inputStream;
            this.outStream = outputStream;
            this.lockObject = outputLockObject;
            // This is implicit because if a lock object is present it means that
            // the output stream is shared by some other thread.
            this.closeOutput = false;
        }


        @Override
        public void run()
        {
            int n;
            byte[] buffer = new byte[16384];

            BufferedOutputStream out = null;

            try {
                if (outFile != null) {
                    out = new BufferedOutputStream(new FileOutputStream(outFile));
                } else if (outStream != null) {
                    out = new BufferedOutputStream(outStream);
                }

                if (lockObject != null) {
                    while ((n = inStream.read(buffer)) > 0) {
                        synchronized (lockObject) {
                            out.write(buffer, 0, n);
                            out.flush();
                        }
                    }
                } else {
                    while ((n = inStream.read(buffer)) > 0) {
                        out.write(buffer, 0, n);
                        out.flush();
                    }
                }
            } catch (IOException x) {
                System.out.println("IOException in the dumper thread: " + x);
            } finally {
                // Do not close the output stream if closeOutput is false
                // or lockObject is used which mean that the stream is shared
                // with some other thread
                if (out != null && closeOutput && lockObject == null) {
                    try {
                        out.close();
                    } catch (IOException x) {
                        System.out.println("Problems when closing the output file: " + x);
                    }
                }
            }
        }
    }
}
