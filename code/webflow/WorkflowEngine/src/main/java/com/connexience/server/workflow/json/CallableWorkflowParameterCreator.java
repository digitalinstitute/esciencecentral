/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.workflow.json;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.workflow.blocks.processor.DataProcessorBlock;
import java.util.Enumeration;
import java.util.Iterator;
import org.json.JSONObject;
import org.pipeline.core.drawing.BlockModel;
import org.pipeline.core.drawing.model.DefaultDrawingModel;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlStorableDataObject;

/**
 * This class creates a set of workflow
 * @author hugo
 */
public class CallableWorkflowParameterCreator {
    /** Workflow to parse */
    private DefaultDrawingModel drawing;
    
    /** JSON version of the properties */
    private JSONObject parametersJson;

    public CallableWorkflowParameterCreator(DefaultDrawingModel drawing, String parametersJson) throws Exception {
        this.drawing = drawing;
        this.parametersJson = new JSONObject(parametersJson);
    }
    
    public WorkflowParameterList createParametersList() throws ConnexienceException {
        WorkflowParameterList params = new WorkflowParameterList();

        Enumeration blocks = drawing.blocks();
        BlockModel block;
        DataProcessorBlock processor;
        Enumeration properties;
        XmlDataObject property;

        while(blocks.hasMoreElements()){
            block = (BlockModel)blocks.nextElement();
            if(block instanceof DataProcessorBlock){
                processor = (DataProcessorBlock)block;
                properties = processor.getEditableProperties().elements();
                while(properties.hasMoreElements()){
                    property = (XmlDataObject)properties.nextElement();
                    if(property.isExposedProperty()){
                        if(property instanceof XmlStorableDataObject){
                            // Complex property
                            if(parametersJson.has(property.getExposedName())){
                                try {
                                    params.add(block.getName(), property.getName(), parametersJson.get(property.getExposedName()).toString());
                                } catch (Exception e){
                                    throw new ConnexienceException("Error setting parameter: " + property.getExposedName() + ": " + e.getMessage(), e);
                                }
                            } else {
                                // If the parameter is missing, the default value (set in the workflow designer) will be used.
                            }

                        } else {
                            if(parametersJson.has(property.getExposedName())){
                                try {
                                    params.add(block.getName(), property.getName(), parametersJson.get(property.getExposedName()).toString());
                                } catch (Exception e){
                                    throw new ConnexienceException("Error setting parameter: " + property.getExposedName() + ": " + e.getMessage(), e);
                                }                                
                            } else {
                                // If the parameter is missing, the default value (set in the workflow designer) will be used.
                            }
                        }
                    }
                }
            }
        }        
        
        return params;
    }
}