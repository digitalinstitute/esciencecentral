
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */


import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Resample;

/**
 * E-Sc Weka Block. Randomly resamples (with replacement) instances
 * from the input Weka ARFF file and writes these out as a new ARFF file.
 *
 * @author Dominic Searson 3/14
 */
public class ResampleARFFBlock extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {

        
        //get user block parameters
        double samplePercentSize = getEditableProperties().doubleValue("Sample percent size", 100); //resample size as % of original
        
        if (samplePercentSize <= 0) {
            throw new IllegalArgumentException("Sample percent size must be >= 0 ");
        }
        
        
        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");

        //load ARFF and convert to instances
        if (arffWrapper.getFileCount() == 1) {
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() - 1);
            
            //create and apply resampling filter
            Resample rsFilter = new Resample();
            rsFilter.setNoReplacement(false);
            rsFilter.setSampleSizePercent(samplePercentSize);  //subsample set size (% of original)
            rsFilter.setInputFormat(wekaInstances);
            Instances filteredInstances = Filter.useFilter(wekaInstances, rsFilter);
            filteredInstances.setRelationName(wekaInstances.relationName());

            //now write arff out in a filewrapper
            String fileName = "resampledAttributes.arff";
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filteredInstances);
            File outFile = new File(getWorkingDirectory(), fileName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);


        } else {
            throw new WekaException("ResampleARFF block can only parse a single ARFF file");
        }

    }

@Override
        public void allDataProcessed() throws Exception {
    }
}

