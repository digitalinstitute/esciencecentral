
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import java.io.PrintWriter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.data.maths.MeanValueCalculator;

public class RegressionStats extends CloudDataProcessorService {
    private double meanActual = 0;
    private double meanPredicted = 0;
    
    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Data inputData = getInputDataSet("model-results");

        // Find out which columns contain the actual and predicted values and extract them from the input data
        ColumnPicker actualPicker = new ColumnPicker(getEditableProperties().stringValue("ActualColumn", "#0"));
        ColumnPicker predictedPicker = new ColumnPicker(getEditableProperties().stringValue("PredictionColumn", "#1"));

        DoubleColumn actual = (DoubleColumn) actualPicker.pickColumn(inputData);
        DoubleColumn predicted = (DoubleColumn) predictedPicker.pickColumn(inputData);
        initialiseDataAttributes(actual, predicted);
        
        JSONObject results = new JSONObject();
        results.put("corrCoef", corrCoef(actual, predicted));
        results.put("meanAbsoluteError", mae(actual, predicted));
        results.put("maxAbsoluteError", maxAbsError(actual, predicted));
        results.put("qSquared", qSquared(actual, predicted));
        results.put("rmsError", rmsError(actual, predicted));
        results.put("rSquared", rSquared(actual, predicted));
        
        File resultsFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("JSONFileName", "results.json"));
        PrintWriter writer = new PrintWriter(resultsFile);
        results.write(writer);
        writer.flush();
        writer.close();
        FileWrapper wrapper = new FileWrapper(getWorkingDirectory());
        wrapper.addFile(resultsFile, false);
        
        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputData("report-json", wrapper);
        
        // Set metadata
        if(getEditableProperties().booleanValue("AttachAsMetadata", true)){
            String category = getEditableProperties().stringValue("MetadataCategory", "MLResults");
            WorkflowInvocationFolder invocation = createApiLink().getWorkflowInvocation(getCallMessage().getInvocationId());
            MetadataCollection c = new MetadataCollection();

            JSONArray names = results.names();
            String name;
            double value;
            
            for(int i=0;i<names.length();i++){
                name = names.getString(i);
                value = results.getDouble(name);
                c.add(new NumericalMetadata(category, name, value));
            }
            createApiLink().uploadMetadata(invocation, c);
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
    
    /**
     * Calculate correlation coefficient r between the actual values and the
     * predicted values.
     *
     */
    private double corrCoef(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {
        double actual, predicted, r;

        /**
         * (actual value - actuals means) * (predicted value - predicted mean)
         */
        double numeratorTerm;

        /**
         * The running sum of numeratorTerm
         */
        double runsumNumeratorTerm = 0;

        /**
         * (actual - actuals mean)^2
         *
         */
        double denominatorTermActuals;

        /**
         * (predicted -predicted mean)^2
         */
        double denominatorTermPredicted;
        int count = 0;

        double runsum_denominatorTermActuals = 0;
        double runsum_denominatorTermPredicted = 0;
        double actualMinusactualsMean, predictedMinusPredictedMean;

        /**
         * Loop through training vals *
         */
        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);

                actualMinusactualsMean = actual - meanActual;
                predictedMinusPredictedMean = predicted - meanPredicted;

                numeratorTerm = actualMinusactualsMean * predictedMinusPredictedMean;
                runsumNumeratorTerm = runsumNumeratorTerm + numeratorTerm;

                denominatorTermActuals = Math.pow(actualMinusactualsMean, 2);
                denominatorTermPredicted = Math.pow(predictedMinusPredictedMean, 2);

                runsum_denominatorTermActuals = runsum_denominatorTermActuals + denominatorTermActuals;
                runsum_denominatorTermPredicted = runsum_denominatorTermPredicted + denominatorTermPredicted;
                count++;
            }
        }

        if (count > 0) {
            double corrCoeffTrain = runsumNumeratorTerm / Math.sqrt(runsum_denominatorTermActuals * runsum_denominatorTermPredicted);
            if (Double.isNaN(corrCoeffTrain) || Double.isInfinite(corrCoeffTrain)) {
                corrCoeffTrain = 0;
            }
            return corrCoeffTrain;
        } else {
            throw new Exception("No suitable data");
        }

    }    
    
    /**
     * Calculate the means of the actual data and the predicted data.
     *
     */
    private void initialiseDataAttributes(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {

        MeanValueCalculator c1 = new MeanValueCalculator(actualColumn);
        this.meanActual = c1.doubleValue();
        MeanValueCalculator c2 = new MeanValueCalculator(predictedColumn);
        this.meanPredicted = c2.doubleValue();
    }    
    
    /**
     * Calculate mean absolute error (MAE) of the actual values and the
     * predicted values.
     */
    private double mae(DoubleColumn actual, DoubleColumn predicted) throws Exception {
        DoubleColumn errCol = new DoubleColumn();
        for (int i = 0; i < actual.getRows(); i++) {
            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                errCol.appendDoubleValue(Math.abs(predicted.getDoubleValue(i) - actual.getDoubleValue(i)));
            }
        }
        return new MeanValueCalculator(errCol).doubleValue();
    }
    
    /**
     * Calculate the q-squared value
     */
    private double qSquared(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {
        double runsumActuals = 0, ssTotal = 0, ssErr = 0;
        double actual, predicted;
        int count = 0;

        double mv = meanActual;

        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);
                runsumActuals = runsumActuals + actual;

                //note the mean of the training data is used here. See Tropsha's papers.
                ssTotal = ssTotal + Math.pow((actual - mv), 2);
                ssErr = ssErr + Math.pow((actual - predicted), 2);
                count++;
            }

        }
        if (count > 0) {
            return 1 - (ssErr / ssTotal);
        } else {
            throw new Exception("No suitable data");
        }

    }    
    
    /**
     * Calculate the RMS (root mean squared) prediction error
     */
    private double rmsError(DoubleColumn actual, DoubleColumn predicted) throws Exception {
        double runSumPredictedSqErr = 0;
        int count = 0;
        for (int i = 0; i < actual.getRows(); i++) {
            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                count++;
                runSumPredictedSqErr = runSumPredictedSqErr + Math.pow((predicted.getDoubleValue(i) - actual.getDoubleValue(i)), 2);
            }
        }
        if (count > 0) {
            double meanSqErr = runSumPredictedSqErr / count;
            return Math.sqrt(meanSqErr);
        } else {
            throw new Exception("No suitable data");
        }
    }
    
    /**
     * Calculate the coefficient of determination R^2 value.
     *
     * @param actualColumn A DoubleColumn containing the actual values.
     * @param predictedColumn A DoubleColumn containing the predicted values.
     * @return The R squared value.
     *
     */
    private double rSquared(DoubleColumn actualColumn, DoubleColumn predictedColumn) throws Exception {

        double runsumActuals = 0, ssTotal = 0, ssErr = 0;
        double actual, predicted;
        int count = 0;
        for (int i = 0; i < actualColumn.getRows(); i++) {
            if (!actualColumn.isMissing(i) && !predictedColumn.isMissing(i)) {
                predicted = predictedColumn.getDoubleValue(i);
                actual = actualColumn.getDoubleValue(i);
                runsumActuals = runsumActuals + actual;
                ssTotal = ssTotal + Math.pow((actual - meanActual), 2);
                ssErr = ssErr + Math.pow((actual - predicted), 2);
                count++;
            }
        }

        if (count > 0) {
            return 1 - (ssErr / ssTotal);
        } else {
            throw new Exception("No suitable data");
        }
    }

    /**
     * Calculates the maximum absolute error attained.
     *
     * @param actual A DoubleColumn of actual y values
     * @param predicted A DoubleColumn of predicted y values
     * @return The maximum absolute error
     */
    private double maxAbsError(DoubleColumn actual, DoubleColumn predicted)
            throws Exception {
        double maxsAbserror = -1, pred, act, err;


        for (int i = 0; i < actual.getRows(); i++) {

            if (!actual.isMissing(i) && !predicted.isMissing(i)) {
                pred = predicted.getDoubleValue(i);
                act = actual.getDoubleValue(i);
                err = Math.abs(pred - act);

                if (err > maxsAbserror) {
                    maxsAbserror = err;
                }

            }
        }
        return maxsAbserror;
    }    
}