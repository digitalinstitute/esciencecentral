
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.WekaClassificationModel;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.StringColumn;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToNominal;

/**
 * E-sc block to implement CLASSIFICATION using the Weka implementation of the
 * Random Forest algorithm. This cannot be used for regression type problems,
 * i.e. those with a numeric modelled y variable.
 *
 * Note: output columns (prediction class labels and actual class labels) are
 * StringColumns.
 *
 * @author Dominic Searson 27/2/2014
 */
public class RandomForestModel extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {

        //access workflow data objects that are inputs to this block
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");

        if (yData.getColumnCount() == 1) {

            //get user properties

            /* shuffles modelled variable before modelling */
            boolean yscramble = getEditableProperties().booleanValue("Scramble modelled variable", false);

            int numTrees = getEditableProperties().intValue("Number of trees", 10); //number of trees in random forest
            if (numTrees < 1) {
                throw new IllegalArgumentException("Illegal value (< 1) found for user block parameter 'Number of trees' : " + numTrees);
            }

            int numRandSel = getEditableProperties().intValue("Random selection variables", 5); //number of features in each random selection of variables
            if (numRandSel < 0) {
                throw new IllegalArgumentException("Illegal value (< 0) found for user block parameter 'Random selection variables' : " + numRandSel);
            }

            int seed = getEditableProperties().intValue("Random seed", 1); //seed for PNRG

            int maxTreeDepth = getEditableProperties().intValue("Max tree depth", 0); //max tree depth, 0 = unlimited
            if (maxTreeDepth < 0) {
                throw new IllegalArgumentException("Illegal value (< 0) found for user block parameter 'Max tree depth' : " + maxTreeDepth);
            }


            Column yCol = yData.column(0);

            //y-scramble if required
            if (yscramble) {
                List yCol_list = yCol.getList();
                Collections.shuffle(yCol_list); //shuffle the list (because it's a reference to the list witin yCol, this will be shuffled.
            }

            //convert data to Weka compatible
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
            
             //get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());

            //create untrained 'model'
            RandomForest model = new RandomForest();
            model.setNumTrees(numTrees);
            model.setNumFeatures(numRandSel);
            model.setSeed(seed);
            model.setMaxDepth(maxTreeDepth);

            //convert 'string' labelled vars to nominal class labels 
            StringToNominal filter = new StringToNominal();
            filter.setAttributeRange("first-last");
            filter.setInputFormat(wekaInstances);
            Instances filteredInstances = Filter.useFilter(wekaInstances, filter);

            //train model
            model.buildClassifier(filteredInstances);

            //the attribute type iof the modelled variable (see http://weka.wikispaces.com/Use+WEKA+in+your+Java+code 
            Attribute classAttribute = filteredInstances.classAttribute();
            if (!classAttribute.isNominal()) {
                throw new DataException("The output (y) variable could not be converted to a Weka 'Nominal' variable");
            }
            int numClassLabels = classAttribute.numValues();

            
            //create a wrapper object for the trained classifier
            WekaClassificationModel modelwrapper = new WekaClassificationModel();
          //  modelwrapper.setNumClassLabels(numClassLabels);
            modelwrapper.setClassifier(model);
            modelwrapper.setClassAttribute(classAttribute);
            
            
            //an evalaution class provides info on the performance of the trained classifier (can be used for extra meta-data for model reporting)
            Evaluation eval = new Evaluation(filteredInstances);
            double[] rawModelPredictions = eval.evaluateModel(model, filteredInstances);

            // Get class label predictions on training data
            Column predicted = new StringColumn("Predicted");
            double currPredVal;

            String classVal;  //the nominal output class, e.g. 'A','B','C' etc
            for (int i = 0; i < filteredInstances.numInstances(); i++) {
                currPredVal = rawModelPredictions[i];
                classVal = classAttribute.value((int) currPredVal);  // converts e.g. 1 or 0 doubles into the class label, e.g. "A" or "B"
                predicted.appendStringValue(classVal);
                
                //add numeric class value and string class label to modelwrapper
                if (! modelwrapper.getClasslabels().containsKey(currPredVal) ) {
                    modelwrapper.getClasslabels().put(currPredVal, classVal);
                }
                
            }
            
            if ( modelwrapper.getClasslabels().size() != modelwrapper.getClassAttribute().numValues() ) {
                throw new DataException("Unexpected number of class labels found.");
            }
            

            //add predictions to output dataTable object
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");
            predictionData.addColumn(predicted);
            setOutputDataSet("y-actual-predicted", predictionData);

            // Save the modelwrapper as an esc workflow object
            ObjectWrapper payload = new ObjectWrapper(modelwrapper);
            setOutputData("model", payload);

            //Add some model meta-data to block outputs
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("ModelDetails", "Algorithm", "Random Forest"));
            mdc.add(new TextMetadata("ModelDetails", "Model type", "Classification"));

            mdc.add(new TextMetadata("ModelDetails", "Weka model class", model.getClass().getSimpleName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of trees", model.getNumTrees()));
            mdc.add(new NumericalMetadata("ModelDetails", "Max. tree depth", model.getMaxDepth()));

            mdc.add(new NumericalMetadata("ModelDetails", "Variables used in random selection", model.getNumFeatures()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of x variables", xData.getColumnCount()));
            mdc.add(new TextMetadata("ModelDetails", "Modelled variable name", yCol.getName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of training instances", wekaInstances.numInstances()));
            mdc.add(new BooleanMetadata("ModelDetails", "Scrambled modelled variable", yscramble));

            //classification specific metrics
            mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "Out of bag error", model.measureOutOfBagError()));
            mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "Training instances correct", eval.correct()));
            mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "Training instances incorrect", eval.incorrect()));

            for (int i = 0; i < numClassLabels; i++) {

                mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "Recall (class '" + classAttribute.value(i) + "')", eval.recall(i)));
                mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "Precision (class '" + classAttribute.value(i) + "')", eval.precision(i)));
                mdc.add(new NumericalMetadata("ClassificationPerformanceTraining", "F-measure (class '" + classAttribute.value(i) + "')", eval.fMeasure(i)));

            }

            mdc.add(new TextMetadata("ClassificationPerformanceTraining", "Confusion Matrix", "<pre>" + eval.toMatrixString() + "</pre>"));

            //data attributes 
            mdc.add(new TextMetadata("DataAttributes", "Weka data summary (filtered)", filteredInstances.toSummaryString()));
            mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", true));
            addMetadataToAllOutputs(mdc);

            //Save text report about model
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "RandomForest_Data.txt"));
            ZipUtils.writeSingleLineFile(report, model.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);

            //Save ARFF report
            File arffFile = new File(getWorkingDirectory(), "WekaTrainingDataSummary.txt");
            ZipUtils.writeSingleLineFile(arffFile, filteredInstances.toSummaryString());
            reportWrapper.addFile(arffFile, false);

            setOutputData("report", reportWrapper);

        } else {
            throw new DataException("Only a single column 'y' output can be modelled using this block.");
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}