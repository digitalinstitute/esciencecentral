
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.DataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.DoubleColumn;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Instances;

public class StandaloneRegressionPredictionBlock extends CloudDataProcessorService {

    /**
     * Trained Weka regression model which is used for prediction
     */
    private Classifier regressionModel;

    @Override
    public void executionAboutToStart() throws Exception {
        // Load the regression model 
        ObjectWrapper modelWrapper = (ObjectWrapper) getInputData("model");
        regressionModel = (Classifier) modelWrapper.getPayload();
    }

    @Override
    public void execute() throws Exception {

        //get input X datawrapper (i.e. predictor variable)
        Data xData = this.getInputDataSet("x");

        //Need to add a dummy Y column becuase some Weka model types
        //throw a fit if the number of atrributes is different to that 
        //encountered in training.
        int xrows = xData.getLargestRows();
        double[] dummyVals = new double[xrows];
        
        for (int i = 0; i < dummyVals.length; i++) {
            dummyVals[i] = 0;
        }
        
        DoubleColumn dummyCol = new DoubleColumn(dummyVals);
        dummyCol.setName("dummyCol");
        xData.addColumn(dummyCol);
        
        
        //convert datawrapper to Weka instances
        Instances wekaInstances = new DataToInstances(xData).toWekaInstances();

        //get data name from 'x' data
        wekaInstances.setRelationName(xData.getName());
        wekaInstances.setClassIndex(wekaInstances.numAttributes() - 1); //??

        Evaluation eval = new Evaluation(wekaInstances);
        double[] predictions = eval.evaluateModel(regressionModel, wekaInstances);
        
        // Generate predictions of response variable y
//        DoubleColumn predicted = new DoubleColumn("predicted");
//        for (int i = 0; i < wekaInstances.numInstances(); i++) {
//            predicted.appendDoubleValue(regressionModel.classifyInstance(wekaInstances.instance(i)));
//        }
        DoubleColumn predicted = new DoubleColumn(predictions);

        //attach predictions to output data set
        Data predictionData = new Data();
        predictionData.addColumn(predicted);
        setOutputDataSet("y-predicted", predictionData);

        // Add some metadata to the outputs
        MetadataCollection mdc = new MetadataCollection();

        mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", false));
        mdc.add(new TextMetadata("DataAttributes", "Weka data summary", wekaInstances.toSummaryString()));
        mdc.add(new NumericalMetadata("ModelDetails", "Number of instances", wekaInstances.numInstances()));
        mdc.add(new TextMetadata("DataAttributes", "Weka data summary", wekaInstances.toSummaryString()));
        addMetadataToAllOutputs(mdc);

        //Save data ARFF report
        FileWrapper wrapper = new FileWrapper(getWorkingDirectory());
        File arffFile = new File(getWorkingDirectory(), "WekaDataSummary.txt");
        ZipUtils.writeSingleLineFile(arffFile, wekaInstances.toSummaryString());
        wrapper.addFile(arffFile, false);
        setOutputData("report", wrapper);

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}