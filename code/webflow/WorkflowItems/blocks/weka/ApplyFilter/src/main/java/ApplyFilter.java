
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.weka.DataToInstances;
import com.connexience.server.weka.InstancesToData;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;

public class ApplyFilter extends CloudDataProcessorService {
    //filter to be applied (supplied to block)
    private Filter wekaFilter;
    
    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
        // Load the filter
        ObjectWrapper objWrapper = (ObjectWrapper) getInputData("filter");

        try {
            wekaFilter = (Filter) objWrapper.getPayload();
        } catch (ClassCastException ex) {
            throw new Exception("The supplied object is not a Weka Filter object.", ex);
        }        
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        boolean appendDummyY = getEditableProperties().booleanValue("AppendDummyY", true);
        
        Data inputData = getInputDataSet("input-data");
        if(appendDummyY){
            DoubleColumn c = new DoubleColumn("Dummy");
            int rows = inputData.getLargestRows();
            for(int i=0;i<rows;i++){
                c.appendDoubleValue(0);
            }
            inputData.addColumn(c);
        }
        DataToInstances inputConverter = new DataToInstances(inputData);
        
        Instances wekaInstances = inputConverter.toWekaInstances();
        wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
        
        //apply supplied filter
        Instances filteredInstances;

        try {
            filteredInstances = Filter.useFilter(wekaInstances, wekaFilter);
            filteredInstances.setRelationName(wekaInstances.relationName());
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new WekaException("This filter could not be applied to the data. Check the ARFF input data is in exactly the same format as the data the filter was created on.");
        }

        Data outputData = new InstancesToData(filteredInstances).toData();
        
        // Remove the dummy column if added
        if(appendDummyY){
            outputData.removeColumn(outputData.getColumnCount() - 1);
        }
        setOutputDataSet("filtered-data", outputData);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}