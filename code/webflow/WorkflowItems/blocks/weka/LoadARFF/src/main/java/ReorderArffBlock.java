/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Reorder;

/**
 * E-Sc block to load a Weka data (ARFF) text file, re-order the attributes (and possibly
 * delete one or more) and then output the resulting ARFF file from the block in a FileWrapper. 
 * 
 * Note: by default Weka treats the final attribute as the variable to be modelled (the 'class variable').
 * 
 * Also note: Weka 'classifiers' (regression and classification modelling blocks) will
 * not accept duplicate attribute definitions in an ARFF file. So a re-ordering 
 * like 1,1,1,1,2 will be rejected by Weka modelling blocks.
 *
 * @author Dominic Searson 3/14
 */
public class ReorderArffBlock extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {


        boolean reorder = false;

        //user block input 
        String attributeOrder = getEditableProperties().stringValue("Attribute configuration", "");
        if (!attributeOrder.isEmpty()) {
            reorder = true;
        }


        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");

        if (arffWrapper.getFileCount() == 1) {
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);


            /**
             * NB Reorder filter info: this generates output with a new order of
             * the attributes. Useful if one wants to move an attribute to the
             * end to use it as class attribute (e.g. with using "2-last,1").
             * But it's not only possible to change the order of all the
             * attributes, but also to leave out attributes. E.g. if you have 10
             * attributes, you can generate the following output order:
             * 1,3,5,7,9,10 or 10,1-5. You can also duplicate attributes, e.g.
             * for further processing later on: e.g. 1,1,1,4,4,4,2,2,2 where the
             * second and the third column of each attribute are processed
             * differently and the first one, i.e. the original one is kept. One
             * can simply inverse the order of the attributes via 'last-first'.
             * After appyling the filter, the index of the class attribute is
             * the last attribute. (from Weka javadoc)
             */
            Instances filteredInstances;
            if (reorder) {

                Reorder reorderFilter = new Reorder();
                reorderFilter.setAttributeIndices(attributeOrder);
                reorderFilter.setInputFormat(wekaInstances);
                filteredInstances = Filter.useFilter(wekaInstances, reorderFilter);
                filteredInstances.setRelationName(wekaInstances.relationName());

            } else {
                filteredInstances = wekaInstances;
            }


            //now write arff out in a filewrapper
            String fileName = "reorderedAttributes.arff";
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filteredInstances);
            File outFile = new File(getWorkingDirectory(), fileName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);



        } else {
            throw new WekaException("ReorderARFF block can only parse a single ARFF file");
        }

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}