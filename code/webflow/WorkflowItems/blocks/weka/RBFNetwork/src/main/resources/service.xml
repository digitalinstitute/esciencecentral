<!--
  e-Science Central
  Copyright (C) 2008-2013 School of Computing Science, Newcastle University

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  version 2 as published by the Free Software Foundation at:
  http://www.gnu.org/licenses/gpl-2.0.html

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
-->
<WorkflowService>
    <!-- Name of the service, and also the caption that will appear     -->
    <!-- in the top line of the block on the workflow editor            -->
    <Name>RBFNetwork</Name>

    <!-- Service description that appears at the bottom of the editor   -->
    <!-- window when the block is selected                              -->
    <Description>Trains a Weka RBF network non-linear regression model using numerical input/output data</Description>

    <!-- Category to place the service in on the editor palette         -->
    <Category>Weka.Regression</Category>

    <!-- Homepage for block documentation                               -->
    <Homepage>/</Homepage>

    <!-- Class name of the service. This needs to extend either the     -->
    <!-- DataProcessorService or the CloudDataProcessorService object   -->
    <ServiceRoutine>RBFNet</ServiceRoutine>

    <!-- Auto deployed service. Do NOT change for dynamically deployed  -->
    <!-- services that are uploaded via this editor                     -->
    <ServiceType>AUTO</ServiceType>

    <!-- Data streaming mode for this service. This can be one of:      -->
    <!--                                                                -->
    <!-- nostream   - Data is passed in one block through service       -->
    <!-- sequential - Data is streamed one connection at a time         -->
    <!-- parallel   - Data is streamed from all connections in parallel -->
    <StreamMode>nostream</StreamMode>
    
 
    <Properties>

        <property name ="Clusters" type ="Integer" default="2" description = "The number of K-means clusters to use (default = 2)"/>
        <Property name="MaxIterations" type="Integer" default="-1" category="Advanced" description="The maximum number of iterations (-1 = until convergence, default = -1)"/>
        <Property name="Seed" type="Integer" default="1" description="The random seed used for K-means clustering (default = 1)"/>
        <Property name="MinStdDev" type="Double" default="0.1" category ="Advanced" description="Min standard deviation of a cluster (default = 0.1)"/>
        
      <!-- debug -->
        <Property name="DebugMode" type="Boolean" default="false" description="" category="Debugging" />
        <Property name="DebugSuspended" type="Boolean" default="true" description="" category="Debugging" />
        <Property name="DebugPort" type="Integer" default="5005"  description="" category="Debugging" />   
        
    </Properties>

    <!-- Definition of all of the inputs to a service. The format is:   -->
    <!--                                                                -->
    <!-- <Input name="" type="" streaming=""/>                          -->
    <!--                                                                -->
    <!-- Where:     name = name of input also displayed on connections  -->
    <!--            type = data-wrapper - mixed matrix of data          -->
    <!--                   file-wrapper - list of file names            -->
    <!--                   object-wrapper - Serialized Java object      -->
    <!--                   properties-wrapper - List of name-value pairs-->
    <!--                   link-wrapper - Reference to server documents -->
    <!--            streaming = true / false - is this a streaming link -->
    <Inputs>
        <Input name="y" type="data-wrapper" streaming="false"/>
        <Input name="x" type="data-wrapper" streaming="false"/>
    </Inputs>

    <!-- Definition of all of the outputs from service. The format is:  -->
    <!--                                                                -->
    <!-- <Output name="" type="" streaming=""/>                         -->
    <!--                                                                -->
    <!-- Where:     name = name of input also displayed on connections  -->
    <!--            type = data-wrapper - mixed matrix of data          -->
    <!--                   file-wrapper - list of file names            -->
    <!--                   object-wrapper - Serialized Java object      -->
    <!--            streaming = true / false - is this a streaming link -->
    <Outputs>
        <Output name="y-actual-predicted" type="data-wrapper" streaming="false"/>
        <Output name="report" type="file-wrapper" streaming="false"/>
        <Output name="model" type="object-wrapper" streaming="false"/>
    </Outputs>

</WorkflowService>