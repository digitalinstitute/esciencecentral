
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;

/**
 * E-Sc block to supply a configured existing Weka 'filter' directly to an input 
 * ARFF data file.
 *
 * For instance - the filter from a StandardiseARFF block can be applied to new
 * data with this block.
 * 
 * This block can only apply the filter if the the structure of the ARFF file
 * is the same as that to which the filter was originally applied/created.
 *
 * @author Dominic Searson
 */
public class ApplyExistingFilter_ARFF extends CloudDataProcessorService {

    //filter to be applied (supplied to block)
    private Filter wekaFilter;

    @Override
    public void executionAboutToStart() throws Exception {

        // Load the filter
        ObjectWrapper objWrapper = (ObjectWrapper) getInputData("input-arff-filter");

        try {
            wekaFilter = (Filter) objWrapper.getPayload();
        } catch (ClassCastException ex) {
            throw new WekaException("The supplied object is not a Weka Filter object.");
        }
    }

    @Override
    public void execute() throws Exception {

        String fileName = getEditableProperties().stringValue("FileName", "appliedExistingFilter.arff");
        
        //load input arff file and convert to Instances
        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");

        if (arffWrapper.getFileCount() == 1) {
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);

            //apply supplied filter
            Instances filteredInstances;

            try {
                filteredInstances = Filter.useFilter(wekaInstances, wekaFilter);
                filteredInstances.setRelationName(wekaInstances.relationName());
            } catch (Exception ex) {
                throw new WekaException("This filter could not be applied to the data. Check the ARFF input data is in exactly the same format as the data the filter was created on.");
            }
            //now write arff out in a filewrapper
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filteredInstances);
            File outFile = new File(getWorkingDirectory(), fileName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);

        } else {
            throw new WekaException("ARFF-ApplyFilter block can only parse a single ARFF file");
        }

    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}