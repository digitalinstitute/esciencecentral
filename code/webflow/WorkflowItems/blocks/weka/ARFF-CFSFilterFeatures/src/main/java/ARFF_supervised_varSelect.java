/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.attributeSelection.CfsSubsetEval;
import weka.attributeSelection.GreedyStepwise;
import weka.attributeSelection.GeneticSearch;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.Ranker;

import weka.core.Instances;
import weka.core.WekaException;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;

/**
 * E-Sc block to perform (supervised) feature selection directly on a Weka ARFF
 * file. It is assumed that the modelled/class variable is the last one defined
 * in the ARFF file. The Weka Filter object is exported within an ObjectWrapper
 * for application on new data using the ARFF-ApplyFilter block.
 *
 * Options available via block menu: 
 * 
 * Evaluation
 * ----------
 * - CfsSubsetEval. 
 * This is an implementation of Hall's 1998 Correlation method for feature 
 * selection. Subsets of input attributes are evaluated to determine their 
 * likely predictive quality of the output (modelled) variable.
 * 
 * - Information gain with respect to the output (IG) (CLASS LABELLED/NOMINAL OUTPUT ONLY)
 * 
 * Search
 * ------
 * - Genetic search
 * - Greedy search
 *
 * Requires an ARFF file as in input and writes an ARFF file as an output.
 *
 * @author Dominic Searson 3/14
 */
public class ARFF_supervised_varSelect extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {

        //get user options from block
        String evalType = getEditableProperties().stringValue("Evaluation method", "Hall's correlation feature subset selection (CFS)");
        String searchType = getEditableProperties().stringValue("Search method", "Greedy stepwise");
        int rankedFeatures = getEditableProperties().intValue("Number of ranked features to retain (IG)", -1);// this only works with class labelled output variable
        String fileName = getEditableProperties().stringValue("FileName", "featureFiltered_data.arff");
        
         if (fileName == null || fileName.isEmpty()) {
            fileName = "featureFiltered_data.arff";            
         }
         
         //load input arff and convert to weka instances
        FileWrapper arffWrapper = (FileWrapper) getInputData("input-arff-file");


        if (arffWrapper.getFileCount() == 1) {
            File arffFile = arffWrapper.getFile(0);
            ArffLoader loader = new ArffLoader();
            loader.setFile(arffFile);
            Instances wekaInstances = loader.getDataSet();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);


            //setup filter feature options
            AttributeSelection filter = new AttributeSelection();
            switch (evalType) {
                case "Hall's correlation feature subset selection (CFS)":
                    CfsSubsetEval cfs = new CfsSubsetEval();
                    filter.setEvaluator(cfs);
                    break;

                case "Information gain with respect to the output (IG)":
                    GainRatioAttributeEval gr = new GainRatioAttributeEval();
                    filter.setEvaluator(gr);
                    Ranker ranker = new Ranker(); //NB MUST use ranker with GainRatio eval method
                    ranker.setNumToSelect(rankedFeatures);
                    filter.setSearch(ranker); 
                    break;
                default:
                    throw new IllegalArgumentException("Unknown option : " +evalType);
            }


            if (!evalType.equals("Information gain with respect to the output (IG)")) {

                switch (searchType) {
                    case "Greedy stepwise":
                        GreedyStepwise greedySearch = new GreedyStepwise();
                        greedySearch.setSearchBackwards(true);
                        filter.setSearch(greedySearch);
                        break;

                    case "Genetic":
                        GeneticSearch genSearch = new GeneticSearch();
                        filter.setSearch(genSearch);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown option: " +searchType);
                }

            }
            
            //apply filter
            filter.setInputFormat(wekaInstances);
            Instances filteredInstances = Filter.useFilter(wekaInstances, filter);
            filteredInstances.setRelationName(wekaInstances.relationName());
            
            //write filtered arff out in a filewrapper
            FileWrapper outputFile = new FileWrapper(getWorkingDirectory());
            ArffSaver saver = new ArffSaver();
            saver.setInstances(filteredInstances);
            File outFile = new File(getWorkingDirectory(), fileName);
            saver.setFile(outFile);
            saver.writeBatch();
            outputFile.addFile(outFile, false);
            setOutputData("output-arff-file", outputFile);

             //write the filter out as an object wrapper
            ObjectWrapper modelWrapper = new ObjectWrapper(filter);
            setOutputData("ARFF-filter", modelWrapper);

        } else {
            throw new WekaException("Only one ARFF file can be accepted by this block");
        }
    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}