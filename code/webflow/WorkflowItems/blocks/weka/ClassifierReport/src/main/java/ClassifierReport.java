
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.util.ZipUtils;
import com.connexience.tools.html2pdf.Html2PdfConverter;
import java.io.File;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Date;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.CSVDataExporter;
import org.pipeline.core.data.manipulation.ColumnPicker;

/**
 * e-Sc block to generate a PDF report on the properties of a predictive
 * CLASSIFICATION type model on training data with numeric/nominal input data 
 * types and a nominal (i.e. category valued) output data type.
 *
 * For REGRESSION type models (all numeric) use the ModelReport block.
 *
 * This block takes as inputs a column of 'actual' class label string values and
 * a column of 'predicted' class labels and computes a number of model
 * performance metrics.
 *
 * The generated report content depends on whether the data is 'training' data
 * or 'test' data.
 *
 *
 * @author Dominic Searson 
 */
public class ClassifierReport implements WorkflowBlock {

    private File tmpDir;
    private boolean isTrainingData = true;
    private boolean isYscrambledTraining = false;
    /*
     * String for controlling formatting of model performance metric values
     * in report
     */
    private static String DECIMALFORMATTINGSTR = "#.#####";

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {


        //check model type is classification and exit if so
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        MetadataItem modelType = mdc.find("ModelDetails", "Model type");

        if ((modelType != null) && (modelType.getStringValue().equalsIgnoreCase("Regression"))) {
            throw new DataException("The Model Report block can not accept model predictions from 'Regression' blocks");
        }

        // Create a temporary directory to store the report html file in
        tmpDir = new File(env.getWorkingDirectory(), env.getBlockContextId() + "-html");
        tmpDir.mkdir();

        // Copy the stylesheet
        ZipUtils.copyFile(env.getExecutionService().getLibraryItem().getFile("report.css"), new File(tmpDir, "report.css"));
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {


        // Get the input data 
        env.getExecutionService().populateInputDataSetMap();
        Data inputData = inputs.getInputDataSet("model-results");

        //get model metadata
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");

        //get optional user data description from block parameter
        String userDatasetName = env.getStringProperty("Dataset", "");

        //did yscrambling occur during training?
        BooleanMetadata yScrambleMeta = (BooleanMetadata) mdc.find("Scrambled modelled variable");
        if (yScrambleMeta != null) {
            if (yScrambleMeta.isBooleanValue()) {
                this.isYscrambledTraining = true;
            }
        }

        // Find out which columns contain the actual and predicted values and extract them from the input data
        //because we are predicting class labels these should be strings.
        ColumnPicker actualPicker = new ColumnPicker(env.getStringProperty("ActualColumn", "#0"));
        ColumnPicker predictedPicker = new ColumnPicker(env.getStringProperty("PredictionColumn", "#1"));

        StringColumn actual = (StringColumn) actualPicker.pickColumn(inputData);
        StringColumn predicted = (StringColumn) predictedPicker.pickColumn(inputData);



        // Check the number of predictions and actual values match, and only proceed if they do
        if (actual.getRows() == predicted.getRows()) {

            
            //find out whether data is training or testing
            initialiseDataAttributes(env);

            // Write the report html
            File report = new File(tmpDir, "report.html");
            PrintWriter writer = new PrintWriter(report);
            writer.println("<html>");
            writer.println("<link rel=\"stylesheet\" href=\"report.css\"/>");
            writer.println("<head></head><body>");

            if (isTrainingData) {
                writer.println("<h1>Classifier report: " + userDatasetName + " training data</h1>");
            } else {
                writer.println("<h1>Classifier report: " + userDatasetName + " testing data</h1>");;
            }

            //generate report tables
            generateWorkflowInfoTable(env, writer);
            generateSummaryTable(env, writer);
            generatePerformanceTable(env, writer);
            
           //close HTML document
            writer.println("</body></html>");
            writer.flush();
            writer.close();

            // Convert to pdf
            File reportPdf = new File(env.getWorkingDirectory(), env.getStringProperty("ReportName", "classReport.pdf"));
            Html2PdfConverter converter = new Html2PdfConverter(report, reportPdf);
            converter.convert();
            outputs.setOutputFileWithFullPath("report", reportPdf);

        } else {
            throw new DataException("Row counts in actual and predicted columns do not match");
        }
    }

    /**
     * Generates some HTML workflow info for the report, e.g. the workflow id,
     * invocation date etc.
     *
     * @param env The block environment
     * @param writer The Writer for the report document
     */
    public void generateWorkflowInfoTable(BlockEnvironment env, PrintWriter writer) throws Exception {

        WorkflowInvocationFolder invocationFolder = env.getExecutionService().getInvocationFolder();

        String workflowId = invocationFolder.getWorkflowId();
        APIBroker api = env.getExecutionService().createApiLink();
        ServerObject serverObject = api.getServerObject(workflowId); //gets server object via rmi 
        String displayName = serverObject.getName();

        String workflowDescription = serverObject.getDescription();
        Date creationDate = serverObject.getCreationDate();

        String versionId = invocationFolder.getVersionId();
        Date invocationDate = invocationFolder.getInvocationDate();
        
        writer.println("<h2>Workflow info</h2>");
        writer.println("<table width=\"100%;\"><tbody>");
        writer.println("<tr><td>Workflow name</td><td>" + displayName + "</td></tr>");
        writer.println("<tr><td>Workflow description</td><td>" + workflowDescription + "</td></tr>");
        writer.println("<tr><td>Workflow creation date</td><td>" + creationDate.toString() + "</td></tr>");
        writer.println("<tr><td>Workflow ID</td><td>" + workflowId + "</td></tr>");
        writer.println("<tr><td>Version ID</td><td>" + versionId + "</td></tr>");
        writer.println("<tr><td>Invocation date</td><td>" + invocationDate.toString() + "</td></tr>");
        writer.println("</tbody></table>");
    }

    /**
     * Generates some HTML data set info for the report, e.g. 'name' of data set
     * etc.
     *
     * @param env The block environment
     * @param writer The Writer for the report document
     * @throws Exception If it doesn't work
     */
    public void generateDataSummaryTable(BlockEnvironment env, PrintWriter writer) throws Exception {
        writer.println("<h2>Data summary</h2>");


        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        if (mdc != null && mdc.size() > 0) {

            writer.println("<table width=\"100%;\"><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                if (md.getCategory() != null && md.getCategory().equals("DataDescription")) {
                    writer.println("<tr>");
                    writer.println("<td>" + md.getName() + "</td>");
                    writer.println("<td>" + md.getStringValue() + "</td>");
                    writer.println("</tr>");
                }
            }
            writer.println("</tbody></table>");
        }
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {
    }

    

    /** Generates a model summary table
     * 
     * @param env
     * @param writer
     * @throws Exception 
     */
    private void generateSummaryTable(BlockEnvironment env, PrintWriter writer) throws Exception {
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        if (mdc != null && mdc.size() > 0) {
            writer.println("<h2>Classifier summary</h2>");
            writer.println("<table width=\"100%;\"><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                if (md.getCategory().equals("ModelDetails")) {
                    writer.println("<tr>");
                    writer.println("<td>" + md.getName() + "</td>");
                    writer.println("<td>" + md.getStringValue() + "</td>");
                    writer.println("</tr>");
                }
            }
            writer.println("</tbody></table>");
        }
    }

    /** Generates a table of classifier performance metrics.
     *  These depend on whether the data is 'training' or 'testing'.
     * 
     * @param env
     * @param writer
     * @throws Exception 
     */
    private void generatePerformanceTable(BlockEnvironment env, PrintWriter writer) throws Exception {
       
        DecimalFormat df = new DecimalFormat(DECIMALFORMATTINGSTR);
        
        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        if (mdc != null && mdc.size() > 0) {
           
            writer.println("<h2>Classifier performance metrics</h2>");

            //warning message is y-scrambling was used during training of model
            if (this.isYscrambledTraining) {
                writer.println("<p class='warning'>Output data was scrambled during training/model fitting. Poor performance metrics below are to be expected.</p>");
            }

            
            //use metadata according to whether its trainibng or testing data
            String typeStr;
            if (this.isTrainingData) {
                typeStr = "ClassificationPerformanceTraining";
            } else {
                typeStr = "ClassificationPerformanceTesting";
            }
           

            writer.println("<table width=\"100%;\"><tbody>");
            MetadataItem md;
            for (int i = 0; i < mdc.size(); i++) {
                md = mdc.get(i);
                if (md.getCategory().equals(typeStr)) {
                    writer.println("<tr>");
                    writer.println("<td>" + md.getName() + "</td>");
                    if (md instanceof NumericalMetadata) {
                      writer.println("<td>" + df.format( ((NumericalMetadata)md).getDoubleValue() ) + "</td>");
                    }
                    else {
                        writer.println("<td>" + md.getStringValue() + "</td>");
                    }
                    writer.println("</tr>");
                }
            }
            writer.println("</tbody></table>");
        }
    }

    private void writeData(Data data, File f) throws Exception {
        // Need to create a new data file
        CSVDataExporter exporter = new CSVDataExporter(data);
        exporter.setDelimiter(" ");
        exporter.setIncludeNames(false);
        exporter.setRowIndexIncluded(true);
        exporter.setRowIndexValue(0);
        PrintWriter dataWriter = new PrintWriter(f);
        exporter.writeToPrintWriter(new PrintWriter(f));
        dataWriter.flush();
        dataWriter.close();
    }

    /**
     * Examines meta-data to see whether the data is marked as 'training or 'testing'
     *
     */
    private void initialiseDataAttributes(BlockEnvironment env) throws Exception {


        MetadataCollection mdc = env.getExecutionService().getInputMetadata("model-results");
        for (MetadataItem i : mdc.getItems()) {
            if (i.getCategory().equals("DataAttributes")) {
                if (i.getName().equals("TrainingData") && i instanceof BooleanMetadata) {
                    isTrainingData = ((BooleanMetadata) i).isBooleanValue();
                }
            }
        }
    }
    
}
    