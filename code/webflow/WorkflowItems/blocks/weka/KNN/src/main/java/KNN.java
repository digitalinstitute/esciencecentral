/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.BooleanMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.util.ZipUtils;
import com.connexience.server.weka.XYDataToInstances;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.engine.datatypes.ObjectWrapper;
import java.io.File;
import java.util.Collections;
import java.util.List;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.maths.MeanValueCalculator;
import weka.core.Instances;
import weka.classifiers.lazy.IBk;
import weka.core.Attribute;
import weka.core.SelectedTag;

/**
 * E-Sc block of Weka K-nearest neighbour 'regression' implementation.
 * 
 * This is only valid for numeric inputs and output.
 *
 * Should provide 'perfect' result on training data (when k=1).
 *
 * @author Dominic Searson 28/2/14
 */
public class KNN extends CloudDataProcessorService {

    @Override
    public void execute() throws Exception {

        //access workflow data objects that are inputs to this block
        Data xData = getInputDataSet("x");
        Data yData = getInputDataSet("y");


        if (yData.getColumnCount() == 1) {

            //get user properties
            int knn = getEditableProperties().intValue("K-nearest neighbours", 1); //default =1 
            if (knn < 1) {
                throw new IllegalArgumentException("Illegal value (< 1) found for user block parameter 'K-nearest neighbours' : " +knn);
            }
            
            boolean useCrossVal = getEditableProperties().booleanValue("Autoselect k", false); //selects k by leave one out crossval on training data
            
            boolean useMSE = getEditableProperties().booleanValue("UseMSE", false); //uses MSE not MAE during cross val on k
            
            /* shuffles modelled variable before modelling */
            boolean yscramble = getEditableProperties().booleanValue("Scramble modelled variable", false);
            
            
            
            String weightingSelection = getEditableProperties().stringValue("Distance weighting", "None");
            int selected = -1;
            if (weightingSelection.equalsIgnoreCase("None")) {
                selected = IBk.WEIGHT_NONE;
            } else if (weightingSelection.equalsIgnoreCase("Inverse")) {
                selected = IBk.WEIGHT_INVERSE;
            } else if (weightingSelection.equalsIgnoreCase("Similarity")) {
                selected = IBk.WEIGHT_SIMILARITY;
            } else {
                throw new IllegalArgumentException("Unknown value found for user block parameter 'K-nearest neighbours' : " +weightingSelection);
            }
            
            
            Column yCol = yData.column(0);
            //y-scramble if required
            if (yscramble) {
                List yCol_list = yCol.getList();
                Collections.shuffle(yCol_list); 
            }

            
            //convert data to Weka compatible
            XYDataToInstances converter = new XYDataToInstances(xData, yCol);
            Instances wekaInstances = converter.toWekaInstances();
            wekaInstances.setClassIndex(wekaInstances.numAttributes() -1);
             
             //get data name from 'x' data
            wekaInstances.setRelationName(xData.getName());

            
            //create untrained 'model'
            IBk model = new IBk();
            
            model.setKNN(knn);
            model.setCrossValidate(useCrossVal);
            model.setMeanSquared(useMSE);
            model.setDistanceWeighting(new SelectedTag(selected,IBk.TAGS_WEIGHTING));
         
            
            //train
            model.buildClassifier(wekaInstances);

            // Get predictions on training data
            DoubleColumn predicted = new DoubleColumn("Predicted");

            double currPredVal;
            for (int i = 0; i < wekaInstances.numInstances(); i++) {

                currPredVal = model.classifyInstance(wekaInstances.instance(i));
                predicted.appendDoubleValue(currPredVal);
            }

            //find the modelled var
            Attribute modelledVar = wekaInstances.classAttribute();
            
            
            //add predictions to output dataTable object
            Data predictionData = new Data();
            predictionData.addColumn(yCol.getCopy());
            predictionData.column(0).setName("Actual");
            predictionData.addColumn(predicted);
            setOutputDataSet("y-actual-predicted", predictionData);

            // Save the model as an esc workflow object
            ObjectWrapper modelWrapper = new ObjectWrapper(model);
            setOutputData("model", modelWrapper);

            //add some model meta-data to block outputs
            MetadataCollection mdc = new MetadataCollection();
            mdc.add(new TextMetadata("ModelDetails", "Algorithm", "K Nearest Neighbour"));
             mdc.add(new TextMetadata("ModelDetails", "Model type", "Regression"));
            mdc.add(new TextMetadata("ModelDetails", "Weka model class", model.getClass().getSimpleName()));

            mdc.add(new NumericalMetadata("ModelDetails", "K nearest neighbours", model.getKNN()));
            mdc.add(new BooleanMetadata("ModelDetails", "Autoselect k", model.getCrossValidate()));
            if (useCrossVal) {
            mdc.add(new BooleanMetadata("ModelDetails", "MSE used in autoselect", model.getMeanSquared() ));    
            }
            
            mdc.add(new TextMetadata("ModelDetails", "Distance weighting method", model.getDistanceWeighting().getSelectedTag().getReadable()));
            mdc.add(new TextMetadata("ModelDetails", "Neighbour search method", model.getNearestNeighbourSearchAlgorithm().getClass().getSimpleName() ));
            
            mdc.add(new NumericalMetadata("ModelDetails", "Number of x variables", xData.getColumnCount()));
            mdc.add(new TextMetadata("ModelDetails", "Modelled variable name", yCol.getName()));
            mdc.add(new NumericalMetadata("ModelDetails", "Number of training instances", wekaInstances.numInstances()));
            mdc.add(new BooleanMetadata("ModelDetails", "Scrambled modelled variable", yscramble));

            //data attributes 
            mdc.add(new TextMetadata("DataAttributes", "Weka training data summary", wekaInstances.toSummaryString() ));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanActual", wekaInstances.meanOrMode(modelledVar) ));
            mdc.add(new NumericalMetadata("DataAttributes", "TrainMeanPredicted", new MeanValueCalculator(predicted).doubleValue()));
            mdc.add(new BooleanMetadata("DataAttributes", "TrainingData", true));

            addMetadataToAllOutputs(mdc);

            //Save text report
            File report = new File(getWorkingDirectory(), getEditableProperties().stringValue("ReportName", "KNN_Data.txt"));
            ZipUtils.writeSingleLineFile(report, model.toString());
            FileWrapper reportWrapper = new FileWrapper(getWorkingDirectory());
            reportWrapper.addFile(report, false);
           
            //Save ARFF report
            File arffFile = new File(getWorkingDirectory(), "WekaTrainingDataSummary.txt");
            ZipUtils.writeSingleLineFile(arffFile,wekaInstances.toSummaryString());
            reportWrapper.addFile(arffFile, false);
           
            setOutputData("report", reportWrapper);
            

        } else {
            throw new DataException("Only a single column 'y' output can be modelled using this block.");
        }
    }

   
    @Override
    public void allDataProcessed() throws Exception {
    }
}