package uk.ac.ncl.eGenome.blocks;

/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import java.io.File;
import java.util.ArrayList;

import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import uk.ac.ncl.eSC.CommonTools;
import uk.ac.ncl.eSC.CommonTools.DumperThread;


public class GZip extends CloudDataProcessorService
{
    private static final String PROP_DECOMPRESS = "Decompress (-d)";
    private static final String PROP_DETAILS = "Compression details (-l)";
    //private static final String PROP_TEST = "Test (-t)";
    private static final String PROP_COMP_RATIO = "Speed-Compression ratio (-#)";
    private static final String PROP_IGNORE_ERRORS = "Ignore errors";

    //private static final String libraryName = "BWA-0.7";
    //private static final String commandName = "bwa";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        FileWrapper outputFiles = new FileWrapper();
        FileWrapper inputFiles = (FileWrapper) getInputData("input-files");
        if (inputFiles.getFileCount() < 1) {
            throw new Exception("Missing input files.");
        }

        ArrayList<String> args = new ArrayList<String>();

        // Main command line argument and mandatory options
        args.add("gzip");
        args.add("-c");

        // Process options
        boolean decompress = getProperties().booleanValue(PROP_DECOMPRESS, false); 
        if (decompress) {
            args.add("-d");
        }

        if (getProperties().booleanValue(PROP_DETAILS, false)) {
            args.add("-l");
        }

        //if (getProperties().booleanValue(PROP_TEST, false)) {
        //    args.add("-t");
        //}

        int ratio = getProperties().intValue(PROP_COMP_RATIO, 0);
        if (ratio < 1 || ratio > 9) {
            throw new Exception("Property '" + PROP_COMP_RATIO + "' must be in range 1–9");
        }

        boolean breakOnError = !getProperties().booleanValue(PROP_IGNORE_ERRORS, true);

        // Process input files and produce output

        // Prepare args array with an extra space for the input argument
        String[] argArray = args.toArray(new String[args.size() + 1]);
        int inputArg = args.size();
        
        for (File input : inputFiles) {
            argArray[inputArg] =  input.toString();
            System.out.println("The command line arguments: " + CommonTools.toString(", ", argArray));
            
            ProcessBuilder pb = new ProcessBuilder(argArray);
            File outputFile;
            if (decompress) {
                String[] fileExt = CommonTools.splitFileNameAndExtension(input.getName());
                if (fileExt[1].equalsIgnoreCase(".gz")) {
                    outputFile = createTempFile(fileExt[0], new File("."));
                } else {
                    outputFile = createTempFile(input.getName(), new File("."));
                }
            } else {
                outputFile = createTempFile(input.getName() + ".gz", new File("."));
            }

            //pb.redirectOutput(outputFile);
            //pb.redirectError(Redirect.INHERIT);

            // Start the subprocess... 
            final Process child = pb.start();

            // Add a shutdown hook to kill the child in the case the block has been terminated.
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override public void run() {
                    child.destroy();
                }
            });

            // Start threads that read child's std out and error streams.
            Thread stdOutTh = new DumperThread(child.getInputStream(), outputFile);
            Thread stdErrTh = new DumperThread(child.getErrorStream(), System.err, false);

            stdOutTh.start();
            stdErrTh.start();

            // Wait for child's result
            int exitCode = child.waitFor();
            
            // Wait for dumpers. This should be quick once the process has finished.
            stdOutTh.join();
            stdErrTh.join();

            if (exitCode != 0) {
                System.out.println("=====================================================");
                System.out.println("gzip exited with code = " + exitCode);
                if (breakOnError) {
                    throw new Exception("gzip failed; see output message for more details.");
                }
            } else {
                outputFiles.addFile(outputFile);
            }
        }

        setOutputData("output-files", outputFiles);
    }
}
