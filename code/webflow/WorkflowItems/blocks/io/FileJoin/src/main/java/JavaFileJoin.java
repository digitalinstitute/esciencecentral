
/**
 * e-Science Central Copyright (C) 2008-2014 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.io.DataImportException;

/**
 * Joins (concatenates) 2 files. The 2nd file is appended directly to the end of the first
 * file. Will work with textual and/or binary data. Probably.
 * 
 * @author Dominic Searson 20/6/14
 */
public class JavaFileJoin extends CloudDataProcessorService {

    @Override
    public void executionAboutToStart() throws Exception {
    }

    @Override
    public void execute() throws Exception {

        //get output file name from block
        String outputFileName = getEditableProperties().stringValue("OutputFileName", "joinedFile.txt");
        
        
        //get input file wrappers from block
        FileWrapper inputFileWrapper1 = (FileWrapper) getInputData("file-1");
        FileWrapper inputFileWrapper2 = (FileWrapper) getInputData("file-2");

        //get input file objects
        File inputFile1 = null, inputFile2 = null;

        if (inputFileWrapper1.getFileCount() == 1) {
            inputFile1 = inputFileWrapper1.getFile(0);
        } 

        if (inputFileWrapper2.getFileCount() == 1) {
            inputFile2 = inputFileWrapper2.getFile(0);
        }

        if (inputFile1 == null) {
            throw new DataImportException("Could not retrieve unique file 1 from wrapper (check there is only 1 file in file-1 wrapper)");
        }
        
        if (inputFile2 == null) {
            throw new DataImportException("Could not retrieve unique file 2 from wrapper (check there is only 1 file in file-2 wrapper)");
        }
        
        //create output file
        File outFile = new File(getWorkingDirectory(), outputFileName);

        //channels on files
        FileChannel sourceChannel1 = null;
        FileChannel sourceChannel2 = null;
        FileChannel destinationChannel = null;
        
        long bytesWritten1 = 0;
        long bytesWritten2 = 0;
        long totalBytesWritten;

        //File IO
        try {
            sourceChannel1 = new FileInputStream(inputFile1).getChannel();
            sourceChannel2 = new FileInputStream(inputFile2).getChannel();
            destinationChannel = new FileOutputStream(outFile).getChannel();

            
            long bytesFile1 = sourceChannel1.size();
            long bytesFile2 = sourceChannel2.size();
            long bytes2Write = bytesFile1 + bytesFile2;
             
            //read from file 1 to file 3
            while (bytesWritten1 < bytesFile1) {
            bytesWritten1 += destinationChannel.transferFrom(sourceChannel1, bytesWritten1, bytesFile1);
            }
            
            //read from file 2 to file 3
            while (bytesWritten2 < bytesFile2) {
            bytesWritten2 += destinationChannel.transferFrom(sourceChannel2, bytesWritten1+bytesWritten2, bytesFile2);
            }
            
            
           totalBytesWritten = bytesWritten1 + bytesWritten2;
           
           if (totalBytesWritten != bytes2Write) {
               System.err.println("Total bytes written to File 3 does not equal File 1 bytes + File 2 bytes.");
               throw new DataException("Total bytes written to File 3 does not equal File 1 bytes + File 2 bytes.");
           }
           
    
        } finally {
            if (sourceChannel1 != null) {
                sourceChannel1.close();
            }

            if (sourceChannel2 != null) {
                sourceChannel2.close();
            }

            if (destinationChannel != null) {
                destinationChannel.close();
            }
        }

        System.out.println("Bytes read from file 1: " + bytesWritten1);
        System.out.println("Bytes read from file 2: " + bytesWritten2);
        System.out.println("Total bytes written to file 3: " + totalBytesWritten);
        
        //Attach output file to block output
        FileWrapper outputFileWrapper = new FileWrapper(getWorkingDirectory());
        outputFileWrapper.addFile(outFile, false);
        setOutputData("file-3", outputFileWrapper);
    }

    @Override
    public void allDataProcessed() throws Exception {
    }
}