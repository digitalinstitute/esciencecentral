
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.io.DelimitedTextDataImporter;
import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.manipulation.BestGuessDataTyper;

import java.io.File;
import java.io.FileInputStream;
import java.util.Enumeration;


/** E-sc block to parse CSV data from a workflow datawrapper
 *
 * @author modified by Dom 7/2/2014
 */
public class ParseCSV extends CloudDataProcessorService {

    private static final String Prop_LABEL_ROW = "LabelRow";
    private static final String Prop_CONTAINS_LABELS = "ContainsLabels";
    private static final String Prop_DELIMITER = "Delimiter";
    private static final String Prop_FAIL_IF_NOT_MATCH = "FailIfNotMatch";
    private static final String Prop_SHUFFLE_COLUMNS_BY_NAME = "ShuffleColumnsByName";
    private static final String Prop_START_ROW = "StartRow";
    private static final String Prop_END_ROW = "EndRow";
    private static final String Prop_LIMIT_ROWS = "LimitRows";
    private static final String Prop_SUBSAMPLE = "Subsample";
    private static final String Prop_SAMPLE_INTERVAL = "SampleInterval";
    private static final String Prop_TEXT_ONLY = "TextOnly";
    private static final String Prop_IMPORT_CHUNK_SIZE = "ImportChunkSize";
    
    private static final String Input_INPUT_FILES = "input-files";
    private static final String Output_IMPORTED_DATA = "imported-data";


    @Override
    public void execute() throws Exception
    {
        // Load the data from the organisation data store
        DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();

        // Set the properties
        importer.setDataStartRow(getProperties().intValue(Prop_START_ROW, 2));
        importer.setDataEndRow(getProperties().intValue(Prop_END_ROW, 1));
        importer.setDelimiterString(getProperties().stringValue(Prop_DELIMITER, ","));
        importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
        importer.setErrorsAsMissing(true);
        importer.setImportColumnNames(getProperties().booleanValue(Prop_CONTAINS_LABELS, true));
        importer.setLimitRows(getProperties().booleanValue(Prop_LIMIT_ROWS, false));
        importer.setNameRow(getProperties().intValue(Prop_LABEL_ROW, 1));
        importer.setSampleInterval(getProperties().intValue(Prop_SAMPLE_INTERVAL, 1));
        importer.setSubsample(getProperties().booleanValue(Prop_SUBSAMPLE, false));
        importer.setUseEnclosingQuotes(true);
        importer.setChunkSize(getProperties().intValue(Prop_IMPORT_CHUNK_SIZE, 5000));

        FileWrapper inputFiles = (FileWrapper) getInputData(Input_INPUT_FILES);

        // Text data depends on Prop_TEXT_ONLY but also whether or not SHUFFLE and FAIL has been set.  
        boolean textData = getProperties().booleanValue(Prop_TEXT_ONLY, false);

        // Select the shuffler that matches the user settings
        ColumnShuffler shuffler;
        if (getProperties().booleanValue(Prop_SHUFFLE_COLUMNS_BY_NAME, false)) {
            if (getProperties().booleanValue(Prop_FAIL_IF_NOT_MATCH, false)) {
                shuffler = new StrictShuffler();
            } else {
                shuffler = new LooseShuffler();
            }
        } else {
            if (getProperties().booleanValue(Prop_FAIL_IF_NOT_MATCH, false)) {
                shuffler = new StrictMatcher();
            } else {
                textData = true;
                shuffler = new LooseMatcher();
            }
        }

        // This setting may be forced by the use of !SHUFFLE_COLMNS && !FAIL.
        // Otherwise, it's likely that an invalid data set is produced.
        importer.setForceTextImport(textData);

        Data dataHeader = null;
        if (textData) {
            for (File f : inputFiles) {
                dataHeader = _appendCSV_TextOnly(f, importer, shuffler, dataHeader);
            }
        } else {
            for (File f : inputFiles) {
                dataHeader = _appendCSV_Guess(f, importer, shuffler, dataHeader);
            }
        }
    }

    private Data _appendCSV_TextOnly(File inputFile, DelimitedTextDataStreamImporter importer, ColumnShuffler shuffler, Data outputDataHeader)
    throws Exception
    {
        try (FileInputStream inStream = new FileInputStream(inputFile)) {
            importer.resetWithInputStream(inStream);

            int chunkCount = 0;
            int rowCount = 0;
            Data data = null;
            String dataName = inputFile.getName();

            if (outputDataHeader == null) {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    data.createEmptyProperties();
                    data.setName(dataName);
                    setOutputDataSet(Output_IMPORTED_DATA, data);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
                if (data != null) {
                    outputDataHeader = data.getEmptyCopy();
                }
            } else {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    data.createEmptyProperties();
                    data.setName(outputDataHeader.getName());
                    shuffler.shuffle(outputDataHeader, data);
                    setOutputDataSet(Output_IMPORTED_DATA, data);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
                outputDataHeader.setName(outputDataHeader.getName() + " + " + dataName);
            }

            System.out.println(String.format("File %s imported: %d rows of data in %d chunk(s)", dataName, rowCount, chunkCount));
            return outputDataHeader;
        } finally {
            importer.terminateRead();
        }
    }


    private Data _appendCSV_Guess(File inputFile, DelimitedTextDataStreamImporter importer, ColumnShuffler shuffler, Data outputDataHeader)
    throws Exception
    {
        try (FileInputStream inStream = new FileInputStream(inputFile)) {
            importer.resetWithInputStream(inStream);

            int chunkCount = 0;
            int rowCount = 0;
            BestGuessDataTyper guesser;
            Data data = null;
            Data guessedData = null;
            String dataName = inputFile.getName();

            if (outputDataHeader == null) {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    guesser = new BestGuessDataTyper(data);
                    guessedData = guesser.guess();
                    guessedData.createEmptyProperties();
                    guessedData.setName(dataName);
                    setOutputDataSet(Output_IMPORTED_DATA, guessedData);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
                if (guessedData != null) {
                    outputDataHeader = guessedData.getEmptyCopy();
                }
            } else {
                while (!importer.isFinished()) {
                    data = importer.importNextChunk();
                    guesser = new BestGuessDataTyper(data);
                    guessedData = guesser.guess();
                    guessedData.createEmptyProperties();
                    guessedData.setName(dataName);
                    shuffler.shuffle(outputDataHeader, guessedData);
                    setOutputDataSet(Output_IMPORTED_DATA, guessedData);
                    chunkCount++;
                    rowCount = rowCount + data.getLargestRows();
                }
                outputDataHeader.setName(outputDataHeader.getName() + " + " + dataName);
            }

            System.out.println(String.format("File %s imported: %d rows of data in %d chunk(s)", dataName, rowCount, chunkCount));
            return outputDataHeader;
        } finally {
            importer.terminateRead();
        }
    }


    private interface ColumnShuffler
    {
        void shuffle(Data header, Data body) throws DataException;
    }


    private int getColumnIndex(Data data, String columnName, int fromIndex)
    throws DataException
    {
        // This is a standard case
        if (fromIndex <= 0) {
            return data.getColumnIndex(columnName);
        }

        Enumeration<?> cols = data.columns();
        // Move to startFrom index ...
        int c = 0;
        while (cols.hasMoreElements() && c < fromIndex) {
            cols.nextElement();
            c++;
        }

        // and then search for the column name
        while (cols.hasMoreElements()) {
            Column col = (Column)cols.nextElement();
            if (col.getName().equals(columnName)) {
                return c;
            }
            c++;
        }

        // Nothing found
        return -1;
    }


    /**
     * This class will try to shuffle data according to the given header
     * and will report error in the case of any mismatch between columns.
     * 
     * @author njc97
     */
    private class StrictShuffler implements ColumnShuffler
    {
        @Override
        public void shuffle(Data header, Data data)
        throws DataException
        {
            int hCols = header.getColumnCount();
            if (hCols != data.getColumnCount()) {
                throw new DataException(String.format("Column counts do not match; expected: %d, actual: %d", hCols, data.getColumnCount()));
            }
            for (int c = 0; c < hCols; c++) {
                int b = getColumnIndex(data, header.column(c).getName(), c);
                if (b == -1) {
                    throw new DataException("Missing column: " + header.column(c).getName());
                }
                if (b != c) {
                    data.swapColumns(b, c);
                }
            }
        }
    }


    /**
     * This is the most difficult case for the block.
     * 
     * This class will try best to shuffle data according to the given header
     * but it will not report errors if the matching cannot be done perfectly.
     * If header and data have different number of columns, LooseShuffler
     * will try to fix it by adding empty columns.
     *  
     * @author njc97
     */
    private class LooseShuffler implements ColumnShuffler
    {
        @Override
        public void shuffle(Data header, Data data)
        throws DataException
        {
            int hCols = header.getColumnCount();
            Column[] matched = new Column[hCols];
            // Extract columns that match the header
            for (int c = 0; c < hCols; c++) {
                int d = data.getColumnIndex(header.column(c).getName());
                if (d != -1) {
                    matched[c] = data.column(d);
                    data.removeColumn(d);
                }
            }
            // And put them in the right places
            for (int c = 0; c < hCols; c++) {
                if (matched[c] != null) {
                    while (c > data.getColumnCount()) {
                        data.addColumn(header.column(c).getEmptyCopy());
                    }
                    data.insertColumn(c, matched[c]);
                }
            }
            // Also, add any extra columns from data to the header.
            int dCols = data.getColumnCount();
            for (int c = hCols; c < dCols; c++) {
                header.addColumn(data.column(c).getEmptyCopy());
            }
        }
    }


    private boolean isColumnMatch(Column c1, Column c2)
    {
        if (c1 == null && c2 == null) {
            return true;
        }
        if (c1 == null || c2 == null) {
            return false;
        }

        if (c1.getName() == null && c2.getName() == null) {
            return true;
        }
        if (c1.getName() == null || c2.getName() == null) {
            return false;
        }

        return c1.getName().equals(c2.getName());
    }


    /**
     * This class will not shuffle columns but will check whether data columns
     * match the given header; it throws an exception if they don't.
     * 
     */
    private class StrictMatcher implements ColumnShuffler
    {
        @Override
        public void shuffle(Data header, Data data) throws DataException
        {
            int hCols = header.getColumnCount();
            if (hCols != data.getColumnCount()) {
                throw new DataException(
                        String.format("Data doesn't match the header; columns expected: %d, columns found: %d", hCols, data.getColumnCount()));
            }

            for (int c = 0; c < hCols; c++) {
                if (!isColumnMatch(header.column(c), data.column(c))) {
                    throw new DataException(
                            String.format("Data doesn't match the header; column #%d expected to be: %s but found %s", c, header.column(c).getName(), data.column(c).getName()));
                }
            }
        }
    }


    /**
     * This class will do nothing except adding missing columns to the body or the header.
     * It's just a case when user selected "Don't shuffle columns" and "Don't fail if columns don't match"  
     * @author njc97
     *
     */
    private class LooseMatcher implements ColumnShuffler
    {
        @Override
        public void shuffle(Data header, Data data) throws DataException
        {
            int hCols = header.getColumnCount();
            int dCols = data.getColumnCount();
            
            if (hCols > dCols) {
                for (int c = dCols; c < hCols; c++) {
                    data.addColumn(header.column(c).getEmptyCopy());
                }
            } else if (hCols < dCols) {
                for (int c = hCols; c < dCols; c++) {
                    header.addColumn(data.column(c).getEmptyCopy());
                }
            }
        }
    }
}
