/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.workflow.util.ZipUtils;

import java.io.File;


public class MyService extends CloudDataProcessorService
{
    /** 
     * This is the main service execution routine. It is called once if 
     * the service has not been configured to accept streaming data or once for
     * each chunk of data if the service has been configured to accept data 
     * streams 
     */
    public void execute() throws Exception {
        FileWrapper topFiles = (FileWrapper)this.getInputData("top-list");
        FileWrapper bottomFiles = (FileWrapper)this.getInputData("bottom-list");
        FileWrapper joinedList;

        if (topFiles.getBaseDir().equals(bottomFiles.getBaseDir())) {
            // This is simple case, so we can literally merge two lists
            joinedList = new FileWrapper(topFiles.getBaseDir());
            for (File file : topFiles.relativeFiles()) {
                joinedList.addFile(file);
            }
            for (File file : bottomFiles.relativeFiles()) {
                joinedList.addFile(file);
            }
        } else {
            // In this case all files must be copied to a common directory
            File tmpDir = createTempDir("FileListJoin-", "", new File("."));

            // [TODO] In Linux, consider using Java 7 and symlinks instead of
            //        actual copying.
            //ZipUtils.copyDirTree(topFiles.getBaseDir(), tmpDir);
            for (File file : topFiles.relativeFiles()) {
                String fileParent = file.getParent();
                File targetDir;
                if (fileParent != null) {
                    targetDir = new File(tmpDir, fileParent);
                    targetDir.mkdirs();
                } else {
                    targetDir = tmpDir;
                }
                ZipUtils.copyFileToDirectory(
                        new File(topFiles.getBaseDir(), file.getPath()), targetDir);
            }

            //ZipUtils.copyDirTree(bottomFiles.getBaseDir(), tmpDir);
            for (File file : bottomFiles.relativeFiles()) {
                String fileParent = file.getParent();
                File targetDir;
                if (fileParent != null) {
                    targetDir = new File(tmpDir, fileParent);
                    targetDir.mkdirs();
                } else {
                    targetDir = tmpDir;
                }
                ZipUtils.copyFileToDirectory(
                        new File(bottomFiles.getBaseDir(), file.getPath()), targetDir);
            }

            joinedList = new FileWrapper(tmpDir);
            joinedList.addDir(tmpDir, false);
        }

        setOutputData("combined-list", joinedList);
    }
}