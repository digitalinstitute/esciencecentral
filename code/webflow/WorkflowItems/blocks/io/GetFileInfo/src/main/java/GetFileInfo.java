/**
 * e-Science Central Copyright (C) 2008-2017 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import java.io.File;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;

import com.connexience.server.workflow.*;

import com.connexience.server.workflow.engine.datatypes.DocumentRecordLinkItem;
import com.connexience.server.workflow.engine.datatypes.FolderLinkItem;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;


public class GetFileInfo implements WorkflowBlock
{
    private static final String Prop_GET_FILE_NAME = "Get Name";
    private static final String Prop_GET_FILE_SIZE = "Get Size";
    private static final String Prop_GET_CTIME = "Get Creation Time";
    private static final String Prop_GET_MTIME = "Get Last Modification Time";

    private static final String _ColName_NAME = "Name";
    private static final String _ColName_SIZE = "Size";
    private static final String _ColName_CTIME = "Creation Time";
    private static final String _ColName_MTIME = "Modification Time";

    //private static final String Prop_FAIL_NON_FILE = "Fail If Not File Reference";
    //private static final String Prop_FAIL_NON_FOLDER = "Fail If Not Folder Reference";
    
    //private static final String Prop_INCLUDE_FILES = "Include Files";
    //private static final String Prop_INCLUDE_FOLDERS = "Include Folders";

    private static final String Input_FILES = "files";

    private static final String Output_FILES_INFO = "files-info";

    public void preExecute(BlockEnvironment env) throws Exception
    {}

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        //boolean failOnNonFiles = getProperties().booleanValue(Prop_FAIL_NON_FILE, false);
        //boolean failOnNonFolders = getProperties().booleanValue(Prop_FAIL_NON_FOLDER, false);
        //boolean includeFiles = getProperties().booleanValue(Prop_INCLUDE_FILES, false);
        //boolean includeFolders = getProperties().booleanValue(Prop_INCLUDE_FOLDERS, false);

        Data outputData = new Data();
        StringColumn fileNames = null;
        IntegerColumn fileSizes = null;
        IntegerColumn versions = null;
        StringColumn docIds = null;
        StringColumn comments = null;
        DateColumn cTimes = null;
        DateColumn mTimes = null;

        if (env.getBooleanProperty(Prop_GET_FILE_NAME, false)) {
            fileNames = new StringColumn(_ColName_NAME);
            outputData.addColumn(fileNames);
        }
        if (env.getBooleanProperty(Prop_GET_FILE_SIZE, false)) {
            fileSizes = new IntegerColumn(_ColName_SIZE);
            outputData.addColumn(fileSizes);
        }
        if (env.getBooleanProperty(Prop_GET_CTIME, false)) {
            cTimes = new DateColumn(_ColName_CTIME);
            outputData.addColumn(cTimes);
        }
        if (env.getBooleanProperty(Prop_GET_MTIME, false)) {
            mTimes = new DateColumn(_ColName_MTIME);
            outputData.addColumn(mTimes);
        }

        for (File f : inputs.getInputFiles(Input_FILES)) {
            if (fileNames != null) {
                fileNames.appendStringValue(f.getName());
            }
            if (fileSizes != null) { 
                fileSizes.appendLongValue(f.length());
            }
            if (mTimes != null) {
                mTimes.appendDateValue(new Date(f.lastModified()));
            }
            if (cTimes != null) {
                BasicFileAttributes attr = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
                cTimes.appendDateValue(new Date(attr.creationTime().toMillis()));
            }
        }

        outputs.setOutputDataSet(Output_FILES_INFO, outputData);
    }

    public void postExecute(BlockEnvironment env) throws Exception
    {}
}