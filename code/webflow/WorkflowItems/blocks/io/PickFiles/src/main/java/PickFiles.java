/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import java.io.File;
import java.util.regex.Pattern;


public class PickFiles extends CloudDataProcessorService
{
    private static final String Prop_FILE_NAME_OR_INDEX = "FileNameOrIndex";
    private static final String Prop_PICK_BY_INDEX = "PickByIndex";
    private static final String Prop_FAIL_IF_EMPTY = "FailIfEmpty";
    private static final String Prop_SIMPLE_MATCH = "SimpleMatching";

    private static final String Input_INPUT_FILES = "input-files";
    
    private static final String Output_SELECTED_FILES = "selected-files";
    private static final String Output_REMAINING_FILES = "remaining-files";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        boolean simpleMatching = getProperties().booleanValue(Prop_SIMPLE_MATCH, false);
        String pattern = getEditableProperties().stringValue(Prop_FILE_NAME_OR_INDEX, "");
        if ("".equals(pattern)) {
            throw new Exception("Property " + Prop_FILE_NAME_OR_INDEX + " has not been set.");
        }

        FileWrapper files = (FileWrapper)getInputData(Input_INPUT_FILES);
        FileWrapper selectedFile = new FileWrapper(getWorkingDirectory());
        FileWrapper remainingFiles = new FileWrapper(getWorkingDirectory());

        if(getProperties().booleanValue(Prop_PICK_BY_INDEX, false)) {
            int index = Integer.parseInt(pattern);
            System.out.println("Selecting by index: " + index);
            if(index >= 0 && index < files.getFileCount()) {
                // Add the file selected by index
                selectedFile.addFile(files.getFile(index).getAbsolutePath(), false);

                // Add all the others
                for (int i = 0; i < index; i++) {
                    remainingFiles.addFile(files.getFile(i).getAbsolutePath(), false);
                }
                for (int i = index + 1; i < files.getFileCount(); i++) {
                    remainingFiles.addFile(files.getFile(i).getAbsolutePath(), false);
                }
            }
        } else {
            System.out.println("Selecting by name pattern: " + pattern);
            Pattern regEx = WildcardUtils.createRegexFromGlob(pattern);

            for (File file : files) {
                if (simpleMatching) {
                    System.out.println("Using simple matching - non Glob.");
                    if (simpleMatch(file.getName(), pattern)) {
                        System.out.println("Selecting: " + file);
                        selectedFile.addFile(file.getAbsolutePath(), false);
                    } else {
                        System.out.println("Not selecting: " + file);
                        remainingFiles.addFile(file.getAbsolutePath(), false);
                    }
                } else {
                    if (regEx.matcher(file.getPath()).matches()) {
                        System.out.println("Selecting: " + file);
                        selectedFile.addFile(file.getAbsolutePath(), false);
                    } else {
                        System.out.println("Not selecting: " + file);
                        remainingFiles.addFile(file.getAbsolutePath(), false);
                    }
                }
            }
        }

        if (!getProperties().booleanValue(Prop_FAIL_IF_EMPTY, false) || selectedFile.getFileCount() > 0) {
            setOutputData(Output_SELECTED_FILES, selectedFile);
            setOutputData(Output_REMAINING_FILES, remainingFiles);
        } else {
            System.err.println("Usage to select files in subdirectories: */sub/dir/file.txt");
            throw new Exception("No files '" + pattern + "' present in the input files.");
        }
    }

    public static boolean simpleMatch(String text, String pattern) {
        if (text.contains(pattern)) {
            return true;
        }

        // Create the cards by splitting using a RegEx. If more speed
        // is desired, a simpler character based splitting can be done.
        String[] cards = pattern.split("\\*");

        // Iterate over the cards.
        for (String card : cards) {
            int idx = text.indexOf(card);

            // Card not detected in the text.
            if (idx == -1) {
                return false;
            }

            // Move ahead, towards the right of the text.
            text = text.substring(idx + card.length());
        }

        return true;
    }
}