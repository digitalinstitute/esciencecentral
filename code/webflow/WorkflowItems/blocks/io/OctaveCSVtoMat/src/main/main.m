[r c] = size(csvfile.files);
segmentedFiles.files = [];
useSubDir = properties.UseSubDir;
subDir = properties.SubDirectoryName;
if(useSubDir == true)
	if ~exist(subDir, 'dir')
	  mkdir(subDir);
	end
end

for i=1:r
	[pathstr,name,ext] = fileparts(csvfile.files(i,:));
        vn = strcat('ACCdata_', num2str(i),'_n')
        c = strcat(vn, ' = csvread(deblank(csvfile.files(i,:)));')
        disp(c)
	eval(c);
	whos
	size(vn)

	if(useSubDir == true)
		eval(['save -' properties.MatFileVersion ' ' subDir filesep name '.mat ' vn]);
		%eval(['segmentedFiles.files = [segmentedFiles.files ; ''' subDir filesep name '.mat''];']);
		if(i==1)
			segmentedFiles.files = strcat(subDir, filesep, name, '.mat');
		else
			segmentedFiles.files = [segmentedFiles.files ; strcat(subDir, filesep, name, '.mat')];
		end

	else
		eval(['save -' properties.MatFileVersion ' ' name '.mat ' vn]);
		if(i==1)
			segmentedFiles.files = strcat(name, '.mat');
		else
			segmentedFiles.files = [segmentedFiles.files ; strcat(name, '.mat')];
		end

	end
endfor
