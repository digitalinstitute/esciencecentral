
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;
import com.jmatio.common.*;
import com.jmatio.types.*;
import com.jmatio.io.*;
import java.io.File;
import java.util.ArrayList;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.util.DataToMatrixConverter;

public class MatlabFileWriter extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        try {
            Data inputData = getInputDataSet("input-data");
            double[][]data = new DataToMatrixConverter(inputData).toDoubleArray();
            
            // Extract the numerical data
            String prefix = getEditableProperties().stringValue("MatrixPrefix", "ESC_");
            MLDouble matlabArray = new MLDouble(prefix + "dbl", data);

            // Get any text columns
            ArrayList dataList = new ArrayList();
            for(int i=0;i<inputData.getColumnCount();i++){
                if(inputData.column(i) instanceof StringColumn){
                    // Text data
                    StringColumn c = (StringColumn)inputData.column(i);
                    String[] cd = new String[c.getRows()];
                    for(int j=0;j<c.getRows();j++){
                        if(!c.isMissing(j)){
                            cd[j] = c.getStringValue(j);
                        } else {
                            cd[j] = "";
                        }
                    }
                    MLChar charCol;
                    if(c.getName() != null && !c.getName().equals("")){
                        charCol = new MLChar(c.getName(), cd);
                    }else {
                        charCol = new MLChar(prefix + "str_" + (i + 1), cd);
                    }
                    dataList.add(charCol);
                    
                } else if(inputData.column(i) instanceof DateColumn){
                    // Timestamps
                    DateColumn d = (DateColumn)inputData.column(i);
                    long[] dd = new long[d.getRows()];
                    for(int j=0;j<d.getRows();j++){
                        if(!d.isMissing(j)){
                            dd[j] = d.dateValue(j).getTime();
                        } else {
                            dd[j] = 0;
                        }
                    }
                    MLInt64 dateCol;
                    if(d.getName() != null && !d.getName().equals("")) {
                        dateCol = new MLInt64(d.getName(), dd, 1);
                    }else{
                        dateCol = new MLInt64(prefix + "date_" + (i+1), dd, 1);
                    }
                    dataList.add(dateCol);
                }
            }
            
            File outputFile = new File(getWorkingDirectory(), getEditableProperties().stringValue("OutputFileName", "data.mat"));
            
            dataList.add(matlabArray);
            MatFileWriter writer = new MatFileWriter();

            writer.write(outputFile, dataList);
            FileWrapper wrapper = new FileWrapper(getWorkingDirectory());
            wrapper.addFile(outputFile, false);
            setOutputData("mat-file", wrapper);
        } catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}