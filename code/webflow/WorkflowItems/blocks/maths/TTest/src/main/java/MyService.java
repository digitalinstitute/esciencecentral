/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.manipulation.*;
import java.util.*;
import org.apache.commons.math3.stat.inference.TestUtils;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        // This is the API link that can be used to access data in the
        // Inkspot platform. It is configured using the identity of
        // the user executing the service

        double significanceLevel = getEditableProperties().doubleValue("SignificanceLevel", 0.05);
        
        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data sample1 = getInputDataSet("input-1");
        Data sample2 = getInputDataSet("input-2");
        
        if(sample1.getNumericalColumnCount()==sample2.getNumericalColumnCount()){
            Data tStatistics = new Data();
            Data pValues = new Data();
            Data columnMatches = new Data();
           
            Vector sample1Columns = new NumericalColumnExtractor(sample1).extractColumns();
            Vector sample2Columns = new NumericalColumnExtractor(sample2).extractColumns();
            
            NumericalColumn c1;
            NumericalColumn c2;
            
            for(int i=0;i<sample1Columns.size();i++){
                c1 = (NumericalColumn)sample1Columns.get(i);
                c2 = (NumericalColumn)sample2Columns.get(i);
                tStatistics.addSingleValue(c1.getName(), TestUtils.pairedT(c1.getDoubleArray(), c2.getDoubleArray()));
                pValues.addSingleValue(c1.getName(), TestUtils.pairedTTest(c1.getDoubleArray(), c2.getDoubleArray()));
                
                if(TestUtils.pairedTTest(c1.getDoubleArray(), c2.getDoubleArray(), significanceLevel)){
                    columnMatches.addSingleValue(c1.getName(), 0);
                } else {
                    columnMatches.addSingleValue(c1.getName(), 1);
                }
            }
            
            setOutputDataSet("t-statistics", tStatistics);
            setOutputDataSet("p-values", pValues);
            setOutputDataSet("column-matches", columnMatches);
           
            
        } else {
            throw new Exception("Number of numerical columns in the two data sets do not match");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}