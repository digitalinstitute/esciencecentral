
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.PropertiesWrapper;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.ColumnPicker;

public class DataToProperties extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Data inputData = getInputDataSet("input-data");

        ColumnPicker namesPicker = new ColumnPicker(getEditableProperties().stringValue("NamesColumn", "#0"));
        ColumnPicker valuesPicker = new ColumnPicker(getEditableProperties().stringValue("ValuesColumn", "#1"));
        
        Column namesColumn = namesPicker.pickColumn(inputData);
        Column valuesColumn = valuesPicker.pickColumn(inputData);
        
        if(namesColumn.getRows()==valuesColumn.getRows()){
            String name;
            String value;
            PropertiesWrapper properties = new PropertiesWrapper();
            
            for(int i=0;i<namesColumn.getRows();i++){
                if(!namesColumn.isMissing(i) && !valuesColumn.isMissing(i)){
                    name = namesColumn.getStringValue(i);
                    value = valuesColumn.getStringValue(i);
                    properties.properties().add(name, value);
                }
            }
            
            setOutputData("properties", properties);
        } else {
            throw new Exception("Names column size does not match values column size");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}