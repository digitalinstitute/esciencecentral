/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.amazonaws.services.cloudformation.model.Output;
import com.connexience.server.workflow.cloud.services.*;

import org.omg.CORBA._PolicyStub;
import org.pipeline.core.data.*;

public class MyService extends CloudDataProcessorService
{
    private static final String Output_JOINED_DATA = "joined-data";

    Data _middleDataCache;
    Data _lowerDataCache;

    /** This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams */
    public void execute() throws Exception
    {
        Data upperData = getInputDataSet("upper-data");
        Data middleData = getInputDataSet("middle-data");
        Data lowerData = getInputDataSet("lower-data");

        if (upperData != null) {
            setOutputDataSet(Output_JOINED_DATA, upperData);

            // All ports are streamed, so we need to cache middle and lower data until upper data is null/empty
            if (middleData != null) {
                if (_middleDataCache == null) {
                    middleData.setReadOnly(false);
                    _middleDataCache = middleData;
                } else {
                    _middleDataCache.appendRows(middleData, true);
                }
            }

            if (lowerData != null) {
                if (_lowerDataCache == null) {
                    lowerData.setReadOnly(false);
                    _lowerDataCache = lowerData;
                } else {
                    _lowerDataCache.appendRows(lowerData, true);
                }
            }
        } else if (middleData != null) {
            if (_middleDataCache != null) {
                _middleDataCache.appendRows(middleData, true);
                setOutputDataSet(Output_JOINED_DATA, _middleDataCache);
                // Clear cache to free memory
                _middleDataCache = null;
            } else {
                setOutputDataSet(Output_JOINED_DATA, middleData);
            }

            // All ports are streamed, so we need to cache lower data until middle data is null/empty
            if (lowerData != null) {
                if (_lowerDataCache == null) {
                    lowerData.setReadOnly(false);
                    _lowerDataCache = lowerData;
                } else {
                    _lowerDataCache.appendRows(lowerData, true);
                }
            }
        } else if (lowerData != null) {
            if (_middleDataCache != null) {
                // Time to dump middle and lower cache to the output
                if (_lowerDataCache != null) {
                    _middleDataCache.appendRows(_lowerDataCache, true);
                    // Clear cache to free memory
                    _lowerDataCache = null;
                }

                _middleDataCache.appendRows(lowerData, true);
                setOutputDataSet(Output_JOINED_DATA, _middleDataCache);
                // Clear cache to free memory
                _middleDataCache = null;
            } else if (_lowerDataCache != null) {
                _lowerDataCache.appendRows(lowerData, true);
                setOutputDataSet(Output_JOINED_DATA, _lowerDataCache);
                // Clear cache to free memory
                _lowerDataCache = null;
            } else {
                setOutputDataSet(Output_JOINED_DATA, lowerData);
            }
        } else {
            throw new Exception("No data present");
        }
    }


    @Override
    public void allDataProcessed() throws Exception
    {
        // Make sure that the middle and lower caches are dumped to the output in the correct order
        if (_middleDataCache != null) {
            if (_lowerDataCache != null) {
                _middleDataCache.appendRows(_lowerDataCache, true);
            }
            setOutputDataSet(Output_JOINED_DATA, _middleDataCache);
        } else if (_lowerDataCache != null) {
            setOutputDataSet(Output_JOINED_DATA, _lowerDataCache);
        }
    }
}