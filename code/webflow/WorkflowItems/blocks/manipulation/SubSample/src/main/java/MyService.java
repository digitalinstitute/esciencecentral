

/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */



import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import org.pipeline.core.data.Data;
import org.pipeline.core.xmlstorage.XmlDataStore;

/** E-sc block to subsample a worflow datawrapper
 * 
 * @author hugo, modfied by Dominic 27/3/14 to propagate data properties
 */
public class MyService extends CloudDataProcessorService {
    
    /** Sample offset value if the last execution did not feat neatly into
     * a sampling interval. This is effectively the number of rows that
     * must be skipped until the next sample point */
    private int sampleOffset = 0;
    
    
    @Override
    public void executionAboutToStart() throws Exception {
        sampleOffset = 0;
    }

   
    @Override
    public void execute() throws Exception {
        Data input = getInputDataSet("input-data");
        int interval = getEditableProperties().intValue("Interval", 2);
        Data results = getEmptyInputDataSet("input-data");
        Data remains = getEmptyInputDataSet("input-data");
        
        //ensure properties from input data are propagated to both output data sets (dps)
        if (input.hasProperties()) {
            
            XmlDataStore inputProps = input.getProperties();
            results.getProperties().copyProperties(inputProps);
            remains.getProperties().copyProperties(inputProps);
         
        }
        
        
        int rows = input.getLargestRows();
        int count = sampleOffset;
        for(int i=0;i<rows;i++){
            if(count>=interval){
                // Sample this row
                results.appendRows(input.getRowSubset(i, i, true), true);
                count = 0;
            } else {
                remains.appendRows(input.getRowSubset(i, i, true), true);
            }
            count++;
        }
        sampleOffset = count;

        setOutputDataSet("subsampled-data", results);
        setOutputDataSet("remaining-data", remains);
    }

    
    @Override
    public void allDataProcessed() throws Exception {

    }
}