
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.util.ZipUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.JsonDataImporter;
import org.pipeline.core.data.manipulation.BestGuessDataTyper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class JSONToData implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {

    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
       /*
        Most of this code comes from the QueryDataset block
        */

        List<File> inputFiles = inputs.getInputFiles("input-files");

        JSONArray dataArray = null;
        try {
            FileInputStream fis = new FileInputStream(inputFiles.get(0));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipUtils.copyInputStream(fis, baos);
            baos.flush();
            baos.close();
            fis.close();
            JSONTokener tokener = new JSONTokener(new String(baos.toByteArray()));

            dataArray = new JSONArray(tokener);
        } catch (JSONException e) {
            System.out.println("Input is not an Array, trying object");

            try {
                FileInputStream fis = new FileInputStream(inputFiles.get(0));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ZipUtils.copyInputStream(fis, baos);
                baos.flush();
                baos.close();
                fis.close();

                JSONTokener tokener = new JSONTokener(new String(baos.toByteArray()));
                JSONObject jsonObj = new JSONObject(tokener);

                dataArray = new JSONArray();
                dataArray.put(jsonObj);
            } catch (JSONException je) {
                System.out.println("Input is not a JSON Object");

            }
        }

        int rows = dataArray.length();
        Iterator keys;
        String stringValue;
        double doubleValue;
        Column col;
        String key;
        JSONObject rowJson;


        // Get the first row
        Data results = new Data();
        if (rows > 0) {
            // Get a sorted set of keys
            rowJson = dataArray.getJSONObject(0);
            keys = rowJson.keys();
            ArrayList<String> sortedKeys = new ArrayList<String>();
            while (keys.hasNext()) {
                sortedKeys.add(keys.next().toString());
            }
            Collections.sort(sortedKeys);
            keys = sortedKeys.iterator();

            // Create the columns with the correct names
            while (keys.hasNext()) {
                key = keys.next().toString();
                if (!key.startsWith("_")) {
                    stringValue = rowJson.getString(key);
                    try {
                        doubleValue = Double.parseDouble(stringValue);
                        col = new DoubleColumn(key);
                        ((DoubleColumn) col).appendDoubleValue(doubleValue);

                    } catch (Exception e) {
                        col = new StringColumn(key);
                        // Replace carriage returns
                        stringValue = stringValue.replace("\n", "");
                        stringValue = stringValue.replace("\r", "");
                        stringValue = stringValue.replace(",", "");

                        ((StringColumn) col).appendStringValue(stringValue);
                    }
                    results.addColumn(col);
                }
            }

            // Now work through the rest of the data
            if (rows > 1) {
                for (int i = 0; i < rows; i++) {
                    rowJson = dataArray.getJSONObject(i);
                    keys = rowJson.keys();
                    while (keys.hasNext()) {
                        key = keys.next().toString();
                        if (!key.startsWith("_")) {
                            if (results.containsColumn(key)) {
                                col = results.column(key);
                                stringValue = rowJson.getString(key);
                                
                                // Replace carriage returns
                                stringValue = stringValue.replace("\n", "");
                                stringValue = stringValue.replace("\r", "");
                                stringValue = stringValue.replace(",", "");

                                if (col instanceof NumericalColumn) {
                                    try {
                                        ((DoubleColumn) col).appendDoubleValue(Double.parseDouble(stringValue));
                                    } catch (Exception e) {
                                        col.appendObjectValue(MissingValue.get());
                                    }
                                } else {
                                    col.appendStringValue(stringValue);
                                }
                            }
                        }
                    }

                }
            }

            outputs.setOutputDataSet("output-data", results);
        } else {
            System.out.println("No data imported");
            outputs.setOutputDataSet("output-data", results);
        }


    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }


}