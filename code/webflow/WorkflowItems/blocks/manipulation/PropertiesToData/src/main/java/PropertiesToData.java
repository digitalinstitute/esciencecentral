/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.StringColumn;

public class PropertiesToData implements WorkflowBlock
{
    private final static String Prop_AS_ROWS = "As Rows";

    private final static String[] USER_PROPS = {
            "Property-0", "Property-1", "Property-2", "Property-3", "Property-4",
            "Property-5", "Property-6", "Property-7", "Property-8", "Property-9"
    };

    private final static String Output_PROPS_DATA = "properties-data";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        
    }

    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        Data outputData = new Data();

        if (env.getBooleanProperty(Prop_AS_ROWS, false)) {
            StringColumn col = new StringColumn("User-Property");

            for (String propName : USER_PROPS) {
                String propValue = env.getStringProperty(propName, "");
                if (!"".equals(propValue)) {
                    col.appendStringValue(propValue);
                }
            }

            outputData.addColumn(col);
        } else {
            for (String propName : USER_PROPS) {
                String propValue = env.getStringProperty(propName, "");
                if (!"".equals(propValue)) {
                    StringColumn col = new StringColumn(propName);
                    col.appendStringValue(propValue);
                    outputData.addColumn(col);
                }
            }
        }

        outputs.setOutputDataSet(Output_PROPS_DATA, outputData);
    }

    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        
    }
}
