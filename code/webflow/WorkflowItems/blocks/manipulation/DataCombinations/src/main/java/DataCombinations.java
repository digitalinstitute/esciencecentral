/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.*;
import org.pipeline.core.data.*;

public class DataCombinations implements WorkflowBlock
{
    private final static String Input_INPUT_1 = "input-1";
    private final static String Input_INPUT_2 = "input-2";
    private final static String Input_INPUT_3 = "input-3";

    private final static String Output_COMBINATIONS = "combinations";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        
    }

    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        Data outputData;

        Data input_1 = inputs.getInputDataSet(Input_INPUT_1);
        Data input_2 = inputs.getInputDataSet(Input_INPUT_2);
        Data input_3 = null;

        // Prepare the output data
        Data output = new Data();

        for (Column c : input_1) {
            output.addColumn(c.getEmptyCopy());
        }
        for (Column c : input_2) {
            output.addColumn(c.getEmptyCopy());
        }

        if (env.getExecutionService().isInputConnected(Input_INPUT_3)) {
            input_3 = inputs.getInputDataSet(Input_INPUT_3);

            for (Column c : input_3) {
                output.addColumn(c.getEmptyCopy());
            }

            _generateCombinations(input_1, input_2, input_3, output);
        } else {
            _generateCombinations(input_1, input_2, output);
        }

        outputs.setOutputDataSet(Output_COMBINATIONS, output);
    }
    
    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        
    }


    private void _generateCombinations(Data input1, Data input2, Data output)
    throws DataException
    {
        int rowCount1 = input1.getLargestRows();
        int rowCount2 = input2.getLargestRows();

        for (int r1 = 0; r1 < rowCount1; r1++) {
            for (int r2 = 0; r2 < rowCount2; r2++) {
                // FIXME: Handle missing data
                int c1;
                for (c1 = 0 ; c1 < input1.getColumns(); c1++) {
                    output.column(c1).appendObjectValue(input1.column(c1).getObjectValue(r1));
                }
                for (int c2 = 0 ; c2 < input2.getColumns(); c2++, c1++) {
                    output.column(c1).appendObjectValue(input2.column(c2).getObjectValue(r2));
                }
            }
        }
    }


    private void _generateCombinations(Data input1, Data input2, Data input3, Data output)
    throws DataException
    {
        int rowCount1 = input1.getLargestRows();
        int rowCount2 = input2.getLargestRows();
        int rowCount3 = input3.getLargestRows();

        for (int r1 = 0; r1 < rowCount1; r1++) {
            for (int r2 = 0; r2 < rowCount2; r2++) {
                for (int r3 = 0; r3 < rowCount3; r3++) {
                    // FIXME: Handle missing data
                    int c1;
                    for (c1 = 0; c1 < input1.getColumns(); c1++) {
                        output.column(c1).appendObjectValue(input1.column(c1).getObjectValue(r1));
                    }
                    for (int c2 = 0; c2 < input2.getColumns(); c2++, c1++) {
                        output.column(c1).appendObjectValue(input2.column(c2).getObjectValue(r2));
                    }
                    for (int c3 = 0; c3 < input3.getColumns(); c3++, c1++) {
                        output.column(c1).appendObjectValue(input3.column(c3).getObjectValue(r3));
                    }
                }
            }
        }
    }
}
