/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;

public class MyService extends CloudDataProcessorService
{
    private final static String Prop_EQUALISE = "Equalise Columns";
    private final static String Equalise_NO = "NO";
    private final static String Equalise_TRIM = "TRIM";
    private final static String Equalise_TRIM_ALL = "TRIM_ALL";
    private final static String Equalise_PAD_LAST = "PAD_LAST";
    private final static String Equalise_PAD_CYCLIC = "PAD_CYCLIC";

    private final static String Prop_IGNORE_EMPTY = "Ignore If Empty";

    private final static String Input_LEFT = "left-hand-data";
    private final static String Input_RIGHT = "right-hand-data";
    
    private final static String Output_JOIN = "joined-data";


    /** This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams */
    public void execute() throws Exception
    {
        boolean ignoreEmpty = getProperties().booleanValue(Prop_IGNORE_EMPTY, false);

        Data leftData = getInputDataSet(Input_LEFT);
        Data rightData = getInputDataSet(Input_RIGHT);
        Data joinedData;

        if (leftData != null && rightData != null) {
            // Both inputs present
            joinedData = leftData;
            joinedData.setReadOnly(false);
            joinData(joinedData, rightData, getProperties().stringValue(Prop_EQUALISE, Equalise_NO));
        } else if (leftData != null && rightData == null) {
            // Only left present
            joinedData = leftData;
            joinedData.setReadOnly(false);
            if (!ignoreEmpty) {
                joinedData.joinData(getEmptyInputDataSet(Input_RIGHT), true);
            }
        } else if (leftData == null && rightData != null) {
            // Only right present
            if (!ignoreEmpty) {
                joinedData = getEmptyInputDataSet(Input_LEFT);
                joinedData.joinData(rightData, true);
            } else {
                joinedData = rightData;
            }
        } else {
            // No input present
            if (ignoreEmpty) {
                joinedData = new Data();
            } else {
                throw new Exception("No input data present");
            }
        }

        setOutputDataSet(Output_JOIN, joinedData);
    }


    /**
     * Code based on {@link org.pipeline.core.data.Data#joinData(Data, boolean)}
     */

    private void joinData(Data base, Data dataToJoin, String dupKind)
    throws IndexOutOfBoundsException, DataException
    {
        if (Equalise_NO.equalsIgnoreCase(dupKind)) {
            base.setReadOnly(false);
            base.joinData(dataToJoin, true); 
        }
        else if (Equalise_PAD_LAST.equalsIgnoreCase(dupKind)) {
            int rows = Math.max(base.getLargestRows(), dataToJoin.getLargestRows());

            // Pad all columns in the base data
            int cols = base.getColumns();
            for (int i = 0 ; i < cols ; i++) {
                Column col = base.column(i);
                Object lastValue;

                if (col.getRows() > 0)
                    // TODO: consider if getObjectValue is enough or copyObjectValue should be used instead
                    lastValue = col.getObjectValue(col.getRows() - 1);
                else
                    lastValue = MissingValue.get();

                while (col.getRows() < rows)
                    col.appendObjectValue(lastValue);
            }

            // Pad all columns in dataToJoin and join them to the base
            cols = dataToJoin.getColumns();
            for (int i = 0 ; i < cols; i++) {
                // TODO: in the original code there is dataToJoin.column(i).getCopy(). Is this really necessary?
                Column col = dataToJoin.column(i);
                Object lastValue;

                if (col.getRows() > 0)
                    // TODO: consider if getObjectValue is enough or copyObjectValue should be used instead
                    lastValue = col.getObjectValue(col.getRows() - 1);
                else
                    lastValue = MissingValue.get();

                while (col.getRows() < rows)
                    col.appendObjectValue(lastValue);

                base.addColumn(col);
            }
        }
        else if (Equalise_PAD_CYCLIC.equalsIgnoreCase(dupKind)) {
            int rows = Math.max(base.getLargestRows(), dataToJoin.getLargestRows());

            // Pad all columns in the base data
            int cols = base.getColumns();
            for (int i = 0 ; i < cols ; i++) {
                // TODO: In the original code there is base.column(i).getCopy(). Is this really necessary?
                Column col = base.column(i);
                if (col.getRows() == 0) {
                    Object value = MissingValue.get();

                    while (col.getRows() < rows)
                        col.appendObjectValue(value);
                }
                else {
                    int r = 0;
                    while (col.getRows() < rows)
                        // TODO: consider if getObjectValue is enough or copyObjectValue should be used instead
                        col.appendObjectValue(col.getObjectValue(r++));
                }
            }

            // Pad all columns in dataToJoin and join them to the base
            cols = dataToJoin.getColumns();
            for (int i = 0 ; i < cols ; i++) {
                // TODO: In the original code there is base.column(i).getCopy(). Is this really necessary?
                Column col = dataToJoin.column(i);
                if (col.getRows() == 0) {
                    Object value = MissingValue.get();

                    while (col.getRows() < rows)
                        col.appendObjectValue(value);
                }
                else {
                    int r = 0;
                    while (col.getRows() < rows)
                        // TODO: consider if getObjectValue is enough or copyObjectValue should be used instead
                        col.appendObjectValue(col.getObjectValue(r++));
                }

                base.addColumn(col);
            }
        } else if (Equalise_TRIM.equalsIgnoreCase(dupKind)) {
            // Trim base or dataToJoin, whichever is shorter
            if (base.getLargestRows() > dataToJoin.getLargestRows()) {
                base.trimToSize(dataToJoin.getLargestRows());
            } else if (base.getLargestRows() < dataToJoin.getLargestRows()) {
                dataToJoin.trimToSize(base.getLargestRows());
            }

            // Join to the base
            int cols = dataToJoin.getColumns();
            for (int i = 0; i < cols; i++) {
                base.addColumn(dataToJoin.column(i));
            }
        } else if (Equalise_TRIM_ALL.equalsIgnoreCase(dupKind)) {
            int minRows = Math.min(base.getSmallestRows(), dataToJoin.getSmallestRows());

            // Trim base if needed
            if (base.getLargestRows() > minRows) {
                base.trimToSize(minRows);
            }
            // Trim data to join if needed
            if (dataToJoin.getLargestRows() > minRows) {
                dataToJoin.trimToSize(minRows);
            }

            // Join to the base
            int cols = dataToJoin.getColumns();
            for (int i = 0; i < cols; i++) {
                base.addColumn(dataToJoin.column(i));
            }
        } else {
            throw new DataException("Unexpected duplication kind: " + dupKind);
        }
    }
}