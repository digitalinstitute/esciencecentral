/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 * <p/>
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.pipeline.core.data.*;

import java.io.File;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MergePDFs extends CloudDataProcessorService {

    public void executionAboutToStart() throws Exception {
    }


    public void execute() throws Exception {
        FileWrapper firstDoc = (FileWrapper) getInputData("first-document");
        FileWrapper otherDocs = (FileWrapper) getInputData("others");
        String outputFilename = getProperties().stringValue("Filename", "report.pdf");

        PDFMergerUtility ut = new PDFMergerUtility();
        if (isInputConnected("first-document")) {
            for (int i = 0; i < firstDoc.getFileCount(); i++) {
                ut.addSource(firstDoc.getFile(i));
            }
        }

        for (int i = 0; i < otherDocs.getFileCount(); i++) {
            ut.addSource(otherDocs.getFile(i));
        }

        ut.setDestinationFileName(outputFilename);
        ut.mergeDocuments();
        FileWrapper outputFiles = new FileWrapper(getWorkingDirectory());
        outputFiles.addFile(new File(outputFilename), true);
        setOutputData("report", outputFiles);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}