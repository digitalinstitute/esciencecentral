/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.*;

public class MyService extends CloudDataProcessorService {
    /** Library data set */
    private Data libraryData = null;
    private int rowCount = 0;
    
    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {
        libraryData = null;
    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        boolean padMissing = getEditableProperties().booleanValue("PadMissing", true);
        String inputIndex = getEditableProperties().stringValue("InputIDColumn", "");
        String libraryIndex = getEditableProperties().stringValue("LibraryIDColumn", "");
        
        Data inputData = getInputDataSet("input-data");

        if(libraryData==null){
            libraryData = getInputDataSet("library-data");
        }
        
        ColumnPicker inputIDPicker = new ColumnPicker(inputIndex);
        ColumnPicker libraryIDPicker = new ColumnPicker(libraryIndex);
        
        Column idCol = inputIDPicker.getColumnReference(inputData);
        Column libraryIDCol = libraryIDPicker.getColumnReference(libraryData);      
            
        // Create a new empty data set to hold the results
        Data outputData = inputData.getEmptyCopy();
        for(int i=0;i<libraryData.getColumns();i++){
            if(!libraryData.column(i).equals(libraryIDCol)){
                // Only add non-id columns
                outputData.addColumn(libraryData.column(i).getEmptyCopy());
            }
        }

        // Do matching           
        Data inputRow;
        Data libraryRow;
        String id;
        int count;

        if(idCol!=null && libraryIDCol!=null){
            int size = idCol.getRows();
            for(int i=0;i<size;i++){
                inputRow = inputData.getRowSubset(i, i, true);
                if(!idCol.isMissing(i)){
                    id = idCol.getStringValue(i);
                    libraryRow = findRow(id, libraryData, libraryIDCol);
                    if(libraryRow!=null){
                        count = 0;

                        // Copy input data
                        for(int j=0;j<inputData.getColumns();j++){
                            outputData.column(count).appendObjectValue(inputData.column(j).copyObjectValue(i));
                            count++;
                        }

                        // Copy secondary data
                        for(int j=0;j<libraryData.getColumns();j++){
                            if(!libraryData.column(j).equals(libraryIDCol)){
                                outputData.column(count).appendObjectValue(libraryRow.column(j).copyObjectValue(0));
                                count++;
                            }
                        }
                        
                        // Update the index column
                        if(outputData.hasIndexColumn()){
                            outputData.getIndexColumn().appendStringValue(Integer.toString(rowCount));
                            rowCount++;
                        }
                        
                    } else {
                        // Should missing values be added for non-matched rows
                        if(padMissing){
                            // Copy primary data
                            count = 0;
                            for(int j=0;j<inputData.getColumns();j++){
                                outputData.column(count).appendObjectValue(inputData.column(j).copyObjectValue(i));
                                count++;
                            }

                            // Pad remains
                            for(int j=count;j<outputData.getColumns();j++){
                                outputData.column(j).appendObjectValue(MissingValue.get());
                            }
                            
                            // Update the index column
                            if(outputData.hasIndexColumn()){
                                outputData.getIndexColumn().appendStringValue(Integer.toString(rowCount));
                                rowCount++;
                            }
                                                    
                        }
                    }
                }
            }        
        }
        
        setOutputDataSet("matched-data", outputData);
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
    
    /** Return a row of data from a data set that contains the specified id string  */
    private Data findRow(String id, Data searchData, Column idCol) {
        try {
            int size = idCol.getRows();
            int row = -1;
            for(int i=0;i<size;i++){
                if(!idCol.isMissing(i)){
                    if(idCol.getStringValue(i).equals(id)){
                        row = i;
                    }
                }
            }
            
            // Extract the data if found
            if(row!=-1){
                return searchData.getRowSubset(row, row, true);
            } else {
                return null;
            }
            
        } catch (Exception e){
            return null;
        }
    }    
}