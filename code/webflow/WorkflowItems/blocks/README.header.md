# Workflow blocks

The list below includes references to all *core* blocks available in the system.
Click any of the links to see documentation for a selected block.
