/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import java.util.List;

import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;

public class GetFileReferences extends CloudDataProcessorService
{
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute()
    throws Exception
    {
        Folder folder = (Folder) getProperties().xmlStorableValue("SourceFolder");
        boolean recursive = getProperties().booleanValue("Recursive", false);
        boolean includeDirs = getProperties().booleanValue("IncludeDirs", false);
        boolean includeFiles = !getProperties().booleanValue("ExcludeFiles", false);

        APIBroker api = createApiLink();
        folder = api.getFolder(folder.getId()); // Make sure it still exists
        if (folder == null) {
            throw new Exception("Folder does not exist");
        }

        LinkWrapper results = new LinkWrapper();
        if (recursive) {
            addContents(folder, api, includeDirs, includeFiles, results);
        } else {
            // Just add the contents of SourceFolder
            if (includeDirs) {
                List<Folder> folders = api.getChildFolders(folder.getId());
                for (Folder f : folders) {
                    results.addFolder(f);
                }
            }

            if (includeFiles) {
                List<DocumentRecord> files = api.getFolderDocuments(folder);
                DocumentVersion v;
                for(DocumentRecord r : files){
                    v = api.getLatestVersion(r.getId());
                    results.addDocument(r, v);
                    System.out.println("Importing: " + r.getName());

                }
            }
        }
        setOutputData("file-references", results);

        // Save the last directory into the global properties
        Folder lastDir = new Folder();
        lastDir.setId(folder.getId());
        getGlobalProperties().add("LastImportDir", lastDir);
    }

    private void addContents(Folder folder, APIBroker api, boolean includeDirs, boolean includeFiles, LinkWrapper contents)
    throws Exception
    {
        // To make it depth-first start with folders
        List<Folder> subdirectories = api.getChildFolders(folder.getId());
        for (Folder f : subdirectories) {
            if (includeDirs) {
                contents.addFolder(f);
            }

            if (!f.getId().equals(folder.getId())) {
                addContents(f, api, includeDirs, includeFiles, contents);
            } else {
                System.out.println("[WARNING] Folder " + folder.getName() + " includes a reference to itself (cyclic-reference); skipped.");
            }
        }
        // And then add documents
        if (includeFiles) {
            List<DocumentRecord> files = api.getFolderDocuments(folder);
            DocumentVersion v;
            for (DocumentRecord r : files) {
                v = api.getLatestVersion(r.getId());
                contents.addDocument(r, v);
                System.out.println("Importing: " + r.getName());
            }
        }
    }
}