# Create User

This block can create one or more user accounts. To create just a single account you may use block properties and set
username, password, first name and surname. First name or surname is optional, you need to provide at least one of
them.

To create more accounts at once use the `user-details` input which must include at least three columns. If the input
is connected then the block properties are used to denote column names (or ids) which will be used to create the 
accounts. For example, if `user-details` includes following:

| FirstName | LastName | Login                    | PlainTextPassword |
|:---------:|:--------:|:------------------------:|:-----------------:|
| Muhammad  | Smith    | Muhammad.Smith@my.domain | ...               |
| Sophia    | Jones    | Sophia.Jones@my.domain   | ...               |
| Oliver    | Wiliams  | Oliver.Wiliams@my.domain | ...               |

you can set the properties as follows:

* `Firstname` := FirstName,
* `Password` := `PlainTextPassword
* `Surname` := LastName,
* `Username` := Login,

Much like for other blocks that work with columns, you can use column index instead of its name or mix names and
indices:

* `Firstname` := FirstName,
* `Password` := #3
* `Surname` := LastName,
* `Username` := #2,

Again, the block requires that username and password are provided for every user, and first name and/or surname.


## Inputs

* `properties` -- An optional set of properties. May be used to set block properties dynamically via input rather than 
                  manually during workflow designing.
* `user-details` -- An optional table with user details.


## Outputs

* `user-id(s)` -- A table of id and home folder name for all generated users.


## Properties

* `Username` --  Logon name for the new user or the column name in the user-details input which will be used for users' logon name
* `Password` -- Password for the new user or the column name in the user-details input which will be used for users' password
* `Firstname` -- Users first name or the column name in the user-details input which will be used for users' first name     
* `Surname` -- Users surname or the column name in the user-details input which will be used for users' surname

