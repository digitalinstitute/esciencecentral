
/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.ConnexienceException;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.ColumnPickerCollection;

import java.rmi.RemoteException;
import java.util.ArrayList;


public class CreateUser implements WorkflowBlock
{
    private static final String Prop_USER_NAME = "Username";
    private static final String Prop_PASSWORD = "Password";
    private static final String Prop_FIRST_NAME = "Firstname";
    private static final String Prop_SURNAME = "Surname";

    private static final String Input_USER_DETAILS = "user-details";

    private static final String Output_USER_IDS = "user-id(s)";


    @Override
    public void preExecute(BlockEnvironment env) throws Exception
    { }


    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        String username  = env.getStringProperty(Prop_USER_NAME, "");
        String password  = env.getStringProperty(Prop_PASSWORD, "");
        String firstname = env.getStringProperty(Prop_FIRST_NAME, "");
        String surname   = env.getStringProperty(Prop_SURNAME, "");

        _verifyUserDetails(username, password, firstname, surname);

        ArrayList<String[]> userDetailsList = new ArrayList<>();

        if (env.getExecutionService().isInputConnected(Input_USER_DETAILS)) {
            // Check that the provided user details are correct.
            Data rawUserDetails = inputs.getInputDataSet(Input_USER_DETAILS);
            ColumnPickerCollection pickerCollection = new ColumnPickerCollection();
            pickerCollection.addColumnPicker(username);
            pickerCollection.addColumnPicker(password);
            if (!"".equals(firstname)) {
                pickerCollection.addColumnPicker(firstname);
            }
            if (!"".equals(surname)) {
                pickerCollection.addColumnPicker(surname);
            }

            Data userDetails = pickerCollection.extractData(rawUserDetails);

            StringColumn userNameCol  = (StringColumn) userDetails.column(0);
            StringColumn passwordCol  = (StringColumn) userDetails.column(1);
            int len = userDetails.getLargestRows();

            // Generate details of the following three use cases.
            if ("".equals(firstname)) {
                StringColumn surnameCol = (StringColumn)userDetails.column(2);

                for (int i = 0; i < len; i++) {
                    String[] userDetailsArray = new String[] { userNameCol.getStringValue(i), passwordCol.getStringValue(i), "", surnameCol.getStringValue(i) };
                    _verifyUserDetails(userDetailsArray[0], userDetailsArray[1], userDetailsArray[2], userDetailsArray[3]);
                    userDetailsList.add(userDetailsArray);
                }
            } else if ("".equals(surname)) {
                StringColumn firstnameCol = (StringColumn)userDetails.column(2);

                for (int i = 0; i < len; i++) {
                    String[] userDetailsArray = new String[] { userNameCol.getStringValue(i), passwordCol.getStringValue(i), firstnameCol.getStringValue(i), "" };
                    _verifyUserDetails(userDetailsArray[0], userDetailsArray[1], userDetailsArray[2], userDetailsArray[3]);
                    userDetailsList.add(userDetailsArray);
                }
            } else {
                StringColumn firstnameCol = (StringColumn)userDetails.column(2);
                StringColumn surnameCol = (StringColumn)userDetails.column(3);

                for (int i = 0; i < len; i++) {
                    String[] userDetailsArray = new String[] { userNameCol.getStringValue(i), passwordCol.getStringValue(i), firstnameCol.getStringValue(i), surnameCol.getStringValue(i) };
                    _verifyUserDetails(userDetailsArray[0], userDetailsArray[1], userDetailsArray[2], userDetailsArray[3]);
                    userDetailsList.add(userDetailsArray);
                }
            }
        } else {
            userDetailsList.add(new String[] { username, password, firstname, surname });
        }

        // Finally, ready to create users
        APIBroker api = env.getExecutionService().createApiLink();
        StringColumn userId_OutCol = new StringColumn("UserID");
        StringColumn homeFolder_OutCol = new StringColumn("HomeFolder");

        for (String[] userDetailsArray : userDetailsList) {
            _addUser(env, api, userDetailsArray[0], userDetailsArray[1], userDetailsArray[2], userDetailsArray[3], userId_OutCol, homeFolder_OutCol);
        }

        Data output = new Data();
        output.addColumn(userId_OutCol);
        output.addColumn(homeFolder_OutCol);
        outputs.setOutputDataSet(Output_USER_IDS, output);
    }


    @Override
    public void postExecute(BlockEnvironment env) throws Exception
    { }


    private void _verifyUserDetails(String username, String password, String firstname, String surname)
    throws IllegalArgumentException
    {
        if ("".equals(username) || "".equals(password) || "".equals(firstname) && "".equals(surname)) {
            throw new IllegalArgumentException("Missing user details. Username, password and at least first name or surname are mandatory");
        }
        // TODO: Add pattern matching for password -- complexity test
        // TODO: Add pattern matching for username -- e-mail address
    }


    private void _addUser(BlockEnvironment env, APIBroker api, String username, String password, String firstname, String surname, StringColumn userIdCol, StringColumn homeFolderCol)
    throws ConnexienceException, RemoteException, DataException
    {
        User u = api.createAccount(firstname, surname, username, password);
        APIBroker newAPI = env.getExecutionService().createAdditionalApiLink(u);
        Folder uHome = newAPI.getFolder(u.getHomeFolderId());

        userIdCol.appendStringValue(u.getId());
        homeFolderCol.appendStringValue(uHome.getName());
    }
}