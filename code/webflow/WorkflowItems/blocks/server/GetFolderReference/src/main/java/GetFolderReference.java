
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

public class GetFolderReference extends CloudDataProcessorService
{
    private static final String Prop_SOURCE_FOLDER = "SourceFolder";
    private static final String Prop_CREATE_METADATA = "CreateBasicMetadata";
    private static final String Prop_METADATA_CATEGORY = "GeneratedMetadataCategory";

    private static final String Output_FOLDER_REF = "folder-reference";
    private static final String Output_METADATA = "metadata";


    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Folder folder = (Folder)getProperties().xmlStorableValue(Prop_SOURCE_FOLDER);

        LinkWrapper results = new LinkWrapper();
        results.addFolder(folder);
        setOutputData(Output_FOLDER_REF, results);

        // Get the metadata for the file
        Data metaData = new Data();
        StringColumn category = new StringColumn("Category");
        StringColumn name = new StringColumn("Name");
        StringColumn value = new StringColumn("Value");
        metaData.addColumn(category);
        metaData.addColumn(name);
        metaData.addColumn(value);

        APIBroker api = createApiLink();
        MetadataCollection mdc = api.getMetadata(folder);
        for(MetadataItem i : mdc.getItems()){
            category.appendStringValue(i.getCategory());
            name.appendStringValue(i.getName());
            value.appendStringValue(i.getStringValue());
        }

        // Should we generate basic metadata
        if(getProperties().booleanValue(Prop_CREATE_METADATA, true)){
            String categoryName = getProperties().stringValue(Prop_METADATA_CATEGORY, "generated");
            category.appendStringValue(categoryName);
            name.appendStringValue("source-name");
            value.appendStringValue(folder.getName());

            category.appendStringValue(categoryName);
            name.appendStringValue("source-id");
            value.appendStringValue(folder.getId());

            category.appendStringValue(categoryName);
            name.appendStringValue("source-creation-date");
            value.appendStringValue(folder.getCreationDate().toString());

            category.appendStringValue(categoryName);
            name.appendStringValue("source-description");
            value.appendStringValue(folder.getDescription());
        }

        setOutputDataSet(Output_METADATA, metaData);

        // [CALA] Taken from 'GetFileReference', looks to me as a hack... commenting out.
        // 
        // Save the last directory into the global properties
        //Folder lastDir = new Folder();
        //lastDir.setId(downloadedRecord.getContainerId());
        //getGlobalProperties().add("LastImportDir", lastDir);
    }
}
