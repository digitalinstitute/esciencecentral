/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import java.util.List;
import org.pipeline.core.data.*;

public class GetRelatedFileReferences extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        LinkWrapper input = (LinkWrapper)getInputData("input-reference");
        if(input.size()==1){
            LinkWrapper relatedFiles = new LinkWrapper();
            LinkPayloadItem item = input.getItem(0);
            ServerObject obj = item.getServerObject();
            if(obj instanceof DocumentRecord){
                DocumentRecord doc = api.getDocument(obj.getId());
                Folder parentFolder = api.getFolder(doc.getContainerId());
                List<DocumentRecord> folderDocs = api.getFolderDocuments(parentFolder);
                for(DocumentRecord d : folderDocs){
                    relatedFiles.addDocument(d, api.getLatestVersion(d.getId()));
                    System.out.println("Adding related document: [" + d.getId() + "]"+ d.getName());
                }
                setOutputData("related-references", relatedFiles);
            } else {
                throw new Exception("Only works with document records");
            }
            
        } else {
            throw new Exception("Only works with a single input file reference");
        }

        
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}