
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.*;
import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.*;
import com.connexience.server.model.metadata.MetadataCollection;
        
import org.pipeline.core.data.*;
import java.io.*;
public class DownloadReferences extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        LinkWrapper inputData = (LinkWrapper)getInputData("file-references");
        DocumentRecord doc;
        DocumentVersion v;
        File f;
        
        File dir;
        if(getProperties().booleanValue("DownloadToSubdirectory", false)){
            dir = new File(getWorkingDirectory(), getProperties().stringValue("Subdirectory", "import"));
            dir.mkdirs();            
        } else {
            dir = getWorkingDirectory();
        }
        
        FileWrapper importedFiles = new FileWrapper(dir);
        
        LinkPayloadItem item;
        
        for(int i=0;i<inputData.size();i++){
            item = inputData.getItem(i);
            if(item instanceof DocumentRecordLinkItem){
                doc = (DocumentRecord)((DocumentRecordLinkItem)item).getDocument();
                v = (DocumentVersion)((DocumentRecordLinkItem)item).getVersion();
                f = new File(dir, doc.getName());
                api.downloadToFile(doc, v.getId(), f);
                importedFiles.addFile(f, false);
                System.out.println("Downloaded: " + doc.getName() + " [" + doc.getId() + "]");
                
            }
        }
        
        setOutputData("files", importedFiles);
        
        if(inputData.size()==1 && getEditableProperties().booleanValue("GetMetadata", false)){
            // Attach metadata
            item = inputData.getItem(0);
            if(item instanceof DocumentRecordLinkItem){
                doc = (DocumentRecord)((DocumentRecordLinkItem)item).getDocument();
                MetadataCollection mdc = api.getMetadata(doc);
                setOutputMetadata("files", mdc);
            }
        }        
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}