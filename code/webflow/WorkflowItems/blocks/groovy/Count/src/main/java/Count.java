/**
 * e-Science Central Copyright (C) 2008-2015 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import groovy.lang.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.xmlstorage.XmlDataObject;
import org.pipeline.core.xmlstorage.XmlStorable;
import org.pipeline.core.xmlstorage.XmlStorageException;
import org.pipeline.core.xmlstorage.xmldatatypes.XmlStorableDataObject;


public class Count implements WorkflowBlock
{
    private final static String Prop_CONDITION = "Condition";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";
    private final static String Prop_AS_ROWS = "As Rows";
    private final static String Prop_MATCHING = "Get Matching";
    private final static String Prop_NON_MATCHING = "Get Non-matching";
    private final static String Prop_TOTAL = "Get Total";

    private final static String Input_A = "A";

    private final static String Output_COUNTS = "counts";

    private GroovyClassLoader _classLoader;
    private GroovyObject _conditionObj;

    private HashMap<String, String> _col2var;
    private ArrayList<String> _selectedColumnNames;
    private String _sanitizedCondition;
    private StringBuilder _scriptText;

    private long _totalRows;
    private long _totalChunks;

    private long _matching;


    private static String _sanitizeProps(BlockEnvironment env, String condition, StringBuilder scriptText)
    throws XmlStorageException
    {
        HashMap<String, String> prop2var = new HashMap<>();

        // Sanitize variable names for the block properties
        Pattern propPattern = Pattern.compile("\\$\\{\\{.*?\\}\\}");
        Matcher propMatcher = propPattern.matcher(condition);
        StringBuilder sanitized = new StringBuilder();
        int varCnt = 0;
        int lastMatchEnd = 0;
        while (propMatcher.find()) {
            String match = propMatcher.group();
            String propName = match.substring(3, match.length() - 2);
            String variableName = prop2var.get(propName);
            if (variableName ==  null) {
                variableName = "PVarP" + varCnt++;
                prop2var.put(propName, variableName);

                scriptText.append("    private static ");
                XmlDataObject prop = env.getExecutionService().getProperties().get(propName);
                switch (prop.getTypeLabel()) {
                    case "String":
                        scriptText.append("String ");
                        scriptText.append(variableName);
                        scriptText.append(" = '");
                        scriptText.append(_escapeQuotes(prop.getValue().toString()));
                        scriptText.append("'");
                        break;
                    case "Integer":
                    case "Long":
                    case "Double":
                    case "Boolean":
                        scriptText.append(prop.getTypeLabel());
                        scriptText.append(' ');
                        scriptText.append(variableName);
                        scriptText.append(" = ");
                        scriptText.append(prop.getValue().toString());
                        break;
                    case "XmlStorable":
                        XmlStorable storableValue = ((XmlStorableDataObject)prop).xmlStorableValue();
                        if (storableValue instanceof StringPairListWrapper) {
                            StringPairListWrapper pairList = (StringPairListWrapper)storableValue;
                            scriptText.append("Map ");
                            scriptText.append(variableName);
                            scriptText.append(" = [");
                            for (String[] entry : pairList) {
                                scriptText.append(entry[0]);
                                scriptText.append(":'");
                                scriptText.append(_escapeQuotes(entry[1]));
                                scriptText.append("', ");
                            }
                            if (pairList.getSize() > 0) {
                                scriptText.setLength(scriptText.length() - 2);
                                scriptText.append("]");
                            } else {
                                scriptText.append(":]");
                            }
                        } else if (storableValue instanceof StringListWrapper) {
                            StringListWrapper stringList = (StringListWrapper)storableValue;
                            scriptText.append("List ");
                            scriptText.append(variableName);
                            scriptText.append(" = ['");
                            for (String entry : stringList) {
                                scriptText.append(_escapeQuotes(entry));
                                scriptText.append("', '");
                            }
                            if (stringList.getSize() > 0) {
                                scriptText.setLength(scriptText.length() - 3);
                            }
                            scriptText.append("]");
                        } else {
                            throw new UnsupportedOperationException("Sanitization of the condition property failed: unsupported property type: " + storableValue.getClass());
                        }
                        break;
                    default:
                        throw new UnsupportedOperationException("Sanitization of the condition property failed: unsupported property type: " + prop.getTypeLabel());
                }
                scriptText.append(";\n");
            }
            sanitized.append(condition.substring(lastMatchEnd, propMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = propMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        return sanitized.toString();
    }

    public void preExecute(BlockEnvironment env) throws Exception
    {
        String condition = env.getStringProperty(Prop_CONDITION, "");
        System.out.println("User condition:\n" + condition);

        _selectedColumnNames = new ArrayList<>();

        // Sanitize variable names for block properties
        _scriptText = new StringBuilder();
        if (env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            _scriptText.append("@groovy.transform.CompileStatic\n");
        }
        _scriptText.append("class Condition {\n");
        condition = _sanitizeProps(env, condition, _scriptText);

        // Sanitize variable names for the columns in Input_A
        Pattern columnPattern = Pattern.compile("\\$" + Input_A + "\\.\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(condition);

        int varCnt = 0;
        int lastMatchEnd = 0;
        _col2var = new HashMap<>();
        StringBuilder sanitized = new StringBuilder();

        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(3 + Input_A.length(), match.length() - 1);
            String variableName = _col2var.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var.put(columnName, variableName);
                _selectedColumnNames.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // For non static compilation we can build a Groovy script that embeds the user condition without data types.
        _classLoader = new GroovyClassLoader();

        if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            _scriptText.append("    static boolean predicate(");
            if (_selectedColumnNames.size() > 0) {
                for (String columnName : _selectedColumnNames) {
                    _scriptText.append(_col2var.get(columnName));
                    _scriptText.append(", ");
                }
                _scriptText.setLength(_scriptText.length() - 2);
            }
            _scriptText.append(") {\n        ");
            _scriptText.append(sanitized);
            _scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + _scriptText.toString());

            Class conditionClass = _classLoader.parseClass(_scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        } else {
            _sanitizedCondition = sanitized.toString();
        }
    }

    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        Data inputData = inputs.getInputDataSet(Input_A);

        ArrayList<Column> selectedColumns = new ArrayList<>();
        for (String columnName : _selectedColumnNames) {
            selectedColumns.add(inputData.column(columnName));
        }

        if (_conditionObj == null) {
            if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
                throw new IllegalStateException("Property '" + Prop_STATIC_COMPILATION + "' not set but the condition object is null");
            }

            // Build a Groovy script that embeds the user condition.
            _scriptText.append("    static boolean predicate(");
            if (selectedColumns.size() > 0) {
                for (Column column : selectedColumns) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        _scriptText.append("Boolean ");
                    }
                    _scriptText.append(_col2var.get(column.getName()));
                    _scriptText.append(", ");
                }
                _scriptText.setLength(_scriptText.length() - 2);
            }

            _scriptText.append(") {\n        ");
            _scriptText.append(_sanitizedCondition);
            _scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + _scriptText.toString());

            Class conditionClass = _classLoader.parseClass(_scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        }

        Data outputData = inputData.getEmptyCopy();
        outputData.setIndexColumn(null);
        Data remainderData = inputData.getEmptyCopy();
        remainderData.setIndexColumn(null);

        _matching += _calculateCounts(_conditionObj, inputData, selectedColumns);

        // Compute some basic statistics
        _totalRows += inputData.getLargestRows();
        _totalChunks++;
    }

    public void postExecute(BlockEnvironment env) throws Exception
    {
        System.out.println("Processed total of " + _totalRows + " rows in " + _totalChunks + " chunks.");

        Data outputData = new Data();

        if (env.getBooleanProperty(Prop_AS_ROWS, false)) {
            StringColumn titleCol = new StringColumn("Statistics");
            IntegerColumn countsCol = new IntegerColumn("Counts");

            if (env.getBooleanProperty(Prop_MATCHING, false)) {
                titleCol.appendStringValue("Matching");
                countsCol.appendLongValue(_matching);
            }

            if (env.getBooleanProperty(Prop_NON_MATCHING, false)) {
                titleCol.appendStringValue("Non-matching");
                countsCol.appendLongValue(_totalRows - _matching);
            }

            if (env.getBooleanProperty(Prop_TOTAL, false)) {
                titleCol.appendStringValue("Total");
                countsCol.appendLongValue(_totalRows);
            }

            outputData.addColumn(titleCol);
            outputData.addColumn(countsCol);
        } else {
            if (env.getBooleanProperty(Prop_MATCHING, false)) {
                IntegerColumn countCol = new IntegerColumn("Matching");
                countCol.appendLongValue(_matching);
                outputData.addColumn(countCol);
            }

            if (env.getBooleanProperty(Prop_NON_MATCHING, false)) {
                IntegerColumn countCol = new IntegerColumn("Non-matching");
                countCol.appendLongValue(_totalRows - _matching);
                outputData.addColumn(countCol);
            }

            if (env.getBooleanProperty(Prop_TOTAL, false)) {
                IntegerColumn countCol = new IntegerColumn("Total");
                countCol.appendLongValue(_totalRows);
                outputData.addColumn(countCol);
            }
        }

        // Pass the outputs
        env.getExecutionService().setOutputDataSet(Output_COUNTS, outputData);
    }


    private static String _escapeQuotes(String text)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            switch (c) {
                case '\'':
                    sb.append('\\');
                    sb.append(c);
                    break;
                default:
                    sb.append(c);
                    break;
            }
        }

        return sb.toString();
    }


    private long _calculateCounts(GroovyObject conditionObj, Data inputData, ArrayList<Column> selectedColumns)
    throws DataException
    {
        long matching = 0;

        Object[] args = new Object[selectedColumns.size()];
        int rowNo = inputData.getLargestRows();

        for (int row = 0; row < rowNo; row++) {
            // For each row set the value of all column variables
            int argNo = 0;
            for (Column column : selectedColumns) {
                Object value = column.getObjectValue(row);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            if ((Boolean) conditionObj.invokeMethod("predicate", args)) {
                matching++;
            }
        }

        return matching;
    }
}
