
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.RowExtractor;

public class MyService implements WorkflowBlock {
    /**
     * This field refers to property 'Copy Input' defined in service.xml
     */
    private final static String Head = "Head";
    private final static String Tail = "Tail";
    private final static String GetColumn = "GetColumn";
	private final static String CONDITION = "condition";
    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_INPUT_1 = "input-1";
    
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_OUTPUT_1 = "output-1";


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        
    }

    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
	public void execute(BlockEnvironment env, BlockInputs inputs,
			BlockOutputs outputs) throws Exception {
		Data outputData = new Data();
		Data inputData = inputs.getInputDataSet(Input_INPUT_1);
		int HeadCon = env.getIntProperty(Head, 0);
		int TailCon = env.getIntProperty(Tail, 0);
		List<String> SelectedColumnName=new ArrayList<>();
		boolean getColumn = env.getBooleanProperty(GetColumn, true);
		String condition=env.getStringProperty(CONDITION, "");
		final ArrayList<Integer> Index = new ArrayList<Integer>();
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			while (ColumnMatcher.find()) {
				String ColumnName = ColumnMatcher.group().replace("{", "")
						.replace("}", "");
				System.out.println(ColumnName);
				SelectedColumnName.add(ColumnName);
			} 
			condition = condition.replace("{", "").replace("}", "");
			Binding context = new Binding();
			for (int row = 0; row < inputData.getLargestRows(); row++) {
				//set the value of variable of the condition
				for(String column:SelectedColumnName){
					System.out.println(column);
					context.setVariable(column, inputData.column(column).getObjectValue(row));
					System.out.println(inputData.column(column).getObjectValue(row));
				}	
				//implement the condition by GroovyShell and store the index of row which meet the conidtion
				GroovyShell sh = new GroovyShell(context);
				System.out.println(sh.toString());
				System.out.println(condition);
				boolean Boolean = (boolean) sh.evaluate(condition);
				System.out.println(Boolean);
				if (Boolean) {
					Index.add(row);
				}
		}
			Integer[] SelectedIndex = Index.toArray(new Integer[0]);
			RowExtractor extractor = new RowExtractor(inputData);
			Data output = extractor.extract(SelectedIndex);
			if (getColumn) {
				int inputData_ColumnSize = output.getColumnCount();
				if (HeadCon > 0||TailCon > 0 && HeadCon <= inputData_ColumnSize&& TailCon <= inputData_ColumnSize) {
					for (int i = 0; i < HeadCon; i++) {
						
						outputData.addColumn(output.column(i));
						
					}
					for (int i = inputData_ColumnSize - 1; i >= inputData_ColumnSize
							- TailCon; i--) {
						
					
						outputData.addColumn(output.column(i));
						
					}
					}else {
					Exception err = new Exception("invalid number of head or tail");
					throw err;
				}
			} else {
				int inputData_RowSize = output.getLargestRows();
				System.out.println("inputData_RowSize: " + inputData_RowSize);
				if (HeadCon > 0 || TailCon > 0 && HeadCon <= inputData_RowSize
						&& TailCon <= inputData_RowSize) {
					int temp = 0;
					for (int j = 0; j < output.getColumnCount(); j++) {
						if(!outputData.containsColumn(output.column(j).getName())){
						outputData.addColumn(output.column(j).getSubset(temp,
								HeadCon, true));
						}
						else{
							for(int k=0;k<HeadCon;k++){
							outputData.column(output.column(j).getName()).insertObjectValue(outputData.column(output.column(j).getName()).getRows(), output.column(j).getObjectValue(k), true);
							}
							}
					}
					int temp1 = inputData_RowSize - TailCon;
					for (int j = 0; j < output.getColumnCount(); j++) {
						if(!outputData.containsColumn(output.column(j).getName())){
						outputData.addColumn(output.column(j).getSubset(temp1,
								inputData_RowSize, true));
						}else{

							for(int k=temp1;k<inputData_RowSize;k++){
							outputData.column(output.column(j).getName()).insertObjectValue(outputData.column(output.column(j).getName()).getRows(), output.column(j).getObjectValue(k), true);
							}
							
						}

					}
				} else {
					Exception err = new Exception("invalid number of head or tail");
					throw err;
				}

			}	
		// Pass this to the output called "output-1"
		outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
	}

    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        
    }
}
