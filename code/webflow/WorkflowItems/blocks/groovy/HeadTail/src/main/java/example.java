import groovy.lang.Binding;
import groovy.lang.GroovyShell;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.pipeline.core.data.Data;
import org.pipeline.core.data.DataException;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.RowExtractor;

public class example {
	public static List<String> SelectedColumnName=new ArrayList<>();
	public Data generate() throws IndexOutOfBoundsException, DataException {
		Data inputData = new Data();
		IntegerColumn Age = new IntegerColumn("Age");
		StringColumn Sex = new StringColumn("Sex");
		IntegerColumn ID = new IntegerColumn("ID");
		StringColumn country = new StringColumn("Country");
		Age.nullifyToSize(10);
		ID.nullifyToSize(10);
		Age.setIntValue(0, 1);
		Age.setIntValue(1, 34);
		Age.setIntValue(2, 12);
		Age.setIntValue(3, 54);
		Age.setIntValue(4, 26);
		Age.setIntValue(5, 31);
		Age.setIntValue(6, 20);
		Age.setIntValue(7, 27);
		Age.setIntValue(8, 30);
		Age.setIntValue(9, 52);
		Sex.insertObjectValue(0, "Male", true);
		Sex.insertObjectValue(1, "Female", true);
		Sex.insertObjectValue(2, "Female", true);
		Sex.insertObjectValue(3, "Male", true);
		Sex.insertObjectValue(4, "Male", true);
		Sex.insertObjectValue(5, "Female", true);
		Sex.insertObjectValue(6, "Male", true);
		Sex.insertObjectValue(7, "Female", true);
		Sex.insertObjectValue(8, "Male", true);
		Sex.insertObjectValue(9, "Female", true);	
		country.insertObjectValue(0, "china", true);
		country.insertObjectValue(1, "uk", true);
		country.insertObjectValue(2, "usa", true);
		country.insertObjectValue(3, "france", true);
		country.insertObjectValue(4, "italy", true);
		country.insertObjectValue(5, "germany", true);
		country.insertObjectValue(6, "japan", true);
		country.insertObjectValue(7, "korean", true);
		country.insertObjectValue(8, "usa", true);
		country.insertObjectValue(9, "africa", true);
		ID.setIntValue(0, 1);
		ID.setIntValue(1,2);
		ID.setIntValue(2,3);
		ID.setIntValue(3, 4);
		ID.setIntValue(4, 5);
		ID.setIntValue(5, 6);
		ID.setIntValue(6, 7);
		ID.setIntValue(7, 8);
		ID.setIntValue(8, 9);
		ID.setIntValue(9, 10);
		inputData.addColumn(Sex);
		inputData.addColumn(Age);
		inputData.addColumn(ID);
		inputData.addColumn(country);
		return inputData;
	}

	public String generateGroovyInput(Data input, String condition)
			throws IndexOutOfBoundsException, DataException {	
		Pattern ColumnPattern = Pattern.compile("\\{.*?\\}");
		Matcher ColumnMatcher = ColumnPattern.matcher(condition);
			while (ColumnMatcher.find()) {
				String ColumnName = ColumnMatcher.group().replace("{", "")
						.replace("}", "");
				System.out.println(ColumnName);
				SelectedColumnName.add(ColumnName);
			} 
			condition = condition.replace("{", "").replace("}", "");
		return condition;
	}

	public Integer[] extractIndex(String condition,Data input) throws IndexOutOfBoundsException, DataException {
		final ArrayList<Integer> Index = new ArrayList<Integer>();
		Binding context = new Binding();
		for (int row = 0; row < input.getLargestRows(); row++) {
			//set the value of variable of the condition
			for(String column:SelectedColumnName){
				System.out.println(column);
				context.setVariable(column, input.column(column).getObjectValue(row));
			}	
			//implement the condition by GroovyShell and store the index of row which meet the conidtion
			GroovyShell sh = new GroovyShell(context);
			System.out.println(condition);
			boolean Boolean = (boolean) sh.evaluate(condition);
			System.out.println(Boolean);
			if (Boolean) {
				Index.add(row);
			}
	}
		for (Integer xxxxxxxxxx : Index) {
			System.out.println(xxxxxxxxxx);
		}
       
		Integer[] SelectedIndex = Index.toArray(new Integer[0]);
		return SelectedIndex;
	}

	public Data extractData(Data input, Integer[] SelectedIndex)
			throws IndexOutOfBoundsException, DataException {
		RowExtractor extractor = new RowExtractor(input);
		Data output = extractor.extract(SelectedIndex);
		return output;
	}

	public Data generateOutput(boolean getColumn, Data output, int HeadCon,
			int TailCon) throws Exception {
		Data outputData = new Data();
		if (getColumn) {
			int inputData_ColumnSize = output.getColumnCount();
			if (HeadCon > 0||TailCon > 0 && HeadCon <= inputData_ColumnSize&& TailCon <= inputData_ColumnSize) {
				for (int i = 0; i < HeadCon; i++) {
					
					outputData.addColumn(output.column(i));
					
				}
				for (int i = inputData_ColumnSize - 1; i >= inputData_ColumnSize
						- TailCon; i--) {
					
				
					outputData.addColumn(output.column(i));
					
				}
				}else {
				Exception err = new Exception("invalid number of head or tail");
				throw err;
			}
		} else {
			int inputData_RowSize = output.getLargestRows();
			System.out.println("inputData_RowSize: " + inputData_RowSize);
			if (HeadCon > 0 || TailCon > 0 && HeadCon <= inputData_RowSize
					&& TailCon <= inputData_RowSize) {
				int temp = 0;
				for (int j = 0; j < output.getColumnCount(); j++) {
					if(!outputData.containsColumn(output.column(j).getName())){
					outputData.addColumn(output.column(j).getSubset(temp,
							HeadCon, true));
					}
					else{
						for(int k=0;k<HeadCon;k++){
						outputData.column(output.column(j).getName()).insertObjectValue(outputData.column(output.column(j).getName()).getRows(), output.column(j).getObjectValue(k), true);
						}
						}
				}
				int temp1 = inputData_RowSize - TailCon;
				for (int j = 0; j < output.getColumnCount(); j++) {
					if(!outputData.containsColumn(output.column(j).getName())){
					outputData.addColumn(output.column(j).getSubset(temp1,
							inputData_RowSize, true));
					}else{

						for(int k=temp1;k<inputData_RowSize;k++){
						outputData.column(output.column(j).getName()).insertObjectValue(outputData.column(output.column(j).getName()).getRows(), output.column(j).getObjectValue(k), true);
						}
						
					}

				}
			} else {
				Exception err = new Exception("invalid number of head or tail");
				throw err;
			}

		}
		return outputData;
	}

	public static void main(String[] arg) throws Exception {
		example example = new example();
		Data inputData = example.generate();
		Data outputData = new Data();
		String condition = "def sex={Sex};def age={Age};sex.charAt(0)=='M'&&age/{ID}>2";
	//	String condition = "{'age+id'}>6";
		String inputcondition = example.generateGroovyInput(inputData,
				condition);
		Integer[] selectedIndex = example.extractIndex(inputcondition,inputData);
		Data temp = example.extractData(inputData, selectedIndex);
		boolean getColumn = false;
		int HeadCon = 1;
		int TailCon = 4;
		outputData = example.generateOutput(getColumn, temp, HeadCon, TailCon);
		System.out.println("Result:");
		for (int i = 0; i < outputData.getColumnCount(); i++) {
			for (int j = 0; j < outputData.column(i).getRows(); j++) {
				System.out.println(outputData.column(i).getName() + ": "
						+ outputData.column(i).getObjectValue(j));
			}
		}
	}
}
