/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;


public class Join implements WorkflowBlock {
    /**
     * This field refers to property 'Copy Input' defined in service.xml
     */
    private final static String Prop_CONDITION = "Condition";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";
    private static final String Prop_JOIN_TYPE = "Join Type";
    private static final String _JOIN_INNER = "INNER";
    private static final String _JOIN_LEFT = "LEFT";
    private static final String _JOIN_RIGHT = "RIGHT";
    private static final String _JOIN_OUTER = "OUTER";

    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_A = "A";
    private final static String Input_B = "B";
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_OUTPUT_1 = "output-1";

    private GroovyClassLoader _classLoader;
    private GroovyObject _conditionObj;

    private HashMap<String, String> _col2var_I1;
    private HashMap<String, String> _col2var_I2;
    private ArrayList<String> _selectedColumnNames_I1;
    private ArrayList<String> _selectedColumnNames_I2;
    private String _sanitizedCondition;

    Data _inputData_I1;
    ArrayList<Column> _selectedColumns_I1;

    // Cache to avoid calling Groovy --> requires a deterministic condition.
    private HashMap<ArgumentArray, Boolean> _predicateValueMap;

    long _totalRows2;
    long _totalChunks2;

    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        _col2var_I1 = new HashMap<>(); // maps columns to groovy variable names
        _col2var_I2 = new HashMap<>(); // maps columns to groovy variable names

        String condition = env.getStringProperty(Prop_CONDITION, "");
        System.out.println("User condition:\n" + condition);

        _selectedColumnNames_I1 = new ArrayList<>();
        _selectedColumnNames_I2 = new ArrayList<>();

        // Sanitize variable names for the columns in INPUT_1
        Pattern columnPattern = Pattern.compile("\\$" + Input_A + "\\.\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(condition);

        int varCnt = 0;
        StringBuilder sanitized = new StringBuilder();
        int lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(Input_A.length() + 3, match.length() - 1); // +3 for the dollar sign, dot and curly brace '$input-1.{'
            String variableName = _col2var_I1.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var_I1.put(columnName, variableName);
                _selectedColumnNames_I1.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // Sanitize variable names for the columns in INPUT_2
        condition = sanitized.toString();
        sanitized.setLength(0);
        columnPattern = Pattern.compile("\\$" + Input_B + "\\.\\{.*?\\}");
        columnMatcher = columnPattern.matcher(condition);
        lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(Input_B.length() + 3, match.length() - 1); // +3 for the dollar sign, dot and curly brace '$input-2.{'
            String variableName = _col2var_I2.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var_I2.put(columnName, variableName);
                _selectedColumnNames_I2.add(columnName);
            }
            sanitized.append(condition.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(condition.substring(lastMatchEnd));

        // Build a Groovy script that embeds the user condition.
        _classLoader = new GroovyClassLoader();

        if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("class Condition {\n    static boolean predicate(");
            if (_selectedColumnNames_I1.size() > 0) {
                for (String columnName : _selectedColumnNames_I1) {
                    scriptText.append(_col2var_I1.get(columnName));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumnNames_I2.size() > 0) {
                for (String columnName : _selectedColumnNames_I2) {
                    scriptText.append(_col2var_I2.get(columnName));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumnNames_I1.size() + _selectedColumnNames_I2.size() > 0) {
                scriptText.setLength(scriptText.length() - 2);
            }

            scriptText.append(") {\n        ");
            scriptText.append(sanitized);
            scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        } else {
            _sanitizedCondition = sanitized.toString();
        }

        // Input-1 is non-streamed so we can read it here in full.
        if (_inputData_I1 == null) {
            _inputData_I1 = env.getExecutionService().getInputDataSet(Input_A);
            _selectedColumns_I1 = new ArrayList<>();
            for (String columnName : _selectedColumnNames_I1) {
                _selectedColumns_I1.add(_inputData_I1.column(columnName));
            }
        }
    }


    /**
     * This code is used to perform the actual block operation. It may be called
     * multiple times if data is being streamed through the block. It is, however, 
     * guaranteed to be called at least once and always after the preExecute
     * method and always before the postExecute method;
     */
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception
    {
        Data inputData_I2 = inputs.getInputDataSet(Input_B);
        ArrayList<Column> selectedColumns_I2 = new ArrayList<>();
        for (String columnName : _selectedColumnNames_I2) {
            selectedColumns_I2.add(inputData_I2.column(columnName));
        }

        if (_predicateValueMap == null) {
            _predicateValueMap = new HashMap<>(Math.max(_inputData_I1.getLargestRows(), inputData_I2.getLargestRows()) * 2, 0.5f);
        }

        if (_conditionObj == null) {
            if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
                throw new IllegalStateException("Property '" + Prop_STATIC_COMPILATION + "' not set but the condition object is null");
            }

            // Build a Groovy script that embeds the user condition.
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("@groovy.transform.CompileStatic\n");
            scriptText.append("class Condition {\n    static boolean predicate(");
            if (_selectedColumns_I1.size() > 0) {
                for (Column column : _selectedColumns_I1) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(_col2var_I1.get(column.getName()));
                    scriptText.append(", ");
                }
            }

            if (selectedColumns_I2.size() > 0) {
                for (Column column : selectedColumns_I2) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(_col2var_I2.get(column.getName()));
                    scriptText.append(", ");
                }
            }

            if (_selectedColumns_I1.size() + selectedColumns_I2.size() > 0) {
                scriptText.setLength(scriptText.length() - 2);
            }

            scriptText.append(") {\n        ");
            scriptText.append(_sanitizedCondition);
            scriptText.append("\n    }\n}\n");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        }

        // Prepare the output data object which includes all columns from both inputs
        // This is slightly different from a regular SQL join that removes duplicated columns but note that
        // the condition may be a free Groovy expression, so the columns on which the join is done may have different
        // values even if the join condition yields true; e.g. input-1.{NumericalColumn} == input-2.{TextColumn}.length()
        Data outputData = _prepareOutputData(_inputData_I1.getEmptyCopy(), inputData_I2.getEmptyCopy());

        switch (env.getStringProperty(Prop_JOIN_TYPE, "")) {
            case _JOIN_INNER:
                _innerJoin(_conditionObj, _inputData_I1, _selectedColumns_I1, inputData_I2, selectedColumns_I2, outputData);
                break;
            case _JOIN_LEFT:
                _leftJoin(_conditionObj, _inputData_I1, _selectedColumns_I1, inputData_I2, selectedColumns_I2, outputData);
                break;
            case _JOIN_RIGHT:
                _rightJoin(_conditionObj, _inputData_I1, _selectedColumns_I1, inputData_I2, selectedColumns_I2, outputData);
                break;
            case _JOIN_OUTER:
                //_outerJoin(_conditionObj, _inputData_I1, _selectedColumns_I1, inputData_I2, selectedColumns_I2, outputData);
                //break;
            default:
                throw new IllegalArgumentException("Unsupported join type: '" + env.getStringProperty(Prop_JOIN_TYPE, "") + "'");
        }

        // Compute some basic statistics
        _totalRows2 += inputData_I2.getLargestRows();
        _totalChunks2++;

        // Pass this to the output called "output-1"
        outputs.setOutputDataSet(Output_OUTPUT_1, outputData);
    }


    /**
     * This operation does something like:
     * <code>
     *      Data outputData = new Data();
     *      outputData.joinData(_inputData_I1.getEmptyCopy(), false);
     *      outputData.joinData(inputData_I2.getEmptyCopy(), false);
     *</code>
     * but in a slightly more careful way to avoid duplicated column names.
     *
     * @param partA
     * @param partB
     * @return data which combines columns from part A and B in the way there are no duplicated column names.
     */
    private static Data _prepareOutputData(Data partA, Data partB)
    throws DataException
    {
        Data output = new Data();
        partA.renameUnnamedColumns();
        partB.renameUnnamedColumns();
        HashMap<String, Integer> aColumns = new HashMap<>(2 * partA.getColumnCount()); // 2* to avoid rehash
        int aColNo = partA.getColumnCount();
        for (int i = 0; i < aColNo; i++) {
            Column c = partA.column(i);
            if (aColumns.containsKey(c.getName())) {
                throw new IllegalArgumentException("Non-unique column name in input " + Input_A +": " + c.getName());
            } else {
                aColumns.put(c.getName(), i);
            }
            output.addColumn(c);
        }

        int bColNo = partB.getColumnCount();
        for (int j = 0; j < bColNo; j++) {
            Column c = partB.column(j);
            Integer i = aColumns.get(c.getName());
            if (i == null) {
                output.addColumn(c);
            } else {
                output.column(i).setName("A." + c.getName());
                c.setName("B." + c.getName());
                output.addColumn(c);
            }
        }

        return output;
    }


    /**
     * A naive O(n*m) implementation of the JOIN operation.
     * TODO: Find time and make it more efficient.
     *
     * @param conditionObj
     * @param inputData_I1
     * @param selectedColumns_I1
     * @param inputData_I2
     * @param selectedColumns_I2
     * @param outputData
     * @throws DataException
     */
    private static void _innerJoin(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData)
            throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();
        int colNo_I2 = inputData_I2.getColumnCount();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        for (int r1 = 0; r1 < rowNo_I1; r1++) {
            // For each row in Input_1 set the value of all its column variables
            int argNo = 0;
            for (Column column : selectedColumns_I1) {
                Object value = column.getObjectValue(r1);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            for (int r2 = 0; r2 < rowNo_I2; r2++) {
                // For each row in Input_2 set the value of all its column variables
                argNo = selectedColumns_I1.size();
                for (Column column : selectedColumns_I2) {
                    Object value = column.getObjectValue(r2);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                if ((Boolean)conditionObj.invokeMethod("predicate", args)) {
                    for (int c = 0; c < colNo_I1; c++) {
                        outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    for (int c = 0; c < colNo_I2; c++) {
                        outputData.column(colNo_I1 + c).appendObjectValue(inputData_I2.column(c).getObjectValue(r2));
                    }
                }
            }
        }
    }


    /**
     * A naive O(n*m) implementation of the LEFT JOIN operation.
     * TODO: Find time and make it more efficient.
     *
     * @param conditionObj
     * @param inputData_I1
     * @param selectedColumns_I1
     * @param inputData_I2
     * @param selectedColumns_I2
     * @param outputData
     * @throws DataException
     */
    private static void _leftJoin(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData)
            throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();
        int colNo_I2 = inputData_I2.getColumnCount();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        for (int r1 = 0; r1 < rowNo_I1; r1++) {
            // For each row in Input_1 set the value of all its column variables
            int argNo = 0;
            for (Column column : selectedColumns_I1) {
                Object value = column.getObjectValue(r1);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            boolean matchFound = false;
            for (int r2 = 0; r2 < rowNo_I2; r2++) {
                // For each row in Input_2 set the value of all its column variables
                argNo = selectedColumns_I1.size();
                for (Column column : selectedColumns_I2) {
                    Object value = column.getObjectValue(r2);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                if ((Boolean)conditionObj.invokeMethod("predicate", args)) {
                    for (int c = 0; c < colNo_I1; c++) {
                        outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    for (int c = 0; c < colNo_I2; c++) {
                        outputData.column(colNo_I1 + c).appendObjectValue(inputData_I2.column(c).getObjectValue(r2));
                    }
                    matchFound = true;
                }
            }

            // This is left-join so even if there's no match between I1.r1 and I2, we need to add I1.r1 to the output.
            if (!matchFound) {
                for (int c = 0; c < colNo_I1; c++) {
                    outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                }
                for (int c = 0; c < colNo_I2; c++) {
                    outputData.column(colNo_I1 + c).appendObjectValue(MissingValue.get());
                }
            }
        }
    }


    /**
     * A naive O(n*m) implementation of the RIGHT JOIN operation.
     * TODO: Find time and make it more efficient.
     *
     * @param conditionObj
     * @param inputData_I1
     * @param selectedColumns_I1
     * @param inputData_I2
     * @param selectedColumns_I2
     * @param outputData
     * @throws DataException
     */
    private static void _rightJoin(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData)
            throws DataException
    {
        int rowNo_I1 = inputData_I1.getLargestRows();
        int colNo_I1 = inputData_I1.getColumnCount();
        int rowNo_I2 = inputData_I2.getLargestRows();
        int colNo_I2 = inputData_I2.getColumnCount();

        Object[] args = new Object[selectedColumns_I1.size() + selectedColumns_I2.size()];

        for (int r2 = 0; r2 < rowNo_I2; r2++) {
            // For each row in Input_2 set the value of all its column variables
            int argNo = selectedColumns_I1.size();
            for (Column column : selectedColumns_I2) {
                Object value = column.getObjectValue(r2);
                args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            boolean matchFound = false;
            for (int r1 = 0; r1 < rowNo_I1; r1++) {
                // For each row in Input_1 set the value of all its column variables
                argNo = 0;
                for (Column column : selectedColumns_I1) {
                    Object value = column.getObjectValue(r1);
                    args[argNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                if ((Boolean)conditionObj.invokeMethod("predicate", args)) {
                    for (int c = 0; c < colNo_I1; c++) {
                        outputData.column(c).appendObjectValue(inputData_I1.column(c).getObjectValue(r1));
                    }
                    for (int c = 0; c < colNo_I2; c++) {
                        outputData.column(colNo_I1 + c).appendObjectValue(inputData_I2.column(c).getObjectValue(r2));
                    }
                    matchFound = true;
                }
            }

            // This is right-join so even if there's no match between I2.r2 and I1, we need to add I2.r2 to the output.
            if (!matchFound) {
                for (int c = 0; c < colNo_I1; c++) {
                    outputData.column(c).appendObjectValue(MissingValue.get());
                }
                for (int c = 0; c < colNo_I2; c++) {
                    outputData.column(colNo_I1 + c).appendObjectValue(inputData_I2.column(c).getObjectValue(r2));
                }
            }
        }
    }


    /**
     * A naive O(n*m) implementation of the OUTER JOIN operation.
     * TODO: Find time and make it more efficient.
     *
     * @param conditionObj
     * @param inputData_I1
     * @param selectedColumns_I1
     * @param inputData_I2
     * @param selectedColumns_I2
     * @param outputData
     * @throws DataException
     */
    private static void _outerJoin(GroovyObject conditionObj, Data inputData_I1, ArrayList<Column> selectedColumns_I1, Data inputData_I2, ArrayList<Column> selectedColumns_I2, Data outputData)
            throws DataException
    {
        throw new UnsupportedOperationException();
    }


    /*
     * This code is called once when all of the data has passed through the block. 
     * It should be used to cleanup any resources that the block has made use of.
     */
    public void postExecute(BlockEnvironment env) throws Exception
    {
        System.out.println("Processed total of " + _inputData_I1.getLargestRows() + " row(s) of " + Input_A + ".");
        System.out.println("Processed total of " + _totalRows2 + " row(s) of " + Input_B + " in " + _totalChunks2 + " chunk(s).");
        if (_predicateValueMap != null) {
            System.out.println("Cache size: " + _predicateValueMap.size() + ".");
        } else {
            System.out.println("No cache used.");
        }
    }


    /**
     * A simple container class to allow arguments array as the key in a hash map.
     *
     */
    private static class ArgumentArray
    {
        final Object[] arguments;
        final int hashCode;

        public ArgumentArray(Object... args)
        {

            this.arguments = args;
            // This is going to be used as the Map key, so calculate hash once and eagerly.
            this.hashCode = Arrays.hashCode(args);
        }


        @Override
        public boolean equals(Object other)
        {
            return other instanceof ArgumentArray && Arrays.equals(arguments, ((ArgumentArray)other).arguments);
        }

        @Override
        public int hashCode()
        {
            return this.hashCode;
        }
    }
}
