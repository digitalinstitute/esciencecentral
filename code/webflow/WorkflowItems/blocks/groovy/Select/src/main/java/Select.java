/**
 * e-Science Central Copyright (C) 2008-2016 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.sun.corba.se.spi.orbutil.fsm.Input;
import groovy.lang.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.connexience.server.workflow.*;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

public class Select implements WorkflowBlock
{
    /**
     * This field refers to property 'Copy Input' defined in service.xml
     */
    private final static String Prop_EXPRESSION = "Expression";
    private final static String Prop_STATIC_COMPILATION = "Use Static Compilation";

    private final static String _EXP_ALL_COLS = "$[*]";
    //private final static String _EXP_ALL_NAMED_COLS = "$[#]";

    private final static String _ALL_COLS_VAR = "XAllColsMapX";
    //private final static String _ALL_NAMED_COLS_VAR = "XAllColsMapX";

    /**
     * This field refers to input port 'input-1' defined in service.xml
     */
    private final static String Input_INPUT_DATA = "input-data";
    
    /**
     * This field refers to output port 'output-1' defined in service.xml
     */
    private final static String Output_OUTPUT_DATA = "output-data";

    private GroovyClassLoader _classLoader;
    private GroovyObject _conditionObj;

    private HashMap<String, String> _col2var;
    private ArrayList<String> _selectedColumnNames;
    private String _sanitizedCondition;
    private boolean _useAllColumnMap;


    /**
     * This method is called when block execution is first started. It should be
     * used to setup any data structures that are used throughout the execution
     * lifetime of the block.
     */
    public void preExecute(BlockEnvironment env) throws Exception
    {
        _col2var = new HashMap<>(); // maps column names to groovy variable names

        String expression = env.getStringProperty(Prop_EXPRESSION, "");
        System.out.println("User expression:\n" + expression);

        //
        // Expression sanitization
        //
        StringBuilder sanitized = new StringBuilder();

        // Find all columns list references and replace them with the special map variable
        int lastHit = 0;
        int hit;
        while ((hit = expression.indexOf(_EXP_ALL_COLS, lastHit)) != -1) {
            sanitized.append(expression.substring(lastHit, hit));
            sanitized.append(_ALL_COLS_VAR);
            lastHit = hit + _EXP_ALL_COLS.length();
        }
        sanitized.append(expression.substring(lastHit));
        _useAllColumnMap = lastHit != 0;
        expression = sanitized.toString();
        sanitized = new StringBuilder();

        //// Find all columns map references and replace them with the special variable
        //sanitized = new StringBuilder();
        //lastHit = 0;
        //while ((hit = expression.indexOf(_EXP_ALL_NAMED_COLS, lastHit)) != -1) {
        //    sanitized.append(expression.substring(lastHit, hit));
        //    sanitized.append(_ALL_NAMED_COLS_VAR);
        //    lastHit = hit + _EXP_ALL_NAMED_COLS.length();
        //}
        //sanitized.append(expression.substring(lastHit));
        //boolean useAllColumnMap = lastHit != 0;

        // Find single column references and replace them with appropriate variables
        _selectedColumnNames = new ArrayList<>();

        // Sanitize variable names for the columns in INPUT_1
        Pattern columnPattern = Pattern.compile("\\$\\{.*?\\}");
        Matcher columnMatcher = columnPattern.matcher(expression);

        int varCnt = 0;
        int lastMatchEnd = 0;
        while (columnMatcher.find()) {
            String match = columnMatcher.group();
            String columnName = match.substring(2, match.length() - 1);
            String variableName = _col2var.get(columnName);
            if (variableName == null) {
                // For each new column name generate a unique and valid Groovy variable name
                variableName = "XVarX" + varCnt++;
                _col2var.put(columnName, variableName);
                _selectedColumnNames.add(columnName);
            }
            sanitized.append(expression.substring(lastMatchEnd, columnMatcher.start()));
            sanitized.append(variableName);
            lastMatchEnd = columnMatcher.end();
        }
        sanitized.append(expression.substring(lastMatchEnd));

        // For non static compilation we can build a Groovy script that embeds the user condition without data types.
        _classLoader = new GroovyClassLoader();

        if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("class Projection {\n    static Map generate(");
            // Always add all columns variable -- it can be null, though
            scriptText.append(_ALL_COLS_VAR);
            scriptText.append(", ");

            if (_selectedColumnNames.size() > 0) {
                for (String columnName : _selectedColumnNames) {
                    scriptText.append(_col2var.get(columnName));
                    scriptText.append(", ");
                }
            }

            scriptText.setLength(scriptText.length() - 2);

            scriptText.append(") {\n        ");
            scriptText.append(sanitized);
            scriptText.append("\n   }\n}");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        } else {
            _sanitizedCondition = sanitized.toString();
        }
    }


    private Object _getColumnNeutralElement(Column col) {
        if (col instanceof StringColumn) {
            return "";
        } else if (col instanceof IntegerColumn) {
            return 0L;
        } else if (col instanceof DoubleColumn) {
            return 0.0;
        } else if (col instanceof DateColumn) {
            return new Date(0);
        } else {
            throw new IllegalArgumentException("Unsupported column type " + col.getClass().toString() + " for column '" + col.getName() + "'");
        }
    }

    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs)
    throws Exception
    {
        Data inputData = inputs.getInputDataSet(Input_INPUT_DATA);

        ArrayList<Column> selectedColumns = new ArrayList<>();
        for (String columnName : _selectedColumnNames) {
            selectedColumns.add(inputData.column(columnName));
        }

        if (_conditionObj == null) {
            if (!env.getBooleanProperty(Prop_STATIC_COMPILATION, false)) {
                throw new IllegalStateException("Property '" + Prop_STATIC_COMPILATION + "' not set but the condition object is null");
            }

            // Build a Groovy script that embeds the user condition.
            StringBuilder scriptText = new StringBuilder();
            scriptText.append("@groovy.transform.CompileStatic\n");
            scriptText.append("class Projection {\n    static Map generate(");
            // Always add all columns variable -- it can be null, though
            scriptText.append("Map ");
            scriptText.append(_ALL_COLS_VAR);
            scriptText.append(", ");

            if (selectedColumns.size() > 0) {
                for (Column column : selectedColumns) {
                    if (String.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("String ");
                    } else if (Double.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Double ");
                    } else if (Integer.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Integer ");
                    } else if (Boolean.class.isAssignableFrom(column.getDataType())) {
                        scriptText.append("Boolean ");
                    }
                    scriptText.append(_col2var.get(column.getName()));
                    scriptText.append(", ");
                }
            }

            scriptText.setLength(scriptText.length() - 2);

            scriptText.append(") {\n        ");
            scriptText.append(_sanitizedCondition);
            scriptText.append("\n   }\n}");

            System.out.println("Sanitized Groovy script:\n" + scriptText.toString());

            Class conditionClass = _classLoader.parseClass(scriptText.toString());
            _conditionObj = (GroovyObject) conditionClass.newInstance();
        }

        //
        // The core of the SELECT operation
        //
        Data outputData = new Data();

        int rowNo = inputData.getLargestRows();
        if (rowNo == 0) {
            // This is tricky part because we need to run the projection somehow to get the correct number and type of
            // output columns. Passing null as the variable value may not work correct because in return the projection
            // may return null which we can only convert to one specific column type...
            //
            // [TODO] Think of a way to pass specific column type from the projection expression.
            LinkedHashMap<String, Object> allColumnMap = null;
            Object[] args = new Object[selectedColumns.size() + 1]; // +1 for the special all columns variable

            if (_useAllColumnMap) {
                allColumnMap = new LinkedHashMap<>();
                Enumeration enumCols = inputData.columns();
                while (enumCols.hasMoreElements()) {
                    Column col = (Column)enumCols.nextElement();
                    allColumnMap.put(col.getName(), _getColumnNeutralElement(col));
                }
            }
            int argsNo = 0;
            args[argsNo++] = allColumnMap;
            for (Column column : selectedColumns) {
                Object value = _getColumnNeutralElement(column);
                args[argsNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            LinkedHashMap projection = (LinkedHashMap) _conditionObj.invokeMethod("generate", args);
            Set<Map.Entry> entrySet = projection.entrySet();

            for (Map.Entry entry : entrySet) {
                Column col;
                if (entry.getValue() == null || entry.getValue() instanceof String) {
                    col = new StringColumn(entry.getKey().toString());
                } else if (entry.getValue() instanceof Long) {
                    col = new IntegerColumn(entry.getKey().toString());
                } else if (entry.getValue() instanceof Double) {
                    col = new DoubleColumn(entry.getKey().toString());
                } else {
                    throw new Exception("Unsupported column type: " + entry.getValue().getClass());
                }
                outputData.addColumn(col);
            }
        } else {
            // This is easy part, we run the projection on every row in the input set
            LinkedHashMap<String, Object> allColumnMap = null;
            Object[] args = new Object[selectedColumns.size() + 1]; // +1 for the special all columns variable

            // For the first row we need to be more careful and detect column types
            int row = 0;
            if (_useAllColumnMap) {
                allColumnMap = new LinkedHashMap<>();
                Enumeration enumCols = inputData.columns();
                while (enumCols.hasMoreElements()) {
                    Column col = (Column)enumCols.nextElement();
                    allColumnMap.put(col.getName(), col.getObjectValue(row));
                }
            }
            int argsNo = 0;
            args[argsNo++] = allColumnMap;
            for (Column column : selectedColumns) {
                Object value = column.getObjectValue(row);
                args[argsNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
            }

            LinkedHashMap projection = (LinkedHashMap) _conditionObj.invokeMethod("generate", args);
            Set<Map.Entry> entrySet = projection.entrySet();

            for (Map.Entry entry : entrySet) {
                Column col;
                if (entry.getValue() instanceof String || entry.getValue() instanceof MissingValue) {
                    col = new StringColumn(entry.getKey().toString());
                } else if (entry.getValue() instanceof Long) {
                    col = new IntegerColumn(entry.getKey().toString());
                } else if (entry.getValue() instanceof Double) {
                    col = new DoubleColumn(entry.getKey().toString());
                } else {
                    throw new Exception("Unsupported column type: " + entry.getValue().getClass());
                }
                outputData.addColumn(col);
                col.appendObjectValue(entry.getValue());
            }

            // For the remaining rows we simply append values to the columns.
            // [NOTE] The assumption is that for each row the user expression will return the same number and type of columns.
            for (row = 1; row < rowNo; row++) {
                if (_useAllColumnMap) {
                    allColumnMap = new LinkedHashMap<>();
                    Enumeration enumCols = inputData.columns();
                    while (enumCols.hasMoreElements()) {
                        Column col = (Column)enumCols.nextElement();
                        allColumnMap.put(col.getName(), col.getObjectValue(row));
                    }
                }

                argsNo = 0;
                args[argsNo++] = allColumnMap;
                for (Column column : selectedColumns) {
                    Object value = column.getObjectValue(row);
                    args[argsNo++] = (value == MissingValue.get() || value instanceof MissingValue ? null : value);
                }

                projection = (LinkedHashMap) _conditionObj.invokeMethod("generate", args);
                int c = 0;
                for (Object element : projection.values()) {
                    outputData.column(c++).appendObjectValue(element);
                }
            }
        }

        outputs.setOutputDataSet(Output_OUTPUT_DATA, outputData);
    }


    public void postExecute(BlockEnvironment env) throws Exception
    { }
}
