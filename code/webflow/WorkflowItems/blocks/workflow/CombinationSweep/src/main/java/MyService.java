/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.workflow.WorkflowDocument;
import com.connexience.server.model.workflow.WorkflowInvocationFolder;
import com.connexience.server.model.workflow.WorkflowParameterList;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MyService extends CloudDataProcessorService
{
	/**
	 * This is the main service execution routine. It is called once if the service has not been configured to accept
	 * streaming data or once for each chunk of data if the service has been configured to accept data streams
	 */
	public void execute() throws Exception
	{
		final boolean debug = getEditableProperties().booleanValue("DebugMode", false);

		final boolean transferData = getEditableProperties().booleanValue("TransferData", true);
		final boolean waitForWorkflow = getEditableProperties().booleanValue("WaitForWorkflow", false);

		final String targetBlockName = getEditableProperties().stringValue("TargetBlockName", "input");
		final String targetPropertyName = getEditableProperties().stringValue("TargetPropertyName", "Source");

		final boolean pauseForFailedWorkflows = getEditableProperties().booleanValue("PauseForFailedWorkflows", false);
		final boolean allowFailedSubworkflows = getEditableProperties().booleanValue("AllowFailedWorkflows", false);

		final APIBroker api = createApiLink();
		final DocumentRecord workflowFile = (DocumentRecord) getEditableProperties().xmlStorableValue("WorkflowFile");
		final WorkflowDocument workflowDocument = api.getWorkflow(workflowFile.getId());

		// Transfer data back to the server if required
		String transferFolderId = null;

		if (transferData)
		{
			transferFolderId = doDataTransfer(api);
		}

		final WorkflowLock lock = waitForWorkflow ? createWorkflowLock(allowFailedSubworkflows, pauseForFailedWorkflows) : null;

		final Data sweepParameters = getInputDataSet("parameter-data");

		// the bags of numbers to be combined
		final List<List<Integer>> bags = new ArrayList<List<Integer>>();

		// generate the bags of property values
		for (int i = 0; i < sweepParameters.getLargestRows(); i++)
		{
			final int startValue = Integer.parseInt(sweepParameters.column("start").getStringValue(i));
			final int stopValue = Integer.parseInt(sweepParameters.column("stop").getStringValue(i));
			final int stepValue = Integer.parseInt(sweepParameters.column("step").getStringValue(i));

			if (debug)
			{
				final String blockName = sweepParameters.column("block").getStringValue(i);
				final String parameterName = sweepParameters.column("name").getStringValue(i);
				final int valueCount = ((stopValue - startValue) / stepValue) + 1;

				System.out.printf("Block: '%s', Property: '%s', %d Values: %d...%d (step: %d)%n", blockName, parameterName, valueCount, startValue, stopValue, stepValue);
			}

			final List<Integer> sweepValues = new ArrayList<Integer>();

			for (int j = startValue; j <= stopValue; j += stepValue)
			{
				sweepValues.add(j);
			}

			bags.add(i, sweepValues);
		}

		final List<List<Integer>> combinations = new ArrayList<List<Integer>>();
		generateCombinations(bags, 0, new ArrayList<Integer>(), combinations);

		System.out.println("Parameter combinations: " + combinations.size());

		// Store IDs of launched workflows
		final Data invocations = new Data();
		invocations.addColumn(new StringColumn("InvocationID"));

		// launch required workflows
		for (final List<Integer> combination : combinations)
		{
			final WorkflowParameterList workflowParameters = new WorkflowParameterList();
			workflowParameters.add(targetBlockName, targetPropertyName, transferFolderId);

			final StringBuilder parameterString = new StringBuilder();

			// build parameter string
			for (int i = 0; i < combination.size(); i++)
			{
				final String blockName = sweepParameters.column("block").getStringValue(i);
				final String propertyName = sweepParameters.column("name").getStringValue(i);
				final Integer propertyValue = combination.get(i);

				// Add property to workflow
				workflowParameters.add(blockName, propertyName, propertyValue.toString());

				// Debugging
				parameterString.append(propertyName);
				parameterString.append("=");
				parameterString.append(propertyValue);
				parameterString.append(",");
			}

			// Debugging, remove last comma
			if (parameterString.length() > 0)
			{
				parameterString.setLength(parameterString.length() - 1);
			}

			if (debug)
			{
				System.out.println("Parameters: " + parameterString.toString());
			}

			// It's assumed that local check of 'waitForWorkflow' is much quicker
			// than remote call to 'executeWorkflow*' and thus the check can
			// be made multiple times without too much overhead.
			final WorkflowInvocationFolder invocation = waitForWorkflow ?
					executeWorkflowWithLock(workflowDocument, workflowParameters, lock, workflowDocument.getName() + ": " + parameterString.toString()) :
					executeWorkflow(workflowDocument, workflowParameters, workflowDocument.getName() + ": " + parameterString.toString());

			invocations.column(0).appendStringValue(invocation.getInvocationId());
		}

		setOutputDataSet("invocation-ids", invocations);
	}

	private void generateCombinations(final List<List<Integer>> bags, final int i, final List<Integer> stack, final List<List<Integer>> combinations)
	{
		if (i == bags.size())
		{
			combinations.add(stack);
		}
		else
		{
			for (int j = 0; j < bags.get(i).size(); j++)
			{
				final List<Integer> newStack = new ArrayList<Integer>();

				newStack.addAll(stack);
				newStack.add(bags.get(i).get(j));

				generateCombinations(bags, i + 1, newStack, combinations);
			}
		}
	}

	private String doDataTransfer(APIBroker api) throws Exception
	{
		String transferFolderName = getEditableProperties().stringValue("TransferFolderName", "transfer");
		FileWrapper inputFiles = (FileWrapper) getInputData("input-files");

		// Create a transfer folder for copying data
		Folder transferFolder = new Folder();
		transferFolder.setName(transferFolderName);
		transferFolder.setContainerId(getInvocationFolder().getId());
		transferFolder = api.saveFolder(transferFolder);

		// Upload all of the source files
		HashMap<String, Folder> subFolderMap = new HashMap<String, Folder>();
		File baseDir = inputFiles.getBaseDir();
		if (!baseDir.isAbsolute())
		{
			baseDir = new File(getWorkingDirectory(), baseDir.getPath());
		}

		for (String fileName : inputFiles.relativeFilePaths())
		{
			File file = new File(baseDir, fileName);
			if (!file.exists())
			{
				System.err.println("Cannot locate input file: " + fileName);
				System.err.println("\tbase dir: " + baseDir);
				System.err.println("\tcurrent dir: " + System.getProperty("user.dir"));
				continue;
			}

			Folder parentFolder = transferFolder;
			int i = 0;
			int j;
			while ((j = fileName.indexOf(File.separator, i)) > -1)
			{
				Folder subFolder;
				if (j > 0)
				{
					subFolder = subFolderMap.get(fileName.substring(0, j));
					if (subFolder == null)
					{
						subFolder = new Folder();
						subFolder.setName(fileName.substring(i, j));
						subFolder.setContainerId(parentFolder.getId());
						subFolder = api.saveFolder(subFolder);
						subFolderMap.put(fileName.substring(0, j), subFolder);
					}
					parentFolder = subFolder;
				}
				i = j + 1;
			}

			DocumentRecord uploadDoc = new DocumentRecord();
			uploadDoc.setName(fileName.substring(i, fileName.length()));
			uploadDoc = api.saveDocument(parentFolder, uploadDoc);
			api.uploadFile(uploadDoc, file);
		}

		return transferFolder.getId();
	}
}