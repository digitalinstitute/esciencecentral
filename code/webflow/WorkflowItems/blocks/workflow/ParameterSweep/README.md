# Parameter Sweep

This block runs multiple invocations of a workflow to perform a parameter sweep.
Parameter values are provided via the `parameter-data` input while mappings between
them and properties in the target workflow are taken from the `ParameterMapping`
property. The `parameter-data` input has the following format:

| Param1 | Param2 | Param3 | ... |
|:------:|:------:|:------:|:---:|
| 1      | 2      | A      | ... |
| 1      | 2      | B      | ... |
| ...    | ...    | ...    | ... |

For each row in this table the block starts one workflow invocation.
The parameters in this table can be mapped to blocks in the target workflow by means 
of a mapping table which is specified by the `ParameterMapping` property.
This property is a two column list of strings.
The first column contains the identifier for the block and property to set, whereas the
second column includes the column name in `parameter-data` which to read the property
value from.
For example, to set a property of `HiddenNeurons` for the block called `Model` using
the *Param1* column in the input data the following row in the `ParameterMapping` table would
be required:

 Block.Property       | Column Name
----------------------|-------------
`Model.HiddenNeurons` | `Param1`


## Inputs

* `parameter-data` -- The table of the parameter sweep values.
* `input-data` -- Files to be transferred to the target workflow.


## Outputs

* `invocation-ids` -- List of the invocation ids for the spawned workflows.


## Properties

* `AllowFailedWorkflows` -- Should execution proceed if there are any failed subworkflows.
* `ParameterMapping` -- Mapping table linking columns in the `parameter-data` input to properties in the child workflow's blocks.
* `PauseForFailedWorkflows` -- Should execution pause if there are failed subworkflows.
* `TargetBlockName` -- Name of the block in the child workflow to send the `input-data` files to.
* `TargetPropertyName` -- Property in the target block that will be configured with the folder used to transfer data to the child workflows.
* `WaitForWorkflow` -- Specifies whether the execution of this workflow should pause until all of the child workflows have completed.
* `WorkflowFile` -- The workflow to start to perform the sweep.
