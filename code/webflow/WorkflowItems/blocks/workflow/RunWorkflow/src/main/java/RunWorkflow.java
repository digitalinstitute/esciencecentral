/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.model.document.*;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.workflow.*;
import com.connexience.server.model.workflow.notification.WorkflowLock;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;

import java.io.File;
import java.util.HashMap;


public class RunWorkflow extends CloudDataProcessorService
{
    private static final String Prop_FAIL_ON_EMPTY = "FailOnEmpty";
    private static final String Prop_TARGET_BLOCK_NAME = "TargetBlockName";
    private static final String Prop_TARGET_PROPERTY_NAME = "TargetPropertyName";
    private static final String Prop_TRANSFER_FOLDER_NAME = "TransferFolderName";
    private static final String Prop_TRANSFER_DATA = "TransferData";
    private static final String Prop_WAIT_FOR_WORKFLOW = "WaitForWorkflow";
    private static final String Prop_PAUSE_FOR_FAILED_WORKFLOWS = "PauseForFailedWorkflows";
    private static final String Prop_ALLOW_FAILED_WORKFLOWS = "AllowFailedWorkflows";
    private static final String Prop_DATA_AS_ROWS = "DataAsRows";
    private static final String Prop_DATA_STRUCTURE = "DataStructure";
    private static final String Prop_WORKFLOW_FILE = "WorkflowFile";

    private static final String DS_ONE_COLUMN = "ONE_COLUMN_MODE";
    private static final String DS_TWO_COLUMN = "TWO_COLUMN_MODE";
    private static final String DS_THREE_COLUMN = "THREE_COLUMN_MODE";

    private static final String Input_PROPERTY_DATA = "property-data";
    private static final String Input_INPUT_FILES = "input-files";
    
    private static final String Output_INVOCATION_ID = "invocation-id";

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        String targetBlockName = getProperties().stringValue(Prop_TARGET_BLOCK_NAME, "").trim();
        if ("".equals(targetBlockName)) {
            throw new Exception("Property " + Prop_TARGET_BLOCK_NAME + " has not been set.");
        }

        String targetPropertyName = getProperties().stringValue(Prop_TARGET_PROPERTY_NAME, "").trim();
        if ("".equals(targetPropertyName)) {
            throw new Exception("Property " + Prop_TARGET_PROPERTY_NAME + " has not been set.");
        }

        // Load input property data
        Data propertyData = getInputDataSet(Input_PROPERTY_DATA);

        String dataStructure = getProperties().stringValue(Prop_DATA_STRUCTURE, "").trim();
        if ("".equals(dataStructure)) {
            throw new Exception("Property " + Prop_DATA_STRUCTURE + " has not been set.");
        }
        int dataStructNo;
        if (DS_ONE_COLUMN.equals(dataStructure)) {
            dataStructNo = 1;
        } else if (DS_TWO_COLUMN.equals(dataStructure)) {
            dataStructNo = 2;
        } else if (DS_THREE_COLUMN.equals(dataStructure)) {
            dataStructNo = 3;
        } else {
            throw new Exception("Invalid value for property " + Prop_DATA_STRUCTURE);
        }

        APIBroker api = createApiLink();

        // Prepare workflow parameters
        WorkflowParameterList params = _getWorkflowParameters(propertyData, dataStructNo, getProperties().booleanValue(Prop_DATA_AS_ROWS, false));
        if (getProperties().booleanValue(Prop_TRANSFER_DATA, false)) {
            String transferFolderId = doDataTransfer(api);
            // Set the target parameter value
            params.add(targetBlockName, targetPropertyName, transferFolderId);
        }

        DocumentRecord wfDoc = (DocumentRecord)getProperties().xmlStorableValue(Prop_WORKFLOW_FILE);
        WorkflowDocument wf = api.getWorkflow(wfDoc.getId());
        WorkflowInvocationFolder invocation;

        // Call in correct way depending on lock requirements
        if(getProperties().booleanValue(Prop_WAIT_FOR_WORKFLOW, false)){
            WorkflowLock lock = createWorkflowLock(
                    getProperties().booleanValue(Prop_ALLOW_FAILED_WORKFLOWS, false), 
                    getProperties().booleanValue(Prop_PAUSE_FOR_FAILED_WORKFLOWS, false));
            invocation = executeWorkflowWithLock(wf, params, lock, wf.getName() + " run #0");
        } else {
            invocation = executeWorkflow(wf, params, wf.getName() + " run #0");
        }

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        Data outputData = new Data();
        outputData.addSingleValue("InvocationID", invocation.getInvocationId());
        setOutputDataSet(Output_INVOCATION_ID, outputData);
    }


    private String doDataTransfer(APIBroker api) throws Exception
    {
        String transferFolderName = getEditableProperties().stringValue(Prop_TRANSFER_FOLDER_NAME, "").trim();
        if ("".equals(transferFolderName)) {
            throw new Exception("Property " + Prop_TRANSFER_FOLDER_NAME + " has not been set.");
        }
        FileWrapper inputFiles = (FileWrapper)getInputData(Input_INPUT_FILES);
        if (inputFiles.getFileCount() == 0 && getProperties().booleanValue(Prop_FAIL_ON_EMPTY, false)) {
            throw new Exception("No input files provided while property " + Prop_FAIL_ON_EMPTY + " was set.");
        }

        // Create a transfer folder for copying data
        Folder transferFolder = new Folder();
        transferFolder.setName(transferFolderName);
        transferFolder.setContainerId(getInvocationFolder().getId());
        transferFolder = api.saveFolder(transferFolder);

        // Upload all of the source files
        HashMap<String, Folder> subFolderMap = new HashMap<String, Folder>();
        File baseDir = inputFiles.getBaseDir();
        if (!baseDir.isAbsolute()) {
            baseDir = new File(getWorkingDirectory(), baseDir.getPath());
        }

        for (String fileName : inputFiles.relativeFilePaths()) {
            File file = new File(baseDir, fileName);
            if (!file.exists()) {
                System.err.println("Cannot locate input file: " + fileName);
                System.err.println("\tbase dir: " + baseDir);
                System.err.println("\tcurrent dir: " + System.getProperty("user.dir"));
                continue;
            }

            Folder parentFolder = transferFolder;
            int i = 0;
            int j;
            while ((j = fileName.indexOf(File.separator, i)) > -1) {
                Folder subFolder;
                if (j > 0) {
                    subFolder = subFolderMap.get(fileName.substring(0, j));
                    if (subFolder == null) {
                        subFolder = new Folder();
                        subFolder.setName(fileName.substring(i, j));
                        subFolder.setContainerId(parentFolder.getId());
                        subFolder = api.saveFolder(subFolder);
                        subFolderMap.put(fileName.substring(0, j), subFolder);
                    }
                    parentFolder = subFolder;
                }
                i = j + 1;
            }

            DocumentRecord uploadDoc = new DocumentRecord();
            uploadDoc.setName(fileName.substring(i, fileName.length()));
            uploadDoc = api.saveDocument(parentFolder, uploadDoc);
            api.uploadFile(uploadDoc, file);
        }

        return transferFolder.getId();
    }


    private WorkflowParameterList _getWorkflowParameters(Data propertyData, int dataStructure, boolean asRows)
    {
        WorkflowParameterList params = new WorkflowParameterList();

        int xxxRowNo = propertyData.getLargestRows();
        if (xxxRowNo == 0) {
            return params;
        }
        if (!asRows && xxxRowNo > 1) {
            throw new IllegalArgumentException("Multiple rows detected in the property data but property values meant to be placed in columns.");
        }

        if (dataStructure == 1) {
            if (asRows) {
                if (propertyData.getColumns() > 1) {
                    throw new IllegalArgumentException("Multiple columns detected in teh property data but property values meant to be placed in rows.");
                }
                Column c = propertyData.column(0);
                for (int r = 0; r < c.getRows(); r++) {
                    params.add(_parse_1ColData(c.getStringValue(r)));
                }
            } else {
                for (int c = 0; c < propertyData.getColumns(); c++) {
                    if (propertyData.column(c).isMissing(0)) {
                        // Stop reading row rowNo if a Missing value has been detected.
                        break;
                    }
                    params.add(_parse_1ColData(
                            propertyData.column(c).getStringValue(0)));
                }
            }
        } else if (dataStructure == 2) {
            if (asRows) {
                if (propertyData.getColumns() != 2) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s with %s set requires exactly 2 columns", propertyData.getColumns(), DS_TWO_COLUMN, Prop_DATA_AS_ROWS));
                }
                Column c0 = propertyData.column(0);
                Column c1 = propertyData.column(1);
                for (int r = 0; r < c0.getRows(); r++) {
                    if (c1.isMissing(r)) {
                        throw new IllegalArgumentException(
                                String.format("Invalid property data: missing property value for %s in row: %d", c0.getStringValue(r), r));
                    }
                    params.add(_parse_2ColData(c0.getStringValue(r), c1.getStringValue(r)));
                }
            } else {
                if (propertyData.getColumns() % 2 != 0) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s requires an even number", propertyData.getColumns(), DS_TWO_COLUMN));
                }

                for (int c = 0; c < propertyData.getColumns(); c += 2) {
                    if (propertyData.column(c).isMissing(0)) {
                        // Stop reading row rowNo if a Missing value has been detected.
                        break;
                    }
                    if (propertyData.column(c + 1).isMissing(0)) {
                        throw new IllegalArgumentException(
                                String.format("Invalid property data: missing property value for %s in row: %d, column: %d", 
                                        propertyData.column(c).getStringValue(0), 0, c));
                    }
                    params.add(_parse_2ColData(
                            propertyData.column(c).getStringValue(0), 
                            propertyData.column(c + 1).getStringValue(0)));
                }
            }
        } else if (dataStructure == 3) {
            if (asRows) {
                if (propertyData.getColumns() != 3) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s with %s set requires exactly 3 columns", propertyData.getColumns(), DS_THREE_COLUMN, Prop_DATA_AS_ROWS));
                }
                Column c0 = propertyData.column(0);
                Column c1 = propertyData.column(1);
                Column c2 = propertyData.column(2);
                for (int r = 0; r < c0.getRows(); r++) {
                    if (c1.isMissing(r) || c2.isMissing(r)) {
                        throw new IllegalArgumentException(
                                String.format("Invalid property data: missing property value for %s in row: %d", c0.getStringValue(r), r));
                    }
                    params.add(new WorkflowParameter(
                            c0.getStringValue(r), 
                            c1.getStringValue(r),
                            c2.getStringValue(r)));
                }

            } else {
                if (propertyData.getColumns() % 3 != 0) {
                    throw new IllegalArgumentException(
                            String.format("Invalid number of columns: %d. Mode %s requires a number divisible by 3", propertyData.getColumns(), DS_THREE_COLUMN));
                }

                for (int c = 0; c < propertyData.getColumns(); c += 3) {
                    if (propertyData.column(c).isMissing(0)) {
                        // Stop reading the row if the block name column has missing value in that row
                        break;
                    }
                    if (propertyData.column(c + 1).isMissing(0) ||
                            propertyData.column(c + 2).isMissing(0)) {
                        throw new IllegalArgumentException("Invalid property data: missing value in row: %d, column: %d");
                    }
                    params.add(new WorkflowParameter(
                            propertyData.column(c).getStringValue(0),
                            propertyData.column(c + 1).getStringValue(0),
                            propertyData.column(c + 2).getStringValue(0)));
                }
            }
        }

        return params;
    }
    
    private WorkflowParameter _parse_2ColData(String blockProp, String value)
    {
        int dot = blockProp.indexOf('.');
        if (dot <= 0 || dot >= blockProp.length() - 1) {
            throw new IllegalArgumentException("Invalid value of the block-property column: " + blockProp + "; Expected: BlockName.PropertyName");
        }

        return new WorkflowParameter(
                blockProp.substring(0, dot),
                blockProp.substring(dot + 1),
                value);
    }

    private WorkflowParameter _parse_1ColData(String blockPropValue)
    {
        int dot = blockPropValue.indexOf('.');
        int eq = blockPropValue.indexOf('=');
        if (dot <= 0 || eq <= dot + 1) {
            throw new IllegalArgumentException("Invalid value of the block-property-value column: " + blockPropValue + "; Expected: BlockName.PropertyName=Value");
        }

        return new WorkflowParameter(
                blockPropValue.substring(0, dot),
                blockPropValue.substring(dot + 1, eq),
                blockPropValue.substring(eq + 1));
    }
}