/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import com.sun.org.apache.xerces.internal.xs.StringList;
import org.apache.commons.io.IOUtils;
import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.MissingValue;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.manipulation.ColumnPicker;
import org.pipeline.core.data.manipulation.ColumnPickerCollection;
import org.pipeline.core.data.manipulation.DataTransposer;
import org.pipeline.core.data.text.TextColumnSplitter;

import java.io.*;
import java.lang.String;
import java.util.ArrayList;

public class ConcatenateDataSets extends CloudDataProcessorService {
    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {

        //Transpose the data from columns into rows?
        Boolean transpose = getEditableProperties().booleanValue("Transpose", true);

        //Include the original filenames which came from the forked workflows?
        Boolean includeFilenames = getEditableProperties().booleanValue("IncludeFilenames", true);

        //Which column to include in the output - only supports one
        String columnSelection = getEditableProperties().stringValue("ColumnSelection", "#0");

        //Input files
        FileWrapper inputFiles = (FileWrapper) getInputData("file-list");

        Data invAndFileIds = getInputDataSet("invocation-ids");

        //Create the output dataset
        Data outputData = new Data();

        //If we're not transposing then create the single column which will hold the results
        if (!transpose) {
            StringColumn results = new StringColumn("Results");
            outputData.addColumn(results);
        }

        //List of the transposed data sets.  Needed to calculate the maximum size to create the output data
        ArrayList<Data> transposedDataSets = new ArrayList<Data>();

        for (int i = 0; i < inputFiles.getFileCount(); i++) {

            //import the file into a Dataset object
            File inputFile = inputFiles.getFile(i);
            DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();
            importer.setImportColumnNames(false);
            importer.setDataStartRow(1);  //No labels
            importer.setForceTextImport(true); //Always use text import
            Data thisData = importer.importFile(inputFile);

            //Get the desired column.  Uses Collection as it returns Data rather than Column which is easier to join
            ColumnPickerCollection pickers = new ColumnPickerCollection();
            pickers.setPickersCopyData(false);
            pickers.populateFromStringArray(new String[]{columnSelection});
            Data selected = pickers.extractData(thisData);

            if (transpose) {


                DataTransposer transposer = new DataTransposer(selected);
                transposer.setNamesPicker(new ColumnPicker("#0"));  //ColumnPicker not used
                transposer.setExtractNamesColumn(false);
                transposer.setIncludeOriginalNames(false); //Don't add column names
                Data transposedData = transposer.transpose();
                transposedDataSets.add(transposedData); //add to the collection of files
            } else {
                if (includeFilenames) {
                    Data filenameRow = new Data();
					Column fileIds = invAndFileIds.column(1);
                    filenameRow.addSingleValue("Results", fileIds.getStringValue(i));   //Add the filename to the output data
                    outputData.appendRows(filenameRow, false);
                }
                outputData.appendRows(selected, false); //Add the data at the bottom of the data set
            }
        }

        //If we're transposing then create the output data
        if (transpose) {

            //Find the larget size data from each of the files as they may be different sizes
            int maxSize = 0;
            for (Data tData : transposedDataSets) {
                if (tData.getColumns() > maxSize) {
                    maxSize = tData.getColumns();
                }
            }

            //create the correct sized output data
            for (int i = 0; i < maxSize; i++) {
                outputData.addColumn(new StringColumn());
            }

            //Append the values from each file dataset
            for (Data tData : transposedDataSets) {
                //For each column
                for (int i = 0; i < outputData.getColumns(); i++) {
                    //Append the data or a MISSING value to keep the set the correct size
                    if (i < tData.getColumns()) {
                        outputData.column(i).appendObjectValue(tData.column(i).getObjectValue(0));
                    } else {
                        outputData.column(i).appendObjectValue(MissingValue.get());
                    }
                }
            }
            if (includeFilenames) {
				Column fileIds = invAndFileIds.column(1);
                outputData.insertColumn(0, fileIds); //Add the filenams column to the first column
            }
        }

        setOutputDataSet("concatenated-data", outputData);
    }
}