# Entry point for users octave service.
# This file gets called once each time
# a chunk of data is passed through the
# service. It should therefore not modify
# the environment status. It is executed
# as a script and therefore can access and
# set any global variables that are needed.

lv = properties.LatentVariables;
[b,p,q,w,t,u,inner,Xres,yres] = mvplsnipals(x.numerical,y.numerical,lv);
ypred = mvpredict(x.numerical,b,lv);
ypredicted.numerical=ypred;