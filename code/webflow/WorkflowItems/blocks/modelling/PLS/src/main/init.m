# This file gets called once when all of
# the data has been passed through the octave
# service. It is not strictly needed, but
# can be used to tidy up and write final
# results files to the workflow directory
# prior to subsequent service invocations