
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import java.io.File;
import org.pipeline.core.data.*;
import org.pipeline.core.data.io.DelimitedTextDataImporter;
import org.pipeline.core.data.manipulation.MissingValueRemover;
import org.pipeline.core.data.manipulation.NumericalColumnExtractor;

public class StandardRegressionDatasets extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        String dataset = getEditableProperties().stringValue("Dataset", null);
        if(dataset!=null){
            File dataFile = getLibraryItem().getFile("data" + File.separator + dataset + ".csv");
            if(dataFile.exists()){
                DelimitedTextDataImporter importer = new DelimitedTextDataImporter();
                Data rawData = importer.importFile(dataFile);
                Data importedData;
                
                // Extract numerical
                if(getEditableProperties().booleanValue("OnlyNumericalData", true)){
                    NumericalColumnExtractor extractor = new NumericalColumnExtractor(rawData);
                    importedData = extractor.copyColumns();
                } else {
                    importedData = rawData;
                }
                
                // Remove missing
                if(getEditableProperties().booleanValue("StripMissing", true)){
                    MissingValueRemover remover = new MissingValueRemover(importedData);
                    setOutputDataSet("data", remover.removeMissingRows());
                } else {
                    setOutputDataSet("data", importedData);
                }
                               
            } else {
                throw new Exception("Dataset: " + dataset + " does not exist");
            }
            
        } else {
            throw new Exception("No dataset selected");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}