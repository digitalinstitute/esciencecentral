/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;

import org.pipeline.core.data.*;
import org.pipeline.apps.neuralnetwork.ffann.*;
import org.pipeline.core.matrix.*;
import org.pipeline.core.xmlstorage.*;
import org.pipeline.core.xmlstorage.io.*;
import org.pipeline.core.util.*;

import java.io.FileInputStream;


public class MyService extends CloudDataProcessorService {
    private FFANNModel model = null;
    
    /** This method is call when a service is about to be started. It is called once
     * regardless of whether or not the service is streaming data. Code that is
     * needed to set up information that needs to be preserved over multiple
     * chunks should be executed here. */
    public void executionAboutToStart() throws Exception {
        FileWrapper modelFile = (FileWrapper)getInputData("model-file");
        if (modelFile.getFileCount() != 1) {
            throw new Exception("JavaFFANNPrecdict only operates on a single model file.");
        }

        FileInputStream stream = new FileInputStream(modelFile.getFile(0));
        XmlDataStoreStreamReader reader = new XmlDataStoreStreamReader(stream);
        XmlDataStore holder = reader.read();
        model = (FFANNModel)holder.xmlStorableValue("NeuralNetwork");
    }

    /** This is the main service execution routine. It is called once if the service
    * has not been configured to accept streaming data or once for each chunk of
    * data if the service has been configured to accept data streams */
    public void execute() throws Exception {
        if(model!=null){
	        Data xData = getInputDataSet("x");
	        Matrix x = new DataToMatrixConverter(xData).toMatrix();
	        Matrix yest = model.getPredictions(x);
            Data outData = new MatrixToDataConverter(yest).toData(); 
            setOutputDataSet("y-pred", outData);
        } else {
            throw new Exception("No model file loaded");
        }
    }

    /** All of the data has been passed through the service. Any clean up code
    * should be placed here */
    public void allDataProcessed() throws Exception {

    }
}