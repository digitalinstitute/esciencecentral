package com.connexience.services.test;

import java.util.Date;

import com.connexience.server.workflow.cloud.services.*;


public class SleepIO extends CloudDataProcessorService
{
    private static final String Prop_MIN_SLEEP_TIME = "Minimum Sleep Time";
    private static final String Prop_MAX_SLEEP_TIME = "Maximum Sleep Time";
    private static final String Prop_REPORT_PROGRESS = "Report Progress";

    private static final String Input_FILES = "files";
    private static final String Input_DATA  = "data";
    private static final String Input_REFS  = "references";

    private static final String Output_FILES = "files";
    private static final String Output_DATA  = "data";
    private static final String Output_REFS  = "references";

    private static final long ProgressReportInterval = 5_000L;

    /**
     * This is the main service execution routine. It is called once if the service
     * has not been configured to accept streaming data or once for each chunk of
     * data if the service has been configured to accept data streams
     */
    public void execute() throws Exception
    {
        long startTime = System.currentTimeMillis();

        String minStr = getProperties().stringValue(Prop_MIN_SLEEP_TIME, "");
        String maxStr = getProperties().stringValue(Prop_MAX_SLEEP_TIME, "");
        int min, max;
        try {
            min = Integer.parseInt(minStr);
        } catch (NumberFormatException x) {
            min = _parseTimeSpanInMillis(minStr);
        }
        try {
            max = Integer.parseInt(maxStr);
        } catch (NumberFormatException x) {
            max = _parseTimeSpanInMillis(maxStr);
        }

        if (max < min) {
            throw new IllegalArgumentException("Value of " + Prop_MAX_SLEEP_TIME + " must be greater or equal to " + Prop_MIN_SLEEP_TIME);
        }

        // First, copy inputs as this may consume some time
        if (isInputConnected(Input_FILES)) {
            setOutputData(Output_FILES, getInputData(Input_FILES));
        }
        if (isInputConnected(Input_DATA)) {
            setOutputDataSet(Output_DATA,  getInputDataSet(Input_DATA));
        }
        if (isInputConnected(Input_REFS)) {
            setOutputData(Output_REFS,  getInputData(Input_REFS));
        }

        // Then, calculate how long we need to sleep
        long sleepTime = (long)(Math.random() * (max - min) + 0.5) + min;
        System.out.println(String.format("Sleep time set to: %.3fs", sleepTime / 1000.0));

        // And then, sleep however long is needed
        if (getProperties().booleanValue(Prop_REPORT_PROGRESS, false)) {
            _waitWithProgress(startTime, sleepTime);
        } else {
            // But make correction for all the time we spent executing already
            sleepTime -= System.currentTimeMillis() - startTime;
            if (sleepTime > 0) {
                Thread.sleep(sleepTime);
            }
        }

        System.out.println(String.format("Sleep completed in: %.3fs", (System.currentTimeMillis() - startTime) / 1000.0));
    }


    private void _waitWithProgress(long startTime, long sleepTimeInMillis)
    throws Exception
    {
        long finishTime = startTime + sleepTimeInMillis;
        long currentTime;

        while ((currentTime = System.currentTimeMillis()) < finishTime) {
            System.out.println(new Date().toString() + ": " + ((currentTime - startTime) / 1000) + "s");
            Thread.sleep(Math.min(ProgressReportInterval, Math.max(0, finishTime - System.currentTimeMillis())));
        }
    }


    private int _parseTimeSpanInMillis(String timeSpan)
    {
        int colon = timeSpan.lastIndexOf(':');
        try {
            // Parse seconds and fractions
            if (colon == -1) {
                return (int)(Double.parseDouble(timeSpan) * 1000 + 0.5);
            }

            int millis = (int)(Double.parseDouble(timeSpan.substring(colon + 1)) * 1000 + 0.5);

            // Parse minutes
            int lastColon = colon;
            colon = timeSpan.lastIndexOf(':', lastColon - 1);
            if (colon == -1) {
                return Integer.parseInt(timeSpan.substring(0, lastColon)) * 60_000 + millis;
            }
            millis += Integer.parseInt(timeSpan.substring(colon + 1, lastColon)) * 60_000;

            // Parse hours
            lastColon = colon;
            colon = timeSpan.lastIndexOf(':', lastColon - 1);
            if (colon != -1) {
                throw new IllegalArgumentException("Cannot parse time span " + timeSpan + ". Expected format is [hh:mm:ss.nn]");
            }
            return Integer.parseInt(timeSpan.substring(colon + 1, lastColon)) * 3_600_000 + millis;
        } catch (NumberFormatException x) {
            throw new IllegalArgumentException("Cannot parse time span " + timeSpan + ". Expected format is [hh:mm:ss.nn]");
        }
    }
}
