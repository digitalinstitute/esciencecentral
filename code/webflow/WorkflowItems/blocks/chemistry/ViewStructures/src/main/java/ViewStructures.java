
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import java.io.File;
import java.io.FileInputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.ColumnPicker;

public class ViewStructures implements WorkflowBlock {
    SMILESWriter writer;

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {
        int width = env.getIntProperty("Width", 300);
        int height = env.getIntProperty("Height", 300);
        writer = new SMILESWriter(width, height);   
    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        Data inputData = inputs.getInputDataSet("struture-data");
        
        String columnName = env.getStringProperty("SMILESColumn", "#0");
        String baseFileName = env.getStringProperty("FileName", "Structure_");
        
        ColumnPicker picker = new ColumnPicker(columnName);
        picker.setCopyData(false);
        Column smilesColumn = picker.pickColumn(inputData);

        String smiles;
        ArrayList<File> results = new ArrayList<File>();
        File f;
        NumberFormat fmt = NumberFormat.getInstance();
        int digits = env.getIntProperty("FileNameDigits", 4);
        fmt.setMaximumIntegerDigits(digits);
        fmt.setMinimumIntegerDigits(digits);
        fmt.setMaximumFractionDigits(0);
        fmt.setGroupingUsed(false);
        for(int i=0;i<smilesColumn.getRows();i++){
            if(!smilesColumn.isMissing(i)){
                smiles = smilesColumn.getStringValue(i); 
                try {
                    writer.parseSmiles(smiles);
                    
                    // Write to temp file
                    f = new File(env.getWorkingDirectory(), baseFileName + fmt.format(i) + ".jpg");
                    writer.write(f);
                    results.add(f);
                } catch (Exception e){
                    e.printStackTrace();
                    System.out.println("Couldn't render: " + smiles);
                }
            }   
        }       
        outputs.setOutputFilesWithFullPath("structure-images", results);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }
}