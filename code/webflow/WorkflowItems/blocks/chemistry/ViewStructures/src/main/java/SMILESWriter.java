/*
 * SMILESWriter.java
 */

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.exception.InvalidSmilesException;
import org.openscience.cdk.geometry.GeometryTools;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.interfaces.IMolecule;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.renderer.Renderer2D;
import org.openscience.cdk.renderer.Renderer2DModel;
import org.openscience.cdk.smiles.SmilesParser;

/**
 * This class writes a smiles structure to a jpeg file
 * @author nhgh
 */
public class SMILESWriter {
    public IAtomContainer atomContainer;
    public Renderer2DModel r2dm;
    public Renderer2D renderer;
    int width;
    int height;
    
    public SMILESWriter(int width, int height) {

        this.atomContainer = atomContainer;
        this.width = width;
        this.height = height;
        
        r2dm = new Renderer2DModel();
        r2dm.setBackgroundDimension(new Dimension(width, height));
        renderer = new Renderer2D(r2dm);        
    }
    
    public void parseSmiles(String smiles) throws Exception {
        SmilesParser parser = new SmilesParser(DefaultChemObjectBuilder.getInstance());
        IMolecule temp = parser.parseSmiles(smiles);        
        StructureDiagramGenerator g = new StructureDiagramGenerator();
        g.setMolecule(temp);
        g.generateCoordinates();
        atomContainer = g.getMolecule();
    }
    
    public void setAtomContainer(IAtomContainer atomContainer)
    {
        this.atomContainer = atomContainer;
    }    
    
    public void write(File file)
    {

        if (atomContainer != null) {
            
 
            GeometryTools.translateAllPositive(atomContainer,r2dm.getRenderingCoordinates());
            GeometryTools.scaleMolecule(atomContainer, r2dm.getBackgroundDimension(), 0.8,r2dm.getRenderingCoordinates());
            GeometryTools.center(atomContainer, r2dm.getBackgroundDimension(),r2dm.getRenderingCoordinates());
            //renderer.paintMolecule(atomContainer, (Graphics2D)graphics,false,true);
            BufferedImage image = new BufferedImage(width, height, ColorSpace.TYPE_RGB);
            Graphics2D g2 = image.createGraphics();
            g2.setColor(Color.WHITE);
            g2.fillRect(0, 0, width, height);
            renderer.paintMolecule(atomContainer, g2, false, true);
            try {
                ImageIO.write(image, "JPEG", file);
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }    
}