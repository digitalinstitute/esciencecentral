/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.test.*;
import org.junit.*;
import static org.junit.Assert.*;

/**
 * This class acts as a JUnit test for the block created by maven, It requires a 
 * test directory in the packaged resources with input files with the same names
 * as the block inputs in order to function
 * @author hugo
 */
public class MyServiceTest {
    private BlockTester harness;
    
    
    public MyServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        
        
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        harness = new BlockTester();
        try {
            harness.setup();
        } catch (Exception e){
            fail("Error setting up class: " + e.getMessage());
        }
    }
    
    @After
    public void tearDown() {
        try {
            harness.deleteInvocationDirectory();
        } catch (Exception e){
            fail("Error cleaning up: " + e.getMessage());   
        }
    }
    
    @Test
    public void executeBlock(){
        try {
            harness.execute();
        } catch (Exception e){
            fail("Error executing block: " + e.getMessage());
        }
    }    
}
