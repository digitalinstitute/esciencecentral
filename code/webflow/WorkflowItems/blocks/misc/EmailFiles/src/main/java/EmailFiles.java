
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.service.DataProcessorService;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.MultiPartEmail;

import java.io.File;


public class EmailFiles extends DataProcessorService {


    public void execute() throws Exception {

        System.out.println("EmailFiles.execute");
        APIBroker api = createApiLink();


        String smtpHost = getProperties().stringValue("SMTPHost", "smtp.googlemail.com");
        int smtpPort = getProperties().intValue("SMTPPort", 465);
        String smtpUser = getProperties().stringValue("SMTPUser", "esciencecentral@googlemail.com");
        String smtpPassword = getProperties().stringValue("SMTPPassword", "");

        String fromAddress = getProperties().stringValue("FromAddress", "esciencecentral@googlemail.com");
        String fromName = getProperties().stringValue("FromName", "e-Science Central");

        String recipient;
        boolean sendToCurrentUser = getProperties().booleanValue("SendToCurrentUser", true);
        if (sendToCurrentUser) {
            //todo: how to get the email address?
            //Get user logon name
            System.out.println("Sending to current User: " + api.getCurrentUser().getDisplayName());
            throw new Exception("Not yet implemented");
        } else {
            //Send to specified recipient
            recipient = getProperties().stringValue("To", "");
        }

        // Create the email message
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName(smtpHost);
        email.setSmtpPort(smtpPort);
        email.setSSLOnConnect(true);
        email.addTo(recipient);
        email.setFrom(fromAddress, fromName);
        email.setSubject("xBIM Toolkit Analysis results");
        email.setMsg("Analysis results\n\nAttached are the result of your submission to the xBIM Toolkit validation service.\n\n");
        email.setBoolHasAttachments(true);


        email.setAuthenticator(new DefaultAuthenticator(smtpUser, smtpPassword));


        FileWrapper files = (FileWrapper) getInputData("input-files");
        for (File file : files) {
            // Create the attachment
            EmailAttachment attachment = new EmailAttachment();
            attachment.setPath(file.getName());
            attachment.setDisposition(EmailAttachment.ATTACHMENT);
            attachment.setName(file.getName());

            email.attach(attachment);
        }

        email.send();

    }


}