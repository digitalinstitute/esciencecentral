import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;

import java.io.Serializable;


public class SparkTest implements Serializable {


    public void test() {
        String logFile = "/Users/nsjw7/Desktop/TC1a/HalfHourlyDataSource.csv"; // Should be some file on your system
        SparkConf conf = new SparkConf().setAppName("Simple Application");
        conf.setMaster("local[1]");

        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<String> logData = sc.textFile(logFile).cache();

        long numAs = logData.filter(new Function<String, Boolean>() {
            public Boolean call(String s) {
                return s.contains("a");
            }
        }).count();

        long numBs = logData.filter(new Function<String, Boolean>() {
            public Boolean call(String s) {
                return s.contains("b");
            }
        }).count();

        System.out.println("Lines with a: " + numAs + ", lines with b: " + numBs);

        sc.stop();
    }

    public static void main(String[] args) {
        SparkTest test = new SparkTest();
        test.test();
    }
}
