
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.BlockEnvironment;
import com.connexience.server.workflow.BlockInputs;
import com.connexience.server.workflow.BlockOutputs;
import com.connexience.server.workflow.WorkflowBlock;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;

import java.util.Arrays;

public class BubbleSort implements WorkflowBlock {

    @Override
    public void preExecute(BlockEnvironment env) throws Exception {

    }

    @Override
    public void execute(BlockEnvironment env, BlockInputs inputs, BlockOutputs outputs) throws Exception {
        Data inputData = inputs.getInputDataSet("input-data");
        Data outputData = new Data();

        for(int i=0; i < inputData.getColumns(); i++ ){
            DoubleColumn col = (DoubleColumn) inputData.column(i);
            double[] data = col.getDoubleArray();
            bubbleSort(data);

            System.out.println("data = " + Arrays.toString(data));

            DoubleColumn outputColumn = new DoubleColumn(data);
            outputData.addColumn(outputColumn);
            for(int j = 0; j < outputColumn.getRows(); j++){
                System.out.println(outputColumn.getStringValue(j));
            }
        }

        outputs.setOutputDataSet("output-data", outputData);
    }

    @Override
    public void postExecute(BlockEnvironment env) throws Exception {

    }


    public void bubbleSort(double[] arr) {
        boolean swapped = true;
        double j = 0.0;
        double tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {
                if (arr[i] > arr[i + 1]) {
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
    }

}