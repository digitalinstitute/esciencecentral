
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;

public class FailWorkflow extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {

    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        boolean alwaysFail = getEditableProperties().booleanValue("AlwaysFail", true);
        double pFail = getEditableProperties().doubleValue("FailureProbability", 0.5);
        if(alwaysFail){
            System.out.println("Fail workflow: AlwaysFail");
            throw new Exception("Workflow failure triggered");
        } else {
            double failValue = Math.random();
            if(failValue<pFail){
                System.out.println("Fail workflow: " + failValue + "<pFail=" + pFail);
                throw new Exception("Workflow failure triggered");
            } else {
                // Copy files through
                System.out.println("Running workflow");
                FileWrapper inData = (FileWrapper)getInputData("input-files");
                setOutputData("output-files", inData);
            }
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}