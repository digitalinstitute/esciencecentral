
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.ConnexienceException;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class WordCountReduce extends CloudDataProcessorService {


    public void execute() throws Exception {

        FileWrapper inputFiles = (FileWrapper) getInputData("input-file");
        if (inputFiles.getFileCount() != 1) {
            throw new ConnexienceException("Reduce can only operate on one file");
        }

        File f = inputFiles.getFile(0);
        BufferedReader br = new BufferedReader(new FileReader(f));
        String line;
        int count = 0;
        String word = "";
        while ((line = br.readLine()) != null) {
            String[] words = line.split(",");
            if(words.length != 2)
            {
                System.err.println("Ignoring invalid line " + line);
                continue;
            }

            Integer value = Integer.parseInt(words[1]);
            word = words[0];
            count = count + value;
        }

        br.close();


        File outputFile = new File("reduced-" + word + ".txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile));
        bw.write(word + "," + count + "\n");

        bw.flush();
        bw.close();

        FileWrapper outputFiles = new FileWrapper();
        outputFiles.addFile(outputFile);
        setOutputData("output-file", outputFiles);
    }

    private void addToMap(Map<String, Integer> counts, String word) {
        if (counts.containsKey(word)) {
            counts.put(word, counts.get(word) + 1);
        } else {
            counts.put(word, 1);
        }
    }
}