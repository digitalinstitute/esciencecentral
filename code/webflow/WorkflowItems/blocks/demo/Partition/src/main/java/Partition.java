
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.DataWrapper;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import org.pipeline.core.data.*;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Partition extends CloudDataProcessorService {


    public void execute() throws Exception {

        Data inputData = getInputDataSet("input-data");
        FileWrapper outputFiles = new FileWrapper();

        Column keysCol = inputData.column(0);
        Column valuesCol = inputData.column(1);

        Set<String> fileNames = new HashSet<String>();

        for (int i = 0; i < inputData.getLargestRows(); i++) {

            String key = keysCol.getStringValue(i);
            String value = valuesCol.getStringValue(i);

            File splitFile = new File(key);
            BufferedWriter bw = new BufferedWriter(new FileWriter(splitFile,true));
            bw.write(key + "," + value + "\n");
            bw.flush();
            bw.close();
            fileNames.add(key);
        }

        for(String filename : fileNames){
            outputFiles.addFile(new File(filename));
        }
        setOutputData("output-file", outputFiles);
    }


}