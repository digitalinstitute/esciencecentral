/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

package cogtool.actr;

import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.StringListWrapper;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.columns.StringColumn;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActrExecute extends CloudDataProcessorService {
	private static final String ACTR_VISUAL_ATTENTION = "ActrVisualAttention";

	private static final String ACTR_MOTOR_INITIATION = "ActrMotorInitiation";

	private static final String ACTR_PECK_FITTS_COEFFICIENT = "ActrPeckFittsCoefficient";

	private static final String ACTR_DAT = "ActrDat";

	// information that needs to be preserved over multiple chunks should be executed here.
	public void executionAboutToStart() throws Exception {

	}

	// Clean-up after all data (chunks) have been processed
	public void allDataProcessed() throws Exception {

	}

	// called once for non-streaming blocks or once per chunk for streaming blocks
	public void execute() throws Exception {
		final File lispLibDir = new File(getDependencyItem("actr6-lisp").getOriginalUnpackedDir(), "lib");

		// File wrappers
		final FileWrapper inputWrapper = (FileWrapper) getInputData("input-model");
		final FileWrapper outputWrapper = new FileWrapper(getWorkingDirectory());

		// ACT-R Lisp File
		final String actrScriptLocation = new File(lispLibDir, "actr6.lisp").getAbsolutePath();

		// CLISP parameters (from eSC block)
		final String clispBinary = getEditableProperties().getPropertyString("ClispBinary");
		final String clispEncoding = getEditableProperties().getPropertyString("ClispEncoding");
		final String clispCommand = getEditableProperties().getPropertyString("ClispCommand");
		final StringListWrapper additionalParameters = (StringListWrapper) getEditableProperties().xmlStorableValue("ClispAdditionalParameters");

		// ACT-R parameters (from eSC block)
		final Map<String, Integer> actrParameters = new HashMap<>();
		actrParameters.put(ACTR_VISUAL_ATTENTION, getEditableProperties().intValue(ACTR_VISUAL_ATTENTION, 85));
		actrParameters.put(ACTR_MOTOR_INITIATION, getEditableProperties().intValue(ACTR_MOTOR_INITIATION, 50));
		actrParameters.put(ACTR_PECK_FITTS_COEFFICIENT, getEditableProperties().intValue(ACTR_PECK_FITTS_COEFFICIENT, 75));
		actrParameters.put(ACTR_DAT, getEditableProperties().intValue(ACTR_DAT, 50));

		// DEBUG parameters
		final boolean debug = getEditableProperties().booleanValue("DebugMode", false);

		// Build CLISP command line
		final List<String> clispParameters = new ArrayList<>();

		// quiet
		clispParameters.add("-q");

		// clispEncoding
		clispParameters.add("-E");
		clispParameters.add(clispEncoding);

		// memory file
		clispParameters.add("-i");
		clispParameters.add(actrScriptLocation);

		// lisp file
		for (final File modelFile : inputWrapper) {
			modifyModel(modelFile, actrParameters);

			if (debug) {
				debugParameters(modelFile);
			}

			clispParameters.add("-i");
			clispParameters.add(modelFile.getAbsolutePath());
		}

		// initial clispCommand
		clispParameters.add("-x");
		clispParameters.add(clispCommand);

		// add additional parameters from block properties
		for (int i = 0; i < additionalParameters.getSize(); i++) {
			clispParameters.add(additionalParameters.getValue(i));
		}

		CommandLine commandline = CommandLine.parse(clispBinary);
		for (final String param : clispParameters) {
			commandline.addArgument(param);
		}

		// Executor
		DefaultExecutor exec = new DefaultExecutor();

		// Set CWD to ACTR lib so relative includes work
		exec.setWorkingDirectory(lispLibDir);

		// Output and Error files
		final File outputFile = new File(getWorkingDirectory(), "actr-output.txt");
		final File errorFile = new File(getWorkingDirectory(), "actr-error.txt");

		// Output and Error streams
		FileOutputStream outputStream = new FileOutputStream(outputFile);
		FileOutputStream errorStream = new FileOutputStream(errorFile);

		// Capture SYSOUT and SYSERR
		PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream, errorStream);
		exec.setStreamHandler(streamHandler);

		// Execute...
		System.out.println("Executing: " + commandline.toString());
		int exitCode = exec.execute(commandline);
		outputStream.flush();
		outputStream.close();
		System.out.println("Process Exited with Status Code: " + exitCode);

		if (exitCode != 0)
		{
			System.out.println("Output:");
			System.out.println("FileUtils.readFileToString(outputFile) = " + FileUtils.readFileToString(outputFile));
			System.out.println();
			System.out.println("Error:");
			System.out.println("FileUtils.readFileToString(errorFile) = " + FileUtils.readFileToString(errorFile));
			System.out.println();
		}

		// Add process output to block output
		outputWrapper.addFile(outputFile);
		setOutputData("output-file", outputWrapper);

		// Create Data wrapper with ACT-R parameters
		final Data parameters = new Data();
		parameters.addColumn(new StringColumn("Name"));
		parameters.addColumn(new StringColumn("Value"));

		for (final Map.Entry<String, Integer> entry : actrParameters.entrySet()) {
			parameters.column("Name").appendStringValue(entry.getKey());
			parameters.column("Value").appendStringValue(entry.getValue().toString());
		}

		parameters.column("Name").appendStringValue("TimeTaken");
		parameters.column("Value").appendStringValue(extractTimeTaken(outputFile));

		setOutputDataSet("output-parameters", parameters);
	}

	private void modifyModel(final File inputFile, final Map<String, Integer> actrParameters) throws Exception {
		// Read LISP file into String
		String input = FileUtils.readFileToString(inputFile);

		// Regex to find decimal values \d+(\.\d+)?
		final String valueExpression = "\\s+(\\d+(\\.\\d+)?)";

		// map eSC block properties to lisp names
		final Map<String, String> lispParameters = new HashMap<>();
		lispParameters.put(ACTR_VISUAL_ATTENTION, ":visual-attention-latency");
		lispParameters.put(ACTR_MOTOR_INITIATION, ":motor-initiation-time");
		lispParameters.put(ACTR_PECK_FITTS_COEFFICIENT, ":peck-fitts-coeff");
		lispParameters.put(ACTR_DAT, ":dat");

		// For each property to set
		for (final String actrParameter : actrParameters.keySet()) {
			final String lispParameter = lispParameters.get(actrParameter);

			final Pattern pattern = Pattern.compile(lispParameter + valueExpression, Pattern.DOTALL);
			Matcher matcher = pattern.matcher(input);

			// If property is found, calculate double value and replace it
			if (matcher.find()) {
				final int intValue = actrParameters.get(actrParameter);
				final double doubleValue = ((double) intValue / 1000);

				input = matcher.replaceFirst(lispParameter + " " + doubleValue);
			}
		}

		// Write back modified LISP file contents
		FileUtils.writeStringToFile(inputFile, input);
	}

	private String extractTimeTaken(final File outputFile) throws Exception
	{
		final List<String> lines = FileUtils.readLines(outputFile);

		if (lines.isEmpty())
		{
			throw new IllegalArgumentException(outputFile.getAbsolutePath() + " is empty.");
		}

		final String lastLine = lines.get(lines.size() - 1);

		try
		{
			return Double.valueOf(lastLine).toString();
		}
		catch (final Exception e)
		{
			throw new IllegalArgumentException("Could not parse time taken from ACTR output as a double", e);
		}
	}

	private void debugParameters(final File modelFile) throws Exception {
		final String haystack = FileUtils.readFileToString(modelFile);

		//(setq *overridden-global-parameters* `(:visual-attention-latency 0.085 :motor-initiation-time 0.050 :peck-fitts-coeff 0.075 :dat 0.050 ,@*overridden-global-parameters*))
		final String needle = "setq\\s+\\*overridden-global-parameters\\*\\s+.*\\*overridden-global-parameters\\*";

		final Pattern pattern = Pattern.compile(needle, Pattern.DOTALL);
		final Matcher matcher = pattern.matcher(haystack);

		if (matcher.find()) {
			System.out.println("overridden-global-parameters: '" + matcher.group() + "'");
		} else {
			System.out.println("Could not find overridden-global-parameters in ACT-R model file.");
		}
	}
}