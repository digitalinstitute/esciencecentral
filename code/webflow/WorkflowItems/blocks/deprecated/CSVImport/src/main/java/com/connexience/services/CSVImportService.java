/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.services;

import com.connexience.server.ConnexienceException;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.workflow.api.APIBroker;
import com.connexience.server.workflow.cloud.services.CloudDataProcessorService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.pipeline.core.data.Data;
import org.pipeline.core.data.io.DelimitedTextDataImporter;
import org.pipeline.core.data.io.DelimitedTextDataStreamImporter;
import org.pipeline.core.data.manipulation.BestGuessDataTyper;
import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * E-sc block to directly import the contents of a CSV file to a datawrapper.
 *
 * @author hugo, modified by Dominic 27/3/14
 * 
 */
public class CSVImportService extends CloudDataProcessorService {

    @Override
    public void execute() throws Exception {

        InputStream inStream = null;
        try {
            XmlDataStore properties = getProperties();

            // Load the data from the organisation data store
            DelimitedTextDataStreamImporter importer = new DelimitedTextDataStreamImporter();
            APIBroker api = createApiLink();

            // Set the properties
            int startRow = getEditableProperties().intValue("StartRow", 1);
            int labelRow = getEditableProperties().intValue("LabelRow", 1);
            boolean containsLabels = getEditableProperties().booleanValue("ContainsLabels", true);
            String columnDelimiter = getEditableProperties().stringValue("Delimiter", ",");
            int endRow = getEditableProperties().intValue("EndRow", 1);
            boolean limitRows = getEditableProperties().booleanValue("LimitRows", false);
            boolean subsample = getEditableProperties().booleanValue("Subsample", false);
            int sampleInterval = getEditableProperties().intValue("SampleInterval", 1);
            boolean textData = getEditableProperties().booleanValue("TextOnly", false);
            int importChunkSize = getEditableProperties().intValue("ImportChunkSize", 5000);
            boolean guessData = getEditableProperties().booleanValue("GuessDataTypes", true);

            importer.setDataStartRow(startRow);
            importer.setDataEndRow(endRow);
            importer.setDelimiterString(columnDelimiter);
            importer.setDelimiterType(DelimitedTextDataImporter.CUSTOM_DELIMITED);
            importer.setErrorsAsMissing(true);
            importer.setImportColumnNames(containsLabels);
            importer.setLimitRows(limitRows);
            importer.setNameRow(labelRow);
            importer.setSampleInterval(sampleInterval);
            importer.setSubsample(subsample);
            importer.setUseEnclosingQuotes(true);
            DocumentRecord doc = (DocumentRecord) properties.xmlStorableValue("Source");


            if (doc.getCurrentArchiveStatus() != DocumentRecord.UNARCHIVED_ARCHIVESTATUS) {
                throw new ConnexienceException("Input file (" + doc.getName() + ") is archived.  Retrieve before running workflow.");
            }

            File tempFile = createTempFile("download");
            api.downloadToFile(doc, tempFile);
            inStream = new FileInputStream(tempFile);

            importer.setForceTextImport(textData);
            importer.setChunkSize(importChunkSize);
            importer.resetWithInputStream(inStream);
            int chunkCount = 0;
            int rowCount = 0;
            BestGuessDataTyper guesser;


            Data data;
            Data guessedData = null;
            String dataName = doc.getName();

            while (!importer.isFinished()) {

                data = importer.importNextChunk();

                if (guessData) {
                    guesser = new BestGuessDataTyper(data);
                    guessedData = guesser.guess();
                    guessedData.createEmptyProperties();
                    guessedData.setName(dataName);
                    setOutputDataSet("imported-data", guessedData);
                } else {
                    data.createEmptyProperties();
                    data.setName(dataName);
                    setOutputDataSet("imported-data", data);
                }
                chunkCount++;
                rowCount = rowCount + data.getLargestRows();
            }

            System.out.println("Imported: " + rowCount + " rows of data in: " + chunkCount + " chunk(s)");

            // Save the last directory into the global properties
            Folder lastDir = new Folder();
            lastDir.setId(doc.getContainerId());
            getGlobalProperties().add("LastImportDir", lastDir);

        } finally {
            if (inStream != null) {
                try {
                    inStream.close();
                } catch (IOException ex) {
                }
            }
        }
    }
}
