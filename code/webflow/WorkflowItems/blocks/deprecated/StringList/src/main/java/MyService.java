/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.xmlstorage.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

public class MyService extends CloudDataProcessorService
{
    private static final String Prop_VALUES = "Values";
    private static final String Prop_AS_ROWS = "AsRows";
    //private static final String Prop_WITH_HEADER = "WithHeader";

    private static final String Output_STRING_LIST = "string-list";
    
  /**
   * This is the main service execution routine. It is called once if the service
   * has not been configured to accept streaming data or once for each chunk of
   * data if the service has been configured to accept data streams
   */
  public void execute() throws Exception
  {
    StringListWrapper list = (StringListWrapper)getEditableProperties().xmlStorableValue(Prop_VALUES);
    Data results = new Data();
    
    if (getProperties().booleanValue(Prop_AS_ROWS, false)) {
        StringColumn col = new StringColumn("Values");
        for (int i = 0; i < list.getSize(); i++) {
            col.appendStringValue(list.getValue(i));
        }
        results.addColumn(col);
    } else {
        for (int i = 0; i < list.getSize(); i++) {
            StringColumn col = new StringColumn("Value-" + i);
            col.appendStringValue(list.getValue(i));
            results.addColumn(col);
        }
    }

    setOutputDataSet(Output_STRING_LIST, results);
  }
}