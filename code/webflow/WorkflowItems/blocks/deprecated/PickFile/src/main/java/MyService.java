/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.util.WildcardUtils;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.engine.datatypes.*;
import org.pipeline.core.data.*;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        boolean pickByName = getEditableProperties().booleanValue("PickByName", false);
        String file = getEditableProperties().stringValue("File", "0");
        
        FileWrapper files = (FileWrapper)getInputData("input-files");
        FileWrapper selectedFile = new FileWrapper(getWorkingDirectory());
        FileWrapper remainingFiles = new FileWrapper(getWorkingDirectory());
        
        String fileName = null;
        if(pickByName){
            System.out.println("Selecting by name: " + file);
            for(int i=0;i<files.getFileCount();i++){
                if(WildcardUtils.wildCardMatch(files.getFile(i).getName(), file)){
                    System.out.println("Selecting: " + files.getFile(i));
                    selectedFile.addFile(files.getFile(i), false);
                } else {
                    System.out.println("Not selecting: " + files.getFile(i));
                    remainingFiles.addFile(files.getFile(i), false);
                }
            }
        } else {
            int index = Integer.parseInt(file);
            System.out.println("Selecting by index: " + index);
            if(index>=0 && index<files.getFileCount()){
                for(int i=0;i<files.getFileCount();i++){
                    if(i==index){
                        selectedFile.addFile(files.getFile(i), false);
                    } else {
                        remainingFiles.addFile(files.getFile(i), false);
                    }
                }
            } else {
                throw new Exception("File index out of bounds");
            }
        }
        
        if(selectedFile.getFileCount()>0){
            setOutputData("selected-file", selectedFile);
            setOutputData("remaining-files", remainingFiles);
        } else {
            throw new Exception("File: " + file + " not present in input files");
        }
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}