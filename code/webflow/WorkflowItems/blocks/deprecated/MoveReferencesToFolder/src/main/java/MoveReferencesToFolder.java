
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.ServerObject;
import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.folder.Folder;
import com.connexience.server.model.security.User;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.LinkPayloadItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import org.pipeline.core.data.*;

public class MoveReferencesToFolder extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();

        Folder targetFolder;
        if(getEditableProperties().booleanValue("MoveToHomeDirectory", false)){
            // Subfolder of home
            User u = api.getCurrentUser();
            targetFolder = api.getFolder(u.getHomeFolderId());
        } else {
            // Subfolder of target dir
            Folder propFolder = (Folder)getEditableProperties().xmlStorableValue("TargetFolder");
            targetFolder = api.getFolder(propFolder.getId());                     
        }
        
        if(getEditableProperties().booleanValue("CreateSubdirectory", false)){
            // Create a subdir
            targetFolder = api.getSubdirectory(targetFolder, getEditableProperties().stringValue("SubdirectoryName", "subdir"));
        }        
        
        
        LinkWrapper inputLinks = (LinkWrapper)getInputData("input-references");
        LinkPayloadItem item;
        ServerObject obj;
        DocumentRecord retrievedDoc;
        Folder retrievedFolder;
        LinkWrapper outputLinks = new LinkWrapper();
        
        for(int i=0;i<inputLinks.size();i++){
            item = inputLinks.getItem(i);
            obj = item.getServerObject();
            if(obj instanceof Folder){
                retrievedFolder = api.getFolder(obj.getId());
                retrievedFolder.setContainerId(targetFolder.getId());
                retrievedFolder = api.saveFolder(retrievedFolder);
                outputLinks.addFolder(retrievedFolder);
                
            } else if(obj instanceof DocumentRecord){
                retrievedDoc = api.getDocument(obj.getId());
                retrievedDoc = api.saveDocument(targetFolder, retrievedDoc);
                outputLinks.addDocument(retrievedDoc, api.getLatestVersion(retrievedDoc.getId()));
            }
        }

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputData("moved-references", outputLinks);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}