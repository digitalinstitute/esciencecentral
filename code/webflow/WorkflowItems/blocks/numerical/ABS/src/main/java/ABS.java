
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;

public class ABS extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        Data inputData = getInputDataSet("input-data");
        Data results = inputData.getCopy();
        int rows;
        
        for(int i=0;i<results.getColumns();i++){
            if(results.column(i) instanceof NumericalColumn){
                if(results.column(i) instanceof IntegerColumn){
                    IntegerColumn col = (IntegerColumn)results.column(i);
                    rows = col.getRows();
                    for(int j=0;j<rows;j++){
                        if(!col.isMissing(j)){
                            col.setLongValue(j, Math.abs(col.getLongValue(j)));
                        }
                    }
                } else if(results.column(i) instanceof DoubleColumn){
                    DoubleColumn col = (DoubleColumn)results.column(i);
                    rows = col.getRows();
                    for(int j=0;j<rows;j++){
                        if(!col.isMissing(j)){
                            col.setDoubleValue(j, Math.abs(col.getDoubleValue(j)));
                        }
                    }
                }
            }
        }
        
        setOutputDataSet("abs", results);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}