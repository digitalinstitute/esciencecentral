
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.apps.filters;

/**
 * Provides outlier screening for a set of readings
 * @author hugo
 */
public class OutlierScreeningFilter {
    private double[] window;
    private double currentMean;
    private int windowLength;
    private double currentStd;
    private int returnedValueOffset = 2;
    private double detectionLimit = 2.0;
    private boolean firstSample = true;
    private double previousValue;
    private double filterConstant = 0.6;
    
    public OutlierScreeningFilter(int windowLength) {
        this.windowLength = windowLength;
        window = new double[this.windowLength];
    }

    public void setDetectionLimit(double detectionLimit) {
        this.detectionLimit = detectionLimit;
    }

    public void setReturnedValueOffset(int returnedValueOffset) {
        this.returnedValueOffset = returnedValueOffset;
    }

    public void setFilterConstant(double filterConstant) {
        this.filterConstant = filterConstant;
    }
    
    public synchronized double filterValue(double value){
        if(!firstSample){
            addSampleToWindow(value);
            previousValue = value;
        } else {
            populateWindow(value);
            firstSample = false;
        }
        
        currentMean = calculateWindowMean();
        currentStd = calculateWindowStd();
        double candidateValue = window[window.length - returnedValueOffset];

        if(Math.abs(candidateValue - currentMean) > (currentStd * detectionLimit)){
            previousValue = (filterConstant * previousValue) + ((1 - filterConstant) * currentMean);
            return previousValue;
        } else {
            previousValue = (filterConstant * previousValue) + ((1 - filterConstant) * candidateValue);
            return previousValue;
        }
    }
    
    private synchronized void addSampleToWindow(double value){
        for(int i=1;i<window.length;i++){
            window[i - 1] = window[i];
        }
        window[window.length - 1] = value;
    }
    
    private synchronized void populateWindow(double value){
        for(int i=0;i<window.length;i++){
            window[i] = value;
        }
    }
    
    private synchronized double calculateWindowMean(){
        double sum = 0;
        for(int i=0;i<window.length;i++){
            sum+=window[i];
        }
        return sum / window.length;
    }
    
    private synchronized double calculateWindowStd(){
        double mean = calculateWindowMean();
        double ssq = 0;
        for(int i=0;i<window.length;i++){
            ssq = ssq + Math.pow(window[i] - mean, 2);
        }
        return Math.sqrt(ssq / (window.length - 1));
    }
}
