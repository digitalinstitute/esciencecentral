/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.services;

import com.connexience.server.workflow.cloud.services.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.manipulation.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.maths.*;
import java.util.*;

public class ScaleService extends CloudDataProcessorService {

    public void execute() throws Exception {
        Data inData = getInputDataSet("input-data");
        Vector numericalColumns = new NumericalColumnExtractor(inData).extractColumns();
        Data outData = new Data();
        DoubleColumn scaledCol;
        DoubleColumn original;
        ScalingInformation parameters = new ScalingInformation(numericalColumns.size());
        UnitVarianceColumnScaler scaler;
        int count = 0;
        
        Enumeration e = numericalColumns.elements();
        while(e.hasMoreElements()){
            original = (DoubleColumn)e.nextElement();
            scaler = new UnitVarianceColumnScaler(original);
            scaledCol = scaler.scaledColumn();
            parameters.setMean(count, scaler.getMean());
            parameters.setStd(count, scaler.getStd());
            outData.addColumn(scaledCol);
            count++;
        }
        
        // Set the outputs
        setOutputDataSet("scaled-data", outData);
        setOutputData("scaling-parameters", new com.connexience.server.workflow.engine.datatypes.ObjectWrapper( parameters));        
        
    }
}