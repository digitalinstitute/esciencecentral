package com.connexience.services;

import org.pipeline.core.drawing.DataType;

/**
 * This is the data type information for the scaling parameters object
 * @author hugo
 */
public class ScalingInformationDataType extends DataType {
    
    /** Creates a new instance of ScalingInformationDataType */
    public ScalingInformationDataType() {
        super("ScalingParameters", "Set of scaling parameters", ScalingInformation.class);
    }
}
