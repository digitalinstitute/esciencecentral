/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.*;
import com.connexience.server.model.metadata.types.DateMetadata;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.DateColumn;
import org.pipeline.core.data.columns.DoubleColumn;
import org.pipeline.core.data.columns.IntegerColumn;
import org.pipeline.core.data.columns.StringColumn;
import org.pipeline.core.data.manipulation.ColumnPicker;

public class MyService extends CloudDataProcessorService {

    public void executionAboutToStart() throws Exception {
    }

    public void execute() throws Exception {
        String categoryName = getEditableProperties().stringValue("Category", "");
        boolean useCategory = getEditableProperties().booleanValue("UseCategory", false);
        String metadataItem = getEditableProperties().stringValue("MetadataItem", "");
        String referenceColumn = getEditableProperties().stringValue("ReferenceColunn", "#0");
        boolean setOutputCategory = getEditableProperties().booleanValue("SetOutputCategory", false);
        String outputCategory = getEditableProperties().stringValue("OutputCategory", "");
        boolean errorIfNoMatch = getEditableProperties().booleanValue("ErrorIfNoMatch", true);
        MetadataCollection mdc = getInputMetadata("input-files");
        
        MetadataItem inputItem = null;
        if(useCategory){
            inputItem = mdc.find(categoryName, metadataItem);
        } else {
            inputItem = mdc.find(metadataItem);
        }
        
        if(inputItem!=null){
            ColumnPicker picker = new ColumnPicker(referenceColumn);
            Data referenceData = getInputDataSet("meta-data");
            Column refCol = picker.pickColumn(referenceData);
            if(refCol!=null){
                // Try and find a matching value
                int rowIndex = -1;
                for(int i=0;i<refCol.getRows();i++){
                    if(!refCol.isMissing(i) && refCol.getStringValue(i).equals(inputItem.getStringValue())){
                        rowIndex = i;
                    }
                }
                
                MetadataItem newItem;
                for(int i=0;i<referenceData.getColumns();i++){
                    newItem = createItemForDataValue(referenceData.column(i), rowIndex, useCategory, categoryName);
                    if(newItem!=null){
                        mdc.add(newItem);
                    }
                }
                
                setOutputData("output-files", getInputData("input-files"));
                if(rowIndex!=-1){
                    // Send the output metadata
                    setOutputMetadata("output-files", mdc);
                    
                } else {
                    // No metadata
                    if(errorIfNoMatch){
                        throw new Exception("No matching metadata");
                    } else {
                        System.out.println("No matching metadata");
                    }
                }
                
            } else {
                throw new Exception("Column: " + referenceColumn + " does not exist in input metadata data set");
            }
            
        } else {
            throw new Exception("Metadata item: " + metadataItem + " cannot be found in input data");
        }
        
    }

    private MetadataItem createItemForDataValue(Column c, int rowIndex, boolean useCategory, String category) throws Exception {
        MetadataItem item;
        if(!c.isMissing(rowIndex)){
            if(c instanceof DoubleColumn){
                item = new NumericalMetadata();
                ((NumericalMetadata)item).setDoubleValue(((DoubleColumn)c).getDoubleValue(rowIndex));
                
            } else if(c instanceof IntegerColumn){
                item = new NumericalMetadata();
                ((NumericalMetadata)item).setLongValue(((IntegerColumn)c).getLongValue(rowIndex));
                
            } else if(c instanceof DateColumn){
                item = new DateMetadata();
                ((DateMetadata)item).setDateValue(((DateColumn)c).getDateValue(rowIndex));
                
            } else {
                item = new TextMetadata();
                ((TextMetadata)item).setTextValue(((StringColumn)c).getStringValue(rowIndex));
            }
            
            item.setName(c.getName().replace(" ", "_"));
            if(useCategory){
                item.setCategory(category);
            }
            return item;
            
        } else {
            // Missing data
            return null;
        }
    }
    
    public void allDataProcessed() throws Exception {
    }
}