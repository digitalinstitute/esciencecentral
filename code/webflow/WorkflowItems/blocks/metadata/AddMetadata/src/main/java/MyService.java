/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.FileWrapper;
import com.connexience.server.workflow.xmlstorage.StringPairListWrapper;
import org.pipeline.core.data.*;

public class MyService extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        FileWrapper inputData = (FileWrapper) getInputData("input-data");

        // Get the input metadata
        MetadataCollection mdc = new MetadataCollection();

        mdc.debugPrint();

        // Add the metadata from the list
        String category = getEditableProperties().stringValue("Category", "General");
        StringPairListWrapper values = (StringPairListWrapper)getEditableProperties().xmlStorableValue("Values");
        TextMetadata tmd;

        for(int i=0;i<values.getSize();i++){
            tmd = new TextMetadata();
            tmd.setCategory(category);
            tmd.setName(values.getValue(i, 0));
            tmd.setTextValue(values.getValue(i, 1));
            mdc.add(tmd);
        }
        setOutputMetadata("output-data", mdc);
        setOutputData("output-data", inputData);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}