
/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */

import com.connexience.server.model.document.DocumentRecord;
import com.connexience.server.model.metadata.MetadataCollection;
import com.connexience.server.model.metadata.MetadataItem;
import com.connexience.server.model.metadata.types.NumericalMetadata;
import com.connexience.server.model.metadata.types.TextMetadata;
import com.connexience.server.workflow.cloud.services.*;
import com.connexience.server.workflow.api.*;
import com.connexience.server.workflow.engine.datatypes.DocumentRecordLinkItem;
import com.connexience.server.workflow.engine.datatypes.LinkWrapper;
import org.pipeline.core.data.*;

public class AttachMetadataToReferences extends CloudDataProcessorService {

    /**
     * This method is call when a service is about to be started. It is called
     * once regardless of whether or not the service is streaming data. Code
     * that is needed to set up information that needs to be preserved over
     * multiple chunks should be executed here.
     */
    public void executionAboutToStart() throws Exception {
    }

    /**
     * This is the main service execution routine. It is called once if the
     * service has not been configured to accept streaming data or once for each
     * chunk of data if the service has been configured to accept data streams
     */
    public void execute() throws Exception {
        APIBroker api = createApiLink();
        String category = getEditableProperties().stringValue("Category", "");
        boolean useCategory;
        if (category == null || category.isEmpty()) {
            useCategory = false;
        } else {
            useCategory = true;
        }
        String namesColReference = getEditableProperties().stringValue("NamesColumn", "#0");
        String valuesColReference = getEditableProperties().stringValue("ValuesColumn", "#1");
        String categoryColReference = getEditableProperties().stringValue("CategoryColumn", "#0");
        boolean hasCategoryColumn = getEditableProperties().booleanValue("ContainsCategoryColumn", false);
        boolean convertTypes = getEditableProperties().booleanValue("AttemptTypeConversion", false);

        // The following method is used to get a set of input data
        // and should be configured using the name of the input
        // data to fetch
        Data inputData = getInputDataSet("metadata-values");
        LinkWrapper references = (LinkWrapper) getInputData("file-references");

        MetadataCollection mdc = new MetadataCollection();
        Column namesColumn = inputData.pickColumn(namesColReference);
        Column valuesColumn = inputData.pickColumn(valuesColReference);
        Column categoryColumn = null;
        if (hasCategoryColumn) {
            categoryColumn = inputData.pickColumn(categoryColReference);
        }

        String name;
        String value;
        MetadataItem md;
        for (int i = 0; i < inputData.getLargestRows(); i++) {
            if (!namesColumn.isMissing(i) && !valuesColumn.isMissing(i)) {
                name = namesColumn.getStringValue(i);
                value = valuesColumn.getStringValue(i);

                if (value != null && !value.isEmpty()) {
                    if (convertTypes) {
                        try {
                            // Parse number
                            double numberValue = Double.parseDouble(value);
                            md = new NumericalMetadata();
                            ((NumericalMetadata) md).setDoubleValue(numberValue);

                        } catch (Exception e) {
                            // Set as string
                            md = new TextMetadata();
                            ((TextMetadata) md).setTextValue(value);
                        }
                    } else {
                        md = new TextMetadata();
                        ((TextMetadata) md).setTextValue(value);
                    }

                    md.setName(name);

                    // Sort out correct categories
                    if (hasCategoryColumn && categoryColumn != null) {
                        if (!categoryColumn.isMissing(i)) {
                            md.setCategory(categoryColumn.getStringValue(i).trim());
                        }
                    } else {
                        if (useCategory) {
                            md.setCategory(category);
                        } else {
                            md.setCategory("");
                        }
                    }
                    mdc.add(md);
                }
            }
        }

        DocumentRecord doc;
        for (int i = 0; i < references.size(); i++) {
            if (references.getItem(i) instanceof DocumentRecordLinkItem) {
                doc = ((DocumentRecordLinkItem) (references.getItem(i))).getDocument();
                api.uploadMetadata(doc, mdc);
            }
        }

        // The following should be used to pass output data sets
        // to a specified output connection. In this case it
        // just copies the output data
        setOutputData("file-references", references);
    }

    /**
     * All of the data has been passed through the service. Any clean up code
     * should be placed here
     */
    public void allDataProcessed() throws Exception {
    }
}