#!/bin/sh

for arg in `ls -d */`
do
	cd ${arg}
	echo `pwd`
	./buildall.sh
	cd ..
done
