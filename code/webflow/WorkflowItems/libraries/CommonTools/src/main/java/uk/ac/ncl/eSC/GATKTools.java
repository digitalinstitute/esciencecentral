package uk.ac.ncl.eSC;

import java.util.List;

import org.pipeline.core.data.Data;


public abstract class GATKTools
{
    public static String RF_ColumnName = "Read Filter";


    public static void prepareReadFilterArguments(Data readFiltersData, List<String> arguments)
    {
        if (readFiltersData.getColumns() < 1 || !RF_ColumnName.equals(readFiltersData.column(0).getName())) {
            throw new IllegalArgumentException("Invalid read filters data. Consider using GATK_ReadFilter_* blocks to build read filter data");
        }

        int rowNo = readFiltersData.column(0).getRows();
        if (rowNo > 0) {
            arguments.add("-rf");

            int colNo = readFiltersData.getColumns();
            for (int r = 0; r < rowNo; r++) {
                for (int c = 0; c < colNo && !readFiltersData.column(c).isMissing(r); c++) {
                    String value = readFiltersData.column(c).getStringValue(r).trim();
                    if ("".equals(value)) {
                        continue;
                    }

                    arguments.add(value);
                }
            }
        }
    }
}
