/*
 * EC50Curve.java
 */
package com.connexience.apps.ec50;

import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;
import org.pipeline.core.data.maths.*;
import org.pipeline.core.optimisation.*;
import org.pipeline.core.xmlstorage.XmlDataStore;
import java.util.*;

/**
 * This class represents an EC50 dose response curve
 * @author hugo
 */
public class EC50Curve implements OptimisableObject {
    public enum FitType {
        EVERYTHING,
        SLOPEANDEC50,
        EC50ONLY,
        TOPBOTTOMEC50
    }
    
    private FitType fitMode = FitType.TOPBOTTOMEC50;
    
    /** Observed data */
    private Data data;
    
    private double fixedBottom = 0.0;
    private double fixedTop = 0.0;
    private double fixedEc50 = 0.0;
    private double fixedSlope = 0.0;
    
    public double getPrediction(double bottom, double top, double ec50, double slope, double x){
        //return bottom + ((top - bottom) / (1 + Math.pow(10, (ec50 - x))));
        return bottom + (top - bottom)/(1+ Math.pow(10, ((ec50-x)*slope))) ;
        //Y=Bottom + (Top-Bottom)/(1+10^((LogEC50-X)*HillSlope)) 
    }
    
    @Override
    public boolean canSupplyInitialParameters() {
        return true;
    }

    @Override
    public double getCost(OptimisedParameterVector opv) {
        try {
            double bottom;
            double top;
            double ec50;
            double slope;

            if(fitMode==FitType.EVERYTHING){
                bottom = opv.get(0);
                top = opv.get(1);
                ec50 = opv.get(2);
                slope = opv.get(3);  
                
            } else if(fitMode==FitType.EC50ONLY){
                bottom = fixedBottom;
                top = fixedTop;
                slope = fixedSlope;
                ec50 = opv.get(0);
                
            } else if(fitMode==FitType.TOPBOTTOMEC50){
                bottom = opv.get(0);
                top = opv.get(1);
                ec50 = opv.get(2);
                slope = fixedSlope;
                
            } else {
                bottom = fixedBottom;
                top = fixedTop;
                ec50 = opv.get(0);
                slope = opv.get(1);
            }
            Data predicions = getPredictionResults(bottom, top, ec50, slope);
            
            double sse = 0;
            double actual;
            double predicted;
            
            for(int i=0;i<predicions.getLargestRows();i++){
                actual = ((DoubleColumn)predicions.column(1)).getDoubleValue(i);
                predicted = ((DoubleColumn)predicions.column(2)).getDoubleValue(i);
                sse = sse + (Math.pow(actual - predicted, 2));
            }
            return sse;
        } catch (Exception e){
            e.printStackTrace();
            return Double.MAX_VALUE;
        }
    }

    @Override
    public OptimisedParameterVector getInitialParameters() {
        OptimisedParameterVector params = null;
        try {
            //double bottom = ((NumericalColumn)data.column(1)).getDoubleValue(0);
            //double top = ((NumericalColumn)data.column(1)).getDoubleValue(data.column(1).getRows() - 1);
            MinValueCalculator minCalc = new MinValueCalculator((NumericalColumn)data.column(1));
            double bottom = minCalc.doubleValue();
            
            MaxValueCalculator maxCalc = new MaxValueCalculator((NumericalColumn)data.column(1));
            double top = maxCalc.doubleValue();
                        
            
            //double xMax = new MaxValueCalculator((NumericalColumn)data.column(0)).doubleValue();
            //double xMin = new MinValueCalculator((NumericalColumn)data.column(0)).doubleValue();
            int xMinIndex = minCalc.getIndexOfMinimumValue();
            double xMin = ((NumericalColumn)data.column(0)).getDoubleValue(xMinIndex);
            
            int xMaxIndex = maxCalc.getIndexOfMaximumValue();
            double xMax = ((NumericalColumn)data.column(0)).getDoubleValue(xMaxIndex);
            
            double slope;
            
            // try and get the mid point in the data
            int ec50Index;
            
            if(xMinIndex<xMaxIndex){
                ec50Index = xMinIndex + (int)((xMaxIndex - xMinIndex) / 2);
                
            } else if(xMaxIndex==xMinIndex){
                ec50Index = xMinIndex;
                
            } else {
                ec50Index = xMaxIndex = (int)((xMinIndex - xMaxIndex) / 2);
            }
            
            //double ec50 = (xMax - xMin) / 2;
            double ec50 = ((NumericalColumn)data.column(0)).getDoubleValue(ec50Index);
            
            boolean negativeSlope = false;
            if(xMin<xMax/*minCalc.getIndexOfMinimumValue()<maxCalc.getIndexOfMaximumValue()*/){
                // Slope goes up
                System.out.println("+ve slope");
                fixedBottom = bottom;
                fixedTop = top;
                //slope = (bottom - top) / (xMin - xMax);
                slope = 1;
                negativeSlope = false;
            } else {
                // Slope goes down
                System.out.println("-ve slope");
                fixedBottom = bottom;
                fixedTop = top;    
                //slope = (bottom - top) / (xMax - xMin);
                slope = -1;
                negativeSlope = true;
            }

            fixedSlope = slope;
            fixedEc50 = ec50;
            
            if(fitMode==FitType.EVERYTHING){
                params = new OptimisedParameterVector(4);
                params.set(0, fixedBottom);
                params.getParameter(0).setLabel("Bottom");
                
                params.set(1, fixedTop);
                params.getParameter(1).setLabel("Top");
                
                params.set(2, fixedEc50);
                params.getParameter(2).setLabel("EC50");
                
                params.set(3, slope);                
                params.getParameter(3).setLabel("Slope");
                
                if(negativeSlope){
                    params.getParameter(3).setEnforceRange(true);
                    params.getParameter(3).setMinimumValue(-4);
                    params.getParameter(3).setMaximumValue(0);
                } else {
                    //params.getParameter(3).setEnforceRange(true);
                    //params.getParameter(3).setMinimumValue(0);
                    //params.getParameter(3).setMaximumValue(4);                    
                }
                
            } else if(fitMode==FitType.EC50ONLY){
                params = new OptimisedParameterVector(1);
                params.set(0, ec50);
                params.getParameter(0).setLabel("EC50");
                
            } else if(fitMode==FitType.TOPBOTTOMEC50){
                params = new OptimisedParameterVector(3);
                params.set(0, fixedBottom);
                params.getParameter(0).setLabel("Bottom");
                
                params.set(1, fixedTop);
                params.getParameter(1).setLabel("Top");
                
                params.set(2, fixedEc50);
                params.getParameter(2).setLabel("EC50");
                
            } else {
                params = new OptimisedParameterVector(2);
                params.set(0, ec50);
                params.getParameter(0).setLabel("EC50");
                
                params.set(1, slope);
                params.getParameter(1).setLabel("Slope");
            }

        } catch (Exception e){
            e.printStackTrace();
        }
        return params;
    }

    @Override
    public int getParameterCount() {
        if(fitMode==FitType.EVERYTHING){
            return 4;
        } else if(fitMode==FitType.EC50ONLY){
            return 1;
        } else if(fitMode==FitType.TOPBOTTOMEC50){
            return 3;
        } else {
            return 2;
        }
    }

    @Override
    public XmlDataStore getProperties() {
        return new XmlDataStore();
    }

    public void setFitMode(FitType fitMode) {
        this.fitMode = fitMode;
    }

    public FitType getFitMode() {
        return fitMode;
    }
    
    @Override
    public void reset() {
        
    }
    
    public void setObservedData(Data observed){
        this.data = observed;
    }
 
    public Data getObservedData(){
        return data;
    }
    
    public double getPrediction(OptimisedParameterVector opv, double x) throws DataException {
        double bottom;
        double top;
        double ec50;
        double slope;
        
        if(fitMode==FitType.EVERYTHING){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = opv.get(3);
            
        } else if(fitMode==FitType.EC50ONLY){
            bottom = fixedBottom;
            top = fixedTop;
            slope = fixedSlope;
            ec50 = opv.get(0);
            
        } else if(fitMode==FitType.TOPBOTTOMEC50){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = fixedSlope;
            
        } else {
            bottom = fixedBottom;
            top = fixedTop;
            ec50 = opv.get(0);
            slope = opv.get(1);
        }
        return getPrediction(bottom, top, ec50, slope, x);
    }
    
    public Data getPredictionResults(double bottom, double top, double ec50, double slope) throws DataException{
        Data results = new Data();
        results.addColumn(data.column(0).getCopy());
        results.addColumn(data.column(1).getCopy());
        
        DoubleColumn prediction = new DoubleColumn("Predicted");
        results.addColumn(prediction);
        
        DoubleColumn x = (DoubleColumn)data.column(0);
        for(int i=0;i<x.getRows();i++){
            prediction.appendDoubleValue(getPrediction(bottom, top, ec50, slope, x.getDoubleValue(i)));
        }

        return results;
    }
    
    public Data getPredictionResults(OptimisedParameterVector opv) throws DataException {
        
        double bottom;
        double top;
        double ec50;
        double slope;
        
        if(fitMode==FitType.EVERYTHING){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = opv.get(3);
            
        } else if(fitMode==FitType.EC50ONLY){
            bottom = fixedBottom;
            top = fixedTop;
            slope = fixedSlope;
            ec50 = opv.get(0);
            
        } else if(fitMode==FitType.TOPBOTTOMEC50){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = fixedSlope;
            
        } else {
            bottom = fixedBottom;
            top = fixedTop;
            ec50 = opv.get(0);
            slope = opv.get(1);
        }
        return getPredictionResults(bottom, top, ec50, slope);
    }
    
    public XmlDataStore getParametersAsProperties(OptimisedParameterVector opv){
        double bottom;
        double top;
        double ec50;
        double slope;
        
        if(fitMode==FitType.EVERYTHING){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = opv.get(3);
            
        } else if(fitMode==FitType.EC50ONLY){
            bottom = fixedBottom;
            top = fixedTop;
            slope = fixedSlope;
            ec50 = opv.get(0);
            
        } else if(fitMode==FitType.TOPBOTTOMEC50){
            bottom = opv.get(0);
            top = opv.get(1);
            ec50 = opv.get(2);
            slope = fixedSlope;
            
        } else {
            bottom = fixedBottom;
            top = fixedTop;
            ec50 = opv.get(0);
            slope = opv.get(1);
        }
        
        XmlDataStore results = new XmlDataStore("Parameters");
        results.add("Top", top);
        results.add("Bottom", bottom);
        results.add("EC50", ec50);
        results.add("Slope", slope);
        return results;
    }
}