/**
 * e-Science Central Copyright (C) 2008-2013 School of Computing Science,
 * Newcastle University
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation at: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.tools.html2pdf;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 * This file converts an HTML file into a PDF document.
 * @author hugo
 */
public class Html2PdfConverter {
    /** Source file name */
    private File sourceFile;
    
    /** Target pdf file */
    private File targetFile;

    public Html2PdfConverter(File sourceFile, File targetFile) {
        this.sourceFile = sourceFile;
        this.targetFile = targetFile;
    }
    
    /** Do the conversion */
    public void convert() throws Exception {
        try {
            URL sourceUrl = sourceFile.toURI().toURL();
            InputStream inStream = ((URLConnection)sourceUrl.openConnection()).getInputStream();
            ByteArrayOutputStream tidiedStream = new ByteArrayOutputStream();
            
            // Tidy up the document
            Tidy tidy = new Tidy();
            tidy.setXHTML(true);
            tidy.setTrimEmptyElements(false);
            tidy.setOutputEncoding("utf-8");
            tidy.parse(inStream, tidiedStream);            
            InputStream tidyIn = new ByteArrayInputStream(tidiedStream.toByteArray());
            
            // Load this into an XML document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", Boolean.FALSE);
            dbf.setFeature("http://xml.org/sax/features/validation", Boolean.FALSE);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(tidyIn);            
            
            // Render this document
            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocument(doc, sourceUrl.toExternalForm());
            renderer.layout();            
            renderer.createPDF(new FileOutputStream(targetFile));
            
        } catch (Exception e){
            throw new Exception("Error converting file: " + e.getMessage(), e);
        }
    }
}