
package com.connexience.libraries.tsb;

import com.connexience.libraries.jdbc.JDBCConnectionInformation;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * This class contains a reference to a JDBC database containing a snapshot
 * of companies data
 * @author hugo
 */
public class CompaniesHouseJDBCReference extends JDBCConnectionInformation {
    
    private String libraryTableName = "";
    private String companiesHouseTableName = "";
    private String processedTableName = "";
    private String neCompaniesTableName = "";
    private String neccTableName = "";
    private String masterTableName = "";
    private boolean libraryBackupUsed = true;


    public String getLibraryTableName() {
        return libraryTableName;
    }

    public void setLibraryTableName(String libraryTableName) {
        this.libraryTableName = libraryTableName;
    }

    public String getCompaniesHouseTableName() {
        return companiesHouseTableName;
    }

    public void setCompaniesHouseTableName(String companiesHouseTableName) {
        this.companiesHouseTableName = companiesHouseTableName;
    }

    public String getNeccTableName() {
        return neccTableName;
    }

    public void setNeccTableName(String neccTableName) {
        this.neccTableName = neccTableName;
    }

    public String getProcessedTableName() {
        return processedTableName;
    }

    public void setProcessedTableName(String processedTableName) {
        this.processedTableName = processedTableName;
    }

    public String getNeCompaniesTableName() {
        return neCompaniesTableName;
    }

    public void setNeCompaniesTableName(String neCompaniesTableName) {
        this.neCompaniesTableName = neCompaniesTableName;
    }

    public boolean isLibraryBackupUsed() {
        return libraryBackupUsed;
    }

    public void setLibraryBackupUsed(boolean libraryBackupUsed) {
        this.libraryBackupUsed = libraryBackupUsed;
    }

    public String getMasterTableName() {
        return masterTableName;
    }

    public void setMasterTableName(String masterTableName) {
        this.masterTableName = masterTableName;
    }
}