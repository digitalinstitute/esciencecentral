package org.pipeline.core.optimisation;

/**
 * This class holds information about an optimiser including the class to 
 * create and the editor panel that is used to configure it.
 * @author hugo
 */
public class OptimiserInfo {
    /** Optimiser id */
    private String id;
    
    /** Optimiser name */
    private String name;
    
    /** Optimiser description */
    private String description;
    
    /** Optimiser class to create */
    private Class optimiserClass;
    
    /** Creates a new instance of OptimiserInfo */
    public OptimiserInfo(String id, String name, String descriptionm, Class optimiserClass) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.optimiserClass = optimiserClass;
    }

    /** Get the id string that is used to create an instance of this optimiser */
    public String getId() {
        return id;
    }

    /** Get the display name of the optimiser */
    public String getName() {
        return name;
    }

    /** Get the description of the optimiser */
    public String getDescription() {
        return description;
    }

    /** Get the class of the optimiser that will get created */
    public Class getOptimiserClass() {
        return optimiserClass;
    }
    
    /** Override toString method */
    public String toString(){
        return name;
    }

}
