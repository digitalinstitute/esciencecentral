package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.apps.neuralnetwork.ffann.FFANNModel;
import org.pipeline.apps.neuralnetwork.ffann.FFANNModelTrainingHarness;
import org.pipeline.core.data.Data;
import org.pipeline.core.drawing.BlockExecutionException;
import org.pipeline.core.drawing.BlockExecutionReport;
import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.model.DefaultBlockModel;
import org.pipeline.core.drawing.model.DefaultInputPortModel;
import org.pipeline.core.drawing.model.DefaultOutputPortModel;
import org.pipeline.core.matrix.Matrix;
import org.pipeline.core.optimisation.OptimisationController;
import org.pipeline.core.util.DataToMatrixConverter;
import org.pipeline.core.util.DataWrapper;
import org.pipeline.core.util.DrawingDataUtilities;
import org.pipeline.core.util.MatrixToDataConverter;
import org.pipeline.core.xmlstorage.XmlDataStore;
import org.pipeline.core.xmlstorage.XmlStorageException;

/**
 * This class provides a feed-forward neural network.
 * @author hugo
 */
public class FFANNTrainingBlock extends DefaultBlockModel {   
    /** Hidden layer neuron count */
    private int hiddenNeurons = 5;
    
    /** Neural network object */
    private FFANNModel model = null;
    
    /** Model optimiser */
    private OptimisationController controller = new OptimisationController();
    
    /** Model training harness */
    private FFANNModelTrainingHarness harness = null;
    
    /** Creates a new instance of FFANNTrainingBlock */
    public FFANNTrainingBlock() throws DrawingException {
        super();
        setLabel("NN Train");
        
        // Create the x-data input
        DefaultInputPortModel xPort = new DefaultInputPortModel("x-data", PortModel.LEFT_OF_BLOCK, 30, this);
        xPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(xPort);
        
        // Create the y-data input 
        DefaultInputPortModel yPort = new DefaultInputPortModel("y-data", PortModel.LEFT_OF_BLOCK, 70, this);
        yPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(yPort);
        
        // Add a model output port
        DefaultOutputPortModel modelPort = new DefaultOutputPortModel("neural-network-model", PortModel.BOTTOM_OF_BLOCK, 50, this);
        modelPort.addDataType(FFANNModelDataTypeWrapper.FFANN_MODEL_DATA_TYPE);
        addOutputPort(modelPort);
        
        // Create the estimated data output
        DefaultOutputPortModel predictionPort = new DefaultOutputPortModel("predicted-y-data", PortModel.RIGHT_OF_BLOCK,  50, this);
        predictionPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addOutputPort(predictionPort);
    }
    
    /** Get the optimisation controller */
    public OptimisationController getController(){
        return controller;
    }
    
    /** Get the training harness */
    public FFANNModelTrainingHarness getHarness(){
        return harness;
    }
    
    /** Get the network model */
    public FFANNModel getModel(){
        return model;
    }
    
    /** Get the number of hidden layer neurons */
    public int getHiddenNeurons(){
        return hiddenNeurons;
    }
    
    /** Set the number of hidden layer neurons */
    public void setHiddenNeurons(int hiddenNeurons){
        if(hiddenNeurons>0){
            this.hiddenNeurons = hiddenNeurons;
            if(model!=null){
                if(this.hiddenNeurons!=model.getHiddenNeurons()){
                    // Reset model 
                    model.setHiddenNeurons(hiddenNeurons);
                    model.initialiseWeights();
                }
            }
        }
    }
        
    /** Execute this block */
    public BlockExecutionReport execute() throws BlockExecutionException {
        try {           
            Data xData = DrawingDataUtilities.getInputData(getInput("x-data"));
            Data yData = DrawingDataUtilities.getInputData(getInput("y-data"));
            
            Matrix x = new DataToMatrixConverter(xData).toMatrix();
            Matrix y = new DataToMatrixConverter(yData).toMatrix();
            
            if(x.getRowDimension()==y.getRowDimension()){
                if(model==null){
                    // Create new model
                    model = new FFANNModel();
                    model.setInputs(x.getColumnDimension());
                    model.setOutputs(y.getColumnDimension());
                    model.setHiddenNeurons(hiddenNeurons);
                    model.initialiseWeights();
                    harness = new FFANNModelTrainingHarness(model);
                    controller.setTarget(harness);
                    
                } else {
                    // Check sizes
                    if(x.getColumnDimension()!=model.getInputs() || y.getColumnDimension()!=model.getOutputs() || hiddenNeurons!=model.getHiddenNeurons()){
                        // Reset model
                        model.setInputs(x.getColumnDimension());
                        model.setOutputs(y.getColumnDimension());
                        model.setHiddenNeurons(hiddenNeurons);
                        model.initialiseWeights();                    
                    }                
                }
                
                Matrix yest = model.getPredictions(x);                
                Data outData = new MatrixToDataConverter(yest).toData(); 
 
                // Copy column names to output data
                if(yData.getColumns()==outData.getColumns()){
                    for(int i=0;i<yData.getColumns();i++){
                        outData.column(i).setName(yData.column(i).getName() + "_est");
                    }
                }
                
                DrawingDataUtilities.setOutputData(getOutput("predicted-y-data"), outData);
                getOutput("neural-network-model").setData(new FFANNModelDataTypeWrapper(model));
                return new BlockExecutionReport(this);
                
            } else {
                return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, "X and Y data sets do not have the same number of rows");
            }
        } catch (Exception e){
            return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, e.getMessage());
        }
        
    }    
    
    /** Recreate from storage */
    public void recreateObject(XmlDataStore xmlDataStore) throws XmlStorageException {
        super.recreateObject(xmlDataStore);
        hiddenNeurons = xmlDataStore.intValue("HiddenNeurons", 5);
        if(xmlDataStore.containsName("Network")){
            model = (FFANNModel)xmlDataStore.xmlStorableValue("Network");
        } else {
            model = null;
        }
        
        if(xmlDataStore.containsName("OptimisationController")){
            controller = (OptimisationController)xmlDataStore.xmlStorableValue("OptimisationController");
        }
        
        if(model!=null && controller!=null){
            harness = new FFANNModelTrainingHarness(model);
            controller.setTarget(harness);
        }
    }

    /** Save to storage */
    public XmlDataStore storeObject() throws XmlStorageException {
        XmlDataStore store = super.storeObject();
        store.add("HiddenNeurons", hiddenNeurons);
        if(model!=null){
            store.add("Network", model);
        }
        store.add("OptimisationController", controller);
        return store;
    }    
}
