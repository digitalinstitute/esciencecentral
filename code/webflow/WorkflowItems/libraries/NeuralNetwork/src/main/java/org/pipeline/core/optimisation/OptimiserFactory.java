package org.pipeline.core.optimisation;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * This class manages the various optimisers that are present in the system. It
 * allows new optimisers to be registered and provides a uniform way of accessing
 * different optimisation engines.
 * @author hugo
 */
public abstract class OptimiserFactory {
    /** Hashtable of optimiser info objects */
    private static Hashtable optimisers = new Hashtable();
    
    /** Default optimiser ID */
    private static String defaultOptimiser = "simplex";
    
    /** Register an optimiser */
    public static void registerOptimiser(OptimiserInfo info){
        if(!optimisers.containsKey(info.getId())){
            optimisers.put(info.getId(), info);
        }
    }
    
    /** Un-register an optimiser */
    public static void unregisterOptimiser(String id){
        if(optimisers.containsKey(id)){
            optimisers.remove(id);
        }
    }
    
    /** Un-register an optimiser */
    public static void unregisterOptimiser(OptimiserInfo info){
        unregisterOptimiser(info.getId());
    }
    
    /** Get the information for a specific optimiser */
    public static OptimiserInfo getInfo(String id){
        if(optimisers.containsKey(id)){
            return (OptimiserInfo)optimisers.get(id);
        } else {
            return null;
        }
    }
    
    /** Get the optimiser info object for a specific optimiser */
    public static OptimiserInfo getInfoForOptimiser(Optimiser optimiser){
        Enumeration e = optimisers.elements();
        OptimiserInfo info;
        while(e.hasMoreElements()){
            info = (OptimiserInfo)e.nextElement();
            if(info.getOptimiserClass().equals(optimiser.getClass())){
                return info;
            }
        }
        return null;
    }
    
    /** Get an Enumeration of all the optimiser types */
    public static Enumeration getOptimisers(){
        return optimisers.elements();
    }
    
    /** Create an optimiser */
    public static Optimiser createOptimiser(String id) throws OptimisationException {
        try {
            if(optimisers.containsKey(id)){
                return (Optimiser)((OptimiserInfo)optimisers.get(id)).getOptimiserClass().newInstance();
                
            } else {
                throw new Exception("Optimiser type does not exist");
            }   
            
        } catch (Exception e){
            throw new OptimisationException("Cannot create optimiser: " + e.getLocalizedMessage());
        }
    }
    
    /** Create an optimiser */
    public static Optimiser createOptimiser(OptimiserInfo info) throws OptimisationException {
        return createOptimiser(info.getId());
    }
    
    /** Create the default optimiser */
    public static Optimiser createDefaultOptimiser() throws OptimisationException {
        return createOptimiser(defaultOptimiser);
    }
}