package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.apps.neuralnetwork.ffann.FFANNModel;
import org.pipeline.core.drawing.DataType;
/**
 * This class defines a data type for a FFANNModel object that can
 * be passed between blocks.
 * @author hugo
 */
public class FFANNModelDataType extends DataType {
   
    /** Creates a new instance of FFANNModelDataType */
    public FFANNModelDataType() {
        super("FFANN Model", "Feed-forward Neural Network object", FFANNModel.class);
    }
}
