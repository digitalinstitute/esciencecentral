package org.pipeline.core.optimisation;

/**
 * This is the Exception class that gets thrown by classes in the optimisation package.
 * @author hugo
 */
public class OptimisationException extends java.lang.Exception {
    
    /**
     * Creates a new instance of <code>OptimisationException</code> without detail message.
     */
    public OptimisationException() {
    }
    
    
    /**
     * Constructs an instance of <code>OptimisationException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public OptimisationException(String msg) {
        super(msg);
    }
}
