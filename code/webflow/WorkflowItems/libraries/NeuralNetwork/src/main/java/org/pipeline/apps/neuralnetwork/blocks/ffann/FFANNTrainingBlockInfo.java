package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.core.drawing.BlockModelInfo;

/**
 * Provides registration information for the neural network block.
 * @author hugo
 */
public class FFANNTrainingBlockInfo extends BlockModelInfo {
    
    /** Creates a new instance of FFANNTrainingBbock */
    public FFANNTrainingBlockInfo() {
        super("ffann-training-block", "FFANN Training", FFANNTrainingBlock.class);
        setCategory("Modelling");
    }
    
}
