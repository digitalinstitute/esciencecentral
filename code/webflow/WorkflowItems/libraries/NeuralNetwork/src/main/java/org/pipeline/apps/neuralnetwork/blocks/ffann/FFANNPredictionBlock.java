package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.apps.neuralnetwork.ffann.FFANNModel;
import org.pipeline.core.data.Data;
import org.pipeline.core.drawing.BlockExecutionException;
import org.pipeline.core.drawing.BlockExecutionReport;
import org.pipeline.core.drawing.DrawingException;
import org.pipeline.core.drawing.PortModel;
import org.pipeline.core.drawing.model.DefaultBlockModel;
import org.pipeline.core.drawing.model.DefaultInputPortModel;
import org.pipeline.core.drawing.model.DefaultOutputPortModel;
import org.pipeline.core.matrix.Matrix;
import org.pipeline.core.util.DataToMatrixConverter;
import org.pipeline.core.util.DataWrapper;
import org.pipeline.core.util.DrawingDataUtilities;
import org.pipeline.core.util.MatrixToDataConverter;

/**
 * This class provides a block that propagates a new set of data
 * through a FFANN model object.
 * @author hugo
 */
public class FFANNPredictionBlock extends DefaultBlockModel {
    
    /** Creates a new instance of FFANNPredictionBlock */
    public FFANNPredictionBlock() throws DrawingException {
        super();
        setLabel("NN Pred");
        
        // X-Data input
        DefaultInputPortModel xPort = new DefaultInputPortModel("x-data", PortModel.LEFT_OF_BLOCK, 50, this);
        xPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addInputPort(xPort);
        
        // Model input
        DefaultInputPortModel modelPort = new DefaultInputPortModel("neural-network-model", PortModel.TOP_OF_BLOCK, 50, this);
        modelPort.addDataType(FFANNModelDataTypeWrapper.FFANN_MODEL_DATA_TYPE);
        addInputPort(modelPort);
        
        // Y-Est output
        DefaultOutputPortModel predictionPort = new DefaultOutputPortModel("predicted-y-data", PortModel.RIGHT_OF_BLOCK,  50, this);
        predictionPort.addDataType(DataWrapper.DATA_WRAPPER_TYPE);
        addOutputPort(predictionPort);            
    }
    
    /** Execute this block */
    public BlockExecutionReport execute() throws BlockExecutionException {
        try {
            Data xData = DrawingDataUtilities.getInputData(getInput("x-data"));
            FFANNModel model = (FFANNModel)((FFANNModelDataTypeWrapper)getInput("neural-network-model").getData()).getPayload();
            
            Matrix x = new DataToMatrixConverter(xData).toMatrix();
            
            if(x.getColumnDimension()==model.getInputs()){
                
                Matrix yest = model.getPredictions(x);                
                Data outData = new MatrixToDataConverter(yest).toData();  
                DrawingDataUtilities.setOutputData(getOutput("predicted-y-data"), outData);
                return new BlockExecutionReport(this);
                
            } else {
                return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, "X and Y data sets do not have the same number of rows");
            }
        } catch (Exception e){
            return new BlockExecutionReport(this, BlockExecutionReport.INTERNAL_ERROR, e.getMessage());
        }
        
    }        
}
