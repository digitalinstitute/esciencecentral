package org.pipeline.core.optimisation;

import org.pipeline.core.xmlstorage.XmlDataStore;

/**
 * This interface defines a class that can interact with an optimiser
 * by providing initial parameters, optimiser cost, settings etc.
 * @author nhgh
 */
public interface OptimisableObject {
    /** Reset to initial conditions, whatever they may be */
    public void reset();
    
    /** Return the number of parameters */
    public int getParameterCount();
    
    /** Get an initial parameter estimate */
    public OptimisedParameterVector getInitialParameters();
    
    /** Can this object provide an initial parameter set estimate */
    public boolean canSupplyInitialParameters();
    
    /** Evaluate this object using a set of parameters and return the error */
    public double getCost(OptimisedParameterVector parameters);
    
    /** Get the settings for this object */
    public XmlDataStore getProperties();
}