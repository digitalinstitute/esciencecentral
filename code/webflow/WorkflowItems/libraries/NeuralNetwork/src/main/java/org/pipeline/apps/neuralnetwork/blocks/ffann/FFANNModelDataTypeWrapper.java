package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.apps.neuralnetwork.ffann.FFANNModel;
import org.pipeline.core.drawing.TransferData;

/**
 * This class provides a transferrable wrapper for a FFANN model
 * @author hugo
 */
public class FFANNModelDataTypeWrapper implements TransferData {
    /** Static reference so only one copy exists */
    public static final FFANNModelDataType FFANN_MODEL_DATA_TYPE = new FFANNModelDataType();
    
    /** Object payload */
    private FFANNModel model;
    
    /**
     * Creates a new instance of FFANNModelDataTypeWrapper 
     */
    public FFANNModelDataTypeWrapper(FFANNModel model) {
        this.model = model;
    }
    
    /** Get the payload from this object */
    public Object getPayload(){
        return model.getCopy();
    }
    
    /** Get a copy of this object */
    public TransferData getCopy(){
        return new FFANNModelDataTypeWrapper(model.getCopy());
    }
}
