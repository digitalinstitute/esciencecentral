package org.pipeline.apps.neuralnetwork.ffann;

/**
 * This class extends the basic neural network model to include first
 * order filters on the hidden layer neurons.
 * @author nhgh
 */
public class FilterFFANNModel {
    
    /** Creates a new instance of FilterFFANNModel */
    public FilterFFANNModel() {
    }
    
}
