package org.pipeline.apps.neuralnetwork.blocks.ffann;

import org.pipeline.core.drawing.BlockModelInfo;

/**
 * This class provides registration info for the FFANNPredictionBlock
 * @author hugo
 */
public class FFANNPredictionBlockInfo extends BlockModelInfo {
    
    /** Creates a new instance of FFANNPredictionBlockInfo */
    public FFANNPredictionBlockInfo() {
        super("ffann-prediction-block", "FFANN Prediction", FFANNPredictionBlock.class);
        setCategory("Modelling");
    }
    
}
