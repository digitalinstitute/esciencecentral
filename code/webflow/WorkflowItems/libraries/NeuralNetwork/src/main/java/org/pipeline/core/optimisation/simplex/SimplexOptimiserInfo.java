package org.pipeline.core.optimisation.simplex;

import org.pipeline.core.optimisation.OptimiserInfo;
/**
 * This class provides registration information for the SimplexOptimiser class.
 * @author hugo
 */
public class SimplexOptimiserInfo extends OptimiserInfo {
    
    /**
     * Creates a new instance of SimplexOptimiserInfo 
     */
    public SimplexOptimiserInfo() {
        super("simplex", "Simplex", "A very basic optimiser that used the Nelder and Mead algorithm", SimplexOptimiser.class);
    }
}
