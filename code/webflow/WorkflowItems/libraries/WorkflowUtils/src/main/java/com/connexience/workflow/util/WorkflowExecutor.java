/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.workflow.util;

import com.connexience.server.api.*;
import com.connexience.server.workflow.xmlstorage.*;
import com.connexience.server.workflow.engine.datatypes.*;
import org.pipeline.core.data.*;
import org.pipeline.core.data.columns.*;

import java.util.*;


public class WorkflowExecutor
{
	API api;
	private boolean debug = false;
	private IWorkflow wf;
	private StringPairListWrapper propertyMapping = null;
	private String[] fileIds = null;
	private IWorkflowParameterList extraParams = null;

	public WorkflowExecutor(API api) throws Exception {
		this.api = api;
		extraParams = (IWorkflowParameterList)api.createObject(IWorkflowParameterList.XML_NAME);
	}

	public void setDebug(boolean debug){
		this.debug = debug;
	}

	public void setWorkflow(IWorkflow wf){
		this.wf = wf;
	}

	public void setExtraParameter(String blockName, String parameterName, String value) throws Exception {
		extraParams.add(blockName, parameterName, value);
	}

	public void setExtraParameters(IWorkflowParameterList extraParams){
		this.extraParams = extraParams;
	}

	public void setFileMappingDetails(StringPairListWrapper propertyMapping, String[] fileIds){
		this.propertyMapping = propertyMapping;
		this.fileIds = fileIds;
	}

	public void setFileMappingDetails(StringPairListWrapper propertyMapping, StringColumn idColumn){
		this.propertyMapping = propertyMapping;
		this.fileIds = new String[idColumn.getRows()];
		for(int i=0;i<idColumn.getRows();i++){
			this.fileIds[i] = idColumn.getStringValue(i);
		}
	}

	public IWorkflowParameterList createParams() throws Exception {
		IWorkflowParameterList params = (IWorkflowParameterList)api.createObject(IWorkflowParameterList.XML_NAME);

		// Copy in file mappings
		if (propertyMapping == null && fileIds == null) {
			// A special case -- file mappings have not been provided.
		} else if (propertyMapping != null && fileIds != null && propertyMapping.getSize() == fileIds.length) {
			String blockName;
			String propertyName;

			// Create mappings
			for(int i=0;i<fileIds.length;i++) {
				blockName = propertyMapping.getValue(i, 0);
				propertyName = propertyMapping.getValue(i, 1);
				params.add(blockName, propertyName, fileIds[i]);
			}

			if (debug) {
				System.out.println("Workflow parameter mapping:");
				IWorkflowParameter param;
				for(int i=0;i<params.size();i++) {
					param = params.getParameter(i);
					System.out.println(param.getBlockName() + "." + param.getName() + "=" + param.getValue());
				}
			}
		} else {
			throw new Exception(
				"Wrong number of files. Expecting: " + (propertyMapping != null ? propertyMapping.getSize() : "null") + 
				", got: " + (fileIds != null ? fileIds.length : "null"));
		}

		// Add any extra properties
		if (extraParams!=null) {
			IWorkflowParameter original;
			for(int i=0;i<extraParams.size();i++) {
				original = extraParams.getParameter(i);
				params.add(original.getBlockName(), original.getName(), original.getValue());
			}
		}

		return params;
	}

	/** Execute a workflow with a set of files */
	public IWorkflowInvocation execute() throws Exception {
		IWorkflowParameterList params = createParams();
		return api.executeWorkflow(wf, params);
	}
}
