
package com.connexience.server.weka;

import java.util.HashMap;
import java.util.Map;
import weka.core.Attribute;

/** Class to contain Weka 'Classifier' models that perform classification 
 * tasks (i.e. predicting class labels).
 *
 * @author Dominic Searson 14/8/2014
 */
public class WekaClassificationModel extends WekaModel {
    
    /* Maps the numeric prediction of the classifier (0,1,2 etc)
     * to the string class label ("apple,"banana", "peach" etc).
     */
    private Map<Double, String> classlabels;
    
    
    /* the Weka Attribute object attached to the predicted class values 
     * 
     */
    private Attribute classAttribute;
    
    
 public WekaClassificationModel() {
     this.clearClassLabels();
 }   
 
 public final void clearClassLabels() {
     classlabels = new HashMap<>();
 }

    public Map<Double, String> getClasslabels() {
        return classlabels;
    }

    public void setClasslabels(Map<Double, String> classlabels) {
        this.classlabels = classlabels;
    }


    public Attribute getClassAttribute() {
        return classAttribute;
    }

    public void setClassAttribute(Attribute classAttribute) {
        this.classAttribute = classAttribute;
    }
    

}
