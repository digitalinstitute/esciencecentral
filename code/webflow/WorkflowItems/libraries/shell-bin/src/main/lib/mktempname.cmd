::
:: A helper function for the Windows Batch shell wrapper.
:: This function may be used to create a unique temporary file name.
::

:MkTempName
SETLOCAL
SET /a OUTFILE=%RANDOM%+100000
SET OUTFILE=output-%OUTFILE:~3%
IF EXIST %OUTFILE% GOTO :MkTempName
ENDLOCAL & SET _MkTempName=%OUTFILE%
:: GOTO :eof
