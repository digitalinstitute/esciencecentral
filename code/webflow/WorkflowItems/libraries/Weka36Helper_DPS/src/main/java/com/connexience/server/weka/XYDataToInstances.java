/**
 * e-Science Central
 * Copyright (C) 2008-2013 School of Computing Science, Newcastle University
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation at:
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.
 */
package com.connexience.server.weka;

import org.pipeline.core.data.Column;
import org.pipeline.core.data.Data;
import weka.core.Instances;

/**
 * This class converts a typical modelling data set with a block of x variables
 * and a y column into a Weka Instances object.
 * @author hugo
 */
public class XYDataToInstances {
    /** X Data set */
    Data xData;
    
    /** Y data column */
    Column yColumn;

    public XYDataToInstances(Data xData, Column yColumn) {
        this.xData = xData;
        this.yColumn = yColumn;
    }
    
    /** Create a labelled set of instances */
    public Instances toWekaInstances() throws Exception {
        Data workingSet = xData.getCopy();
        workingSet.addColumn(yColumn.getCopy());
        Instances wekaInstances = new DataToInstances(workingSet).toWekaInstances();
        wekaInstances.setClassIndex(xData.getColumns());
        return wekaInstances;
    }
}
