/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connexience.server.weka;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.pipeline.core.data.Data;

import org.pipeline.core.data.io.DelimitedTextDataImporter;
import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;

import weka.core.Instance;
import weka.core.Instances;

/**
 *
 * @author hugo
 */
public class Test {
    public static void main(String[] args) throws Exception {
        // Load the standard data set
        DelimitedTextDataImporter importer = new DelimitedTextDataImporter();
        Data d = importer.importFile(new File("/Users/hugo/data.csv"));
        
        // Convert to WEKA instances
        DataToInstances c1 = new DataToInstances(d);
        Instances instances = c1.toWekaInstances();
        
        // Set the modelled variable to be the last one (Q4)
        instances.setClassIndex(instances.numAttributes() - 1);
        
        // Do the regression
        LinearRegression regression = new LinearRegression();
        regression.buildClassifier(instances);
        
        // Display the results
        System.out.println(regression.toString());
        
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ObjectOutputStream objStream = new ObjectOutputStream(stream);
        objStream.writeObject(regression);
        objStream.flush();
        objStream.close();
        
        ByteArrayInputStream inStream = new ByteArrayInputStream(stream.toByteArray());
        ObjectInputStream objReader = new ObjectInputStream(inStream);
        
        Classifier c = (Classifier)objReader.readObject();
        System.out.println(c.toString());
        
        Instance testInstance = instances.firstInstance();
        System.out.println("Prediction: " + c.classifyInstance(testInstance));
    }
}