
package com.connexience.libraries.jdbc;

/**
 * This class loads the JDBC drivers contained in this library
 * @author hugo
 */
public class DriverLoader {
    public static final void loadDrivers() throws Exception {
        Class.forName("org.postgresql.Driver");
    }
}
