---
# e-Science Central
# Copyright (C) 2008-2015 School of Computing Science, Newcastle University
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation at:
# http://www.gnu.org/licenses/gpl-2.0.html
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.

#
# This playbook makes a dump of database and local fs store, which can then be used
# to clone the server with site-standalone-restore.yml
#
# To use the playbook just run:
#
# > ansible-playbook -i YOUR_INVENTORY.ini site-standalone-dump.yml
#
#
# To run the playbook only to dump the fs store issue:
#
# > ansible-playbook -i ~/ansible-inv/upv-inv.ini site-standalone-dump.yml --tags "store-dump"
#
#
# To define the dump directory (otherwise it will be generated automatically) run:
#
# > ansible-playbook -i ~/ansible-inv/upv-inv.ini site-standalone-dump.yml --extra-vars "dump_directory=dump-20150430-055608"
#
#
# To dump only neo4j database run:
#
# > ansible-playbook -i ~/ansible-inv/recomp.ini site-standalone-dump.yml --tags "neoonly-dump"
#

# Generate a dump directory in case it wasn't provided by the user
#
- hosts: all
  connection: local
  tasks:
    - name: Setting the dump directory name if not defined
      local_action: shell date +%Y%m%d-%H%M%S
      register: datetime
      when: dump_directory is not defined

    - set_fact:
        dump_directory: "dump-{{ datetime.stdout }}"
      when: dump_directory is not defined
  tags:
    - store-dump
    - no-store-dump
    - neoonly-dump

# Prepare to do the dump; check variables, stop jboss on the server
#
- hosts: esc_server
  sudo: yes
  tasks:
    - fail: msg="The dump directory has not been defined"
      when: dump_directory is not defined

    - name: The dump will be stored in
      debug: var=dump_directory
 
    - name: Stop the jboss service
      service: name=jboss-as state=stopped
      register: jboss_state
  tags:
    - store-dump
    - no-store-dump
    - neoonly-dump

# Do the db dump
#
- hosts: db_server
  sudo: yes
  roles:
    - role: esc-db-dump
  tags:
    - no-store-dump

# Do the server dump: file store and neo4j
#
- hosts: esc_server
  sudo: yes
  roles:
    - esc-store-dump
  tags:
    - store-dump

- hosts: esc_server
  sudo: yes
  roles:
    - esc-neo4j-dump
  tags:
    - no-store-dump
    - neoonly-dump

# Start jboss if it was running before the dump
#
- hosts: esc_server
  sudo: yes
  tasks:
    - name: Start the jboss service if needed
      service: name=jboss-as state=started
      when: jboss_state.changed
  tags:
    - store-dump
    - no-store-dump
    - neoonly-dump


