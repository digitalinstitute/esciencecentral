---
# e-Science Central
# Copyright (C) 2008-2016 School of Computing Science, Newcastle University
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation at:
# http://www.gnu.org/licenses/gpl-2.0.html
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, 5th Floor, Boston, MA 02110-1301, USA.

# Basic check to see whether admin username and password has been set
- name: Check admin username and password
  fail: msg="Please set the admin credentials in your inventory or playbook; see roles/esc-server/defaults/main.yml for details"
  when: RegInfo_FirstName is not defined or RegInfo_LastName is not defined or RegInfo_UserName is not defined or RegInfo_Password is not defined
  tags:
    - register_org

# Register OS-specific variables
#
# Note that there's no 'with_all_found' loop yet, so simulating that with multiple 'with_first_found' invocations.
- name: Register OS-specific variables
  include_vars: "{{ item }}"
  with_first_found:
    - files: "{{ ansible_distribution|lower }}-{{ ansible_distribution_version }}.yml"
      skip: true
- include_vars: "{{ item }}"
  with_first_found:
    - files: "{{ ansible_distribution|lower }}-{{ ansible_distribution_major_version }}.yml"
      skip: true
- include_vars: "{{ item }}"
  with_first_found:
    - files: "{{ ansible_distribution|lower }}.yml"
      skip: true
- include_vars: "{{ item }}"
  with_first_found:
    - files: "{{ ansible_os_family|lower }}.yml"
      skip: true

# Tasks with some basic preparations to install the server
#
- name: Install common dependencies 
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - unzip
    - zip
    - groff
    - graphviz
    - pdfjam
    - curl  # curl is needed by the registrer organisation script (not e-SC itself)

- name: Install OS-specific dependencies
  package:
    name: "{{ item }}"
    state: present
  with_items: "{{ package_list }}"

- name: Add group "esc"
  group: name=esc

- name: Add the user representing e-SC server
  user: name={{ jboss_user }} group=esc

- name: Create working directories
  file: path=/mnt/server/tmp state=directory owner={{ jboss_user }} group=esc
- file: path={{ RegInfo_StorageFolder }} state=directory owner={{ jboss_user }} group=esc


# Tasks to prepare jBoss AS for the deployment of e-Science Central server
#
- name: Copy jBoss wildfly AS to the installation directory
  copy: src={{ jboss_dist_url }} dest={{ ansible_env.HOME }}
  when: "not 'http://' in jboss_dist_url"

- name: Fetch jBoss wildfly AS from the remote location
  get_url: url={{ jboss_dist_url }} dest={{ ansible_env.HOME }} force=no owner={{ jboss_user }} group=esc
  when: "'http://' in jboss_dist_url"

- name: Extract jBoss wildfly AS to the installation directory
# The source directory should be changed to local ansible 'files' once this approach is widely used by everybody
#  unarchive: src={{ jboss_dist_dir }}/jboss-as-7.1.3.final.zip  dest={{ jboss_home_dir }} creates={{ jboss_home_dir }}/jboss-modules.jar
# Since the jboss_dist_url package unpacks directory wildfly-9.0.2.Final we create a link named as {{ jboss_home_dir }}
  unarchive: src={{ ansible_env.HOME }}/{{ jboss_dist_url | basename }} dest=/usr/local/ remote_src=True keep_newer=True owner={{ jboss_user }} group=esc

- file: dest={{ jboss_home_dir }} src=/usr/local/wildfly-9.0.2.Final state=link

- name: Copy modules and basic configuration for e-SC server
  copy: src=wildfly9.0.2.Final/modules dest={{ jboss_home_dir }}/
- copy: src=wildfly9.0.2.Final/standalone dest={{ jboss_home_dir }}/

- debug: var=server_address

- name: Overwrite standalone config with {{ server_address }}
  template: src=wildfly9.0.2.Final/standalone.conf dest={{ jboss_home_dir }}/bin/

# Set the location of the database server
- set_fact: db_server_address='localhost'
  when: "'db_server' in group_names"
- set_fact: db_server_address={{ item }}
  with_items: groups.db_server
  when: "'db_server' not in group_names"

- name: Overwrite standalone XML descriptor
  # This template requires that variable db_server_address is set correctly
  template: src=wildfly9.0.2.Final/standalone.xml dest={{ jboss_home_dir }}/standalone/configuration/
  notify: restart jboss-as

- name: Change ownership of the jBoss AS installation directory
  file: path={{ jboss_home_dir }}/ owner={{ jboss_user }} group=esc state=directory recurse=yes

- name: Change mode of startup scripts
  shell: chmod a+x {{ jboss_home_dir }}/bin/*.sh

- name: Copy esc.ear to the jBoss AS standalone deployment directory
  copy: src={{ esc_dist_dir }}/esc.ear dest={{ jboss_home_dir }}/standalone/deployments/
  # No need to notify jboss, it monitors the deployments directory itself

# Hack/Bug: path in the stat module is different than in the copy module below
#- name: Check whether monitorserver exists
#  stat: path={{ performance_monitor_dist_dir }}/monitor.ear get_checksum=no
#  register: perfprov
#  become: false
#  delegate_to: localhost
#  tags:
#    - performance

#- name: Copy monitorserver.ear to jBoss standalone deployment directory
#  copy: src={{ performance_monitor_dist_dir }}/monitor.ear dest={{ jboss_home_dir }}/standalone/deployments
#  when: perfprov.stat.exists
#  tags:
#    - performance

# Tasks to run jboss-as as a system service
#
- name: Copy the jBoss service config
  file: path=/etc/jboss-as state=directory
- template: src=wildfly9.0.2.Final/init.d/wildfly.conf dest=/etc/default/wildfly
  notify: restart jboss-as

- name: Copy the jboss-as service init script
  copy: src={{ wildfly_init_script }} dest=/etc/init.d/wildfly mode=0755

# This workaround is needed on 16.04 to allow init.d wildfly script to be enabled by Ansible
- name: A workaround to the issue with init.d scripts and systemclt
  shell: systemctl enable wildfly
  when: ansible_distribution == "Ubuntu" and ansible_distribution_version == "16.04"

- name: Enable the jboss-as service to be started at boot
  service: name=wildfly enabled=yes


# Tasks to register an e-SC organisation
#
- include: register-org.yml
  when: isJbossStartAllowed|default(True)
  tags:
    - register_org

# Tasks to prepare the maven property file
#
- name: Create the inkspot directory locally
  file: state=directory path=~/.inkspot
  become: false
  delegate_to: localhost

#- name: Create the maven properties file locally
#  template: src=maven.props dest=~/.inkspot/maven.props
#  sudo: no
#  delegate_to: localhost

- name: Append the serverURL property to users maven.props file
  tags:
    - debug_props
    - register_org
  lineinfile: >
    dest=~/.inkspot/maven.props
    create=yes
    state=present
    backup=no
    insertafter="### DO NOT EDIT THE FOLLOWING PART -- MANAGED BY ANSIBLE"
    line="{{ item }}"
  become: false
  delegate_to: localhost
  with_items:
    - '### DO NOT EDIT THE FOLLOWING PART -- MANAGED BY ANSIBLE'
    - 'password={{ RegInfo_Password }}'
    - 'username={{ RegInfo_UserName }}'
    - 'serverURL=http://{{ groups.esc_server[0] }}:{{ server_port }}'
