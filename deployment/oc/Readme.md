To install Openshift Origin on OS X

https://github.com/openshift/origin/blob/master/docs/cluster_up_down.md

To run Openshift locally with a persistant directory, logging and metrics

oc cluster up --host-data-dir=$HOME/openshift-data/ --metrics --logging
