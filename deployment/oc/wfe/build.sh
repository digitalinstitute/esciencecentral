#!/bin/bash

#Need to login first
# OPENSHIFT_TOKEN=$(oc whoami -t)
# docker login -u developer -p ${OPENSHIFT_TOKEN} 172.30.1.1:5000

echo 'Building WFE'
docker build -t oc-escwfe .
docker tag oc-escwfe:latest 172.30.1.1:5000/esciencecentral/oc-escwfe:latest
docker push 172.30.1.1:5000/esciencecentral/oc-escwfe:latest
