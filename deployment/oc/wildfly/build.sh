#!/bin/bash

#Need to login first
# OPENSHIFT_TOKEN=$(oc whoami -t)
# docker login -u developer -p ${OPENSHIFT_TOKEN} 172.30.1.1:5000

echo 'Building e-Science Central'
docker build -t oc-escas .
docker tag oc-escas:latest 172.30.1.1:5000/esciencecentral/oc-escas:latest
docker push 172.30.1.1:5000/esciencecentral/oc-escas:latest
