#!/bin/bash
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

CODE="$(dirname $(dirname "$PWD"))/code"
echo $CODE

docker run -it --rm --name escBuild -v $CODE:/usr/src/esc -v ~/.m2:/root/.m2 -w /usr/src/esc maven:3.2-jdk-8 mvn clean install
