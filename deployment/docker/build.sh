#!/bin/bash

echo 'Building DB';
docker build -t sjwoodman/escdb db/;

echo 'Building WFE'
cp $ESC_HOME/code/webflow/WorkflowEngine/target/engine-package.zip wfe/;
docker build -t sjwoodman/escwfe wfe/;
rm wfe/engine-package.zip;

echo 'Building e-SC'
cp $ESC_HOME/code/server/Ear/target/esc.ear wildfly/wildfly/standalone/deployments;
docker build -t sjwoodman/escas --no-cache wildfly/;
rm wildfly/wildfly/standalone/deployments/esc.ear;
