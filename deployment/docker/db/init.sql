CREATE USER connexience WITH PASSWORD 'connexience';

CREATE DATABASE connexience;
CREATE DATABASE logeventsdb;

GRANT ALL PRIVILEGES ON DATABASE connexience TO connexience;
GRANT ALL PRIVILEGES ON DATABASE logeventsdb TO connexience;
