/*
Add the XML Metadata table.  This isn't added automatically as Hibernate doesn't support the XML data type natively.
Author: Simon
Date: May 2009
*/

CREATE TABLE xmlmetadata
(
  id character varying(255) NOT NULL,
  "xml" xml NOT NULL,
  creatorid character varying(255),
  CONSTRAINT xmlmetadatas_pkey PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE xmlmetadata OWNER TO connexiencedb;