﻿-- View: search_union

-- DROP VIEW search_union;

CREATE OR REPLACE VIEW search_union AS 
              (        (        (        (        (        (        (        (        (        (         SELECT o.id, o.name, o.creatorid, o.containerid, o.description, o.title, NULL::text AS body, 'Note.class'::text AS objecttype
                                                                                                   FROM objectsflat o
                                                                                                  WHERE o.objecttype::text = 'NOTE'::text
                                                                                        UNION 
                                                                                                 SELECT o.id, o.name, o.creatorid, o.containerid, o.profileid AS description, o.firstname AS title, o.surname AS body, 'User.class'::text AS objecttype
                                                                                                   FROM objectsflat o
                                                                                                  WHERE o.objecttype::text = 'USER'::text)
                                                                        UNION 
                                                                                 SELECT o.id, o.name, o.creatorid, o.containerid, o.description, o.title, o.description AS body, 'Blog.class'::text AS objecttype
                                                                                   FROM objectsflat o
                                                                                  WHERE o.objecttype::text = 'BLOG'::text)
                                                                UNION 
                                                                         SELECT o.id, o.name, o.creatorid, o.threadid AS containerid, o.description, o.title, o.message AS body, 'TextMessage.class'::text AS objecttype
                                                                           FROM objectsflat o
                                                                          WHERE o.objecttype::text = 'MESSAGE'::text OR o.objecttype::text = 'TEXTMESSAGE'::text)
                                                        UNION 
                                                                 SELECT t.id, t.tagtext AS name, NULL::character varying AS creatorid, NULL::character varying AS containerid, NULL::text AS description, NULL::character varying AS title, NULL::text AS body, 'Tag.class'::text AS objecttype
                                                                   FROM tags t)
                                                UNION 
                                                         SELECT up.id, up.text AS name, NULL::character varying AS creatorid, o.id AS containerid, NULL::text AS description, NULL::character varying AS title, NULL::text AS body, 'UserProfile.class'::text AS objecttype
                                                           FROM userprofiles up
                                                     CROSS JOIN objectsflat o
                                                     WHERE o.profileid::text = up.id::text)
                                        UNION 
                                                 SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'Group.class'::text AS objecttype
                                                   FROM objectsflat o
                                                  WHERE o.objecttype::text = 'GROUP'::text)
                                UNION 
                                         SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'Application.class'::text AS objecttype
                                           FROM objectsflat o
                                          WHERE o.objecttype::text = 'APPLICATION'::text)
                        UNION 
                                 SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'Workflow.class'::text AS objecttype
                                   FROM objectsflat o
                                  WHERE o.objecttype::text = 'WORKFLOWDOCUMENTRECORD'::text)
                UNION 
                         SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'DocumentRecord.class'::text AS objecttype
                           FROM objectsflat o
                          WHERE o.objecttype::text = 'DOCUMENTRECORD'::text)
        UNION 
                 SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'WorkflowService.class'::text AS objecttype
                   FROM objectsflat o
                  WHERE o.objecttype::text = 'WORKFLOWSERVICE'::text)
UNION 
         SELECT o.id, o.name, o.creatorid, o.containerid, o.description, NULL::character varying AS title, NULL::text AS body, 'Folder.class'::text AS objecttype
           FROM objectsflat o
          WHERE o.objecttype::text = 'FOLDER'::text OR o.objecttype::text = 'WORKFLOWINVOCATION'::text OR o.objecttype::text = 'EXTERNALOBJECT'::text OR o.objecttype::text = 'LINKSFOLDER'::text OR o.objecttype::text = 'EVENT'::text OR o.objecttype::text = 'GROUPEVENT'::text;

ALTER TABLE search_union
  OWNER TO connexience;

