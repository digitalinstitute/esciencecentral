vacuum;

--Create the e-SC Indexes
DROP INDEX IF EXISTS dv_recordid;
DROP INDEX IF EXISTS objflat_invid;
DROP INDEX IF EXISTS objflat_objecttype;
DROP INDEX IF EXISTS objflat_containerid;
DROP INDEX IF EXISTS objflat_creatorid;
DROP INDEX IF EXISTS objflat_id;
DROP INDEX IF EXISTS perms_principalid;
DROP INDEX IF EXISTS perms_targetid;
CREATE INDEX dv_recordid ON documentversions(documentrecordid);
CREATE INDEX objflat_invid ON objectsflat(invocationid);
CREATE INDEX objflat_objecttype ON objectsflat(objecttype);
CREATE INDEX objflat_containerid ON objectsflat(containerid);
CREATE INDEX objflat_creatorid ON objectsflat(creatorid);
CREATE INDEX objflat_id ON objectsflat(id);
CREATE INDEX perms_principalid ON permissions(principalid);
CREATE INDEX perms_targetid ON permissions(targetobjectid);

DROP INDEX IF EXISTS objflat_invstatus;
CREATE INDEX objflat_invstatus ON objectsflat(invocationStatus);

DROP INDEX IF EXISTS loggerconfigurations_id_idx;
DROP INDEX IF EXISTS loggerdeployments_id_idx;
DROP INDEX IF EXISTS loggerdeployments_logger_id_idx;
DROP INDEX IF EXISTS loggerdeployments_study_id_idx;
DROP INDEX IF EXISTS loggerdeployments_subjectgroup_id_idx;
DROP INDEX IF EXISTS loggerdeployments_loggerconfiguration_id_idx;
CREATE INDEX loggerconfigurations_id_idx ON loggerconfigurations(id);
CREATE INDEX loggerdeployments_id_idx ON loggerdeployments(id);
CREATE INDEX loggerdeployments_logger_id_idx ON loggerdeployments(logger_id);
CREATE INDEX loggerdeployments_study_id_idx ON loggerdeployments(study_id);
CREATE INDEX loggerdeployments_subjectgroup_id_idx ON loggerdeployments(subjectgroup_id);
CREATE INDEX loggerdeployments_loggerconfiguration_id_idx ON loggerdeployments(loggerconfiguration_id);

DROP INDEX IF EXISTS loggers_id_idx;
DROP INDEX IF EXISTS loggers_loggertype_id_idx;
DROP INDEX IF EXISTS loggers_serialnumber_idx;
CREATE INDEX loggers_id_idx ON loggers(id);
CREATE INDEX loggers_loggertype_id_idx ON loggers(loggertype_id);
CREATE INDEX loggers_serialnumber_idx ON loggers(serialnumber);

DROP INDEX IF EXISTS loggertypes_id_idx;
CREATE INDEX loggertypes_id_idx ON loggertypes(id);

DROP INDEX IF EXISTS phases_id_idx;
DROP INDEX IF EXISTS phases_study_id_idx;
CREATE INDEX phases_id_idx ON phases(id);
CREATE INDEX phases_study_id_idx ON phases(study_id);

DROP INDEX IF EXISTS projects_id_idx;
CREATE INDEX projects_id_idx ON projects(id);

DROP INDEX IF EXISTS subjectgroups_id_idx;
DROP INDEX IF EXISTS subjectgroups_phase_id_idx;
DROP INDEX IF EXISTS subjectgroups_parent_id_idx;
CREATE INDEX subjectgroups_id_idx ON subjectgroups(id);
CREATE INDEX subjectgroups_phase_id_idx ON subjectgroups(phase_id);
CREATE INDEX subjectgroups_parent_id_idx ON subjectgroups(parent_id);

DROP INDEX IF EXISTS subjects_id_idx;
DROP INDEX IF EXISTS subjects_subjectgroup_id_idx;
CREATE INDEX subjects_id_idx ON subjects(id);
CREATE INDEX subjects_subjectgroup_id_idx ON subjects(subjectgroup_id);

vacuum;
