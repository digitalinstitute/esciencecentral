DROP INDEX IF EXISTS record_status;
CREATE INDEX record_status ON record(status);

DROP INDEX IF EXISTS record_invocationid;
CREATE INDEX record_invocationid ON record(invocationid);
