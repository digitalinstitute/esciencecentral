--
-- PostgreSQL database dump
--

-- Started on 2011-02-03 10:11:09 GMT

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 2008 (class 1262 OID 42529)
-- Name: connexience; Type: DATABASE; Schema: -; Owner: connexiencedb
--

CREATE DATABASE connexience WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE connexience OWNER TO connexiencedb;

\connect connexience

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1579 (class 1259 OID 42530)
-- Dependencies: 3
-- Name: accountactvity; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE accountactvity (
    id bigint NOT NULL,
    accountid character varying(255),
    purchasetime bigint,
    currency character varying(255),
    cost numeric(19,2),
    type character varying(255),
    quantity integer,
    transactionid character varying(255),
    processorname character varying(255)
);


ALTER TABLE public.accountactvity OWNER TO connexiencedb;

--
-- TOC entry 1580 (class 1259 OID 42538)
-- Dependencies: 3
-- Name: appsubscriptions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE appsubscriptions (
    id bigint NOT NULL,
    userid character varying(255),
    applicationid character varying(255),
    executeworkflows boolean,
    postnews boolean,
    viewconnections boolean,
    viewfiles boolean,
    viewdetails boolean,
    uploadfiles boolean,
    modifyfiles boolean
);


ALTER TABLE public.appsubscriptions OWNER TO connexiencedb;

--
-- TOC entry 1581 (class 1259 OID 42546)
-- Dependencies: 3
-- Name: blockhelp; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE blockhelp (
    id character varying(255) NOT NULL,
    serviceid character varying(255),
    versionid character varying(255),
    htmldata text
);


ALTER TABLE public.blockhelp OWNER TO connexiencedb;

--
-- TOC entry 1582 (class 1259 OID 42554)
-- Dependencies: 3
-- Name: blogpostread; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE blogpostread (
    id character varying(255) NOT NULL,
    blogid character varying(255),
    postid character varying(255),
    userid character varying(255)
);


ALTER TABLE public.blogpostread OWNER TO connexiencedb;

--
-- TOC entry 1583 (class 1259 OID 42562)
-- Dependencies: 3
-- Name: bookcontent; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE bookcontent (
    id bigint NOT NULL,
    contenttype character varying(255) NOT NULL,
    sectionid character varying(255),
    authorid character varying(255),
    signature bytea,
    signaturetimestamp date,
    approverid character varying(255),
    approversignature bytea,
    approversignaturetimestamp date,
    reviewerid character varying(255),
    reviewersignature bytea,
    reviewersignaturetimestamp date,
    textcontent text
);


ALTER TABLE public.bookcontent OWNER TO connexiencedb;

--
-- TOC entry 1584 (class 1259 OID 42570)
-- Dependencies: 3
-- Name: clusterenvironments; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE clusterenvironments (
    id bigint NOT NULL,
    clusterid character varying(255),
    environmentfileid character varying(255),
    environmentid character varying(255)
);


ALTER TABLE public.clusterenvironments OWNER TO connexiencedb;

--
-- TOC entry 1585 (class 1259 OID 42578)
-- Dependencies: 3
-- Name: clusterjobs; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE clusterjobs (
    id bigint NOT NULL,
    userid character varying(255),
    jobid character varying(255),
    status integer,
    executionok boolean,
    submissiontime timestamp without time zone,
    executionstarttime timestamp without time zone,
    executionendtime timestamp without time zone,
    clusterid character varying(255),
    results text,
    folderid character varying(255)
);


ALTER TABLE public.clusterjobs OWNER TO connexiencedb;

--
-- TOC entry 1586 (class 1259 OID 42586)
-- Dependencies: 3
-- Name: documentversions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE documentversions (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentrecordid character varying(255),
    signaturedata bytea,
    "timestamp" timestamp without time zone,
    versionnumber integer,
    size bigint,
    comments character varying(255),
    certificatedata bytea
);


ALTER TABLE public.documentversions OWNER TO connexiencedb;

--
-- TOC entry 1587 (class 1259 OID 42594)
-- Dependencies: 3
-- Name: downloadreservations; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE downloadreservations (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentid character varying(255),
    versionid character varying(255),
    "timestamp" timestamp without time zone
);


ALTER TABLE public.downloadreservations OWNER TO connexiencedb;

--
-- TOC entry 1588 (class 1259 OID 42602)
-- Dependencies: 3
-- Name: externallogondetails; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE externallogondetails (
    id character varying(255) NOT NULL,
    userid character varying(255),
    externaluserid character varying(255)
);


ALTER TABLE public.externallogondetails OWNER TO connexiencedb;

--
-- TOC entry 1589 (class 1259 OID 42610)
-- Dependencies: 3
-- Name: groupmembership; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE groupmembership (
    id bigint NOT NULL,
    userid character varying(255),
    groupid character varying(255)
);


ALTER TABLE public.groupmembership OWNER TO connexiencedb;

--
-- TOC entry 1590 (class 1259 OID 42618)
-- Dependencies: 3
-- Name: groupprofiles; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE groupprofiles (
    id character varying(255) NOT NULL
);


ALTER TABLE public.groupprofiles OWNER TO connexiencedb;

--
-- TOC entry 1619 (class 1259 OID 42844)
-- Dependencies: 3
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: connexiencedb
--

CREATE SEQUENCE hibernate_sequence
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO connexiencedb;

--
-- TOC entry 2011 (class 0 OID 0)
-- Dependencies: 1619
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: connexiencedb
--

SELECT pg_catalog.setval('hibernate_sequence', 13, true);


--
-- TOC entry 1591 (class 1259 OID 42623)
-- Dependencies: 3
-- Name: images; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE images (
    id character varying(255) NOT NULL,
    serverobjectid character varying(255),
    type character varying(255),
    data bytea
);


ALTER TABLE public.images OWNER TO connexiencedb;

--
-- TOC entry 1592 (class 1259 OID 42631)
-- Dependencies: 3
-- Name: keys; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE keys (
    id character varying(255) NOT NULL,
    objectid character varying(255),
    keystoredata bytea,
    certificate bytea
);


ALTER TABLE public.keys OWNER TO connexiencedb;

--
-- TOC entry 1593 (class 1259 OID 42639)
-- Dependencies: 3
-- Name: logevents; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logevents (
    id character varying(255) NOT NULL,
    operation character varying(255) NOT NULL,
    "timestamp" bigint,
    userid character varying(255),
    principalname character varying(255),
    workflowid character varying(255),
    invocationid character varying(255),
    versionid character varying(255),
    workflowname character varying(255),
    state character varying(255),
    objectid character varying(255),
    objectname character varying(255),
    objecttype character varying(255),
    serviceid character varying(255),
    serviceversionid character varying(255),
    serviceversionnumber integer,
    servicename character varying(255),
    granteeid character varying(255),
    granteetype character varying(255),
    granteename character varying(255)
);


ALTER TABLE public.logevents OWNER TO connexiencedb;

--
-- TOC entry 1594 (class 1259 OID 42647)
-- Dependencies: 3
-- Name: logeventsannonymised; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logeventsannonymised (
    id integer NOT NULL,
    operation character varying(255) NOT NULL,
    "timestamp" bigint,
    userid integer,
    workflowid integer,
    invocationid integer,
    versionid integer,
    state character varying(255),
    objectid integer,
    objecttype character varying(255),
    granteeid integer,
    granteetype character varying(255)
);


ALTER TABLE public.logeventsannonymised OWNER TO connexiencedb;

--
-- TOC entry 1595 (class 1259 OID 42655)
-- Dependencies: 3
-- Name: logmessageproperties; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logmessageproperties (
    id bigint NOT NULL,
    logmessageid bigint,
    name character varying(255),
    value character varying(255)
);


ALTER TABLE public.logmessageproperties OWNER TO connexiencedb;

--
-- TOC entry 1596 (class 1259 OID 42663)
-- Dependencies: 3
-- Name: logmessages; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logmessages (
    id bigint NOT NULL,
    messagedate timestamp without time zone,
    messagetext character varying(255),
    objectid character varying(255),
    organisationid character varying(255),
    principalid character varying(255),
    type integer
);


ALTER TABLE public.logmessages OWNER TO connexiencedb;

--
-- TOC entry 1597 (class 1259 OID 42671)
-- Dependencies: 3
-- Name: logondetails; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logondetails (
    id bigint NOT NULL,
    userid character varying(255),
    logonname character varying(255),
    hashedpassword character varying(255)
);


ALTER TABLE public.logondetails OWNER TO connexiencedb;

--
-- TOC entry 1598 (class 1259 OID 42679)
-- Dependencies: 3
-- Name: metadata; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE metadata (
    id bigint NOT NULL,
    objectid character varying(255),
    name character varying(255),
    value character varying(255)
);


ALTER TABLE public.metadata OWNER TO connexiencedb;

--
-- TOC entry 1599 (class 1259 OID 42687)
-- Dependencies: 3
-- Name: objectlocators; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE objectlocators (
    id bigint NOT NULL,
    objectid character varying(255),
    partnershipid character varying(255),
    organisationid character varying(255)
);


ALTER TABLE public.objectlocators OWNER TO connexiencedb;

--
-- TOC entry 1600 (class 1259 OID 42695)
-- Dependencies: 3
-- Name: objectsflat; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE objectsflat (
    id character varying(255) NOT NULL,
    objecttype character varying(255) NOT NULL,
    name character varying(255),
    organisationid character varying(255),
    containerid character varying(255),
    description text,
    creatorid character varying(255),
    timeinmillis bigint,
    jndiname character varying(255),
    storedata bytea,
    remotecertificatedata bytea,
    remoteorganisationid character varying(255),
    remotehost character varying(255),
    remoteport integer,
    allowdatafolderbrowse boolean,
    allowgrouplisting boolean,
    allowdatauploads boolean,
    allowpermissionsetting boolean,
    enabled boolean,
    tablename character varying(255),
    idcolumn character varying(255),
    datacolumn character varying(255),
    buffersize integer,
    databasevendor integer,
    directory character varying(255),
    accesskeyid character varying(255),
    accesskey character varying(255),
    organisationbucket character varying(255),
    rootuserid character varying(255),
    firstname character varying(255),
    surname character varying(255),
    defaultgroupid character varying(255),
    homefolderid character varying(255),
    profileid character varying(255),
    webfolderid character varying(255),
    workflowfolderid character varying(255),
    inboxfolderid character varying(255),
    sentmessagesfolderid character varying(255),
    externalobjectsfolderid character varying(255),
    hashkey character varying(255),
    protectedgroup boolean,
    adminapprovejoin boolean,
    nonmemberslist boolean,
    admingroupid character varying(255),
    groupfolderid character varying(255),
    userfolderid character varying(255),
    datafolderid character varying(255),
    projectsfolderid character varying(255),
    trustedfolderid character varying(255),
    datastoreid character varying(255),
    partnersfolderid character varying(255),
    documenttypesfolderid character varying(255),
    databasesfolderid character varying(255),
    clusterfolderid character varying(255),
    servicesfolderid character varying(255),
    applicationsfolderid character varying(255),
    publicgroupid character varying(255),
    defaultuserid character varying(255),
    managernode character varying(255),
    publicvisible boolean,
    partnershipid character varying(255),
    partnerfolderid character varying(255),
    enddate bigint,
    startdate bigint,
    milestone boolean,
    estimatedcompletion integer,
    parentprojectid character varying(255),
    invocationid character varying(255),
    workflowid character varying(255),
    invocationdate timestamp without time zone,
    invocationstatus integer,
    currentblockid character varying(255),
    engineid character varying(255),
    approvergroupid character varying(255),
    approversigningenforced boolean,
    reviewergroupid character varying(255),
    reviewersigningenforced boolean,
    applicationid character varying(255),
    typestring character varying(255),
    documenttypeid character varying(255),
    versioned boolean,
    maxversions integer,
    limitversions boolean,
    currentversionnumber integer,
    intermediatedatastored boolean,
    externaldatasupported boolean,
    externaldatablockname character varying(255),
    enginetype character varying(255),
    category character varying(255),
    libraryname character varying(255),
    formattype integer,
    mimetype character varying(255),
    extension character varying(255),
    homepage character varying(255),
    serviceurl character varying(255),
    serviceroutine character varying(255),
    type integer,
    propertynames bytea,
    propertytypes bytea,
    inputnames bytea,
    inputtypes bytea,
    inputmodes bytea,
    outputnames bytea,
    outputtypes bytea,
    propertydescriptions bytea,
    propertydefaults bytea,
    externallyprovisioned boolean,
    externalscripttype character varying(255),
    externalscriptid character varying(255),
    streammode character varying(255),
    title character varying(255),
    body text,
    secretkey character varying(255),
    applicationurl character varying(255),
    columnurl character varying(255),
    documentediturl character varying(255),
    mimetypelist character varying(255),
    iconurl character varying(255),
    showiniframe boolean,
    documentationurl character varying(255),
    venue character varying(255),
    sourceobjectid character varying(255),
    sinkobjectid character varying(255),
    "timestamp" timestamp without time zone,
    shortname character varying(255),
    privateurl character varying(255),
    blogid character varying(255),
    notification boolean,
    summary text,
    postid character varying(255),
    text text,
    authorname character varying(255),
    message text,
    recipientid text,
    senderid character varying(255),
    read boolean,
    threadid character varying(255),
    isread boolean,
    comment bytea,
    status integer,
    groupid character varying(255),
    label character varying(255),
    itemtype character varying(255),
    mainitemid character varying(255),
    metadataid character varying(255),
    promotionalbalance integer,
    purchasedbalance integer,
    currencylabel character varying(255),
    pagenumber integer
);


ALTER TABLE public.objectsflat OWNER TO connexiencedb;

--
-- TOC entry 1601 (class 1259 OID 42703)
-- Dependencies: 3
-- Name: permissions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE permissions (
    id bigint NOT NULL,
    principalid character varying(255),
    targetobjectid character varying(255),
    type character varying(255),
    universal boolean
);


ALTER TABLE public.permissions OWNER TO connexiencedb;

--
-- TOC entry 1602 (class 1259 OID 42711)
-- Dependencies: 3
-- Name: products; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE products (
    id character varying(255) NOT NULL,
    producttype character varying(255) NOT NULL,
    name character varying(255),
    description character varying(255),
    categorylabel character varying(255),
    ownerid character varying(255),
    expirable boolean,
    duration integer,
    autorenewing boolean,
    productcost integer,
    imageurl character varying(255),
    purchasewithpromotionaltokens boolean,
    singlesubscription boolean,
    objectidorname character varying(255),
    active boolean,
    initialvalue integer,
    units character varying(255),
    value integer
);


ALTER TABLE public.products OWNER TO connexiencedb;

--
-- TOC entry 1603 (class 1259 OID 42719)
-- Dependencies: 3
-- Name: promotialitems; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE promotialitems (
    id bigint NOT NULL,
    quantity integer,
    creationtime bigint,
    expirytime bigint,
    promotioncode character varying(255)
);


ALTER TABLE public.promotialitems OWNER TO connexiencedb;

--
-- TOC entry 1604 (class 1259 OID 42724)
-- Dependencies: 3
-- Name: propertygroups; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE propertygroups (
    id bigint NOT NULL,
    objectproperty boolean,
    objectid character varying(255),
    name character varying(255),
    description character varying(255),
    organisationid character varying(255)
);


ALTER TABLE public.propertygroups OWNER TO connexiencedb;

--
-- TOC entry 1605 (class 1259 OID 42732)
-- Dependencies: 3
-- Name: propertyitems; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE propertyitems (
    id bigint NOT NULL,
    groupid bigint,
    name character varying(255),
    value text
);


ALTER TABLE public.propertyitems OWNER TO connexiencedb;

--
-- TOC entry 1606 (class 1259 OID 42740)
-- Dependencies: 3
-- Name: rememberedlogins; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE rememberedlogins (
    id character varying(255) NOT NULL,
    userid character varying(255),
    cookieid character varying(255),
    expirydate timestamp without time zone
);


ALTER TABLE public.rememberedlogins OWNER TO connexiencedb;

--
-- TOC entry 1607 (class 1259 OID 42748)
-- Dependencies: 3
-- Name: servicehosts; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE servicehosts (
    id bigint NOT NULL,
    ipaddress character varying(255),
    active boolean,
    guid character varying(255)
);


ALTER TABLE public.servicehosts OWNER TO connexiencedb;

--
-- TOC entry 1608 (class 1259 OID 42756)
-- Dependencies: 3
-- Name: serviceinstances; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE serviceinstances (
    id bigint NOT NULL,
    rmiport integer,
    running boolean,
    hostid character varying(255),
    name character varying(255)
);


ALTER TABLE public.serviceinstances OWNER TO connexiencedb;

--
-- TOC entry 1609 (class 1259 OID 42764)
-- Dependencies: 3
-- Name: servicexml; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE servicexml (
    id character varying(255) NOT NULL,
    serviceid character varying(255),
    versionid character varying(255),
    xmldata text
);


ALTER TABLE public.servicexml OWNER TO connexiencedb;

--
-- TOC entry 1610 (class 1259 OID 42772)
-- Dependencies: 3
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE subscriptions (
    id bigint NOT NULL,
    subscriptiontype character varying(255) NOT NULL,
    name character varying(255),
    categorylabel character varying(255),
    userid character varying(255),
    expiry bigint,
    autorenewing boolean,
    productid character varying(255),
    imageurl character varying(255),
    starttime bigint,
    value integer,
    units character varying(255)
);


ALTER TABLE public.subscriptions OWNER TO connexiencedb;

--
-- TOC entry 1611 (class 1259 OID 42780)
-- Dependencies: 3
-- Name: tags; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tags (
    id character varying(255) NOT NULL,
    tagtext text
);


ALTER TABLE public.tags OWNER TO connexiencedb;

--
-- TOC entry 1612 (class 1259 OID 42788)
-- Dependencies: 3
-- Name: tagstoobjects; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tagstoobjects (
    id character varying(255) NOT NULL,
    serverobjectid text,
    tagid text,
    creatorid character varying(255),
    weight integer,
    createdate timestamp without time zone
);


ALTER TABLE public.tagstoobjects OWNER TO connexiencedb;

--
-- TOC entry 1613 (class 1259 OID 42796)
-- Dependencies: 3
-- Name: ticketgroups; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE ticketgroups (
    id bigint NOT NULL,
    ticketid character varying(255),
    groupid character varying(255)
);


ALTER TABLE public.ticketgroups OWNER TO connexiencedb;

--
-- TOC entry 1614 (class 1259 OID 42804)
-- Dependencies: 3
-- Name: tickets; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tickets (
    id character varying(255) NOT NULL,
    organisationid character varying(255),
    userid character varying(255),
    lastaccesstime timestamp without time zone,
    superticket boolean
);


ALTER TABLE public.tickets OWNER TO connexiencedb;

--
-- TOC entry 1615 (class 1259 OID 42812)
-- Dependencies: 3
-- Name: uploadreservations; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE uploadreservations (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentid character varying(255),
    signature bytea,
    "timestamp" timestamp without time zone,
    certificatehash bytea,
    comments character varying(255)
);


ALTER TABLE public.uploadreservations OWNER TO connexiencedb;

--
-- TOC entry 1616 (class 1259 OID 42820)
-- Dependencies: 3
-- Name: userprofiles; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE userprofiles (
    id character varying(255) NOT NULL,
    text text,
    website character varying(255),
    emailaddress character varying(255),
    defaultdomain character varying(255)
);


ALTER TABLE public.userprofiles OWNER TO connexiencedb;

--
-- TOC entry 1617 (class 1259 OID 42828)
-- Dependencies: 3
-- Name: workflowengines; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE workflowengines (
    id character varying(255) NOT NULL,
    engineip character varying(255),
    queuename character varying(255),
    lastaccesstime bigint
);


ALTER TABLE public.workflowengines OWNER TO connexiencedb;

--
-- TOC entry 1618 (class 1259 OID 42836)
-- Dependencies: 3
-- Name: workflowservicelogs; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE workflowservicelogs (
    id bigint NOT NULL,
    invocationid character varying(255),
    contextid character varying(255),
    outputtext text
);


ALTER TABLE public.workflowservicelogs OWNER TO connexiencedb;

--
-- TOC entry 1966 (class 0 OID 42530)
-- Dependencies: 1579
-- Data for Name: accountactvity; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY accountactvity (id, accountid, purchasetime, currency, cost, type, quantity, transactionid, processorname) FROM stdin;
\.


--
-- TOC entry 1967 (class 0 OID 42538)
-- Dependencies: 1580
-- Data for Name: appsubscriptions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY appsubscriptions (id, userid, applicationid, executeworkflows, postnews, viewconnections, viewfiles, viewdetails, uploadfiles, modifyfiles) FROM stdin;
\.


--
-- TOC entry 1968 (class 0 OID 42546)
-- Dependencies: 1581
-- Data for Name: blockhelp; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY blockhelp (id, serviceid, versionid, htmldata) FROM stdin;
\.


--
-- TOC entry 1969 (class 0 OID 42554)
-- Dependencies: 1582
-- Data for Name: blogpostread; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY blogpostread (id, blogid, postid, userid) FROM stdin;
\.


--
-- TOC entry 1970 (class 0 OID 42562)
-- Dependencies: 1583
-- Data for Name: bookcontent; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY bookcontent (id, contenttype, sectionid, authorid, signature, signaturetimestamp, approverid, approversignature, approversignaturetimestamp, reviewerid, reviewersignature, reviewersignaturetimestamp, textcontent) FROM stdin;
\.


--
-- TOC entry 1971 (class 0 OID 42570)
-- Dependencies: 1584
-- Data for Name: clusterenvironments; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY clusterenvironments (id, clusterid, environmentfileid, environmentid) FROM stdin;
\.


--
-- TOC entry 1972 (class 0 OID 42578)
-- Dependencies: 1585
-- Data for Name: clusterjobs; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY clusterjobs (id, userid, jobid, status, executionok, submissiontime, executionstarttime, executionendtime, clusterid, results, folderid) FROM stdin;
\.


--
-- TOC entry 1973 (class 0 OID 42586)
-- Dependencies: 1586
-- Data for Name: documentversions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY documentversions (id, userid, documentrecordid, signaturedata, "timestamp", versionnumber, size, comments, certificatedata) FROM stdin;
\.


--
-- TOC entry 1974 (class 0 OID 42594)
-- Dependencies: 1587
-- Data for Name: downloadreservations; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY downloadreservations (id, userid, documentid, versionid, "timestamp") FROM stdin;
\.


--
-- TOC entry 1975 (class 0 OID 42602)
-- Dependencies: 1588
-- Data for Name: externallogondetails; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY externallogondetails (id, userid, externaluserid) FROM stdin;
\.


--
-- TOC entry 1976 (class 0 OID 42610)
-- Dependencies: 1589
-- Data for Name: groupmembership; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY groupmembership (id, userid, groupid) FROM stdin;
3	8ac3a18f2deaf8d8012deaf9ba54002e	8ac3a18f2deaf8d8012deaf9afb9002b
\.


--
-- TOC entry 1977 (class 0 OID 42618)
-- Dependencies: 1590
-- Data for Name: groupprofiles; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY groupprofiles (id) FROM stdin;
8ac3a18f2deaf8d8012deaf98a4d0022
8ac3a18f2deaf8d8012deaf99ec60026
8ac3a18f2deaf8d8012deaf9afb5002a
\.


--
-- TOC entry 1978 (class 0 OID 42623)
-- Dependencies: 1591
-- Data for Name: images; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY images (id, serverobjectid, type, data) FROM stdin;
\.


--
-- TOC entry 1979 (class 0 OID 42631)
-- Dependencies: 1592
-- Data for Name: keys; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY keys (id, objectid, keystoredata, certificate) FROM stdin;
8ac3a18f2deaf8d8012deaf8ebaf0003	8ac3a18f2deaf8d8012deaf8e7640002	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\352\\370\\351\\301\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\222\\315}\\035\\223\\240\\240\\353\\2663\\241\\237Z\\210\\340|\\332H\\346\\232\\202\\326\\263\\335z"\\357\\207\\012\\241\\325\\247\\251\\375\\010\\\\\\036\\221\\032\\372\\222\\320)\\002\\013\\017\\037!\\245\\252\\300Q3\\216k\\343\\201\\314\\361\\311\\231\\220\\210\\202\\234\\260t\\236\\233\\031<\\365\\331\\013\\016\\177\\252\\223\\012\\007ZBm\\223\\304\\363\\370\\2306\\270\\221\\026g"y-\\000\\222uW\\213\\346\\205\\337\\011\\246a\\026\\213\\354/$\\231_\\255q\\010VO\\363\\346\\262\\207\\211in\\323\\210uH\\256\\221!A^\\237%\\253\\203\\212m\\3760\\305^/<\\211\\330\\224f=\\022,S\\037\\224$\\220\\010\\243\\213\\010\\235\\032\\323\\2729_U6;m\\255\\201\\2535\\231\\012\\367E+\\367\\016\\003\\323\\321\\375\\031\\037\\366\\374\\211\\255\\331<\\335\\350P\\345ND\\252V\\330\\355\\227\\003\\227n,\\360\\313\\003\\232\\377\\364P\\376\\022\\006\\313\\327)\\345\\3179z6\\367\\333\\346Y&\\375k1\\207j\\3338$Mp\\306H},\\022f\\217\\372\\031\\252\\250>4\\267wn\\341\\011\\246BC0\\226\\301\\275\\316\\224d\\354H36\\347\\254\\007!\\347\\015\\305\\310\\262\\351\\213\\354P\\004\\372x5\\011c\\254\\210\\3012\\332\\011GK\\312\\332\\201\\025t\\302\\346\\300\\223\\330Q\\340\\244\\220\\236Xn\\364\\276\\315\\030$\\373_\\003-R\\3607\\022-\\231\\352\\301#D\\367I^\\263\\257-{\\226\\311b\\2115O7\\262\\233\\355\\231y\\217+\\241Z\\327@\\026p\\004y\\022n\\006\\315W6\\027\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3610\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ})0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf8e76400020\\036\\027\\015110203100217Z\\027\\015210131100217Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf8e76400020\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\2007@l8x\\330T\\214\\366\\273\\313(\\273"\\260\\220\\363\\244\\343\\340\\236V@\\212\\024o\\303\\267\\234\\361\\254\\205\\225\\227-\\220\\3126T\\2633y*\\021$H\\277qQ\\\\\\300\\332\\0344l5\\3706\\357lcV\\211v\\253\\313\\026\\377\\323\\325\\037{\\032\\313 \\234b\\336\\230-\\210\\207\\341f\\220<\\030\\346\\257\\344:\\342NX\\240\\313\\305k\\264\\340\\350\\316\\231\\351\\226\\322\\\\5\\213\\010\\361\\013\\216W\\037h\\311\\324\\267\\351\\306B'\\324\\276\\377\\204\\0050\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024C\\037\\214\\243\\240"\\246#1\\002\\353\\030\\371\\344\\207\\320\\177\\353&\\217\\002\\024\\004\\337\\275.\\316\\010\\225/\\251\\356O\\026\\221;\\247Hc\\001\\027e)\\032K\\366\\021\\015\\211\\320>\\241yk\\230\\017\\353\\371\\257Y\\325o	0\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ})0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf8e76400020\\036\\027\\015110203100217Z\\027\\015210131100217Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf8e76400020\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\2007@l8x\\330T\\214\\366\\273\\313(\\273"\\260\\220\\363\\244\\343\\340\\236V@\\212\\024o\\303\\267\\234\\361\\254\\205\\225\\227-\\220\\3126T\\2633y*\\021$H\\277qQ\\\\\\300\\332\\0344l5\\3706\\357lcV\\211v\\253\\313\\026\\377\\323\\325\\037{\\032\\313 \\234b\\336\\230-\\210\\207\\341f\\220<\\030\\346\\257\\344:\\342NX\\240\\313\\305k\\264\\340\\350\\316\\231\\351\\226\\322\\\\5\\213\\010\\361\\013\\216W\\037h\\311\\324\\267\\351\\306B'\\324\\276\\377\\204\\0050\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024C\\037\\214\\243\\240"\\246#1\\002\\353\\030\\371\\344\\207\\320\\177\\353&\\217\\002\\024\\004\\337\\275.\\316\\010\\225/\\251\\356O\\026\\221;\\247Hc\\001\\027e
8ac3a18f2deaf8d8012deaf9602a0006	8ac3a18f2deaf8d8012deaf95c050005	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\352\\371^\\007\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\026thK\\240\\244~\\305l\\350kN\\034\\356\\011{\\212\\307A\\311\\341\\304t \\004\\322\\260\\210(\\020%\\202\\021Y\\267o\\317\\340;\\332\\366\\363\\024\\362\\370\\340'X\\306\\000\\342\\215\\002\\343C\\372Qu\\316h\\260\\300\\300\\222b\\242\\247\\2068=\\004\\370\\202\\344\\240\\321\\202y\\000\\015\\202\\2324+\\346\\376o`\\346\\321<\\225\\227\\360\\220\\017\\277^\\331\\234\\311\\332\\017\\201]$8=\\220\\260\\353\\220\\011\\357\\353:\\012\\307G\\276\\303\\370\\233o\\030Q3sc\\370\\352u\\334\\275\\334\\276j\\221RQC!\\361M\\225?#Q\\213\\015\\256\\237Xs\\360\\251\\023\\225d\\342m\\374\\321\\004\\257@\\366\\255\\002\\316\\270\\201\\337\\3630\\272}\\266\\247~ \\031;\\035C\\006\\347^\\204\\227S\\306O\\275\\277\\317\\011\\262\\365\\013\\307.#\\026&\\232m\\262\\253\\351\\1774L\\203\\225\\254d\\272\\377\\350M\\362\\347o\\206\\225h{\\267\\324]^\\035\\355\\034\\234\\264\\330\\271\\206:\\317\\014\\320\\002\\034\\305N\\274\\253N\\2608qK\\257\\370\\241\\272\\236\\201\\247\\245\\037\\233\\2365G\\331| xQ\\350;XK\\023\\325\\362?\\312\\222!u%\\242\\222\\312\\200\\021\\335he\\326\\304\\233\\267\\210\\2172\\324~\\21080\\300\\217D\\235\\316\\235\\252b\\213\\373\\316\\276\\210wZr\\222\\0327\\011\\336\\331\\001\\273W \\240\\370\\007\\246 \\033w`5\\332s\\365\\025\\337\\311\\306|2'o\\261\\232\\003\\250\\334\\024m\\3113D\\015\\0149\\265\\327"k\\247\\334v\\353\\265^\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3630\\202\\002\\3570\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MJ}G0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf95c0500050\\036\\027\\015110203100247Z\\027\\015210131100247Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf95c0500050\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\316\\273\\017\\012\\274]\\326\\036\\006\\257\\035\\0245l\\257'\\341\\254\\201\\251@^\\216Zr\\357\\265\\243*+\\340q\\256\\000\\361\\251\\237\\340\\257\\345\\021\\004\\020\\221\\314VI't\\326}\\314\\252m\\204\\377i\\210\\010h\\372\\202vB\\244t\\017\\376am*\\256N|S\\301RYU\\311I\\275\\336+\\037\\257\\325\\202\\3026\\232\\205T\\247\\325\\276t\\315\\236\\215C\\354\\220\\313\\250\\252\\236\\3555\\313\\205\\366\\210\\027\\265o\\203\\231\\355;gK*.\\003\\377\\315\\0030\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\025\\000\\206\\321\\250<"\\330D\\235!\\225X"\\332\\226\\345g\\355\\007\\3208\\002\\024wf\\244\\015\\022\\226RcW\\014\\227c\\277\\375\\225\\246f)\\024\\213\\274\\272\\0117\\201\\253u\\234n\\342\\373\\035\\000\\337\\0243\\013\\326I\\206	0\\202\\002\\3570\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MJ}G0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf95c0500050\\036\\027\\015110203100247Z\\027\\015210131100247Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf95c0500050\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\316\\273\\017\\012\\274]\\326\\036\\006\\257\\035\\0245l\\257'\\341\\254\\201\\251@^\\216Zr\\357\\265\\243*+\\340q\\256\\000\\361\\251\\237\\340\\257\\345\\021\\004\\020\\221\\314VI't\\326}\\314\\252m\\204\\377i\\210\\010h\\372\\202vB\\244t\\017\\376am*\\256N|S\\301RYU\\311I\\275\\336+\\037\\257\\325\\202\\3026\\232\\205T\\247\\325\\276t\\315\\236\\215C\\354\\220\\313\\250\\252\\236\\3555\\313\\205\\366\\210\\027\\265o\\203\\231\\355;gK*.\\003\\377\\315\\0030\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\025\\000\\206\\321\\250<"\\330D\\235!\\225X"\\332\\226\\345g\\355\\007\\3208\\002\\024wf\\244\\015\\022\\226RcW\\014\\227c\\277\\375\\225\\246f)\\024\\213
8ac3a18f2deaf8d8012deaf9be790030	8ac3a18f2deaf8d8012deaf9ba54002e	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\352\\371\\274V\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\310WD\\015\\227\\213\\021o!@\\201\\276\\232\\276Y}\\251\\372\\377&5\\204\\231'\\247\\215Y\\025D\\242\\351\\1778Z\\277E\\243\\247\\266\\252\\341\\320\\022\\211q\\343\\007jjSV\\216\\361\\241r\\304\\315\\224\\002\\021\\234<)(\\271\\311\\223\\361k\\262\\325\\346\\215\\011\\011\\260\\240z~O|\\364\\375"\\317\\231]\\307\\267y\\354\\352\\036\\241\\255P{3\\250M;\\303#\\252\\253b\\035\\351\\204B\\361\\240ll\\211\\204\\252\\210\\237\\224\\013[\\033\\365\\020c\\030\\207\\233MX6\\316I\\013v(\\213.6\\317U\\233\\362Rq\\201\\026f\\301\\264O\\232\\312\\271\\035\\2762\\256,\\035\\356\\354N\\035}\\226\\350\\3621\\375^PZ\\335+\\246\\037\\316w\\341\\340\\0143o\\336w\\252\\255\\201&\\261\\025"\\313\\235LJ'\\262O\\370H\\237\\233BCc\\324\\224\\2411I\\377\\262\\347\\322\\256\\015\\270>N9\\347\\245W?k\\213\\303\\307\\301\\317\\232\\022\\347\\373\\366\\314\\330\\227\\270\\342\\247OXq\\217\\345\\023\\312k_\\275&\\250\\036\\225r-{T\\261$\\227\\030\\225\\250v\\225\\215\\373\\204F\\24548\\032\\373'4\\002\\365\\335\\326V\\251\\255\\272\\342\\323\\007\\261Q\\000\\273\\220LC*o\\205B\\271\\310\\227\\3323Q \\271\\364i\\226\\365\\266\\352YZM\\374qG\\367G\\212\\207\\230\\331i\\301a5\\265\\371\\317\\366\\321Ib5\\370\\364?\\035.K\\352<\\262\\032w\\356\\272Na\\276\\\\T\\251\\212\\006 \\037-\\337\\345\\313\\222\\377*X\\326\\276\\006\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3610\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ}_0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf9ba54002e0\\036\\027\\015110203100311Z\\027\\015210131100311Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf9ba54002e0\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200,\\207\\321.0m\\010\\355\\000\\276\\375\\344\\036"\\260\\201\\016\\367\\333ru\\023!\\357+\\331w\\037GB\\227\\274?\\031\\344\\013\\216\\351\\206\\220\\032\\217*\\262yI\\3561\\234\\030\\226\\337\\363W\\234\\277CR\\244<\\224m\\262\\362\\310\\341\\260\\013\\300\\255\\324xt\\005\\234\\0231\\337\\354\\016\\024VM\\313G\\242\\300\\244\\2561\\302\\271\\2468N\\215\\012\\315o2\\326\\356\\353P\\220b\\274.he\\346\\007\\030(S<3\\354\\300\\007\\312\\325\\241\\214Tr\\330\\3410\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024`ZI\\211A\\216\\262*\\325?\\036\\207\\016n\\236\\3026\\263\\353\\313\\002\\024k\\314\\330\\017\\224\\272\\036Od\\274\\326tfZ\\272!\\367\\351\\221\\313\\2307\\177\\220\\004\\000\\343"\\362\\377h\\255\\251\\020n\\226X\\350\\323\\314	0\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ}_0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf9ba54002e0\\036\\027\\015110203100311Z\\027\\015210131100311Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deaf8d8012deaf9ba54002e0\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200,\\207\\321.0m\\010\\355\\000\\276\\375\\344\\036"\\260\\201\\016\\367\\333ru\\023!\\357+\\331w\\037GB\\227\\274?\\031\\344\\013\\216\\351\\206\\220\\032\\217*\\262yI\\3561\\234\\030\\226\\337\\363W\\234\\277CR\\244<\\224m\\262\\362\\310\\341\\260\\013\\300\\255\\324xt\\005\\234\\0231\\337\\354\\016\\024VM\\313G\\242\\300\\244\\2561\\302\\271\\2468N\\215\\012\\315o2\\326\\356\\353P\\220b\\274.he\\346\\007\\030(S<3\\354\\300\\007\\312\\325\\241\\214Tr\\330\\3410\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024`ZI\\211A\\216\\262*\\325?\\036\\207\\016n\\236\\3026\\263\\353\\313\\002\\024k\\314\\330\\017\\224\\272\\036Od\\274\\326tfZ\\272!\\367\\351\\221\\313
\.


--
-- TOC entry 1980 (class 0 OID 42639)
-- Dependencies: 1593
-- Data for Name: logevents; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logevents (id, operation, "timestamp", userid, principalname, workflowid, invocationid, versionid, workflowname, state, objectid, objectname, objecttype, serviceid, serviceversionid, serviceversionnumber, servicename, granteeid, granteetype, granteename) FROM stdin;
8ac3a18f2deaf8d8012deaf960980008	WRITE	1296727367815	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf960610007	Users	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960c00009	WRITE	1296727367867	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960e6000b	WRITE	1296727367887	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf960c6000a	Groups	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960f4000c	WRITE	1296727367920	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf96120000e	WRITE	1296727367964	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf960fa000d	Data	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9612d000f	WRITE	1296727367977	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961380011	WRITE	1296727367988	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf9612e0010	Partnerships	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961470012	WRITE	1296727368003	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961560014	WRITE	1296727368019	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf9614d0013	Document Types	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961610015	WRITE	1296727368030	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9616b0017	WRITE	1296727368040	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf961620016	Databases	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961760018	WRITE	1296727368051	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf96182001a	WRITE	1296727368062	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf961770019	Clusters	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9618e001b	WRITE	1296727368074	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9619b001d	WRITE	1296727368087	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf9618f001c	Services	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961ab001e	WRITE	1296727368103	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961b40020	WRITE	1296727368113	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf961ac001f	Applications	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961bf0021	WRITE	1296727368124	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf98a6d0024	WRITE	1296727378537	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	admin	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf98a770025	MAKE_GROUP	1296727378547	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	admin	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf99ed10028	WRITE	1296727383759	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	users	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf99ed90029	MAKE_GROUP	1296727383766	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	users	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9afc2002c	WRITE	1296727388095	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf9afb9002b	Public	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9afdc002d	MAKE_GROUP	1296727388121	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf9afb9002b	Public	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9ba61002f	WRITE	1296727390809	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf9ba54002e	Public User	user	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7a740032	WRITE	1296727439985	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7a6b0031	File Data Store	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7ae00033	WRITE	1296727440093	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7aed0035	WRITE	1296727440106	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7ae50034	Groups	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7af70036	WRITE	1296727440116	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b040038	WRITE	1296727440129	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7afc0037	Data	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b0f0039	WRITE	1296727440140	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b17003b	WRITE	1296727440148	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b10003a	Partnerships	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b21003c	WRITE	1296727440158	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b2a003e	WRITE	1296727440167	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b22003d	Document Types	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b34003f	WRITE	1296727440177	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b3c0041	WRITE	1296727440185	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b350040	Databases	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b450042	WRITE	1296727440194	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b4d0044	WRITE	1296727440202	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b460043	Clusters	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b560045	WRITE	1296727440212	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b5e0047	WRITE	1296727440220	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b570046	Services	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b680048	WRITE	1296727440229	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b70004a	WRITE	1296727440237	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa7b690049	Applications	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b79004b	WRITE	1296727440247	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d74004c	WRITE	1296727444848	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d86004e	WRITE	1296727444867	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8d7e004d	Data	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d90004f	WRITE	1296727444877	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d970051	WRITE	1296727444885	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8d910050	Partnerships	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8da10052	WRITE	1296727444894	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8daa0054	WRITE	1296727444904	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8da30053	Document Types	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8db40055	WRITE	1296727444913	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dbc0057	WRITE	1296727444921	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8db40056	Databases	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dc50058	WRITE	1296727444930	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dcd005a	WRITE	1296727444938	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8dc60059	Clusters	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dd6005b	WRITE	1296727444947	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8ddd005d	WRITE	1296727444955	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8dd7005c	Services	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8de7005e	WRITE	1296727444964	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8def0060	WRITE	1296727444972	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deafa8de8005f	Applications	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8df90061	WRITE	1296727444982	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	0	\N	\N	8ac3a18f2deaf8d8012deaf95c050005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafd12750062	SHARE	1296727609946	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf961ac001f	8ac3a18f2deaf8d8012deaf961ac001f	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	group	UnknownUser
8ac3a18f2deaf8d8012deafd128a0063	SHARE	1296727609982	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf961ac001f	8ac3a18f2deaf8d8012deaf961ac001f	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	group	UnknownUser
8ac3a18f2deaf8d8012deafd12a40064	SHARE	1296727610002	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf961ac001f	8ac3a18f2deaf8d8012deaf961ac001f	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	group	UnknownUser
8ac3a18f2deaf8d8012deafd12b80065	SHARE	1296727610028	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf961ac001f	8ac3a18f2deaf8d8012deaf961ac001f	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	group	UnknownUser
8ac3a18f2deaf8d8012deafd63ad0066	SHARE	1296727630729	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf9614d0013	8ac3a18f2deaf8d8012deaf9614d0013	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	group	UnknownUser
8ac3a18f2deaf8d8012deafd63c20067	SHARE	1296727630773	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf9614d0013	8ac3a18f2deaf8d8012deaf9614d0013	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	group	UnknownUser
8ac3a18f2deaf8d8012deafdb5fe0068	SHARE	1296727651823	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf960c6000a	8ac3a18f2deaf8d8012deaf960c6000a	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	group	UnknownUser
8ac3a18f2deaf8d8012deafdb6170069	SHARE	1296727651848	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf960c6000a	8ac3a18f2deaf8d8012deaf960c6000a	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	group	UnknownUser
8ac3a18f2deaf8d8012deafde432006a	SHARE	1296727663651	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf960610007	8ac3a18f2deaf8d8012deaf960610007	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	group	UnknownUser
8ac3a18f2deaf8d8012deafde448006b	SHARE	1296727663675	8ac3a18f2deaf8d8012deaf8e7420001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf960610007	8ac3a18f2deaf8d8012deaf960610007	folder	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	group	UnknownUser
\.


--
-- TOC entry 1981 (class 0 OID 42647)
-- Dependencies: 1594
-- Data for Name: logeventsannonymised; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logeventsannonymised (id, operation, "timestamp", userid, workflowid, invocationid, versionid, state, objectid, objecttype, granteeid, granteetype) FROM stdin;
\.


--
-- TOC entry 1982 (class 0 OID 42655)
-- Dependencies: 1595
-- Data for Name: logmessageproperties; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logmessageproperties (id, logmessageid, name, value) FROM stdin;
\.


--
-- TOC entry 1983 (class 0 OID 42663)
-- Dependencies: 1596
-- Data for Name: logmessages; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logmessages (id, messagedate, messagetext, objectid, organisationid, principalid, type) FROM stdin;
\.


--
-- TOC entry 1984 (class 0 OID 42671)
-- Dependencies: 1597
-- Data for Name: logondetails; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logondetails (id, userid, logonname, hashedpassword) FROM stdin;
2	8ac3a18f2deaf8d8012deaf8e7420001	root	5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8
\.


--
-- TOC entry 1985 (class 0 OID 42679)
-- Dependencies: 1598
-- Data for Name: metadata; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY metadata (id, objectid, name, value) FROM stdin;
\.


--
-- TOC entry 1986 (class 0 OID 42687)
-- Dependencies: 1599
-- Data for Name: objectlocators; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY objectlocators (id, objectid, partnershipid, organisationid) FROM stdin;
\.


--
-- TOC entry 1987 (class 0 OID 42695)
-- Dependencies: 1600
-- Data for Name: objectsflat; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY objectsflat (id, objecttype, name, organisationid, containerid, description, creatorid, timeinmillis, jndiname, storedata, remotecertificatedata, remoteorganisationid, remotehost, remoteport, allowdatafolderbrowse, allowgrouplisting, allowdatauploads, allowpermissionsetting, enabled, tablename, idcolumn, datacolumn, buffersize, databasevendor, directory, accesskeyid, accesskey, organisationbucket, rootuserid, firstname, surname, defaultgroupid, homefolderid, profileid, webfolderid, workflowfolderid, inboxfolderid, sentmessagesfolderid, externalobjectsfolderid, hashkey, protectedgroup, adminapprovejoin, nonmemberslist, admingroupid, groupfolderid, userfolderid, datafolderid, projectsfolderid, trustedfolderid, datastoreid, partnersfolderid, documenttypesfolderid, databasesfolderid, clusterfolderid, servicesfolderid, applicationsfolderid, publicgroupid, defaultuserid, managernode, publicvisible, partnershipid, partnerfolderid, enddate, startdate, milestone, estimatedcompletion, parentprojectid, invocationid, workflowid, invocationdate, invocationstatus, currentblockid, engineid, approvergroupid, approversigningenforced, reviewergroupid, reviewersigningenforced, applicationid, typestring, documenttypeid, versioned, maxversions, limitversions, currentversionnumber, intermediatedatastored, externaldatasupported, externaldatablockname, enginetype, category, libraryname, formattype, mimetype, extension, homepage, serviceurl, serviceroutine, type, propertynames, propertytypes, inputnames, inputtypes, inputmodes, outputnames, outputtypes, propertydescriptions, propertydefaults, externallyprovisioned, externalscripttype, externalscriptid, streammode, title, body, secretkey, applicationurl, columnurl, documentediturl, mimetypelist, iconurl, showiniframe, documentationurl, venue, sourceobjectid, sinkobjectid, "timestamp", shortname, privateurl, blogid, notification, summary, postid, text, authorname, message, recipientid, senderid, read, threadid, isread, comment, status, groupid, label, itemtype, mainitemid, metadataid, promotionalbalance, purchasedbalance, currencylabel, pagenumber) FROM stdin;
8ac3a18f2deaf8d8012deaf8e7420001	USER	Root User	\N	\N	Root User	\N	1296727336764	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Root	User	\N				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf8e7640002	ROOT	\N	\N	\N	\N	\N	1296727336804	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf8e7420001	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7a6b0031	FILEDATESTORE	File Data Store	8ac3a18f2deaf8d8012deaf95c050005	\N	File Data Store for organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727420230	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	/Users/nsjw7/temp/orgdata	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960610007	USERSFOLDER	Users	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	User accounts	8ac3a18f2deaf8d8012deaf8e7420001	1296727367777	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d7e004d	DATAFOLDER	Data	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Organisation Documents and Data	8ac3a18f2deaf8d8012deaf8e7420001	1296727444862	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960c6000a	GROUPSFOLDER	Groups	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Group details	8ac3a18f2deaf8d8012deaf8e7420001	1296727367878	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7ae50034	GROUPSFOLDER	Groups	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Group details	8ac3a18f2deaf8d8012deaf8e7420001	1296727440101	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf960fa000d	DATAFOLDER	Data	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Organisation Documents and Data	8ac3a18f2deaf8d8012deaf8e7420001	1296727367930	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8d910050	PARTNERSFOLDER	Partnerships	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Partnership connections for this organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727444880	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9612e0010	PARTNERSFOLDER	Partnerships	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Partnership connections for this organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727367982	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf98a620023	GROUP	admin	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deafa7ae50034	administrators	8ac3a18f2deaf8d8012deaf8e7420001	1296727368935	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9614d0013	DOCUMENTTYPESFOLDER	Document Types	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Document types recognised by the organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727368013	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf99eca0027	GROUP	users	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deafa7ae50034		8ac3a18f2deaf8d8012deaf8e7420001	1296727379472	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961620016	DATABASEFOLDER	Databases	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Databases that can be queried by Organisation Users	8ac3a18f2deaf8d8012deaf8e7420001	1296727368034	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9afb9002b	GROUP	Public	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deafa7ae50034	\N	8ac3a18f2deaf8d8012deaf8e7420001	1296727387977	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961770019	CLUSTERFOLDER	Clusters	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Compute Cluster Instances	8ac3a18f2deaf8d8012deaf8e7420001	1296727368055	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7afc0037	DATAFOLDER	Data	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Organisation Documents and Data	8ac3a18f2deaf8d8012deaf8e7420001	1296727440124	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9618f001c	SERVICESFOLDER	Services	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Workflow Services Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727368079	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8da30053	DOCUMENTTYPESFOLDER	Document Types	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Document types recognised by the organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727444899	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf961ac001f	APPLICATIONSFOLDER	Applications	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	External Application Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727368108	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8db40056	DATABASEFOLDER	Databases	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Databases that can be queried by Organisation Users	8ac3a18f2deaf8d8012deaf8e7420001	1296727444916	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf9ba54002e	USER	Public User	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf960610007	\N	8ac3a18f2deaf8d8012deaf8e7420001	1296727390350	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Public	User	8ac3a18f2deaf8d8012deaf9afb9002b				\N	\N	\N	\N	4c08f9e83c78c2268428ae8b6156d097	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b10003a	PARTNERSFOLDER	Partnerships	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Partnership connections for this organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727440144	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dc60059	CLUSTERFOLDER	Clusters	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Compute Cluster Instances	8ac3a18f2deaf8d8012deaf8e7420001	1296727444934	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b22003d	DOCUMENTTYPESFOLDER	Document Types	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Document types recognised by the organisation	8ac3a18f2deaf8d8012deaf8e7420001	1296727440162	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8dd7005c	SERVICESFOLDER	Services	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Workflow Services Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727444951	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa8de8005f	APPLICATIONSFOLDER	Applications	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	External Application Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727444968	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b350040	DATABASEFOLDER	Databases	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Databases that can be queried by Organisation Users	8ac3a18f2deaf8d8012deaf8e7420001	1296727440181	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deaf95c050005	ORGANISATION	organisation	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	This is a sample organisation\t	\N	1296727342160	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf99eca0027	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deafa7ae50034	8ac3a18f2deaf8d8012deaf960610007	8ac3a18f2deaf8d8012deafa8d7e004d	\N	\N	8ac3a18f2deaf8d8012deafa7a6b0031	8ac3a18f2deaf8d8012deafa8d910050	8ac3a18f2deaf8d8012deafa8da30053	8ac3a18f2deaf8d8012deafa8db40056	8ac3a18f2deaf8d8012deafa8dc60059	8ac3a18f2deaf8d8012deafa8dd7005c	8ac3a18f2deaf8d8012deafa8de8005f	8ac3a18f2deaf8d8012deaf9afb9002b	8ac3a18f2deaf8d8012deaf9ba54002e	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b460043	CLUSTERFOLDER	Clusters	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Compute Cluster Instances	8ac3a18f2deaf8d8012deaf8e7420001	1296727440198	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b570046	SERVICESFOLDER	Services	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	Workflow Services Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727440215	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deaf8d8012deafa7b690049	APPLICATIONSFOLDER	Applications	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf95c050005	External Application Definitions	8ac3a18f2deaf8d8012deaf8e7420001	1296727440233	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- TOC entry 1988 (class 0 OID 42703)
-- Dependencies: 1601
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY permissions (id, principalid, targetobjectid, type, universal) FROM stdin;
4	8ac3a18f2deaf8d8012deaf99eca0027	8ac3a18f2deaf8d8012deaf961ac001f	read	f
5	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deaf961ac001f	read	f
6	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deaf961ac001f	add	f
7	8ac3a18f2deaf8d8012deaf99eca0027	8ac3a18f2deaf8d8012deaf961ac001f	add	f
8	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deaf9614d0013	read	f
9	8ac3a18f2deaf8d8012deaf99eca0027	8ac3a18f2deaf8d8012deaf9614d0013	read	f
10	8ac3a18f2deaf8d8012deaf99eca0027	8ac3a18f2deaf8d8012deaf960c6000a	add	f
11	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deaf960c6000a	add	f
12	8ac3a18f2deaf8d8012deaf99eca0027	8ac3a18f2deaf8d8012deaf960610007	read	f
13	8ac3a18f2deaf8d8012deaf98a620023	8ac3a18f2deaf8d8012deaf960610007	read	f
\.


--
-- TOC entry 1989 (class 0 OID 42711)
-- Dependencies: 1602
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY products (id, producttype, name, description, categorylabel, ownerid, expirable, duration, autorenewing, productcost, imageurl, purchasewithpromotionaltokens, singlesubscription, objectidorname, active, initialvalue, units, value) FROM stdin;
\.


--
-- TOC entry 1990 (class 0 OID 42719)
-- Dependencies: 1603
-- Data for Name: promotialitems; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY promotialitems (id, quantity, creationtime, expirytime, promotioncode) FROM stdin;
\.


--
-- TOC entry 1991 (class 0 OID 42724)
-- Dependencies: 1604
-- Data for Name: propertygroups; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY propertygroups (id, objectproperty, objectid, name, description, organisationid) FROM stdin;
\.


--
-- TOC entry 1992 (class 0 OID 42732)
-- Dependencies: 1605
-- Data for Name: propertyitems; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY propertyitems (id, groupid, name, value) FROM stdin;
\.


--
-- TOC entry 1993 (class 0 OID 42740)
-- Dependencies: 1606
-- Data for Name: rememberedlogins; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY rememberedlogins (id, userid, cookieid, expirydate) FROM stdin;
\.


--
-- TOC entry 1994 (class 0 OID 42748)
-- Dependencies: 1607
-- Data for Name: servicehosts; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY servicehosts (id, ipaddress, active, guid) FROM stdin;
\.


--
-- TOC entry 1995 (class 0 OID 42756)
-- Dependencies: 1608
-- Data for Name: serviceinstances; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY serviceinstances (id, rmiport, running, hostid, name) FROM stdin;
\.


--
-- TOC entry 1996 (class 0 OID 42764)
-- Dependencies: 1609
-- Data for Name: servicexml; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY servicexml (id, serviceid, versionid, xmldata) FROM stdin;
\.


--
-- TOC entry 1997 (class 0 OID 42772)
-- Dependencies: 1610
-- Data for Name: subscriptions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY subscriptions (id, subscriptiontype, name, categorylabel, userid, expiry, autorenewing, productid, imageurl, starttime, value, units) FROM stdin;
\.


--
-- TOC entry 1998 (class 0 OID 42780)
-- Dependencies: 1611
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tags (id, tagtext) FROM stdin;
\.


--
-- TOC entry 1999 (class 0 OID 42788)
-- Dependencies: 1612
-- Data for Name: tagstoobjects; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tagstoobjects (id, serverobjectid, tagid, creatorid, weight, createdate) FROM stdin;
\.


--
-- TOC entry 2000 (class 0 OID 42796)
-- Dependencies: 1613
-- Data for Name: ticketgroups; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY ticketgroups (id, ticketid, groupid) FROM stdin;
\.


--
-- TOC entry 2001 (class 0 OID 42804)
-- Dependencies: 1614
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tickets (id, organisationid, userid, lastaccesstime, superticket) FROM stdin;
8ac3a18f2deaf8d8012deaf8ec250004	8ac3a18f2deaf8d8012deaf95c050005	8ac3a18f2deaf8d8012deaf8e7420001	2011-02-03 10:07:54.505	t
\.


--
-- TOC entry 2002 (class 0 OID 42812)
-- Dependencies: 1615
-- Data for Name: uploadreservations; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY uploadreservations (id, userid, documentid, signature, "timestamp", certificatehash, comments) FROM stdin;
\.


--
-- TOC entry 2003 (class 0 OID 42820)
-- Dependencies: 1616
-- Data for Name: userprofiles; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY userprofiles (id, text, website, emailaddress, defaultdomain) FROM stdin;
\.


--
-- TOC entry 2004 (class 0 OID 42828)
-- Dependencies: 1617
-- Data for Name: workflowengines; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY workflowengines (id, engineip, queuename, lastaccesstime) FROM stdin;
\.


--
-- TOC entry 2005 (class 0 OID 42836)
-- Dependencies: 1618
-- Data for Name: workflowservicelogs; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY workflowservicelogs (id, invocationid, contextid, outputtext) FROM stdin;
\.


--
-- TOC entry 1887 (class 2606 OID 42537)
-- Dependencies: 1579 1579
-- Name: accountactvity_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY accountactvity
    ADD CONSTRAINT accountactvity_pkey PRIMARY KEY (id);


--
-- TOC entry 1889 (class 2606 OID 42545)
-- Dependencies: 1580 1580
-- Name: appsubscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY appsubscriptions
    ADD CONSTRAINT appsubscriptions_pkey PRIMARY KEY (id);


--
-- TOC entry 1891 (class 2606 OID 42553)
-- Dependencies: 1581 1581
-- Name: blockhelp_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY blockhelp
    ADD CONSTRAINT blockhelp_pkey PRIMARY KEY (id);


--
-- TOC entry 1893 (class 2606 OID 42561)
-- Dependencies: 1582 1582
-- Name: blogpostread_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY blogpostread
    ADD CONSTRAINT blogpostread_pkey PRIMARY KEY (id);


--
-- TOC entry 1895 (class 2606 OID 42569)
-- Dependencies: 1583 1583
-- Name: bookcontent_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY bookcontent
    ADD CONSTRAINT bookcontent_pkey PRIMARY KEY (id);


--
-- TOC entry 1897 (class 2606 OID 42577)
-- Dependencies: 1584 1584
-- Name: clusterenvironments_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY clusterenvironments
    ADD CONSTRAINT clusterenvironments_pkey PRIMARY KEY (id);


--
-- TOC entry 1899 (class 2606 OID 42585)
-- Dependencies: 1585 1585
-- Name: clusterjobs_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY clusterjobs
    ADD CONSTRAINT clusterjobs_pkey PRIMARY KEY (id);


--
-- TOC entry 1901 (class 2606 OID 42593)
-- Dependencies: 1586 1586
-- Name: documentversions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY documentversions
    ADD CONSTRAINT documentversions_pkey PRIMARY KEY (id);


--
-- TOC entry 1903 (class 2606 OID 42601)
-- Dependencies: 1587 1587
-- Name: downloadreservations_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY downloadreservations
    ADD CONSTRAINT downloadreservations_pkey PRIMARY KEY (id);


--
-- TOC entry 1905 (class 2606 OID 42609)
-- Dependencies: 1588 1588
-- Name: externallogondetails_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY externallogondetails
    ADD CONSTRAINT externallogondetails_pkey PRIMARY KEY (id);


--
-- TOC entry 1907 (class 2606 OID 42617)
-- Dependencies: 1589 1589
-- Name: groupmembership_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY groupmembership
    ADD CONSTRAINT groupmembership_pkey PRIMARY KEY (id);


--
-- TOC entry 1909 (class 2606 OID 42622)
-- Dependencies: 1590 1590
-- Name: groupprofiles_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY groupprofiles
    ADD CONSTRAINT groupprofiles_pkey PRIMARY KEY (id);


--
-- TOC entry 1911 (class 2606 OID 42630)
-- Dependencies: 1591 1591
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- TOC entry 1913 (class 2606 OID 42638)
-- Dependencies: 1592 1592
-- Name: keys_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY keys
    ADD CONSTRAINT keys_pkey PRIMARY KEY (id);


--
-- TOC entry 1915 (class 2606 OID 42646)
-- Dependencies: 1593 1593
-- Name: logevents_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logevents
    ADD CONSTRAINT logevents_pkey PRIMARY KEY (id);


--
-- TOC entry 1917 (class 2606 OID 42654)
-- Dependencies: 1594 1594
-- Name: logeventsannonymised_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logeventsannonymised
    ADD CONSTRAINT logeventsannonymised_pkey PRIMARY KEY (id);


--
-- TOC entry 1919 (class 2606 OID 42662)
-- Dependencies: 1595 1595
-- Name: logmessageproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logmessageproperties
    ADD CONSTRAINT logmessageproperties_pkey PRIMARY KEY (id);


--
-- TOC entry 1921 (class 2606 OID 42670)
-- Dependencies: 1596 1596
-- Name: logmessages_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logmessages
    ADD CONSTRAINT logmessages_pkey PRIMARY KEY (id);


--
-- TOC entry 1923 (class 2606 OID 42678)
-- Dependencies: 1597 1597
-- Name: logondetails_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logondetails
    ADD CONSTRAINT logondetails_pkey PRIMARY KEY (id);


--
-- TOC entry 1925 (class 2606 OID 42686)
-- Dependencies: 1598 1598
-- Name: metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- TOC entry 1927 (class 2606 OID 42694)
-- Dependencies: 1599 1599
-- Name: objectlocators_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY objectlocators
    ADD CONSTRAINT objectlocators_pkey PRIMARY KEY (id);


--
-- TOC entry 1929 (class 2606 OID 42702)
-- Dependencies: 1600 1600
-- Name: objectsflat_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY objectsflat
    ADD CONSTRAINT objectsflat_pkey PRIMARY KEY (id);


--
-- TOC entry 1931 (class 2606 OID 42710)
-- Dependencies: 1601 1601
-- Name: permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 1933 (class 2606 OID 42718)
-- Dependencies: 1602 1602
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 1935 (class 2606 OID 42723)
-- Dependencies: 1603 1603
-- Name: promotialitems_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY promotialitems
    ADD CONSTRAINT promotialitems_pkey PRIMARY KEY (id);


--
-- TOC entry 1937 (class 2606 OID 42731)
-- Dependencies: 1604 1604
-- Name: propertygroups_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY propertygroups
    ADD CONSTRAINT propertygroups_pkey PRIMARY KEY (id);


--
-- TOC entry 1939 (class 2606 OID 42739)
-- Dependencies: 1605 1605
-- Name: propertyitems_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY propertyitems
    ADD CONSTRAINT propertyitems_pkey PRIMARY KEY (id);


--
-- TOC entry 1941 (class 2606 OID 42747)
-- Dependencies: 1606 1606
-- Name: rememberedlogins_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY rememberedlogins
    ADD CONSTRAINT rememberedlogins_pkey PRIMARY KEY (id);


--
-- TOC entry 1943 (class 2606 OID 42755)
-- Dependencies: 1607 1607
-- Name: servicehosts_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY servicehosts
    ADD CONSTRAINT servicehosts_pkey PRIMARY KEY (id);


--
-- TOC entry 1945 (class 2606 OID 42763)
-- Dependencies: 1608 1608
-- Name: serviceinstances_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY serviceinstances
    ADD CONSTRAINT serviceinstances_pkey PRIMARY KEY (id);


--
-- TOC entry 1947 (class 2606 OID 42771)
-- Dependencies: 1609 1609
-- Name: servicexml_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY servicexml
    ADD CONSTRAINT servicexml_pkey PRIMARY KEY (id);


--
-- TOC entry 1949 (class 2606 OID 42779)
-- Dependencies: 1610 1610
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- TOC entry 1951 (class 2606 OID 42787)
-- Dependencies: 1611 1611
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 1953 (class 2606 OID 42795)
-- Dependencies: 1612 1612
-- Name: tagstoobjects_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tagstoobjects
    ADD CONSTRAINT tagstoobjects_pkey PRIMARY KEY (id);


--
-- TOC entry 1955 (class 2606 OID 42803)
-- Dependencies: 1613 1613
-- Name: ticketgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY ticketgroups
    ADD CONSTRAINT ticketgroups_pkey PRIMARY KEY (id);


--
-- TOC entry 1957 (class 2606 OID 42811)
-- Dependencies: 1614 1614
-- Name: tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- TOC entry 1959 (class 2606 OID 42819)
-- Dependencies: 1615 1615
-- Name: uploadreservations_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY uploadreservations
    ADD CONSTRAINT uploadreservations_pkey PRIMARY KEY (id);


--
-- TOC entry 1961 (class 2606 OID 42827)
-- Dependencies: 1616 1616
-- Name: userprofiles_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY userprofiles
    ADD CONSTRAINT userprofiles_pkey PRIMARY KEY (id);


--
-- TOC entry 1963 (class 2606 OID 42835)
-- Dependencies: 1617 1617
-- Name: workflowengines_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY workflowengines
    ADD CONSTRAINT workflowengines_pkey PRIMARY KEY (id);


--
-- TOC entry 1965 (class 2606 OID 42843)
-- Dependencies: 1618 1618
-- Name: workflowservicelogs_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY workflowservicelogs
    ADD CONSTRAINT workflowservicelogs_pkey PRIMARY KEY (id);


--
-- TOC entry 2010 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: nsjw7
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nsjw7;
GRANT ALL ON SCHEMA public TO nsjw7;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2011-02-03 10:11:09 GMT

--
-- PostgreSQL database dump complete
--

