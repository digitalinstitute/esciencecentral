--
-- PostgreSQL database dump
--

-- Started on 2011-02-08 10:15:21 GMT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 2047 (class 1262 OID 32747)
-- Name: testAdmin; Type: DATABASE; Schema: -; Owner: connexiencedb
--

CREATE DATABASE "testAdmin" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_GB.UTF-8' LC_CTYPE = 'en_GB.UTF-8';


ALTER DATABASE "testAdmin" OWNER TO connexiencedb;

\connect "testAdmin"

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1607 (class 1259 OID 32748)
-- Dependencies: 6
-- Name: accountactvity; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE accountactvity (
    id bigint NOT NULL,
    accountid character varying(255),
    purchasetime bigint,
    currency character varying(255),
    cost numeric(19,2),
    type character varying(255),
    quantity integer,
    transactionid character varying(255),
    processorname character varying(255)
);


ALTER TABLE public.accountactvity OWNER TO connexiencedb;

--
-- TOC entry 1608 (class 1259 OID 32754)
-- Dependencies: 6
-- Name: appsubscriptions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE appsubscriptions (
    id bigint NOT NULL,
    userid character varying(255),
    applicationid character varying(255),
    executeworkflows boolean,
    postnews boolean,
    viewconnections boolean,
    viewfiles boolean,
    viewdetails boolean,
    uploadfiles boolean,
    modifyfiles boolean
);


ALTER TABLE public.appsubscriptions OWNER TO connexiencedb;

--
-- TOC entry 1609 (class 1259 OID 32760)
-- Dependencies: 6
-- Name: blockhelp; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE blockhelp (
    id character varying(255) NOT NULL,
    serviceid character varying(255),
    versionid character varying(255),
    htmldata text
);


ALTER TABLE public.blockhelp OWNER TO connexiencedb;

--
-- TOC entry 1610 (class 1259 OID 32766)
-- Dependencies: 6
-- Name: blogpostread; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE blogpostread (
    id character varying(255) NOT NULL,
    blogid character varying(255),
    postid character varying(255),
    userid character varying(255)
);


ALTER TABLE public.blogpostread OWNER TO connexiencedb;

--
-- TOC entry 1611 (class 1259 OID 32772)
-- Dependencies: 6
-- Name: bookcontent; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE bookcontent (
    id bigint NOT NULL,
    contenttype character varying(255) NOT NULL,
    sectionid character varying(255),
    authorid character varying(255),
    signature bytea,
    signaturetimestamp date,
    approverid character varying(255),
    approversignature bytea,
    approversignaturetimestamp date,
    reviewerid character varying(255),
    reviewersignature bytea,
    reviewersignaturetimestamp date,
    textcontent text
);


ALTER TABLE public.bookcontent OWNER TO connexiencedb;

--
-- TOC entry 1612 (class 1259 OID 32778)
-- Dependencies: 6
-- Name: clusterenvironments; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE clusterenvironments (
    id bigint NOT NULL,
    clusterid character varying(255),
    environmentfileid character varying(255),
    environmentid character varying(255)
);


ALTER TABLE public.clusterenvironments OWNER TO connexiencedb;

--
-- TOC entry 1613 (class 1259 OID 32784)
-- Dependencies: 6
-- Name: clusterjobs; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE clusterjobs (
    id bigint NOT NULL,
    userid character varying(255),
    jobid character varying(255),
    status integer,
    executionok boolean,
    submissiontime timestamp without time zone,
    executionstarttime timestamp without time zone,
    executionendtime timestamp without time zone,
    clusterid character varying(255),
    results text,
    folderid character varying(255)
);


ALTER TABLE public.clusterjobs OWNER TO connexiencedb;

--
-- TOC entry 1614 (class 1259 OID 32790)
-- Dependencies: 6
-- Name: documentversions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE documentversions (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentrecordid character varying(255),
    signaturedata bytea,
    "timestamp" timestamp without time zone,
    versionnumber integer,
    size bigint,
    comments character varying(255),
    certificatedata bytea
);


ALTER TABLE public.documentversions OWNER TO connexiencedb;

--
-- TOC entry 1615 (class 1259 OID 32796)
-- Dependencies: 6
-- Name: downloadreservations; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE downloadreservations (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentid character varying(255),
    versionid character varying(255),
    "timestamp" timestamp without time zone
);


ALTER TABLE public.downloadreservations OWNER TO connexiencedb;

--
-- TOC entry 1616 (class 1259 OID 32802)
-- Dependencies: 6
-- Name: externallogondetails; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE externallogondetails (
    id character varying(255) NOT NULL,
    userid character varying(255),
    externaluserid character varying(255)
);


ALTER TABLE public.externallogondetails OWNER TO connexiencedb;

--
-- TOC entry 1617 (class 1259 OID 32808)
-- Dependencies: 6
-- Name: groupmembership; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE groupmembership (
    id bigint NOT NULL,
    userid character varying(255),
    groupid character varying(255)
);


ALTER TABLE public.groupmembership OWNER TO connexiencedb;

--
-- TOC entry 1618 (class 1259 OID 32814)
-- Dependencies: 6
-- Name: groupprofiles; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE groupprofiles (
    id character varying(255) NOT NULL
);


ALTER TABLE public.groupprofiles OWNER TO connexiencedb;

--
-- TOC entry 1619 (class 1259 OID 32817)
-- Dependencies: 6
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: connexiencedb
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO connexiencedb;

--
-- TOC entry 2050 (class 0 OID 0)
-- Dependencies: 1619
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: connexiencedb
--

SELECT pg_catalog.setval('hibernate_sequence', 15, true);


--
-- TOC entry 1620 (class 1259 OID 32819)
-- Dependencies: 6
-- Name: images; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE images (
    id character varying(255) NOT NULL,
    serverobjectid character varying(255),
    type character varying(255),
    data bytea
);


ALTER TABLE public.images OWNER TO connexiencedb;

--
-- TOC entry 1621 (class 1259 OID 32825)
-- Dependencies: 6
-- Name: keys; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE keys (
    id character varying(255) NOT NULL,
    objectid character varying(255),
    keystoredata bytea,
    certificate bytea
);


ALTER TABLE public.keys OWNER TO connexiencedb;

--
-- TOC entry 1622 (class 1259 OID 32831)
-- Dependencies: 6
-- Name: logevents; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logevents (
    id character varying(255) NOT NULL,
    operation character varying(255) NOT NULL,
    "timestamp" bigint,
    userid character varying(255),
    principalname character varying(255),
    workflowid character varying(255),
    invocationid character varying(255),
    versionid character varying(255),
    workflowname character varying(255),
    state character varying(255),
    objectid character varying(255),
    objectname character varying(255),
    objecttype character varying(255),
    serviceid character varying(255),
    serviceversionid character varying(255),
    serviceversionnumber integer,
    servicename character varying(255),
    granteeid character varying(255),
    granteetype character varying(255),
    granteename character varying(255)
);


ALTER TABLE public.logevents OWNER TO connexiencedb;

--
-- TOC entry 1623 (class 1259 OID 32837)
-- Dependencies: 6
-- Name: logeventsannonymised; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logeventsannonymised (
    id integer NOT NULL,
    operation character varying(255) NOT NULL,
    "timestamp" bigint,
    userid integer,
    workflowid integer,
    invocationid integer,
    versionid integer,
    state character varying(255),
    objectid integer,
    objecttype character varying(255),
    granteeid integer,
    granteetype character varying(255)
);


ALTER TABLE public.logeventsannonymised OWNER TO connexiencedb;

--
-- TOC entry 1624 (class 1259 OID 32843)
-- Dependencies: 6
-- Name: logmessageproperties; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logmessageproperties (
    id bigint NOT NULL,
    logmessageid bigint,
    name character varying(255),
    value character varying(255)
);


ALTER TABLE public.logmessageproperties OWNER TO connexiencedb;

--
-- TOC entry 1625 (class 1259 OID 32849)
-- Dependencies: 6
-- Name: logmessages; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logmessages (
    id bigint NOT NULL,
    messagedate timestamp without time zone,
    messagetext character varying(255),
    objectid character varying(255),
    organisationid character varying(255),
    principalid character varying(255),
    type integer
);


ALTER TABLE public.logmessages OWNER TO connexiencedb;

--
-- TOC entry 1626 (class 1259 OID 32855)
-- Dependencies: 6
-- Name: logondetails; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE logondetails (
    id bigint NOT NULL,
    userid character varying(255),
    logonname character varying(255),
    hashedpassword character varying(255)
);


ALTER TABLE public.logondetails OWNER TO connexiencedb;

--
-- TOC entry 1627 (class 1259 OID 32861)
-- Dependencies: 6
-- Name: metadata; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE metadata (
    id bigint NOT NULL,
    objectid character varying(255),
    name character varying(255),
    value character varying(255)
);


ALTER TABLE public.metadata OWNER TO connexiencedb;

--
-- TOC entry 1628 (class 1259 OID 32867)
-- Dependencies: 6
-- Name: objectlocators; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE objectlocators (
    id bigint NOT NULL,
    objectid character varying(255),
    partnershipid character varying(255),
    organisationid character varying(255)
);


ALTER TABLE public.objectlocators OWNER TO connexiencedb;

--
-- TOC entry 1629 (class 1259 OID 32873)
-- Dependencies: 6
-- Name: objectsflat; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE objectsflat (
    id character varying(255) NOT NULL,
    objecttype character varying(255) NOT NULL,
    name character varying(255),
    organisationid character varying(255),
    containerid character varying(255),
    description text,
    creatorid character varying(255),
    timeinmillis bigint,
    jndiname character varying(255),
    storedata bytea,
    remotecertificatedata bytea,
    remoteorganisationid character varying(255),
    remotehost character varying(255),
    remoteport integer,
    allowdatafolderbrowse boolean,
    allowgrouplisting boolean,
    allowdatauploads boolean,
    allowpermissionsetting boolean,
    enabled boolean,
    tablename character varying(255),
    idcolumn character varying(255),
    datacolumn character varying(255),
    buffersize integer,
    databasevendor integer,
    directory character varying(255),
    accesskeyid character varying(255),
    accesskey character varying(255),
    organisationbucket character varying(255),
    rootuserid character varying(255),
    firstname character varying(255),
    surname character varying(255),
    defaultgroupid character varying(255),
    homefolderid character varying(255),
    profileid character varying(255),
    webfolderid character varying(255),
    workflowfolderid character varying(255),
    inboxfolderid character varying(255),
    sentmessagesfolderid character varying(255),
    externalobjectsfolderid character varying(255),
    hashkey character varying(255),
    protectedgroup boolean,
    adminapprovejoin boolean,
    nonmemberslist boolean,
    admingroupid character varying(255),
    groupfolderid character varying(255),
    userfolderid character varying(255),
    datafolderid character varying(255),
    projectsfolderid character varying(255),
    trustedfolderid character varying(255),
    datastoreid character varying(255),
    partnersfolderid character varying(255),
    documenttypesfolderid character varying(255),
    databasesfolderid character varying(255),
    clusterfolderid character varying(255),
    servicesfolderid character varying(255),
    applicationsfolderid character varying(255),
    publicgroupid character varying(255),
    defaultuserid character varying(255),
    managernode character varying(255),
    publicvisible boolean,
    partnershipid character varying(255),
    partnerfolderid character varying(255),
    enddate bigint,
    startdate bigint,
    milestone boolean,
    estimatedcompletion integer,
    parentprojectid character varying(255),
    invocationid character varying(255),
    workflowid character varying(255),
    invocationdate timestamp without time zone,
    invocationstatus integer,
    currentblockid character varying(255),
    engineid character varying(255),
    approvergroupid character varying(255),
    approversigningenforced boolean,
    reviewergroupid character varying(255),
    reviewersigningenforced boolean,
    applicationid character varying(255),
    typestring character varying(255),
    documenttypeid character varying(255),
    versioned boolean,
    maxversions integer,
    limitversions boolean,
    currentversionnumber integer,
    intermediatedatastored boolean,
    externaldatasupported boolean,
    externaldatablockname character varying(255),
    enginetype character varying(255),
    category character varying(255),
    libraryname character varying(255),
    formattype integer,
    mimetype character varying(255),
    extension character varying(255),
    homepage character varying(255),
    serviceurl character varying(255),
    serviceroutine character varying(255),
    type integer,
    propertynames bytea,
    propertytypes bytea,
    inputnames bytea,
    inputtypes bytea,
    inputmodes bytea,
    outputnames bytea,
    outputtypes bytea,
    propertydescriptions bytea,
    propertydefaults bytea,
    externallyprovisioned boolean,
    externalscripttype character varying(255),
    externalscriptid character varying(255),
    streammode character varying(255),
    title character varying(255),
    body text,
    secretkey character varying(255),
    applicationurl character varying(255),
    columnurl character varying(255),
    documentediturl character varying(255),
    mimetypelist character varying(255),
    iconurl character varying(255),
    showiniframe boolean,
    documentationurl character varying(255),
    venue character varying(255),
    sourceobjectid character varying(255),
    sinkobjectid character varying(255),
    "timestamp" timestamp without time zone,
    shortname character varying(255),
    privateurl character varying(255),
    blogid character varying(255),
    notification boolean,
    summary text,
    postid character varying(255),
    text text,
    authorname character varying(255),
    message text,
    recipientid text,
    senderid character varying(255),
    read boolean,
    threadid character varying(255),
    isread boolean,
    comment bytea,
    status integer,
    groupid character varying(255),
    label character varying(255),
    itemtype character varying(255),
    mainitemid character varying(255),
    metadataid character varying(255),
    promotionalbalance integer,
    purchasedbalance integer,
    currencylabel character varying(255),
    pagenumber integer
);


ALTER TABLE public.objectsflat OWNER TO connexiencedb;

--
-- TOC entry 1630 (class 1259 OID 32879)
-- Dependencies: 6
-- Name: permissions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE permissions (
    id bigint NOT NULL,
    principalid character varying(255),
    targetobjectid character varying(255),
    type character varying(255),
    universal boolean
);


ALTER TABLE public.permissions OWNER TO connexiencedb;

--
-- TOC entry 1631 (class 1259 OID 32885)
-- Dependencies: 6
-- Name: products; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE products (
    id character varying(255) NOT NULL,
    producttype character varying(255) NOT NULL,
    name character varying(255),
    description character varying(255),
    categorylabel character varying(255),
    ownerid character varying(255),
    expirable boolean,
    duration integer,
    autorenewing boolean,
    productcost integer,
    imageurl character varying(255),
    purchasewithpromotionaltokens boolean,
    singlesubscription boolean,
    objectidorname character varying(255),
    active boolean,
    initialvalue integer,
    units character varying(255),
    value integer
);


ALTER TABLE public.products OWNER TO connexiencedb;

--
-- TOC entry 1632 (class 1259 OID 32891)
-- Dependencies: 6
-- Name: promotialitems; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE promotialitems (
    id bigint NOT NULL,
    quantity integer,
    creationtime bigint,
    expirytime bigint,
    promotioncode character varying(255)
);


ALTER TABLE public.promotialitems OWNER TO connexiencedb;

--
-- TOC entry 1633 (class 1259 OID 32894)
-- Dependencies: 6
-- Name: propertygroups; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE propertygroups (
    id bigint NOT NULL,
    objectproperty boolean,
    objectid character varying(255),
    name character varying(255),
    description character varying(255),
    organisationid character varying(255)
);


ALTER TABLE public.propertygroups OWNER TO connexiencedb;

--
-- TOC entry 1634 (class 1259 OID 32900)
-- Dependencies: 6
-- Name: propertyitems; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE propertyitems (
    id bigint NOT NULL,
    groupid bigint,
    name character varying(255),
    value text
);


ALTER TABLE public.propertyitems OWNER TO connexiencedb;

--
-- TOC entry 1635 (class 1259 OID 32906)
-- Dependencies: 6
-- Name: rememberedlogins; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE rememberedlogins (
    id character varying(255) NOT NULL,
    userid character varying(255),
    cookieid character varying(255),
    expirydate timestamp without time zone
);


ALTER TABLE public.rememberedlogins OWNER TO connexiencedb;

--
-- TOC entry 1636 (class 1259 OID 32912)
-- Dependencies: 6
-- Name: servicehosts; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE servicehosts (
    id bigint NOT NULL,
    ipaddress character varying(255),
    active boolean,
    guid character varying(255)
);


ALTER TABLE public.servicehosts OWNER TO connexiencedb;

--
-- TOC entry 1637 (class 1259 OID 32918)
-- Dependencies: 6
-- Name: serviceinstances; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE serviceinstances (
    id bigint NOT NULL,
    rmiport integer,
    running boolean,
    hostid character varying(255),
    name character varying(255)
);


ALTER TABLE public.serviceinstances OWNER TO connexiencedb;

--
-- TOC entry 1638 (class 1259 OID 32924)
-- Dependencies: 6
-- Name: servicexml; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE servicexml (
    id character varying(255) NOT NULL,
    serviceid character varying(255),
    versionid character varying(255),
    xmldata text
);


ALTER TABLE public.servicexml OWNER TO connexiencedb;

--
-- TOC entry 1639 (class 1259 OID 32930)
-- Dependencies: 6
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE subscriptions (
    id bigint NOT NULL,
    subscriptiontype character varying(255) NOT NULL,
    name character varying(255),
    categorylabel character varying(255),
    userid character varying(255),
    expiry bigint,
    autorenewing boolean,
    productid character varying(255),
    imageurl character varying(255),
    starttime bigint,
    value integer,
    units character varying(255)
);


ALTER TABLE public.subscriptions OWNER TO connexiencedb;

--
-- TOC entry 1640 (class 1259 OID 32936)
-- Dependencies: 6
-- Name: tags; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tags (
    id character varying(255) NOT NULL,
    tagtext text
);


ALTER TABLE public.tags OWNER TO connexiencedb;

--
-- TOC entry 1641 (class 1259 OID 32942)
-- Dependencies: 6
-- Name: tagstoobjects; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tagstoobjects (
    id character varying(255) NOT NULL,
    serverobjectid text,
    tagid text,
    creatorid character varying(255),
    weight integer,
    createdate timestamp without time zone
);


ALTER TABLE public.tagstoobjects OWNER TO connexiencedb;

--
-- TOC entry 1642 (class 1259 OID 32948)
-- Dependencies: 6
-- Name: ticketgroups; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE ticketgroups (
    id bigint NOT NULL,
    ticketid character varying(255),
    groupid character varying(255)
);


ALTER TABLE public.ticketgroups OWNER TO connexiencedb;

--
-- TOC entry 1643 (class 1259 OID 32954)
-- Dependencies: 6
-- Name: tickets; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE tickets (
    id character varying(255) NOT NULL,
    organisationid character varying(255),
    userid character varying(255),
    lastaccesstime timestamp without time zone,
    superticket boolean
);


ALTER TABLE public.tickets OWNER TO connexiencedb;

--
-- TOC entry 1644 (class 1259 OID 32960)
-- Dependencies: 6
-- Name: uploadreservations; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE uploadreservations (
    id character varying(255) NOT NULL,
    userid character varying(255),
    documentid character varying(255),
    signature bytea,
    "timestamp" timestamp without time zone,
    certificatehash bytea,
    comments character varying(255)
);


ALTER TABLE public.uploadreservations OWNER TO connexiencedb;

--
-- TOC entry 1645 (class 1259 OID 32966)
-- Dependencies: 6
-- Name: userprofiles; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE userprofiles (
    id character varying(255) NOT NULL,
    text text,
    website character varying(255),
    emailaddress character varying(255),
    defaultdomain character varying(255)
);


ALTER TABLE public.userprofiles OWNER TO connexiencedb;

--
-- TOC entry 1646 (class 1259 OID 32972)
-- Dependencies: 6
-- Name: workflowengines; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE workflowengines (
    id character varying(255) NOT NULL,
    engineip character varying(255),
    queuename character varying(255),
    lastaccesstime bigint
);


ALTER TABLE public.workflowengines OWNER TO connexiencedb;

--
-- TOC entry 1647 (class 1259 OID 32978)
-- Dependencies: 6
-- Name: workflowservicelogs; Type: TABLE; Schema: public; Owner: connexiencedb; Tablespace: 
--

CREATE TABLE workflowservicelogs (
    id bigint NOT NULL,
    invocationid character varying(255),
    contextid character varying(255),
    outputtext text
);


ALTER TABLE public.workflowservicelogs OWNER TO connexiencedb;

--
-- TOC entry 2005 (class 0 OID 32748)
-- Dependencies: 1607
-- Data for Name: accountactvity; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY accountactvity (id, accountid, purchasetime, currency, cost, type, quantity, transactionid, processorname) FROM stdin;
\.


--
-- TOC entry 2006 (class 0 OID 32754)
-- Dependencies: 1608
-- Data for Name: appsubscriptions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY appsubscriptions (id, userid, applicationid, executeworkflows, postnews, viewconnections, viewfiles, viewdetails, uploadfiles, modifyfiles) FROM stdin;
\.


--
-- TOC entry 2007 (class 0 OID 32760)
-- Dependencies: 1609
-- Data for Name: blockhelp; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY blockhelp (id, serviceid, versionid, htmldata) FROM stdin;
\.


--
-- TOC entry 2008 (class 0 OID 32766)
-- Dependencies: 1610
-- Data for Name: blogpostread; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY blogpostread (id, blogid, postid, userid) FROM stdin;
\.


--
-- TOC entry 2009 (class 0 OID 32772)
-- Dependencies: 1611
-- Data for Name: bookcontent; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY bookcontent (id, contenttype, sectionid, authorid, signature, signaturetimestamp, approverid, approversignature, approversignaturetimestamp, reviewerid, reviewersignature, reviewersignaturetimestamp, textcontent) FROM stdin;
\.


--
-- TOC entry 2010 (class 0 OID 32778)
-- Dependencies: 1612
-- Data for Name: clusterenvironments; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY clusterenvironments (id, clusterid, environmentfileid, environmentid) FROM stdin;
\.


--
-- TOC entry 2011 (class 0 OID 32784)
-- Dependencies: 1613
-- Data for Name: clusterjobs; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY clusterjobs (id, userid, jobid, status, executionok, submissiontime, executionstarttime, executionendtime, clusterid, results, folderid) FROM stdin;
\.


--
-- TOC entry 2012 (class 0 OID 32790)
-- Dependencies: 1614
-- Data for Name: documentversions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY documentversions (id, userid, documentrecordid, signaturedata, "timestamp", versionnumber, size, comments, certificatedata) FROM stdin;
\.


--
-- TOC entry 2013 (class 0 OID 32796)
-- Dependencies: 1615
-- Data for Name: downloadreservations; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY downloadreservations (id, userid, documentid, versionid, "timestamp") FROM stdin;
\.


--
-- TOC entry 2014 (class 0 OID 32802)
-- Dependencies: 1616
-- Data for Name: externallogondetails; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY externallogondetails (id, userid, externaluserid) FROM stdin;
\.


--
-- TOC entry 2015 (class 0 OID 32808)
-- Dependencies: 1617
-- Data for Name: groupmembership; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY groupmembership (id, userid, groupid) FROM stdin;
2	8ac3a18f2deb6a85012deb6b467e002e	8ac3a18f2deb6a85012deb6b4180002b
3	8ac3a18f2deb6a85012deb6d3317003f	8ac3a18f2deb6a85012deb6d30cc003c
4	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6d30cc003c
13	8a8081892e04badb012e04bd6a850002	8ac3a18f2deb6a85012deb6d2b260038
15	8a8081892e04badb012e04bd6a850002	8ac3a18f2deb6a85012deb6cc2f90033
\.


--
-- TOC entry 2016 (class 0 OID 32814)
-- Dependencies: 1618
-- Data for Name: groupprofiles; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY groupprofiles (id) FROM stdin;
8ac3a18f2deb6a85012deb6b15220022
8ac3a18f2deb6a85012deb6b2e310026
8ac3a18f2deb6a85012deb6b417c002a
8ac3a18f2deb6a85012deb6cc2f60032
8ac3a18f2deb6a85012deb6d2b230037
8ac3a18f2deb6a85012deb6d30c7003b
8a8081892e04badb012e04be22b40018
\.


--
-- TOC entry 2017 (class 0 OID 32819)
-- Dependencies: 1620
-- Data for Name: images; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY images (id, serverobjectid, type, data) FROM stdin;
\.


--
-- TOC entry 2018 (class 0 OID 32825)
-- Dependencies: 1621
-- Data for Name: keys; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY keys (id, objectid, keystoredata, certificate) FROM stdin;
8ac3a18f2deb6a85012deb6a98210003	8ac3a18f2deb6a85012deb6a93980002	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\353j\\226\\007\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\257?f\\3715\\024\\271s?\\341qC\\305\\356\\375A{t~L\\334\\331;\\004\\274\\350\\271\\234\\334\\253\\230\\271\\310\\036\\204\\023\\264 8\\335\\331\\177\\010\\013\\002\\343\\366\\246\\245t\\213J\\224\\\\\\243\\352\\250\\002\\265|Q\\366\\340\\035\\336\\366\\235/\\237u\\302<l!\\010\\317O[?\\025\\212\\201\\334E\\203e\\241\\332\\307\\355\\367\\036*"e5\\375\\276\\177:l\\355\\020\\017\\224\\242SF(\\277\\001\\336\\332_y\\012|\\366\\342\\360"]\\225\\236\\334\\2467\\2606\\033\\300\\2570\\0142\\007\\012\\253\\304\\221\\203\\233\\0079\\032\\333\\324O35\\326\\342\\223\\302\\0367\\355\\203\\023\\312}\\2537\\230b\\216 ;\\273$\\246xN!\\035\\315\\255e8\\362\\221,7\\030\\335\\370\\372-\\247\\272\\277\\030H\\310\\366\\346\\241\\027#o\\262g\\005\\0050\\230j\\276n\\330\\341\\342\\356\\223Hxu\\255h\\257}8\\351i)[u\\227\\015\\374\\313\\204'\\243\\312\\022\\354gi\\300\\364\\003##\\355\\343\\265\\271K\\216F\\305\\240\\350\\233\\343<\\220\\240k/*\\270\\300\\270\\366|\\321\\021\\3479\\260)\\274NN\\0277\\300S\\011C\\336\\013\\346\\031\\212\\337\\0050\\301z]S\\216\\314sE\\241\\222\\351\\227+\\217\\357\\370E\\317\\377c\\031^8=\\240\\271\\005\\345\\257\\361\\210\\032\\241\\275\\223\\211\\371\\200\\206\\315\\\\\\022\\251H\\304\\341\\017`\\201\\366\\313\\314\\236\\305$\\310g\\321\\345\\201\\263ye\\031sJ\\236\\364>\\246vD1\\355\\360`N\\235\\026\\336\\321n\\364\\270\\022\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3610\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232C0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6a939800020\\036\\027\\015110203120627Z\\027\\015210131120627Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6a939800020\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200c{{\\241\\032\\243.\\232\\316\\250z\\320\\334)\\267\\260\\303C[\\025A\\220\\3176v\\301h~=\\272\\223,1;\\332\\273\\364\\230\\250\\017+\\255\\227\\346"&N\\013\\220r)\\376\\305\\007IB\\025!\\303\\243D\\003\\017$Y\\023\\226\\272&\\245\\275\\302\\202\\026\\306\\010\\304\\327[\\355\\330\\242\\3564&4\\334\\371\\014\\003\\335\\353\\203\\331h\\204\\264\\324"\\346\\217w\\360U\\010\\267\\336\\322\\216\\372"(\\350Bk\\236d\\015z$\\022\\334\\210w$*\\257\\2110\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024\\011\\366`\\223.\\002=P\\332\\356\\366\\300\\200cy\\271}\\243\\235\\335\\002\\024\\023 \\351\\036\\007\\334\\301\\263\\226\\0037s\\371\\235\\275\\2246\\270\\250p\\236\\016\\312\\374\\006\\006\\210\\310\\341\\312S\\2151\\342bB`\\226\\247\\324	0\\202\\002\\3550\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232C0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6a939800020\\036\\027\\015110203120627Z\\027\\015210131120627Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6a939800020\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200c{{\\241\\032\\243.\\232\\316\\250z\\320\\334)\\267\\260\\303C[\\025A\\220\\3176v\\301h~=\\272\\223,1;\\332\\273\\364\\230\\250\\017+\\255\\227\\346"&N\\013\\220r)\\376\\305\\007IB\\025!\\303\\243D\\003\\017$Y\\023\\226\\272&\\245\\275\\302\\202\\026\\306\\010\\304\\327[\\355\\330\\242\\3564&4\\334\\371\\014\\003\\335\\353\\203\\331h\\204\\264\\324"\\346\\217w\\360U\\010\\267\\336\\322\\216\\372"(\\350Bk\\236d\\015z$\\022\\334\\210w$*\\257\\2110\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024\\011\\366`\\223.\\002=P\\332\\356\\366\\300\\200cy\\271}\\243\\235\\335\\002\\024\\023 \\351\\036\\007\\334\\301\\263\\226\\0037s\\371\\235\\275\\2246\\270\\250p
8ac3a18f2deb6a85012deb6af9a20006	8ac3a18f2deb6a85012deb6af58e0005	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\353j\\367\\213\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001wIZ\\275EF\\2072\\256\\316\\325\\214\\216=\\260\\321\\363\\3433\\314jj\\256\\370\\302\\206kK\\035{\\267=\\377r\\230\\317)\\244\\032\\206}\\003\\243\\236\\372\\237Qt\\313\\012y\\033\\377\\377\\264\\231\\376\\027|C\\325\\215\\207\\261\\005\\365h\\325\\301\\325\\017\\014E\\347^\\202\\342\\347^Ro\\266\\327\\230\\271,S\\204`i\\331\\004m}}\\354\\226zM\\225Gsg\\320\\261\\350\\247\\3259Q|\\225\\317\\354\\375\\377\\322\\253\\000D\\027\\2244\\373\\0022\\300\\324\\243\\323\\335Ev\\321\\222`\\3042\\337C\\344D\\2701\\235si\\361\\316\\034 "\\030\\372\\303\\225bi1\\025\\367]Q\\266\\274&2\\352\\2365\\222\\322K\\257NoT*$A\\312<\\210\\352\\356\\376\\217-)\\302w\\0347i\\305\\374\\327\\257E\\377\\031\\322fI\\361\\322pY\\273\\351\\361\\214\\3701\\241\\320`\\012\\012/\\203\\016>\\252\\0343\\336zlw\\255\\330Z\\377\\264z\\270t6\\027\\241\\244+\\267\\026\\023\\033`y\\251)[B_9m\\347\\364v\\006\\237a\\003\\233\\363\\003\\227\\011\\233O\\364\\303\\252\\356\\310\\335v\\024\\030\\316~\\244y\\205D4\\305}\\333\\3036\\226B\\333\\000\\254\\273b\\225\\210\\251\\006\\232\\001/\\014\\001klU\\254\\274\\223&i\\357\\201\\327\\336\\337\\207\\266o\\316&\\253WI\\310\\0022\\032\\275\\310 \\255_\\333a+\\035)\\035\\264\\255\\255\\323\\227\\310\\010\\216f\\210!$\\331V\\033\\024\\332yqL\\212\\357\\321\\371$tR\\036\\353s\\232\\027\\012\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3620\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232\\\\0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6af58e00050\\036\\027\\015110203120652Z\\027\\015210131120652Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6af58e00050\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200\\037\\303\\254]0\\370\\366\\341EjF\\2077H\\304\\025\\317\\\\q\\263'\\354\\226\\326\\322!\\250#\\346\\322W\\262z\\341\\246Y\\206\\264\\200~\\301}_\\301\\232zH\\021\\213\\352\\311{k\\352\\337\\260K%\\304X\\220M\\255kZ62\\247\\3627P2\\352\\270\\300\\306\\210\\026{\\254\\351\\016gWu\\226\\037j\\217\\260\\3274\\263\\351\\217{\\343\\311NQ\\230\\377\\224^]Rt\\222\\026\\232>\\257\\237\\035\\021\\234\\343\\331\\3517@\\370\\323Xk\\013\\345\\2430\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\0241\\311\\232.\\301}>S\\232s'L\\320'\\015\\364\\231_H\\272\\002\\025\\000\\203Q@\\022E\\344[,,-\\207,\\223\\023\\216Gv\\004\\365\\310\\251\\245\\206\\236\\252\\365\\211\\370Z\\003)\\310\\344\\021^\\343\\242\\343S\\351	0\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232\\\\0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6af58e00050\\036\\027\\015110203120652Z\\027\\015210131120652Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6af58e00050\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200\\037\\303\\254]0\\370\\366\\341EjF\\2077H\\304\\025\\317\\\\q\\263'\\354\\226\\326\\322!\\250#\\346\\322W\\262z\\341\\246Y\\206\\264\\200~\\301}_\\301\\232zH\\021\\213\\352\\311{k\\352\\337\\260K%\\304X\\220M\\255kZ62\\247\\3627P2\\352\\270\\300\\306\\210\\026{\\254\\351\\016gWu\\226\\037j\\217\\260\\3274\\263\\351\\217{\\343\\311NQ\\230\\377\\224^]Rt\\222\\026\\232>\\257\\237\\035\\021\\234\\343\\331\\3517@\\370\\323Xk\\013\\345\\2430\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\0241\\311\\232.\\301}>S\\232s'L\\320'\\015\\364\\231_H\\272\\002\\025\\000\\203Q@\\022E\\344[,,-\\207,\\223\\023\\216Gv\\004\\365\\310
8ac3a18f2deb6a85012deb6b4a8f0030	8ac3a18f2deb6a85012deb6b467e002e	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\353kHr\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\2008\\3352\\217\\234\\227\\231\\331?"\\273\\311\\261\\031\\272\\342<%\\262\\305D\\237\\207h\\023Y\\203\\261\\310\\034\\3605\\232V\\025\\214\\235\\367P\\270\\276\\304u\\222T\\352A+\\250\\207\\354AW\\357\\340\\024L\\223\\2765\\335\\375l\\013J\\3771\\235R\\013C\\253\\225I\\251\\213\\312lp\\363\\314\\306\\031\\324\\323\\273\\247$f\\241\\276\\313\\274ou\\277\\300\\250\\357\\032\\343\\262\\221d\\307\\207 KR!\\032G\\230\\327\\304\\220`\\013_\\342\\251\\3224\\360"\\037\\200I\\257\\230\\007Q\\353w\\302y\\222\\022\\213\\374A\\322%\\014[\\030\\223\\217\\325D\\321\\022@{\\352;\\002\\206\\034\\242\\332\\022K9\\276\\313\\275\\201Q\\331pj\\254y\\255\\004\\227\\314\\210\\314\\357t<\\364P\\014\\216\\214\\315h\\237\\371\\260\\224\\2359\\024\\227$\\240\\242Y*\\310\\203Ah\\3729=M\\207o\\340\\223]\\030\\315\\336jk\\275\\276g\\275\\357\\277\\315\\360\\247\\362N\\271\\2215r\\267'\\014I;\\3629\\253\\317\\303\\247R\\363\\243(\\036\\025\\213\\016\\241\\232\\202\\3643\\030\\350\\351\\336\\001\\324\\262\\006\\377\\327\\241\\026\\017\\313>\\011)\\3004n\\016\\337zy\\364\\365\\327\\214\\327Q\\230\\363I\\335\\254\\227Y\\025\\344s\\275\\022\\311C\\206\\221rd1\\233\\320`ssJ?\\241o\\325\\307O\\330U\\024\\356\\331Q\\243\\354/\\240\\213\\224\\344P{\\363M\\343=\\313\\315\\3224\\342\\3349BL\\032i|\\265i`\\240Te\\014\\236\\323\\233k\\206\\027\\374\\252\\263f\\242F\\223\\030@\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3620\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232p0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6b467e002e0\\036\\027\\015110203120712Z\\027\\015210131120712Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6b467e002e0\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200k\\313\\300I+e\\360C\\336\\2431\\234@iD\\014{\\216\\310\\344\\260\\024\\347\\323]\\342S\\255\\232jR\\357(\\315\\322\\376\\373\\236j\\247$\\013\\304\\004l\\327\\245\\030\\202\\244\\235\\212\\304\\276n\\021\\277%j%\\036\\227l\\352T\\344\\360\\221\\222\\010E\\367\\010\\303\\343\\264\\371\\256\\013\\225\\213\\033j\\353X\\272\\225\\011\\017j\\356\\265\\002]Xv/\\300\\325\\230[%\\226\\030\\344\\001\\010\\356\\371\\013\\226R\\220\\234\\035\\322^d\\333\\207?\\325\\346\\033=@H\\3640\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\024i\\247g\\034\\031\\334\\036\\006H\\251q\\240\\\\\\340\\262\\3370\\013W\\241\\002\\025\\000\\202r\\260\\310V\\005\\251?\\267o\\274^\\360\\315JHd\\362\\220\\215\\036\\3362A\\243c\\361}\\20196\\362e\\2246\\323\\302\\344\\232\\272	0\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232p0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6b467e002e0\\036\\027\\015110203120712Z\\027\\015210131120712Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6b467e002e0\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200k\\313\\300I+e\\360C\\336\\2431\\234@iD\\014{\\216\\310\\344\\260\\024\\347\\323]\\342S\\255\\232jR\\357(\\315\\322\\376\\373\\236j\\247$\\013\\304\\004l\\327\\245\\030\\202\\244\\235\\212\\304\\276n\\021\\277%j%\\036\\227l\\352T\\344\\360\\221\\222\\010E\\367\\010\\303\\343\\264\\371\\256\\013\\225\\213\\033j\\353X\\272\\225\\011\\017j\\356\\265\\002]Xv/\\300\\325\\230[%\\226\\030\\344\\001\\010\\356\\371\\013\\226R\\220\\234\\035\\322^d\\333\\207?\\325\\346\\033=@H\\3640\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\024i\\247g\\034\\031\\334\\036\\006H\\251q\\240\\\\\\340\\262\\3370\\013W\\241\\002\\025\\000\\202r\\260\\310V\\005\\251?\\267o\\274^\\360\\315JHd\\362\\220\\215
8ac3a18f2deb6a85012deb6d37900041	8ac3a18f2deb6a85012deb6d3317003f	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\353m5s\\000\\000\\001\\2160\\202\\001\\2120\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001v\\356\\035\\2605K\\037\\351mbVR\\375\\373\\257%h\\276PSG\\221\\251\\027\\337s\\345\\301m\\225\\30217\\357<\\334\\037\\374=\\210\\364\\011\\325|\\226\\031}\\012.\\256\\232:\\017\\036\\013\\300\\012\\314\\004\\027!\\027j8\\360\\015W\\312Il~84-\\342\\374\\274\\3700\\357\\265\\\\\\207\\032\\013\\015dZ\\373\\\\E\\300\\313aa\\015\\206\\277\\031@\\002G\\020\\0364T\\004Zk\\273\\251\\346f\\230y\\366\\355\\332{\\010m\\023\\2506\\267\\347\\002\\221\\275=\\272\\024\\261\\010\\313p\\253\\000\\274\\367Mn\\030\\351=\\210\\365\\007\\232\\000\\360W\\221\\231C\\363\\214\\022\\375\\242\\337\\270\\011\\215\\361\\356*%\\301\\350r\\211-{\\370\\204x\\3142\\304\\241\\000,\\327\\330\\322^\\350nE\\341\\022\\346@\\335\\345\\203\\260\\212,\\202}\\342\\025\\231\\032E\\031\\232\\024\\223\\341\\003Z\\276\\361S\\227\\222\\203Y,aw\\2425\\267R"\\026\\336,KV\\346\\326\\037+`O&Z\\234x-\\306{P\\037\\325rA\\277\\327\\226#\\314\\271$rA@'\\211\\341\\225\\354`\\006\\367s\\330 \\223\\226!xE\\243\\240\\315\\3376^SO\\005n|L\\353\\244\\305\\374\\034\\271>S\\276Y\\3171\\2351\\001\\204\\022N{[\\303\\272\\315o\\365_K\\017\\342\\367\\027:\\221@HD\\031\\272N\\266\\233\\015\\3060\\2706\\342Q\\233[`\\3424\\364}\\301I\\016\\227\\014\\264e\\230\\207)~\\253\\012F\\256\\224T\\030\\241\\006\\2631Ar\\021\\336\\243?\\030\\331\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3620\\202\\002\\3560\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MJ\\232\\3560\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d3317003f0\\036\\027\\015110203120918Z\\027\\015210131120918Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d3317003f0\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\367c\\030=\\247#\\214Kt,\\300\\207\\242\\202\\026M\\346wq\\353\\254\\265\\020E\\037\\373\\020\\320\\017u)3bM\\203\\237\\035K s~W\\352\\212\\221\\367t\\343K\\232\\337X\\223\\211j\\320;\\212j\\345/\\010Eq\\017\\245\\342\\247is\\337\\314Q\\021\\260)\\254R.J\\0158\\020\\321\\227\\326\\305\\302ms\\253U\\363E\\000\\271\\323\\263\\317\\253K](\\250Q\\026O\\325\\325\\304\\217\\305\\351[\\012\\351\\201zf\\216~\\034Rm\\332\\244dQ0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024o\\254\\256!fY\\214\\315y\\370\\324J\\276\\026\\347\\333x\\016\\342\\021\\002\\024C\\006I*\\361\\331\\273F'\\242\\262\\020\\375\\020\\330\\376\\004\\331\\316\\232\\356\\332\\255\\356\\241\\255V\\362>b\\371\\032\\220P:\\272\\317}p\\272	0\\202\\002\\3560\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MJ\\232\\3560\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d3317003f0\\036\\027\\015110203120918Z\\027\\015210131120918Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d3317003f0\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\367c\\030=\\247#\\214Kt,\\300\\207\\242\\202\\026M\\346wq\\353\\254\\265\\020E\\037\\373\\020\\320\\017u)3bM\\203\\237\\035K s~W\\352\\212\\221\\367t\\343K\\232\\337X\\223\\211j\\320;\\212j\\345/\\010Eq\\017\\245\\342\\247is\\337\\314Q\\021\\260)\\254R.J\\0158\\020\\321\\227\\326\\305\\302ms\\253U\\363E\\000\\271\\323\\263\\317\\253K](\\250Q\\026O\\325\\325\\304\\217\\305\\351[\\012\\351\\201zf\\216~\\034Rm\\332\\244dQ0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024o\\254\\256!fY\\214\\315y\\370\\324J\\276\\026\\347\\333x\\016\\342\\021\\002\\024C\\006I*\\361\\331\\273F'\\242\\262\\020\\375\\020\\330\\376\\004\\331\\316\\232
8ac3a18f2deb6a85012deb6d3cd70044	8ac3a18f2deb6a85012deb6d38920042	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001-\\353m:\\225\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\237\\341\\276\\203\\231\\016\\345\\307\\273\\315*/\\307\\241B\\262\\311\\263q\\224\\003\\230\\233pP\\264\\251w% \\317+Y\\256\\204\\\\\\036\\367\\253\\371"\\340P\\216\\274n\\036\\0144b\\234\\032\\242jw\\273\\260W\\200\\376>\\2139\\341\\024+\\017\\205\\374\\247\\264\\005\\266\\213\\311\\034\\350\\033N\\0254\\237\\036\\366w~a\\261vr79\\305.\\314]gd\\263\\254\\021(m\\227R?\\222\\266-\\343]]\\177\\240\\204\\213]n\\005;\\010\\343=(:2\\307b\\033#\\367^D9\\323\\345F\\345'\\030q\\233\\260\\344\\227\\224wa?\\341\\211U\\035H6\\210S}"\\352\\217L\\325\\313R\\331C\\322\\233\\323\\024\\012\\230\\350\\001#\\017\\266YjDs\\2352tL\\000JU\\354U\\267\\253%\\252\\015%u\\373\\020\\210\\274\\243NlL\\325\\332\\017\\255rKv\\340K\\215\\357\\300\\013% \\222^\\213Z!\\334\\242\\237\\343\\322\\034P\\301\\020B~v\\261\\375\\351\\207\\202\\007\\027\\035\\333!\\355\\313\\261\\312Uk\\220\\300\\020\\371\\312\\344\\223X\\004\\214h\\342\\304\\246\\375\\203= \\230xFu*\\223@T\\031\\330\\314\\007\\233T\\360\\274\\227g}jM\\273p\\016\\325G\\236S\\004\\025\\211\\265\\034Y\\224l\\366\\227\\355D"\\327\\242_\\315Y\\244\\023,\\337\\253\\347s\\000\\326\\261\\271e\\243\\000)d\\341\\2459\\370hj/\\014\\364\\002[{\\001y\\015\\270e\\262K\\250'\\247\\026\\204\\333,;v/e\\331\\342\\302\\275\\261[\\223\\213\\326;\\221\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3620\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232\\3600\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d389200420\\036\\027\\015110203120920Z\\027\\015210131120920Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d389200420\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200t\\353\\260c(*\\253d\\323\\234L\\021Rr\\371\\211\\340S\\317\\247'/\\336\\264\\252(\\010\\240\\275,\\205\\352Km$\\026\\303g\\004\\336\\211\\242\\303\\275\\020N\\0010\\341\\365\\324V\\020\\234\\253N\\030\\207\\313\\205\\036\\2440-\\273y#\\374/\\254\\324\\217a\\236\\302^\\023?\\215\\257\\230\\237a\\252k8G`\\327\\354`4\\356\\224\\002\\222\\206!\\003\\372\\367\\236>\\300u\\227O\\364\\263\\002\\340a\\210\\200\\0307\\231\\366\\331\\035\\353\\371j\\360`\\234e$0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\024\\010OYLP\\347i\\005\\355@\\346zr\\266\\3410\\374X\\337\\011\\002\\025\\000\\211\\014\\304\\312\\346,\\234\\252\\233\\034\\352[)\\272\\301\\311/J\\323\\217\\362\\376|f-\\313\\220\\356\\220Sem\\327\\225\\320\\2348\\351\\261\\362	0\\202\\002\\3560\\202\\002\\253\\240\\003\\002\\001\\002\\002\\004MJ\\232\\3600\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d389200420\\036\\027\\015110203120920Z\\027\\015210131120920Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8ac3a18f2deb6a85012deb6d389200420\\202\\001\\2670\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\204\\000\\002\\201\\200t\\353\\260c(*\\253d\\323\\234L\\021Rr\\371\\211\\340S\\317\\247'/\\336\\264\\252(\\010\\240\\275,\\205\\352Km$\\026\\303g\\004\\336\\211\\242\\303\\275\\020N\\0010\\341\\365\\324V\\020\\234\\253N\\030\\207\\313\\205\\036\\2440-\\273y#\\374/\\254\\324\\217a\\236\\302^\\023?\\215\\257\\230\\237a\\252k8G`\\327\\354`4\\356\\224\\002\\222\\206!\\003\\372\\367\\236>\\300u\\227O\\364\\263\\002\\340a\\210\\200\\0307\\231\\366\\331\\035\\353\\371j\\360`\\234e$0\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\0030\\0000-\\002\\024\\010OYLP\\347i\\005\\355@\\346zr\\266\\3410\\374X\\337\\011\\002\\025\\000\\211\\014\\304\\312\\346,\\234\\252\\233\\034\\352[)\\272\\301\\311/J\\323\\217
8a8081892e04badb012e04bd6fc70004	8a8081892e04badb012e04bd6a850002	\\376\\355\\376\\355\\000\\000\\000\\002\\000\\000\\000\\001\\000\\000\\000\\001\\000\\005mykey\\000\\000\\001.\\004\\275l\\257\\000\\000\\001\\2170\\202\\001\\2130\\016\\006\\012+\\006\\001\\004\\001*\\002\\021\\001\\001\\005\\000\\004\\202\\001w\\337u\\257\\027\\011Vr\\313|\\007\\317\\233\\235\\026\\237;\\311Fb\\360]v\\361\\000\\223\\327-%\\364\\350\\371E}C\\322Dy\\376\\237\\305=\\206\\225\\320\\032S\\263\\305\\305\\223\\026\\2063k\\270\\370\\3073\\370ug\\303O\\304\\343hjx\\333\\201(\\3457\\362$\\213Q\\201 \\340\\330[#\\000\\342\\005\\\\\\001\\203\\257\\372\\201\\240~z\\214"H\\333\\0230\\034\\012\\253\\327R\\217\\006\\320\\333\\377\\361HH\\010\\011\\021Q\\254\\034"\\225\\211\\021\\003\\306E\\227\\264\\202\\3353\\005[>x:L\\025\\006\\355,\\245\\246n\\252jf\\326\\222\\347\\273\\251E\\346\\245\\235\\313\\215:<\\0019\\247\\324\\300\\240x\\353\\337\\246\\215\\004\\207bz\\012\\236\\177\\205:\\304\\223\\264\\272\\005F\\014qy\\247\\016w\\276\\177\\242\\224\\271\\200\\313\\225$\\255\\302E_\\003i\\364\\005\\031xf\\242?\\305!!\\027\\331\\306\\033e\\007f\\244%\\037\\030\\000\\211\\370X\\357SG\\205y\\3316$\\035ro\\320iv\\004\\376\\210V\\311\\335\\011)`\\351\\255P\\377h\\216\\301Gn\\216\\275\\264\\231?V&\\327p\\326\\015\\250D\\226\\005\\204\\210\\375p\\220{5#\\245$\\346\\232\\026~\\365*\\315\\367x+\\344\\270\\331_\\012\\275\\325\\314\\363Dp\\001 \\213\\034\\306Q\\020[\\264\\246\\267$\\346\\022\\257\\274\\213\\22262\\012\\011\\024\\311\\341p\\330I\\312\\265%Z^\\304uE\\247\\214\\215\\264}\\347HO\\227`2K\\317\\010V\\376\\224\\034<\\350\\314\\240v\\207t\\333\\356\\340\\000\\000\\000\\001\\000\\005X.509\\000\\000\\002\\3620\\202\\002\\3560\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MQ\\025\\3360\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8a8081892e04badb012e04bd6a8500020\\036\\027\\015110208100726Z\\027\\015210205100726Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8a8081892e04badb012e04bd6a8500020\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\236\\236\\2250.z\\327\\341\\207\\204\\004`^\\373\\321\\207\\257\\263G\\252\\277\\3032g"\\3352\\313\\031\\253\\015eO\\354\\245\\270LY\\033\\0317X\\330\\004\\277\\222\\023\\332<%\\316v\\037>\\205\\271!\\355\\261\\334T\\026l\\027\\351\\242\\224m\\015\\356\\377\\273+O\\260\\342A\\253\\276\\374\\002\\303\\227\\235\\360\\370\\012{N\\320H\\006\\340\\311\\361Q\\204t\\373\\353W4ok\\366\\024\\347U9T\\261\\362\\263\\036\\015CQ\\221\\277\\204F\\253.\\003\\017\\275W\\3140\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024i\\010W\\021\\024\\233\\266\\252\\276R9-=L\\342\\342\\006=|\\235\\002\\024@\\314\\272\\207a\\247<v\\016g\\222.\\274\\201\\243X\\324gz\\214\\246\\352\\356\\313;\\011\\344vv\\335m$\\244\\247\\214\\0138\\274\\326\\300	0\\202\\002\\3560\\202\\002\\254\\240\\003\\002\\001\\002\\002\\004MQ\\025\\3360\\013\\006\\007*\\206H\\3168\\004\\001\\005\\0000Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8a8081892e04badb012e04bd6a8500020\\036\\027\\015110208100726Z\\027\\015210205100726Z0Z1\\0240\\022\\006\\003U\\004\\013\\023\\013Connexience1\\0270\\025\\006\\012\\011\\222&\\211\\223\\362,d\\001\\031\\026\\007Objects1)0'\\006\\003U\\004\\003\\023 8a8081892e04badb012e04bd6a8500020\\202\\001\\2700\\202\\001,\\006\\007*\\206H\\3168\\004\\0010\\202\\001\\037\\002\\201\\201\\000\\375\\177S\\201\\035u\\022)R\\337J\\234.\\354\\344\\347\\366\\021\\267R<\\357D\\000\\303\\036?\\200\\266Q&iE]@"Q\\373Y=\\215X\\372\\277\\305\\365\\2720\\366\\313\\233Ul\\327\\201;\\200\\0354o\\362f`\\267k\\231P\\245\\244\\237\\237\\350\\004{\\020"\\302O\\273\\251\\327\\376\\267\\306\\033\\370;W\\347\\306\\250\\246\\025\\017\\004\\373\\203\\366\\323\\305\\036\\303\\0025T\\023Z\\026\\2212\\366u\\363\\256+a\\327*\\357\\362"\\003\\031\\235\\321H\\001\\307\\002\\025\\000\\227`P\\217\\025#\\013\\314\\262\\222\\271\\202\\242\\353\\204\\013\\360X\\034\\365\\002\\201\\201\\000\\367\\341\\240\\205\\326\\233=\\336\\313\\274\\253\\\\6\\270W\\271y\\224\\257\\273\\372:\\352\\202\\371WL\\013=\\007\\202gQYW\\216\\272\\324YO\\346q\\007\\020\\201\\200\\264I\\026q#\\350L(\\026\\023\\267\\317\\0112\\214\\310\\246\\341<\\026z\\213T|\\215(\\340\\243\\256\\036+\\263\\246u\\221n\\243\\177\\013\\372!5b\\361\\373bz\\001$;\\314\\244\\361\\276\\250Q\\220\\211\\250\\203\\337\\341Z\\345\\237\\006\\222\\213f^\\200{U%d\\001L;\\376\\317I*\\003\\201\\205\\000\\002\\201\\201\\000\\236\\236\\2250.z\\327\\341\\207\\204\\004`^\\373\\321\\207\\257\\263G\\252\\277\\3032g"\\3352\\313\\031\\253\\015eO\\354\\245\\270LY\\033\\0317X\\330\\004\\277\\222\\023\\332<%\\316v\\037>\\205\\271!\\355\\261\\334T\\026l\\027\\351\\242\\224m\\015\\356\\377\\273+O\\260\\342A\\253\\276\\374\\002\\303\\227\\235\\360\\370\\012{N\\320H\\006\\340\\311\\361Q\\204t\\373\\353W4ok\\366\\024\\347U9T\\261\\362\\263\\036\\015CQ\\221\\277\\204F\\253.\\003\\017\\275W\\3140\\013\\006\\007*\\206H\\3168\\004\\001\\005\\000\\003/\\0000,\\002\\024i\\010W\\021\\024\\233\\266\\252\\276R9-=L\\342\\342\\006=|\\235\\002\\024@\\314\\272\\207a\\247<v\\016g\\222.\\274\\201\\243X\\324gz\\214
\.


--
-- TOC entry 2019 (class 0 OID 32831)
-- Dependencies: 1622
-- Data for Name: logevents; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logevents (id, operation, "timestamp", userid, principalname, workflowid, invocationid, versionid, workflowname, state, objectid, objectname, objecttype, serviceid, serviceversionid, serviceversionnumber, servicename, granteeid, granteetype, granteename) FROM stdin;
8ac3a18f2deb6a85012deb6af9f40008	WRITE	1296734812640	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af9a70007	Users	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa130009	WRITE	1296734812687	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa21000b	WRITE	1296734812701	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afa17000a	Groups	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa2c000c	WRITE	1296734812713	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa4d000e	WRITE	1296734812746	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afa31000d	Data	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa58000f	WRITE	1296734812757	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa620011	WRITE	1296734812767	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afa590010	Partnerships	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa730012	WRITE	1296734812780	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa900014	WRITE	1296734812813	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afa870013	Document Types	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa9a0015	WRITE	1296734812823	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afaa40017	WRITE	1296734812833	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afa9b0016	Databases	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afab30018	WRITE	1296734812848	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afacc001a	WRITE	1296734812873	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afab40019	Clusters	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afad8001b	WRITE	1296734812885	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afaeb001d	WRITE	1296734812896	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afad9001c	Services	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afb0a001e	WRITE	1296734812934	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afb1a0020	WRITE	1296734812950	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6afb10001f	Applications	folder	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afb250021	WRITE	1296734812962	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b15440024	WRITE	1296734819648	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6b153a0023	Admin	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b154e0025	MAKE_GROUP	1296734819658	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6b153a0023	Admin	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b2e3f0028	WRITE	1296734826044	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6b2e350027	Users	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b2e4a0029	MAKE_GROUP	1296734826054	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6b2e350027	Users	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b4188002c	WRITE	1296734830981	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6b4180002b	Public	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b418f002d	MAKE_GROUP	1296734830989	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6b4180002b	Public	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6b468c002f	WRITE	1296734832260	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6b467e002e	Public User	user	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6cc3000034	WRITE	1296734929662	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6cc2f90033	Admin	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6cc3080035	MAKE_GROUP	1296734929669	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6cc2f90033	Admin	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6cc9800036	WRITE	1296734931326	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d2b2e0039	WRITE	1296734956331	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d2b260038	Users	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d2b35003a	MAKE_GROUP	1296734956339	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d2b260038	Users	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d30d5003d	WRITE	1296734957778	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d30cc003c	Public	group	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d30dc003e	MAKE_GROUP	1296734957786	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d30cc003c	Public	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d33280040	WRITE	1296734958368	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d3317003f	Public User	user	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d389e0043	WRITE	1296734959767	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d38920042	Public User	user	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d43ff0045	WRITE	1296734962684	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d9a3b0047	WRITE	1296734984757	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d9a300046	orgdata	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d9a9e0048	WRITE	1296734984859	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6da0390049	WRITE	1296734986294	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6af58e0005	organisation	unknown	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6f0973004a	SHARE	1296735078701	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afb10001f	8ac3a18f2deb6a85012deb6afb10001f	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6f0990004b	SHARE	1296735078781	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afb10001f	8ac3a18f2deb6a85012deb6afb10001f	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6f41a5004c	SHARE	1296735093123	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afa870013	8ac3a18f2deb6a85012deb6afa870013	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6f41c2004d	SHARE	1296735093168	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afa870013	8ac3a18f2deb6a85012deb6afa870013	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6f956a004e	SHARE	1296735114566	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afa17000a	8ac3a18f2deb6a85012deb6afa17000a	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6f9586004f	SHARE	1296735114610	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6afa17000a	8ac3a18f2deb6a85012deb6afa17000a	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb6fc6770050	SHARE	1296735127126	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6af9a70007	8ac3a18f2deb6a85012deb6af9a70007	folder	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d38920042	public	Public User
8ac3a18f2deb6a85012deb7043740052	WRITE	1296735159143	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6d38920042	Public User	public	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd6ac70003	WRITE	1297159645873	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd6a850002	admin@admin.com	user	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70190006	WRITE	1297159647253	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd6ffa0005	admin@admin.com Home Folder	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70230008	WRITE	1297159647264	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd701a0007	Workflows	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd7033000a	WRITE	1297159647275	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd70240009	Web Folder	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd7054000c	WRITE	1297159647312	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd7035000b	Inbox	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd705f000e	WRITE	1297159647323	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd7055000d	Sent Messages	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70690010	WRITE	1297159647333	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd7060000f	External Objects	folder	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd709d0011	WRITE	1297159647357	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd6a850002	admin@admin.com	user	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70b60013	WRITE	1297159647406	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd6a850002	admin@admin.com	user	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70e70014	WRITE	1297159647455	8ac3a18f2deb6a85012deb6d38920042	Public User	\N	\N	0	\N	\N	8a8081892e04badb012e04bd6a850002	admin@admin.com	user	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd714b0015	SHARE	1297159647490	INTERNAL_TICKET		\N	\N	\N	\N	\N	8a8081892e04badb012e04bd6a850002	admin@admin.com	user	\N	\N	\N	\N	8a8081892e04badb012e04bd6a850002	user	
8a8081892e04badb012e04bd716a0016	LOGIN	1297159647588	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd71700017	REGISTER	1297159647566	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04be22c30019	WRITE	1297159692991	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	0	\N	\N	8ac3a18f2deb6a85012deb6cc2f90033	Admin	group	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04be22e0001a	MAKE_GROUP	1297159693001	8ac3a18f2deb6a85012deb6a935e0001	Root User	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6cc2f90033	Admin	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c09a58001b	LOGIN	1297159854644	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c148f7001d	WRITE	1297159899380	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	0	\N	\N	8a8081892e04badb012e04c148ee001c	Text	unknown	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c1eb9d001f	WRITE	1297159941017	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	0	\N	\N	8a8081892e04badb012e04c1eb7f001e	jpg	unknown	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c2153c0021	WRITE	1297159951673	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	0	\N	\N	8a8081892e04badb012e04c215330020	Java	unknown	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c2591c0023	WRITE	1297159969045	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	0	\N	\N	8a8081892e04badb012e04c2590f0022	CSV	unknown	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c291790025	WRITE	1297159983478	8a8081892e04badb012e04bd6a850002	admin admin	\N	\N	0	\N	\N	8a8081892e04badb012e04c2916d0024	Xml	unknown	\N	\N	\N	\N	\N	\N	\N
\.


--
-- TOC entry 2020 (class 0 OID 32837)
-- Dependencies: 1623
-- Data for Name: logeventsannonymised; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logeventsannonymised (id, operation, "timestamp", userid, workflowid, invocationid, versionid, state, objectid, objecttype, granteeid, granteetype) FROM stdin;
\.


--
-- TOC entry 2021 (class 0 OID 32843)
-- Dependencies: 1624
-- Data for Name: logmessageproperties; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logmessageproperties (id, logmessageid, name, value) FROM stdin;
\.


--
-- TOC entry 2022 (class 0 OID 32849)
-- Dependencies: 1625
-- Data for Name: logmessages; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logmessages (id, messagedate, messagetext, objectid, organisationid, principalid, type) FROM stdin;
\.


--
-- TOC entry 2023 (class 0 OID 32855)
-- Dependencies: 1626
-- Data for Name: logondetails; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY logondetails (id, userid, logonname, hashedpassword) FROM stdin;
1	8ac3a18f2deb6a85012deb6a935e0001	root	5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8
12	8a8081892e04badb012e04bd6a850002	admin@admin.com	d033e22ae348aeb5660fc2140aec35850c4da997
\.


--
-- TOC entry 2024 (class 0 OID 32861)
-- Dependencies: 1627
-- Data for Name: metadata; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY metadata (id, objectid, name, value) FROM stdin;
\.


--
-- TOC entry 2025 (class 0 OID 32867)
-- Dependencies: 1628
-- Data for Name: objectlocators; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY objectlocators (id, objectid, partnershipid, organisationid) FROM stdin;
\.


--
-- TOC entry 2026 (class 0 OID 32873)
-- Dependencies: 1629
-- Data for Name: objectsflat; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY objectsflat (id, objecttype, name, organisationid, containerid, description, creatorid, timeinmillis, jndiname, storedata, remotecertificatedata, remoteorganisationid, remotehost, remoteport, allowdatafolderbrowse, allowgrouplisting, allowdatauploads, allowpermissionsetting, enabled, tablename, idcolumn, datacolumn, buffersize, databasevendor, directory, accesskeyid, accesskey, organisationbucket, rootuserid, firstname, surname, defaultgroupid, homefolderid, profileid, webfolderid, workflowfolderid, inboxfolderid, sentmessagesfolderid, externalobjectsfolderid, hashkey, protectedgroup, adminapprovejoin, nonmemberslist, admingroupid, groupfolderid, userfolderid, datafolderid, projectsfolderid, trustedfolderid, datastoreid, partnersfolderid, documenttypesfolderid, databasesfolderid, clusterfolderid, servicesfolderid, applicationsfolderid, publicgroupid, defaultuserid, managernode, publicvisible, partnershipid, partnerfolderid, enddate, startdate, milestone, estimatedcompletion, parentprojectid, invocationid, workflowid, invocationdate, invocationstatus, currentblockid, engineid, approvergroupid, approversigningenforced, reviewergroupid, reviewersigningenforced, applicationid, typestring, documenttypeid, versioned, maxversions, limitversions, currentversionnumber, intermediatedatastored, externaldatasupported, externaldatablockname, enginetype, category, libraryname, formattype, mimetype, extension, homepage, serviceurl, serviceroutine, type, propertynames, propertytypes, inputnames, inputtypes, inputmodes, outputnames, outputtypes, propertydescriptions, propertydefaults, externallyprovisioned, externalscripttype, externalscriptid, streammode, title, body, secretkey, applicationurl, columnurl, documentediturl, mimetypelist, iconurl, showiniframe, documentationurl, venue, sourceobjectid, sinkobjectid, "timestamp", shortname, privateurl, blogid, notification, summary, postid, text, authorname, message, recipientid, senderid, read, threadid, isread, comment, status, groupid, label, itemtype, mainitemid, metadataid, promotionalbalance, purchasedbalance, currencylabel, pagenumber) FROM stdin;
8ac3a18f2deb6a85012deb6a935e0001	USER	Root User	\N	\N	Root User	\N	1296734786392	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Root	User	\N				\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6a93980002	ROOT	\N	\N	\N	\N	\N	1296734786456	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6a935e0001	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d2b260038	GROUP	Users	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa17000a		8ac3a18f2deb6a85012deb6a935e0001	1296734951313	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6af9a70007	USERSFOLDER	Users	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	User accounts	8ac3a18f2deb6a85012deb6a935e0001	1296734812583	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d30cc003c	GROUP	Public	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa17000a	\N	8ac3a18f2deb6a85012deb6a935e0001	1296734957680	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa17000a	GROUPSFOLDER	Groups	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Group details	8ac3a18f2deb6a85012deb6a935e0001	1296734812695	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa31000d	DATAFOLDER	Data	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Organisation Documents and Data	8ac3a18f2deb6a85012deb6a935e0001	1296734812720	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa590010	PARTNERSFOLDER	Partnerships	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Partnership connections for this organisation	8ac3a18f2deb6a85012deb6a935e0001	1296734812761	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa870013	DOCUMENTTYPESFOLDER	Document Types	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Document types recognised by the organisation	8ac3a18f2deb6a85012deb6a935e0001	1296734812807	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d9a300046	FILEDATESTORE	orgdata	8ac3a18f2deb6a85012deb6af58e0005	\N		8ac3a18f2deb6a85012deb6a935e0001	1296734969861	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	/Users/nsjw7/temp/orgdata	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afa9b0016	DATABASEFOLDER	Databases	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Databases that can be queried by Organisation Users	8ac3a18f2deb6a85012deb6a935e0001	1296734812827	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6af58e0005	ORGANISATION	organisation	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005		\N	1296734796638	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6d2b260038	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	8ac3a18f2deb6a85012deb6cc2f90033	8ac3a18f2deb6a85012deb6afa17000a	8ac3a18f2deb6a85012deb6af9a70007	8ac3a18f2deb6a85012deb6afa31000d	\N	\N	8ac3a18f2deb6a85012deb6d9a300046	8ac3a18f2deb6a85012deb6afa590010	8ac3a18f2deb6a85012deb6afa870013	8ac3a18f2deb6a85012deb6afa9b0016	8ac3a18f2deb6a85012deb6afab40019	8ac3a18f2deb6a85012deb6afad9001c	8ac3a18f2deb6a85012deb6afb10001f	8ac3a18f2deb6a85012deb6d30cc003c	8ac3a18f2deb6a85012deb6d38920042	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afab40019	CLUSTERFOLDER	Clusters	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Compute Cluster Instances	8ac3a18f2deb6a85012deb6a935e0001	1296734812852	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6d38920042	USER	Public User	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af9a70007	\N	8ac3a18f2deb6a85012deb6a935e0001	1296734959679	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Public	User	8ac3a18f2deb6a85012deb6d30cc003c		8ac3a18f2deb6a85012deb70435f0051		\N	\N	\N	\N	6f2a2822c164b6ba6950f6f9b08ccb3e	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afad9001c	SERVICESFOLDER	Services	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	Workflow Services Definitions	8ac3a18f2deb6a85012deb6a935e0001	1296734812889	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6afb10001f	APPLICATIONSFOLDER	Applications	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af58e0005	External Application Definitions	8ac3a18f2deb6a85012deb6a935e0001	1296734812944	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8ac3a18f2deb6a85012deb6cc2f90033	GROUP	Admin	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa17000a		8ac3a18f2deb6a85012deb6a935e0001	1296734920319	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	t	t	t	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd6ffa0005	FOLDER	admin@admin.com Home Folder	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa31000d	Home folder for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647226	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd701a0007	FOLDER	Workflows	8ac3a18f2deb6a85012deb6af58e0005	8a8081892e04badb012e04bd6ffa0005	Workflow folder for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647258	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd70240009	FOLDER	Web Folder	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa31000d	Web folder for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647268	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd7035000b	FOLDER	Inbox	8ac3a18f2deb6a85012deb6af58e0005	8a8081892e04badb012e04bd70240009	Inbox folder for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647285	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd7055000d	FOLDER	Sent Messages	8ac3a18f2deb6a85012deb6af58e0005	8a8081892e04badb012e04bd70240009	Sent Messages folder for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647317	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd7060000f	FOLDER	External Objects	8ac3a18f2deb6a85012deb6af58e0005	8a8081892e04badb012e04bd70240009	External objects for admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159647328	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c1eb7f001e	DOCUMENTTYPE	jpg	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa870013	\N	8a8081892e04badb012e04bd6a850002	1297159940917	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	image/jpeg	jpg	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04bd6a850002	USER	admin@admin.com	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6af9a70007	User admin@admin.com	8a8081892e04badb012e04bd6a850002	1297159645780	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	admin	admin	8ac3a18f2deb6a85012deb6d2b260038	8a8081892e04badb012e04bd6ffa0005	8a8081892e04badb012e04bd70a50012	8a8081892e04badb012e04bd70240009	8a8081892e04badb012e04bd701a0007	8a8081892e04badb012e04bd7035000b	8a8081892e04badb012e04bd7055000d	8a8081892e04badb012e04bd7060000f	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c148ee001c	DOCUMENTTYPE	Text	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa870013	\N	8a8081892e04badb012e04bd6a850002	1297159899234	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	text/plain	txt	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c215330020	DOCUMENTTYPE	Java	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa870013	\N	8a8081892e04badb012e04bd6a850002	1297159951554	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	text/java	java	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c2590f0022	DOCUMENTTYPE	CSV	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa870013	\N	8a8081892e04badb012e04bd6a850002	1297159968948	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	text/csv	csv	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
8a8081892e04badb012e04c2916d0024	DOCUMENTTYPE	Xml	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6afa870013	\N	8a8081892e04badb012e04bd6a850002	1297159983379	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	0	text/xml	xml	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N
\.


--
-- TOC entry 2027 (class 0 OID 32879)
-- Dependencies: 1630
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY permissions (id, principalid, targetobjectid, type, universal) FROM stdin;
5	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afb10001f	read	t
6	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afb10001f	add	t
7	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afa870013	read	t
8	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afa870013	add	t
9	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afa17000a	read	t
10	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6afa17000a	add	t
11	8ac3a18f2deb6a85012deb6d38920042	8ac3a18f2deb6a85012deb6af9a70007	read	t
14	8a8081892e04badb012e04bd6a850002	8a8081892e04badb012e04bd6a850002	read	t
\.


--
-- TOC entry 2028 (class 0 OID 32885)
-- Dependencies: 1631
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY products (id, producttype, name, description, categorylabel, ownerid, expirable, duration, autorenewing, productcost, imageurl, purchasewithpromotionaltokens, singlesubscription, objectidorname, active, initialvalue, units, value) FROM stdin;
\.


--
-- TOC entry 2029 (class 0 OID 32891)
-- Dependencies: 1632
-- Data for Name: promotialitems; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY promotialitems (id, quantity, creationtime, expirytime, promotioncode) FROM stdin;
\.


--
-- TOC entry 2030 (class 0 OID 32894)
-- Dependencies: 1633
-- Data for Name: propertygroups; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY propertygroups (id, objectproperty, objectid, name, description, organisationid) FROM stdin;
\.


--
-- TOC entry 2031 (class 0 OID 32900)
-- Dependencies: 1634
-- Data for Name: propertyitems; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY propertyitems (id, groupid, name, value) FROM stdin;
\.


--
-- TOC entry 2032 (class 0 OID 32906)
-- Dependencies: 1635
-- Data for Name: rememberedlogins; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY rememberedlogins (id, userid, cookieid, expirydate) FROM stdin;
\.


--
-- TOC entry 2033 (class 0 OID 32912)
-- Dependencies: 1636
-- Data for Name: servicehosts; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY servicehosts (id, ipaddress, active, guid) FROM stdin;
\.


--
-- TOC entry 2034 (class 0 OID 32918)
-- Dependencies: 1637
-- Data for Name: serviceinstances; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY serviceinstances (id, rmiport, running, hostid, name) FROM stdin;
\.


--
-- TOC entry 2035 (class 0 OID 32924)
-- Dependencies: 1638
-- Data for Name: servicexml; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY servicexml (id, serviceid, versionid, xmldata) FROM stdin;
\.


--
-- TOC entry 2036 (class 0 OID 32930)
-- Dependencies: 1639
-- Data for Name: subscriptions; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY subscriptions (id, subscriptiontype, name, categorylabel, userid, expiry, autorenewing, productid, imageurl, starttime, value, units) FROM stdin;
\.


--
-- TOC entry 2037 (class 0 OID 32936)
-- Dependencies: 1640
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tags (id, tagtext) FROM stdin;
\.


--
-- TOC entry 2038 (class 0 OID 32942)
-- Dependencies: 1641
-- Data for Name: tagstoobjects; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tagstoobjects (id, serverobjectid, tagid, creatorid, weight, createdate) FROM stdin;
\.


--
-- TOC entry 2039 (class 0 OID 32948)
-- Dependencies: 1642
-- Data for Name: ticketgroups; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY ticketgroups (id, ticketid, groupid) FROM stdin;
\.


--
-- TOC entry 2040 (class 0 OID 32954)
-- Dependencies: 1643
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY tickets (id, organisationid, userid, lastaccesstime, superticket) FROM stdin;
8ac3a18f2deb6a85012deb6bb80e0031	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6a935e0001	2011-02-03 12:12:07.098	t
8ac3a18f2deb6a85012deb6ab03f0004	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6a935e0001	2011-02-03 12:07:13.487	t
8a8081892e04badb012e04bae7410001	8ac3a18f2deb6a85012deb6af58e0005	8ac3a18f2deb6a85012deb6a935e0001	2011-02-08 10:14:11.377	t
\.


--
-- TOC entry 2041 (class 0 OID 32960)
-- Dependencies: 1644
-- Data for Name: uploadreservations; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY uploadreservations (id, userid, documentid, signature, "timestamp", certificatehash, comments) FROM stdin;
\.


--
-- TOC entry 2042 (class 0 OID 32966)
-- Dependencies: 1645
-- Data for Name: userprofiles; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY userprofiles (id, text, website, emailaddress, defaultdomain) FROM stdin;
8ac3a18f2deb6a85012deb70435f0051		\N	\N	
8a8081892e04badb012e04bd70a50012		\N	admin@admin.com	http://localhost:8080/esc
\.


--
-- TOC entry 2043 (class 0 OID 32972)
-- Dependencies: 1646
-- Data for Name: workflowengines; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY workflowengines (id, engineip, queuename, lastaccesstime) FROM stdin;
\.


--
-- TOC entry 2044 (class 0 OID 32978)
-- Dependencies: 1647
-- Data for Name: workflowservicelogs; Type: TABLE DATA; Schema: public; Owner: connexiencedb
--

COPY workflowservicelogs (id, invocationid, contextid, outputtext) FROM stdin;
\.


--
-- TOC entry 1926 (class 2606 OID 32990)
-- Dependencies: 1607 1607
-- Name: accountactvity_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY accountactvity
    ADD CONSTRAINT accountactvity_pkey PRIMARY KEY (id);


--
-- TOC entry 1928 (class 2606 OID 32992)
-- Dependencies: 1608 1608
-- Name: appsubscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY appsubscriptions
    ADD CONSTRAINT appsubscriptions_pkey PRIMARY KEY (id);


--
-- TOC entry 1930 (class 2606 OID 32994)
-- Dependencies: 1609 1609
-- Name: blockhelp_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY blockhelp
    ADD CONSTRAINT blockhelp_pkey PRIMARY KEY (id);


--
-- TOC entry 1932 (class 2606 OID 32996)
-- Dependencies: 1610 1610
-- Name: blogpostread_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY blogpostread
    ADD CONSTRAINT blogpostread_pkey PRIMARY KEY (id);


--
-- TOC entry 1934 (class 2606 OID 32998)
-- Dependencies: 1611 1611
-- Name: bookcontent_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY bookcontent
    ADD CONSTRAINT bookcontent_pkey PRIMARY KEY (id);


--
-- TOC entry 1936 (class 2606 OID 33000)
-- Dependencies: 1612 1612
-- Name: clusterenvironments_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY clusterenvironments
    ADD CONSTRAINT clusterenvironments_pkey PRIMARY KEY (id);


--
-- TOC entry 1938 (class 2606 OID 33002)
-- Dependencies: 1613 1613
-- Name: clusterjobs_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY clusterjobs
    ADD CONSTRAINT clusterjobs_pkey PRIMARY KEY (id);


--
-- TOC entry 1940 (class 2606 OID 33004)
-- Dependencies: 1614 1614
-- Name: documentversions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY documentversions
    ADD CONSTRAINT documentversions_pkey PRIMARY KEY (id);


--
-- TOC entry 1942 (class 2606 OID 33006)
-- Dependencies: 1615 1615
-- Name: downloadreservations_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY downloadreservations
    ADD CONSTRAINT downloadreservations_pkey PRIMARY KEY (id);


--
-- TOC entry 1944 (class 2606 OID 33008)
-- Dependencies: 1616 1616
-- Name: externallogondetails_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY externallogondetails
    ADD CONSTRAINT externallogondetails_pkey PRIMARY KEY (id);


--
-- TOC entry 1946 (class 2606 OID 33010)
-- Dependencies: 1617 1617
-- Name: groupmembership_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY groupmembership
    ADD CONSTRAINT groupmembership_pkey PRIMARY KEY (id);


--
-- TOC entry 1948 (class 2606 OID 33012)
-- Dependencies: 1618 1618
-- Name: groupprofiles_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY groupprofiles
    ADD CONSTRAINT groupprofiles_pkey PRIMARY KEY (id);


--
-- TOC entry 1950 (class 2606 OID 33014)
-- Dependencies: 1620 1620
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- TOC entry 1952 (class 2606 OID 33016)
-- Dependencies: 1621 1621
-- Name: keys_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY keys
    ADD CONSTRAINT keys_pkey PRIMARY KEY (id);


--
-- TOC entry 1954 (class 2606 OID 33018)
-- Dependencies: 1622 1622
-- Name: logevents_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logevents
    ADD CONSTRAINT logevents_pkey PRIMARY KEY (id);


--
-- TOC entry 1956 (class 2606 OID 33020)
-- Dependencies: 1623 1623
-- Name: logeventsannonymised_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logeventsannonymised
    ADD CONSTRAINT logeventsannonymised_pkey PRIMARY KEY (id);


--
-- TOC entry 1958 (class 2606 OID 33022)
-- Dependencies: 1624 1624
-- Name: logmessageproperties_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logmessageproperties
    ADD CONSTRAINT logmessageproperties_pkey PRIMARY KEY (id);


--
-- TOC entry 1960 (class 2606 OID 33024)
-- Dependencies: 1625 1625
-- Name: logmessages_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logmessages
    ADD CONSTRAINT logmessages_pkey PRIMARY KEY (id);


--
-- TOC entry 1962 (class 2606 OID 33026)
-- Dependencies: 1626 1626
-- Name: logondetails_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY logondetails
    ADD CONSTRAINT logondetails_pkey PRIMARY KEY (id);


--
-- TOC entry 1964 (class 2606 OID 33028)
-- Dependencies: 1627 1627
-- Name: metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- TOC entry 1966 (class 2606 OID 33030)
-- Dependencies: 1628 1628
-- Name: objectlocators_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY objectlocators
    ADD CONSTRAINT objectlocators_pkey PRIMARY KEY (id);


--
-- TOC entry 1968 (class 2606 OID 33032)
-- Dependencies: 1629 1629
-- Name: objectsflat_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY objectsflat
    ADD CONSTRAINT objectsflat_pkey PRIMARY KEY (id);


--
-- TOC entry 1970 (class 2606 OID 33034)
-- Dependencies: 1630 1630
-- Name: permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 1972 (class 2606 OID 33036)
-- Dependencies: 1631 1631
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 1974 (class 2606 OID 33038)
-- Dependencies: 1632 1632
-- Name: promotialitems_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY promotialitems
    ADD CONSTRAINT promotialitems_pkey PRIMARY KEY (id);


--
-- TOC entry 1976 (class 2606 OID 33040)
-- Dependencies: 1633 1633
-- Name: propertygroups_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY propertygroups
    ADD CONSTRAINT propertygroups_pkey PRIMARY KEY (id);


--
-- TOC entry 1978 (class 2606 OID 33042)
-- Dependencies: 1634 1634
-- Name: propertyitems_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY propertyitems
    ADD CONSTRAINT propertyitems_pkey PRIMARY KEY (id);


--
-- TOC entry 1980 (class 2606 OID 33044)
-- Dependencies: 1635 1635
-- Name: rememberedlogins_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY rememberedlogins
    ADD CONSTRAINT rememberedlogins_pkey PRIMARY KEY (id);


--
-- TOC entry 1982 (class 2606 OID 33046)
-- Dependencies: 1636 1636
-- Name: servicehosts_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY servicehosts
    ADD CONSTRAINT servicehosts_pkey PRIMARY KEY (id);


--
-- TOC entry 1984 (class 2606 OID 33048)
-- Dependencies: 1637 1637
-- Name: serviceinstances_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY serviceinstances
    ADD CONSTRAINT serviceinstances_pkey PRIMARY KEY (id);


--
-- TOC entry 1986 (class 2606 OID 33050)
-- Dependencies: 1638 1638
-- Name: servicexml_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY servicexml
    ADD CONSTRAINT servicexml_pkey PRIMARY KEY (id);


--
-- TOC entry 1988 (class 2606 OID 33052)
-- Dependencies: 1639 1639
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- TOC entry 1990 (class 2606 OID 33054)
-- Dependencies: 1640 1640
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 1992 (class 2606 OID 33056)
-- Dependencies: 1641 1641
-- Name: tagstoobjects_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tagstoobjects
    ADD CONSTRAINT tagstoobjects_pkey PRIMARY KEY (id);


--
-- TOC entry 1994 (class 2606 OID 33058)
-- Dependencies: 1642 1642
-- Name: ticketgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY ticketgroups
    ADD CONSTRAINT ticketgroups_pkey PRIMARY KEY (id);


--
-- TOC entry 1996 (class 2606 OID 33060)
-- Dependencies: 1643 1643
-- Name: tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- TOC entry 1998 (class 2606 OID 33062)
-- Dependencies: 1644 1644
-- Name: uploadreservations_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY uploadreservations
    ADD CONSTRAINT uploadreservations_pkey PRIMARY KEY (id);


--
-- TOC entry 2000 (class 2606 OID 33064)
-- Dependencies: 1645 1645
-- Name: userprofiles_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY userprofiles
    ADD CONSTRAINT userprofiles_pkey PRIMARY KEY (id);


--
-- TOC entry 2002 (class 2606 OID 33066)
-- Dependencies: 1646 1646
-- Name: workflowengines_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY workflowengines
    ADD CONSTRAINT workflowengines_pkey PRIMARY KEY (id);


--
-- TOC entry 2004 (class 2606 OID 33068)
-- Dependencies: 1647 1647
-- Name: workflowservicelogs_pkey; Type: CONSTRAINT; Schema: public; Owner: connexiencedb; Tablespace: 
--

ALTER TABLE ONLY workflowservicelogs
    ADD CONSTRAINT workflowservicelogs_pkey PRIMARY KEY (id);


--
-- TOC entry 2049 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: nsjw7
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nsjw7;
GRANT ALL ON SCHEMA public TO nsjw7;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2011-02-08 10:15:21 GMT

--
-- PostgreSQL database dump complete
--

