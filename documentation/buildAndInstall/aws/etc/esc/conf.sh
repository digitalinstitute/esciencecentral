#!/bin/bash
export ENGINE_HOME=/esc/engine
export CONFIG_URL=http://localhost:8080
export DOWNLOAD_URL=http://localhost:8080
export RUNTIME_ROOT_DIRECTORY=/mnt/tmp/workflow
export ENGINE_USER=ubuntu
export ENGINE_GROUP=ubuntu
export JAVA_HOME=/usr/lib/jvm/default-java
export ENGINE_LOG_FILE=/esc/wfe.log
