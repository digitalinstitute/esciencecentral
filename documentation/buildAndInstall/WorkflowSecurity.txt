To get workflow isolation to work properly, you need to make some changes to the sudoers
file in Linux/OSX (N.B. Isolation doesn't work in windows).

For OSX use:

# Workflow engine permissions
nsjw7 ALL=NOPASSWD:/usr/bin/dscl
nsjw7 ALL=NOPASSWD:/bin/chmod
nsjw7 ALL=NOPASSWD:/usr/sbin/chown
nsjw7 ALL=NOPASSWD:/usr/bin/chgrp
nsjw7 ALL=NOPASSWD:/usr/bin/killall
nsjw7 ALL=(%wfusers) NOPASSWD:/usr/bin/java
nsjw7 ALL=(%wfusers) NOPASSWD:/System/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home/bin/java

N.B. Substitute 'nsjw7' with the user that is running the workflow engine. Also the /System/Library/Java... entry should point at the actual java installation

For Linux use:

workflowengine ALL=NOPASSWD:/bin/rpm
workflowengine ALL=NOPASSWD:/usr/bin/yum
workflowengine ALL=NOPASSWD:/usr/sbin/useradd
workflowengine ALL=NOPASSWD:/bin/chmod
workflowengine ALL=NOPASSWD:/bin/chown
workflowengine ALL=NOPASSWD:/bin/chgrp
workflowengine ALL=NOPASSWD:/usr/bin/killall
workflowengine ALL=(%wfusers) NOPASSWD: /usr/bin/java
workflowengine ALL=(%wfusers) NOPASSWD: java


