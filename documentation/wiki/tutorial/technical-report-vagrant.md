# eSC & Vagrant

## New Files
If you do an `svn update` you'll notice two files at the root of the eSC source structure: `Vagrantfile` and `bootstrap.sh`. These files facilitate the quick deployment of  a single VM with the following features:

- Ubuntu 12.04 LTS
- Java 7
- Postgres 9.1 *(configured with connexience user and databases)*
- JBoss 7.1.3 *(with eScience Central configuration)*
- inkspot.ear deployed to the jboss (if the file is built -- more on this in a second)

## Getting Started

1. Install Vagrant ([link](vagrantup.com))
2. Install VirtualBox ([link](virtualbox.org)) 
3. In your terminal, `cd <eSC directory>` (the directory containing `bin/`, `code/`, `documentation/`, `Vagrantfile` and `bootstrap.sh`)
4. Execute `vagrant up` and watch as it downloads the base VM, deploys it locally and configures the Ubuntu instance.

## File Sharing
Vagrant does some magic where it mounts the directory containing the `Vagrantfile` under `/vagrant` on the Linux VM. So for us, this means `/vagrant` contains (`bin/`, `code/`, `documentation/`, `Vagrantfile` and `bootstrap.sh`).

This is a shared file space, so working on the code and building eSC locally will be instantly available inside the VM under `/vagrant`. This allows you to code and build the EAR in your IDE and the file is instantly available within the VM to copy into the JBoss deployment directory.

## Connecting to the VM
When `vagrant up` has successfully completed, you can connect to the VM instance by executing `vagrant ssh` (**NOTE:** You still need to be in the eSC directory where the `Vagrantfile` is present).

Once you have SSHd in, the VM will have 4 useful aliases that may be executed:

1. `deploy` -- copies inkspot.ear from /vagrant/code/server/Ear/target.ear to your ~/jboss/standalone/deployments directory
	* **NOTE:** Build as normal by invoking `mvn install` on your local machine
2. `jbossup` -- launches JBoss in the background
3. `jbossdown` -- kills JBoss (using the PID which is written to `~/jboss.pid`)
4. `jbosstail` -- tails the `server.log` file so you can see what your JBoss output is doing

## Recap and Quickstart
1. install vagrant and virtualbox
2. open terminal, `cd` to your esc working copy (where `bin/`, `code/`, `documentation/`, `Vagrantfile` and `bootstrap.sh` are)
3. execute `vagrant up`
4. execute `vagrant ssh`
5. execute `deploy` (make sure you have built `inkspot.ear` locally)
6. execute `jbossup`
7. execute `jbosstail` to see JBoss starting up

**NOTE:** You can actually redeploy within your VM without even `ssh`-ing into it. The `vagrant ssh` command can be given a `-c` parameter to make a single command execute inside the VM without leaving your current shell session. The following triggers a deployment by copying inkspot.ear into the jboss deployment directory without leaving your shell session:

    vagrant ssh -c 'cp /vagrant/code/server/Ear/target/inkspot.ear /home/vagrant/jboss/standalone/deployments/
    
## Port Forwarding
As part of the vagrant config (which you can see in `Vagrantfile`), traffic from port 8080 on your localhost is directed to port 8080 on the VM so in your local browser, you can point it at http://localhost:8080/beta and see your newly deployed instance.

## Shutting Down / Cleanup
From within the eSC directory, you can execute:

1. `vagrant suspend` to suspend the VM and its current state
	- `vagrant up` will cause this VM to come back to life exactly as it was when it was suspended
2. `vagrant halt` to shut the VM down
	- `vagrant up` will cause the VM to boot up from fresh, but with the file system and databases in tact
3. `vagrant destroy` to shut down the VM and completely remove the image
	- `vagrant up` will cause a new VM image to be deployed (and configured using `bootstrap.sh`)
	
## Provisioning through 'bootstrap.sh'
The `bootstrap.sh` file is used to *provision* the VM -- in Vagrant this means the bootstrap.sh script is run once when the VM is created. It will not be re-run if you simply `suspend` or `halt` your VM and bring it back `up`. This file currently does all of the setup of postgres, java and jboss and expects the contents of /vagrant to be the top level eSC directory. Be very careful if you intend to change this file, probably fire me an e-mail first.

## Nice Stuff
1. This is portable, so any machine with vagrant and virtual box installed will be able to throw up an instance using vagrant up.
2. Vagrant can be configured to use other providers (VirtualBox is default) -- these include VMWare, AWS and a couple of others
3. Sharing. From your local terminal (i.e. NOT inside the VM ssh) you can do `vagrant share` (it will ask you to sign up at vagrantcloud.com) and you can get a temporary public domain name that points directly at your locally running VM instance. By default only ports forwarded in your `Vagrantfile` are forwarded but you can open up SSH access or complete DMZ access to the VM if you like).
4. No impact on current work methods (if you hate the idea of this!).

## Future Work
1. Vagrant can bring up multiple VMs (e.g., separate web+db & worker or separate web, db & worker)
2. Vagrant is compatible with Chef, Puppet and Docker so we could go as far as to create a public image (docker gets my vote for this) that can be pulled down and launched as a local VM with even less setup time (e.g., the image could come down with java, postgres and jboss already).
3. If we intend to use Vagrant to deploy to AWS or other cloud providers, we won't rely on magic mounting /vagrant but will use the `Vagrantfile` to copy only what is needed to the VM at spawn time (e.g., jboss, jboss customisations, inkspot.ear)