# How to build source code

To build the sources you will need to use [Apache Maven](http://maven.apache.org) version 3+ (on Ubuntu you can install it with `apt-get install maven`). Change to the `code` directory and run maven build process:

~~~~~
> cd code
> mvn clean install
~~~~~

**Note:** maven requires the Java Development Kit. Currently, e-Science Central can only be built using **JDK 7** (neither JDK 8 nor JDK 6 and earlier will work).

If everything works fine, you should see maven logs on the screen:

~~~~~
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO]
[INFO] meta-project
[INFO] server-common
[INFO] server-utils
[INFO] workflow-engine

...
~~~~~

And after a while the build should complete successfully.

~~~~~
...

[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO] 
[INFO] meta-project ...................................... SUCCESS [9.355s]
[INFO] server-common ..................................... SUCCESS [24.032s]
[INFO] server-utils ...................................... SUCCESS [5.751s]
[INFO] workflow-engine ................................... SUCCESS [19.640s]
[INFO] server-beans ...................................... SUCCESS [12.590s]
[INFO] MongoDatasets ..................................... SUCCESS [3.877s]
[INFO] login-module ...................................... SUCCESS [0.908s]
[INFO] website ........................................... SUCCESS [16.861s]
[INFO] api-model ......................................... SUCCESS [3.917s]
[INFO] api-server ........................................ SUCCESS [3.747s]
[INFO] website-api-common ................................ SUCCESS [2.210s]
[INFO] website-api ....................................... SUCCESS [9.385s]
[INFO] workflow-manager .................................. SUCCESS [9.184s]
[INFO] inkspot-ear ....................................... SUCCESS [17.115s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 2:19.817s
[INFO] Finished at: Tue Sep 30 17:44:06 BST 2014
[INFO] Final Memory: 137M/329M
[INFO] ------------------------------------------------------------------------
~~~~~

If you experience any issues with building, please report problems to [esciencecentral-users@lists.sourceforge.net](mailto:esciencecentral-users@lists.sourceforge.net).
