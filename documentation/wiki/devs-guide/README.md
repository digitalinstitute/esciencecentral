e-Science Central Developers Guide
==================================

This guide includes (or will include) information for developers on how to integrate with the e-Science Central system.
Integration may involve tasks like developing your own blocks or integrating your external application with the system via API.
Currently, the guide covers the following topics:

* [Building the source code](building-instructions-r2317.md)
* [Getting started with the REST API](rest-api/getting-started.md)
* Using the storage API in [Java](rest-api/java-rest-client-r2317.md#storage-api-client) and in C#
* Using the workflow API in [Java](rest-api/java-rest-client-r2317.md#workflow-api-client) and in C#
* Understanding the data model
