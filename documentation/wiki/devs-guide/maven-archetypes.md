# Maven Archetypes #

[TOC]

To ease in development of e-Science Central blocks and libraries using your desktop computer a set of maven archetypes is provided. These archetype generate a ready-to-use block or library templates which you can build, upload and test in your instance. They are a good starting point to code your blocks.

However, before you can use any of the archetypes you need to build them.

## Building Archetypes ##

The archetypes are placed in the code repository under the [`/code/tools/maven`](../../../code/tools/maven) directory.
To compile, build and install the archetypes in your maven repository run commands:

~~~~~
cd /code/tools/maven
mvn clean install -P all
~~~~~

## Using Archetypes ##

You can use the maven archetypes to generate blocks for Java, JavaScript, Octave, R and gnuplot. Under `/code/tools/maven` you will find all available artifacts named `workflow-block-*`.
There's also an archetype to generate an e-Science Central java and binary library `workflow-library`, `workflow-binary-library`.

For example, to generate a Java block template using maven issue (on **Linux**):

~~~~~
mvn archetype:generate \
    -DarchetypeGroupId=com.connexience \
    -DarchetypeArtifactId=workflow-block-java \
    -DarchetypeVersion=3.1-SNAPSHOT \
    -DgroupId=org.myblocks \
    -DartifactId=FirstOne \
    -Dversion=1.0 \
    -DinteractiveMode=false
~~~~~

when on **Windows** change `'\'` to `'^'`:

~~~~~
mvn archetype:generate ^
    -DarchetypeGroupId=com.connexience ^
    -DarchetypeArtifactId=workflow-block-java ^
    -DarchetypeVersion=3.1-SNAPSHOT ^
    -DgroupId=org.myblocks ^
    -DartifactId=FirstOne ^
    -Dversion=1.0 ^
    -DinteractiveMode=false
~~~~~
---
**Note:** if this command fails reporting: `The desired archetype does not exist (com.connexience:workflow-block-java:3.1-SNAPSHOT)`, you need to build the archetypes first; please refer to the section above.
