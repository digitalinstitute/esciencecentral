#!/bin/bash

DIR=$(readlink -f $(dirname $0))
. $DIR/conf

sudo /vm/bin/vm-reset ag 1 || exit 1

scp -r $CONNEXIENCE_HOME/commercial/webflow/WorkflowCloud qa-01: || exit 1
ssh qa-01 'mkdir .inkspot' || exit 1
cat $DIR/engine.xml | sed "s|@SERVER_HOSTNAME@|10.9.1.1|g" | sed "s|@JMS_PORT@|5445|g" | ssh qa-01 'cat - > .inkspot/engine.xml' || exit 1
ssh qa-01 "echo 'completionServer=10.9.1.1' > .inkspot/performance.properties"
ssh qa-01 'mkdir -p /workflow/invocations /workflow/library /workflow/static' || exit 1
ssh qa-01 'cd WorkflowCloud; nohup bin/startserver.sh' || exit 1
