#!/bin/bash

source conf

rm -rf $INKSPOT_HOME/workflow-items/core
rm -rf /tmp/core

mkdir /tmp/core || exit 1
mkdir /tmp/core/lib || exit 1
mkdir -p $INKSPOT_HOME/workflow-items || exit 1

cp $CONNEXIENCE_HOME/code/webflow/WorkflowCloud/lib/APIInterface.jar /tmp/core/lib/ || exit 1
cp $CONNEXIENCE_HOME/code/webflow/WorkflowCloud/lib/ServerCommon.jar /tmp/core/lib/ || exit 1
cp $CONNEXIENCE_HOME/code/webflow/WorkflowCloud/lib/WorkflowCloud.jar /tmp/core/lib/ || exit 1
cp $CONNEXIENCE_HOME/code/webflow/WorkflowCloud/lib/WorkflowCore.jar /tmp/core/lib/ || exit 1

echo "<dependencies></dependencies>" > /tmp/core/dependencies.xml
echo "<library><type>JavaLibrary</type><name>core</name></library>" > /tmp/core/library.xml

cd /tmp/core
mkdir -p $INKSPOT_HOME/filedatastore
zip -r $INKSPOT_HOME/workflow-items/core *

mv $INKSPOT_HOME/workflow-items/core.zip $INKSPOT_HOME/workflow-items/core
mkdir -p $INKSPOT_HOME/filedatastore/8ac3a18f2deb6a85012deb6af58e0005/01/8ac3a1b22fdf714c012fdf883c630001
cp $INKSPOT_HOME/workflow-items/core $INKSPOT_HOME/filedatastore/8ac3a18f2deb6a85012deb6af58e0005/01/8ac3a1b22fdf714c012fdf883c630001/8ac3a1b22fdf714c012fdf883d760002



