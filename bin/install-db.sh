#!/bin/bash

source conf

mkdir -p $INKSPOT_HOME

sudo -u postgres psql -c 'create database connexience' || exit 1
#sudo -u postgres psql -c "create user connexience with password 'connexience'";
sudo -u postgres psql -c 'alter database connexience owner to connexience' || exit 1

cat $DB_IMPORT | sed "s|/opt/inkspot/filedatastore|$INKSPOT_HOME/filedatastore|" | sed "s|weetabix.csv|FoodData.csv|" > /tmp/import.sql || exit 1

sudo -u postgres psql -d connexience -f /tmp/import.sql || exit 1
rm /tmp/import.sql || exit 1

mkdir -p $INKSPOT_HOME/filedatastore
unzip $FILESTORE_IMPORT -d $INKSPOT_HOME/filedatastore || exit 1

