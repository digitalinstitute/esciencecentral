#!/bin/bash

source conf

mkdir -p $INKSPOT_HOME

unzip $JBOSS_ZIP -d $INKSPOT_HOME || exit 1

cp -R $CONNEXIENCE_HOME/documentation/buildAndInstall/jboss7/jboss-as-7.0.0.Final-inkspot/* $JBOSS_HOME || exit 1

#unzip -o $CONNEXIENCE_HOME/documentation/buildAndInstall/jboss7/jboss-as-7.0.0.Final-inkspot.zip -d $JBOSS_HOME || exit 1

$MVN clean install -f $CONNEXIENCE_HOME/commercial/MetaProject/pom.xml || exit 1
cp $CONNEXIENCE_HOME/commercial/server/Ear/target/inkspot-ear-1.0-SNAPSHOT.ear $JBOSS_HOME/standalone/deployments/ || exit 1

$JBOSS_HOME/bin/standalone.sh --server-config=inkspot.xml 1>$JBOSS_HOME/run.out 2>$JBOSS_HOME/run.err &
