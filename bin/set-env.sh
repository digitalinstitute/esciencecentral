#!/bin/bash

# This must be the absolute path to the java keystore file
TRUSTSTORE_PATH=???

# This is the password that protects the keystore
TRUSTSTORE_PASSWORD=???

export MAVEN_OPTS="-Djavax.net.ssl.trustStore=$TRUSTSTORE_PATH -Djavax.net.ssl.trustStorePassword=$TRUSTSTORE_PASSWORD"
