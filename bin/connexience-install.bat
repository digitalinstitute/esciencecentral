@echo off
::
:: This script installs the required local jars into the maven repository
::

call mvn install:install-file -Dpackaging=jar -DgroupId=com.netoprise -DartifactId=neo4j-connector-api -Dversion=0.3-SNAPSHOT -Dfile=../code/jars/neo4j-connector-api-0.3-SNAPSHOT.jar
call mvn install:install-file -Dpackaging=jar -DgroupId=jcaptcha      -DartifactId=jcaptcha            -Dversion=1.0          -Dfile=../code/jars/jcaptcha-1.0.jar
