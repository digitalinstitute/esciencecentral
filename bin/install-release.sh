#!/bin/sh

#TODO: stop ServiceHost and JBoss beforehand

ESC_RELEASE_NUM=2.1-SNAPSHOT;

# EXPECTED_ARGS=1
# E_BADARGS=65

# if [ $# -ne $EXPECTED_ARGS ]
# then
#   echo "Usage: `basename $0` {SERVER_NAME}"
#   exit $E_BADARGS
# fi

JBOSS_VERSION=jboss-as-7.1.1.Final;
export JBOSS_HOME=~/${JBOSS_VERSION};
WORKING_DIR=~/esc-${ESC_RELEASE_NUM};
ZIP_NAME=${WORKING_DIR}.zip;
DB_NAME=$1;

unzip ${ZIP_NAME} -d ${WORKING_DIR}
cd ${WORKING_DIR};

mv ${JBOSS_VERSION} ~

echo "1. Start JBoss to migrate schema"
echo "2. Stop JBoss"
echo "3. run the migrations in ${WORKING_DIR}/migrations"
echo "4. Start JBoss"
echo "5. Start ServiceHost"

#Todo: move this to separate script

#run the migrations
# cd migrations;
# for file in *.sql; 
# do   
# 	psql -U $DB_USER -d $DB_NAME -f $file  || exit 1  ;
# done
# cd ..;


#mv ${JBOSS_VERSION} ~

#$JBOSS_HOME/bin/standalone.sh 1>$JBOSS_HOME/run.out 2>$JBOSS_HOME/run.err &	

#tail -f $JBOSS_HOME/run.out

# cd ServiceHost/dist;
# ./bin/starthost.sh &
# disown

# tail -f temp/WorkflowService/service.log

