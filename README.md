# README #

e-Science Central is a Cloud based Platform for Data Analysis.  It supports secure storage and versioning of data, audit and provenance logs and processing of data using workflows.  Workflows are composed of blocks which can be written in **Java**, **R**, **Octave** or **Javascript**.  e-Science Central is portable and can be run on **Amazon AWS**, **Windows Azure** or your own hardware.  The e-Science Central platform along with basic analysis blocks are provided under an Open Source License.  e-Science Central is developed by a team based at Newcastle University, UK.

e-Science Central is licensed as GPLv2 and Copyright Newcastle University unless explicitly stated otherwise. A copy of the license is vailable in the root of the e-Science Central repository.

Contributers to e-Science Central include:

* Hugo Hiden
* Simon Woodman
* Jacek Cala
* Mark Turner
* Stephen Dowsland
* Derek Mortimer
* Dominic Searson
* Martyn Taylor